import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinContest/changeUserNameAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        changeUserNameResponse: undefined,
        success: true,
        errorMessage: ''
    },
    error: false
};

{ /* Request - Game Types List */ }
const changeUserName = (state, action) => {
    return produce(state, (draft) => {
        draft.model.changeUserNameResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const changeUserNameFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.changeUserNameResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_USER_NAME:
            return changeUserName(state, action);
        case actionTypes.CHANGE_USER_NAME_FAILED:
            return changeUserNameFailed(state, action);
        case actionTypes.RESET_CHANGE_USER_NAME_STATE:
            return resetChangeUserNameState(state);
        default:
            return state;
    }
};

export default (reducer);

const resetChangeUserNameState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.changeUserNameResponse = undefined;
    })
}