import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, SafeAreaView, Dimensions } from 'react-native'
import { containerStyles, positionStyles, heightStyles, widthStyles, flexDirectionStyles, justifyContentStyles, alignmentStyles, colorStyles, marginStyles, paddingStyles, borderStyles, imageStyles, fontFamilyStyles, fontSizeStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { addLog, getPaymentTimeStamp } from '../../appUtils/commonUtlis';
import WebFields from '../../webServices/webFields.json';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import { AppConstants } from '../../appUtils/appConstants';
import RNPgReactNativeSdk from 'react-native-pg-react-native-sdk';
import { Constants } from '../../appUtils/constants';
import * as generalSetting from '../../webServices/generalSetting';
import colors from '../../assets/colors/colors';
import moment from 'moment';
import CustomProgressbar from '../../appUtils/customProgressBar';

const height = (Dimensions.get('window').height - 210);

const Temp = (props) => {

    return (

        <SafeAreaView style={[containerStyles.container, colorStyles.greyBackgroundColor]}>

            <View style={[containerStyles.container, colorStyles.greyBackgroundColor]}>

                <View style={[heightStyles.height_210, alignmentStyles.alignItemsCenter, justifyContentStyles.center, borderStyles.borderTopLeftRadius_5, borderStyles.borderTopRightRadius_5, { backgroundColor: colors.yellow }]}>

                    {/* Text - Status */}
                    <Text style={[fontFamilyStyles.semiBold, fontSizeStyles.fontSize_08, paddingStyles.padding_03, marginStyles.leftMargin_02, colorStyles.whiteColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>Success</Text>
                </View>

                {/* Wrapper - Transaction Details */}
                <View style={[colorStyles.whiteBackgroundColor, { height: height, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 15 }]}>

                    {/* Wrapper - Amount */}
                    <View style={[flexDirectionStyles.row, marginStyles.topMargin_5]}>

                        {/* Text - Amount Header */}
                        <Text style={[{ width: '45%', fontSize: 18 }, fontFamilyStyles.extraBold, colorStyles.redColor]}>{Constants.text_amount}</Text>

                        {/* Text - Amount */}
                        <Text style={[{ width: '55%', textAlign: 'right', fontSize: 18 }, fontFamilyStyles.extraBold, colorStyles.redColor]}>{Constants.text_rupees}500</Text>
                    </View>

                    {/* Text - Amount Separator */}
                    <Text style={[{ height: 1, marginTop: 15, marginBottom: 15 }, colorStyles.silverBackgroundColor]} />

                    {/* Wrapper - Payment Mode */}
                    <View style={flexDirectionStyles.row}>

                        {/* Text - Payment Mode Header */}
                        <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_payment_mode}</Text>

                        {/* Text - Payment Mode */}
                        <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>Net Banking</Text>
                    </View>

                    {/* Wrapper - Order Id */}
                    <View style={[flexDirectionStyles.row, marginStyles.topMargin_25]}>

                        {/* Text - Order Id Header */}
                        <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_order_id}</Text>

                        {/* Text - Order Id */}
                        <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>1234567890</Text>
                    </View>

                    {/* Wrapper - Order Date */}
                    <View style={[flexDirectionStyles.row, marginStyles.topMargin_25]}>

                        {/* Text - Order Date Header */}
                        <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_order_date}</Text>

                        {/* Text - Order Date */}
                        <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>12 April 2021</Text>
                    </View>

                    {/* TouchableOpacity - Done Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20]} onPress={() => props.navigation.navigate(Constants.nav_account)} activeOpacity={0.5}>

                        {/* Text - Button (Done) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_done}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}
export default Temp;