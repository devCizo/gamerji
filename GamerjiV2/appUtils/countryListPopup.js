import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import colors from '../assets/colors/colors'
import { fontFamilyStyleNew } from './commonStyles';
import CustomProgressbar from './customProgressBar';
import CustomMarquee from './customMarquee';
import { showErrorToastMessage } from './commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../store/actions/index';
import { FlatList } from 'react-native-gesture-handler';
import * as generalSetting from '../webServices/generalSetting'

const CountryListPopup = (props) => {

    var countryList = props.countryList

    const selectCountry = item => {
        props.setSelectedCountry(item)
        props.openCountryListPicker(false)
    }

    return (
        <>
            <View style={{ height: 400, backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

                {/* Close Button */}
                <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                    <Text style={{ flex: 1, marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} >Select Country</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.openCountryListPicker(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, flex: 1 }}
                    data={countryList}
                    renderItem={({ item, index }) => <CountryListPopupItem item={item} selectCountry={selectCountry} />}
                    showsVerticalScrollIndicator={false}
                />

            </View>
        </>
    )
}

const CountryListPopupItem = (props) => {

    let item = props.item

    return (
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.5} onPress={() => props.selectCountry(item)} >
            <Image style={{ marginLeft: 10, height: 30, width: 30, resizeMode: 'contain' }} source={item.flag && item.flag.default && { uri: generalSetting.UPLOADED_FILE_URL + item.flag.default }} />

            <Text style={{ marginLeft: 12, color: colors.black, fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold, }} >{item.name}</Text>

            <Text style={{ marginLeft: 6, color: colors.black, fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold, }} >( {item.dialingCode} )</Text>

            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#D5D7E3' }} />

        </TouchableOpacity>
    )
}

export default CountryListPopup