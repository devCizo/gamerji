import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/profile/levelAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        levelListResponse: undefined,
    },
    error: false
};

{ /* Request - Game Types List */ }
const levelList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.levelListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const levelListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.levelListResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LEVEL_LIST:
            return levelList(state, action);
        case actionTypes.LEVEL_LIST_FAILED:
            return levelListFailed(state, action);
        case actionTypes.RESET_LEVEL_LIST_STATE:
            return resetLevelListState(state);
        default:
            return state;
    }
};

export default (reducer);

const resetLevelListState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.levelListResponse = undefined;
    })
}