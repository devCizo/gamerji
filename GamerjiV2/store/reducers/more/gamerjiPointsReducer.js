import produce from 'immer';
import * as actionTypes from '../../actions/more/gamerjiPointsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        gamerjiPointsResponse: undefined,
        gamerjiPointCategoriesResponse: undefined,
    },
    error: false
};

{ /* Request - Gamerji Points List */ }
const gamerjiPointsList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.gamerjiPointsResponse = action.model;
    });
};

{ /* Request Failure - Gamerji Points List */ }
const gamerjiPointsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gamerjiPointsResponse = action.model;
    });
};

{ /* Reset State - Gamerji Points List */ }
const resetGamerjiPointsListState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.gamerjiPointsResponse = undefined;
        draft.model.gamerjiPointCategoriesResponse = undefined;
    });
};

{ /* Request - Gamerji Points List */ }
const gamerjiPointCategoryList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.gamerjiPointCategoriesResponse = action.model;
    });
};

{ /* Request Failure - Gamerji Points List */ }
const gamerjiPointCategoryListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gamerjiPointCategoriesResponse = action.model;
    });
};

{ /* Reset State - Gamerji Points List */ }
const resetGamerjiPointCategoryListState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.gamerjiPointCategoriesResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GAMERJI_POINTS_LIST:
            return gamerjiPointsList(state, action);
        case actionTypes.GAMERJI_POINTS_LIST_FAILED:
            return gamerjiPointsListFailed(state, action);
        case actionTypes.RESET_GAMERJI_POINTS_LIST_STATE:
            return resetGamerjiPointsListState(state);
        case actionTypes.GAMERJI_POINTS_CATEGORY_LIST:
            return gamerjiPointCategoryList(state, action);
        case actionTypes.GAMERJI_POINTS_CATEGORY_LIST_FAILED:
            return gamerjiPointCategoryListFailed(state, action);
        case actionTypes.RESET_GAMERJI_POINTS_CATEGORY_LIST_STATE:
            return resetGamerjiPointCategoryListState(state);
        default:
            return state;
    }
};

export default reducer;