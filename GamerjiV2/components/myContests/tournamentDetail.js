
import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView, BackHandler, Share, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import TournamentCommonDetail from '../joinTournament/tournamentCommonDetail'
import { ProgressBar } from 'react-native-paper';
import HtmlText from 'react-native-html-to-text';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog, checkIsSponsorAdsEnabled, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import moment from 'moment';
import Modal from 'react-native-modal';
import WiningPrizePool from '../JoinContest/WiningPrizePool'
import WiningPrizePoolRound from '../JoinContest/WiningPrizePoolRound'

import ReportIssue from '../myContests/reportIssue'
import Clipboard from '@react-native-clipboard/clipboard';
import CustomMarquee from '../../appUtils/customMarquee';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { Constants } from '../../appUtils/constants';
import ImageViewer from 'react-native-image-zoom-viewer';

const TournamentDetail = (props) => {

    const dispatch = useDispatch();
    const tournamentId = props.route.params.tournamentId
    const [tournament, setTournamentDetail] = useState(undefined);
    const [activeTab, setActiveTab] = useState('details');
    const [tournamentPlayers, setTournamentPlayers] = useState([]);
    const [captainData, setCaptainData] = useState(undefined);
    const [isVisibleWinningPrizePoolPopup, setWinningPrizePoolPopup] = useState(false)
    const [isVisibleWinningPrizePoolRoundPopup, setWinningPrizePoolRoundPopup] = useState(false)
    const [providedRatings, setProvidedRatings] = useState(undefined);
    const [isVissibleReportIssuePopup, setReportIssuePopupVisibility] = useState(false);
    const [currentContest, setCurrentContest] = useState(undefined);

    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const htmlToFormattedText = require("html-to-formatted-text");

    //Initiate
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestTournamentDetail(tournamentId))

        return () => {
            dispatch(actionCreators.resetTournamentDetailState())
        }
    }, []);

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        dispatch(actionCreators.requestTournamentDetail(tournamentId))
        dispatch(actionCreators.requestTournamentPlayers({ filter: { "event": tournamentId } }))

    }, [refreshing]);

    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.tournamentDetail.model }),
        shallowEqual
    );
    var { tournamentDetailResponse, tournamentPlayersResponse } = currentState

    //TournamentDetailResponse
    useEffect(() => {
        if (tournamentDetailResponse) {
            setLoading(false)
            setRefreshing(false)
            if (tournamentDetailResponse.success == true && tournamentDetailResponse.item) {

                var tempTournament = { ...tournamentDetailResponse }

                var tempContest = []

                for (let index = 0; index < tempTournament.contests.length; index++) {
                    var tempElement = { ...tempTournament.contests[index] }
                    if (index == tempTournament.contests.length - 1) {
                        tempElement.isOpen = true
                    } else {
                        tempElement.isOpen = false
                    }
                    tempContest.push(tempElement)

                }
                tempTournament.contests = [...tempContest]
                setTournamentDetail(tempTournament)


                if (tournamentDetailResponse.isRated && tournamentDetailResponse.isRated == true && tournamentDetailResponse.tournamentRate && tournamentDetailResponse.tournamentRate[0]) {
                    setProvidedRatings(tournamentDetailResponse.tournamentRate[0] && tournamentDetailResponse.tournamentRate[0].rate)
                }
            }
            tournamentDetailResponse = undefined
        }
    }, [tournamentDetailResponse]);

    //Tournament Players Response
    useEffect(() => {
        if (tournamentPlayersResponse && tournamentPlayersResponse.list) {

            var tempPlayers = [...tournamentPlayersResponse.list]

            let index = tempPlayers.findIndex(item => {
                if (item.user && item.user._id && tournament.contests && tournament.contests.length > 0 && tournament.contests[0] && tournament.contests[0].user) {
                    if (tournament.contests[0].user._id == item.user._id) {
                        return true
                    }
                }
            })
            var contestArr = [];
            //  contestArr[0] = contestDetailResponse.captainData[0];
            var cnt = 1;
            tournamentPlayersResponse.list.forEach(element => {
                console.log(element);
                if (element._id !== tournamentPlayersResponse.captainData[0]._id) {
                    contestArr[cnt] = element;
                    cnt++;
                }
                if (element._id === tournamentPlayersResponse.captainData[0]._id) {
                    contestArr[0] = element;

                }

            });
            if (tournamentPlayersResponse.list.length === contestArr.length) {
                setTournamentPlayers(contestArr);
                setCaptainData(contestArr[0]);

            }
            // if (index != -1) {
            //     let currentUser = tempPlayers[index]
            //     tempPlayers.splice(index, 1)
            //     tempPlayers.splice(0, 0, currentUser)
            // }
            // setTournamentPlayers(tempPlayers)
        }
    }, [tournamentPlayersResponse]);

    //Tab select
    let selectTabButton = (value) => {
        setActiveTab(value)
        if (value == 'players' && tournamentPlayers.length == 0) {
            let palyload = {
                skip: 0,
                limit: 100000,
                filter: {
                    "event": tournamentId
                }
            }
            dispatch(actionCreators.requestTournamentPlayers(palyload))
        }
    }

    let closeWiningPrizePool = () => {
        setWinningPrizePoolPopup(false)
    }
    let closeWiningPrizePoolRound = () => {
        setWinningPrizePoolRoundPopup(false)
    }

    //Rate Tournament
    let provideRating = (ratings) => {
        let payload = {
            rate: ratings,
            event: tournamentId
        }
        dispatch(actionCreators.requestRateContest(payload))
        setProvidedRatings(ratings)
    }

    let getRatedData = () => {

        var rate = 1
        if (providedRatings) {
            rate = providedRatings
        } else if (tournament.isRated && tournament.isRated == true && tournament.tournamentRate && tournament.tournamentRate[0] && tournament.tournamentRate[0].rate) {
            rate = tournament.tournamentRate[0].rate
        }

        var title = ''
        if (rate == 1) {
            title = 'Very Bad'
        } else if (rate == 2) {
            title = 'Bad'
        } else if (rate == 3) {
            title = 'Average'
        } else if (rate == 4) {
            title = 'Good'
        } else if (rate == 5) {
            title = 'Very Good'
        }
        return title
    }

    //Report Issue Submitted
    let issueSubmitted = () => {
        dispatch(actionCreators.requestTournamentDetail(tournamentId))
    }
    let openWinningPrizePoolRoundPopup = (item) => {
        setCurrentContest(item)
        setWinningPrizePoolRoundPopup(true)
    }

    let getReportIssueTitle = () => {
        if (tournament && tournament.item.isReportIssue && tournament.item.isReportIssue == true) {
            if (tournament.item.reportIssue && tournament.item.reportIssue.length > 0 && tournament.item.reportIssue[0].reportIssue) {
                return tournament.item.reportIssue[0].reportIssue.issue
            }
        }
        return 'Report Issue'
    }
    const getCurrencyImage = () => {

        return (
            tournament.currency && tournament.currency.code == 'coin' ?
                <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={tournament.currency && tournament.currency.img && tournament.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.currency.img.default }} />
                :
                <Text style={{ fontSize: 10, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{tournament.currency.symbol}</Text>
        )
    }
    let openReportIssuePopup = () => {
        if (tournament && tournament.item && tournament.item.isReportIssue && tournament.item.isReportIssue == true) {
            showErrorToastMessage('You already have submitted issue')
        } else {
            setReportIssuePopupVisibility(true)
        }
    }

    const showReportIssue = () => {
        if (tournament && tournament.game && tournament.game.settings && tournament.game.settings.reportIssues && tournament.game.settings.reportIssues.length > 0) {
            return true
        }

        return false
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const shareWithFriends = () => {
        if (tournament.item && tournament.item.shortCode) {
            generateDynamicLink()
        }
    }

    const generateDynamicLink = async () => {

        addLog('tournament.item.shortCode', tournament.item.shortCode)

        const link = await dynamicLinks().buildShortLink({
            link: 'https://www.gamerji.com/contestTournamentDetail?shortCode=' + tournament.item.shortCode,
            domainUriPrefix: 'https://gamerji.page.link',
            ios: {
                bundleId: 'com.gamerji',
                appStoreId: '1466052584'
            },
            android: {
                packageName: 'com.esports.gamerjipro'
            },
            analytics: {
                campaign: 'banner',
            },
        });
        //    var link1 = 'https://www.gamerji.com/contestTournamentDetail?shortCode=' + tournament.item.shortCode;
        if (link) {
            addLog('link', link)
            Share.share({
                message: 'Hey! Join me on Gamerji for this exciting match. Click on the following link  and let\'s play together\n' + link,
                url: link,
                title: ''
            }, {
                // Android only:
                dialogTitle: '',
                // iOS only:
                excludedActivityTypes: [
                ]
            })
        }
    }

    const getTableImageUrl = () => {
        if (tournament.item && tournament.item.eventTableImage && tournament.item.eventTableImage.default) {
            return [{ url: generalSetting.UPLOADED_FILE_URL + tournament.item.eventTableImage.default }]
        }
        return []
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >TOURNAMENT DETAILS</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {tournament &&

                    <ScrollView style={{ flex: 1 }}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >

                        {tournament.item && tournament.item.bannerImage && tournament.item.bannerImage.default && tournament.item.bannerImage.default != '' ?
                            // Header Landscape Image
                            < Image style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 112, borderRadius: 10, overflow: 'hidden', resizeMode: 'cover' }} source={tournament.item && tournament.item.bannerImage && tournament.item.bannerImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.item.bannerImage.default }} />

                            : <></>
                        }

                        {/* Tabs View */}
                        <View style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 34, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 17 }} >

                            <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'details' ? colors.black : 'transparent' }} onPress={() => selectTabButton('details')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Details</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'table' ? colors.black : 'transparent' }} onPress={() => selectTabButton('table')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Table</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'players' ? colors.black : 'transparent' }} onPress={() => selectTabButton('players')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Players</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'rules' ? colors.black : 'transparent' }} onPress={() => selectTabButton('rules')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Rules</Text>
                            </TouchableOpacity>
                        </View>

                        {activeTab == 'details' &&

                            // Tournament Detail
                            <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20 }} >
                                <TournamentCommonDetail tournament={tournament.item} isFromTournamentDetail={true} currency={tournament.currency} />

                                {/* Tournament Image */}
                                <View style={{ position: 'absolute', top: 40, left: 8, width: 100, height: 130, borderRadius: 5, overflow: 'hidden' }} >

                                    <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, width: 130, height: 150 }} source={tournament.item && tournament.item.featuredImage && tournament.item.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.item.featuredImage.default }} />
                                    <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, width: 100, height: 70 }} source={require('../../assets/images/tournament_image_layer.png')} />
                                    <Text style={{ position: 'absolute', left: 0, right: 0, bottom: 6, color: 'white', textAlign: 'center', fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} >{tournament.gameType && tournament.gameType.name}</Text>

                                </View>

                                <Text style={{ color: colors.black, textAlign: 'center', fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >Tournament Rounds</Text>

                                <FlatList
                                    style={{ marginTop: 10 }}
                                    data={tournament.contests}
                                    renderItem={({ item, index }) => <TournamentDetailRoundItem captainData={captainData} tournament={tournament} index={index} openWinningPrizePoolRoundPopup={openWinningPrizePoolRoundPopup} setWinningPrizePoolRoundPopup={setWinningPrizePoolRoundPopup} setWinningPrizePoolPopup={setWinningPrizePoolPopup} />}
                                    keyExtractor={(item, index) => index.toString()}
                                />

                                {/* Share And Report */}
                                <View style={{ flexDirection: 'row', marginTop: 16, height: 68, justifyContent: 'space-between' }} >

                                    {/* Share With Friend */}
                                    <TouchableOpacity style={{ width: showReportIssue() ? '47%' : '100%', alignItems: 'center' }} onPress={() => shareWithFriends()} activeOpacity={0.9}>
                                        <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, borderRadius: 25, backgroundColor: colors.red }} />
                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/share_with_friend.png')} ></Image>
                                        <Text style={{ marginTop: 4, color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 15, fontFamily: fontFamilyStyleNew.bold }}>Share with Friends</Text>
                                    </TouchableOpacity>

                                    {/* Report Issue */}
                                    {showReportIssue() &&
                                        <TouchableOpacity style={{ width: '47%', alignItems: 'center' }} onPress={() => openReportIssuePopup()} activeOpacity={0.9} >
                                            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, borderRadius: 25, backgroundColor: colors.red }} />
                                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/report_issue.png')} ></Image>
                                            <CustomMarquee style={{ marginTop: 4, marginLeft: 4, marginRight: 4, color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} value={getReportIssueTitle()} />
                                        </TouchableOpacity>
                                    }
                                </View>

                                {/* Provide Ratings */}
                                {!providedRatings ?
                                    <View style={{ marginTop: 26, marginBottom: 40, alignItems: 'center', height: 68 }} >

                                        <CustomMarquee style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} value={`Rate ` + (tournament.item.host && tournament.item.host.name)} />

                                        <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'center' }} >

                                            <TouchableOpacity style={{ alignItems: 'center', width: 46 }} onPress={() => provideRating(1)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate1_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Very Bad</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(2)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate2_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Bad</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(3)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate3_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Average</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(4)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate4_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Good</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(5)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate5_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Very Good</Text> */}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    :
                                    <View style={{ marginTop: 26, alignSelf: 'center', alignItems: 'center' }} >
                                        <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }}>Your Provided Ratings</Text>

                                        {providedRatings == 1 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate1_icon.png')} />
                                        }
                                        {providedRatings == 2 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate2_icon.png')} />
                                        }
                                        {providedRatings == 3 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate3_icon.png')} />
                                        }
                                        {providedRatings == 4 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate4_icon.png')} />
                                        }
                                        {providedRatings == 5 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate5_icon.png')} />
                                        }
                                        {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>{getRatedData()}</Text> */}
                                    </View>
                                }

                            </View>
                        }

                        {activeTab == 'table' &&

                            <View style={{ marginTop: 14, marginLeft: 20, marginRight: 20, height: Constants.SCREEN_HEIGHT - 330 }} >
                                {/* <Image style={{ flex: 1, resizeMode: 'contain' }} source={tournament.item && tournament.item.eventTableImage && tournament.item.eventTableImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.item.eventTableImage.default }} ></Image> */}
                                <ImageViewer
                                    imageUrls={getTableImageUrl()}
                                    backgroundColor={colors.white}
                                    renderIndicator={() => null}
                                />
                            </View>
                        }

                        {activeTab === 'players' &&
                            <View style={{ flex: 1 }} >

                                {/* Header */}
                                <View style={{ marginTop: 16, marginRight: 20, marginLeft: 20, borderRadius: 25, overflow: 'hidden' }} >
                                    <TournamentPlayerHeader />
                                </View>

                                <View style={{ marginTop: 6, marginLeft: 20, marginRight: 20, borderRadius: 20, borderWidth: 1, borderColor: '#DFE4E9', overflow: 'hidden' }}>
                                    <FlatList
                                        data={tournamentPlayers}
                                        renderItem={({ item, index }) => <TournamentPlayerItem tournamentItem={tournament.item} item={item} index={index} getCurrencyImage={getCurrencyImage} />}
                                        keyExtractor={(item, index) => index.toString()}
                                    >
                                    </FlatList>
                                </View>

                                {/* <FlatList
                                    style={{ marginTop: 6, marginLeft: 20, marginRight: 20, borderRadius: 20, borderWidth: 1, borderColor: '#DFE4E9', }}
                                    data={tournamentPlayers}
                                    renderItem={({ item, index }) => <TournamentPlayerItem item={item} index={index} />}
                                    keyExtractor={(item, index) => index.toString()}
                                >
                                </FlatList> */}

                            </View>
                        }

                        {activeTab == 'rules' && (
                            <View style={{ marginTop: 16, marginLeft: 20, marginRight: 20, marginBottom: 10, flex: 1, borderRadius: 20, borderColor: '#DFE4E9', borderWidth: 1 }} >
                                {/* <HtmlText style={{ marginTop: 24, marginLeft: 18, marginRight: 18, marginBottom: 24, color: '#82878D', fontFamily: fontFamilyStyleNew.regular, fontSize: 13 }} html={tournament.item && tournament.item.rules} ></HtmlText> */}

                                <Text style={{ marginTop: 24, marginLeft: 18, marginRight: 18, marginBottom: 24, color: '#82878D', fontFamily: fontFamilyStyleNew.regular, fontSize: 13 }}>{htmlToFormattedText(tournament.item && tournament.item.rules)}</Text>
                            </View>
                        )}
                    </ScrollView>
                }

                {/* {tournament &&
                    //Open Game
                    <TouchableOpacity style={{ height: 46, marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 20, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => null} activeOpacity={0.5} >
                        <CustomMarquee style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16 }} value={`OPEN ` + (tournament.game && tournament.game.name) + ` APP`} />
                          <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>  
                    </TouchableOpacity>
                } */}

                {checkIsSponsorAdsEnabled('tournamentDetails') &&
                    <SponsorBannerAds screenCode={'tournamentDetails'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('tournamentDetails')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}

            {tournament &&
                <Modal isVisible={isVisibleWinningPrizePoolPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <WiningPrizePool contest={tournament.item} closeWiningPrizePool={closeWiningPrizePool} currency={tournament.currency} />
                </Modal>
            }

            {tournament &&
                <Modal isVisible={isVisibleWinningPrizePoolRoundPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <WiningPrizePoolRound contest={tournament.item} currentContest={currentContest} closeWiningPrizePool={closeWiningPrizePoolRound} currency={tournament.currency} />
                </Modal>
            }

            {tournament &&
                <Modal isVisible={isVissibleReportIssuePopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <ReportIssue contestTournament={tournament} isContest={true} setReportIssuePopupVisibility={setReportIssuePopupVisibility} issueSubmitted={issueSubmitted} />
                </Modal>
            }

        </SafeAreaView>
    )
}

const TournamentPlayerHeader = () => {

    return (
        <View style={{ height: 50, backgroundColor: colors.yellow, flexDirection: 'row' }} >

            {/* User name */}
            <View style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ width: 92, alignItems: 'center' }} >
                    <Image style={{ width: 18, height: 18 }} source={require('../../assets/images/username_contest_detail.png')} />
                    <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >User Name</Text>
                </View>
            </View>

            {/* Kills */}
            <View style={{ width: 64, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 18, height: 13 }} source={require('../../assets/images/kills_contest_detail.png')} />
                <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >Kills</Text>
                <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
            </View>

            {/* Rank */}
            <View style={{ right: 0, width: 64, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 18, height: 15 }} source={require('../../assets/images/rank_contest_detail.png')} />
                <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >Rank</Text>
                <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
            </View>

            {/* Points */}
            <View style={{ right: 0, width: 64, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 18, height: 15 }} source={require('../../assets/images/points_icon.png')} />
                <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >Points</Text>
                <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
            </View>
        </View>
    )
}

const TournamentDetailRoundItem = (props) => {

    const [contest, setContest] = useState(props.tournament.contests && props.tournament.contests.length > 0 ? props.tournament.contests[props.index] : undefined)
    let rounds = props.tournament.item && props.tournament.item.rounds && props.tournament.item.rounds.length > 0 ? props.tournament.item.rounds : []
    let currency = props.tournament.currency

    let getRoundName = () => {
        if (rounds.length > 0) {
            for (let i = 0; i < rounds.length; i++) {
                let round = rounds[i]

                for (let j = 0; j < round.matches.length; j++) {
                    const match = round.matches[j];

                    if (match.matchId == contest.code) {
                        return round.name
                    }
                }
            }
        }
    }

    let findMapPerspectivePer = (findType, type) => {
        if (props.tournament.item && props.tournament.item.titles) {
            let index = props.tournament.item.titles.findIndex(obj => obj.type == findType)
            if (index < 0) {
                return undefined
            }
            if (type == 'title') { return props.tournament.item.titles[index].name }
            if (type == 'value') { return props.tournament.item.titles[index].value }
            return true
        }
    }

    let getRoundStatus = () => {

        if (contest.isFinalRound) {
            return undefined
        }

        if (contest.status != 'completed') {
            return 'pending'
        }
        if (props.tournament.contests.length > props.index) {
            return 'qualified'
        }
        return 'notQualified'


    }
    let getRoundStatusDesc = () => {

        if (contest.isFinalRound) {
            return undefined
        }

        if (getRoundStatus() == 'pending') {
            return 'Pending'
        }
        if (getRoundStatus() == 'qualified') {
            return 'Qualified for Next Round'
        }
        return 'Sorry! You are not Qualified'
    }

    let getProgressValue = () => {
        if (contest.totalJoinedPlayers && contest.userCount) {
            return contest.totalJoinedPlayers / contest.userCount
        } else {
            return 0
        }
    }

    let totalRemainingPlayers = () => {
        let players = contest.userCount - contest.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' remaining'
    }

    let totalJoinedPlayersPlayers = () => {
        let players = contest.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' joined'
    }

    const isRoomIdAvailable = () => {
        if (contest.roomId && contest.roomId != '') {
            return true
        }
        return false
    }

    const isPasswordAvailable = () => {
        if (contest.roomPassword && contest.roomPassword != '') {
            return true
        }
        return false
    }

    const copyRoomIdToClipboard = () => {
        if (isRoomIdAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contest.roomId)
        }
    }

    const copyPasswordToClipboard = () => {
        if (isPasswordAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contest.roomPassword)
        }
    }

    const getCurrencyImage = () => {

        return (
            currency && currency.code == 'coin' ?
                <Image style={{ height: 12, width: 12, resizeMode: 'contain' }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                :
                <Text style={{ fontSize: 12, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{currency.symbol}</Text>
        )
    }

    const openCloseRoundSectionPressed = () => {
        var tempContest = { ...contest }
        tempContest.isOpen = !tempContest.isOpen
        setContest(tempContest)
    }

    const displayColumn = () => {
        if (contest.contestUserItem.column && contest.contestUserItem.column != '') {
            return contest.contestUserItem.column
        }
    }

    const getRoomIdPasswordWidth = () => {
        if (displayColumn()) {
            return '38%'
        }
        return '50%'
    }

    return (
        <View>
            <View style={{ flex: 1, alignSelf: 'stretch', marginBottom: contest.isOpen ? 31 : 10 }}>

                {/* Title View */}
                <TouchableOpacity style={{ height: 40, backgroundColor: colors.black, borderTopLeftRadius: 20, borderTopRightRadius: 20, flexDirection: 'row', justifyContent: 'flex-end', borderBottomLeftRadius: contest.isOpen ? 0 : 20, borderBottomRightRadius: contest.isOpen ? 0 : 20 }} onPress={() => openCloseRoundSectionPressed()} activeOpacity={1} >

                    <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginLeft: 8, flex: 1, alignSelf: 'center', color: colors.white }} numberOfLines={1} ellipsizeMode='tail' >{getRoundName()}</Text>

                    <View style={{ height: 24, backgroundColor: colors.black, marginRight: 0, flexDirection: 'row', borderTopRightRadius: 12, borderBottomLeftRadius: 12, borderBottomRightRadius: 12, alignItems: 'center', justifyContent: 'center' }}>

                        {getRoundStatus() == 'pending' &&
                            <Image style={{ marginLeft: 14, width: 14, height: 14 }} source={require('../../assets/images/round_pending.png')} />
                        }
                        {getRoundStatus() == 'qualified' &&
                            <Image style={{ marginLeft: 14, width: 14, height: 14 }} source={require('../../assets/images/round_qualified.png')} />
                        }
                        {getRoundStatus() == 'notQualified' &&
                            <Image style={{ marginLeft: 14, width: 14, height: 14 }} source={require('../../assets/images/round_not_qualified.png')} />
                        }
                        <Text style={{ marginLeft: 6, color: colors.white, textAlign: 'center', fontSize: 10, fontFamily: fontFamilyStyleNew.semiBold }} >{getRoundStatusDesc()}</Text>

                        {contest.isOpen ?
                            <Image style={{ marginLeft: 16, marginRight: 8, width: 13, height: 13 }} source={require('../../assets/images/round_right.png')} />
                            :
                            <Image style={{ marginLeft: 16, marginRight: 8, width: 13, height: 13 }} source={require('../../assets/images/round_down.png')} />
                        }

                    </View>
                </TouchableOpacity>

                {/* Date Time View */}
                {contest.isOpen &&
                    <>
                        <View style={{ height: 40, backgroundColor: colors.yellow, flexDirection: 'row' }} >

                            {/* Date */}
                            <View style={{ height: 40, width: '50%', justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 10, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >Date</Text>
                                <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{moment(contest.date).format('DD/MM/yyyy')}</Text>
                            </View>

                            {/* Time */}
                            <View style={{ height: 40, width: '50%', justifyContent: 'center' }}>
                                <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 8, bottom: 8, left: 0, width: 1 }} />
                                <Text style={{ marginLeft: 10, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >Time</Text>
                                <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{moment(contest.time).format('hh:mm A')}</Text>
                            </View>

                            {/* Map */}
                            {/* <View style={{ height: 40, width: '25%', justifyContent: 'center' }}>
                        <Text style={{ marginLeft: 10, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findMapPerspectivePer('maps', 'title')}</Text>
                        <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findMapPerspectivePer('maps', 'value')}</Text>
                        <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 8, bottom: 8, right: 0, width: 1 }} />
                    </View> */}

                            {/* Perspective */}
                            {/* <View style={{ height: 40, width: '25%', justifyContent: 'center' }}>
                        <Text style={{ marginLeft: 10, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findMapPerspectivePer('perspectives', 'title')}</Text>
                        <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findMapPerspectivePer('perspectives', 'value')}</Text>
                    </View> */}
                        </View>

                        {/* Winning Winner Main View */}
                        <View style={{ backgroundColor: colors.black, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, }}>

                            {/* Winning Winner Sub View */}
                            <View style={{ height: 40, flexDirection: 'row', justifyContent: 'space-between' }} >

                                {/* Winning */}
                                <View style={{ height: 40, width: '33.33%', justifyContent: 'center' }}>

                                    <Text style={{ marginLeft: 10, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Prize Pool</Text>
                                    <View style={{ marginTop: 2, marginLeft: 10, flexDirection: 'row', alignItems: 'center' }} >
                                        {getCurrencyImage()}
                                        <Text style={{ marginLeft: 2, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{props.tournament.item.prizePool}</Text>
                                    </View>

                                    <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                                </View>

                                {/* Qualification */}
                                {contest.isFinalRound === true ?
                                    <TouchableOpacity style={{ height: 40, width: '33.33%', justifyContent: 'center' }} onPress={() => props.setWinningPrizePoolPopup(true)} activeOpacity={0.5} >

                                        <Text style={{ color: colors.white, position: 'absolute', fontSize: 10, top: 7, left: 10, fontFamily: fontFamilyStyleNew.regular }}>Winners</Text>

                                        <View style={{ position: 'absolute', flexDirection: 'row', fontSize: 10, alignItems: 'center', fontWeight: 'bold', top: 19, left: 10 }}>
                                            <Text style={{ color: colors.white, fontSize: 10, fontWeight: 'bold', fontFamily: fontFamilyStyleNew.regular }}>{props.tournament.item && props.tournament.item.totalWinners}</Text>
                                            <Image style={{ marginLeft: 3, width: 5, height: 8, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                                        </View>

                                        <Text style={{ position: 'absolute', backgroundColor: '#DEAB03', top: 8, bottom: 8, right: 0, width: 1 }} />
                                    </TouchableOpacity>
                                    : <TouchableOpacity style={{ height: 40, width: '33.33%', justifyContent: 'center' }} onPress={() => props.openWinningPrizePoolRoundPopup(contest)} activeOpacity={0.5} >

                                        <Text style={{ marginLeft: 10, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Qualification</Text>

                                        <View style={{ marginLeft: 10, flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{ fontSize: 11, marginTop: 1, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.winnerCount}</Text>
                                            <Image style={{ marginLeft: 3, width: 7, height: 10, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                                        </View>

                                        <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                                    </TouchableOpacity>

                                }
                                {/* <Text style={{ color: colors.white, position: 'absolute', fontSize: 10, top: 7, left: 10, fontFamily: fontFamilyStyleNew.regular }}>Winners</Text>

                            <View style={{ position: 'absolute', flexDirection: 'row', fontSize: 10, alignItems: 'center', fontWeight: 'bold', top: 19, left: 10 }}>
                                <Text style={{ color: colors.white, fontSize: 10, fontWeight: 'bold', fontFamily: fontFamilyStyleNew.regular }}>{props.tournament.item && props.tournament.item.totalWinners}</Text>
                                <Image style={{ marginLeft: 3, width: 5, height: 8, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                            </View>

                            <Text style={{ position: 'absolute', backgroundColor: '#DEAB03', top: 8, bottom: 8, right: 0, width: 1 }} /> */}


                                {/* Per Kill */}
                                {/* <View style={{ height: 40, width: '25%', justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 10, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Per Kill</Text>
                            <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >₹5</Text>
                            <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                        </View> */}

                                {/* Id */}
                                <View style={{ height: 40, width: '33.33%', justifyContent: 'center' }}>
                                    <Text style={{ marginLeft: 10, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >ID</Text>
                                    <Text style={{ marginLeft: 10, marginTop: 2, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{props.tournament.item && props.tournament.item.shortCode}</Text>
                                </View>
                            </View>

                            {/* Spots Progress Indicator */}
                            <View style={{ marginTop: 5, marginLeft: 11, marginRight: 11, height: 5, flex: 1 }} >
                                <ProgressBar style={{ height: 5, width: '100%', backgroundColor: 'white', borderRadius: 2.5 }} progress={getProgressValue()} color={colors.yellow} />
                            </View>

                            {/* Spots Available View */}
                            <View style={{ marginTop: 5, marginLeft: 11, marginRight: 11, marginBottom: 30, justifyContent: 'space-between', flexDirection: 'row' }} >
                                {/* Spots Available */}
                                <Text style={{ color: colors.white, fontSize: 10, marginBottom: 0, fontFamily: fontFamilyStyleNew.regular }} >{totalRemainingPlayers()}</Text>

                                {/* Spots Available Left */}
                                <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalJoinedPlayersPlayers()}</Text>
                            </View>
                        </View>
                    </>
                }
            </View>

            {/* Bottom View */}
            {contest.isOpen &&
                <View style={{ position: 'absolute', height: 44, left: 18, right: 18, bottom: 127, backgroundColor: colors.red, borderTopLeftRadius: 5, borderTopRightRadius: 5, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, justifyContent: 'space-between', flexDirection: 'row' }} >

                    {/* Room ID */}
                    <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} onPress={() => copyRoomIdToClipboard()} activeOpacity={0.8} >
                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Room ID</Text>

                        <View style={{ marginTop: 2, flexDirection: 'row' }} >
                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{isRoomIdAvailable() ? contest.roomId : '-'}</Text>
                            {isRoomIdAvailable() &&
                                <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                            }
                        </View>
                    </TouchableOpacity>

                    {/* Password */}
                    <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} onPress={() => copyPasswordToClipboard()} activeOpacity={0.8} >
                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Password</Text>

                        <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{isPasswordAvailable() ? contest.roomPassword : '-'}</Text>
                            {isPasswordAvailable() &&
                                <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                            }
                        </View>

                        <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, bottom: 8, left: 0, width: 1 }} />
                    </TouchableOpacity>

                    {/* Slot */}
                    {contest.contestUserItem && contest.contestUserItem.column ?
                        <View style={{ flex: 1, justifyContent: 'center' }} >

                            <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, bottom: 8, left: 0, width: 1 }} />

                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Slot</Text>

                            <Text style={{ marginTop: 2, marginLeft: 10, height: 14, width: 30, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold, backgroundColor: colors.black, borderRadius: 3, textAlign: 'center', overflow: 'hidden' }}>{contest.contestUserItem.column}</Text>

                        </View>
                        : <View></View>}
                </View>
            }

            {contest.isOpen &&
                // Current Player
                contest.contestUserItem &&
                <View style={{ borderRadius: 10, marginBottom: 20, overflow: 'hidden' }} >
                    {/* Current Player Header */}
                    <TournamentPlayerHeader />

                    {/* Current Player Detail */}
                    <TournamentPlayerItem item={contest.contestUserItem} tournamentItem={props.tournament.item} getCurrencyImage={getCurrencyImage} index={0} />
                </View>
            }
        </View>
    )
}

const TournamentPlayerItem = (props) => {

    const player = props.item

    const openPlayerDetails = () => {
        if (player.user && player.user._id) {
            RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: player.user._id })
        }
    }

    if (props.index == 0) {
        return (
            <TouchableOpacity style={{ height: 50, flexDirection: 'row', backgroundColor: colors.black, alignItems: 'center' }} activeOpacity={0.9} onPress={() => openPlayerDetails()} >

                {/* Badge */}
                {/* <Image style={{ marginLeft: 8, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
                {player.level && player.level.featuredImage ?
                    <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                    :
                    <Text></Text>

                }

                <View style={{ marginLeft: 8, flex: 1 }}>

                    {/* Username */}
                    <View style={{}}>
                        <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} value={player.uniqueIGN} />
                    </View>
                    {/* Mobile number */}
                    <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: 'white' }} >XXXXX {player.user && player.user.phone && player.user.phone.substr(player.user.phone.length - 5)}</Text>

                    {props.tournamentItem.status == "completed" ?
                        player.amount > 0 ?
                            <View style={{}}>
                                <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: 'white' }} > <Image style={{ tintColor: colors.yellow, marginLeft: 8, width: 10, height: 10 }} source={require('../../assets/images/ic_trophy.png')} /> Won {props.getCurrencyImage()} {player.amount}</Text>

                            </View> : <View></View>
                        : <View></View>}
                </View>

                {/* Kills */}
                <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} >{!player.kills ? '-' : (player.kills == '' ? '-' : player.kills)}</Text>

                {/* Ranks */}
                <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} >{!player.rank ? '-' : (player.rank == '' ? '-' : player.rank)}</Text>

                {/* Points */}
                <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} >{!player.points ? '-' : (player.points == '' ? '-' : player.points)}</Text>

                {/* Separator line */}
                {/* <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} /> */}

            </TouchableOpacity>
        );
    }

    return (

        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.9} onPress={() => openPlayerDetails()} >

            {/* Badge */}
            {/* <Image style={{ marginLeft: 8, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
            {player.level && player.level.featuredImage ?
                <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                :
                <Text></Text>

            }

            <View style={{ marginLeft: 8, flex: 1 }}>

                {/* Username */}
                <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} value={player.uniqueIGN} />
                {/* Mobile number */}
                <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.black }} >xxxxx {player.user && player.user.phone && player.user.phone.substr(player.user.phone.length - 5)}</Text>

                {props.tournamentItem.status == "completed" ?
                    player.amount > 0 ?
                        <View style={{}}>

                            <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: colors.black }} >
                                <Image style={{ tintColor: colors.yellow, marginLeft: 8, width: 10, height: 10 }} source={require('../../assets/images/ic_trophy.png')} /> Won {props.getCurrencyImage()} {player.amount}
                            </Text>

                        </View> : <View></View>
                    : <View></View>}
            </View>

            {/* Kills */}
            <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{!player.kills ? '-' : (player.kills == '' ? '-' : player.kills)}</Text>

            {/* Ranks */}
            <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{!player.rank ? '-' : (player.rank == '' ? '-' : player.rank)}</Text>

            {/* Points */}
            <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{!player.points ? '-' : (player.points == '' ? '-' : player.points)}</Text>

            {/* Separator line */}
            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} />

        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
})

export default TournamentDetail;