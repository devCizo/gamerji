import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Keyboard, TextInput } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { showSuccessToastMessage, showErrorToastMessage, addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { shallowEqual, connect, useDispatch, useSelector } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import Modal from 'react-native-modal';
import SingleTournamentToJoin from '../joinTournament/singleTournamentToJoin';
import SingleContestToJoin from '../JoinContest/singleContestToJoin';

const BottomSheetJoinViaInviteCode = (props) => {

    // Variable Declarations
    const [inviteCode, setInviteCode] = useState('');
    const [isLoading, setLoading] = useState(false);

    const dispatch = useDispatch();
    const [isOpenJoinViaInviteCode, setJoinViaInviteCodeView] = useState(false)


    const [contestTournamentDetail, setContestTournamentDetail] = useState(undefined)
    const [isOpenSingleTournamentPopup, setOpenSingleTournamentPopup] = useState(false)
    const [isOpenSingleContestPopup, setOpenSingleContestPopup] = useState(false)

    //Open Close InputView
    let openCloseJoinViaInviteCodeView = () => {
        setJoinViaInviteCodeView(!isOpenJoinViaInviteCode)
        if (!isOpenJoinViaInviteCode) {
            setInviteCode('')
        }
    }

    //Apply Invite Code
    let applyInviteCode = () => {

        if (inviteCode == '') {
            showErrorToastMessage('Please enter invite code')
            return
        }

        let payload = {
            code: inviteCode
        }
        setLoading(true)
        dispatch(actionCreators.requestJoinViaInviteCode(payload))
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.joinViaInviteCode.model }),
        shallowEqual
    );
    var { joinViaInviteCodeResponse } = currentState

    //JoinViaInviteCode Response
    useEffect(() => {
        if (joinViaInviteCodeResponse) {
            setLoading(false)
            if (joinViaInviteCodeResponse.item) {

                if (joinViaInviteCodeResponse.item.isSingle && joinViaInviteCodeResponse.item.isSingle == true) {
                    setOpenSingleContestPopup(true)
                    setContestTournamentDetail(joinViaInviteCodeResponse.item)
                    // RootNavigation.navigate(Constants.nav_single_contest_to_join, { contest: joinViaInviteCodeResponse.item })
                } else {
                    setOpenSingleTournamentPopup(true)
                    setContestTournamentDetail(joinViaInviteCodeResponse.item)
                    // RootNavigation.navigate(Constants.nav_single_tournament_to_join, { tournament: joinViaInviteCodeResponse.item })
                }
            } else {
                if (joinViaInviteCodeResponse.errors && joinViaInviteCodeResponse.errors[0] && joinViaInviteCodeResponse.errors[0].msg) {
                    showErrorToastMessage(joinViaInviteCodeResponse.errors[0].msg)
                }
            }
            joinViaInviteCodeResponse = undefined
        }
    }, [joinViaInviteCodeResponse]);

    return (
        <>
            {/* Main Bottom Container */}
            <View style={{ borderTopStartRadius: 50, borderTopEndRadius: 50, overflow: 'hidden' }}>

                {/* Top curved header */}
                <View style={{ height: 65, backgroundColor: '#FFC609', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, marginLeft: 50, marginRight: 50, textAlign: 'center' }}>{Constants.header_join_via_invite_code}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() =>
                        props.openJoinViaInviteCodePopup()} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                <View style={styles.bottomContainerView}>

                    <View style={styles.newUserNameContainer}>

                        <Text style={{ left: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}>{Constants.text_invite_code}</Text>

                        <TextInput
                            style={[styles.textInputs]}
                            placeholder={Constants.validation_invite_code}
                            onChangeText={text => setInviteCode(text)}
                            spellCheck={false}
                            autoCorrect={false}
                            autoCapitalize={'characters'}>
                        </TextInput>
                    </View>

                    {/* TouchableOpacity - Submit Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_40, { left: 20, right: 20 }]} activeOpacity={0.5} onPress={() => applyInviteCode()} >

                        {/* Text - Button (Submit) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>
                {isOpenSingleTournamentPopup &&
                    <Modal isVisible={true}
                        coverScreen={true}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <SingleTournamentToJoin tournamentDetail={contestTournamentDetail} setOpenSingleTournamentPopup={setOpenSingleTournamentPopup} />
                    </Modal>
                }

                {isOpenSingleContestPopup &&
                    <Modal isVisible={true}
                        coverScreen={true}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <SingleContestToJoin contestDetail={contestTournamentDetail} setOpenSingleContestPopup={setOpenSingleContestPopup} />
                    </Modal>
                }
            </View>
            {/* {isLoading && <CustomProgressbar />} */}

            {checkIsSponsorAdsEnabled('joinViaInviteCodePopup') &&
                <SponsorBannerAds screenCode={'joinViaInviteCodePopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('joinViaInviteCodePopup')} />
            }
        </>
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        // height: 280,
    },
    topRoundCorve: {
        height: 65,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        backgroundColor: '#FFC609',
        left: 0,
        right: 0,
        position: 'relative',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeButton: {
        height: 65,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 0
    },
    bottomContainerView: {
        backgroundColor: 'white',
        height: 250,
        flexDirection: 'column',
    },
    textInputs: {
        position: 'absolute',
        height: 46,
        borderRadius: 23,
        borderWidth: 1,
        borderColor: '#082240',
        top: 24,
        left: 20,
        right: 20,
        paddingHorizontal: 15,
        fontWeight: '600',
        fontFamily: fontFamilyStyleNew.regular
    },
    newUserNameContainer: {
        top: 36,
        height: 70,
    },
})

{ /* State - Map State To Props */ }
function mapStateToProps(state) {
    return {
        list: state.more.model,
        isSucess: state.more.sucess,
        isError: state.more.error,
        errorMessage: state.more.errorMessage,
    };
}

{ /* Dispatch - Map Dispatch To Props */ }
const mapDispatchToProps = dispatch => ({
    requestApplyPromoCode: data => dispatch(actionCreators.requestApplyPromoCode(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(BottomSheetJoinViaInviteCode);