import produce from 'immer';
import * as actionTypes from '../../actions/more/addVideosAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        addVideosResponse: undefined
    },
    error: false
};

{ /* Request - Add Videos */ }
const addVideosRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.addVideosResponse = action.model;
    });
};

{ /* Request Failure - Add Videos */ }
const addVideosRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.addVideosResponse = action.model;
    });
};

{ /* Reset State - Add Videos */ }
const resetAddVideosState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.addVideosResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_ADD_VIDEOS:
            return addVideosRequest(state, action);
        case actionTypes.REQUEST_ADD_VIDEOS_FAILED:
            return addVideosRequestFailed(state, action);
        case actionTypes.RESET_ADD_VIDEOS_STATE:
            return resetAddVideosState(state);
        default:
            return state;
    }
};

export default reducer;