import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import moment from 'moment';
import { AppConstants } from '../../appUtils/appConstants';
import { addLog, getUpiPattern, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import { FlatList } from 'react-native-gesture-handler';

const WalletPaymentPopup = (props) => {

    const walletList = props.walletList

    const selectWallet = item => {

        var params = {
            paymentType: AppConstants.PAYMENT_TYPE_WALLET,
            bankId: item.code
        }

        props.setOpenWalletListPopup(false)
        props.walletSelectedFromPopup(params)
    }

    return (
        <>
            <View style={{ borderTopStartRadius: 50, borderTopEndRadius: 50, backgroundColor: colors.yellow, height: 400 }} >

                {/* Header View */}
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    {/* Title */}
                    <Text style={{ marginLeft: 60, flex: 1, color: colors.black, fontSize: 22, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Select Wallet</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }} onPress={() => props.setOpenWalletListPopup(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, flex: 1 }}
                    data={walletList}
                    renderItem={({ item, index }) => <WalletListPopupItem item={item} selectWallet={selectWallet} />}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        </>
    )
}

const WalletListPopupItem = (props) => {

    let item = props.item

    return (
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.5} onPress={() => props.selectWallet(item)} >

            {/* Icon */}
            <Image style={{ marginLeft: 10, height: 30, width: 30, resizeMode: 'contain' }} source={item.icon && item.icon.default && { uri: generalSetting.UPLOADED_FILE_URL + item.icon.default }} />

            {/* Name */}
            <Text style={{ marginLeft: 12, color: colors.black, fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold, }} >{item.name}</Text>

            {/* Seprarator Line */}
            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#E5B326' }} />

        </TouchableOpacity>
    )
}

export default WalletPaymentPopup