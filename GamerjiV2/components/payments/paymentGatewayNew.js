import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { Constants } from '../../appUtils/constants';
import { addLog, getPaymentTimeStamp } from '../../appUtils/commonUtlis';
import { AppConstants, cashFreeAPIMode, cashFreeClientId } from '../../appUtils/appConstants';
import RNPgReactNativeSdk from 'react-native-pg-react-native-sdk';
import * as generalSetting from '../../webServices/generalSetting'
import WebFields from '../../webServices/webFields.json';
import moment from 'moment';

const PaymentGatewayNew = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [uniqueOrderId, setUniqueOrderId] = useState('');
    const [isLoading, setLoading] = useState(false)

    const paramsData = props.route.params.params;
    const coinStore = paramsData.coinStore
    const avatarId = paramsData.avatarId

    // Init
    useEffect(() => {

        const uniqueId = getPaymentTimeStamp();
        setUniqueOrderId(uniqueId);

        if (uniqueId != "") {
            let payload = {
                orderId: uniqueId,
                orderAmount: paramsData.amount,
                orderCurrency: AppConstants.CURRENCY_INR
            }
            setLoading(true)
            dispatch(actionCreators.requestGeneratePaymentToken(payload))
        }

        return () => {
            dispatch(actionCreators.resetPaymentGatewayState())
        }
    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.paymentGateway.model }),
        shallowEqual
    );
    var { generateTokenResponse, createTransactionsResponse } = currentState

    // Generate Token Response
    useEffect(() => {
        if (generateTokenResponse) {
            setLoading(false)

            if (generateTokenResponse.cftoken && generateTokenResponse.cftoken != '') {
                var payload = {
                    user: global.profile._id,
                    transactionUniqueID: uniqueOrderId,
                    amount: paramsData.amount,
                    paymentType: paramsData.paymentTypeForCreateTransaction
                }
                if (coinStore) {
                    payload['coinStore'] = coinStore.coinStoreId
                    payload['currency'] = coinStore.currencyId
                }
                if (avatarId) {
                    payload['avatar'] = avatarId
                }

                addLog('payload', payload)

                setLoading(true)
                dispatch(actionCreators.requestCreateTransactions(payload))
            } else {
                RootNavigation.goBack()
            }
        }
    }, [generateTokenResponse])

    // Create Transactions Response
    useEffect(() => {
        if (createTransactionsResponse) {
            setLoading(false)
            if (createTransactionsResponse._id) {
                moveToPaymentGatewayScreen()
            } else {
                props.route.params.paymentStatusCallBack({ orderAmount: paramsData.amount, txStatus: 'FAILED', txTime: moment(new Date()).format('DD/MM/yyyy hh:mm A') })
                RootNavigation.goBack()
            }
        }
    }, [createTransactionsResponse])

    const moveToPaymentGatewayScreen = () => {

        let params = generatePaymentParams()

        RNPgReactNativeSdk.startPaymentWEB(params, cashFreeAPIMode(), (result) => {
            const response = JSON.parse(result);
            addLog('Response:::>', response)

            var paymentGatewayResult = { ...response }
            paymentGatewayResult['paymentType'] = paramsData.paymentType
            paymentGatewayResult['transactionId'] = createTransactionsResponse._id
            addLog('paymentGatewayResult', paymentGatewayResult)

            props.route.params.paymentStatusCallBack(paymentGatewayResult)
            RootNavigation.goBack()
        });
    }

    const generatePaymentParams = () => {
        var params = {
            "paymentOption": paramsData.paymentType,
            "orderId": uniqueOrderId,
            "orderAmount": paramsData.amount,
            "appId": cashFreeClientId(),
            "tokenData": generateTokenResponse.cftoken,
            "orderCurrency": AppConstants.CURRENCY_INR,
            "orderNote": Constants.text_recharge,
            "notifyUrl": generalSetting.API_URL + WebFields.VERIFY_PAYMENT.MODE,
            "customerName": global.profile && global.profile.name,
            "customerPhone": global.profile && global.profile.phone,
            "customerEmail": global.profile && global.profile.email
        }

        if (paramsData.paymentType === AppConstants.PAYMENT_TYPE_NET_BANKING || paramsData.paymentType === AppConstants.PAYMENT_TYPE_WALLET) {
            params.paymentCode = paramsData.bankId;
        } else if (paramsData.paymentType === AppConstants.PAYMENT_TYPE_UPI) {
            params.upi_vpa = paramsData.upiAddress;
        } else {
            params.card_number = paramsData.cardNumber;
            params.card_expiryMonth = paramsData.cardExpiryMonth;
            params.card_expiryYear = paramsData.cardExpiryYear;
            params.card_cvv = paramsData.cardCVV;
            params.card_holder = paramsData.cardHolderName;
            params.card_save = paramsData.card_save
        }
        addLog("Payment Request Params :::> ", params);

        return params
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

export default PaymentGatewayNew