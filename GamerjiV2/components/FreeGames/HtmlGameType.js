import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, Dimensions, Linking } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, shadowStyle, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, checkIsSponsorAdsEnabled, convertNumberToMillions } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import ChangeUserName from '../JoinContest/ChangeUserName'
import Modal from 'react-native-modal';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';
import LinearGradient from 'react-native-linear-gradient';

const HtmlGameType = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    var [html5Categories, setHtml5Categories] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [isVissibleChangeUserNamePopup, setChangeUserNamePopup] = useState(false);
    const game = props.route.params.game;
    const width = (Dimensions.get('window').width - 40);

    // This Use-Effect function should call the Game Types API
    useEffect(() => {

        addLog("game==>", game);
        if (game && game._id) {
            let payload = {
                //game: game._id,
                sortby: 'order', sort: 'asc'
            }
            setLoading(true)
            addLog("payload==>", payload);
            dispatch(actionCreators.requestHtml5Categories(payload))
        }

        return () => {
            dispatch(actionCreators.resetHtml5CategoriesState())
        }
    }, []);

    //  API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.html5Categorie.model }),
        shallowEqual
    );
    var { html5CategoriesResponse } = currentState

    // This Use-Effect function should set the Game Types Data
    useEffect(() => {
        addLog("useEffect == html5CategoriesResponse==> ", html5CategoriesResponse)
        if (html5CategoriesResponse) {
            setLoading(false)
            var tempGameTypes = []
            if (game && game.isTournament && game.isTournament == true) {
                tempGameTypes.push({ 'isTournament': true, 'name': 'TOURNAMENT', 'featuredImage': game.tournamentFeaturedImage })
            }
            if (html5CategoriesResponse.list) {
                tempGameTypes.push(...html5CategoriesResponse.list)
            }
            setHtml5Categories(tempGameTypes)
        }

        html5CategoriesResponse = undefined
    }, [html5CategoriesResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let openChangeUserNamePopup = () => {
        addLog('openChangeUserNamePopup')
        setChangeUserNamePopup(true)
    }

    let closeUserNamePopup = () => {
        setChangeUserNamePopup(false)
    }

    let moveToTournamentList = () => {
        RootNavigation.navigate(Constants.nav_tournament_list, { game });
        // RootNavigation.navigate(Constants.nav_chart)
    }

    let openApp = () => {
        Linking.canOpenURL('market://details?id=com.pubg.imobile').then(supported => {
            if (supported) {
                console.log('accepted')
                return Linking.openURL('market://details?id=com.pubg.imobile')
            } else {
                console.log('an error occured')
            }
        }).catch(
            console.log('an error occured')
        )
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - HTML Game Name Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_05]} numberOfLines={1} ellipsizeMode='tail'> {game && game.name} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}

                    <ScrollView style={[scrollViewStyles.wrapperFullScrollView, positionStyles.relative, { marginBottom: 10 }]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - How to Join? */}


                        {/* Wrapper - HTML Game Types Items */}
                        <View style={[marginStyles.topMargin_03, { marginLeft: 5, marginRight: 5 }]}>
                            <FlatList
                                data={html5Categories}
                                renderItem={({ item }) => <HtmlGameTypesItem item={item} navigation={props.navigation} moveToTournamentList={moveToTournamentList} />}
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={2}
                            // scrollEnabled={false}
                            />
                        </View>


                    </ScrollView>

                    <View style={{ width: '100%' }}>



                        {checkIsSponsorAdsEnabled('htmlCategories') &&
                            <SponsorBannerAds screenCode={'htmlCategories'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('htmlCategories')} />
                        }
                    </View>
                </View>
            </View>

            {/* <Modal isVisible={isVissibleChangeUserNamePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            // onSwipeComplete={setJoinContestWalletPopup()}
            >
                <ChangeUserName game={game} closeUserNamePopup={closeUserNamePopup} />
            </Modal> */}

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

const HtmlGameTypesItem = (props) => {

    const gameType = props.item

    addLog("HtmlGameTypesItem==>", gameType);

    const redirectToGameList = () => {

        props.navigation.navigate(Constants.nav_html_game, { gameType });
    }

    return (

        <TouchableOpacity style={{ width: (Dimensions.get('window').width - 10) / 2, height: ((Dimensions.get('window').width - 30) / 2) }} onPress={redirectToGameList} activeOpacity={0.5} >

            <View style={[{ flex: 1, marginTop: 3, marginLeft: 3, marginRight: 3, marginBottom: 3, height: ((Dimensions.get('window').width - 30) / 2), borderRadius: 10, backgroundColor: colors.black, borderColor: colors.color_input_text_border, borderWidth: 0.5, alignItems: 'center' }, shadowStyle.shadow]}>

                <Text style={{ marginTop: 7, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} ellipsizeMode='tail'> {gameType.name} </Text>

                {/* <View style={{ width: '100%', marginTop: 7, alignItems: 'center', backgroundColor: 'red' }}> */}
                {/* <CustomMarquee value={gameType.name + gameType.name} style={[fontFamilyStyles.bold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_13, marginStyles.leftMargin_5, marginStyles.rightMargin_5, { marginTop: 3 }]} /> */}
                {/* <CustomMarquee style={{ marginLeft: 2, marginRight: 2, fontSize: 16, color: colors.white, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'green' }} value={gameType.name + gameType.name} /> */}
                {/* <CustomMarquee style={{ marginLeft: 2, marginRight: 2, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red' }} value={gameType.name + gameType.name} /> */}
                {/* </View> */}

                {/* <View style={{ marginTop: 7, backgroundColor: 'green', width: '100%', }} >
                    <CustomMarquee style={{ marginLeft: 2, marginRight: 2, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red' }} value={gameType.name + gameType.name} />
                    <CustomMarquee style={{ marginLeft: 2, marginRight: 2, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red', textAlign: 'center' }} value={gameType.name + gameType.name} />
                </View> */}



                <Image style={{ position: 'absolute', top: 32, left: 3, right: 3, bottom: 3, borderRadius: 10 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + gameType.featuredImage.default }} />
                {/* Linear Gradient - Shadow  */}
                <LinearGradient colors={['#00000000', '#000000']} style={[positionStyles.absolute, heightStyles.height_90, widthStyles.width_100p, borderStyles.borderRadius_10, resizeModeStyles.cover, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, otherStyles.zIndex_1]} >

                    <View style={{ position: 'absolute', bottom: 10, flexDirection: 'row', left: 12, alignItems: 'center' }}>
                        <Image style={{ height: 16, width: 16, resizeMode: 'contain', alignSelf: 'center' }} source={require('../../assets/images/ic_joystick_round.png')} />

                        <View style={{ marginLeft: 4, flexDirection: 'row', alignSelf: 'center' }}>

                            <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, fontSize: 14 }} numberOfLines={1} ellipsizeMode='tail'>{convertNumberToMillions(gameType.totalJoinedPlayers)}</Text>

                            <Text style={{ marginLeft: 2, fontFamily: fontFamilyStyleNew.regular, color: colors.white, fontSize: 14, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_played}</Text>
                        </View>
                    </View>

                </LinearGradient>
            </View>
        </TouchableOpacity>


    )
}

export default HtmlGameType;