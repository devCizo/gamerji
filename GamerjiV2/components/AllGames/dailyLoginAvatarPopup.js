import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, Dimensions } from 'react-native'
import colors from '../../assets/colors/colors'
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles'
import moment from 'moment';
import { Constants } from '../../appUtils/constants'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds'
import * as generalSetting from '../../webServices/generalSetting'

const DailyLoginAvatarPopup = (props) => {

    const reward = props.reward

    const claimReward = () => {
        props.claimReward()
        props.setOpenDailyLoginAvatarPopup(false)
    }

    return (
        /* Main View */
        <View style={{ backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

            {/* Close Button */}
            <TouchableOpacity style={{ height: 60, aspectRatio: 1, alignSelf: 'flex-end', justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.setOpenDailyLoginAvatarPopup(false)} activeOpacity={0.5} >
                <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
            </TouchableOpacity>

            {/* <Image style={{ height: 95, width: 95, alignSelf: 'center' }} source={require('../../assets/images/ic_daily_rewards.png')} /> */}

            <Text style={{ marginTop: 0, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 26, textAlign: 'center', }} numberOfLines={2} >Daily Login Rewards</Text>


            <Image style={{ marginTop: 10, height: 286, resizeMode: 'contain' }} source={reward.avatar && reward.avatar.img && reward.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + reward.avatar.img.default }} />
            {reward.day == props.currentDay && reward.isClaimed == false ?
                <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => claimReward()} activeOpacity={0.5} >
                    <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >COLLECT REWARDS</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                </TouchableOpacity>
                : <></>}
            <Text style={{ textAlign: 'center', marginTop: 10, marginBottom: 10, fontFamily: fontFamilyStyleNew.bold, fontSize: 23, color: colors.red }}>{reward.avatar && reward.avatar.name}</Text>

        </View>
    )
}

export default DailyLoginAvatarPopup