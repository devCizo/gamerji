import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, StyleSheet, Dimensions } from 'react-native'
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import { addLog } from '../../appUtils/commonUtlis';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew, fontFamilyStyles } from '../../appUtils/commonStyles';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

const Chart = (props) => {

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Game Name */}
                <Text style={[fontFamilyStyles.extraBold, styles.gameTypeName]} numberOfLines={1} >Stats</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>
                <ScrollView style={{}} >

                    {/* <Image style={{ position: 'absolute', top: 30, left: 10, height: 100, width: 100, }} source={require('../../assets/images/dummy1.png')} /> */}

                    <Text style={{ marginTop: 30, marginBottom: 10, alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Bezier Line Chart</Text>
                    <LineChart
                        style={{ marginLeft: 10, marginRight: 10 }}
                        data={{
                            labels: ["January", "February", "March", "April", "May", "June"],
                            datasets: [
                                {
                                    data: [
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100
                                    ]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel="$"
                        yAxisSuffix="k"
                        yAxisInterval={1} // optional, defaults to 1
                        chartConfig={{
                            backgroundColor: null,//colors.black, //"#e26a00",
                            backgroundGradientFrom: null,//colors.black,//"#fb8c00",
                            backgroundGradientTo: null,//colors.yellow, //"#ffa726",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            // color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                // stroke: "#ffa726"
                                stroke: colors.blue
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />

                    <Text style={{ marginTop: 30, marginBottom: 10, alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Progress Chart</Text>
                    <ProgressChart
                        data={{
                            labels: ["Swim", "Bike", "Run"], // optional
                            data: [0.4, 0.6, 0.8]
                        }}
                        width={Dimensions.get("window").width}
                        height={220}
                        strokeWidth={16}
                        radius={32}
                        chartConfig={{
                            backgroundColor: null,//colors.black, //"#e26a00",
                            backgroundGradientFrom: null,//colors.black,//"#fb8c00",
                            backgroundGradientTo: null,//colors.yellow, //"#ffa726",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            // color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                // stroke: "#ffa726"
                                stroke: colors.blue
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                        hideLegend={false}
                    />

                    <Text style={{ marginTop: 30, marginBottom: 10, alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Bar Chart</Text>
                    <BarChart
                        // style={graphStyle}
                        data={{
                            labels: ["January", "February", "March", "April", "May", "June"],
                            datasets: [
                                {
                                    data: [20, 45, 28, 80, 99, 43]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width}
                        height={220}
                        yAxisLabel="$"
                        chartConfig={{
                            backgroundColor: null,//colors.black, //"#e26a00",
                            backgroundGradientFrom: null,//colors.black,//"#fb8c00",
                            backgroundGradientTo: null,//colors.yellow, //"#ffa726",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            // color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                // stroke: "#ffa726"
                                stroke: colors.blue
                            }
                        }}
                        verticalLabelRotation={30}
                    />

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default Chart

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: colors.black,
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})


const data = [
    {
        name: "Seoul",
        population: 21500000,
        color: "rgba(131, 167, 234, 1)",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "Toronto",
        population: 2800000,
        color: "#F00",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "Beijing",
        population: 527612,
        color: "red",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "New York",
        population: 8538000,
        color: "#ffffff",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "Moscow",
        population: 11920000,
        color: "rgb(0, 0, 255)",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    }
];