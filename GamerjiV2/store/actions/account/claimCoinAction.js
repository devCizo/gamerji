import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const requestClaimCoin = (viewModel) => {
    return {
        type: CLAIM_COIN,
        model: viewModel
    };
};

export const requestClaimCoinFailed = (viewModel) => {
    return {
        type: CLAIM_COIN_FAILED,
        model: viewModel
    };
};

export const requestClaimCoins = (payload, earnCoinId) => {
    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.EARN_COIN.CLAIM_COIN + earnCoinId);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;

        axios
            .post(WebFields.EARN_COIN.CLAIM_COIN + earnCoinId, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(requestClaimCoin(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(requestClaimCoinFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const CLAIM_COIN = 'CLAIM_COIN';
export const CLAIM_COIN_FAILED = 'CLAIM_COIN_FAILED';
