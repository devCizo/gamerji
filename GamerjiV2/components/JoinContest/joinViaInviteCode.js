import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, showErrorToastMessage } from '../../appUtils/commonUtlis';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';
import Modal from 'react-native-modal';
import SingleTournamentToJoin from '../joinTournament/singleTournamentToJoin';
import SingleContestToJoin from './singleContestToJoin';

const JoinViaInviteCode = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const [isOpenJoinViaInviteCode, setJoinViaInviteCodeView] = useState(false)
    var [inviteCode, setInviteCode] = useState('');

    const [contestTournamentDetail, setContestTournamentDetail] = useState(undefined)
    const [isOpenSingleTournamentPopup, setOpenSingleTournamentPopup] = useState(false)
    const [isOpenSingleContestPopup, setOpenSingleContestPopup] = useState(false)

    //Open Close InputView
    let openCloseJoinViaInviteCodeView = () => {
        setJoinViaInviteCodeView(!isOpenJoinViaInviteCode)
        if (!isOpenJoinViaInviteCode) {
            setInviteCode('')
        }
    }

    //Apply Invite Code
    let applyInviteCode = () => {

        if (inviteCode == '') {
            showErrorToastMessage('Please enter invite code')
            return
        }

        let payload = {
            code: inviteCode
        }
        props.setLoading(true)
        dispatch(actionCreators.requestJoinViaInviteCode(payload))
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.joinViaInviteCode.model }),
        shallowEqual
    );
    var { joinViaInviteCodeResponse } = currentState

    //JoinViaInviteCode Response
    useEffect(() => {
        if (joinViaInviteCodeResponse) {
            props.setLoading(false)
            if (joinViaInviteCodeResponse.item) {

                if (joinViaInviteCodeResponse.item.isSingle && joinViaInviteCodeResponse.item.isSingle == true) {
                    setOpenSingleContestPopup(true)
                    setContestTournamentDetail(joinViaInviteCodeResponse.item)
                    // RootNavigation.navigate(Constants.nav_single_contest_to_join, { contest: joinViaInviteCodeResponse.item })
                } else {
                    setOpenSingleTournamentPopup(true)
                    setContestTournamentDetail(joinViaInviteCodeResponse.item)
                    // RootNavigation.navigate(Constants.nav_single_tournament_to_join, { tournament: joinViaInviteCodeResponse.item })
                }
            } else {
                if (joinViaInviteCodeResponse.errors && joinViaInviteCodeResponse.errors[0] && joinViaInviteCodeResponse.errors[0].msg) {
                    showErrorToastMessage(joinViaInviteCodeResponse.errors[0].msg)
                }
            }
            joinViaInviteCodeResponse = undefined
        }
    }, [joinViaInviteCodeResponse]);


    return (
        < View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, height: isOpenJoinViaInviteCode ? 95 : 40, backgroundColor: colors.yellow, borderRadius: 10 }
        }>
            <TouchableOpacity style={{ height: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => openCloseJoinViaInviteCodeView()} activeOpacity={0.5} >

                <Text style={{ marginLeft: 12, textAlign: 'center', fontSize: 14, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >Join Via Invite Code</Text>

                {isOpenJoinViaInviteCode ?
                    <Image style={{ marginRight: 12, width: 20, height: 20 }} source={require('../../assets/images/drop_right_blue.png')} />
                    :
                    <Image style={{ marginRight: 12, width: 20, height: 20 }} source={require('../../assets/images/drop_down_blue.png')} />
                }
            </TouchableOpacity>

            {isOpenJoinViaInviteCode &&
                // Text Input view
                <View style={{ height: 50, flexDirection: 'row' }}>
                    <TextInput
                        style={{ flex: 1, marginTop: 5, marginLeft: 12, marginRight: 12, height: 40, borderRadius: 10, paddingHorizontal: 10, backgroundColor: 'white', fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }}
                        placeholder='Enter Invite Code'
                        onChangeText={text => setInviteCode(text)}
                        autoCorrect={false}
                        autoCapitalize='characters'
                    >{inviteCode}</TextInput>

                    {/* Apply Button */}
                    <TouchableOpacity style={{ position: 'absolute', top: 10, bottom: 10, right: 22, backgroundColor: colors.black, justifyContent: 'center', borderRadius: 5 }} onPress={() => applyInviteCode()} activeOpacity={0.5} >
                        <Text style={{ marginLeft: 12, marginRight: 12, textAlign: 'center', fontSize: 14, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold }} >APPLY</Text>
                    </TouchableOpacity>
                </View>
            }

            {isOpenSingleTournamentPopup &&
                <Modal isVisible={true}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SingleTournamentToJoin tournamentDetail={contestTournamentDetail} setOpenSingleTournamentPopup={setOpenSingleTournamentPopup} />
                </Modal>
            }

            {isOpenSingleContestPopup &&
                <Modal isVisible={true}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SingleContestToJoin contestDetail={contestTournamentDetail} setOpenSingleContestPopup={setOpenSingleContestPopup} />
                </Modal>
            }

        </View >
    )
}

export default JoinViaInviteCode;