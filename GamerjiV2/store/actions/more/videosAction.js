import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIDEO_LIST = 'VIDEO_LIST';
export const VIDEO_LIST_FAILED = 'VIDEO_LIST_FAILED';
export const RESET_VIDEOS_LIST_STATE = 'RESET_VIDEOS_LIST_STATE';

{ /* Request - Video List */ }
export const videoList = (viewModel) => {
    return {
        type: VIDEO_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Video List */ }
export const videoListFailed = (viewModel) => {
    return {
        type: VIDEO_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Videos List */ }
export const resetVideosListState = () => {
    return {
        type: RESET_VIDEOS_LIST_STATE,
    };
};

{ /* Request - Videos */ }
export const requestVideos = (payload) => {

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIDEO_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(videoList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(videoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};