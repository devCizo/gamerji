import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import RewardStoreRedeemPopup from './rewardStoreRedeemPopup';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';

const RewardStore = (props) => {

    const dispatch = useDispatch()
    const [categoryList, setCategoryList] = useState([])
    const [productList, setProductList] = useState([])
    const [isLoading, setLoading] = useState(false)
    const [isOpenRedeemPopup, setOpenRedeemPopup] = useState(false)
    const [selectedVoucher, setSelectedVoucher] = useState(undefined)

    useEffect(() => {

        setLoading(true);
        dispatch(actionCreators.requestRewardCategoryList({}))

        return () => {
            dispatch(actionCreators.resetRewardStoreState())
        }
    }, []);

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.rewardStore.model }),
        shallowEqual
    );
    var { categoryListResponse, categoryProductListResponse } = currentState

    // Category List Response
    useEffect(() => {
        if (categoryListResponse) {
            setLoading(false)

            if (categoryListResponse.list) {
                var tempArr = [{ isAll: true }]

                setCategoryList([...tempArr, ...categoryListResponse.list])
                selectCategory(0)
            }
        }
        categoryListResponse = undefined
    }, [categoryListResponse])

    // Category Product List Response
    useEffect(() => {
        if (categoryProductListResponse) {
            setLoading(false)

            if (categoryProductListResponse.list) {
                setProductList(categoryProductListResponse.list)
            }
        }
        categoryProductListResponse = undefined
    }, [categoryProductListResponse])


    const selectCategory = index => {
        setProductList([])
        setLoading(true);
        if (index == 0) {
            dispatch(actionCreators.requestRewardCategoryProductList({}))
        } else {
            dispatch(actionCreators.requestRewardCategoryProductList({ filter: { rewardCategory: categoryList[index]._id } }))
        }
    }

    const redeemPressed = (item) => {
        setSelectedVoucher(item)
        setOpenRedeemPopup(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            <View style={{ flex: 1, backgroundColor: colors.black }}>

                {/* Navigation Bar */}
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                    {/* Back Button */}
                    {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={RootNavigation.goBack}>
                        <Image style={{ width: 25, height: 23, alignSelf: 'center' }} source={require('../../assets/images/back_icon.png')} />
                    </TouchableOpacity>}

                    {/* Navigation Title */}
                    <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >REWARD STORE</Text>
                </View>

                {/* Main Category */}
                <View style={{}}>
                    <FlatList
                        style={{ marginTop: 5, marginLeft: 10, marginRight: 10 }}
                        data={categoryList}
                        renderItem={({ item, index }) => < RewardStoreCategoryItem item={item} index={index} selectCategory={selectCategory} />}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>

                <FlatList style={{ flex: 1 }}

                    style={{ marginTop: 20, marginLeft: 7.5, marginRight: 7.5 }}
                    data={productList}
                    renderItem={({ item, index }) => <RewardStoreVoucherItem item={item} redeemPressed={redeemPressed} />}
                    numColumns={2}
                    showsVerticalScrollIndicator={false}
                />

                <TouchableOpacity style={{ marginTop: 10, marginBottom: 20, height: 46, width: 220, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', backgroundColor: colors.yellow, borderRadius: 23 }} activeOpacity={0.6} onPress={() => RootNavigation.navigate(Constants.nav_my_rewards)}>
                    <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, }} >My Rewards</Text>
                </TouchableOpacity>

                {isLoading && <CustomProgressbar />}

                {isOpenRedeemPopup &&
                    <Modal
                        isVisible={isOpenRedeemPopup}
                        coverScreen={false}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <RewardStoreRedeemPopup setOpenRedeemPopup={setOpenRedeemPopup} voucher={selectedVoucher} />
                    </Modal>
                }

                {checkIsSponsorAdsEnabled('rewardStore') &&
                    <SponsorBannerAds screenCode={'rewardStore'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('rewardStore')} />
                }
            </View>

        </SafeAreaView>
    )
}

const RewardStoreCategoryItem = (props) => {

    return (
        <TouchableOpacity style={{ marginLeft: 8, marginRight: 8, alignItems: 'center' }} activeOpacity={0.5} onPress={() => props.selectCategory(props.index)} >

            <View style={{ height: 56, width: 56, backgroundColor: '#FEEDB4', borderRadius: 28, borderWidth: 1, borderColor: '#FFAC3E', justifyContent: 'center', alignItems: 'center' }}>

                {props.item.isAll ?
                    <Image style={{ height: 24, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/reward-dummy.png')} ></Image>
                    :
                    <Image style={{ height: 24, width: 24, resizeMode: 'contain' }} source={{ uri: props.item.img && props.item.img.default && generalSetting.UPLOADED_FILE_URL + props.item.img.default }} ></Image>
                }
            </View>

            <Text style={{ marginTop: 6, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >{props.item.isAll ? 'All' : props.item.name}</Text>
        </TouchableOpacity>
    )
}

const RewardStoreVoucherItem = (props) => {

    const width = (Constants.SCREEN_WIDTH - 45) / 2


    return (
        <View style={{ marginTop: 7.5, marginLeft: 7.5, marginRight: 7.5, marginBottom: 7.5, width: (Constants.SCREEN_WIDTH - 45) / 2, height: width + 55, borderRadius: 9, borderColor: colors.white, borderWidth: 1, overflow: 'hidden' }} >
            {/* height: 170 */}

            <Image style={{ flex: 1, resizeMode: 'contain' }} source={{ uri: props.item.img && props.item.img.default && generalSetting.UPLOADED_FILE_URL + props.item.img.default }} ></Image>

            <View style={{ height: 80, backgroundColor: colors.white, borderBottomLeftRadius: 8, borderBottomRightRadius: 8 }}>

                <View style={{ alignItems: 'center', flexDirection: 'row' }} >

                    {/* Voucher Name */}
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <CustomMarquee style={{ marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={0} value={props.item.name} />

                    </View>
                    {/* <Text style={{ marginLeft: 2, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center', alignSelf: 'center' }} >₹50</Text> */}

                    {/* Info Button */}
                    <TouchableOpacity style={{ height: 26, width: 26, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.6} onPress={() => props.redeemPressed(props.item)}>
                        <Image style={{ width: 20, height: 20 }} source={require('../../assets/images/info-reward.png')} />
                    </TouchableOpacity>
                </View>

                <Text style={{ marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, }} >{props.item.rewardCategory && props.item.rewardCategory.name}</Text>

                <View style={{ marginTop: 8, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }} >

                    <View style={{ height: 20, alignItems: 'center', flexDirection: 'row' }}>
                        {/* Discount Image */}
                        <Image style={{ marginLeft: 6, height: 15, width: 14, resizeMode: 'contain' }} source={require('../../assets/images/discount-reward.png')} />
                        <Text style={{ marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, }} >{props.item.coinAmount}</Text>
                    </View>

                    <TouchableOpacity style={{ marginRight: 4, height: 20, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.yellow, borderRadius: 10 }} activeOpacity={0.6} onPress={() => props.redeemPressed(props.item)}>
                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, }} >Redeem</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View>
    )
}

export default RewardStore;