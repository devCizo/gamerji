import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, Dimensions, ScrollView } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomDatePicker from '../../appUtils/customDatePicker';
import moment from 'moment';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { addLog, isValidEmail, showErrorToastMessage } from '../../appUtils/commonUtlis';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';;
import StateListPopup from '../../appUtils/stateListPopup'
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import { AdjustTokens, adjustTrackEvent } from '../../commonComponents/adjustIntegration';

const UpdateProfileAfterSignUp = (props) => {

    const dispatch = useDispatch()

    const [gamerjiName, setGamerjiName] = useState('')

    const [dob, setDOB] = useState('Select your date of birth');
    const [isOpenDatePickerPopup, setIsOpenDatePickerPopup] = useState(false);

    const [stateList, setStateList] = useState([])
    const [selectedState, setSelectedState] = useState(undefined)
    const [isOpenStateListPicker, setOpenStateListPicker] = useState(false)

    const [email, setEmail] = useState('')

    const [isLoading, setLoading] = useState(false);

    const [avtars, setAvtars] = useState([])
    const [avatarId, setAvatarId] = useState('')
    const [banners, setBanners] = useState([])
    const [bannerId, setBannerId] = useState('')

    const [singupCode, setSingupCode] = useState('')
    const [appliedSignupCode, setAppliedSignupCode] = useState(undefined)

    // API Call
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestStateList({
            skip: 0,
            limit: 100, sortBy: 'name', sort: 'asc'
        }))
        dispatch(actionCreators.requestAvatarsForProfileInfo())
        //dispatch(actionCreators.requestBannersForProfileInfo())

        return () => {
            dispatch(actionCreators.resetDobStateValidationState())
            dispatch(actionCreators.resetProfileInfoState())
        }

    }, []);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.dobStateValidation.model }),
        shallowEqual
    );
    var { stateListResponse, updateProfileResponse } = currentState

    // API Response
    const { currentStateForProfileInfo } = useSelector(
        (state) => ({ currentStateForProfileInfo: state.profileInfo.model }),
        shallowEqual
    );
    var { avatarsForProfileInfoResponse, bannersForProfileInfoResponse, applySignupCodeResponse } = currentStateForProfileInfo

    // State List Response
    useEffect(() => {
        if (stateListResponse) {
            setLoading(false)
            if (stateListResponse.list) {
                setStateList(stateListResponse.list)
            }
        }
    }, [stateListResponse]);

    //Avatars For Profile Info Response
    useEffect(() => {
        if (avatarsForProfileInfoResponse) {
            if (avatarsForProfileInfoResponse.list) {
                var tempArr = avatarsForProfileInfoResponse.list.map(function (item, index) {
                    var itm = { ...item }
                    if (index == 0) {
                        itm.selected = true
                        setAvatarId(item._id)
                    }
                    return itm;
                });
                setAvtars(tempArr)
            }
        }
        avatarsForProfileInfoResponse = undefined
    }, [avatarsForProfileInfoResponse]);

    // Banners For Profile Info Response
    useEffect(() => {
        if (bannersForProfileInfoResponse) {
            if (bannersForProfileInfoResponse.list) {
                var tempArr = bannersForProfileInfoResponse.list.map(function (item, index) {
                    var itm = { ...item }
                    if (index == 0) {
                        itm.selected = true
                        setBannerId(item._id)
                    }
                    return itm;
                });
                setBanners(tempArr)
            }
        }
        bannersForProfileInfoResponse = undefined
    }, [bannersForProfileInfoResponse]);


    //Update profile data
    let updateProfileData = () => {
        // let message = {
        //     message: "Some message title",
        //     type: "info",
        //     position: "top",
        //     description: "Lorem ipsum dolar sit amet"
        // }

        // showMessage(message);

        // return

        if (gamerjiName == '') {
            showErrorToastMessage('Please enter gamerji username')
        } else if (dob == 'Select your date of birth') {
            showErrorToastMessage('Please select date of birth')
        } else if (!selectedState) {
            showErrorToastMessage('Please select state')
        } else if (email == '') {
            showErrorToastMessage('Please enter email address')
        } else if (!isValidEmail(email)) {
            showErrorToastMessage('Please enter valid email address')
        } else {
            var payload = {
                gamerjiName: gamerjiName,
                dateOfBirth: dob,
                address: {
                    state: selectedState._id
                },
                email: email,
                isDetailsFilled: true
            }

            if (avatarId) {
                payload.avatar = avatarId
            }
            if (bannerId) {
                payload.userBanner = bannerId
            }
            if (appliedSignupCode) {
                payload.code = singupCode
            }
            addLog("updateProfileData payload==>", payload);
            // API Call
            setLoading(true)
            dispatch(actionCreators.requestUpdateProfile(payload))
        }
    }

    //Update Profile Response
    useEffect(() => {
        if (updateProfileResponse) {
            setLoading(false)
            adjustTrackEvent(AdjustTokens.singup)
            if (updateProfileResponse.item) {
                props.navigation.replace(Constants.nav_bottom_navigation)
            }
        }

        updateProfileResponse = undefined

    }, [updateProfileResponse])

    const openDatePickerPopup = (visible) => {
        setIsOpenDatePickerPopup(visible)
    }

    const avtarSelected = (index) => {

        var tempArr = [...avtars]
        tempArr.forEach((item, index) => {
            item.selected = false
        })
        tempArr[index].selected = true
        setAvtars(tempArr)
        setAvatarId(tempArr[index]._id)
    }

    const bannerSelected = (index) => {
        var tempArr = [...banners]
        tempArr.forEach((item, index) => {
            item.selected = false
        })
        tempArr[index].selected = true
        setBanners(tempArr)
        setBannerId(tempArr[index]._id)
    }

    const setDOBFromPicker = (dobPicker) => {
        setDOB(moment(dobPicker).format('DD/MM/YYYY'))
    }

    // Singup Code
    const applySingupCode = () => {

        if (appliedSignupCode) {
            setSingupCode('')
            setAppliedSignupCode(undefined)
            return
        }

        if (singupCode == '') {
            showErrorToastMessage('Please enter signup code')
        } else {
            setLoading(true)
            dispatch(actionCreators.requestApplySignupCode({ code: singupCode }))
        }
    }

    // Banners For Profile Info Response
    useEffect(() => {
        if (applySignupCodeResponse) {
            setLoading(false)
            addLog('applySignupCodeResponse', applySignupCodeResponse)
            if (applySignupCodeResponse.item) {
                setAppliedSignupCode(applySignupCodeResponse.item)
            } else {
                if (applySignupCodeResponse.errors && applySignupCodeResponse.errors[0] && applySignupCodeResponse.errors[0].msg) {
                    showErrorToastMessage(applySignupCodeResponse.errors[0].msg)
                }
            }
        }
        applySignupCodeResponse = undefined
    }, [applySignupCodeResponse]);

    const validateGamerjiUsername = text => {
        if (text.includes(' ')) {
            setGamerjiName(text.trim());
        } else {
            setGamerjiName(text);
        }
        addLog('text', text)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >

                {/* Navigation Title */}
                <Text style={{ flex: 1, color: 'white', fontSize: 20, textAlign: 'center', alignSelf: 'center', fontFamily: fontFamilyStyleNew.extraBold, }} numberOfLines={1} >CREATE PROFILE</Text>

                {/* Skip Button */}
                {/* <TouchableOpacity style={{ marginRight: 20, width: 60, height: 30, flexDirection: 'row', backgroundColor: colors.white, borderRadius: 15, justifyContent: 'space-between', alignItems: 'center', justifyContent: 'center' }} onPress={() => skipProfileInfo()} activeOpacity={0.5} >

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }} >SKIP</Text>

                </TouchableOpacity> */}
            </View>

            <View style={{ flex: 1, borderTopLeftRadius: 40, borderTopRightRadius: 40, overflow: 'hidden' }}>
                <ScrollView style={{ flex: 1, backgroundColor: 'white' }}
                    keyboardShouldPersistTaps={'always'}
                >
                    {/* Container View */}
                    <View style={{}}>

                        {/* Signup Code */}
                        <View style={{ width: '100%', backgroundColor: colors.yellow, borderTopLeftRadius: 40, borderTopRightRadius: 40, borderBottomLeftRadius: 40, borderBottomRightRadius: 40, }}>

                            <View style={{ marginTop: 30, marginBottom: 30 }}>

                                <Text style={{ marginLeft: 20, marginRight: 20, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Signup Code</Text>

                                <View style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, flexDirection: 'row' }} >
                                    <TextInput
                                        style={{ flex: 1, paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: colors.black }}
                                        placeholder='Enter Signup Code' placeholderTextColor={'#70717A'}
                                        onChangeText={text => setSingupCode(text)}
                                        autoCapitalize='characters'
                                        autoCorrect={false}
                                        editable={appliedSignupCode ? false : true}
                                    >
                                        {singupCode}
                                    </TextInput>

                                    <TouchableOpacity style={{ marginRight: 0, height: 44, width: 84, backgroundColor: appliedSignupCode ? colors.red : colors.black, borderRadius: 23, alignItems: 'center', justifyContent: 'center' }} onPress={() => updateProfileData()} activeOpacity={0.5} onPress={() => applySingupCode()} >

                                        <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{appliedSignupCode ? 'REMOVE' : 'APPLY'}</Text>

                                    </TouchableOpacity>
                                </View>

                                {appliedSignupCode &&
                                    <View style={{ marginTop: 12, alignSelf: 'center', flexDirection: 'row', alignItems: 'center' }}>
                                        <Image style={{ height: 16, width: 16, }} source={require('../../assets/images/check_blue.png')} />
                                        <Text style={{ marginLeft: 4, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>{singupCode} Signup code applied</Text>
                                    </View>
                                }

                            </View>

                        </View>

                        {/* Gamerji Username */}
                        <View style={{ marginTop: 30, marginLeft: 20, marginRight: 20, }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>Gamerji Username</Text>

                            <TextInput
                                style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                placeholder='Enter Gamerji Username' placeholderTextColor={'#70717A'}
                                onChangeText={text => setGamerjiName(text.trim())}
                                autoCorrect={false}
                                maxLength={30}
                                autoCapitalize='none'
                            >{gamerjiName.trim()}
                            </TextInput>

                            <Text style={{ marginTop: 12, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12 }}>Note: "Gamerji Username" is a unique name and cannot be changed once selected</Text>
                        </View>

                        {/* DOB */}
                        <View style={{ marginTop: 28, marginLeft: 20, marginRight: 20, }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>What's your date of birth?</Text>

                            <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => openDatePickerPopup(true)}
                                activeOpacity={0.5} >

                                <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                    {dob}
                                </Text>

                                <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />

                            </TouchableOpacity>
                        </View>

                        {/* State */}

                        <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20, }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>Where are you from?</Text>

                            <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => setOpenStateListPicker(true)}
                                activeOpacity={0.5} >

                                <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                    {selectedState ? selectedState.name : 'Select your state'}
                                </Text>

                                <Image style={{ marginRight: 15, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />

                            </TouchableOpacity>
                        </View>

                        {/* Email */}
                        <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20 }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>Email Address</Text>

                            <TextInput
                                style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                placeholder='Enter Email Address' placeholderTextColor={'#70717A'}
                                onChangeText={text => setEmail(text)}
                                autoCorrect={false}
                                keyboardType={'email-address'}
                                autoCapitalize='none'
                            >{email}
                            </TextInput>
                        </View>

                        <View style={{}} >

                            {avtars.length > 0 &&
                                <View style={{}} >
                                    {/* <Text style={{ marginTop: 20, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} ></Text> */}
                                    <Text style={{
                                        marginTop: 18, marginLeft: 20, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, alignSelf: 'center'
                                    }}>Select a gaming avatar for your profile</Text>
                                    <FlatList
                                        style={{ marginTop: 12, marginLeft: 10, marginRight: 10 }}
                                        data={avtars}
                                        renderItem={({ item, index }) => <ProfileInfoAvatarItem item={item} index={index} avtarSelected={avtarSelected} />}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                    />
                                </View>
                            }

                            {/* {banners.length > 0 &&
                            <View>
                                <View style={{ alignItems: 'center' }} >
                                    <Text style={{ marginTop: 20, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Banner</Text>
                                    <Image style={{ marginTop: 2, height: 2, width: 30, backgroundColor: colors.yellow, borderRadius: 1, overflow: 'hidden' }} />
                                </View>

                                <FlatList
                                    style={{ marginTop: 15, marginLeft: 10, marginRight: 10, height: 80 }}
                                    data={banners}
                                    renderItem={({ item, index }) => <ProfileInfoFrameItem item={item} index={index} bannerSelected={bannerSelected} />}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                />
                            </View>
                        } */}

                            {/* Signup Code */}
                            {/* <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Signup Code</Text>

                            <View style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row' }} >
                                <TextInput
                                    style={{ flex: 1, paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                    placeholder='Enter Signup Code' placeholderTextColor={'#70717A'}
                                    onChangeText={text => setSingupCode(text)}
                                    autoCorrect={false}
                                    editable={appliedSignupCode ? false : true}
                                >
                                    {singupCode}
                                </TextInput>

                                <TouchableOpacity style={{ marginRight: 0, height: 46, width: 84, backgroundColor: appliedSignupCode ? colors.red : colors.yellow, borderRadius: 23, alignItems: 'center', justifyContent: 'center' }} onPress={() => updateProfileData()} activeOpacity={0.5} onPress={() => applySingupCode()} >

                                    <Text style={{ color: appliedSignupCode ? colors.white : colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{appliedSignupCode ? 'REMOVE' : 'APPLY'}</Text>

                                </TouchableOpacity>
                            </View>

                            {appliedSignupCode &&
                                <View style={{ marginTop: 12, alignSelf: 'center', flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>{singupCode} Signup code applied</Text>
                                    <Image style={{ marginLeft: 4, height: 24, width: 24 }} source={require('../../assets/images/check_green.png')} />
                                </View>
                            }

                        </View> */}

                            {/* Submit Button */}
                            <TouchableOpacity style={{ marginTop: 30, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => updateProfileData()} activeOpacity={0.5} >

                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >SUBMIT</Text>
                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </View>

            <Modal
                isVisible={isOpenDatePickerPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <CustomDatePicker setDOBFromPicker={setDOBFromPicker} openDatePickerPopup={openDatePickerPopup} mode='date' />
            </Modal>

            {isOpenStateListPicker &&
                <Modal
                    isVisible={isOpenStateListPicker}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <StateListPopup stateList={stateList} openStateListPicker={setOpenStateListPicker} setSelectedState={setSelectedState} />
                </Modal>
            }

            {isLoading && <CustomProgressbar />}

        </SafeAreaView >
    )
}

const ProfileInfoAvatarItem = (props) => {

    var item = props.item
    var isSelected = item.selected && item.selected ? true : false

    return (
        <TouchableOpacity onPress={() => props.avtarSelected(props.index)} activeOpacity={0.5}>

            <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 5, borderWidth: isSelected ? 3 : 0, borderColor: isSelected ? colors.yellow : null }} >
                <Image style={{ marginTop: 5, marginBottom: 5, height: 240, width: 120, resizeMode: 'contain', }} source={{ uri: item.img && item.img.default && generalSetting.UPLOADED_FILE_URL + item.img.default }} />
            </View>

            {isSelected &&
                <Image style={{ position: 'absolute', top: 5, right: 15, height: 15, width: 15 }} source={require('../../assets/images/check_yellow_bg.png')} />
            }
            <View style={{ width: 120, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5} >

                <Text style={{ color: colors.black, flexWrap: 'wrap', flex: 0.3, alignItems: 'center', justifyContent: 'center', flexDirection: 'column', }}>{item.name}</Text>
            </View>

        </TouchableOpacity>
    )
}

const ProfileInfoFrameItem = (props) => {

    var item = props.item
    var isSelected = item.selected && item.selected ? true : false

    return (
        <TouchableOpacity onPress={() => props.bannerSelected(props.index)} activeOpacity={0.5} >

            <Image style={{ marginLeft: 10, marginRight: 10, height: 66, width: 138, borderRadius: 5, borderWidth: isSelected ? 3 : 2, borderColor: isSelected ? colors.yellow : colors.grey }} source={{ uri: item.img && item.img.default && generalSetting.UPLOADED_FILE_URL + item.img.default }} />

            {isSelected &&
                <Image style={{ position: 'absolute', top: 5, right: 15, height: 11, width: 11 }} source={require('../../assets/images/check_yellow_bg.png')} />
            }

        </TouchableOpacity>
    )
}

export default UpdateProfileAfterSignUp