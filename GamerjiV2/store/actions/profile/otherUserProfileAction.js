import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';

export const otherProfileRequest = (viewModel) => {
    return {
        type: REQUEST_OTHER_PROFILE,
        model: viewModel
    };
};

export const otherProfileRequestFailed = (viewModel) => {
    return {
        type: REQUEST_OTHER_PROFILE_FAILED,
        model: viewModel
    };
};


export const requestOtherUserProfile = (payload) => {
    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.ACCOUNT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ACCOUNT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(otherProfileRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(otherProfileRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};



export const otherProfileStatList = (viewModel) => {
    return {
        type: OTHER_PROFILE_STAT_LIST,
        model: viewModel
    };
};

export const otherProfileStatListFailed = (viewModel) => {
    return {
        type: OTHER_PROFILE_STAT_LIST_FAILED,
        model: viewModel
    };
};


{ /* Request - Stats */ }
export const requestOtherProfileStats = (payload) => {
    addLog("STATS PAYLOAD:::> ", payload);
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_STAT_LIST, payload)
            .then((response) => {
                addLog("STATS SUCCESS:::> ", '');
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(otherProfileStatList(response.data));
            })
            .catch((error) => {
                addLog("STATS ERROR:::> ", '');
                addLog("Error:::> ", error.response.data);
                dispatch(otherProfileStatListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const resetOtherProfileState = () => {
    return {
        type: RESET_OTHER_USER_PROFILE_STATE,
    };
};


export const REQUEST_OTHER_PROFILE = 'REQUEST_OTHER_PROFILE';
export const REQUEST_OTHER_PROFILE_FAILED = 'REQUEST_OTHER_PROFILE_FAILED';
export const OTHER_PROFILE_STAT_LIST = 'OTHER_PROFILE_STAT_LIST';
export const OTHER_PROFILE_STAT_LIST_FAILED = 'OTHER_PROFILE_STAT_LIST_FAILED';
export const RESET_OTHER_USER_PROFILE_STATE = 'RESET_OTHER_USER_PROFILE_STATE'
