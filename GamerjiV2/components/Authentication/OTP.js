import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, ScrollView, TouchableOpacity, BackHandler, Keyboard, SafeAreaView, Image, DeviceEventEmitter, Platform, TextInput } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, fontStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import OtpInputs from '../../commonComponents/OtpInputs';
import * as actionCreators from '../../store/actions/index';
import WebFields from '../../webServices/webFields.json';
import colors from '../../assets/colors/colors';
import { addLog, showSuccessToastMessage, showErrorToastMessage } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { AdjustTokens, adjustTrackEvent } from '../../commonComponents/adjustIntegration';
// import RNOtpReader from 'react-native-otp-reader'
// import SmsRetriever from 'react-native-sms-retriever';
// import RNOtpVerify from 'react-native-otp-verify';
// import RnSmsRetriever from "rn-sms-retriever";
import * as ReadSms from 'react-native-read-sms/ReadSms';

const OTP = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const selectedCountry = props.route.params.selectedCountry
    const mobileNo = props.route.params.mobileNo
    const [timerCount, setTimer] = useState(Constants.INT_OTP_TIMEOUT)
    const [isLoading, setLoading] = useState(false)
    const [otp, setOtp] = useState('')

    const startReadSMS = async () => {
        const hasPermission = await ReadSms.requestReadSMSPermission();
        if (hasPermission) {
            ReadSms.startReadSMS((status, sms, error) => {
                if (status == "success") {
                    onChangeOtpText(sms.slice(26, 32))
                    // console.log("Great!! you have received new sms:", sms);
                }
            });
        }
    }

    useEffect(() => {
        startReadSMS();

        return () => {
            ReadSms.stopReadSMS();
        }
    }, []);
    // Get the phone number (first gif)
    /*_onPhoneNumberPressed = async () => {
        try {
            const phoneNumber = await SmsRetriever.requestPhoneNumber();
        } catch (error) {
            console.log(JSON.stringify(error));
        }
    };

    // Get the SMS message (second gif)
    _onSmsListenerPressed = async () => {
        try {
            const registered = await SmsRetriever.startSmsRetriever();
            if (registered) {
                SmsRetriever.addSmsListener(event => {
                    console.log(event.message);
                    SmsRetriever.removeSmsListener();
                });
            }
        } catch (error) {
            console.log(JSON.stringify(error));
        }
    };*/

    /*RNOtpReader.GenerateHashString((hash) => {
        addLog(' OTP Hash Key:::> ', hash);
    });

    RNOtpReader.StartObservingIncomingSMS((message) => {
        console.log('Started the SMS observer successfully', message);
    }, (error) => {
        console.log('Starting the SMS observer failed', error);
    });

    DeviceEventEmitter.addListener('otpReceived', (message) => {
        // Retrived Message
        console.log('message', message);

        // OTP Message
        const regex = /[0-9]{6}/gm; // OTP code length is 6
        const otpFound = message && message.message.match(regex);
        const found = otpFound || [];
        if (!isEmpty(found)) {
            console.log('OTP Code', found.join(''));
        }
    });*/

    /*useEffect(() => {
        let smsListener: undefined | EmitterSubscription;
        async function innerAsync() {
            // get list of available phone numbers
            const selectedPhone = await RnSmsRetriever.requestPhoneNumber();
            console.log('Selected Phone is : ' + selectedPhone);
            // get App Hash
            const hash = await RnSmsRetriever.getAppHash();
            console.log('Your App Hash is : ' + hash);
            // set Up SMS Listener;
            smsListener = DeviceEventEmitter.addListener(RnSmsRetriever.SMS_EVENT, (data: any) => {
                console.log(data, 'SMS value');
            });
            // start Retriever;
            await RnSmsRetriever.startSmsRetriever();
        }
        // only to be used with Android
        if (Platform.OS === 'android') innerAsync();
        return () => {
            // remove the listsner on unmount
            smsListener?.remove();
        };
    }, []);*/

    // This function should call the OTP API to be verified
    const callToOTPVerficationAPI = (otp) => {
        addLog(otp.length)
        if (otp.length >= 6) {
            Keyboard.dismiss();
            let payload = {
                type: WebFields.OTP.REQUEST_VALIDATE_OTP,
                username: mobileNo,
                otp: otp,
            }
            setLoading(true)
            dispatch(actionCreators.userRequestOTP(payload))
        }
    }

    // This function should call the Resend OTP API
    const callToResendOTPAPI = () => {
        Keyboard.dismiss();
        let payload = {
            type: WebFields.LOGIN.REQUEST_OTP_REQUEST,
            username: mobileNo,
            phoneCode: selectedCountry.dialingCode,
            country: selectedCountry._id
        }
        setLoading(true)
        dispatch(actionCreators.userRequestOTP(payload))
        showHideResendOTP()
    };

    // This function sets the OTP Request API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.authentication.model }),
        shallowEqual
    );
    var { authenticationResponse } = currentState

    // This function checks the response and then it should be redirected to the OTP screen
    useEffect(() => {
        if (authenticationResponse) {
            setLoading(false)
            if (authenticationResponse.success && authenticationResponse.type == 'validateOTP') {

                // if (props.route.params.isNewUser && props.route.params.isNewUser == true) {
                //     props.navigation.navigate(Constants.nav_update_profile_after_signUp);
                // } else {
                //     props.navigation.navigate(Constants.nav_bottom_navigation);
                // }
                addLog("authenticationResponse.item==>", authenticationResponse.item);

                adjustTrackEvent(AdjustTokens.login)

                if (authenticationResponse.item && authenticationResponse.item.isDetailsFilled == false) {
                    props.navigation.navigate(Constants.nav_update_profile_after_signUp);
                } else {
                    props.navigation.navigate(Constants.nav_bottom_navigation);
                }

                return () => {
                    dispatch(actionCreators.resetAuthenticationState())
                }
            } else if (authenticationResponse.errors) {
                if (authenticationResponse.errors[0] && authenticationResponse.errors[0].msg) {
                    showErrorToastMessage(authenticationResponse.errors[0].msg)
                }
            }
        }
        authenticationResponse = undefined
    }, [authenticationResponse]);

    // This function should set the OTP Timer of 30 seconds.
    const setCountDownTimer = () => {
        setTimer(timerCount - 1);
    }

    // This function should show/hide Resend OTP Button Visibility.
    const showHideResendOTP = () => {
        setTimer(Constants.INT_OTP_TIMEOUT)
    }

    // This function should check if the timer is not 0 then it should set the OTP Timer again
    useEffect(() => {
        if (timerCount !== 0) {
            setTimeout(() => {
                setCountDownTimer();
            }, 1000);
        }
    }, [timerCount]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const onChangeOtpText = otp => {
        setOtp(otp)
        if (otp.length == 6) {
            addLog('success')
            callToOTPVerficationAPI(otp)
        }
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Container - Full Curve Background */}
                <View style={marginStyles.topMargin_120} />

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, paddingStyles.padding_20, borderStyles.bodyTopRoundedCorner_60, colorStyles.whiteBackgroundColor]}>

                    {/* Image - GamerJi Logo */}
                    <Image style={[positionStyles.absolute, heightStyles.height_103, widthStyles.width_132, alignmentStyles.alignSelfCenter, resizeModeStyles.resizeMode, marginStyles.top_75_minus]} source={require('../../assets/images/logo.png')} />

                    {/* Scroll View */}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Image - User Authentication */}
                        {/* <Image style={[heightStyles.height_190, widthStyles.width_301, alignmentStyles.alignSelfCenter, marginStyles.topMargin_40, marginStyles.bottomMargin_10]} source={require('../../assets/images/otp_bg.png')} /> */}
                        {/* <Image style={{ position: "relative", height: 215, width: 265, alignSelf: "center", marginTop: 40, marginBottom: 10 }} source={require('../../assets/images/login_logo.png')} /> */}

                        {/* Text - Enter OTP Header */}
                        <Text style={[fontFamilyStyles.bold, colorStyles.blackColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_30, marginStyles.topMargin_38]}> {Constants.text_enter_otp} </Text>

                        {/* Text - We have sent an OTP to your number */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]}> {Constants.text_sent_otp} </Text>

                        {/* Text - Mobile No */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.bold, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_10]}> {selectedCountry.dialingCode} - {mobileNo} </Text>

                        {/* Text - Enter the OTP you received */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]}> {Constants.text_otp_received} </Text>

                        {/* OTP Input view */}
                        <TextInput style={{ marginTop: 20, height: 44, width: 200, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', alignSelf: 'center', textAlign: 'center', fontSize: 18, fontFamily: fontFamilyStyleNew.bold, color: colors.black }}
                            placeholder={'Enter OTP'}
                            onChangeText={text => onChangeOtpText(text)}
                            keyboardType='number-pad'
                            maxLength={6}
                        >
                            {otp}
                        </TextInput>

                        {/* Wrapper - OTP Inputs */}
                        {/* <View style={[containerStyles.container, flexDirectionStyles.row, alignmentStyles.alignSelfCenter, marginStyles.topMargin_20]}> */}

                        {/* Text Input - Array Class Component */}
                        {/* <OtpInputs getOtp={(otp) => callToOTPVerficationAPI(otp)} />
                        </View> */}

                        {/* Text - Didn't receive the OTP? */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_20]}> {Constants.text_otp_did_not_received} </Text>

                        {/* Text - Request for a new one in 30 Seconds */}
                        {timerCount != 0 ? <Text style={[alignmentStyles.alignSelfCenter, colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, marginStyles.topMargin_10]}> {Constants.text_request_new_otp} {timerCount} Seconds</Text> : null
                        }

                        {/* TouchableOpacity - Resend OTP Button Click Event */}
                        <TouchableOpacity style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]} disabled={timerCount == 0 ? false : true} onPress={callToResendOTPAPI} activeOpacity={0.5}>

                            {/* Text - Resend OTP */}
                            <Text style={[fontFamilyStyles.bold, timerCount == 0 ? colorStyles.redColor : colorStyles.silverColor, fontSizeStyles.fontSize_16, fontStyles.underline, timerCount == 0 ? colorStyles.redUnderlineColor : colorStyles.silverUnderlineColor]} > {Constants.action_resend_otp} </Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default OTP;