import moment from 'moment';
import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import DatePicker from 'react-native-date-picker';
import colors from '../assets/colors/colors';
import { fontFamilyStyleNew } from './commonStyles';
import { addLog } from './commonUtlis';

const CustomDatePicker = (props) => {

    const [dobPicker, setDOB] = useState(new Date(moment(new Date()).subtract(18, 'years').format('yyyy-MM-DD')))

    const setDOBFromPicker = (dobPicker) => {
        setDOB(dobPicker)
    }

    const doneBtnPress = () => {
        props.setDOBFromPicker(dobPicker)
        props.openDatePickerPopup(false)
    }

    return (
        <View style={styles.mainContainer}>
            {/* Main View */}
            <View style={styles.mainContainer}>

                {/* Header View */}
                <View style={styles.headerContainer}>

                    <Text style={{ marginLeft: 60, color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center' }}>Select Date Of Birth</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() => props.openDatePickerPopup()} >
                        <Image style={{ height: 25, width: 25 }} source={require('../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Center Content */}
                <View style={styles.centerContentContainer} >

                    <DatePicker
                        mode={props.mode}
                        date={dobPicker}
                        onDateChange={setDOBFromPicker}
                        maximumDate={new Date(moment(new Date()).subtract(18, 'years').format('yyyy-MM-DD'))}
                    />

                    {/* Done Button */}
                    <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, backgroundColor: colors.black, borderRadius: 23, justifyContent: 'center', alignItems: 'center' }} onPress={() => doneBtnPress()} activeOpacity={1} >
                        <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Done</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.white,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    centerContentContainer: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 30,
        alignSelf: 'center'
    },
})

export default CustomDatePicker;



