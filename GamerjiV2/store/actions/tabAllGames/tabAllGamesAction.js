import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';

{ /* Action Types */ }
export const BANNER_LIST = 'BANNER_LIST';
export const BANNER_LIST_FAILED = 'BANNER_LIST_FAILED';
export const FEATURED_TOURNAMENT_LIST = 'FEATURED_TOURNAMENT_LIST';
export const FEATURED_TOURNAMENT_LIST_FAILED = 'FEATURED_TOURNAMENT_LIST_FAILED';
export const ALL_GAMES_LIST = 'ALL_GAMES_LIST';
export const ALL_GAMES_LIST_FAILED = 'ALL_GAMES_LIST_FAILED';
export const HTML5_GAME_SETTING_LIST = 'HTML5_GAME_SETTING_LIST';
export const HTML5_GAME_SETTING_LIST_FAILED = 'HTML5_GAME_SETTING_LIST_FAILED';
export const PROFILE_REQUEST = 'PROFILE_REQUEST'
export const PROFILE_REQUEST_FAILED = 'PROFILE_REQUEST_FAILED'
export const DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST = 'DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST'
export const DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST_FAILED = 'DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST_FAILED'
export const FEATURED_TOURNAMENT_DETAIL = 'FEATURED_TOURNAMENT_DETAIL';
export const FEATURED_TOURNAMENT_DETAIL_FAILED = 'FEATURED_TOURNAMENT_DETAIL_FAILED';
export const SCREENS_LIST = 'SCREENS_LIST'
export const SCREENS_LIST_FAILED = 'SCREENS_LIST_FAILED'
export const SYNC_CONTACTS = 'SYNC_CONTACTS'
export const SYNC_CONTACTS_FAILED = 'SYNC_CONTACTS_FAILED'
export const INFO_POPUP_LIST = 'INFO_POPUP_LIST'
export const INFO_POPUP_LIST_FAILED = 'INFO_POPUP_LIST_FAILED'
export const RESET_ALL_GAMES_STATE = 'RESET_ALL_GAMES_STATE';

export const VERSION_INFO_POPUP_LIST = 'VERSION_INFO_POPUP_LIST'
export const VERSION_INFO_POPUP_LIST_FAILED = 'VERSION_INFO_POPUP_LIST_FAILED'

{ /* Request - Html5 Game Setting List */ }
export const versionInfoList = (viewModel) => {
    return {
        type: VERSION_INFO_POPUP_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Html5 Game Setting List */ }
export const versionInfoListFailed = (viewModel) => {
    return {
        type: VERSION_INFO_POPUP_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Html5 Game Setting List */ }
export const requestVersionInfoList = (payload) => {

    return (dispatch) => {
        // addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.APP_VERSION.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(versionInfoList(response.data));
            })
            .catch((error) => {
                // addLog(error.response.data);
                dispatch(versionInfoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


{ /* Request - Html5 Game Setting List */ }
export const html5GameSettingList = (viewModel) => {
    return {
        type: HTML5_GAME_SETTING_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Html5 Game Setting List */ }
export const html5GameSettingListFailed = (viewModel) => {
    return {
        type: HTML5_GAME_SETTING_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Html5 Game Setting List */ }
export const requestHtml5GameSettings = (payload) => {

    return (dispatch) => {
        // addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HTML5GAME_SETTINGS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(html5GameSettingList(response.data));
            })
            .catch((error) => {
                // addLog(error.response.data);
                dispatch(html5GameSettingListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Banner List */ }
export const bannerList = (viewModel) => {
    return {
        type: BANNER_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Banner List */ }
export const bannerListFailed = (viewModel) => {
    return {
        type: BANNER_LIST_FAILED,
        model: viewModel
    };
};
{ /* Request - Featured Tournament List */ }
export const featuredTournamentList = (viewModel) => {
    return {
        type: FEATURED_TOURNAMENT_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Featured Tournament List */ }
export const featuredTournamentListFailed = (viewModel) => {
    return {
        type: FEATURED_TOURNAMENT_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - All Games List */ }
export const allGamesList = (viewModel) => {
    return {
        type: ALL_GAMES_LIST,
        model: viewModel
    };
};

{ /* Request Failure - All Games List */ }
export const allGamesListFailed = (viewModel) => {
    return {
        type: ALL_GAMES_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Get Profile */ }
export const getProfileRequest = (viewModel) => {
    return {
        type: PROFILE_REQUEST,
        model: viewModel
    };
};

{ /* Request Failure - Get Profile */ }
export const getProfileRequestFailed = (viewModel) => {
    return {
        type: PROFILE_REQUEST_FAILED,
        model: viewModel
    };
};

{ /* Request - Daily Login Rewards Status Check */ }
export const dailyLoginRewardsStatusCheckRequest = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST,
        model: viewModel
    };
};

{ /* Request Failure - Daily Login Rewards Status Check */ }
export const dailyLoginRewardsStatusCheckRequestFailed = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST_FAILED,
        model: viewModel
    };
};

export const featuredTournamentDetail = (viewModel) => {
    return {
        type: FEATURED_TOURNAMENT_DETAIL,
        model: viewModel
    };
};

export const featuredTournamentDetailFailed = (viewModel) => {
    return {
        type: FEATURED_TOURNAMENT_DETAIL_FAILED,
        model: viewModel
    };
};

{ /* Request - Screens List */ }
export const screensList = (viewModel) => {
    return {
        type: SCREENS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Screens List */ }
export const screensListFailed = (viewModel) => {
    return {
        type: SCREENS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - All Games */ }
export const resetAllGamesState = () => {
    return {
        type: RESET_ALL_GAMES_STATE,
    };
};

{ /* Request - Banners */ }
export const requestBanners = (payload) => {

    return (dispatch) => {
        // addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.BANNERS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(bannerList(response.data));
            })
            .catch((error) => {
                // addLog(error.response.data);
                dispatch(bannerListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Featured Tournaments */ }
export const requestFeaturedTournaments = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.FEATURED_TOURNAMENTS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(featuredTournamentList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(featuredTournamentListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - All Games */ }
export const requestAllGames = (payload) => {
    return (dispatch) => {

        addLog("URL:::> ", WebFields.ALL_GAMES.MODE);
        addLog("Payload:::> ", payload);
        addLog("Token:::> ", WebFields.HEADERS.ACCESS_TOKEN);

        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ALL_GAMES.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(allGamesList(response.data));
                global.allGamesList = response.data && response.data.list || []
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(allGamesListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Get Profile */ }
export const requestGetProfile = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GET_PROFILE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.profile = response.data.item



                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(getProfileRequest(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(getProfileRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Daily Login Rewards Status Check */ }
export const requestDailyLoginRewardsStatusCheck = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_REWARDS_CHECK.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(dailyLoginRewardsStatusCheckRequest(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(dailyLoginRewardsStatusCheckRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const requestFeaturedTournamentDetail = (payload) => {

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_VIA_INVITE_CODE.GET, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(featuredTournamentDetail(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(featuredTournamentDetailFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Screens */ }
export const requestScreens = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.SCREENS_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.screens = response.data.list
                saveDataToLocalStorage(AppConstants.key_screens_data, JSON.stringify(response.data.list))

                dispatch(screensList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(screensListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const syncContacts = (viewModel) => {
    return {
        type: SYNC_CONTACTS,
        model: viewModel
    };
};

export const syncContactsFailed = (viewModel) => {
    return {
        type: SYNC_CONTACTS_FAILED,
        model: viewModel
    };
};

export const requestSyncContacts = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ACCOUNT.SYNC_CONTACTS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                dispatch(syncContacts(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(syncContactsFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const infoPupupList = (viewModel) => {
    return {
        type: INFO_POPUP_LIST,
        model: viewModel
    };
};

export const infoPupupListFailed = (viewModel) => {
    return {
        type: INFO_POPUP_LIST_FAILED,
        model: viewModel
    };
};

export const requestInfoPupupList = (payload) => {
    return (dispatch) => {
        addLog('requestInfoPupupList')
        addLog("URL:::> ", WebFields.INFORMATIONS.LIST);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.INFORMATIONS.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                dispatch(infoPupupList(response.data));

                global.infoPupupList = response.data.list
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(infoPupupListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


// ===============

export const customerCareMessageList = (viewModel) => {
    return {
        type: CUSTOMER_CARE_MESSAGE_LIST,
        model: viewModel
    };
};

export const customerCareMessageListFailed = (viewModel) => {
    return {
        type: CUSTOMER_CARE_MESSAGE_LIST_FAILED,
        model: viewModel
    };
};

export const requestCustomerCareMessageList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.TICKET_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(customerCareMessageList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(customerCareMessageListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const CUSTOMER_CARE_MESSAGE_LIST = 'CUSTOMER_CARE_MESSAGE_LIST'
export const CUSTOMER_CARE_MESSAGE_LIST_FAILED = 'CUSTOMER_CARE_MESSAGE_LIST_FAILED'