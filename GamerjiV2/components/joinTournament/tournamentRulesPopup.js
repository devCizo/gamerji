import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, Dimensions, ScrollView } from 'react-native'
import colors from '../../assets/colors/colors'
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles'
import moment from 'moment';
import { Constants } from '../../appUtils/constants'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds'

const TournamentRulesPopup = (props) => {

    var tournament = props.tournament
    const insets = useSafeAreaInsets();
    const htmlToFormattedText = require("html-to-formatted-text");
    const [mainViewHeight, setMainViewHeight] = useState(undefined);

    let find_dimesions = (layout) => {
        if (mainViewHeight) {
            return
        }
        var { x, y, width, height } = layout;

        if (height >= Constants.SCREEN_HEIGHT) {
            setMainViewHeight(Constants.SCREEN_HEIGHT - 100)
        } else {
            setMainViewHeight(height)
        }

    }

    return (

        /* Main View */
        <View style={{ height: mainViewHeight, backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50, }} onLayout={(event) => { find_dimesions(event.nativeEvent.layout) }} >
            {/* Header View */}
            <View style={styles.headerContainer}>

                <Text style={{ flex: 1, marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} >Rules</Text>

                {/* Close Button */}
                <TouchableOpacity style={styles.closeButton} onPress={() => props.setTournamentRulesOpen(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>

            <ScrollView>
                <Text style={{ marginLeft: 20, marginRight: 20, marginBottom: 20 + insets.bottom, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>{htmlToFormattedText(tournament.rules)}</Text>
            </ScrollView>

            {checkIsSponsorAdsEnabled('rulesPopup') &&
                <SponsorBannerAds screenCode={'rulesPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('rulesPopup')} />
            }
        </View>
    )
}


export default TournamentRulesPopup;

const styles = StyleSheet.create({
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    nextButton: {
        height: 46,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})
