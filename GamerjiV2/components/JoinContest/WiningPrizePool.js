import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, safeAreaBottomHeight } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';


const WiningPrizePool = (props) => {


    const prizePool = props.contest.prizePool //Total prize pool
    const prizepool = props.contest.prizepool //Prize pool array
    const pointpool = props.contest.pointpool //points pool array
    var isPointBasedTournmanet = props.contest.winningTerm && props.contest.winningTerm == 'point base'
    const [mainViewHeight, setMainViewHeight] = useState(212)
    const insets = useSafeAreaInsets();
    const [activeTab, setActiveTab] = useState('prizepool')
    var currency = props.currency || props.contest.currency

    //Initial
    useEffect(() => {
    }, [])

    useEffect(() => {
        var height = 0
        if (activeTab == 'prizepool') {
            height = 212 + (prizepool.length * 50) + insets.bottom
        } else {
            height = 212 + (pointpool.length * 50) + insets.bottom
        }
        if (isPointBasedTournmanet) {
            height = height + 54
        }
        if (height >= Constants.SCREEN_HEIGHT) {
            height = Constants.SCREEN_HEIGHT - 100
        }
        setMainViewHeight(height)

    }, [activeTab])

    const getCurrencyImage = () => {
        return (
            currency && currency.code == 'coin' ?
                <Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                :
                <Text style={{ fontSize: 18, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{currency.symbol}</Text>
        )
    }

    return (
        <View style={{ height: mainViewHeight, backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50, }} >
            {/* Header View */}
            <View style={styles.headerContainer}>

                <Text style={{ marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, flex: 1, textAlign: 'center', }} >Winning Breakup</Text>

                {/* Close Button */}
                <TouchableOpacity style={{ marginRight: 0, height: 60, width: 60, justifyContent: 'center', alignItems: 'center' }} onPress={() => props.closeWiningPrizePool()} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>

            {isPointBasedTournmanet &&
                <View style={{ marginTop: 14, marginLeft: 20, marginRight: 20, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 20 }} >

                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab == 'prizepool' ? colors.black : 'transparent' }} onPress={() => setActiveTab('prizepool')} activeOpacity={0.5} >
                        <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold }} >Prize Pool</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab == 'points' ? colors.black : 'transparent' }} onPress={() => setActiveTab('points')} activeOpacity={0.5} >
                        <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold }} >Points</Text>
                    </TouchableOpacity>
                </View>
            }

            {activeTab == 'prizepool' &&
                <View>
                    {prizePool !== 0 ?
                        <>
                            <Text style={{ marginTop: 16, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, textAlign: 'center' }} >Prize Pool</Text>
                            <View style={{ marginTop: 8, flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }} >
                                {getCurrencyImage()}
                                <Text style={{ marginLeft: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 20 }} >{prizePool}</Text>
                            </View>
                        </>
                        :
                        <Text style={{ marginTop: 16, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, textAlign: 'center' }} >Rewards</Text>
                    }
                </View>
            }

            {activeTab == 'prizepool' ?
                <FlatList
                    style={{ marginTop: 16, flexGrow: 0 }}
                    data={prizepool}
                    renderItem={({ item }) => <WiningPrizePoolItem item={item} currency={currency} />}
                    keyExtractor={(item, index) => index.toString()}
                />
                :
                <FlatList
                    style={{ marginTop: 16, flexGrow: 0 }}
                    data={pointpool}
                    renderItem={({ item }) => <WiningPrizePoolPointsItem item={item} currency={currency} />}
                    keyExtractor={(item, index) => index.toString()}
                />
            }

            {currency.prizePoolNote && currency.prizePoolNote != '' &&
                <Text style={{ marginTop: 8, marginLeft: 8, marginRight: 8, marginBottom: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 11, textAlign: 'center' }} >{currency.prizePoolNote}</Text>
            }

            {checkIsSponsorAdsEnabled('prizePoolPopup') &&
                <SponsorBannerAds screenCode={'prizePoolPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('prizePoolPopup')} />
            }
        </View>
    );
}

export default WiningPrizePool;

const WiningPrizePoolItem = (props) => {

    const prizepool = props.item
    var currency = props.currency

    const isAmount = () => {
        if (prizepool.amount != 0) {
            return prizepool.amount
        }
        return undefined
    }

    const getCurrencyImage = () => {
        return (
            currency && currency.code == 'coin' ?
                <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                :
                <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'center' }} >{currency.symbol}</Text>
        )
    }

    return (
        <View style={{ height: 50 }} >

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 }} >
                <Text
                    style={{ marginLeft: 16, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, textAlign: 'center' }} >Rank : {prizepool.rankFrom}{prizepool.rankTo !== '0' && ' - '}{prizepool.rankTo !== '0' && prizepool.rankTo}
                </Text>

                {isAmount() ?
                    <View style={{ marginRight: 16, flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }} >
                        {getCurrencyImage()}
                        <Text style={{ marginLeft: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'center' }} >{prizepool.amount}</Text>
                    </View>
                    :
                    <View style={{ marginLeft: 16, marginRight: 16, flex: 1, alignItems: 'flex-end', overflow: 'hidden' }}>
                        <CustomMarquee style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'center' }} value={prizepool.prize} />
                    </View>
                }

            </View>

            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DEAB03' }} />
        </View>
    );
}

const WiningPrizePoolPointsItem = (props) => {

    const point = props.item

    return (
        <View style={{ height: 50 }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 }} >
                <Text
                    style={{ marginLeft: 16, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, textAlign: 'center' }} >Rank : {point.rankFrom}{point.rankTo !== '0' && ' - '}{point.rankTo !== '0' && point.rankTo}
                </Text>

                <Text style={{ flex: 1, marginLeft: 20, marginRight: 16, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'right' }} >{point.point}</Text>
            </View>

            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DEAB03' }} />
        </View>
    );
}

const styles = StyleSheet.create({

    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
})