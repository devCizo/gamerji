import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Contest List */ }
export const changeUserName = (viewModel) => {
    return {
        type: CHANGE_USER_NAME,
        model: viewModel
    };
};

{ /* Request Failure - All Games Banner List */ }
export const changeUserNameFailed = (viewModel) => {
    return {
        type: CHANGE_USER_NAME_FAILED,
        model: viewModel
    };
};

{ /* Request - All Games Banner */ }
export const requestChangeUserName = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(changeUserName(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(changeUserNameFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetChangeUserNameState = () => {
    return {
        type: RESET_CHANGE_USER_NAME_STATE,
    };
};


export const CHANGE_USER_NAME = 'CHANGE_USER_NAME';
export const CHANGE_USER_NAME_FAILED = 'CHANGE_USER_NAME_FAILED';
export const RESET_CHANGE_USER_NAME_STATE = 'RESET_CHANGE_USER_NAME_STATE';
