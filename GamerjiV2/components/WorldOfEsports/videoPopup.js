import React, { useEffect, useState, useCallback } from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { showMessage } from 'react-native-flash-message';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { addLog, checkIsSponsorAdsEnabled, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';

import YoutubePlayer from "react-native-youtube-iframe";
import getVideoId from 'get-video-id';

const VideoPopup = (props) => {


    const [playing, setPlaying] = useState(false);

    // This function should set the youtube player video play state
    const onStateChange = useCallback((state) => {

        if (state === "ended") {
            setPlaying(false);
            addLog("Video has finished playing!");
        }
    }, []);




    return (
        <View style={styles.mainContainer}>
            {/* Main View */}
            <View style={styles.mainContainer}>

                {/* Header View */}
                <View style={styles.headerContainer}>



                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() => props.setIsOpenVideoPopup(false)} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Center Content */}
                <View style={styles.centerContentContainer} >


                    {/* Message */}
                    <View style={{ flexDirection: 'column' }} >
                        <YoutubePlayer
                            height={240}
                            play={playing}
                            videoId={getVideoId(props.featuredVideoItem.url).id}
                            onChangeState={onStateChange} />
                    </View>
                    <Text style={{ marginTop: 10, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center' }}>{props.featuredVideoItem.name}</Text>
                </View>

            </View>


        </View >
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.yellow,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        height: 450
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        position: 'absolute', top: 0, right: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1
    },
    centerContentContainer: {
        marginTop: 10,
        alignSelf: 'center'
    },
})

export default VideoPopup;