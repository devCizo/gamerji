import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIEW_ALL_MEDAL_LIST = 'VIEW_ALL_MEDAL_LIST'
export const VIEW_ALL_MEDAL_LIST_FAILED = 'VIEW_ALL_MEDAL_LIST_FAILED'
export const RESET_VIEW_ALL_MEDALS_LIST_STATE = 'RESET_VIEW_ALL_MEDALS_LIST_STATE'

{ /* Request - View All Medal List */ }
export const viewAllMedalList = (viewModel) => {
    return {
        type: VIEW_ALL_MEDAL_LIST,
        model: viewModel
    };
};

{ /* Request Failure - View All Medal List */ }
export const viewAllMedalListFailed = (viewModel) => {
    return {
        type: VIEW_ALL_MEDAL_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - View All Medal List */ }
export const resetViewAllMedalsState = () => {
    return {
        type: RESET_VIEW_ALL_MEDALS_LIST_STATE,
    };
};

{ /* Request - View All Medals */ }
export const requestViewAllMedals = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_MEDAL_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(viewAllMedalList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(viewAllMedalListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};