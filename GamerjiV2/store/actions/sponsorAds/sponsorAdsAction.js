import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const SPONSOR_ADS_LIST = 'SPONSOR_ADS_LIST'
export const SPONSOR_ADS_LIST_FAILED = 'SPONSOR_ADS_LIST_FAILED'
export const RESET_SPONSOR_ADS_LIST_STATE = 'RESET_SPONSOR_ADS_LIST_STATE'

{ /* Request - Sponsor Ads List */ }
export const sponsorAdsList = (viewModel, code) => {
    return {
        type: SPONSOR_ADS_LIST,
        model: viewModel,
        screenCode: code
    };
};

{ /* Request Failure - Sponsor Ads List */ }
export const sponsorAdsListFailed = (viewModel, code) => {
    return {
        type: SPONSOR_ADS_LIST_FAILED,
        model: viewModel,
        screenCode: code
    };
};

{ /* Reset State - Sponsor Ads List */ }
export const resetSponsorAdsState = (code) => {
    return {
        type: RESET_SPONSOR_ADS_LIST_STATE,
        screenCode: code
    };
};

{ /* Request - Sponsor Ads */ }
export const requestSponsorAds = (payload, code) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.SPONSOR_ADS_LIST.MODE, payload)
            .then((response) => {
                // addLog("URL ADS:::> ", response.config.baseURL + response.config.url);
                // addLog("Request ADS:::> ", response.config.data);
                // addLog("Response ADS:::> ", response.data);
                dispatch(sponsorAdsList(response.data, code));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(sponsorAdsListFailed(error.response.data, code));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};