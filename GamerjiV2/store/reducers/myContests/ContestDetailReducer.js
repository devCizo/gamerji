import produce from 'immer';
import * as actionTypes from '../../actions/myContests/ContestDetailAction';

const initialState = {
    model: {
        contestDetailResponse: undefined,
        rateContestResponse: undefined,
    },
    error: false
};

const contestDetail = (state, action) => {
    return produce(state, (draft) => {
        draft.model.contestDetailResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const contestDetailFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.contestDetailResponse = action.model;
    });
};

const rateContest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.rateContestResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const rateContestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.rateContestResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTEST_DETAIL:
            return contestDetail(state, action);
        case actionTypes.CONTEST_DETAIL_FAILED:
            return contestDetailFailed(state, action);
        case actionTypes.RATE_CONTEST:
            return rateContest(state, action);
        case actionTypes.RATE_CONTEST_FAILED:
            return rateContestFailed(state, action);
        case actionTypes.RESET_CONTEST_DETAIL_STATE:
            return resetContestDetailState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetContestDetailState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.contestDetailResponse = undefined;
        draft.model.rateContestResponse = undefined;
    })
}