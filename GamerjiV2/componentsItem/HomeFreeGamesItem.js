import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, fontFamilyStyleNew, positionStyles, otherStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import CustomMarquee from '../appUtils/customMarquee';
import colors from '../assets/colors/colors';
import * as generalSetting from '../webServices/generalSetting'
import LinearGradient from 'react-native-linear-gradient';
import { convertNumberToMillions } from '../appUtils/commonUtlis';

const HomeFreeGamesItem = (props) => {

    // Variable Declarations
    const gameName = props.item && props.item.name || '';
    const gameId = props.item && props.item._id || '';
    const width = (Dimensions.get('window').width - 30) / 3;

    // This function should redirect you to the Game Type Screen 
    const redirectToGameTypeScreen = () => {
        props.navigation.navigate(Constants.nav_html_game_type, { game: props.item });
    }

    return (
        // TouchableOpacity - All Games Click Event
        <TouchableOpacity onPress={redirectToGameTypeScreen} activeOpacity={1}>

            {/* Wrapper - All Games Main View */}
            <View style={[containerStyles.container, borderStyles.borderRadius_5, colorStyles.primaryBackgroundColor, { width: width, marginBottom: 5, height: 120 }, marginStyles.rightMargin_5]}>

                {/* Wrapper - All Games View */}
                <View style={[flexDirectionStyles.column, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_5, alignmentStyles.oveflow, paddingStyles.padding_008]}>

                    {/* Wrapper - Game Name */}
                    <View style={{ alignItems: 'center', width: '100%' }}>

                        {/* Text - Game Name */}
                        <CustomMarquee value={gameName} style={[fontFamilyStyles.bold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_13, { marginTop: 3 }]} />
                    </View>

                    {/* Image - Featured Game Image */}
                    {/* <Image style={[heightStyles.height_90, { width: '100%' }, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.topMargin_4]}
                        source={{ uri: generalSetting.UPLOADED_FILE_URL + (props.item && props.item.featuredImage && props.item.featuredImage.default) }} /> */}

                    <Image style={[heightStyles.height_90, { width: '100%' }, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.topMargin_4]}
                        source={{ uri: "https://www.gamerji.tech/leagueops/uploads/h5games/GH5FI-17092020224528.png" }} />

                    {/* Linear Gradient - Shadow  */}
                    <LinearGradient colors={['#00000000', '#000000']} style={[positionStyles.absolute, heightStyles.height_90, widthStyles.width_100p, borderStyles.borderRadius_5, resizeModeStyles.cover, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, otherStyles.zIndex_1]} >

                        <View style={{ position: 'absolute', bottom: 5, flexDirection: 'row', left: 5, alignItems: 'center' }}>
                            <Image style={{ height: 14, width: 14, resizeMode: 'contain', alignSelf: 'center' }} source={require('../assets/images/ic_joystick_round.png')} />

                            <View style={{ marginLeft: 2, flexDirection: 'row', alignSelf: 'center' }}>

                                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, fontSize: 12 }} numberOfLines={1} ellipsizeMode='tail'>{convertNumberToMillions(props.item.totalJoinedPlayers)}</Text>

                                <Text style={{ marginLeft: 2, fontFamily: fontFamilyStyleNew.regular, color: colors.white, fontSize: 12, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_played}</Text>
                            </View>
                        </View>

                    </LinearGradient>
                </View>
            </View>
        </TouchableOpacity >
    )
}

export default HomeFreeGamesItem;