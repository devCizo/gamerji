import produce from 'immer';
import * as actionTypes from '../../actions/payments/paymentGatewayAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        generateTokenResponse: undefined,
        createTransactionsResponse: undefined,
        checkTransactionsResponse: undefined,
    },
    error: false
};

{ /* Request - Generate Payment Token */ }
const generatePaymentTokenRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.generateTokenResponse = action.model;
    });
};

{ /* Request Failure - Generate Payment Token */ }
const generatePaymentTokenRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.generateTokenResponse = action.model;
    });
};

{ /* Request - Create Transactions */ }
const createTransactionsRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.createTransactionsResponse = action.model;
    });
};

{ /* Request Failure - Create Transactions */ }
const createTransactionsRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.createTransactionsResponse = action.model;
    });
};

{ /* Request - Check Transactions */ }
const checkTransactionsRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.checkTransactionsResponse = action.model;
    });
};

{ /* Request Failure - Check Transactions */ }
const checkTransactionsRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.checkTransactionsResponse = action.model;
    });
};

{ /* Reset State - Payment Gateway */ }
const resetPaymentGatewayState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.generateTokenResponse = undefined;
        draft.model.createTransactionsResponse = undefined;
        draft.model.checkTransactionsResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_GENERATE_PAYMENT_TOKEN:
            return generatePaymentTokenRequest(state, action);
        case actionTypes.REQUEST_GENERATE_PAYMENT_TOKEN_FAILED:
            return generatePaymentTokenRequestFailed(state, action);
        case actionTypes.REQUEST_CREATE_TRANSACTION:
            return createTransactionsRequest(state, action);
        case actionTypes.REQUEST_CREATE_TRANSACTION_FAILED:
            return createTransactionsRequestFailed(state, action);
        case actionTypes.REQUEST_CHECK_TRANSACTIONS:
            return checkTransactionsRequest(state, action);
        case actionTypes.REQUEST_CHECK_TRANSACTIONS_FAILED:
            return checkTransactionsRequestFailed(state, action);
        case actionTypes.RESET_PAYMENT_GATEWAY_STATE:
            return resetPaymentGatewayState(state);
        default:
            return state;
    }
};

export default reducer;