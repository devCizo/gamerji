import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import colors from '../assets/colors/colors'
import { fontFamilyStyleNew } from './commonStyles';
import CustomProgressbar from './customProgressBar';
import CustomMarquee from './customMarquee';
import { showErrorToastMessage } from './commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../store/actions/index';
import { FlatList } from 'react-native-gesture-handler';
import * as generalSetting from '../webServices/generalSetting'
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";

export const flashMessageRef = React.createRef();

const StateListPopup = (props) => {

    var stateList = props.stateList

    const selectState = item => {
        props.setSelectedState(item)
        props.openStateListPicker(false)

        // let message = {
        //     message: "Some message title",
        //     type: "info",
        //     position: "top",
        //     description: "Lorem ipsum dolar sit amet"
        // }

        // showMessage(message);
    }

    return (
        <>
            <View style={{ height: 400, backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

                {/* Close Button */}
                <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                    <Text style={{ flex: 1, marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} >Select State</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.openStateListPicker(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, flex: 1 }}
                    data={stateList}
                    renderItem={({ item, index }) => <StateListPopupItem item={item} selectState={selectState} />}
                    showsVerticalScrollIndicator={false}
                />

            </View>

            <FlashMessage ref={flashMessageRef} />
        </>
    )
}

const StateListPopupItem = (props) => {

    let item = props.item

    return (
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.5} onPress={() => props.selectState(item)} >

            <Text style={{ marginLeft: 12, color: colors.black, fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold, }} >{item.name}</Text>
            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#D5D7E3' }} />

        </TouchableOpacity>
    )
}

export default StateListPopup