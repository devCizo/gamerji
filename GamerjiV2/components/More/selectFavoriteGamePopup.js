import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Image, Text, TouchableOpacity, StyleSheet, FlatList, Dimensions, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { alignmentStyles, borderStyles, colorStyles, flexDirectionStyles, fontFamilyStyleNew, fontFamilyStyles, fontSizeStyles, heightStyles, marginStyles, otherStyles, positionStyles, shadowStyles } from '../../appUtils/commonStyles';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import colors from '../../assets/colors/colors';
import * as generalSetting from '../../webServices/generalSetting'
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const SelectFavoriteGamePopup = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [allGames, setAllGames] = useState(global.allGamesList);
    const [gameIdArr, setGameIdArr] = useState('')
    const [isLoading, setLoading] = useState(false);
    var arrIds = [];

    // This useEffect function should binds the "selected" id for the Selected Favorite Games and binds the array 
    useEffect(() => {
        var tempArr = []

        for (let index = 0; index < allGames.length; index++) {
            var element = { ...allGames[index] }


            if (global.profile && global.profile.favoriteGames) {
                let index = global.profile.favoriteGames.findIndex(item => {
                    if (item == element._id) {
                        return true
                    }
                })

                if (index < 0) {
                    element.selected = false
                } else {
                    element.selected = true
                }
            }

            tempArr.push(element)
        }
        setAllGames(tempArr)
    }, [])

    // This function should sets the Selected Favorite Games to the arrIds
    const selectFavoriteGames = (index) => {
        var tempArr = [...allGames]
        tempArr[index].selected = !tempArr[index].selected
        setAllGames(tempArr)
        // setGameId(tempArr[index]._id)

        tempArr.forEach(element => {
            if (element.selected == true) {
                arrIds.push(element._id)
                setGameIdArr(arrIds)
            }
        });
    }

    // This function should check the validation first and then call the Update Favorite Games API
    const checkValidation = () => {
        addLog('Selected Games:::> ', gameIdArr.length)

        if (gameIdArr.length < 1) {
            showErrorToastMessage(Constants.error_select_one_game)
            return;
        }
        callToUpdateFavoriteGamesAPI();
    };

    // This function should call the Update Favorite Games API
    const callToUpdateFavoriteGamesAPI = () => {
        let payload = {
            favoriteGames: gameIdArr
        }
        addLog('payload:::> ', payload)
        setLoading(true)
        dispatch(actionCreators.requestUpdateFavoriteGamesPopup(payload))
    }

    // This function sets the Update Email Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.updateFavoriteGamesPopup.model }),
        shallowEqual
    );
    var { updateFavoriteGamesResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (updateFavoriteGamesResponse) {
            setLoading(false)
            showSuccessToastMessage(Constants.success_favorite_games_updated)
            props.openSelectFavoriteGamePopup();

            return () => {
                dispatch(actionCreators.resetUpdateFavoriteGamesPopupState())
            }
        }
        updateFavoriteGamesResponse = undefined
    }, [updateFavoriteGamesResponse])

    return (
        <View style={{ height: Constants.SCREEN_HEIGHT - 100, backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50 }}>

            {/* Header View */}

            {/* Close Button */}
            <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1 }} onPress={() => props.openSelectFavoriteGamePopup(false)} activeOpacity={0.5}>
                <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
            </TouchableOpacity>

            {/* Center Content */}
            <View style={{ marginTop: 44, flex: 1 }}>

                <Text style={{ marginLeft: 20, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, fontSize: 16 }}>{Constants.text_select_your}</Text>

                <Text style={{ marginTop: 5, marginLeft: 20, fontFamily: fontFamilyStyleNew.extraBold, color: colors.red, fontSize: 24 }}>{Constants.text_favorite_game}</Text>


                <FlatList
                    style={{ marginTop: 16, marginLeft: 20, marginRight: 20, flex: 1, }}
                    data={allGames}
                    renderItem={({ item, index }) => <SelectFavoriteGameItem item={item} index={index} selectFavoriteGames={selectFavoriteGames} />}
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={2}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false} />

                <TouchableOpacity style={[heightStyles.height_46, flexDirectionStyles.row, colorStyles.redBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, marginStyles.topMargin_20, { marginLeft: 16, marginRight: 16, marginBottom: 20 }]}
                    onPress={() => checkValidation()} activeOpacity={0.5}>

                    <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                    <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                </TouchableOpacity>

                {checkIsSponsorAdsEnabled('selectFavoriteGamePopup') &&
                    <SponsorBannerAds screenCode={'selectFavoriteGamePopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('selectFavoriteGamePopup')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}
        </View >
    );
}

const SelectFavoriteGameItem = (props) => {

    // Variable Declarations
    var item = props.item
    var isSelected = item.selected && item.selected ? true : false
    const width = (Dimensions.get('window').width - 65) / 2;

    return (
        <TouchableOpacity style={{ height: 97, marginRight: 10, marginBottom: 15, borderRadius: 5, borderWidth: 3, borderColor: isSelected ? colors.green : colors.black }} onPress={() => props.selectFavoriteGames(props.index)} activeOpacity={0.5}>

            <Image style={{ flex: 1, width: width, resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + (props.item && props.item.featuredImage && props.item.featuredImage.default) }} />

            {isSelected &&
                <Image style={{ position: 'absolute', top: 5, right: 7, height: 16, width: 16 }} source={require('../../assets/images/ic_check_red.png')} />
            }

            <LinearGradient colors={['#00000000', '#000000']} style={{ position: 'absolute', height: 70, width: '100%', bottom: 0, zIndex: 1 }} >

                <View style={{ margin: 10, alignSelf: 'center', top: 35 }}>

                    <CustomMarquee value={props.item.name} style={{ fontFamily: fontFamilyStyleNew.bold, color: colors.white, fontSize: 15, marginLeft: 10, marginRight: 10 }} />
                </View>
            </LinearGradient>
        </TouchableOpacity>
    )
}

export default SelectFavoriteGamePopup;