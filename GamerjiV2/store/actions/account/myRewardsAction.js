import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';


export const getMyRewardList = (viewModel) => {
    return {
        type: MY_REWARD_LIST,
        model: viewModel
    };
};

export const getMyRewardListFailed = (viewModel) => {
    return {
        type: MY_REWARD_LIST_FAILED,
        model: viewModel
    };
};

export const requestMyRewardList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.REWARD.MY_REWARD);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.REWARD.MY_REWARD, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getMyRewardList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getMyRewardListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetMyRewardsState = () => {
    return {
        type: RESET_MY_REWARDS_STATE,
    };
};

export const MY_REWARD_LIST = 'MY_REWARD_LIST';
export const MY_REWARD_LIST_FAILED = 'MY_REWARD_LIST_FAILED';
export const RESET_MY_REWARDS_STATE = 'RESET_MY_REWARDS_STATE';