import produce from 'immer';
import * as actionTypes from '../../actions/tabAllGames/updateFavoriteGamesPopupAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        updateFavoriteGamesResponse: undefined
    },
    error: false
};

{ /* Request - Update Favorite Games Popup */ }
const updateFavoriteGamesPopupRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.updateFavoriteGamesResponse = action.model;
    });
};

{ /* Request Failure - Update Favorite Games Popup */ }
const updateFavoriteGamesPopupRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.updateFavoriteGamesResponse = action.model;
    });
};

{ /* Reset State - Update Favorite Games Popup */ }
const resetUpdateFavoriteGamesPopupState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.updateFavoriteGamesResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_UPDATE_FAVORITE_GAMES:
            return updateFavoriteGamesPopupRequest(state, action);
        case actionTypes.REQUEST_UPDATE_FAVORITE_GAMES_FAILED:
            return updateFavoriteGamesPopupRequestFailed(state, action);
        case actionTypes.RESET_UPDATE_FAVORITE_GAMES_STATE:
            return resetUpdateFavoriteGamesPopupState(state);
        default:
            return state;
    }
};

export default reducer;