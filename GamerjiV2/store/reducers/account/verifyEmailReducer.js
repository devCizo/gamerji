import produce from 'immer';
import * as actionTypes from '../../actions/account/verifyEmailAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        verifyEmailResponse: undefined
    }
};

{ /* Request - Verify Email OTP */ }
const verifyEmailOTPRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.verifyEmailResponse = action.model;
    });
};

{ /* Request Failure - Verify Email OTP */ }
const verifyEmailOTPRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.verifyEmailResponse = action.model;
    });
};

{ /* Validate Failure - Verify Email Validate OTP */ }
const verifyEmailValidateOTPRequestFailed = (state) => {
    return produce(state, (draft) => {
        draft.verifyEmailResponse = action.model;
        draft.error = false;
    });
};

{ /* Reset State - Verify Email */ }
const resetVerifyEmailState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.verifyEmailResponse = undefined;
    })
}

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VERIFY_EMAIL_REQUEST_OTP:
            return verifyEmailOTPRequest(state, action);
        case actionTypes.VERIFY_EMAIL_REQUEST_OTP_FAILED:
            return verifyEmailOTPRequestFailed(state, action);
        case actionTypes.VERIFY_EMAIL_VALIDATE_OTP_FAILED:
            return verifyEmailValidateOTPRequestFailed(state, action);
        case actionTypes.RESET_VERIFY_EMAIL_STATE:
            return resetVerifyEmailState(state);
        default:
            return state;
    }
};

export default reducer;