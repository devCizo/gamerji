import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const complaintCategory = (viewModel) => {
    return {
        type: COMPLAINT_CATEGORY,
        model: viewModel
    };
};

export const complaintCategoryFailed = (viewModel) => {
    return {
        type: COMPLAINT_CATEGORY_FAILED,
        model: viewModel
    };
};

export const requestComplaintCategory = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.CATEGORY, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(complaintCategory(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(complaintCategoryFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const COMPLAINT_CATEGORY = 'COMPLAINT_CATEGORY';
export const COMPLAINT_CATEGORY_FAILED = 'COMPLAINT_CATEGORY_FAILED';



export const complaintSubCategory = (viewModel) => {
    return {
        type: COMPLAINT_SUB_CATEGORY,
        model: viewModel
    };
};

export const complaintSubCategoryFailed = (viewModel) => {
    return {
        type: COMPLAINT_SUB_CATEGORY_FAILED,
        model: viewModel
    };
};

export const requestComplaintSubCategory = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.CATEGORY, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(complaintSubCategory(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(complaintSubCategoryFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const COMPLAINT_SUB_CATEGORY = 'COMPLAINT_SUB_CATEGORY';
export const COMPLAINT_SUB_CATEGORY_FAILED = 'COMPLAINT_SUB_CATEGORY_FAILED';



export const gameList = (viewModel) => {
    return {
        type: GAME_LIST,
        model: viewModel
    };
};

export const gameListFailed = (viewModel) => {
    return {
        type: GAME_LIST_FAILED,
        model: viewModel
    };
};

export const requestGameList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.GAME, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gameList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(gameListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const GAME_LIST = 'GAME_LIST';
export const GAME_LIST_FAILED = 'GAME_LIST_FAILED';



export const gameTypeList = (viewModel) => {
    return {
        type: GAME_TYPE_LIST,
        model: viewModel
    };
};

export const gameTypeListFailed = (viewModel) => {
    return {
        type: GAME_TYPE_LIST_FAILED,
        model: viewModel
    };
};

export const requestGameTypeList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.GAME_TYPE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gameTypeList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(gameTypeListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const GAME_TYPE_LIST = 'GAME_TYPE_LIST';
export const GAME_TYPE_LIST_FAILED = 'GAME_TYPE_LIST_FAILED';


export const raiseComplaint = (viewModel) => {
    return {
        type: RAISE_COMPLAINT,
        model: viewModel
    };
};

export const raiseComplaintFailed = (viewModel) => {
    return {
        type: RAISE_COMPLAINT_FAILED,
        model: viewModel
    };
};

export const requestRaiseComplaint = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.CUSTOMER_CARE.CREATE_TICKET)
        addLog("payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.CREATE_TICKET, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(raiseComplaint(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(raiseComplaintFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const RAISE_COMPLAINT = 'RAISE_COMPLAINT';
export const RAISE_COMPLAINT_FAILED = 'RAISE_COMPLAINT_FAILED';



export const uploadRaiseComplaintImage = (viewModel) => {
    return {
        type: UPLOAD_RAISE_COMPLAINT_IMAGE,
        model: viewModel
    };
};

export const uploadRaiseComplaintImageFailed = (viewModel) => {
    return {
        type: UPLOAD_RAISE_COMPLAINT_IMAGE_FAILED,
        model: viewModel
    };
};

export const requestUploadRaiseComplaintImage = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.CUSTOMER_CARE.UPLOAD_IMAGE)
        addLog("payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.UPLOAD_IMAGE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(uploadRaiseComplaintImage(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(uploadRaiseComplaintImageFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const UPLOAD_RAISE_COMPLAINT_IMAGE = 'UPLOAD_RAISE_COMPLAINT_IMAGE';
export const UPLOAD_RAISE_COMPLAINT_IMAGE_FAILED = 'UPLOAD_RAISE_COMPLAINT_IMAGE_FAILED';

export const resetRaiseComplaintState = () => {
    return {
        type: RESET_RAISE_COMPLAINT_STATE,
    };
};
export const RESET_RAISE_COMPLAINT_STATE = 'RESET_RAISE_COMPLAINT_STATE';