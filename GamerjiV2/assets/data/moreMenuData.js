import React from 'react'
import { Constants } from '../../appUtils/constants';

const moreMenuData = [
    {
        id: '3',
        image: require('../../assets/images/invitation.png'),
        title: Constants.nav_more_join_via_invite_code
    },
    {
        id: '1',
        image: require('../../assets/images/ic_nav_more_leaderboard.png'),
        title: Constants.nav_more_leaderboard
    },
    {
        id: '13',
        image: require('../../assets/images/ic_nav_more_refer_a_friend.png'),
        title: Constants.nav_more_refer_a_friend
    },
    {
        id: '2',
        image: require('../../assets/images/live-streaming.png'),
        title: Constants.nav_more_stream_on_gamerji
    },
    {
        id: '12',
        image: require('../../assets/images/ic_rewards_deposit.png'),
        title: Constants.nav_daily_login_rewards
    },
    {
        id: '5',
        image: require('../../assets/images/ic_nav_more_apply_promo_code.png'),
        title: Constants.nav_more_apply_promo_code
    },
    {
        id: '6',
        image: require('../../assets/images/ic_nav_more_how_to_play.png'),
        title: Constants.nav_more_how_to_play
    },
    {
        id: '7',
        image: require('../../assets/images/ic_nav_more_gamerji_points.png'),
        title: Constants.nav_more_gamerji_points
    },
    {
        id: '8',
        image: require('../../assets/images/ic_nav_more_customer_care.png'),
        title: Constants.nav_more_customer_care
    },

    {
        id: '10',
        image: require('../../assets/images/ic_nav_more_legality.png'),
        title: Constants.nav_more_legality
    },
    // {
    //     id: '11',
    //     image: require('../../assets/images/videos.png'),
    //     title: Constants.nav_more_videos
    // },

    {
        id: '9',
        image: require('../../assets/images/how_to_join.png'),
        title: 'Choose your Favourite Game'
    },

];

export default moreMenuData;