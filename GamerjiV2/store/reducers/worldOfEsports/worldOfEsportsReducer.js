import produce from 'immer';
import * as actionTypes from '../../actions/worldOfEsports/worldOfEsportsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        featuredVideoListResponse: undefined,
        liveFeaturedVideoListResponse: undefined,
        blogsListResponse: undefined,
        topProfileListResponse: undefined
    },
    error: false
};

{ /* Request - Featured Video List */ }
const featuredVideoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.featuredVideoListResponse = action.model;
    });
};

{ /* Request Failure - Featured Video List */ }
const featuredVideoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.featuredVideoListResponse = action.model;
    });
};

{ /* Request - Featured Video List */ }
const liveFeaturedVideoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.liveFeaturedVideoListResponse = action.model;
    });
};

{ /* Request Failure - Featured Video List */ }
const liveFeaturedVideoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.liveFeaturedVideoListResponse = action.model;
    });
};

{ /* Request - Blogs List */ }
const blogsList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.blogsListResponse = action.model;
    });
};

{ /* Request Failure - Blogs List */ }
const blogsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.blogsListResponse = action.model;
    });
};

{ /* Request - Top Profile List */ }
const topProfileList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.topProfileListResponse = action.model;
    });
};

{ /* Request Failure - Top Profile List */ }
const topProfileListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.topProfileListResponse = action.model;
    });
};

{ /* Reset State - World Of Esports */ }
const resetWorldOfEsportsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.featuredVideoListResponse = undefined;
        draft.model.blogsListResponse = undefined;
        draft.model.topProfileListResponse = undefined;
        draft.model.liveFeaturedVideoListResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FEATURED_VIDEO_LIST:
            return featuredVideoList(state, action);
        case actionTypes.FEATURED_VIDEO_LIST_FAILED:
            return featuredVideoListFailed(state, action);
        case actionTypes.LIVE_FEATURED_VIDEO_LIST:
            return liveFeaturedVideoList(state, action);
        case actionTypes.LIVE_FEATURED_VIDEO_LIST_FAILED:
            return liveFeaturedVideoListFailed(state, action);
        case actionTypes.BLOGS_LIST:
            return blogsList(state, action);
        case actionTypes.BLOGS_LIST_LIST_FAILED:
            return blogsListFailed(state, action);
        case actionTypes.TOP_PROFILE_LIST:
            return topProfileList(state, action);
        case actionTypes.TOP_PROFILE_LIST_FAILED:
            return topProfileListFailed(state, action);
        case actionTypes.RESET_WORLD_OF_ESPORTS_STATE:
            return resetWorldOfEsportsState(state);
        default:
            return state;
    }
};

export default reducer;