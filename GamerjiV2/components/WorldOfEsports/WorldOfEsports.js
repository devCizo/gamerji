import React, { useState, useCallback, useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, ScrollView, SafeAreaView, Image, FlatList, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, widthStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import TopProfilesItem from '../../componentsItem/TopProfilesItem';
import EsportsNewsItem from '../../componentsItem/EsportsNewsItem';
import FeaturedVideosItem from '../../componentsItem/FeaturedVideosItem';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import LiveVideosItem from '../../componentsItem/LiveVideosItem';
import FlatListIndicatorItem from '../../componentsItem/FlatListIndicatorItem';
import SponsorBannerAds from "../../commonComponents/SponsorBannerAds";
import { addLog, checkIsSponsorAdsEnabled } from "../../appUtils/commonUtlis";
import YoutubePlayer from "react-native-youtube-iframe";
import CommonInfoPopup from '../../commonComponents/commonInfoPopup';
import VideoPopup from '../WorldOfEsports/videoPopup';


const WorldOfEsports = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(true);
    const [featuredVideos, setFeaturedVideos] = useState(undefined);
    const [liveFeaturedVideos, setLiveFeaturedVideos] = useState(undefined);
    const [totalFeaturedVideosCount, setTotalFeaturedVideosCount] = useState(false);
    const [totalRecordsFeaturedVideos, setTotalRecordsFeaturedVideos] = useState(false);
    const [blogs, setBlogs] = useState(undefined);
    const [totalBlogsCount, setTotalBlogsCount] = useState(false);
    const [totalRecordsBlogs, setTotalRecordsBlogs] = useState(false);
    const [topProfiles, setTopProfiles] = useState(undefined);
    const [totalTopProfilesCount, setTotalTopProfilesCount] = useState(false);
    const [totalRecordsTopProfiles, setTotalRecordsTopProfiles] = useState(false);
    const height = Dimensions.get('window').height;
    const [featuredVideosFlatListIndex, setFeaturedVideosFlatListIndex] = useState(0)
    const [isOpenCommonInfoPopup, setOpenCommonInfoPopup] = useState(false)
    const [isOpenFeaturedVideoPopup, setOpenFeaturedVideoPopup] = useState(false)
    const [featuredVideoId, setFeaturedVideoId] = useState(undefined);
    const [playing, setPlaying] = useState(false);
    const [featuredVideoItem, setFeaturedVideoItem] = useState(undefined);


    var indexFlatListFeaturedVideos = 0
    var featuredVideosCount = 0
    var refFlatlistFeaturedVideos = null
    var refFlatlistIndicator = null
    const onStateChange = useCallback((state) => {
        if (state === "ended") {
            setPlaying(false);
            addLog("Video has finished playing!");
        }
    }, []);
    // This Use-Effect function should call the Featured Videos, Blogs & Top Profiles API
    useEffect(() => {
        // Featured Videos API Params
        var payload = {
            skip: 0,
            limit: 5,
            filter: {
                isLive: false
            }
        }
        dispatch(actionCreators.requestFeaturedVideos(payload))

        // Live Featured Videos API Params
        var payload = {
            skip: 0,
            limit: 5,
            filter: {
                isLive: false
            }
        }
        dispatch(actionCreators.requestLiveFeaturedVideos(payload))

        // Blogs API Params
        payload = {
            skip: 0,
            limit: 5,
            sortBy: 'createdAt',
            sort: 'desc'
        }
        dispatch(actionCreators.requestBlogs(payload))

        // Top Profiles API Params
        payload = {
            skip: 0,
            limit: 5,
            type: ["appUser"],
            sort: 'desc',
            sortBy: 'followers',
            action: "topProfile"
        }
        setLoading(true)
        dispatch(actionCreators.requestTopProfile(payload))

        return () => {
            dispatch(actionCreators.resetWorldOfEsportsState())
        }
    }, [])

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.worldOfEsports.model }),
        shallowEqual
    );
    var { featuredVideoListResponse, blogsListResponse, topProfileListResponse, liveFeaturedVideoListResponse } = currentState

    // This function should return the Featured Videos API Response
    useEffect(() => {
        if (featuredVideoListResponse && featuredVideoListResponse.list) {
            setLoading(false)
            setTotalFeaturedVideosCount(featuredVideoListResponse.count)

            if (featuredVideoListResponse.count == 0) {
                setTotalRecordsFeaturedVideos(true)
            }

            // var imageArr = [];
            // featuredVideoListResponse.list.forEach(element => {
            //     imageArr.push(generalSetting.UPLOADED_FILE_URL + element.img.default)
            // });
            featuredVideosCount = featuredVideoListResponse.list.length
            setFeaturedVideos(featuredVideoListResponse.list);

            setInterval(() => {
                setAutoScrollBanner()
            }, 10000)
        }
        featuredVideoListResponse = undefined
    }, [featuredVideoListResponse])


    // This function should return the Featured Videos API Response
    useEffect(() => {
        if (liveFeaturedVideoListResponse && liveFeaturedVideoListResponse.list) {
            setLoading(false)
            setTotalFeaturedVideosCount(liveFeaturedVideoListResponse.count)

            if (liveFeaturedVideoListResponse.count == 0) {
                setTotalRecordsFeaturedVideos(true)
            }

            // var imageArr = [];
            // featuredVideoListResponse.list.forEach(element => {
            //     imageArr.push(generalSetting.UPLOADED_FILE_URL + element.img.default)
            // });
            featuredVideosCount = liveFeaturedVideoListResponse.list.length
            setLiveFeaturedVideos(liveFeaturedVideoListResponse.list);

            setInterval(() => {
                setAutoScrollBanner()
            }, 10000)
        }
        liveFeaturedVideoListResponse = undefined
    }, [liveFeaturedVideoListResponse])

    // This function should return the Blogs API Response
    useEffect(() => {
        if (blogsListResponse && blogsListResponse.list) {
            setLoading(false)
            setBlogs(blogsListResponse.list)
            setTotalBlogsCount(blogsListResponse.count)

            if (blogsListResponse.count == 0) {
                setTotalRecordsBlogs(true)
            }
        }
        blogsListResponse = undefined
    }, [blogsListResponse])

    // This function should return the Top Profile API Response
    useEffect(() => {
        addLog("topProfileListResponse===>", topProfileListResponse);
        if (topProfileListResponse && topProfileListResponse.list) {
            setLoading(false)
            setTopProfiles(topProfileListResponse.list)
            setTotalTopProfilesCount(topProfileListResponse.count)

            if (topProfileListResponse.count == 0) {
                setTotalRecordsTopProfiles(true)
            }
        }
        topProfileListResponse = undefined
    }, [topProfileListResponse])

    let onBannersFlatListScrollEnd = (e) => {
        let pageNumber = Math.min(Math.max(Math.floor(e.nativeEvent.contentOffset.x / (Constants.SCREEN_WIDTH - 40) + 0.5), 0), featuredVideos.length);
        indexFlatListFeaturedVideos = pageNumber
        setFeaturedVideosFlatListIndex(pageNumber)
    }

    const openFeaturedVideoPopup = (item) => {

        setFeaturedVideoItem(item)
        setOpenFeaturedVideoPopup(true)
        // addLog("isOpenFeaturedVideoPopup==>", isOpenFeaturedVideoPopup);
        // addLog("featuredVideoId==>", featuredVideoId);
    }
    const setAutoScrollBanner = () => {
        if (indexFlatListFeaturedVideos >= featuredVideosCount) {
            indexFlatListFeaturedVideos = 0
        }
        indexFlatListFeaturedVideos = indexFlatListFeaturedVideos + 1
        setFeaturedVideosFlatListIndex(indexFlatListFeaturedVideos)
    }
    // This function should open up the popup for the winnings information
    let openBonusInfoPopup = (visible, bonusType) => {
        setBonusInfo(bonusType);
        setIsOpenBonusInfoPopup(visible)
    }

    useEffect(() => {
        if (featuredVideos) {
            if (featuredVideos.length == 0) {
                return
            }
            if (featuredVideosFlatListIndex >= featuredVideos.length) {
                setFeaturedVideosFlatListIndex(0)
                return
            }
            if (refFlatlistFeaturedVideos) {
                refFlatlistFeaturedVideos.scrollToIndex({ animated: true, index: featuredVideosFlatListIndex })
            }
        }
    }, [featuredVideosFlatListIndex])

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Top View */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* Text - World Of Esports Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_world_of_esports} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderRadius_40, colorStyles.whiteBackgroundColor, marginStyles.bottomMargin_10, alignmentStyles.oveflow]}>

                    <ScrollView style={[containerStyles.container, colorStyles.redBackgroundColor]} keyboardShouldPersistTaps='handled'
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        bounces={false}
                    >

                        <View>


                            {/* Wrapper - Live Videos Header */}
                            <View style={[colorStyles.redBackgroundColor]}>

                                {/* Text - Live Videos */}
                                <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.semiBold, colorStyles.whiteColor, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_18, marginStyles.topMargin_10]}> {Constants.text_featured_videos} </Text>
                            </View>

                            {/* Wrapper - Featured Videos */}
                            <View style={[positionStyles.absolute, colorStyles.yellowBackgroundColor, borderStyles.borderRadius_40, { height: height - 450, top: 80, left: 0, right: 0, marginTop: 50 }]}>

                                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 120 }}>

                                    {/* Text - Featured Videos */}
                                    <Text style={[colorStyles.blackColor, alignmentStyles.alignTextCenter, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_18]}> {Constants.text_popular_videos} </Text>


                                    <View style={{ flexDirection: 'column', position: 'absolute', right: 0, marginRight: 20 }}>

                                        <TouchableOpacity onPress={() => props.navigation.navigate(Constants.nav_view_all_featured_videos, { youTubeChannelLink: props.youTubeChannelLink })} activeOpacity={0.5}>

                                            <Text style={[fontFamilyStyles.bold, colorStyles.redColor, fontSizeStyles.fontSize_12]}> {Constants.text_view_all}  </Text>

                                            <Text style={[heightStyles.height_2, widthStyles.width_45, marginStyles.topMargin_2, colorStyles.redBackgroundColor, alignmentStyles.alignSelfCenter]} />
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            </View>

                            {/* Wrapper - Live Videos */}
                            <View style={[positionStyles.absolute, heightStyles.height_170, borderStyles.borderRadius_5, colorStyles.yellowBackgroundColor, alignmentStyles.alignSelfCenter, marginStyles.topMargin_40, marginStyles.leftMargin_20, marginStyles.rightMargin_20]}>

                                <View style={[{ height: 180 }, marginStyles.margin_10]}>

                                    {/* Flatlist - Live Videos */}
                                    <FlatList
                                        // ref={(ref) => refFlatlistFeaturedVideos = ref}
                                        data={liveFeaturedVideos}
                                        renderItem={({ index, item }) => <LiveVideosItem item={item} index={index} />}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        pagingEnabled={true}
                                        onMomentumScrollEnd={onBannersFlatListScrollEnd}
                                    />

                                    {/* Flatlist Indicator - Live Videos */}
                                    {/* <FlatList
                                        ref={(ref) => refFlatlistIndicator = ref}
                                        style={{ marginTop: 10, height: 10, alignSelf: 'center' }}
                                        data={liveFeaturedVideos}
                                        renderItem={({ index, item }) => <FlatListIndicatorItem item={item} selectedIndex={featuredVideosFlatListIndex} currentIndex={index} />}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                    /> */}
                                </View>
                            </View>

                            {/* Wrapper - Featured Videos */}
                            <View style={[widthStyles.width_100p, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, { height: 560, borderTopLeftRadius: 40, borderTopRightRadius: 40, marginTop: 280 }]}>

                                <View style={[flexDirectionStyles.column]}>

                                    {!totalRecordsFeaturedVideos ?
                                        <FlatList style={{ marginTop: 11, top: -40, marginBottom: -10, marginLeft: 20 }}
                                            data={featuredVideos}
                                            renderItem={({ item, index }) => <FeaturedVideosItem setFeaturedVideoId={setFeaturedVideoId} openFeaturedVideoPopup={openFeaturedVideoPopup} item={item} index={index} />}
                                            keyExtractor={(item) => item.id}
                                            horizontal={true}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                        :
                                        <View style={{ height: 50, marginTop: 11, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>
                                        </View>
                                    }

                                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                        <Text style={[colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_18]}> {Constants.text_top_profiles} </Text>

                                        {totalTopProfilesCount >= 3 &&
                                            <View style={{ flexDirection: 'column', position: 'absolute', right: 0, marginRight: 20 }}>

                                                <TouchableOpacity onPress={() => props.navigation.navigate(Constants.nav_view_all_top_profiles)} activeOpacity={0.5}>

                                                    <Text style={[fontFamilyStyles.bold, colorStyles.redColor, fontSizeStyles.fontSize_12]}> {Constants.text_view_all} </Text>

                                                    <Text style={[heightStyles.height_2, widthStyles.width_45, marginStyles.topMargin_2, colorStyles.redBackgroundColor, alignmentStyles.alignSelfCenter]} />
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    </View>

                                    {!totalRecordsTopProfiles ?
                                        <FlatList style={{ marginTop: 11, marginLeft: 20 }}
                                            data={topProfiles}
                                            renderItem={({ item, index }) => <TopProfilesItem item={item} />}
                                            keyExtractor={(item) => item.id}
                                            horizontal={true}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                        :
                                        <View style={{ height: 50, marginTop: 11, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>
                                        </View>
                                    }

                                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 30 }}>

                                        {/* Text - eSports News */}
                                        <Text style={[colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_18]}> {Constants.text_esports_news} </Text>

                                        {totalBlogsCount >= 3 &&
                                            <View style={{ flexDirection: 'column', position: 'absolute', right: 0, marginRight: 20 }}>

                                                <TouchableOpacity onPress={() => props.navigation.navigate(Constants.nav_view_all_esports_news)} activeOpacity={0.5}>

                                                    <Text style={[fontFamilyStyles.bold, colorStyles.redColor, fontSizeStyles.fontSize_12]}> {Constants.text_view_all} </Text>

                                                    <Text style={[heightStyles.height_2, widthStyles.width_45, marginStyles.topMargin_2, colorStyles.redBackgroundColor, alignmentStyles.alignSelfCenter]} />
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    </View>

                                    {!totalRecordsBlogs ?
                                        <FlatList style={{ marginTop: 20, marginLeft: 20 }}
                                            data={blogs}
                                            renderItem={({ item, index }) => <EsportsNewsItem item={item} index={index} />}
                                            keyExtractor={(item) => item.id}
                                            horizontal={true}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                        :
                                        <View style={{ height: 50, marginTop: 11, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>
                                        </View>
                                    }
                                </View>
                            </View>
                        </View>

                    </ScrollView>

                    {checkIsSponsorAdsEnabled('worldOfEsports') &&
                        <View style={{ marginLeft: 20, marginRight: 20, marginBottom: 30 }}>
                            <SponsorBannerAds screenCode={'worldOfEsports'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('worldOfEsports')} />
                        </View>
                    }

                </View>

            </View>
            {isOpenFeaturedVideoPopup &&
                <Modal
                    isVisible={isOpenFeaturedVideoPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <VideoPopup featuredVideoItem={featuredVideoItem} setIsOpenVideoPopup={setOpenFeaturedVideoPopup} />
                    {/* <View style={{
                        backgroundColor: colors.yellow,
                        borderTopStartRadius: 50,
                        borderTopEndRadius: 50,
                    }}>

                        <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1 }} onPress={() => setOpenFeaturedVideoPopup(false)} activeOpacity={0.5} >
                            <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'flex-end', alignItems: "center", marginTop: 60, marginLeft: 5, marginRight: 5, backgroundColor: colors.yellow }}>
                            {featuredVideoId ?
                                <YoutubePlayer
                                    height={250}
                                    width={350}
                                    play={playing}
                                    videoId={featuredVideoId}
                                    onChangeState={onStateChange} />
                                : <Text> </Text>
                            }
                        </View>
                    </View> */}
                </Modal>
            }

            {isLoading && <CustomProgressbar />}
        </SafeAreaView >
    )
}

export default WorldOfEsports;