import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_APPLY_PROMO_CODE = 'REQUEST_APPLY_PROMO_CODE';
export const REQUEST_APPLY_PROMO_CODE_FAILED = 'REQUEST_APPLY_PROMO_CODE_FAILED';
export const RESET_APPLY_PROMO_CODE_STATE = 'RESET_APPLY_PROMO_CODE_STATE';

{ /* Request - Apply Promo Code */ }
export const applyPromoCodeRequest = (viewModel) => {
    return {
        type: REQUEST_APPLY_PROMO_CODE,
        model: viewModel
    };
};

{ /* Request Failure - Apply Promo Code */ }
export const applyPromoCodeRequestFailed = (viewModel) => {
    return {
        type: REQUEST_APPLY_PROMO_CODE_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Apply Promo Code */ }
export const resetApplyPromoCodeState = () => {
    return {
        type: RESET_APPLY_PROMO_CODE_STATE,
    };
};

{ /* Request - Apply Promo Code */ }
export const requestApplyPromoCode = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.APPLY_PROMO_CODE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(applyPromoCodeRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(applyPromoCodeRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};