import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const myContestList = (viewModel) => {
    return {
        type: MY_CONTEST_LIST,
        model: viewModel
    };
};

export const myContestListFailed = (viewModel) => {
    return {
        type: MY_CONTEST_LIST_FAILED,
        model: viewModel
    };
};

export const requestMyContestList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.MY_CONTEST_TOURNAMENT_LIST.CONTEST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(myContestList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(myContestListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const myTournamentList = (viewModel) => {
    return {
        type: MY_TOURNAMENT_LIST,
        model: viewModel
    };
};

export const myTournamentListFailed = (viewModel) => {
    return {
        type: MY_TOURNAMENT_LIST_FAILED,
        model: viewModel
    };
};

export const requestMyTournamentList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.MY_CONTEST_TOURNAMENT_LIST.TOURNAMENT, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(myTournamentList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(myTournamentListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const MY_CONTEST_LIST = 'MY_CONTEST_LIST';
export const MY_CONTEST_LIST_FAILED = 'MY_CONTEST_LIST_FAILED';
export const MY_TOURNAMENT_LIST = 'MY_TOURNAMENT_LIST';
export const MY_TOURNAMENT_LIST_FAILED = 'MY_TOURNAMENT_LIST_FAILED';

export const resetMyContestTournamentState = () => {
    return {
        type: RESET_MY_CONTEST_TOURNAMENT_STATE,
    };
};

export const RESET_MY_CONTEST_TOURNAMENT_STATE = 'RESET_MY_CONTEST_TOURNAMENT_STATE';