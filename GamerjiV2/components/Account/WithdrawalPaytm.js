import React, { useEffect, useRef } from 'react'
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Button, Dimensions, Image, ScrollView, SafeAreaView } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';

const WithdrawalPaytm = (props, navigation) => {

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Withdrawal Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_withdrawal} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Your Winnings */}
                        <View style={[flexDirectionStyles.row, positionStyles.relative, heightStyles.height_50, colorStyles.redBackgroundColor, shadowStyles.shadowBackground, colorStyles.redShadowColor, borderStyles.borderRadius_50, paddingStyles.leftPadding_24]}>

                            {/* Image - Winning Trophy */}
                            <Image style={[alignmentStyles.alignSelfCenter]} source={require('../../assets/images/ic_winning_trophy.png')} />

                            {/* Text - Your Winnings */}
                            <Text style={[alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_13]}>{Constants.text_your_winnings}</Text>

                            {/* Text - Your Winnings Balance */}
                            <Text style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, fontFamilyStyles.bold, colorStyles.whiteColor, paddingStyles.rightPadding_17, fontSizeStyles.fontSize_05]}>{Constants.text_rupees}{'250.00'} </Text>
                        </View>

                        {/* Image - Paytm */}
                        <Image style={[heightStyles.height_32, widthStyles.width_100, alignmentStyles.alignSelfCenter, marginStyles.topMargin_30, marginStyles.bottomMargin_20]} source={require('../../assets/images/ic_paytm_big.png')} />

                        {/* Text - Mobile Number */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_43]}> {Constants.text_mobile_no} </Text>

                        {/* Wrapper - Mobile Number */}
                        <View style={[containerStyles.container, heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7]}>

                            {/* Text Validator - Mobile Number */}
                            <Text style={[colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}> +91 - {global.profile && global.profile.phone}
                            </Text>
                        </View>

                        {/* Text - This no. is linked to your Fantasyji eSport account and can’t be changed */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_15, marginStyles.rightMargin_15, marginStyles.topMargin_30, alignmentStyles.alignTextCenter]}> {Constants.text_withdraw_paytm_warning} </Text>
                    </ScrollView>

                    {/* TouchableOpacity - Proceed Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20]} onPress={() => props.navigation.navigate(Constants.nav_verify_otp)} activeOpacity={0.5}>

                        {/* Text - Button (Proceed) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_proceed}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default WithdrawalPaytm;