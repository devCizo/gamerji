import React from 'react'
import { Constants } from '../../appUtils/constants';

const myRecentTransactionsData = [{
        title: "29th June, 2020",
        data: [
            { label: 'Bonus Credited', amount: '₹50', transactionId: 'ABC111', transactionDate: '29th June, 2020 11.55 AM', gameName: 'PUBG', gameType: 'Solo', isShown: true },
            { label: 'Won A Contest', amount: '₹100', transactionId: 'ABC222', transactionDate: '29th June, 2020 01.23 PM', gameName: 'PLite', gameType: 'Duo', isShown: false },
            { label: 'Deposit Balance', amount: '₹150', transactionId: 'ABC333', transactionDate: '29th June, 2020 08.00 PM', gameName: 'FreeFire', gameType: 'Squad', isShown: false },
            { label: 'Refunded', amount: '₹200', transactionId: 'ABC444', transactionDate: '29th June, 2020 10.45 PM', gameName: 'PUBG', gameType: 'Tournament', isShown: false },
        ]
    },
    {
        title: "30th June, 2020",
        data: [
            { label: 'Deposit Balance', amount: '₹230', transactionId: 'XYZ888', transactionDate: '30th June, 2020 10.45 PM', gameName: 'Mario Kart', gameType: 'Tournament', isShown: false },
            { label: 'Refunded', amount: '₹980', transactionId: 'XYZ999', transactionDate: '30th June, 2020 10.45 PM', gameName: 'Asphalt', gameType: 'Solo', isShown: false },
        ]
    }
];

export default myRecentTransactionsData;