import produce from 'immer';
import * as actionTypes from '../../actions/profile/editProfileAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        avatarsListResponse: undefined,
        profileBannersListResponse: undefined,
        editProfileResponse: undefined,
    },
    error: false
};

{ /* Request - Avatar List */ }
const avatarList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.avatarsListResponse = action.model;
    });
};

{ /* Request Failure - Avatar List */ }
const avatarListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.avatarsListResponse = action.model;
    });
};

{ /* Request - Banner List */ }
const profileBannerList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.profileBannersListResponse = action.model;
    });
};

{ /* Request Failure - Banner List */ }
const profileBannerListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.profileBannersListResponse = action.model;
    });
};

{ /* Request - Edit Profile */ }
const editProfileRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.editProfileResponse = action.model;
    });
};

{ /* Request Failure - Edit Profile */ }
const editProfileRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.editProfileResponse = action.model;
    });
};

{ /* Reset State - Edit Profile */ }
const resetEditProfileState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.avatarsListResponse = undefined;
        draft.model.profileBannersListResponse = undefined;
        draft.model.editProfileResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AVATAR_LIST:
            return avatarList(state, action);
        case actionTypes.AVATAR_LIST_FAILED:
            return avatarListFailed(state, action);
        case actionTypes.PROFILE_BANNER_LIST:
            return profileBannerList(state, action);
        case actionTypes.PROFILE_BANNER_LIST_FAILED:
            return profileBannerListFailed(state, action);
        case actionTypes.REQUEST_EDIT_PROFILE:
            return editProfileRequest(state, action);
        case actionTypes.REQUEST_EDIT_PROFILE_FAILED:
            return editProfileRequestFailed(state, action);
        case actionTypes.RESET_EDIT_PROFILE_STATE:
            return resetEditProfileState(state, action);
        default:
            return state;
    }
};

export default reducer;