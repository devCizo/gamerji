import React, { useState } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants'
import colors from '../assets/colors/colors';
const { height, width } = Dimensions.get('window');

const NoRecordsFound = (props) => {

    return (
        <View style={{ flexDirectionStyles: 'row', height: height - 120, justifyContent: 'center', top: 20 }}>

            {/* Text - No Records Found */}
            <Text style={{ color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>

            {/* Text - No Records Separator */}
            <Text style={[heightStyles.height_2, widthStyles.width_50, marginStyles.topMargin_4, colorStyles.redBackgroundColor, alignmentStyles.alignSelfCenter]} />
        </View>
    )
}

export default NoRecordsFound;