import produce from 'immer';
import * as actionTypes from '../../actions/more/leaderboardAction';

const initialState = {
    model: {
        leaderboardListResponse: undefined,
    },
    error: false
};

const leaderboardList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.leaderboardListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const leaderboardListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.leaderboardListResponse = action.model;
    });
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LEADERBOARD_LIST:
            return leaderboardList(state, action);
        case actionTypes.LEADERBOARD_LIST_FAILED:
            return leaderboardListFailed(state, action);
        case actionTypes.RESET_LEADERBOARD_STATE:
            return resetLeaderboardState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetLeaderboardState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.leaderboardListResponse = undefined;
    })
}