import { PermissionsAndroid, Alert } from 'react-native';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import Geolocation from '@react-native-community/geolocation';
import { showMessage } from 'react-native-flash-message';

// Add Dynamic Log
export const addLog = (tag, value) => {
    console.log(tag, value)
}

// Get Payment Current Date with Random Number
export const getPaymentTimeStamp = () => {
    return (moment(Date()).format('YYYYMMDDhhmmss') + '' + (Math.floor(Math.random() * 10000)));
}

// Get UPI Pattern
export const getUpiPattern = () => {
    return /^[\w.-]+@[\w.-]+$/;
}

// This function will compare the Input Year is same as Current Year 
// Also compare the Input Year is greater than the Current Year
export const compareCurrentYear = (inputYear) => {
    var currentYear = new Date().getFullYear().toString().slice(2, 4);

    addLog("Input Year:::> ", inputYear);
    addLog("Current Year:::> ", currentYear);

    return moment(inputYear).isSameOrAfter(currentYear);
}

// This function will compare the Input Year is same as Current Year 
export const checkSameYear = (inputYear) => {
    var currentYear = new Date().getFullYear().toString().slice(2, 4);

    addLog("Input Year:::> ", inputYear);
    addLog("Current Year:::> ", currentYear);

    return moment(inputYear).isSame(currentYear);
}

// This function will compare the Input Month with the Current Month
export const checkMonthOfTheCurrentYear = (inputMonth) => {
    var currentMonth = ('0' + (new Date().getMonth() + 1)).slice(-2)

    addLog("Input Month:::> ", inputMonth);
    addLog("Current Month:::> ", currentMonth);

    return moment(inputMonth).isSameOrAfter(currentMonth);
}

// This function should show the toast notification for Success.
export const showSuccessToastMessage = (message) => {
    // return Toast.show({
    //     type: 'success',
    //     position: 'top',
    //     text1: message,
    //     // text2: message,
    //     visibilityTime: 4000,
    //     autoHide: true,
    //     topOffset: 40,
    //     bottomOffset: 40,
    //     onShow: () => { },
    //     onHide: () => { }, // called when Toast hides (if `autoHide` was set to `true`)
    //     onPress: () => { },
    //     props: {} // any custom props passed to the Toast component
    // });


    showMessage({
        message: message,
        type: "error",
        position: "top",
        // description: "Lorem ipsum dolar sit amet"
    });
}

// This function should show the toast notification for Error.
export const showErrorToastMessage = (message) => {
    // return Toast.show({
    //     type: 'error',
    //     position: 'top',
    //     text1: message,
    //     // text2: message,
    //     visibilityTime: 4000,
    //     autoHide: true,
    //     topOffset: 40,
    //     bottomOffset: 40,
    //     onShow: () => { },
    //     onHide: () => { }, // called when Toast hides (if `autoHide` was set to `true`)
    //     onPress: () => { },
    //     props: {} // any custom props passed to the Toast component
    // });
    showMessage({
        message: message,
        type: "error",
        position: "top",
        // description: "Lorem ipsum dolar sit amet"
    });
}

// This function should show the toast notification for Info.
export const showInfoToastMessage = (message) => {
    return Toast.show({
        type: 'info',
        position: 'top',
        // text1: message,
        text2: message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 40,
        bottomOffset: 40,
        onShow: () => { },
        onHide: () => { }, // called when Toast hides (if `autoHide` was set to `true`)
        onPress: () => { },
        props: {} // any custom props passed to the Toast component
    });
}

// This function should validate the right format of an email.
export const validateEmail = (value) => {
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(value);
};

// This function should set the currency format using comma separator.
export const setCurrencyFormat = (value) => {
    value = value.toString();
    var lastThree = value.substring(value.length - 3);
    var otherNumbers = value.substring(0, value.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
    return res;
}

// This function should set the numbers into B, M, K format.
export const convertNumberToMillions = (labelValue) => {

    if (labelValue < 1e3) return labelValue;
    if (labelValue >= 1e3 && labelValue < 1e6) return +(labelValue / 1e3).toFixed(1) + "K";
    if (labelValue >= 1e6 && labelValue < 1e9) return +(labelValue / 1e6).toFixed(1) + "M";
    if (labelValue >= 1e9 && labelValue < 1e12) return +(labelValue / 1e9).toFixed(1) + "B";
    if (labelValue >= 1e12) return +(labelValue / 1e12).toFixed(1) + "T";
}

// This function should check the runtime permissions for the Android & iOS devices.
export const hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
        const hasPermission = await hasPermissionIOS();
        return hasPermission;
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
        return true;
    }

    const hasPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
        return true;
    }

    const status = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
        ToastAndroid.show(
            'Location permission denied by user.',
            ToastAndroid.LONG,
        );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        ToastAndroid.show(
            'Location permission revoked by user.',
            ToastAndroid.LONG,
        );
    }

    return false;
};

const hasPermissionIOS = async () => {
    const openSetting = () => {
        Linking.openSettings().catch(() => {
            Alert.alert('Unable to open settings');
        });
    };
    const status = await Geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
        return true;
    }

    if (status === 'denied') {
        Alert.alert('Location permission denied');
    }

    if (status === 'disabled') {
        Alert.alert(
            `Turn on Location Services to allow "${appConfig.displayName}" to determine your location.`,
            '', [
            { text: 'Go to Settings', onPress: openSetting },
            { text: "Don't Use Location", onPress: () => { } },
        ],
        );
    }

    return false;
};

export async function checkRuntimePermissions() {

    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
            'title': 'ReactNativeCode Location Permission',
            'message': 'ReactNativeCode App needs access to your location '
        }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            Alert.alert("Location Permission Granted.");
        } else {
            Alert.alert("Location Permission Not Granted");
        }
    } catch (err) {
        addLog('Error:::> ', err)
    }
}

// This function should check if Sponsor Ads enabled
export const checkIsSponsorAdsEnabled = (screenName) => {
    ///  return undefined
    if (global.screens) {
        var tempArr = global.screens.filter(item => {
            if (item.code === screenName) {
                if (item.isGamerjiAds == true || item.isFbAds == true) {
                    return item
                }
            }
        })

        if (tempArr.length > 0) {
            return tempArr[0]
        }
        return undefined
    }
}

export const isValidURL = (url) => {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(url);
}

export const isValidEmail = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email) === false) {
        return false
    } else {
        return true
    }
}

import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const safeAreaTopHeight = () => {
    const insets = useSafeAreaInsets();
    return insets.top
}

export const safeAreaBottomHeight = () => {
    const insets = useSafeAreaInsets();
    return insets.bottom
}