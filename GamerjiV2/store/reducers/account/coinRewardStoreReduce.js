import produce from 'immer';
import * as actionTypes from '../../actions/account/coinRewardStoreAction';

const initialState = {
    model: {
        coinPackListResponse: undefined,
        avatarCategoryListResponse: undefined,
        buyAvatarResponse: undefined,
    },
    error: false
};

// ========

const coinPackList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.coinPackListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const coinPackListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.coinPackListResponse = action.model;
    });
};

// ========

const avatarCategoryList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.avatarCategoryListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const avatarCategoryListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.avatarCategoryListResponse = action.model;
    });
};

// ========

const buyAvatar = (state, action) => {
    return produce(state, (draft) => {
        draft.model.buyAvatarResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const buyAvatarFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.buyAvatarResponse = action.model;
    });
};

// ========

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COIN_PACK_LIST:
            return coinPackList(state, action);
        case actionTypes.COIN_PACK_LIST_FAILED:
            return coinPackListFailed(state, action);
        case actionTypes.AVATAR_CATEGORY_LIST:
            return avatarCategoryList(state, action);
        case actionTypes.AVATAR_CATEGORY_LIST_FAILED:
            return avatarCategoryListFailed(state, action);
        case actionTypes.BUY_AVATAR:
            return buyAvatar(state, action);
        case actionTypes.BUY_AVATAR_FAILED:
            return buyAvatarFailed(state, action);
        case actionTypes.RESET_COIN_REWARD_STORE_STATE:
            return resetCoinRewardStoreState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetCoinRewardStoreState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.coinPackListResponse = undefined;
        draft.model.avatarCategoryListResponse = undefined;
        draft.model.buyAvatarResponse = undefined;
    })
}