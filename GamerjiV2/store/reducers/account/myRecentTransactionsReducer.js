import produce from 'immer';
import * as actionTypes from '../../actions/account/myRecentTransactionsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        myRecentTransactionsResponse: undefined,
        emailInvoiceResponse: undefined
    },
    error: false
};

{ /* Request - My Recent Transactions */ }
const myRecentTransactionsRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.myRecentTransactionsResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - My Recent Transactions */ }
const myRecentTransactionsRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.myRecentTransactionsResponse = action.model;
    });
};

{ /* Request - Email Invoice */ }
const emailInvoiceRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.emailInvoiceResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Email Invoice */ }
const emailInvoiceRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.emailInvoiceResponse = action.model;
    });
};

{ /* Reset State - My Recent Transactions */ }
const resetMyRecentTransactionsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.myRecentTransactionsResponse = undefined;
        draft.model.emailInvoiceResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_MY_RECENT_TRANSACTIONS:
            return myRecentTransactionsRequest(state, action);
        case actionTypes.REQUEST_MY_RECENT_TRANSACTIONS_FAILED:
            return myRecentTransactionsRequestFailed(state, action);
        case actionTypes.REQUEST_EMAIL_INVOICE:
            return emailInvoiceRequest(state, action);
        case actionTypes.REQUEST_EMAIL_INVOICE_FAILED:
            return emailInvoiceRequestFailed(state, action);
        case actionTypes.RESET_MY_RECENT_TRANSACTIONS_STATE:
            return resetMyRecentTransactionsState(state);
        default:
            return state;
    }
};

export default reducer;