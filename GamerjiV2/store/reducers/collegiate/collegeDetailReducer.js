import produce from 'immer';
import * as actionTypes from '../../actions/collegiate/collegeDetailAction';

const initialState = {
    model: {
        collegeMembersResponse: undefined,
    },
    error: false
};

const collegeMembers = (state, action) => {
    return produce(state, (draft) => {
        draft.model.collegeMembersResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const collegeMembersFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.collegeMembersResponse = action.model;
    });
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COLLEGE_MEMBERS:
            return collegeMembers(state, action);
        case actionTypes.COLLEGE_MEMBERS:
            return collegeMembersFailed(state, action);
        case actionTypes.RESET_COLLEGE_DETAIL_STATE:
            return resetCollegeDetailState(state);
        default:
            return state;
    }
};

export default reducer;

const resetCollegeDetailState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.collegeMembersResponse = undefined;
    })
}