import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, Dimensions, Linking } from 'react-native'
import { scrollViewStyles, justifyContentStyles, containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, fontFamilyStyleNew, positionStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';

import colors from '../../assets/colors/colors';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';

import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, checkIsSponsorAdsEnabled, convertNumberToMillions } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import ChangeUserName from '../JoinContest/ChangeUserName'
import JoinHtmlGameValidation from '../FreeGames/JoinHtmlGameValidation'
import Modal from 'react-native-modal';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';
import LinearGradient from 'react-native-linear-gradient';



const HtmlGame = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    var [html5Games, setHtml5Games] = useState(undefined);
    var [html5GameItem, setHtml5GameItem] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [isVissibleGamePopup, setGamePopup] = useState(false);

    const gameType = props.route.params.gameType;
    const width = (Dimensions.get('window').width - 40);

    // This Use-Effect function should call the Game Types API
    useEffect(() => {

        addLog("gameType==>", gameType);
        if (gameType && gameType._id) {
            let payload = {
                skip: 0,
                limit: 100,
                html5Category: gameType._id,
                sortby: 'order', sort: 'asc'
            }
            setLoading(true)
            addLog("payload==>", payload);
            dispatch(actionCreators.requestHtml5Games(payload))
        }

        return () => {
            dispatch(actionCreators.resetHtml5GamesState())
        }
    }, []);

    //  API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.html5Games.model }),
        shallowEqual
    );
    var { html5GameResponse } = currentState

    // This Use-Effect function should set the Game Types Data
    useEffect(() => {
        addLog("useEffect == html5GameResponse==> ", html5GameResponse)
        if (html5GameResponse) {
            setLoading(false)
            var tempGameTypes = []
            // if (game && game.isTournament && game.isTournament == true) {
            //     tempGameTypes.push({ 'isTournament': true, 'name': 'TOURNAMENT', 'featuredImage': game.tournamentFeaturedImage })
            // }
            if (html5GameResponse.list) {
                tempGameTypes.push(...html5GameResponse.list)
            }
            setHtml5Games(tempGameTypes)
        }

        html5GameResponse = undefined
    }, [html5GameResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let openHtmlGamePopup = (item) => {
        addLog('openHtmlGamePopup')
        setHtml5GameItem(item);
        setGamePopup(true);
    }

    let closeHtmlGamePopup = () => {
        setGamePopup(false)
    }

    let moveToTournamentList = () => {
        //  RootNavigation.navigate(Constants.nav_tournament_list, { game });
        // RootNavigation.navigate(Constants.nav_chart)
    }
    const redirectToGameTypeScreen = (htmlGame) => {

        props.navigation.navigate(Constants.nav_html_game_detail, { htmlGame });
    }

    let openApp = () => {
        Linking.canOpenURL('market://details?id=com.pubg.imobile').then(supported => {
            if (supported) {
                console.log('accepted')
                return Linking.openURL('market://details?id=com.pubg.imobile')
            } else {
                console.log('an error occured')
            }
        }).catch(
            console.log('an error occured')
        )
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - HTML Game Name Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_05]} numberOfLines={1} ellipsizeMode='tail'> {gameType && gameType.name} </Text>
                </View>

                {/* Wrapper - HTML Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}

                    <ScrollView style={[scrollViewStyles.wrapperFullScrollView, positionStyles.relative, { marginBottom: 10 }]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>



                        {/* Wrapper - HTML Game  Items */}
                        <View style={[marginStyles.topMargin_03, { marginLeft: 10, marginRight: 10 }]}>
                            <FlatList
                                data={html5Games}
                                renderItem={({ item }) => <HtmlGamesItem item={item} navigation={props.navigation} openHtmlGamePopup={openHtmlGamePopup} />}
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={3}
                            // scrollEnabled={false}
                            />
                        </View>


                    </ScrollView>
                    <Modal isVisible={isVissibleGamePopup}
                        coverScreen={false}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <JoinHtmlGameValidation navigation={props.navigation} htmlGame={html5GameItem} closeHtmlGamePopup={closeHtmlGamePopup} />
                    </Modal>
                    <View style={{ width: '100%' }}>



                        {checkIsSponsorAdsEnabled('htmlGames') &&
                            <SponsorBannerAds screenCode={'htmlGames'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('htmlGames')} />
                        }
                    </View>
                </View>
            </View>



            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

const HtmlGamesItem = (props) => {
    const width = (Dimensions.get('window').width - 30) / 3;
    const htmlGame = props.item

    addLog("HtmlGamesItem==>", htmlGame);





    return (



        <TouchableOpacity onPress={() => props.openHtmlGamePopup(htmlGame)} activeOpacity={1}>

            {/* Wrapper - All Games Main View */}
            <View style={[containerStyles.container, borderStyles.borderRadius_5, colorStyles.primaryBackgroundColor, { width: width, marginBottom: 5, height: 120 }, marginStyles.rightMargin_5]}>

                {/* Wrapper - All Games View */}
                <View style={[flexDirectionStyles.column, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_5, alignmentStyles.oveflow, paddingStyles.padding_008]}>

                    {/* Wrapper - Game Name */}
                    <View style={{ alignItems: 'center', width: '100%' }}>

                        {/* Text - Game Name */}
                        <CustomMarquee value={htmlGame.name} style={[fontFamilyStyles.bold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_13, { marginTop: 3 }]} />
                    </View>

                    {/* Image - Featured Game Image */}
                    <Image style={[heightStyles.height_90, { width: '100%' }, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.topMargin_4]}
                        source={{ uri: generalSetting.UPLOADED_FILE_URL + (htmlGame && htmlGame.thumbImage && htmlGame.thumbImage.default) }} />

                    {/* Linear Gradient - Shadow  */}
                    <LinearGradient colors={['#00000000', '#000000']} style={[positionStyles.absolute, heightStyles.height_90, widthStyles.width_100p, borderStyles.borderRadius_5, resizeModeStyles.cover, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, otherStyles.zIndex_1]} >

                        <View style={{ position: 'absolute', bottom: 5, flexDirection: 'row', left: 5, alignItems: 'center' }}>
                            <Image style={{ height: 14, width: 14, resizeMode: 'contain', alignSelf: 'center' }} source={require('../../assets/images/ic_joystick_round.png')} />

                            <View style={{ marginLeft: 4, flexDirection: 'row', alignSelf: 'center' }}>

                                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, fontSize: 12 }} numberOfLines={1} ellipsizeMode='tail'>{convertNumberToMillions(htmlGame.totalJoinedPlayers)}</Text>

                                <Text style={{ marginLeft: 2, fontFamily: fontFamilyStyleNew.regular, color: colors.white, fontSize: 12, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_played}</Text>
                            </View>
                        </View>

                    </LinearGradient>
                </View>
            </View>
        </TouchableOpacity >

        // TouchableOpacity - Game Type Click Event
        // <TouchableOpacity onPress={redirectToContestList} >
        //   <View style={[containerStyles.container, widthStyles.width_100p, heightStyles.height_24, otherStyles.aspectRatio_1, colorStyles.silverBackgroundColor, borderStyles.borderRadius_5, paddingStyles.padding_005]}>

        //     {/* Wrapper - Game Type Main View */}
        //     <View style={[flexDirectionStyles.column, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_5, alignmentStyles.oveflow, paddingStyles.padding_008, otherStyles.aspectRatio_1]}>

        //       {/* Text - Game Type */}
        //       <Text style={[fontFamilyStyles.extraBold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01, marginStyles.topMargin_01, otherStyles.capitalize]} numberOfLines={1} ellipsizeMode='tail'> {props.item.name} </Text>

        //       {/* Image - Game Type */}
        //       <Image style={[positionStyles.absolute, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.top_04, marginStyles.bottom_005, marginStyles.left_01, marginStyles.right_01]}
        //         source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.featuredImage.default }} />
        //     </View>
        //   </View>
        // </TouchableOpacity>
    )
}

export default HtmlGame;