import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, justifyContentStyles, positionStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import * as generalSetting from '../webServices/generalSetting'
import CustomMarquee from '../appUtils/customMarquee';
import * as RootNavigation from '../appUtils/rootNavigation'

const EsportsNewsItem = (props) => {

    // Variable Declarations
    let color = ['#FFC609', '#43F2FF', '#09FFC4'];
    const width = (Dimensions.get('window').width - 50) / 2;
    const isFromViewAllEsportsNews = props.isFromViewAllEsportsNews ? props.isFromViewAllEsportsNews : false

    return (
        <TouchableOpacity style={{ height: 150, width: isFromViewAllEsportsNews ? width : 160, borderRadius: 5, backgroundColor: color[props.index % color.length], marginRight: 10, marginBottom: isFromViewAllEsportsNews ? 10 : 0 }} onPress={() => RootNavigation.navigate(Constants.nav_esports_news_details, { blog: props.item })} activeOpacity={0.9} >

            <View style={{ flexDirection: 'column', margin: 5 }}>

                <Image style={{ height: 85, width: '100%', borderRadius: 5 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />

                <CustomMarquee value={props.item.title} style={{ marginTop: 10, marginLeft: 7, fontFamily: fontFamilyStyleNew.bold, color: colors.black, fontSize: 14 }} />

                <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, marginLeft: 5, color: colors.black, fontSize: 11 }} numberOfLines={2} ellipsizeMode='tail'> {props.item.shortDescription} </Text>
            </View>
        </TouchableOpacity>
    )
}

export default EsportsNewsItem;