import React, { useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, Platform } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';

import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar';
import { PermissionsAndroid } from 'react-native';
import Contacts from 'react-native-contacts';
import ContactsListItem from '../../componentsItem/ContactsListItem';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';

const SearchUserList = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();

    const [followerList, setFollowerList] = useState([])
    const [searchText, setSearchText] = useState('')
    const [searchUserList, setSearchUserList] = useState([])
    const [followerListFetchingStatus, setFollowerListFetchingStatus] = useState(false);

    const [followingList, setFollowingList] = useState([])
    const [followingListFetchingStatus, setFollowingListFetchingStatus] = useState(false);

    const [isLoading, setLoading] = useState(false);
    const [activeTab, setActiveTab] = useState('followers');

    const [followerCount, setFollowerCount] = useState(global.profile.followers ? global.profile.followers : 0)
    const [followingCount, setFollowingCount] = useState(global.profile.followings ? global.profile.followings : 0)

    // useEffect(() => {
    //     getFollowerList()

    //     return () => {
    //         dispatch(actionCreators.resetFriendsState())
    //     }
    // }, [])

    // const getFollowerList = () => {

    //     if (followerList.length == 0) {
    //         setLoading(true)
    //     }
    //     setFollowerListFetchingStatus(true)

    //     let payload = {
    //         skip: followerList.length,
    //         limit: 20
    //     }
    //     dispatch(actionCreators.requestFollowerList(payload))
    // }

    // const getFollowingList = () => {

    //     if (followingList.length == 0) {
    //         setLoading(true)
    //     }
    //     setFollowingListFetchingStatus(true)

    //     let payload = {
    //         skip: followingList.length,
    //         limit: 20
    //     }
    //     dispatch(actionCreators.requestFollowingList(payload))
    // }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.searchUsers.model }),
        shallowEqual
    );
    var { searchUserResponse } = currentState;

    addLog("searchUserResponse==>", searchUserResponse);

    // Follower List Response
    useEffect(() => {
        if (searchUserResponse) {
            setLoading(false)

            if (searchUserResponse.list) {
                //   setFollowerListFetchingStatus(searchUserResponse.list.length == 0)

                var tempArr = searchUserResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setSearchUserList([...tempArr])
            }
        }
        searchUserResponse = undefined
    }, [searchUserResponse])

    // Following List Response
    // useEffect(() => {
    //     if (followingListResponse) {
    //         setLoading(false)

    //         if (followingListResponse.list) {
    //             setFollowingListFetchingStatus(followingListResponse.list.length == 0)

    //             var tempArr = followingListResponse.list.map(item => {
    //                 let tempItem = { ...item }
    //                 return tempItem
    //             })
    //             setFollowingList([...followingList, ...tempArr])
    //         }
    //     }
    //     followingListResponse = undefined
    // }, [followingListResponse])

    // Tab select
    // let selectTabButton = (value) => {
    //     setActiveTab(value)
    //     if (value == 'followers') {
    //         if (followerList.length == 0) {
    //             getFollowerList()
    //         }
    //     } else if (value == 'following') {
    //         if (followingList.length == 0) {
    //             getFollowingList()
    //         }
    //     }
    // }



    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }
    // Search User
    const applySearchUser = () => {

        // setLoading(true)
        // dispatch(actionCreators.requestSearchUsers({ q: searchText, type: "appUser" }))

        // if (searchText) {
        //     setSearchText('')
        //     setSearchUserList(undefined)
        //     return
        // }

        if (searchText == '') {
            showErrorToastMessage('Please enter Search Text.')
        } else {

            setLoading(true)
            dispatch(actionCreators.requestSearchUsers({ q: searchText, type: "appUser" }))
        }
    }
    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }} >

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', justifyContent: 'flex-start' }} >

                {/* Back Button */}
                {<TouchableOpacity style={{ width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={{ width: 25, height: 23 }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: 'white', fontFamily: fontFamilyStyleNew.bold, fontSize: 20, textAlign: 'center', alignSelf: 'center', }} numberOfLines={1} >SEARCH</Text>
            </View>

            {/* Container View */}
            <View style={{ flex: 1, marginTop: 0, backgroundColor: 'white', borderTopStartRadius: 40, borderTopEndRadius: 40, }}>

                {/* Search View */}
                <View style={{ width: '100%', backgroundColor: colors.yellow, borderTopLeftRadius: 40, borderTopRightRadius: 40, borderBottomLeftRadius: 40, borderBottomRightRadius: 40, }}>

                    <View style={{ marginTop: 30, marginBottom: 30 }}>



                        <View style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, flexDirection: 'row' }} >
                            <TextInput
                                style={{ flex: 1, paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: colors.black }}
                                placeholder='Search...' placeholderTextColor={'#70717A'}
                                onChangeText={text => setSearchText(text)}
                                autoCorrect={false}
                                editable={true}
                            >

                            </TextInput>

                            <TouchableOpacity style={{ marginRight: 0, height: 44, width: 84, backgroundColor: colors.black, borderRadius: 23, alignItems: 'center', justifyContent: 'center' }} activeOpacity={0.5} onPress={() => applySearchUser()} >

                                <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Search</Text>

                            </TouchableOpacity>
                        </View>



                    </View>

                </View>

                {searchUserList ?
                    <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }} >
                        <FlatList
                            style={{}}
                            data={searchUserList}
                            renderItem={({ item, index }) => <SearchUserListItem item={item} index={index} />}
                            onEndReachedThreshold={0.1}
                        // onEndReached={() => {
                        //     addLog('onEndReached')
                        //     if (activeTab == 'followers' && !followerListFetchingStatus) {
                        //         getFollowerList()
                        //     }
                        //     if (activeTab == 'following' && !followingListFetchingStatus) {
                        //         getFollowingList()
                        //     }
                        // }}
                        />
                    </View> : <></>

                }



                {/* {activeTab != 'contacts' ? */}
                {/* <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }} >
                    <SearchUserListItem
                        style={{}}
                        data={activeTab == 'followers' ? followerList : followingList}
                        renderItem={({ item, index }) => <FriendListItem item={item} index={index} selectedTab={activeTab} removeUnFollowUser={removeUnFollowUser} />}
                        onEndReachedThreshold={0.1}
                        onEndReached={() => {
                            addLog('onEndReached')
                            if (activeTab == 'followers' && !followerListFetchingStatus) {
                                getFollowerList()
                            }
                            if (activeTab == 'following' && !followingListFetchingStatus) {
                                getFollowingList()
                            }
                        }}
                    />
                </View> */}
                {/* :
                    <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }} >
                        <FlatList
                            data={activeTab == 'followers' ? followerList : followingList}
                            renderItem={({ item, index }) => <ContactsListItem item={item} />}
                            numColumns={1}
                            keyExtractor={(item, index) => index}
                        />
                    </View>
                } */}
            </View>

            {
                checkIsSponsorAdsEnabled('friends') &&
                <SponsorBannerAds screenCode={'friends'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('friends')} />
            }

            {isLoading && <CustomProgressbar />}
        </SafeAreaView >
    )
}

const SearchUserListItem = (props) => {

    const user = props.item

    const removeUnFollowUser = () => {
        props.removeUnFollowUser(props.index)
    }

    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: user._id })} >
            <View style={{ marginTop: 6, marginLeft: 20, marginRight: 20, marginBottom: 6, height: 50, borderRadius: 25, borderColor: '#D5D7E3', borderWidth: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end' }} >

                <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10, flex: 1 }}>
                    {/* <Image style={{ marginLeft: 12, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
                    {user.level && user.level.featuredImage ?
                        <Image style={{ marginLeft: 12, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + user.level.featuredImage.default }} />
                        :
                        <Text></Text>
                    }
                    <View style={{ marginLeft: 12, flex: 1 }}>
                        <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.black, width: "100%" }} value={(user.gamerjiName)} />
                    </View>

                </View>

                {/* <TouchableOpacity style={{ marginRight: 12, width: 90, height: 30, borderRadius: 15, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', alignItems: 'center' }} onPress={() => removeUnFollowUser()} activeOpacity={0.5} >
                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, color: colors.black }} numberOfLines={1} >{props.selectedTab == 'followers' ? 'Remove' : 'Following'}</Text>
            </TouchableOpacity> */}
            </View >
        </TouchableOpacity>
    )
}

export default SearchUserList;