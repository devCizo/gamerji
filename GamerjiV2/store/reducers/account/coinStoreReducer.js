import produce from 'immer';
import * as actionTypes from '../../actions/account/coinStoreAction';

const initialState = {
    model: {
        coinStoreListResponse: undefined,
    },
    error: false
};

const coinStoreList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.coinStoreListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const coinStoreListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.coinStoreListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COIN_STORE_LIST:
            return coinStoreList(state, action);
        case actionTypes.COIN_STORE_LIST_FAILED:
            return coinStoreListFailed(state, action);
        case actionTypes.RESET_COIN_STORE_STATE:
            return resetCoinStoreState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetCoinStoreState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.coinStoreListResponse = undefined;
    })
}