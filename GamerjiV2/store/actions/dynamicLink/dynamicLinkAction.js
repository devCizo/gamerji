import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const getContestTournamentByDynamicLink = (viewModel) => {
    return {
        type: GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK,
        model: viewModel
    };
};

export const getContestTournamentByDynamicLinkFailed = (viewModel) => {
    return {
        type: GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK_FAILED,
        model: viewModel
    };
};

export const requestGetContestTournamentByDynamicLink = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.JOIN_VIA_INVITE_CODE.GET);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_VIA_INVITE_CODE.GET, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getContestTournamentByDynamicLink(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getContestTournamentByDynamicLinkFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetDynamicLinkState = () => {
    return {
        type: RESET_DYNAMIC_LINK_STATE,
    };
};

export const GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK = 'GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK';
export const GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK_FAILED = 'GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK_FAILED';
export const RESET_DYNAMIC_LINK_STATE = 'RESET_DYNAMIC_LINK_STATE';