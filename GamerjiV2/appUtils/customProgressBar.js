import * as React from 'react';
import { View, Text, ActivityIndicator, Dimensions } from 'react-native';
import { containerStyles, widthStyles, heightStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../appUtils/commonStyles';
import colors from '../assets/colors/colors';
import { Constants } from './constants';

const CustomProgressbar = (props) => {

    return (

        <View style={{ position: 'absolute', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', top: (Dimensions.get('window').height / 2) - 50, left: (Dimensions.get('window').width / 2) - 50, height: 100, width: 100, backgroundColor: colors.silver, borderRadius: 10 }}>
            <ActivityIndicator size="large" color={colors.black} />
        </View>

        // <View style={[containerStyles.container, positionStyles.relative, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, { width: 100, height: 100 }]} >

        //     {/* Wrapper - Loading with Indicator */}
        //     <View style={[flexDirectionStyles.column, borderStyles.borderRadius_10, colorStyles.silverBackgroundColor, alignmentStyles.alignSelfCenter, justifyContentStyles.center, paddingStyles.padding_05]}>

        //         {/* Activity Indicator */}
        //         <ActivityIndicator size="large" color={colors.black} />

        //         {/* Text - Loading */}
        //         <Text style={[colorStyles.blackColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_040, alignmentStyles.alignTextCenter, marginStyles.topMargin_01]}>{Constants.text_loading}</Text>
        //     </View>
        // </View>
    );
}

export default CustomProgressbar;



