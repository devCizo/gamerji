import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, TextInput, Dimensions } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import ModalDropdown from 'react-native-modal-dropdown';
import moment from 'moment';
import DatePicker from 'react-native-date-picker'
import RBSheet from "react-native-raw-bottom-sheet";
import ImagePicker from "react-native-image-crop-picker";
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomProgressBar from '../../appUtils/customProgressBar';

import axios from 'axios';

const raiseComplaint = (props) => {

    addLog('raiseComplaint', props.route)

    const dispatch = useDispatch();

    const [isLoading, setLoading] = useState(false)

    const [email, setEmail] = useState(global.profile.email ? global.profile.email : '')
    const [describeIssue, setDescribeIssue] = useState('')

    const [categoryList, setCategoryList] = useState(undefined)
    const [category, setCategory] = useState('Select Category')
    const [categoryId, setCategoryId] = useState(undefined)
    const [isOpenCategoryPopup, setOpenCategoryPopup] = useState(false)

    const [subCategoryList, setSubCategoryList] = useState(undefined)
    const [subCategory, setSubCategory] = useState('Select Sub Category')
    const [subCategoryId, setSubCategoryId] = useState(undefined)

    const [gameList, setGameList] = useState(undefined)
    const [gameName, setGameName] = useState('Select Game')
    const [gameId, setGameId] = useState(undefined)

    const [gameTypeList, setGameTypeList] = useState(undefined)
    const [gameTypeName, setGameTypeName] = useState('Select Game Type')
    const [gameTypeId, setGameTypeId] = useState(undefined)

    const [contestDate, setContestDate] = useState('')
    const [contestDatePicker, setDateFromPicker] = useState(new Date())

    const [contestTime, setContestTime] = useState('')
    const [contestTimePicker, setTimeFromPicker] = useState(new Date())

    const [selectedScreenShot, setSelectedScreenshot] = useState(undefined)

    //Initiate
    useEffect(() => {
        dispatch(actionCreators.requestComplaintCategory({ isParent: true }))

        return () => {
            dispatch(actionCreators.resetRaiseComplaintState())
        }
    }, []);


    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.raiseComplaint.model }),
        shallowEqual
    )
    var { complaintCategoryResponse, complaintSubCategoryResponse, gameListResponse, gameTypeListResponse, raiseComplaintResponse, uploadRaiseComplaintImageResponse } = currentState


    // Complaint Category Response
    useEffect(() => {
        if (complaintCategoryResponse && complaintCategoryResponse.list) {
            setCategoryList(complaintCategoryResponse.list)
        }
    }, [complaintCategoryResponse])

    // Complaint Sub Category Response
    useEffect(() => {
        if (complaintSubCategoryResponse && complaintSubCategoryResponse.list) {
            setSubCategoryList(complaintSubCategoryResponse.list)
        }
        complaintSubCategoryResponse = undefined
    }, [complaintSubCategoryResponse])

    // Game List Response
    useEffect(() => {
        if (gameListResponse && gameListResponse.list) {
            setGameList(gameListResponse.list)
        }
        gameListResponse = undefined
    }, [gameListResponse]);

    // Game Type List Response
    useEffect(() => {
        if (gameTypeListResponse && gameTypeListResponse.list) {
            setGameTypeList(gameTypeListResponse.list)
        }
        gameTypeListResponse = undefined
    }, [gameTypeListResponse])

    // Upload Raise Complaint Image Response
    useEffect(() => {
        if (uploadRaiseComplaintImageResponse) {
            if (uploadRaiseComplaintImageResponse.item) {
                raiseComplaint()
            } else if (raiseComplaintResponse.errors && raiseComplaintResponse.errors[0] && raiseComplaintResponse.errors[0].msg) {
                setLoading(false)
                showErrorToastMessage(raiseComplaintResponse.errors[0].msg)
            }
        }
    }, [uploadRaiseComplaintImageResponse])

    // Raise Complaint Response
    useEffect(() => {
        if (raiseComplaintResponse) {

            setLoading(false)
            if (raiseComplaintResponse.success && raiseComplaintResponse.success == true) {

                RootNavigation.goBack()
                if (props.route.params.getTicketList) {
                    props.route.params.getTicketList()
                }
            } else if (raiseComplaintResponse.errors && raiseComplaintResponse.errors[0] && raiseComplaintResponse.errors[0].msg) {

                showErrorToastMessage(raiseComplaintResponse.errors[0].msg)
            }
        }
        raiseComplaintResponse = undefined
    }, [raiseComplaintResponse])

    //Submit Data
    let submitData = () => {

        if (email == '') {
            showErrorToastMessage('Please enter email address')
            return
        }
        if (!categoryId) {
            showErrorToastMessage('Please select category')
            return
        }
        if (subCategoryList && !subCategoryId) {
            showErrorToastMessage('Please select sub category')
            return
        }
        if (gameList) {
            if (!gameId) {
                showErrorToastMessage('Please select game')
                return
            }
            if (!gameTypeId) {
                showErrorToastMessage('Please select game type')
                return
            }
        }
        if (describeIssue == '') {
            showErrorToastMessage('Please describe your issue')
            return
        }

        if (selectedScreenShot) {

            setLoading(true)

            let filename = selectedScreenShot.path.substring(selectedScreenShot.path.lastIndexOf('/') + 1, selectedScreenShot.path.length);

            var payload = new FormData();
            payload.append('file', { uri: selectedScreenShot.path, name: filename, type: 'image/jpeg' });

            addLog('data', payload)

            dispatch(actionCreators.requestUploadRaiseComplaintImage(payload))
        } else {
            setLoading(true)
            raiseComplaint()
        }
    }

    const raiseComplaint = () => {


        var payload = {
            name: global.profile.name,
            email: email,
            phone: global.profile.phone,
            phoneCode: global.profile.phoneCode,
            category: categoryId,
            description: describeIssue
        }

        if (subCategoryId) {
            payload['subCategory'] = subCategoryId
        }
        if (gameList) {
            if (gameId) {
                payload['game'] = gameId
            }
            if (gameTypeId) {
                payload['gameType'] = gameTypeId
            }
            payload['contestDate'] = contestDatePicker//moment(contestDatePicker).format('DD/MM/yyyy')
            payload['contestTime'] = contestTimePicker//moment(contestTimePicker).format('hh:mm A')
        }

        if (uploadRaiseComplaintImageResponse && uploadRaiseComplaintImageResponse.item && uploadRaiseComplaintImageResponse.item.screenShot) {
            payload['screenShot'] = uploadRaiseComplaintImageResponse.item.screenShot
        }

        // addLog('payload', payload)
        dispatch(actionCreators.requestRaiseComplaint(payload))
    }

    const categorySelected = (idx, value) => {
        setSubCategoryList(undefined)
        setSubCategory('Select Sub Category')
        setSubCategoryId(undefined)
        setGameList(undefined)
        setGameName('Select Game')
        setGameId(undefined)

        setCategory(value)
        setCategoryId(categoryList[idx]._id)

        if (categoryList[idx].isContestDependency && categoryList[idx].isContestDependency == true) {
            dispatch(actionCreators.requestGameList({}))
        } else {
            dispatch(actionCreators.requestComplaintSubCategory({ parent: categoryList[idx]._id }))
        }
    }

    const subCategorySelected = (idx, value) => {
        setSubCategory(value)
        setSubCategoryId(subCategoryList[idx]._id)
        if (subCategoryList[idx].isContestDependency && subCategoryList[idx].isContestDependency == true) {
            dispatch(actionCreators.requestGameList({}))
        }
    }

    const gameSelected = (idx, value) => {
        setGameName(value)
        setGameId(gameList[idx]._id)

        dispatch(actionCreators.requestGameTypeList({ game: gameList[idx]._id }))
    }

    const gameTypeSelected = (idx, value) => {
        if (gameName == 'Select Game') {
            showErrorToastMessage('Please select game first')
            return
        }
        setGameTypeName(value)
        setGameTypeId(gameTypeList[idx]._id)
    }

    const dropDownImage = () => {
        return (
            <Image style={{ position: 'absolute', right: 18, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />
        )
    }

    const selectProfilePic = () => {

        ImagePicker.openPicker({
            mediaType: "photo",
        }).then((image) => {
            addLog(image);
            if (image) {
                setSelectedScreenshot(image)
            }
        });
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >CUSTOMER CARE</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                <ScrollView style={{ marginLeft: 20, marginRight: 20, marginBottom: 20 }} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} >

                    {/* Name */}
                    <Text style={{ marginTop: 40, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Name</Text>
                    <TextInput
                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.color_input_text_border, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black }}
                        editable={true} >{global.profile.name}
                    </TextInput>

                    {/* Mobile Number */}
                    <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Mobile Number</Text>
                    <TextInput
                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.color_input_text_border, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black }}
                        editable={false} >{global.profile.phone}
                    </TextInput>

                    {/* Email */}
                    <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Email</Text>
                    <TextInput
                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.color_input_text_border, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }}
                        placeholder='Enter email address' autoCorrect={false} autoCapitalize={'none'} keyboardType={'email-address'} onChangeText={text => setEmail(text)}>{email}
                    </TextInput>

                    {/* Category */}
                    <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Category</Text>

                    {categoryList &&
                        <ModalDropdown
                            style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', }}
                            textStyle={{ width: Dimensions.get('window').width - 40, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: category != 'Select Category' ? colors.black : '#BCBCBE' }}
                            dropdownStyle={{ marginTop: 12, marginLeft: 0, width: Dimensions.get('window').width - 40, borderWidth: 1, borderColor: '#D5D7E3' }}
                            dropdownTextStyle={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                            defaultValue={category}
                            options={categoryList.map(category => (category.name))}
                            defaultIndex={-1}
                            animated={true}
                            isFullWidth={true}
                            onSelect={(idx, value) => categorySelected(idx, value)}
                            renderRightComponent={dropDownImage}
                        />
                    }

                    {subCategoryList &&
                        // Sub Category
                        <View>
                            <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Sub Category</Text>

                            <ModalDropdown
                                style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', }}
                                textStyle={{ width: Dimensions.get('window').width - 40, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: subCategory != 'Select Sub Category' ? colors.black : '#BCBCBE' }}
                                dropdownStyle={{ marginTop: 12, marginLeft: 0, width: Dimensions.get('window').width - 40, borderWidth: 1, borderColor: '#D5D7E3' }}
                                dropdownTextStyle={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                defaultValue={subCategory}
                                options={subCategoryList.map(subCategory => (subCategory.name))}
                                defaultIndex={-1}
                                animated={true}
                                isFullWidth={true}
                                onSelect={(idx, value) => subCategorySelected(idx, value)}
                                renderRightComponent={dropDownImage}
                            />
                        </View>
                    }

                    {gameList &&
                        <View>

                            {/* Game */}
                            <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Game</Text>
                            <ModalDropdown
                                style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', }}
                                textStyle={{ width: Dimensions.get('window').width - 40, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: gameName != 'Select Game' ? colors.black : '#BCBCBE' }}
                                dropdownStyle={{ marginTop: 12, marginLeft: 0, width: Dimensions.get('window').width - 40, borderWidth: 1, borderColor: '#D5D7E3' }}
                                dropdownTextStyle={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                defaultValue={gameName}
                                options={gameList.map(game => (game.name))}
                                defaultIndex={-1}
                                animated={true}
                                isFullWidth={true}
                                onSelect={(idx, value) => gameSelected(idx, value)}
                                renderRightComponent={dropDownImage}
                            />

                            {/* Game Type */}
                            <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Game Type</Text>
                            <ModalDropdown
                                style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', }}
                                textStyle={{ width: Dimensions.get('window').width - 40, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: gameTypeName != 'Select Game Type' ? colors.black : '#BCBCBE' }}
                                dropdownStyle={{ marginTop: 12, marginLeft: 0, width: Dimensions.get('window').width - 40, borderWidth: 1, borderColor: '#D5D7E3' }}
                                dropdownTextStyle={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                defaultValue={gameTypeName}
                                options={gameTypeList ? gameTypeList.map(category => (category.name)) : []}
                                defaultIndex={-1}
                                animated={true}
                                isFullWidth={true}
                                onSelect={(idx, value) => gameTypeSelected(idx, value)}
                                renderRightComponent={dropDownImage}
                            />

                            {/* Contest Date */}
                            <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Contest Date</Text>

                            <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => contestDate != '' ? null : this.DatePicker.open()} activeOpacity={0.5} >

                                <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: colors.black }}>
                                    {contestDate != '' ? contestDate : (moment(contestDatePicker).format('DD/MM/yyyy'))}
                                </Text>
                                <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />
                            </TouchableOpacity>

                            {/* Contest Time */}
                            <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Contest Time</Text>

                            <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => contestTime != '' ? null : this.timePicker.open()} activeOpacity={0.5} >

                                <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: colors.black }}>
                                    {contestTime != '' ? contestTime : (moment(contestTimePicker).format('hh:mm A'))}
                                </Text>
                                <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />
                            </TouchableOpacity>
                        </View>
                    }

                    {/* Describe your Issue */}
                    <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Describe your issue</Text>
                    <TextInput
                        style={{ marginTop: 8, height: 100, borderRadius: 8, borderWidth: 1, borderColor: colors.color_input_text_border, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold, textAlignVertical: 'top' }}
                        placeholder='Describe your issue'
                        onChangeText={text => setDescribeIssue(text)}
                        multiline={true}
                    />

                    {/* Upload Screen Shot */}
                    <TouchableOpacity style={{ marginTop: 40, height: 120, backgroundColor: colors.yellow, borderRadius: 10, alignItems: 'center' }} onPress={() => selectProfilePic()} activeOpacity={0.5} >

                        {/* Image */}
                        <View style={{ marginTop: -10, height: 60, width: 60, backgroundColor: colors.white, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }} >
                            <Image style={{ height: 24, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/upload_screenshot.png')} />
                        </View>
                        <Text style={{ marginTop: 17, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >{!selectedScreenShot ? 'Upload Screenshot' : 'Image file attached'}</Text>
                        <Text style={{ marginTop: 3, color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >Note: Please upload screenshot of your game</Text>
                    </TouchableOpacity>
                    <Text style={{ marginLeft: 2, marginTop: 10, color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >Any raised service request will be resolved within 72 hours.</Text>
                    {/* Submit */}
                    <TouchableOpacity style={{ height: 46, marginTop: 46, marginBottom: 20, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => submitData()} activeOpacity={0.5} >
                        <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16 }} >SUBMIT</Text>
                        <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                    </TouchableOpacity>

                </ScrollView>

                {checkIsSponsorAdsEnabled('raiseAComplaint') &&
                    <SponsorBannerAds screenCode={'raiseAComplaint'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('raiseAComplaint')} />
                }
            </View>

            {/* Date Picker */}
            {/* <RBSheet
                ref={ref => {
                    this.DatePicker = ref;
                }} >
                <View style={styles.dateHeaderContainer}>
                    <TouchableOpacity
                        onPress={() => this.DatePicker.close()}
                        style={styles.dateHeaderButton} >
                        <Text style={styles.dateHeaderButtonCancel}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.DatePicker.close()}
                        style={[styles.dateHeaderButton]}>
                        <Text style={styles.dateHeaderButtonDone}>Done</Text>
                    </TouchableOpacity >
                </View>
                <DatePicker
                    mode='date'
                    date={contestDatePicker}
                    onDateChange={setDateFromPicker}
                />
            </RBSheet> */}

            {/* Time Picker */}
            {/* <RBSheet
                ref={ref => {
                    this.timePicker = ref;
                }} >
                <View style={styles.dateHeaderContainer}>
                    <TouchableOpacity
                        onPress={() => this.timePicker.close()}
                        style={styles.dateHeaderButton} >
                        <Text style={styles.dateHeaderButtonCancel}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.timePicker.close()}
                        style={[styles.dateHeaderButton]}>
                        <Text style={styles.dateHeaderButtonDone}>Done</Text>
                    </TouchableOpacity >
                </View>
                <DatePicker
                    mode='time'
                    date={contestTimePicker}
                    onDateChange={setTimeFromPicker}
                />
            </RBSheet> */}


            {/* {isOpenStateListPicker &&
                <Modal
                    isVisible={isOpenStateListPicker}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <CustomListPicker list={categoryList} openStateListPicker={setOpenStateListPicker} setSelectedState={setSelectedState} />
                </Modal>
            } */}

            {isLoading && <CustomProgressBar />}

        </SafeAreaView >
    )
}

export default raiseComplaint;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
    dateHeaderContainer: {
        height: 45,
        borderBottomWidth: 1,
        borderColor: "#ccc",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    dateHeaderButton: {
        height: "100%",
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    dateHeaderButtonCancel: {
        fontSize: 18,
        color: "#666",
        fontWeight: "400"
    },
    dateHeaderButtonDone: {
        fontSize: 18,
        color: "#006BFF",
        fontWeight: "500"
    },
})

// const launchImageLibrary = () => {
//     let options = {
//         storageOptions: {
//             skipBackup: true,
//             path: 'images',
//         },
//     };
//     ImagePicker.launchImageLibrary(options, (response) => {
//         console.log('Response = ', response);

//         if (response.didCancel) {
//             console.log('User cancelled image picker');
//         } else if (response.error) {
//             console.log('ImagePicker Error: ', response.error);
//         } else if (response.customButton) {
//             console.log('User tapped custom button: ', response.customButton);
//             alert(response.customButton);
//         } else {
//             const source = { uri: response.uri };
//             console.log('response', JSON.stringify(response));


//             // setState({
//             //     filePath: response,
//             //     fileData: response.data,
//             //     fileUri: response.uri
//             // });
//         }
//     });

// }