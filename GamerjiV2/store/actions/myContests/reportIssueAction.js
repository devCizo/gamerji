import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const reportIssue = (viewModel) => {
    return {
        type: REPORT_ISSUE,
        model: viewModel
    };
};

export const reportIssueFailed = (viewModel) => {
    return {
        type: REPORT_ISSUE_FAILED,
        model: viewModel
    };
};

export const requestReportIssue = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.REPORT_ISSUE.REQUEST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(reportIssue(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(reportIssueFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const REPORT_ISSUE = 'REPORT_ISSUE';
export const REPORT_ISSUE_FAILED = 'REPORT_ISSUE_FAILED';

export const resetReportIssueState = () => {
    return {
        type: RESET_REPORT_ISSUE_STATE,
    };
};

export const RESET_REPORT_ISSUE_STATE = 'RESET_REPORT_ISSUE_STATE';