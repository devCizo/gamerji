import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, TextInput, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew, shadowStyle } from '../../appUtils/commonStyles';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import { addLog } from '../../appUtils/commonUtlis';
import HtmlText from 'react-native-html-to-text';
import { WebView } from 'react-native-webview'
import YoutubePlayer from "react-native-youtube-iframe";
import getVideoId from 'get-video-id';

const HowToJoin = (props) => {

    // Variables
    const dispatch = useDispatch()
    const game = props.route.params.game;
    const [isLoading, setLoading] = useState(false)
    const [howToJoinData, setHowToJoinData] = useState([])


    // Init
    useEffect(() => {

        setLoading(true);
        dispatch(actionCreators.requestHowToJoinList({ filter: { game: game._id } }))

        return () => {
            dispatch(actionCreators.resetHowToJoinState())
        }
    }, []);

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.howToJoin.model }),
        shallowEqual
    );
    var { howToJoinListResponse } = currentState

    // Category List Response
    useEffect(() => {

        if (howToJoinListResponse) {
            setLoading(false)

            addLog('howToJoinListResponse.list', howToJoinListResponse.list)

            if (howToJoinListResponse.list) {

                var tempArr = howToJoinListResponse.list.map(item => {
                    let tempItem = { ...item };
                    tempItem.isSelected = false;
                    return tempItem;
                })

                setHowToJoinData(tempArr)
            }
        }
        howToJoinListResponse = undefined
    }, [howToJoinListResponse])

    let openCloseSection = (index) => {
        var tempArr = [...howToJoinData]
        tempArr[index].isSelected = !tempArr[index].isSelected
        setHowToJoinData(tempArr)
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            <View style={{ backgroundColor: colors.black }}>

                {/* Navigation Bar */}
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                    {/* Back Button */}
                    {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={RootNavigation.goBack}>
                        <Image style={{ width: 25, height: 23, alignSelf: 'center' }} source={require('../../assets/images/back_icon.png')} />
                    </TouchableOpacity>}

                    {/* Navigation Title */}
                    <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >HOW TO JOIN</Text>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: 'white', borderTopStartRadius: 40, borderTopEndRadius: 40, paddingBottom: 20 }}>


                {/* {howToJoinData &&
                    <WebView
                        originWhitelist={['*']}
                        source={{ html: howToJoinData.description }}
                        style={{ marginLeft: 12, marginRight: 12, marginBottom: 12, flex: 1, backgroundColor: 'clear' }}
                    />
                } */}

                <FlatList style={{ marginTop: 10 }}
                    data={howToJoinData}
                    renderItem={({ item, index }) => <HowToJoinItem item={item} index={index} openCloseSection={openCloseSection} />}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                />


            </View>


            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default HowToJoin

const HowToJoinItem = (props) => {

    let item = props.item

    return (
        <View style={[shadowStyle.shadow, { marginTop: 8, marginLeft: 12, marginRight: 12, marginBottom: 8, borderRadius: 10, borderWidth: 1, borderColor: '#DFE4E9', backgroundColor: colors.white, }]} >

            {/* Title */}
            <TouchableOpacity style={{ height: 48, justifyContent: 'center' }} onPress={() => props.openCloseSection(props.index)} >
                <Text style={{ marginLeft: 12, color: colors.black, alignSelf: "center", fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >{item.title}</Text>

            </TouchableOpacity>
            <FlatList
                data={item.content}
                renderItem={({ item }) => <HowToJoinItemSubItem item={item} />}
            />


        </View>
    )
}

const HowToJoinItemSubItem = (props) => {

    let item = props.item
    const [playing, setPlaying] = useState(false)

    addLog("props.item==>", props.item)

    const onStateChange = useCallback((state) => {
        if (state === "ended") {
            setPlaying(false)
            addLog("Video has finished playing!")
        }
    }, []);

    return (
        <View>
            {item.type == "text" ?
                <View style={{ marginLeft: 12, marginRight: 12, marginTop: 5 }}>
                    <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >{item.content}</Text>
                </View>
                :
                <Text style={{ height: 0 }}></Text>
            }

            {item.type == "image" ?
                <View style={{ height: 200 }}>
                    <Image style={{ marginLeft: 8, marginRight: 8, marginTop: 5, height: "100%", resizeMode: 'contain' }} source={item.content && { uri: item.content.replace("http://gamerji.me:9171/uploads/", generalSetting.UPLOADED_FILE_URL) }} />
                </View>
                : <Text style={{ height: 0 }}></Text>
            }

            {item.type == "video" ?
                <View style={{ marginLeft: 8, marginRight: 8, }}>
                    <YoutubePlayer
                        height={200}
                        play={playing}
                        videoId={getVideoId(item.content).id}
                        onChangeState={onStateChange} />

                </View>
                : <Text style={{ height: 0 }}></Text>
            }

        </View>
    )
}