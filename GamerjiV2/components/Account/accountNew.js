import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, TextInput, BackHandler, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { ScrollView } from 'react-native-gesture-handler';
import CommonInfoPopup from '../../commonComponents/commonInfoPopup';
import { showMessage } from 'react-native-flash-message';

const accountNew = (props) => {

    // Variable Declarations
    const dispatch = useDispatch()

    const [commonInfoPopupType, setCommonInfoPopupType] = useState(false)
    const [isOpenCommonInfoPopup, setOpenCommonInfoPopup] = useState(false)

    const [isLoading, setLoading] = useState(false)
    const [refreshing, setRefreshing] = useState(false);

    // Init
    useEffect(() => {
        setLoading(true)
        fetchAccountData()
        return () => {
            dispatch(actionCreators.resetAccountState())
        }
    }, []);

    const fetchAccountData = () => {
        dispatch(actionCreators.requestAccount())
    };

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        fetchAccountData()

    }, [refreshing]);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.account.model }),
        shallowEqual
    );
    const { accountResponse } = currentState

    // Account Response
    useEffect(() => {
        if (accountResponse) {
            setLoading(false)
            setRefreshing(false)
        }
    }, [accountResponse])

    const getDepositeCash = () => {
        return parseFloat(accountResponse.wallet && accountResponse.wallet.depositedAmount)
    }

    const getWinningCash = () => {
        return parseFloat(accountResponse.wallet && accountResponse.wallet.winningAmount)
    }

    const getBonusCash = () => {
        return parseFloat(accountResponse.wallet && accountResponse.wallet.bonusAmount)
    }

    const getTotalCash = () => {
        return getDepositeCash() + getWinningCash() + getBonusCash()
    }

    const getTotalCoin = () => {
        return accountResponse.wallet && accountResponse.wallet.coinAmount
    }

    const moveToAddBalanceScreen = () => {
        global.addBalanceScreenType = Constants.wallet_payment_type
        global.addBalanceCurrencyType = Constants.money_payment
        global.paymentSuccessCompletion = refreshAccountData
        props.navigation.push(Constants.nav_add_balance)
    }

    const moveToCoinRewardStoreScreen = () => {
        global.addBalanceCurrencyType = Constants.coin_payment
        global.paymentSuccessCompletion = refreshAccountData
        props.navigation.push(Constants.nav_coin_reward_store)
    }

    const moveToWithdrawalScreen = () => {
        if (!accountResponse.isEmailVerified) {
            showErrorToastMessage(Constants.error_verify_email_to_withdraw)

            return;
        }
        props.navigation.navigate(Constants.nav_withdrawal, { setAccountsData: fetchAccountData })
    }



    const moveToEmailVerifyScreen = () => {
        props.navigation.navigate(Constants.nav_mobile_add_email_verification, { setAccountsData: fetchAccountData })
    }

    const refreshAccountData = () => {
        fetchAccountData()
    }

    const handleBackButton = () => {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const openCommonInfoPopup = type => {
        setCommonInfoPopupType(type)
        setOpenCommonInfoPopup(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            <View style={{ flex: 1, marginBottom: 10, backgroundColor: colors.white, borderBottomLeftRadius: 40, borderBottomRightRadius: 40 }}>

                {/* Navigation Bar */}
                {/* <View style={{ backgroundColor: colors.black }}> */}

                {/* <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}> */}

                {/* Back Button */}
                {/* <TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }}
                        onPress={() => RootNavigation.goBack()}>
                        <Image style={{ width: 25, height: 23, alignSelf: 'center' }} source={require('../../assets/images/back_icon.png')} />
                    </TouchableOpacity> */}

                {/* Navigation Title */}
                {/* <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >ACCOUNT</Text> */}
                {/* </View>
                </View> */}

                {accountResponse &&
                    <ScrollView style={{ flex: 1 }}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >

                        {/* Top View */}
                        <View style={{ height: 335, backgroundColor: colors.black, borderBottomLeftRadius: 90, borderBottomRightRadius: 90, flexDirection: 'row' }} >

                            {/* Avatar */}
                            <Image style={{ marginTop: 20, width: 160, height: 242, resizeMode: 'contain' }} source={accountResponse.avatar && accountResponse.avatar.img && accountResponse.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + accountResponse.avatar.img.default }} />

                            {/* Right side view */}
                            <View style={{}} >

                                {/* User name */}
                                <View style={{ marginTop: 54, width: Constants.SCREEN_WIDTH - 180 }} >
                                    <CustomMarquee style={{ color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'left' }} value={accountResponse.gamerjiName} />
                                </View>


                                <View style={{ flexDirection: 'row' }} >
                                    {/* Add Balance */}
                                    {/* <TouchableOpacity style={{ marginTop: 11, height: 30, width: 95, backgroundColor: colors.yellow, borderRadius: 15, justifyContent: 'center' }} onPress={() => moveToAddBalanceScreen()} activeOpacity={0.5} >
                                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Add Balance</Text>
                                    </TouchableOpacity> */}

                                    {/* Add Coins */}
                                    <TouchableOpacity style={{ marginTop: 11, marginLeft: 5, height: 30, width: 80, backgroundColor: '#43F1FE', borderRadius: 15, justifyContent: 'center' }} onPress={() => moveToCoinRewardStoreScreen()} activeOpacity={0.5} >
                                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Coin Store</Text>
                                    </TouchableOpacity>
                                </View>

                                {/* Cash View */}
                                <View style={{ marginTop: 28, width: Constants.SCREEN_WIDTH - 180, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >

                                    {/* Cash icon */}
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image style={{ width: 24, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/cash-icon.png')} />

                                        {/* Total Cash */}
                                        <View style={{ marginLeft: 8 }} >
                                            <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Total Cash</Text>
                                            <Text style={{ color: colors.white, fontSize: 11, fontFamily: fontFamilyStyleNew.regular }} >Balance</Text>
                                        </View>
                                    </View>

                                    {/* Cash Amount */}
                                    <Text style={{ color: colors.white, fontSize: 28, fontFamily: fontFamilyStyleNew.bold }} >₹{getTotalCash()}</Text>
                                </View>

                                {/* Separator Line */}
                                <Image style={{ marginTop: 10, width: Constants.SCREEN_WIDTH - 180, height: 1, backgroundColor: '#393C53' }} />

                                {/* Coin View */}
                                <View style={{ marginTop: 10, width: Constants.SCREEN_WIDTH - 180, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >

                                    {/* Coin icon */}
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image style={{ width: 24, height: 21, resizeMode: 'contain' }} source={require('../../assets/images/total-coin-icon.png')} />

                                        {/* Total Coin */}
                                        <View style={{ marginLeft: 8 }} >
                                            <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Total Coin</Text>
                                            <Text style={{ color: colors.white, fontSize: 11, fontFamily: fontFamilyStyleNew.regular }} >Balance</Text>
                                        </View>
                                    </View>

                                    {/* Coin Amount */}
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                        <Image style={{ width: 20, height: 22, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                                        <Text style={{ marginLeft: 2, color: colors.white, fontSize: 28, fontFamily: fontFamilyStyleNew.bold }} >{getTotalCoin()}</Text>
                                    </View>
                                </View>

                            </View>
                        </View>

                        {/* Center View */}
                        <View style={{ position: 'absolute', top: 262, left: 20, right: 20, height: 135, backgroundColor: colors.white, borderWidth: 1, borderColor: '#F5F2F2', borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 30, borderBottomRightRadius: 30, flexDirection: 'row' }} >

                            {/* Deposite Cash */}
                            <View style={{ width: '33.33%', alignItems: 'center' }} >

                                {/* Icon */}
                                <View style={{ marginTop: 12, height: 44, width: 44, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/deposite-cash-new.png')} />
                                </View>

                                {/* Title */}
                                <Text style={{ marginTop: 11, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Deposit Cash</Text>

                                {/* Amount */}
                                <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                                    <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹{getDepositeCash()}</Text>
                                    <TouchableOpacity style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center' }} onPress={() => openCommonInfoPopup('depositCash')} activeOpacity={0.5} >
                                        <Image style={{ width: 14, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/Info-new-icon.png')} />
                                    </TouchableOpacity>
                                </View>

                                {/* Separator Line */}
                                <Image style={{ position: 'absolute', top: 10, bottom: 26, right: 0, width: 1, backgroundColor: '#E5E6E9' }} />
                            </View>

                            {/* Winning Cash */}
                            <View style={{ width: '33.33%', alignItems: 'center' }} >

                                {/* Icon */}
                                <View style={{ marginTop: 12, height: 44, width: 44, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/winning-cash-new.png')} />
                                </View>

                                {/* Title */}
                                <Text style={{ marginTop: 11, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Winning Cash</Text>

                                {/* Amount */}
                                <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                                    <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹{getWinningCash()}</Text>
                                    <TouchableOpacity style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center' }} onPress={() => openCommonInfoPopup('winningCash')} activeOpacity={0.5} >
                                        <Image style={{ width: 14, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/Info-new-icon.png')} />
                                    </TouchableOpacity>
                                </View>

                                {/* Separator Line */}
                                <Image style={{ position: 'absolute', top: 10, bottom: 26, right: 0, width: 1, backgroundColor: '#E5E6E9' }} />
                            </View>

                            {/* Bonus Cash */}
                            <View style={{ width: '33.33%', alignItems: 'center' }} >

                                {/* Icon */}
                                <View style={{ marginTop: 12, height: 44, width: 44, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/bonus-cash-new.png')} />
                                </View>

                                {/* Title */}
                                <Text style={{ marginTop: 11, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Bonus Cash</Text>

                                {/* Amount */}
                                <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                                    <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹{getBonusCash()}</Text>
                                    <TouchableOpacity style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center' }} onPress={() => openCommonInfoPopup('bonusCash')} activeOpacity={0.5} >
                                        <Image style={{ width: 14, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/Info-new-icon.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        {/* Withdrawal */}
                        <TouchableOpacity style={{ position: 'absolute', top: 380, height: 34, width: 96, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.black, borderRadius: 17 }} onPress={() => moveToWithdrawalScreen()} activeOpacity={0.5} >
                            <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Withdrawal</Text>
                        </TouchableOpacity>

                        {/* Bottom View */}
                        <View style={{ marginTop: 100, marginLeft: 20, marginRight: 20, height: 90, justifyContent: 'space-between', flexDirection: 'row' }} >

                            {/* Rewards View */}
                            <TouchableOpacity style={{ width: '32%', alignItems: 'center' }} onPress={() => moveToCoinRewardStoreScreen()} activeOpacity={0.9} >

                                <Image style={{ position: 'absolute', top: 9, left: 0, right: 0, bottom: 0, backgroundColor: colors.black, borderRadius: 10 }} />

                                <View style={{ height: 44, width: 44, backgroundColor: colors.yellow, justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/rewards-new-icon.png')} />
                                </View>
                                <Text style={{ marginTop: 16, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Rewards</Text>
                            </TouchableOpacity>

                            {/* Mobile & Email View */}
                            <TouchableOpacity style={{ width: '32%', height: 90, alignItems: 'center' }} onPress={() => moveToEmailVerifyScreen()} activeOpacity={0.9} >

                                <Image style={{ position: 'absolute', top: 9, left: 0, right: 0, bottom: 0, backgroundColor: colors.black, borderRadius: 10 }} />

                                <View style={{ height: 44, width: 44, backgroundColor: colors.yellow, justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/mobile-email-new.png')} />
                                </View>

                                <View style={{ flex: 1, width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                    {accountResponse && accountResponse.isEmailVerified &&
                                        <Image style={{ marginLeft: 8, marginRight: 8, width: 16, height: 16, resizeMode: 'contain', tintColor: 'white' }} source={require('../../assets/images/ic_phone_verified.png')} />
                                    }
                                    <Text style={{ color: colors.white, flex: 1, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: accountResponse.isEmailVerified ? 'left' : 'center' }} >Mobile & Email</Text>
                                </View>
                            </TouchableOpacity>

                            {/* Withdrawal View */}
                            <TouchableOpacity style={{ width: '32%', height: 90, alignItems: 'center' }} onPress={() => moveToWithdrawalScreen()} activeOpacity={0.9} >

                                <Image style={{ position: 'absolute', top: 9, left: 0, right: 0, bottom: 0, backgroundColor: colors.black, borderRadius: 10 }} />

                                <View style={{ height: 44, width: 44, backgroundColor: colors.yellow, justifyContent: 'center', alignItems: 'center', borderRadius: 22 }} >
                                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/rewards-new-icon.png')} />
                                </View>
                                <Text style={{ marginTop: 16, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Withdrawal</Text>
                            </TouchableOpacity>

                        </View>

                    </ScrollView>
                }

                {isLoading && <CustomProgressbar />}

                {accountResponse && checkIsSponsorAdsEnabled('account') &&
                    <View style={{ marginBottom: 25, backgroundColor: colors.black }}>
                        <SponsorBannerAds screenCode={'account'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('account')} />
                    </View>
                }

            </View>

            <Modal
                isVisible={isOpenCommonInfoPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <CommonInfoPopup popupType={commonInfoPopupType} setOpenCommonInfoPopup={setOpenCommonInfoPopup} />
            </Modal>

        </SafeAreaView>
    )

}

export default accountNew