import produce from 'immer';
import * as actionTypes from '../../actions/joinContest/dobStateValidationAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        updateProfileResponse: undefined,
        stateListResponse: undefined,
        success: true,
        errorMessage: ''
    },
};

{ /* Request - Game Types List */ }
const updateProfile = (state, action) => {
    return produce(state, (draft) => {
        draft.model.updateProfileResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const updateProfileFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.updateProfileResponse = action.model;
    });
};

{ /* Request - Game Types List */ }
const stateList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.stateListResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const stateListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.stateListResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_PROFILE:
            return updateProfile(state, action);
        case actionTypes.UPDATE_PROFILE_FAILED:
            return updateProfileFailed(state, action);
        case actionTypes.STATE_LIST:
            return stateList(state, action);
        case actionTypes.STATE_LIST_FAILED:
            return stateListFailed(state, action);
        case actionTypes.RESET_DOB_STATE_VALIDATION_STATE:
            return resetDobStateValidationState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetDobStateValidationState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.updateProfileResponse = undefined;
        draft.model.stateListResponse = undefined;
    })
}