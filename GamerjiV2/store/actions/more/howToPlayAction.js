import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const howToPlayList = (viewModel) => {
    return {
        type: HOW_TO_PLAY_LIST,
        model: viewModel
    };
};

export const howToPlayListFailed = (viewModel) => {
    return {
        type: HOW_TO_PLAY_LIST_FAILED,
        model: viewModel
    };
};

export const requestHowToPlayList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HOW_TO_PLAY.GET, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(howToPlayList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(howToPlayListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetHowToPlayState = () => {
    return {
        type: RESET_HOW_TO_PLAY_STATE,
    };
};

export const HOW_TO_PLAY_LIST = 'HOW_TO_PLAY_LIST';
export const HOW_TO_PLAY_LIST_FAILED = 'HOW_TO_PLAY_LIST_FAILED';
export const RESET_HOW_TO_PLAY_STATE = 'RESET_HOW_TO_PLAY_STATE';