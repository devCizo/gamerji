import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const leaderboardList = (viewModel) => {
    return {
        type: LEADERBOARD_LIST,
        model: viewModel
    };
};

export const leaderboardListFailed = (viewModel) => {
    return {
        type: LEADERBOARD_LIST_FAILED,
        model: viewModel
    };
};

export const requestLeaderboardList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        if (payload['type'] == 'today') {
            axios
                .post(WebFields.LEADERBOARD.TODAY, payload)
                .then((response) => {
                    addLog("URL:::> ", response.config.baseURL + response.config.url);
                    addLog("Request:::> ", response.config.data);
                    addLog("Response:::> ", response.data);
                    dispatch(leaderboardList(response.data));
                })
                .catch((error) => {
                    addLog(error.response.data);
                    dispatch(leaderboardListFailed(error.response.data));
                })
                .finally(() => {
                    // $('#overlay').hide();
                });
        } else if (payload['type'] == 'byWeek') {
            axios
                .post(WebFields.LEADERBOARD.WEEKLY, payload)
                .then((response) => {
                    addLog("URL:::> ", response.config.baseURL + response.config.url);
                    addLog("Request:::> ", response.config.data);
                    addLog("Response:::> ", response.data);
                    dispatch(leaderboardList(response.data));
                })
                .catch((error) => {
                    addLog(error.response.data);
                    dispatch(leaderboardListFailed(error.response.data));
                })
                .finally(() => {
                    // $('#overlay').hide();
                });
        } else if (payload['type'] == 'monthly') {
            axios
                .post(WebFields.LEADERBOARD.MONTHLY, payload)
                .then((response) => {
                    addLog("URL:::> ", response.config.baseURL + response.config.url);
                    addLog("Request:::> ", response.config.data);
                    addLog("Response:::> ", response.data);
                    dispatch(leaderboardList(response.data));
                })
                .catch((error) => {
                    addLog(error.response.data);
                    dispatch(leaderboardListFailed(error.response.data));
                })
                .finally(() => {
                    // $('#overlay').hide();
                });
        } else if (payload['type'] == 'allTime') {
            axios
                .post(WebFields.LEADERBOARD.ALLOVER, payload)
                .then((response) => {
                    addLog("URL:::> ", response.config.baseURL + response.config.url);
                    addLog("Request:::> ", response.config.data);
                    addLog("Response:::> ", response.data);
                    dispatch(leaderboardList(response.data));
                })
                .catch((error) => {
                    addLog(error.response.data);
                    dispatch(leaderboardListFailed(error.response.data));
                })
                .finally(() => {
                    // $('#overlay').hide();
                });
        }



    };
};

export const LEADERBOARD_LIST = 'LEADERBOARD_LIST';
export const LEADERBOARD_LIST_FAILED = 'LEADERBOARD_LIST_FAILED';

export const resetLeaderboardState = () => {
    return {
        type: RESET_LEADERBOARD_STATE,
    };
};
export const RESET_LEADERBOARD_STATE = 'RESET_LEADERBOARD_STATE';