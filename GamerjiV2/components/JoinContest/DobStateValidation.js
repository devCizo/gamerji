import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, TextInput, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import moment from 'moment';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { AppConstants } from '../../appUtils/appConstants';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import { showErrorToastMessage } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import Modal from 'react-native-modal';
import CustomDatePicker from '../../appUtils/customDatePicker';
import StateListPopup from '../../appUtils/stateListPopup'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const DobStateValidation = (props) => {

    const dispatch = useDispatch();
    var game = props.route.params.game

    const [gamerjiName, setGamerjiName] = useState('')
    const [lockGamerjiNameField, setLockGamerjiNameField] = useState('')

    const [dob, setDOB] = useState('Select your date of birth')
    const [isOpenDatePickerPopup, setIsOpenDatePickerPopup] = useState(false);
    const [lockDobField, setDobFieldLocak] = useState(false)

    const [lockStateField, setStateFieldLock] = useState(false)
    const [stateList, setStateList] = useState([])
    const [selectedState, setSelectedState] = useState(undefined)
    const [isOpenStateListPicker, setOpenStateListPicker] = useState(false)

    const [gameName, setGameName] = useState('')

    const [isLoading, setLoading] = useState(false);

    useEffect(() => {

        // API Call

        setLoading(true)
        dispatch(actionCreators.requestStateList())

        if (global.profile) {
            if (global.profile.gamerjiName) {
                setGamerjiName(global.profile.gamerjiName)
                if (global.profile.gamerjiName != '') {
                    setLockGamerjiNameField(true)
                }
            }
            if (global.profile.dateOfBirth) {
                //  setDOB(global.profile.dateOfBirth)
                setDOB(moment(global.profile.dateOfBirth).format('DD/MM/YYYY'))
                if (global.profile.dateOfBirth != '') {
                    setDobFieldLocak(true)
                }
            }
            if (global.profile.gameNames) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == game._id)
                if (index > -1) {
                    setGameName(global.profile.gameNames[index].uniqueIGN)
                }
            }
        }

        return () => {
            addLog('Unmount')
            dispatch(actionCreators.resetDobStateValidationState())
        }

    }, []);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.dobStateValidation.model }),
        shallowEqual
    );
    var { stateListResponse, updateProfileResponse } = currentState


    // State List Response
    useEffect(() => {
        if (stateListResponse) {
            setLoading(false)
            if (stateListResponse.list) {
                setStateList(stateListResponse.list)

                if (global.profile.address && global.profile.address.state) {
                    let index = stateListResponse.list.findIndex(obj => obj._id == global.profile.address.state)
                    if (index > -1) {
                        setSelectedState(stateListResponse.list[index])
                        setStateFieldLock(true)
                    }
                }
            }
        }
    }, [stateListResponse]);

    //Update Profile Response
    useEffect(() => {
        if (updateProfileResponse) {
            setLoading(false)
            if (updateProfileResponse.item) {

                if (props.route.params.dobStateUpdateSuccess) {
                    props.route.params.dobStateUpdateSuccess()
                }
                RootNavigation.goBack()

                var profile = updateProfileResponse.item
                global.profile = profile

                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(profile))
            }
        }

        updateProfileResponse = undefined

    }, [updateProfileResponse])

    let updateProfileData = () => {

        if (gamerjiName == '') {
            showErrorToastMessage('Please enter your team name')
        } else if (dob == '') {
            showErrorToastMessage('Please select your date of birth')
        } else if (!selectedState) {
            showErrorToastMessage('Please select your state')
        } else if (gameName == '') {
            showErrorToastMessage('Please enter game name')
        } else {
            var gameNames = [...global.profile.gameNames]
            gameNames.push({
                game: game._id,
                uniqueIGN: gameName
            })

            let payload = {
                gamerjiName: gamerjiName,
                dateOfBirth: dob,
                address: {
                    state: selectedState._id
                },
                gameNames: gameNames
            }
            // API Call
            setLoading(true)
            dispatch(actionCreators.requestUpdateProfile(payload))
        }
    }

    const setDOBFromPicker = (dobPicker) => {
        setDOB(moment(dobPicker).format('DD/MM/YYYY'))
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={() => RootNavigation.goBack()} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                <Text style={styles.navigationTitle} >DOB & STATE</Text>

            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {/* Gamerji Username */}
                <View style={{ marginTop: 50, marginLeft: 20, marginRight: 20, }}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Gamerji User Name</Text>

                    {/* <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', paddingHorizontal: 15, fontWeight: '600', fontSize: 16, color: '#70717A', backgroundColor: '#E8E9EB' }} placeholder='Enter Gamerji Username' placeholderTextColor={'#70717A'} >Mortel007</TextInput> */}
                    <TextInput
                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A', backgroundColor: lockGamerjiNameField ? '#E8E9EB' : null }}
                        placeholder='Enter Gamerji Username' placeholderTextColor={'#70717A'}
                        onChangeText={text => setGamerjiName(text.trim())}
                        autoCorrect={false}
                        editable={!lockGamerjiNameField}
                        autoCapitalize='none'
                        maxLength={30}
                    >{gamerjiName}
                    </TextInput>

                </View>

                {/* DOB */}
                <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20, }}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>What's your date of birth?</Text>

                    <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', backgroundColor: lockDobField ? '#E8E9EB' : null }}
                        onPress={() => lockDobField ? null : setIsOpenDatePickerPopup(true)} activeOpacity={lockDobField ? 1 : 0.5} >
                        <Text
                            style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                            {moment(dob).format('DD/MM/YYYY')}
                        </Text>
                        <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />

                    </TouchableOpacity>

                </View>

                {/* State */}

                <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20, }}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Where are you from?</Text>

                    <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', backgroundColor: lockStateField ? '#E8E9EB' : null }}
                        onPress={() => lockStateField ? null : setOpenStateListPicker(true)}
                        activeOpacity={lockStateField ? 1 : 0.5} >

                        <Text
                            style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                            {selectedState ? selectedState.name : 'Select your state'}
                        </Text>

                        <Image style={{ marginRight: 15, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />

                    </TouchableOpacity>
                </View>

                {/* Game name */}
                <View style={{ marginTop: 18, marginLeft: 20, marginRight: 20, }}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Your {game.name} Name</Text>

                    <TextInput
                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                        placeholder={`Enter your ${game.name} name`}
                        placeholderTextColor={'#70717A'}
                        onChangeText={text => setGameName(text)}
                        autoCorrect={false}>{gameName}
                    </TextInput>
                </View>

                {/* Join Contest Button */}
                <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => updateProfileData()} activeOpacity={0.5} >
                    <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >SUBMIT</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                </TouchableOpacity>

                {checkIsSponsorAdsEnabled('dobAndStateValidation') &&
                    <SponsorBannerAds screenCode={'dobAndStateValidation'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('dobAndStateValidation')} />
                }
            </View>

            <Modal
                isVisible={isOpenDatePickerPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <CustomDatePicker setDOBFromPicker={setDOBFromPicker} openDatePickerPopup={setIsOpenDatePickerPopup} mode='date' />
            </Modal>

            {isLoading && <CustomProgressbar />}

            {isOpenStateListPicker &&
                <Modal
                    isVisible={isOpenStateListPicker}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <StateListPopup stateList={stateList} openStateListPicker={setOpenStateListPicker} setSelectedState={setSelectedState} />
                </Modal>
            }

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center'
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
    },
    dateHeaderContainer: {
        height: 45,
        borderBottomWidth: 1,
        borderColor: "#ccc",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    dateHeaderButton: {
        height: "100%",
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    dateHeaderButtonCancel: {
        fontSize: 18,
        color: "#666",
        fontWeight: "400"
    },
    dateHeaderButtonDone: {
        fontSize: 18,
        color: "#006BFF",
        fontWeight: "500"
    },
})

export default DobStateValidation;