import produce from 'immer';
import * as actionTypes from '../../actions/more/applyPromoCodeAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        applyPromoCodeResponse: undefined
    },
    error: false
};

{ /* Request - Apply Promo Code */ }
const applyPromoCodeRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.applyPromoCodeResponse = action.model;
    });
};

{ /* Request Failure - Apply Promo Code */ }
const applyPromoCodeRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.applyPromoCodeResponse = action.model;
    });
};

{ /* Reset State - Apply Promo Code */ }
const resetApplyPromoCodeState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.applyPromoCodeResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_APPLY_PROMO_CODE:
            return applyPromoCodeRequest(state, action);
        case actionTypes.REQUEST_APPLY_PROMO_CODE_FAILED:
            return applyPromoCodeRequestFailed(state, action);
        case actionTypes.RESET_APPLY_PROMO_CODE_STATE:
            return resetApplyPromoCodeState(state);
        default:
            return state;
    }
};

export default reducer;