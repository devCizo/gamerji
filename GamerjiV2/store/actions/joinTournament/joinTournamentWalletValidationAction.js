import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const joinTournament = (viewModel) => {
    return {
        type: JOIN_TOURNAMENT,
        model: viewModel
    };
};

export const joinTournamentFailed = (viewModel) => {
    return {
        type: JOIN_TOURNAMENT_FAILED,
        model: viewModel
    };
};

export const requestJoinTournament = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_CONTEST_TOURNAMENT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(joinTournament(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(joinTournamentFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetJoinTournamentState = () => {
    return {
        type: RESET_JOIN_TOURNAMENT_STATE,
    };
};

export const JOIN_TOURNAMENT = 'JOIN_TOURNAMENT';
export const JOIN_TOURNAMENT_FAILED = 'JOIN_TOURNAMENT_FAILED';
export const RESET_JOIN_TOURNAMENT_STATE = 'RESET_JOIN_TOURNAMENT_STATE';