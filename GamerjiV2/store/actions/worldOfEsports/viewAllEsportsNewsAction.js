import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIEW_ALL_BLOGS_LIST = 'VIEW_ALL_BLOGS_LIST';
export const VIEW_ALL_BLOGS_LIST_FAILED = 'VIEW_ALL_BLOGS_LIST_FAILED';
export const RESET_VIEW_ALL_BLOGS_STATE = 'RESET_VIEW_ALL_BLOGS_STATE';

{ /* Request - View All Blogs List */ }
export const viewAllBlogsList = (viewModel) => {
    return {
        type: VIEW_ALL_BLOGS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - View All Blogs List */ }
export const viewAllBlogsListFailed = (viewModel) => {
    return {
        type: VIEW_ALL_BLOGS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - View All Blogs List */ }
export const resetViewAllBlogsState = () => {
    return {
        type: RESET_VIEW_ALL_BLOGS_STATE,
    };
};

{ /* Request - View All Blogs */ }
export const requestViewAllBlogs = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.WORLD_OF_ESPORTS.MODE_BLOGS_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(viewAllBlogsList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(viewAllBlogsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};