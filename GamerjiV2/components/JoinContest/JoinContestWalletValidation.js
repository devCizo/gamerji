import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import { connect } from 'react-redux';
import moment from 'moment';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CommonInfoPopup from '../../commonComponents/commonInfoPopup';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const JoinContestWalletValidation = (props) => {

    const dispatch = useDispatch();
    const [contest, setContest] = useState(props.contest)
    const [isLoading, setLoading] = useState(false);
    const [openCommonInfoPopup, setOpenCommonInfoPopup] = useState(false);

    var walletUsageLimit = props.walletUsageLimit

    useEffect(() => {

        return () => {
            dispatch(actionCreators.resetJoinContestState())
        }
    }, [])

    let JoinContestValidation = () => {

        // let amountToAdd = 40
        // props.closeWalletValidationPopup()
        // props.moveToAddBalance(amountToAdd)
        // return

        if (walletUsageLimit && walletUsageLimit.joinFlag == false) {

            var amountToAdd = 0
            if (!isCoinContest()) {
                amountToAdd = walletUsageLimit.entryFee - walletUsageLimit.walletBalance
            } else {
                amountToAdd = walletUsageLimit.toBuyCoinAmount
            }
            //     addLog('amountToAdd', amountToAdd)
            props.moveToAddBalance(amountToAdd, isCoinContest())
            props.closeWalletValidationPopup()

        } else if (contest.gameType && contest.gameType.players && Number(contest.gameType.players) > 1) {
            props.moveToSquadRegistration()
            props.closeWalletValidationPopup()
        } else {
            let payload = {
                type: 'contest',
                contest: contest._id,
                isActive: contest.isActive
            }
            if (global.profile && global.profile.gameNames.length > 0) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == contest.game._id)
                if (index > -1) {
                    payload['uniqueIGN'] = global.profile.gameNames[index].uniqueIGN
                }
            }
            setLoading(true)
            dispatch(actionCreators.requestJoinContest(payload))
        }
    }

    // CheckSquad API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.joinContest.model }),
        shallowEqual
    );
    var { joinContestResponse } = currentState

    useEffect(() => {

        if (joinContestResponse) {
            setLoading(false)
            if (joinContestResponse.item) {
                addLog("joinContestResponse===>", joinContestResponse);
                props.setContestAsJoined()
                addLog("closeWalletValidationPopup===>");
                props.closeWalletValidationPopup()
            } else if (joinContestResponse.errors) {
                if (joinContestResponse.errors[0] && joinContestResponse.errors[0].msg) {
                    showErrorToastMessage(joinContestResponse.errors[0].msg)
                }
            }
            joinContestResponse = undefined
            currentState = undefined
        }
    }, [joinContestResponse]);

    const getUtilizeAndWinnings = () => {

        if (walletUsageLimit) {
            if (isCoinContest()) {
                if (walletUsageLimit.coinAmount != null) {
                    return walletUsageLimit.coinAmount
                }
            } else {
                if (walletUsageLimit.walletBalance != null) {
                    return walletUsageLimit.walletBalance
                }
            }
        }
        return 0
    }

    const isFreeContest = () => {
        if (walletUsageLimit.entryFee == '0' || walletUsageLimit.entryFee == '') {
            return true
        }
        return false
    }

    const toPayAmount = () => {
        if (walletUsageLimit.toPay == '0' || walletUsageLimit.toPay == '' || walletUsageLimit.toPay == null) {
            return undefined
        }
        return walletUsageLimit.toPay
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const isCoinContest = () => {
        addLog("contest==>", contest);
        if (contest.currency && contest.currency.code == 'coin') {
            return true
        }
        return false
    }

    const getCurrencyImage = (isWhite) => {
        return (
            isCoinContest() ?
                <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                :
                <Text style={{ color: isWhite ? colors.white : colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{contest.currency.symbol}</Text>
        )
    }

    return (
        <>
            <View style={{}} >

                {contest &&

                    //  Main View 
                    <View style={styles.mainContainer} >
                        {/* Main View */}
                        <View style={styles.mainContainer} >

                            {/* Header View */}
                            <View style={styles.headerContainer}>

                                <Text style={{ marginLeft: 60, color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center', }} numberOfLines={2} >{contest && contest.title}</Text>

                                {/* Close Button */}
                                <TouchableOpacity style={styles.closeButton} onPress={() => props.closeWalletValidationPopup()} activeOpacity={0.5} >
                                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                                </TouchableOpacity>
                            </View>

                            {/* Center Content */}
                            <View style={styles.centerContentContainer} >

                                {/* Game Name & Date */}
                                <View style={{ flexDirection: 'row' }} >
                                    <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular, marginBottom: 0, flex: 1 }} >{contest.game && contest.game.name} - {contest.gameType && contest.gameType.name}</Text>
                                    <Text style={{ color: colors.black, marginRight: 0, fontSize: 14, fontFamily: fontFamilyStyleNew.regular, width: 190, textAlign: 'right' }} >{moment(contest.date).format('DD MMM yyyy')} {moment(contest.time).format('hh:mm A')}</Text>
                                </View>

                                {/* Confimation View */}
                                <View style={{ backgroundColor: colors.red, marginTop: 12 }} >
                                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, marginTop: 10, marginLeft: 12 }}
                                    >Confirmation</Text>
                                    <View style={{ marginTop: 3, marginLeft: 12, marginBottom: 10, flexDirection: 'row', alignItems: 'center' }} >
                                        <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}
                                        >{isCoinContest() ? 'Coin Balance' : 'Unutilized Balance + Winnings'} = </Text>
                                        {getCurrencyImage(true)}
                                        <Text style={{ marginLeft: 2, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}
                                        >{getUtilizeAndWinnings()}</Text>
                                    </View>
                                </View>

                                {/* Entry Fee */}
                                <View style={{ marginTop: 14, flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginBottom: 0 }} >Entry Fee</Text>

                                    {!isFreeContest() ?
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                            {getCurrencyImage()}
                                            <Text style={{ marginLeft: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{walletUsageLimit.entryFee}</Text>
                                        </View>
                                        :
                                        <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Free</Text>
                                    }

                                </View>

                                {/* Usable Cash Bonus */}
                                {!isCoinContest() &&
                                    <View style={{ marginTop: 14, flexDirection: 'row' }} >

                                        <Image style={{ height: 18, width: 18, marginBottom: 0 }} source={require('../../assets/images/usable_cash_bonus_popup.png')} />

                                        <Text style={{ marginLeft: 8, height: 18, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Usable Cash Bonus</Text>

                                        <TouchableOpacity style={{ marginLeft: 6, width: 18, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5} onPress={() => setOpenCommonInfoPopup(true)} >
                                            <Image style={{ height: 14, width: 14 }} source={require('../../assets/images/info_icon_popup.png')} />
                                        </TouchableOpacity>

                                        {isFreeContest() ?
                                            <Text style={{ flex: 1, textAlign: 'right', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >-</Text>
                                            :
                                            <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }} >
                                                {getCurrencyImage()}
                                                <Text style={{ marginLeft: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{walletUsageLimit.cashBalance}</Text>
                                            </View>
                                        }
                                    </View>
                                }

                                {/* Separator Line */}
                                <Image style={{ marginTop: 16, height: 1, backgroundColor: colors.black, opacity: 0.2 }} />

                                {/* To Pay */}
                                <View style={{ marginTop: 14, flexDirection: 'row' }} >

                                    <Image style={{ height: 15, width: 18, marginBottom: 0 }} source={require('../../assets/images/to_pay_popup.png')} />

                                    <Text style={{ marginLeft: 8, height: 18, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >To Pay</Text>

                                    {toPayAmount() ?
                                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }} >
                                            {getCurrencyImage()}
                                            <Text style={{ marginLeft: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{toPayAmount()}</Text>
                                        </View>
                                        :
                                        <Text style={{ flex: 1, textAlign: 'right', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Free</Text>
                                    }
                                </View>

                                {/* Separator Line */}
                                <Image style={{ marginTop: 14, height: 1, backgroundColor: colors.black, opacity: 0.2 }} />

                                {/* Joining T & C */}
                                {contest.currency && contest.currency.joiningNote && contest.currency.joiningNote != '' &&
                                    <Text style={{ marginTop: 14, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }} >{contest.currency.joiningNote}</Text>
                                }

                                {/* Notes */}

                                {contest.game && contest.game.settings && contest.game.settings && contest.game.settings.levelNote && contest.game.settings.levelNote != '' &&
                                    <Text style={{ marginTop: 14, color: colors.red, fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }} >{contest.game.settings.levelNote}</Text>
                                }

                            </View>

                            {/* Join Contest Button */}
                            <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => JoinContestValidation()} activeOpacity={0.5} >

                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >JOIN CONTEST</Text>
                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                            </TouchableOpacity>
                        </View>

                        {checkIsSponsorAdsEnabled('joinContestWalletPopup') &&
                            <SponsorBannerAds screenCode={'joinContestWalletPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('joinContestWalletPopup')} />
                        }
                    </View>
                }

                <Modal
                    isVisible={openCommonInfoPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <CommonInfoPopup popupType={'usableCashBonus'} setOpenCommonInfoPopup={setOpenCommonInfoPopup} />
                </Modal>

            </View>
            {isLoading && <CustomProgressbar />}
        </>
    )
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.yellow,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    centerContentContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
    },
})

export default JoinContestWalletValidation;