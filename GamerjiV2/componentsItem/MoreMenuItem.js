import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { containerStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, heightStyles, widthStyles, otherStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import RightArrowWhiteImage from '../assets/images/ic_right_arrow_black.svg';
import moreMenuData from '../assets/data/moreMenuData';
import { Constants } from '../appUtils/constants';
import { SafeAreaView } from 'react-navigation';
import Modal from 'react-native-modal';
import SelectFavoriteGamePopup from '../components/More/selectFavoriteGamePopup';

const MoreMenuItem = (props) => {

    // Variable Declarations
    const [isLegalityOpen, setIsLegalityOpen] = useState(false);
    const [isOpenSelectFavoriteGame, setIsOpenSelectFavoriteGame] = useState(false);

    // This function should redirect you to the Game Type Screen 
    const redirectToGameTypeScreen = () => {

        const txt = moreMenuData.map(
            (i, arrIndex) => {
                if (arrIndex == props.index) {
                    if (i.title !== Constants.nav_more_legality) {

                        if (i.id == 2 && props.count != 0 && props.status == 2) {
                            // props.navigation.navigate(Constants.nav_more_add_videos, { id: props.id });
                            props.navigation.navigate(Constants.nav_view_all_videos, { isFromMyVideos: true, youTubeChannelLink: props.youTubeChannelLink })
                        } else if (i.id == 3) {
                            props.setOpenJoinViaInviteCodePopup(true);
                        } else if (i.id == 5) {
                            props.setOpenApplyPromoCodePopup(true);
                        } else if (i.id == 9) {
                            setIsOpenSelectFavoriteGame(true);
                        } else {
                            props.navigation.navigate(i.title, {
                                count: props.count,
                                status: props.status,
                                checkStreamerStatus: props.checkStreamerStatus
                            });
                        }
                    } else {
                        setIsLegalityOpen(!isLegalityOpen);
                    }
                }
            }
        );
        return txt;
    }

    return (
        <SafeAreaView>
            <View>
                {/* TouchableOpacity - More Menu Click Event */}
                <TouchableOpacity style={[marginStyles.margin_1, otherStyles.zIndex_1]} onPress={() => redirectToGameTypeScreen()} activeOpacity={1}>

                    {/* Wrapper - Main View */}
                    <View style={[flexDirectionStyles.row, heightStyles.height_50, isLegalityOpen ? colorStyles.redBackgroundColor : colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.bottomMargin_10]}>

                        {/* Wrapper - Menu Image */}
                        <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter, justifyContentStyles.center, marginStyles.leftMargin_10]}>

                            {/* Image - Menu Icons */}
                            <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, resizeModeStyles.resizeMode]} source={props.item.image} />
                        </View>

                        {/* Text - Title */}
                        <Text style={[fontFamilyStyles.semiBold, isLegalityOpen ? colorStyles.whiteColor : colorStyles.blackColor, fontSizeStyles.fontSize_15, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_5]}>{props.item.id == 2 && props.count != 0 && props.status == 2 ? Constants.header_my_videos : props.item.title}</Text>

                        {/* Image - Right Arrow */}
                        {isLegalityOpen ?
                            <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../assets/images/ic_down_arrow_white.png')} />
                            :
                            <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                        }
                    </View>
                </TouchableOpacity>

                {
                    isLegalityOpen &&

                    // Wrapper - Legality Selection View
                    <View style={[heightStyles.height_110, colorStyles.yellowBackgroundColor, borderStyles.borderRadius_10, paddingStyles.topPadding_45, marginStyles.top_45_minus, marginStyles.bottomMargin_30_minus]}>

                        {/* TouchableOpacity - Terms Of Use Menu Click Event */}
                        <TouchableOpacity style={marginStyles.leftMargin_20} activeOpacity={1}
                            onPress={() => props.navigation.navigate(Constants.nav_more_legality, {
                                legalityType: Constants.nav_more_terms_of_use
                            }, setIsLegalityOpen(false))}>

                            {/* Text - Terms Of Use */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_12]}>{Constants.nav_more_terms_of_use}</Text>
                        </TouchableOpacity>

                        {/* Text - Deposit Cash Info Separator */}
                        <Text style={[heightStyles.height_05_new, colorStyles.redBackgroundColor, marginStyles.leftMargin_20, marginStyles.rightMargin_20, marginStyles.margin_10]} />

                        {/* TouchableOpacity - Privacy Policy Menu Click Event */}
                        <TouchableOpacity style={[marginStyles.leftMargin_20]} activeOpacity={1}
                            onPress={() => props.navigation.navigate(Constants.nav_more_legality, {
                                legalityType: Constants.nav_more_privacy_policy
                            }, setIsLegalityOpen(false))}>

                            {/* Text - Privacy Policy */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_12]}>{Constants.nav_more_privacy_policy}</Text>
                        </TouchableOpacity>
                    </View>
                }
            </View>

            {global.allGamesList && global.allGamesList.length != 0 &&
                < Modal
                    isVisible={isOpenSelectFavoriteGame}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SelectFavoriteGamePopup openSelectFavoriteGamePopup={setIsOpenSelectFavoriteGame} />
                </Modal>
            }

        </SafeAreaView>
    );
}

export default MoreMenuItem;