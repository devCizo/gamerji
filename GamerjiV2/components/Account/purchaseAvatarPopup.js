import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { exp } from 'react-native/Libraries/Animated/Easing';
import * as generalSetting from '../../webServices/generalSetting';
import { FlatList } from 'react-native-gesture-handler';
import { Constants } from '../../appUtils/constants';

const PurchaseAvatarPopup = (props) => {

    const selectedAvatarToPurchase = props.selectedAvatarToPurchase
    const purchaseType = props.purchaseType

    useEffect(() => {

    }, [])
    const checkAvatars = (avatar_id) => {
        var avatarArr = [];
        global.profile.avatars.forEach((entry) => {
            if (entry != null) {
                avatarArr.push(entry._id);
            }
        });

        return avatarArr.includes(avatar_id)
    }
    const purchaseClicked = () => {
        props.setOpenPurchaseAvatarPopup(false)
        props.purchaseItemClickedFromPopup(selectedAvatarToPurchase, purchaseType)
    }

    return (
        <>
            <View style={{ backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

                {/* Close Button */}
                <TouchableOpacity style={{ height: 60, aspectRatio: 1, alignSelf: 'flex-end', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.setOpenPurchaseAvatarPopup(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>

                {/* Avatar */}
                {purchaseType == 'bundle' ?
                    <Image style={{ height: 322, width: 209, resizeMode: 'contain', alignSelf: 'center' }} source={selectedAvatarToPurchase.avatar && selectedAvatarToPurchase.avatar.img && selectedAvatarToPurchase.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + selectedAvatarToPurchase.avatar.img.default }} />
                    :
                    <Image style={{ height: 322, width: 209, resizeMode: 'contain', alignSelf: 'center' }} source={selectedAvatarToPurchase.img && selectedAvatarToPurchase.img.default && { uri: generalSetting.UPLOADED_FILE_URL + selectedAvatarToPurchase.img.default }} />
                }

                <View style={{ marginBottom: 42 }}>
                    <View style={{ marginLeft: 20, marginRight: 20, marginBottom: 24, height: purchaseType == 'bundle' ? 110 : 85, backgroundColor: colors.black, borderRadius: 20, alignItems: 'center' }}>
                        <Text style={{ marginTop: 24, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >{purchaseType == 'bundle' ? selectedAvatarToPurchase.avatar.name : selectedAvatarToPurchase.name}</Text>

                        {purchaseType == 'bundle' &&
                            <View style={{ marginTop: 13, flexDirection: 'row', alignItems: 'center' }} >
                                <Image style={{ width: 14, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                                <Text style={{ marginLeft: 5, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >{selectedAvatarToPurchase.coins}</Text>
                                <Text style={{ marginLeft: 2, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} >Coins</Text>
                                <Text style={{ marginLeft: 4, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, alignSelf: 'center' }} >+</Text>
                                <Text style={{ marginLeft: 4, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >{selectedAvatarToPurchase.avatar && selectedAvatarToPurchase.avatar.name}</Text>
                            </View>
                        }
                    </View>
                    {purchaseType == 'bundle' ?
                        <TouchableOpacity style={{ position: 'absolute', bottom: 0, height: 48, width: 156, alignSelf: 'center', borderRadius: 24, backgroundColor: colors.red, alignItems: 'center', justifyContent: 'center' }} activeOpacity={0.8} onPress={() => purchaseClicked()} >
                            <Text style={{ color: colors.white, fontSize: 19, fontFamily: fontFamilyStyleNew.bold }} >{selectedAvatarToPurchase.currency && selectedAvatarToPurchase.currency.symbol}{selectedAvatarToPurchase.amount}</Text>

                        </TouchableOpacity> :
                        checkAvatars(selectedAvatarToPurchase._id) == false ?
                            <TouchableOpacity style={{ position: 'absolute', bottom: 0, height: 48, width: 156, alignSelf: 'center', borderRadius: 24, backgroundColor: colors.red, alignItems: 'center', justifyContent: 'center' }} activeOpacity={0.8} onPress={() => purchaseClicked()} >
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image style={{ width: 24, height: 28, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                                    <Text style={{ marginLeft: 10, color: colors.white, fontSize: 19, fontFamily: fontFamilyStyleNew.bold }} >{selectedAvatarToPurchase.coinAmount}</Text>
                                </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={{ position: 'absolute', bottom: 0, height: 48, width: 156, alignSelf: 'center', borderRadius: 24, backgroundColor: colors.red, alignItems: 'center', justifyContent: 'center' }} activeOpacity={0.8}  >
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                    <Text style={{ marginLeft: 10, color: colors.white, fontSize: 19, fontFamily: fontFamilyStyleNew.bold }} >Bought</Text>
                                </View>
                            </TouchableOpacity>
                    }
                </View>

                {/* <FlatList
                    style={{ marginTop: 4, marginLeft: 20, marginRight: 20, flexGrow: 0 }}
                    data={avatarList}
                    renderItem={({ item, index }) => <TournamentTimingItem item={item} index={index} />}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false} */}
                {/* /> */}
                {/* <FlatList
                    ref={(ref) => nextRoundTimingsFlatListRef = ref}
                    data={avatarList}
                    renderItem={({ item, index }) => <PurchaseAvatarPopupItem item={item} index={index} />}
                    keyExtractor={(item, index) => index.toString()}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                    onMomentumScrollEnd={onAvatarScrollEnd}
                /> */}

            </View>
        </>
    )
}

const PurchaseAvatarPopupItem = props => {

    const item = props.item

    return (
        <View style={{ width: Constants.SCREEN_WIDTH, alignItems: 'center' }}>
            <Image style={{ height: 322, width: 209, resizeMode: 'contain' }} source={item.avatar && item.avatar.img && item.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.avatar.img.default }} />
        </View>
    )
}

export default PurchaseAvatarPopup