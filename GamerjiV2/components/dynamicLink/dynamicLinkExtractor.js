
import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { addLog } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SingleTournamentToJoin from '../joinTournament/singleTournamentToJoin';
import SingleContestToJoin from '../JoinContest/singleContestToJoin';
import Modal from 'react-native-modal';

const DynamicLinkExtractor = (props) => {

    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false)
    const [isOpenContestTournamentPopup, setOpenContestTournamentPopup] = useState(false)
    const [tournamentDetail, setTournamentDetail] = useState(undefined)
    const [contestDetail, setContestDetail] = useState(undefined)

    useEffect(() => {
        if (props.shortCode) {
            let payload = {
                code: props.shortCode
            }
            setLoading(true)
            dispatch(actionCreators.requestGetContestTournamentByDynamicLink(payload))
        }
    }, [])

    const { currentState } = useSelector(
        (state) => ({ currentState: state.dynamicLink.model }),
        shallowEqual
    );
    var { joinViaDynamicLinkResponse } = currentState

    const closeContestTournmentDetailPopup = () => {
        dispatch(actionCreators.resetDynamicLinkState())
        setOpenContestTournamentPopup(false)
        props.setShortCodeForContestTournament(undefined)
    }


    // Join Via Dynamic Link Response
    useEffect(() => {
        if (joinViaDynamicLinkResponse) {
            setLoading(false)
            if (joinViaDynamicLinkResponse.item) {
                if (joinViaDynamicLinkResponse.item.isSingle && joinViaDynamicLinkResponse.item.isSingle == true) {
                    addLog('contest detail')
                    setContestDetail(joinViaDynamicLinkResponse.item)
                    //RootNavigation.navigate(Constants.nav_single_contest_to_join, { contest: joinViaDynamicLinkResponse.item })
                } else {
                    addLog('tournament detail')
                    setTournamentDetail(joinViaDynamicLinkResponse.item)
                    //RootNavigation.navigate(Constants.nav_single_tournament_to_join, { tournament: joinViaDynamicLinkResponse.item })
                }
                setOpenContestTournamentPopup(true)
            } else {
                if (joinViaDynamicLinkResponse.errors && joinViaDynamicLinkResponse.errors[0] && joinViaDynamicLinkResponse.errors[0].msg) {
                    showErrorToastMessage(joinViaDynamicLinkResponse.errors[0].msg)
                }
            }
            // dispatch(actionCreators.resetDynamicLinkState())
            // props.setShortCodeForContestTournament(undefined)
            joinViaDynamicLinkResponse = undefined
        }
    }, [joinViaDynamicLinkResponse]);

    return (
        <>
            {isLoading && <CustomProgressbar />}
            {
                isOpenContestTournamentPopup && contestDetail &&
                <Modal isVisible={true}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SingleContestToJoin contestDetail={contestDetail} setOpenSingleContestPopup={closeContestTournmentDetailPopup} />
                </Modal>
            }
            {
                isOpenContestTournamentPopup && tournamentDetail &&
                <Modal isVisible={true}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SingleTournamentToJoin tournamentDetail={tournamentDetail} setOpenSingleTournamentPopup={closeContestTournmentDetailPopup} />
                </Modal>
            }
        </>
    )
}

export default DynamicLinkExtractor