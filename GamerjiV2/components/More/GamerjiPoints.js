import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, Image, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList } from 'react-native'
import { fontFamilyStyleNew, containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import GamerjiPointsItem from '../../componentsItem/GamerjiPointsItem';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const GamerjiPoints = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [gamerjiPoints, setGamerjiPoints] = useState([]);
    const [gamerjiPointCateories, setGamerjiPointCateories] = useState([]);
    const [activeTab, setActiveTab] = useState(undefined);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 10

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToGamerjiPointCategoryListAPI();



        return () => {
            dispatch(actionCreators.resetGamerjiPointsListState())
        }
    }, [])

    //Tab select
    let selectTabButton = (value) => {
        callToGamerjiPointsListAPI(value);
        setActiveTab(value)

    }
    // This Use-Effect function should call the Gamerji Points API
    const callToGamerjiPointCategoryListAPI = () => {
        let payload = {

            sortBy: 'order',
            sort: 'asc'
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestGamerjiPointCategories(payload))
    }
    // This Use-Effect function should call the Gamerji Points API
    const callToGamerjiPointsListAPI = (pointCategory) => {
        let payload = {
            // skip: gamerjiPoints.length,
            // limit: pageLimit,
            sortBy: 'order',
            sort: 'asc',
            pointCategory: pointCategory

        }

        setIsFooterLoading(true)
        dispatch(actionCreators.requestGamerjiPoints(payload))
    }

    // This function sets the Gamerji Points API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.gamerjiPoints.model }),
        shallowEqual
    );
    var { gamerjiPointsResponse, gamerjiPointCategoriesResponse } = currentState

    useEffect(() => {
        if (gamerjiPointCategoriesResponse && gamerjiPointCategoriesResponse.list) {

            setGamerjiPointCateories(gamerjiPointCategoriesResponse.list)

            callToGamerjiPointsListAPI(gamerjiPointCategoriesResponse.list[0]._id);

            setActiveTab(gamerjiPointCategoriesResponse.list[0]._id)

        }
        gamerjiPointCategoriesResponse = undefined
    }, [gamerjiPointCategoriesResponse])

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (gamerjiPointsResponse) {
            setLoading(false)

            if (gamerjiPointsResponse.count == 0) {
                setTotalRecords(true)
            }

            if (gamerjiPointsResponse.list) {
                // setIsFooterLoading(gamerjiPointsResponse.list.length == 0)
                // var tempArr = gamerjiPointsResponse.list.map(item => {
                //     let tempItem = { ...item }
                //     return tempItem
                // })
                // setGamerjiPoints([...tempArr])
                setGamerjiPoints(gamerjiPointsResponse.list)
            }
        }
        gamerjiPointsResponse = undefined
    }, [gamerjiPointsResponse])

    addLog('gamerjiPointsResponse===>', gamerjiPointsResponse);
    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>


            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Gamerji Points Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_more_gamerji_points} </Text>
                </View>


                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>
                    {/* Tabs View */}
                    <View style={{ marginTop: 15, height: 34, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 17 }} >

                        {gamerjiPointCateories && gamerjiPointCateories.map((category, index) => {

                            return (<TouchableOpacity style={{ width: '33.33%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === category._id ? colors.black : 'transparent' }} onPress={() => selectTabButton(category._id)}>
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >{category.name}</Text>


                            </TouchableOpacity>)
                        })}
                        {/* Today */}
                        {/* <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'today' ? colors.black : 'transparent' }} onPress={() => selectTabButton('today')}>
                            <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Today</Text>
                            <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                        </TouchableOpacity> */}

                        {/* By Week */}
                        {/* <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'byWeek' ? colors.black : 'transparent' }} onPress={() => selectTabButton('byWeek')}>
                            <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >By Week</Text>
                            <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                        </TouchableOpacity> */}

                        {/* Monthly */}
                        {/* <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'monthly' ? colors.black : 'transparent' }} onPress={() => selectTabButton('monthly')}>
                            <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Monthly</Text>
                            <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                        </TouchableOpacity> */}

                        {/* All Time */}
                        {/* <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'allTime' ? colors.black : 'transparent' }} onPress={() => selectTabButton('allTime')}>
                            <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >All Time</Text>
                        </TouchableOpacity> */}
                    </View>

                    {/* Wrapper - Gamerji Points Lists */}
                    {/* <FlatList
                        style={{ marginTop: 10 }}
                        data={gamerjiPoints}
                        renderItem={({ item, index }) => <GamerjiPointsItem item={item} />}
                        keyExtractor={(item) => item.id}
                        horizontal={false}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        onEndReachedThreshold={0.1}
                        onEndReached={() => {
                            if (!isFooterLoading) {
                                //  callToGamerjiPointsListAPI()
                            }
                        }} /> */}
                    {gamerjiPoints && gamerjiPoints.length > 0 ?
                        <FlatList
                            style={{ marginTop: 10 }}
                            data={gamerjiPoints}
                            renderItem={({ item, index }) => <GamerjiPointsItem item={item} />}
                            keyExtractor={(item) => item.id}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    //  callToGamerjiPointsListAPI()
                                }
                            }} />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('gamerjiPoints') &&
                        <SponsorBannerAds screenCode={'gamerjiPoints'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('gamerjiPoints')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}
export default GamerjiPoints;