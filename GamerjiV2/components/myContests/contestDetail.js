import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView, BackHandler, Share, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import ContestCommonDetail from '../JoinContest/ContestCommonDetail';
import Modal from 'react-native-modal';
import ReportIssue from './reportIssue'
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import Clipboard from '@react-native-clipboard/clipboard';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';
import * as generalSetting from '../../webServices/generalSetting'


const ContestDetail = (props) => {

    const dispatch = useDispatch();
    const contestId = props.route.params.contestId
    const [isActiveTab, setIsActiveTab] = useState('details');
    const [contestDetail, setContestDetail] = useState(undefined);
    const [isVissibleReportIssuePopup, setReportIssuePopupVisibility] = useState(false);
    const [providedRatings, setProvidedRatings] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const htmlToFormattedText = require("html-to-formatted-text");

    const [contestUsers, setContestUsers] = useState([])
    const [captainData, setCaptainData] = useState(undefined);

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        dispatch(actionCreators.requestContestDetail(contestId))
        return () => {
            dispatch(actionCreators.resetContestDetailState())
        }

    }, [refreshing]);

    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestContestDetail(contestId))

        return () => {
            dispatch(actionCreators.resetContestDetailState())
        }
    }, []);

    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.contestDetail.model }),
        shallowEqual
    );
    var { contestDetailResponse, rateContestResponse } = currentState

    //Contest Detail Response
    useEffect(() => {
        if (contestDetailResponse) {
            setLoading(false)
            setRefreshing(false)

            if (contestDetailResponse.success == true && contestDetailResponse.item) {
                setContestDetail(contestDetailResponse)
                var contestArr = [];
                //  contestArr[0] = contestDetailResponse.captainData[0];
                var cnt = 1;
                contestDetailResponse.contestUsers.forEach(element => {
                    console.log(element);
                    if (element._id !== contestDetailResponse.captainData[0]._id) {
                        contestArr[cnt] = element;
                        cnt++;
                    }
                    if (element._id === contestDetailResponse.captainData[0]._id) {
                        contestArr[0] = element;

                    }

                });
                if (contestDetailResponse.contestUsers.length === contestArr.length) {
                    setContestUsers(contestArr);
                    setCaptainData(contestArr[0]);

                }

                // setContestUsers(contestDetailResponse.contestUsers)
                //  setCaptainData(contestDetailResponse.captainData)

                if (contestDetailResponse.isRated && contestDetailResponse.isRated == true && contestDetailResponse.contestRate && contestDetailResponse.contestRate[0]) {
                    setProvidedRatings(contestDetailResponse.contestRate[0] && contestDetailResponse.contestRate[0].rate)
                }
            }
        }
        contestDetailResponse = undefined
    }, [contestDetailResponse]);

    // Rate Contest Response
    useEffect(() => {
        if (rateContestResponse && rateContestResponse.success == true && rateContestResponse.item) {
        }
        rateContestResponse = undefined
    }, [rateContestResponse]);

    //Report Issue Submitted
    let issueSubmitted = () => {
        dispatch(actionCreators.requestContestDetail(contestId))
    }

    let getReportIssueTitle = () => {
        if (contestDetail && contestDetail.isReportIssue && contestDetail.isReportIssue == true) {
            if (contestDetail.reportIssue && contestDetail.reportIssue.length > 0 && contestDetail.reportIssue[0].reportIssue) {
                return contestDetail.reportIssue[0].reportIssue.issue
            }
        }
        return 'Report Issue'
    }

    let openReportIssuePopup = () => {
        if (contestDetail && contestDetail.isReportIssue && contestDetail.isReportIssue == true) {
            showErrorToastMessage('You already have submitted issue')
        } else {
            setReportIssuePopupVisibility(true)
        }
    }

    //Rate contest
    let provideRating = (ratings) => {
        let payload = {
            rate: ratings,
            contest: contestDetail.item._id
        }
        dispatch(actionCreators.requestRateContest(payload))
        setProvidedRatings(ratings)
    }

    let getRatedData = () => {

        var rate = 1
        if (providedRatings) {
            rate = providedRatings
        } else if (contestDetail.isRated && contestDetail.isRated == true && contestDetail.contestRate && contestDetail.contestRate[0] && contestDetail.contestRate[0].rate) {
            rate = contestDetail.contestRate[0].rate
        }

        var title = ''
        if (rate == 1) {
            title = 'Very Bad'
        } else if (rate == 2) {
            title = 'Bad'
        } else if (rate == 3) {
            title = 'Average'
        } else if (rate == 4) {
            title = 'Good'
        } else if (rate == 5) {
            title = 'Very Good'
        }
        return title
    }

    const isRoomIdAvailable = () => {
        if (contestDetail.item && contestDetail.item.roomId && contestDetail.item.roomId != '') {
            return true
        }
        return false
    }

    const isPasswordAvailable = () => {
        if (contestDetail.item && contestDetail.item.roomPassword && contestDetail.item.roomPassword != '') {
            return true
        }
        return false
    }

    const copyRoomIdToClipboard = () => {
        if (isRoomIdAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contestDetail.item.roomId)
        }
    }

    const copyPasswordToClipboard = () => {
        if (isPasswordAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contestDetail.item.roomPassword)
        }
    }

    const showReportIssue = () => {
        if (contestDetail && contestDetail.game && contestDetail.game.settings && contestDetail.game.settings.reportIssues && contestDetail.game.settings.reportIssues.length > 0) {
            return true
        }

        return false
    }

    const displayColumn = () => {
        if (contestDetail && contestDetail.column && contestDetail.column != '') {
            return contestDetail.column
        }
    }

    const getRoomIdPasswordWidth = () => {
        if (displayColumn()) {
            return '38%'
        }
        return '50%'
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const shareWithFriends = () => {
        generateDynamicLink()
    }
    const getCurrencyImage = () => {
        return (
            contestDetail.item && contestDetail.item.currency && contestDetail.item.currency.code == 'coin' ?
                <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={contestDetail.item.currency && contestDetail.item.currency.img && contestDetail.item.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contestDetail.item.currency.img.default }} />
                :
                // <Image style={{ height: 12, width: 8, tintColor: colors.white, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                <Text style={{ fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contestDetail.item.currency.symbol}</Text>
        )
    }
    const getCurrencyImageBlack = () => {
        return (
            contestDetail.item && contestDetail.item.currency && contestDetail.item.currency.code == 'coin' ?
                <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={contestDetail.item.currency && contestDetail.item.currency.img && contestDetail.item.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contestDetail.item.currency.img.default }} />
                :
                // <Image style={{ height: 12, width: 8, tintColor: colors.white, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                <Text style={{ fontSize: 12, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{contestDetail.item.currency.symbol}</Text>
        )
    }
    const generateDynamicLink = async () => {

        const link = await dynamicLinks().buildShortLink({
            link: 'https://www.gamerji.com/contestTournamentDetail?shortCode=' + contestDetail.item.shortCode,
            domainUriPrefix: 'https://gamerji.page.link',
            ios: {
                bundleId: 'com.gamerji',
                appStoreId: '1466052584'
            },
            android: {
                packageName: 'com.esports.gamerjipro'
            },
            analytics: {
                campaign: 'banner',
            },
        });

        if (link) {
            addLog('link', link)
            Share.share({
                message: 'Hey! Join me on Gamerji for this exciting match. Click on the following link and let\'s play together\n' + link,
                url: link,
                title: ''
            }, {
                // Android only:
                dialogTitle: '',
                // iOS only:
                excludedActivityTypes: [
                ]
            })
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                <TouchableOpacity style={styles.backButton} onPress={() => props.navigation.goBack()} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>

                {/* Navigation Title */}
                <Text style={[fontFamilyStyles.extraBold, styles.navigationTitle]} numberOfLines={1} >CONTEST DETAILS</Text>

            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {contestDetail &&
                    <View style={{ flex: 1 }} >

                        {/* Tabs View */}
                        <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 20 }} >

                            <TouchableOpacity style={{ width: '33.3%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: isActiveTab === 'details' ? colors.black : 'transparent' }} onPress={() => setIsActiveTab('details')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Details</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '33.3%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: isActiveTab === 'players' ? colors.black : 'transparent' }} onPress={() => setIsActiveTab('players')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Players</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '33.3%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: isActiveTab === 'rules' ? colors.black : 'transparent' }} onPress={() => setIsActiveTab('rules')} activeOpacity={0.5} >
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Rules</Text>
                                <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                            </TouchableOpacity>
                        </View>

                        {isActiveTab === 'details' && (
                            <ScrollView style={{ flex: 1 }}
                                refreshControl={
                                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                }>

                                {/* Contest Detail */}
                                <View style={styles.contestListItemContainer}>

                                    {contestDetail ? <ContestCommonDetail item={contestDetail.item} /> : <Text></Text>
                                    }

                                    {/* Bottom View */}
                                    <View style={styles.contestDetailBottomContainer} >

                                        {/* Room ID */}
                                        <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} onPress={() => copyRoomIdToClipboard()} activeOpacity={0.5} >

                                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Room ID</Text>

                                            <View style={{ marginTop: 2, flexDirection: 'row' }} >
                                                <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{isRoomIdAvailable() ? contestDetail.item.roomId : '-'}</Text>
                                                {isRoomIdAvailable() &&
                                                    <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                                                }
                                            </View>
                                            <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, bottom: 8, right: 0, width: 1 }} />
                                        </TouchableOpacity>

                                        {/* Password */}
                                        <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} onPress={() => copyPasswordToClipboard()} activeOpacity={0.5} >

                                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Password</Text>

                                            <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{isPasswordAvailable() ? contestDetail.item.roomPassword : '-'}</Text>
                                                {isPasswordAvailable() &&
                                                    <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                                                }
                                            </View>
                                        </TouchableOpacity>

                                        {/* Slot */}
                                        {displayColumn() &&
                                            <View style={{ flex: 1, justifyContent: 'center' }} >
                                                <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Slot</Text>
                                                <Text style={{ marginTop: 2, marginLeft: 10, height: 14, width: 30, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold, backgroundColor: colors.black, borderRadius: 3, textAlign: 'center', overflow: 'hidden' }}>{displayColumn()}</Text>
                                            </View>
                                        }
                                    </View>
                                </View>

                                {/* Current Player */}
                                {captainData &&
                                    <View style={{ marginTop: 16, marginRight: 20, marginLeft: 20, borderRadius: 10, overflow: 'hidden' }}>

                                        {/* Current Player Header */}
                                        <ContestPlayerHeader />

                                        {/* Current Player Detail */}
                                        <ContestPlayerItem item={captainData} index={0} contestItem={contestDetail.item} getCurrencyImage={getCurrencyImage} getCurrencyImageBlack={getCurrencyImageBlack} />
                                    </View>
                                }

                                {/* Share And Report */}
                                <View style={{ flexDirection: 'row', marginTop: 16, marginLeft: 20, marginRight: 20, height: 68, justifyContent: 'space-between' }} >

                                    {/* Share With Friend */}
                                    <TouchableOpacity style={{ width: showReportIssue() ? '47%' : '100%', alignItems: 'center' }} onPress={() => shareWithFriends()} activeOpacity={0.9} >
                                        <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, borderRadius: 25, backgroundColor: colors.red }} />
                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/share_with_friend.png')} ></Image>
                                        <Text style={{ marginTop: 4, color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 15, fontFamily: fontFamilyStyleNew.bold }}>Share with Friends</Text>
                                    </TouchableOpacity>

                                    {/* Report Issue */}
                                    {showReportIssue() &&
                                        <TouchableOpacity style={{ width: '47%', alignItems: 'center' }} onPress={() => openReportIssuePopup()} activeOpacity={0.9} >
                                            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, borderRadius: 25, backgroundColor: colors.red }} />
                                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/report_issue.png')} ></Image>
                                            <CustomMarquee style={{ marginTop: 4, marginLeft: 4, marginRight: 4, color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} value={getReportIssueTitle()} />
                                        </TouchableOpacity>
                                    }
                                </View>

                                {/* Provide Ratings */}
                                {!providedRatings ?
                                    <View style={{ marginTop: 26, alignItems: 'center' }} >

                                        <CustomMarquee style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} value={`Rate ` + (contestDetail.item.host && contestDetail.item.host.name)} />

                                        <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'center' }} >

                                            <TouchableOpacity style={{ alignItems: 'center', width: 46 }} onPress={() => provideRating(1)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate1_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Very Bad</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(2)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate2_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Bad</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(3)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate3_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Average</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(4)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate4_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Good</Text> */}
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center', width: 46 }} onPress={() => provideRating(5)} activeOpacity={0.5} >
                                                <Image style={{ width: 32, height: 32 }} source={require('../../assets/images/rate5_icon.png')} />
                                                {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>Very Good</Text> */}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    :
                                    <View style={{ marginTop: 26, alignSelf: 'center', alignItems: 'center' }} >
                                        <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }}>Your Provided Ratings</Text>

                                        {providedRatings == 1 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate1_icon.png')} />
                                        }
                                        {providedRatings == 2 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate2_icon.png')} />
                                        }
                                        {providedRatings == 3 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate3_icon.png')} />
                                        }
                                        {providedRatings == 4 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate4_icon.png')} />
                                        }
                                        {providedRatings == 5 &&
                                            <Image style={{ marginTop: 15, width: 32, height: 32 }} source={require('../../assets/images/rate5_icon.png')} />
                                        }
                                        {/* <Text style={{ marginTop: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }}>{getRatedData()}</Text> */}
                                    </View>
                                }

                            </ScrollView>
                        )}
                        {isActiveTab === 'players' && (
                            <ScrollView style={{ flex: 1 }}
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false}
                                bounces={false}
                                refreshControl={
                                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                }>
                                <View style={{ flex: 1 }} >

                                    {/* Header */}
                                    <View style={{ marginTop: 16, marginRight: 20, marginLeft: 20, borderRadius: 25, overflow: 'hidden' }} >
                                        <ContestPlayerHeader />
                                    </View>

                                    <View style={{ marginTop: 6, marginLeft: 20, marginRight: 20, flex: 1, borderRadius: 20, borderWidth: 1, borderColor: '#DFE4E9', overflow: 'hidden' }}>
                                        <FlatList
                                            style={{}}
                                            data={contestUsers}
                                            renderItem={({ item, index }) => <ContestPlayerItem contestItem={contestDetail.item} item={item} index={index} getCurrencyImage={getCurrencyImage} getCurrencyImageBlack={getCurrencyImageBlack} />}
                                            keyExtractor={(item, index) => index.toString()}
                                        />
                                    </View>

                                </View>
                            </ScrollView>
                        )}
                        {isActiveTab === 'rules' && (
                            <ScrollView style={{ marginTop: 16, marginLeft: 20, marginRight: 20, marginBottom: 10, flex: 1, borderRadius: 20, borderColor: '#DFE4E9', borderWidth: 1 }}>
                                <Text style={{ marginTop: 24, marginLeft: 18, marginRight: 18, marginBottom: 24, color: '#82878D', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }}>{htmlToFormattedText(contestDetail.item && contestDetail.item.rules)}</Text>
                            </ScrollView>
                        )}

                        {/* Open Game */}
                        {/* {contestDetail && contestDetail.game &&
                            <TouchableOpacity style={styles.openGameAppButton} activeOpacity={0.5} >
                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.extraBold, fontSize: 16 }} >OPEN {contestDetail.game && contestDetail.game.name} APP</Text>
                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                            </TouchableOpacity>
                        } */}

                        {checkIsSponsorAdsEnabled('contestDetails') &&
                            <SponsorBannerAds screenCode={'contestDetails'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('contestDetails')} />
                        }
                    </View>
                }
            </View>

            <Modal isVisible={isVissibleReportIssuePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <ReportIssue contestTournament={contestDetail} isContest={true} setReportIssuePopupVisibility={setReportIssuePopupVisibility} issueSubmitted={issueSubmitted} />
            </Modal>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView >
    );
}

const ContestPlayerHeader = () => {
    return (
        <View style={{ height: 50, backgroundColor: colors.yellow, flexDirection: 'row' }} >

            {/* <View style={{ marginTop: 16, marginRight: 20, marginLeft: 20, height: 50, borderRadius: 25, backgroundColor: colors.yellow, flexDirection: 'row' }} > */}

            {/* User name */}
            <View style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ width: 110, alignItems: 'center' }} >
                    <Image style={{ width: 18, height: 18 }} source={require('../../assets/images/username_contest_detail.png')} />
                    <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >User's InGameName</Text>
                </View>
            </View>

            {/* Kills */}
            <View style={{ width: 64, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 18, height: 13 }} source={require('../../assets/images/kills_contest_detail.png')} />
                <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >Kills</Text>
                <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
            </View>

            {/* Rank */}
            <View style={{ right: 0, width: 64, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 18, height: 15 }} source={require('../../assets/images/rank_contest_detail.png')} />
                <Text style={{ color: colors.black, marginTop: 5, fontSize: 9, fontFamily: fontFamilyStyleNew.semiBold }} >Rank</Text>
                <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
            </View>
        </View>
    )
}

const ContestPlayerItem = (props) => {

    const player = props.item
    addLog("props.contestItem.status", props.contestItem.status);
    const openPlayerDetails = () => {
        if (player.user && player.user._id)
            RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: player.user._id })
    }

    if (props.index == 0) {
        return (
            <TouchableOpacity style={{ height: 50, flexDirection: 'row', backgroundColor: colors.black, alignItems: 'center' }} activeOpacity={0.9} onPress={() => openPlayerDetails()} >

                {/* Badge */}
                {/* <Image style={{ marginLeft: 8, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
                {player.level && player.level.featuredImage ?
                    <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                    :
                    <Text></Text>

                }

                <View style={{ marginLeft: 8, flex: 1 }}>

                    {/* Username */}
                    <View style={{}}>
                        <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} value={player.uniqueIGN} />
                    </View>
                    {/* Mobile number */}
                    <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: 'white' }} >XXXXX {player.user && player.user.phone.substr(player.user.phone.length - 5)}</Text>
                    {props.contestItem.status == "completed" ?
                        player.totalAmount > 0 ?
                            <View style={{}}>

                                <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: 'white' }} >
                                    <Image style={{ tintColor: colors.yellow, marginLeft: 8, width: 10, height: 10 }} source={require('../../assets/images/ic_trophy.png')} /> Won {props.getCurrencyImage()} {player.totalAmount}
                                </Text>

                            </View> : <View></View>
                        : <View></View>}
                </View>

                {/* Kills */}
                <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} >{player.kills}</Text>

                {/* Ranks */}
                <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: 'white' }} >{player.rank}</Text>

                {/* Separator line */}
                {/* <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} /> */}

            </TouchableOpacity>
        );
    }

    return (

        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.9} onPress={() => openPlayerDetails()} >

            {/* Badge */}
            {/* <Image style={{ marginLeft: 8, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
            {player.level && player.level.featuredImage ?
                <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                :
                <Text></Text>

            }


            <View style={{ marginLeft: 8, flex: 1 }}>

                {/* Username */}
                <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.user && player.uniqueIGN}</Text>
                {/* Mobile number */}
                <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.black }} >xxxxx {player.user && player.user.name.substr(player.user.name.length - 5)}</Text>
                {props.contestItem.status == "completed" ?
                    player.totalAmount > 0 ?
                        <View style={{}}>

                            <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: colors.black }} >
                                <Image style={{ tintColor: colors.yellow, marginLeft: 8, width: 10, height: 10 }} source={require('../../assets/images/ic_trophy.png')} /> Won {props.getCurrencyImageBlack()} {player.totalAmount}
                            </Text>

                        </View> : <View></View>
                    : <View></View>}
            </View>

            {/* Kills */}
            <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.kills > 0 ? player.kills : player.rank ? 0 : '-'

            }</Text>

            {/* Ranks */}
            <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.rank || '-'}</Text>

            {/* Separator line */}
            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} />

        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50, flexDirection: 'row', justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center'
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
    contestListItemContainer: {
        flex: 1,
        marginTop: 18,
        marginLeft: 20,
        marginRight: 20,
    },
    contestItemMainDataContainer: {
        flex: 1,
        alignSelf: 'stretch',
        marginBottom: 25,
    },
    contestItemTitleView: {
        height: 40,
        backgroundColor: colors.yellow,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    contestItemId: {
        height: 20,
        width: 60,
        backgroundColor: colors.black,
        marginRight: 0,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dateTimeContainer: {
        height: 40,
        backgroundColor: colors.black,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    winningWinnerMainContainer: {
        backgroundColor: colors.yellow,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    winningWinneSubContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    spotsAvailableContainer: {
        marginTop: 5,
        marginLeft: 11,
        marginRight: 11,
        marginBottom: 25,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    contestDetailBottomContainer: {
        position: 'absolute',
        height: 44,
        left: 18,
        right: 18,
        bottom: 0,
        backgroundColor: colors.red,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    openGameAppButton: {
        height: 46,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

{ /* State - Map State To Props */ }
function mapStateToProps(state) {
    return {
        detail: state.contestDetail.model,
        isSuccess: state.contestDetail.sucess,
        isError: state.contestDetail.error,
        errorMessage: state.contestDetail.errorMessage,
    };
}

{ /* Dispatch - Map Dispatch To Props */ }
const mapDispatchToProps = dispatch => ({
    requestContestDetail: data => dispatch(actionCreators.requestContestDetail(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContestDetail, ContestPlayerItem);