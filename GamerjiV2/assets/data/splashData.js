import React from 'react'
import { Constants } from '../../appUtils/constants';
import UsersPlayed from '../../assets/images/ic_joystick.svg';
import SafePayments from '../../assets/images/ic_safe_payments.svg';
import InstantWithdrawals from '../../assets/images/ic_instant_withdrawls.svg';

const splashData = [{
        id: '1',
        image: <UsersPlayed />,
        title: Constants.text_users_played
    },
    {
        id: '2',
        image: <SafePayments />,
        title: Constants.text_safe_payments
    },
    {
        id: '3',
        image: <InstantWithdrawals />,
        title: Constants.text_instant_withdrawals
    }
];

export default splashData;