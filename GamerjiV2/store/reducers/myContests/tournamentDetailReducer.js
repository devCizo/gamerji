import produce from 'immer';
import * as actionTypes from '../../actions/myContests/tournamentDetailAction';

const initialState = {
    model: {
        tournamentDetailResponse: undefined,
        tournamentPlayersResponse: undefined,
    },
    error: false
};

const tournamentDetail = (state, action) => {
    return produce(state, (draft) => {
        draft.model.tournamentDetailResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const tournamentDetailFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.tournamentDetailResponse = action.model;
    });
};

const tournamentPlayers = (state, action) => {
    return produce(state, (draft) => {
        draft.model.tournamentPlayersResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const tournamentPlayersFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.tournamentPlayersResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOURNAMENT_DETAIL:
            return tournamentDetail(state, action);
        case actionTypes.TOURNAMENT_DETAIL_FAILED:
            return tournamentDetailFailed(state, action);
        case actionTypes.TOURNAMENT_PLAYER:
            return tournamentPlayers(state, action);
        case actionTypes.TOURNAMENT_PLAYER_FAILED:
            return tournamentPlayersFailed(state, action);
        case actionTypes.RESET_TOURNAMENT_DETAIL_STATE:
            return resetTournamentDetailState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetTournamentDetailState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.tournamentDetailResponse = undefined;
        draft.model.tournamentPlayersResponse = undefined;
    })
}