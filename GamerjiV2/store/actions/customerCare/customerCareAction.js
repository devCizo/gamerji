import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const ticketList = (viewModel) => {
    return {
        type: TICKET_LIST,
        model: viewModel
    };
};

export const ticketListFailed = (viewModel) => {
    return {
        type: TICKET_LIST_FAILED,
        model: viewModel
    };
};

export const requestTicketList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.TICKET_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(ticketList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(ticketListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const TICKET_LIST = 'TICKET_LIST';
export const TICKET_LIST_FAILED = 'TICKET_LIST_FAILED';

export const resetCustomerCareState = () => {
    return {
        type: RESET_CUSTOMER_CARE_STATE,
    };
};
export const RESET_CUSTOMER_CARE_STATE = 'RESET_CUSTOMER_CARE_STATE';