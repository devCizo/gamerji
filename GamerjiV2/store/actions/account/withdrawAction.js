import axios from '../../../webServices/axios-api';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW = 'REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW';
export const REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW_FAILED = 'REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW_FAILED';
export const REQUEST_WITHDRAW_NOW = 'REQUEST_WITHDRAW_NOW';
export const REQUEST_WITHDRAW_NOW_FAILED = 'REQUEST_WITHDRAW_NOW_FAILED';
export const RESET_WITHDRAW_STATE = 'RESET_WITHDRAW_STATE';

{ /* Request - Get Profile Data For Withdraw */ }
export const getProfileDataForWithdrawRequest = (viewModel) => {
    return {
        type: REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW,
        model: viewModel
    };
};

{ /* Request Failure - Get Profile Data For Withdraw */ }
export const getProfileDataForWithdrawRequestFailed = (viewModel) => {
    return {
        type: REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW_FAILED,
        model: viewModel
    };
};

{ /* Request - Withdraw Now */ }
export const withdrawNowRequest = (viewModel) => {
    return {
        type: REQUEST_WITHDRAW_NOW,
        model: viewModel
    };
};

{ /* Request Failure - Withdraw Now */ }
export const withdrawNowRequestFailed = (viewModel) => {
    return {
        type: REQUEST_WITHDRAW_NOW_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Withdraw */ }
export const resetWithdrawState = () => {
    return {
        type: RESET_WITHDRAW_STATE,
    };
};

{ /* Request - Profile Data For Withdraw */ }
export const requestProfileDataForWithdraw = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ACCOUNT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.profile = response.data.item
                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(getProfileDataForWithdrawRequest(response.data.item));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(getProfileDataForWithdrawRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Withdraw Now */ }
export const requestWithdrawNow = (payload) => {
    addLog("payload:::> ", payload);
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.WITHDRAW_NOW.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(withdrawNowRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(withdrawNowRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};