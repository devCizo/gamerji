import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { containerStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, heightStyles, widthStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import CustomMarquee from '../appUtils/customMarquee';

const GamerjiPointsItem = (props) => {

    // Variable Declarations
    const width = (Dimensions.get('window').width);

    return (
        // Wrapper - Main View 
        <View style={[flexDirectionStyles.row, heightStyles.height_44, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.bottomMargin_15, marginStyles.margin_1]}>

            <View style={[containerStyles.container, flexDirectionStyles.row, justifyContentStyles.spaceBetween]}>

                {/* Text - Title */}
                <View style={[containerStyles.container, flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_12]}>

                    <CustomMarquee value={props.item.name} style={[{ width: width - 150 }, fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_12, alignmentStyles.alignSelfCenter]} />
                </View>

                {/* Text - Points */}
                <Text style={[{ width: 80 }, fontFamilyStyles.bold, colorStyles.redColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, marginStyles.rightMargin_12]}>{props.item.points ? props.item.points + ' ' + Constants.text_points : '-'}</Text>
            </View>
        </View>
    );
}

export default GamerjiPointsItem;