import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, Dimensions, Linking, RefreshControl } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, shadowStyle, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import ChangeUserName from './ChangeUserName'
import Modal from 'react-native-modal';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';

const GameType = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    var [gameTypes, setGameTypes] = useState(undefined)
    const [isLoading, setLoading] = useState(false)
    const [isVissibleChangeUserNamePopup, setChangeUserNamePopup] = useState(false)
    const game = props.route.params.game
    const width = (Dimensions.get('window').width - 40)
    const [refreshing, setRefreshing] = useState(false);

    // This Use-Effect function should call the Game Types API
    useEffect(() => {

        setLoading(true)
        getGameType()

        return () => {
            dispatch(actionCreators.resetGameTypesState())
        }
    }, []);

    const getGameType = () => {
        if (game && game._id) {
            let payload = {
                game: game._id,
                sortby: 'order', sort: 'asc'
            }
            dispatch(actionCreators.requestGameTypes(payload))
        }
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.gameTypes.model }),
        shallowEqual
    );
    var { gameTypesResponse } = currentState

    // This Use-Effect function should set the Game Types Data
    useEffect(() => {

        if (gameTypesResponse) {
            setLoading(false)
            setRefreshing(false)
            var tempGameTypes = []
            if (game && game.isTournament && game.isTournament == true) {
                tempGameTypes.push({ 'isTournament': true, 'name': 'TOURNAMENT', 'featuredImage': game.tournamentFeaturedImage })
            }
            if (gameTypesResponse.list) {
                tempGameTypes.push(...gameTypesResponse.list)
            }
            setGameTypes(tempGameTypes)
        }
        gameTypesResponse = undefined
    }, [gameTypesResponse]);

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setGameTypes([])
        getGameType()

    }, [refreshing]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let openChangeUserNamePopup = () => {
        addLog('openChangeUserNamePopup')
        setChangeUserNamePopup(true)
    }

    let closeUserNamePopup = () => {
        setChangeUserNamePopup(false)
    }

    let moveToTournamentList = () => {
        RootNavigation.navigate(Constants.nav_tournament_list, { game });
        // RootNavigation.navigate(Constants.nav_chart)
    }

    let openApp = () => {
        Linking.canOpenURL('market://details?id=com.pubg.imobile').then(supported => {
            if (supported) {
                console.log('accepted')
                return Linking.openURL('market://details?id=com.pubg.imobile')
            } else {
                console.log('an error occured')
            }
        }).catch(
            console.log('an error occured')
        )
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Game Name Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_05]} numberOfLines={1} ellipsizeMode='tail'> {game && game.name} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}

                    <ScrollView style={[scrollViewStyles.wrapperFullScrollView, positionStyles.relative, { marginBottom: 10, marginTop: 15 }]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >

                        {/* Wrapper - How to Join? */}
                        <TouchableOpacity style={[flexDirectionStyles.row, alignmentStyles.alignSelfCenter, marginStyles.topMargin_03]} onPress={() => RootNavigation.navigate(Constants.nav_how_to_join, { game })} activeOpacity={0.5} >

                            {/* Image - Joystick */}
                            <Image style={[alignmentStyles.alignSelfCenter, { height: 24, width: 24 }]} source={require('../../assets/images/how_to_join.png')} />

                            {/* Text - How to Join? */}
                            <Text style={[fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_04, alignmentStyles.alignSelfCenter, { marginTop: 4, marginLeft: 4 }]}> {Constants.text_how_to_join} </Text>
                        </TouchableOpacity>

                        {/* Wrapper - Game Types Items */}
                        <View style={[marginStyles.topMargin_03, { marginLeft: 5, marginRight: 5 }]}>
                            <FlatList
                                data={gameTypes}
                                renderItem={({ item }) => <GameTypesItem item={item} navigation={props.navigation} moveToTournamentList={moveToTournamentList} />}
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={2}
                            // scrollEnabled={false}
                            />
                        </View>

                        {/* Text - Want to Add / Change PUBG Username? */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_04, alignmentStyles.alignTextCenter, marginStyles.topMargin_02]}> {Constants.text_want_to_add_change} {game && game.name} {Constants.text_add_change_username}</Text>

                        {/* TouchableOpacity - Click Here Button Click Event */}
                        <TouchableOpacity style={[alignmentStyles.alignSelfCenter]} onPress={() => openChangeUserNamePopup()} activeOpacity={0.5} >

                            {/* Text - Button (Click Here) */}
                            <Text style={[fontFamilyStyles.extraBold, colorStyles.redColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, otherStyles.capitalize]}>{Constants.action_click_here}</Text>
                        </TouchableOpacity>
                    </ScrollView>

                    <View style={{ width: '100%' }}>

                        <View style={{ padding: 5 }}>

                            {/* TouchableOpacity - My Contests Button Click Event */}
                            <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, alignmentStyles.alignSelfCenter, marginStyles.bottomMargin_01, { width: width }]}
                                onPress={() => RootNavigation.navigate(Constants.nav_my_contests, { game })} activeOpacity={0.5} >
                                {/* // onPress={() => openApp()} activeOpacity={0.5} > */}

                                {/* Text - Button (My Contests) */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>MY CONTESTS</Text>

                                {/* Image - Right Arrow */}
                                <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_03]} />
                            </TouchableOpacity>
                        </View>

                        {/* {checkIsSponsorAdsEnabled('gameType') &&
                            <SponsorBannerAds screenCode={'gameType'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('gameType')} />
                        } */}
                    </View>
                </View>
            </View>

            <Modal isVisible={isVissibleChangeUserNamePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            // onSwipeComplete={setJoinContestWalletPopup()}
            >
                <ChangeUserName game={game} closeUserNamePopup={closeUserNamePopup} />
            </Modal>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

const GameTypesItem = (props) => {

    const gameType = props.item

    const redirectToContestList = () => {

        if (gameType.isTournament && gameType.isTournament == true) {
            props.moveToTournamentList()
        } else {
            props.navigation.navigate(Constants.nav_contest_list, { gameType });
        }
    }

    return (

        <TouchableOpacity style={{ width: (Dimensions.get('window').width - 10) / 2, height: ((Dimensions.get('window').width - 30) / 2) }} onPress={redirectToContestList} activeOpacity={0.5} >

            <View style={[{ flex: 1, marginTop: 3, marginLeft: 3, marginRight: 3, marginBottom: 3, height: ((Dimensions.get('window').width - 50) / 2), borderRadius: 10, backgroundColor: colors.black, borderColor: colors.color_input_text_border, borderWidth: 0.5, alignItems: 'center' }, shadowStyle.shadow]}>

                {/* <Text style={{ marginTop: 7, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} ellipsizeMode='tail'> {gameType.name} </Text> */}
                <CustomMarquee value={gameType.name} style={{ marginTop: 7, marginLeft: 4, marginRight: 4, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} />

                {/* <View style={{ width: '100%', marginTop: 7, alignItems: 'center', backgroundColor: 'red' }}> */}
                {/* <CustomMarquee value={gameType.name + gameType.name} style={[fontFamilyStyles.bold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_13, marginStyles.leftMargin_5, marginStyles.rightMargin_5, { marginTop: 3 }]} /> */}
                {/* <CustomMarquee style={{ marginLeft: 2, marginRight: 2, fontSize: 16, color: colors.white, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'green' }} value={gameType.name + gameType.name} /> */}
                {/* <CustomMarquee style={{ marginLeft: 2, marginRight: 2, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red' }} value={gameType.name + gameType.name} /> */}
                {/* </View> */}

                {/* <View style={{ marginTop: 7, backgroundColor: 'green', width: '100%', }} >
                    <CustomMarquee style={{ marginLeft: 2, marginRight: 2, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red' }} value={gameType.name + gameType.name} />
                    <CustomMarquee style={{ marginLeft: 2, marginRight: 2, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, backgroundColor: 'red', textAlign: 'center' }} value={gameType.name + gameType.name} />
                </View> */}



                <Image style={{ position: 'absolute', top: 32, left: 3, right: 3, bottom: 3, borderRadius: 4 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + gameType.featuredImage.default }} />

            </View>
        </TouchableOpacity>

        // TouchableOpacity - Game Type Click Event
        // <TouchableOpacity onPress={redirectToContestList} >
        //   <View style={[containerStyles.container, widthStyles.width_100p, heightStyles.height_24, otherStyles.aspectRatio_1, colorStyles.silverBackgroundColor, borderStyles.borderRadius_5, paddingStyles.padding_005]}>

        //     {/* Wrapper - Game Type Main View */}
        //     <View style={[flexDirectionStyles.column, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_5, alignmentStyles.oveflow, paddingStyles.padding_008, otherStyles.aspectRatio_1]}>

        //       {/* Text - Game Type */}
        //       <Text style={[fontFamilyStyles.extraBold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01, marginStyles.topMargin_01, otherStyles.capitalize]} numberOfLines={1} ellipsizeMode='tail'> {props.item.name} </Text>

        //       {/* Image - Game Type */}
        //       <Image style={[positionStyles.absolute, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.top_04, marginStyles.bottom_005, marginStyles.left_01, marginStyles.right_01]}
        //         source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.featuredImage.default }} />
        //     </View>
        //   </View>
        // </TouchableOpacity>
    )
}

export default GameType;