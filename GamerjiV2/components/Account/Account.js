import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowBlackImage from '../../assets/images/ic_right_arrow_black.svg';
import colors from '../../assets/colors/colors';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import Modal from 'react-native-modal';
import CommonInfoPopup from '../../commonComponents/commonInfoPopup';
import * as generalSetting from '../../webServices/generalSetting';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const Account = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [wallet, setWallet] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [totalCashBalance, setTotalCashBalance] = useState(undefined);
    const [hidePANView] = React.useState(false);
    const [isOpenBonusInfoPopup, setIsOpenBonusInfoPopup] = useState(false);
    const [bonusInfo, setBonusInfo] = useState(false);

    // This Use-Effect function should call the Accounts API
    useEffect(() => {
        setLoading(true)
        setAccountsData();

        return () => {
            dispatch(actionCreators.resetAccountState())
        }
    }, []);

    // This function calls the API when coming to this screen for the first time
    // Also calls when coming from the Withdrawal Screen
    function setAccountsData() {
        dispatch(actionCreators.requestAccount())
    };

    // This function sets the Account Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.account.model }),
        shallowEqual
    );
    var { accountResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (accountResponse) {
            setLoading(false)
            setWallet(accountResponse)
            let depositCash = parseFloat(accountResponse.wallet && accountResponse.wallet.depositedAmount);
            let winningCash = parseFloat(accountResponse.wallet && accountResponse.wallet.winningAmount);
            let bonusCash = parseFloat(accountResponse.wallet && accountResponse.wallet.bonusAmount);
            let totalBalance = depositCash + winningCash + bonusCash;
            setTotalCashBalance(totalBalance)
        }
        accountResponse = undefined
    }, [accountResponse])

    // This function should check the validation first and then it sould be redirected to the Withdrawal Screen.
    const checkValidationForWithdraw = () => {
        if (!wallet.isEmailVerified) {
            showErrorToastMessage(Constants.error_verify_email_to_withdraw)
            return;
        }
        props.navigation.navigate(Constants.nav_withdrawal, { setAccountsData: setAccountsData })
    }

    // This function should open up the popup for the winnings information
    let openBonusInfoPopup = (visible, bonusType) => {
        setBonusInfo(bonusType);
        setIsOpenBonusInfoPopup(visible)
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const moveToAddBalanceScreen = () => {
        global.addBalanceScreenType = Constants.wallet_payment_type
        global.addBalanceCurrencyType = Constants.coin_pack_payment
        global.paymentSuccessCompletion = refreshWallet
        props.navigation.navigate(Constants.nav_add_balance)
    }

    const moveToAddCoinScreen = () => {
        global.addBalanceScreenType = Constants.wallet_payment_type
        global.paymentSuccessCompletion = refreshWallet
        props.navigation.navigate(Constants.nav_coin_store)
    }

    const refreshWallet = () => {
        setAccountsData()
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Account Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_05]}> {Constants.header_account} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView style={[bodyStyles.wrapperBodyContent]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Account Info */}
                        <View style={[flexDirectionStyles.row]}>

                            {/* Wrapper - User Profile Info */}
                            <View style={[widthStyles.width_80p]}>

                                {/* Wrapper - User Profile Pic & Name */}
                                <View style={[flexDirectionStyles.row, heightStyles.height_80, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_12, paddingStyles.leftPadding_02]}>

                                    {/* Image - User Profile Pic */}
                                    {!wallet.avatar || wallet.avatar == null ?
                                        <Image style={[borderStyles.borderRadius_12, borderStyles.borderWidth_2, colorStyles.whiteBorderColor, paddingStyles.padding_02, alignmentStyles.alignSelfCenter, { resizeMode: 'cover' }]} source={require('../../assets/images/ic_profile_pic.png')} />
                                        :
                                        <Image style={[borderStyles.borderRadius_12, borderStyles.borderWidth_2, colorStyles.whiteBorderColor, paddingStyles.padding_02, alignmentStyles.alignSelfCenter, { height: 70, width: 70 }]} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.avatar.img.default }} />
                                    }

                                    {/* <Image style={[borderStyles.borderRadius_12, borderStyles.borderWidth_2, colorStyles.whiteBorderColor, paddingStyles.padding_02, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/ic_profile_pic.png')} /> */}

                                    {/* Wrapper - User Profile Info */}
                                    <View style={[flexDirectionStyles.column, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_03,]}>

                                        {/* Text - User Name */}
                                        <CustomMarquee value={wallet.name} style={[widthStyles.width_30, fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_04]} />

                                        {/* TouchableOpacity - Add Balance Button Click Event */}
                                        <TouchableOpacity style={[widthStyles.width_20, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_20, marginStyles.topMargin_01]}
                                            onPress={() => moveToAddBalanceScreen()} activeOpacity={0.5}>

                                            {/* Text - Button (Add Balance) */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_025, alignmentStyles.alignSelfCenter, paddingStyles.padding_005]}>{Constants.action_add_balance}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                            {/* Wrapper - Total Cash Balance */}
                            <View style={[positionStyles.absolute, widthStyles.width_100p, alignmentStyles.alignItemsEnd, alignmentStyles.alignSelfCenter]}>

                                {/* Wrapper - Total Cash Balance View */}
                                <View style={[flexDirectionStyles.row, widthStyles.width_35, heightStyles.height_085, borderStyles.borderRadius_12, colorStyles.redBackgroundColor, paddingStyles.leftPadding_03, paddingStyles.rightPadding_05, paddingStyles.topPadding_01]}>

                                    {/* Wrapper - Total Cash Balance Info */}
                                    <View style={[widthStyles.width_100p, flexDirectionStyles.row]}>

                                        {/* Wrapper - Currency Info */}
                                        <View style={widthStyles.width_20p}>

                                            {/* Image - Currency */}
                                            <Image style={[heightStyles.height_16, widthStyles.width_24, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/ic_currency.png')} />
                                        </View>

                                        {/* Wrapper - Total Cash Balance Info */}
                                        <View style={[widthStyles.width_80p, flexDirectionStyles.column, marginStyles.leftMargin_02]}>

                                            {/* Text - Total Cash Balance */}
                                            <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_03]}>{Constants.text_total_cash_balance}</Text>

                                            {/* Text - Total Cash Balance Amount */}
                                            <Text style={[fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, marginStyles.topMargin_005]} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_rupees}{totalCashBalance && totalCashBalance != null ? totalCashBalance : '0'}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>

                        {/* Wrapper - Total Winnings Cash */}
                        <View style={[positionStyles.relative, flexDirectionStyles.column, borderStyles.borderWidth_1, colorStyles.whiteBackgroundColor, colorStyles.primaryShadowColor, shadowStyles.shadowBackground, borderStyles.borderRadius_10, colorStyles.greyColor, colorStyles.greyBorderColor, marginStyles.topMargin_03, paddingStyles.padding_02]}>

                            {/* Wrapper - Deposit Cash */}
                            <View style={[flexDirectionStyles.row]}>

                                {/* Image - Deposit Cash */}
                                <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_deposit_cash.png')} />

                                {/* Text - Deposit Cash */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_035, marginStyles.leftMargin_03]}>{Constants.text_deposit_cash}</Text>

                                {/* Wrapper - Deposit Cash Info */}
                                <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.right_0, alignmentStyles.alignSelfCenter]}>

                                    {/* Text - Deposit Cash Info Separator */}
                                    <Text style={[heightStyles.height_03, widthStyles.width_2, colorStyles.silverBackgroundColor, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - Deposit Amount */}
                                    <Text style={[widthStyles.width_65, fontFamilyStyles.extraBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_04, marginStyles.leftMargin_02, marginStyles.rightMargin_02, alignmentStyles.alignTextRight]}
                                        numberOfLines={1} ellipsizeMode='tail'>
                                        {Constants.text_rupees}{wallet.wallet && wallet.wallet.depositedAmount ? wallet.wallet.depositedAmount : '0'}</Text>

                                    {/* TouchableOpacity - Deposit Cash Info Button Click */}
                                    <TouchableOpacity style={alignmentStyles.alignSelfCenter} onPress={() => openBonusInfoPopup(true, Constants.text_deposit_cash)} activeOpacity={0.5}>

                                        {/* Image - Info */}
                                        <Image style={[heightStyles.height_14_new, widthStyles.width_14_new, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_info.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* Text - Deposit Cash Separator */}
                            <Text style={[heightStyles.height_001, colorStyles.silverBackgroundColor, marginStyles.topMargin_02]} />

                            {/* Wrapper - Winning Cash */}
                            <View style={[flexDirectionStyles.row, marginStyles.topMargin_02]}>

                                {/* Image - Winning Cash */}
                                <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_winning_cash.png')} />

                                {/* Text - Winning Cash */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_035, marginStyles.leftMargin_03]}>{Constants.text_winning_cash}</Text>

                                {/* Wrapper - Winning Cash Info */}
                                <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.right_0, alignmentStyles.alignSelfCenter]}>

                                    {/* TouchableOpacity - Withdrawal Button Click Event */}
                                    {!wallet.WithdrawalEnabled &&
                                        <TouchableOpacity style={[widthStyles.width_15, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_5, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_02]}
                                            onPress={() =>
                                                checkValidationForWithdraw()}
                                            // moveToAddBalanceScreen()}
                                            activeOpacity={0.5}>

                                            {/* Text - Button (Withdrawal) */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_025, alignmentStyles.alignSelfCenter, paddingStyles.padding_005]}>{Constants.action_withdraw}</Text>
                                        </TouchableOpacity>
                                    }

                                    {/* Text - Winning Cash Info Separator */}
                                    <Text style={[heightStyles.height_03, widthStyles.width_2, colorStyles.silverBackgroundColor, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - Winning Amount */}
                                    <Text style={[widthStyles.width_65, fontFamilyStyles.extraBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_04, marginStyles.leftMargin_02, marginStyles.rightMargin_02, alignmentStyles.alignTextRight]}
                                        numberOfLines={1} ellipsizeMode='tail'>
                                        {Constants.text_rupees}{wallet.wallet && wallet.wallet.winningAmount ? wallet.wallet.winningAmount : '0'}</Text>

                                    {/* TouchableOpacity - Winning Cash Info Button Click */}
                                    <TouchableOpacity style={alignmentStyles.alignSelfCenter} onPress={() =>
                                        openBonusInfoPopup(true, Constants.text_winning_cash)}
                                        activeOpacity={0.5}>

                                        {/* Image - Info */}
                                        <Image style={[heightStyles.height_14_new, widthStyles.width_14_new, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_info.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* Text - Winning Cash Separator */}
                            <Text style={[heightStyles.height_001, colorStyles.silverBackgroundColor, marginStyles.topMargin_02]} />

                            {/* Wrapper - Bonus Cash */}
                            <View style={[flexDirectionStyles.row, marginStyles.topMargin_02]}>

                                {/* Image - Bonus Cash */}
                                <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_bonus_cash.png')} />

                                {/* Text - Bonus Cash */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_035, marginStyles.leftMargin_03]}>{Constants.text_bonus_cash}</Text>

                                {/* Wrapper - Bonus Cash Info */}
                                <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.right_0, alignmentStyles.alignSelfCenter]}>

                                    {/* Text - Bonus Cash Info Separator */}
                                    <Text style={[heightStyles.height_03, widthStyles.width_2, colorStyles.silverBackgroundColor, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - Bonus Amount */}
                                    <Text style={[widthStyles.width_65, fontFamilyStyles.extraBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_04, marginStyles.leftMargin_02, marginStyles.rightMargin_02, alignmentStyles.alignTextRight]}
                                        numberOfLines={1} ellipsizeMode='tail'>
                                        {Constants.text_rupees}{wallet.wallet && wallet.wallet.bonusAmount ? wallet.wallet.bonusAmount : '0'}</Text>

                                    {/* TouchableOpacity - Bonus Cash Info Button Click */}
                                    <TouchableOpacity style={alignmentStyles.alignSelfCenter} onPress={() => openBonusInfoPopup(true, Constants.text_bonus_cash)} activeOpacity={0.5}>

                                        {/* Image - Info */}
                                        <Image style={[heightStyles.height_14_new, widthStyles.width_14_new, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_info.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* Text - Bonus Cash Separator */}
                            <Text style={[heightStyles.height_001, colorStyles.silverBackgroundColor, marginStyles.topMargin_02]} />

                            {/* Wrapper - Coins */}
                            <View style={[flexDirectionStyles.row, marginStyles.topMargin_02]}>

                                {/* Image - Coins */}
                                <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/usable_gamerji_coin_popup.png')} />

                                {/* Text - Coins */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_035, marginStyles.leftMargin_03]}>{Constants.text_gamerji_coins}</Text>

                                {/* Wrapper - Coins Info */}
                                <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.right_0, alignmentStyles.alignSelfCenter]}>

                                    {/* TouchableOpacity - Add Coins Button Click Event */}
                                    <TouchableOpacity style={[widthStyles.width_15, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_5, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_02]}
                                        onPress={() => moveToAddCoinScreen()} activeOpacity={0.5}>

                                        {/* Text - Button (Add Coins) */}
                                        <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_025, alignmentStyles.alignSelfCenter, paddingStyles.padding_005]}>{Constants.action_add_coins}</Text>
                                    </TouchableOpacity>

                                    {/* Text - Coins Info Separator */}
                                    <Text style={[heightStyles.height_03, widthStyles.width_2, colorStyles.silverBackgroundColor, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - Coins Amount */}
                                    <View style={{ width: 82, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >

                                        {/* Coin Image */}
                                        <Image style={{ height: 18, width: 18, marginRight: 4 }} source={require('../../assets/images/usable_gamerji_coin_popup.png')} />

                                        <Text style={[fontFamilyStyles.extraBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_04, marginStyles.rightMargin_02, alignmentStyles.alignTextRight, { alignSelf: 'center' }]}
                                            numberOfLines={1} ellipsizeMode='tail'>
                                            {wallet.wallet && wallet.wallet.coinAmount ? wallet.wallet.coinAmount : '0'}
                                        </Text>
                                    </View>

                                    {/* TouchableOpacity - Bonus Cash Info Button Click */}
                                    <TouchableOpacity style={alignmentStyles.alignSelfCenter} onPress={() => openBonusInfoPopup(true, Constants.text_gamerji_coins)} activeOpacity={0.5}>

                                        {/* Image - Info */}
                                        <Image style={[heightStyles.height_14_new, widthStyles.width_14_new, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_info.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        {/* TouchableOpacity - My Recent Transactions Button Click Event */}
                        <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.topMargin_03]}
                            onPress={() =>
                                props.navigation.navigate(Constants.nav_my_recent_transactions)}
                            activeOpacity={0.5}>

                            {/* Image - My Recent Transactions */}
                            <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, justifyContentStyles.center, marginStyles.leftMargin_16]} source={require('../../assets/images/ic_my_recent_transactions.png')} />

                            {/* Text - Button (My Recent Transactions) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_5, otherStyles.capitalize]}>{Constants.action_my_recent_transactions}</Text>

                            {/* Image - Right Arrow */}
                            <RightArrowBlackImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_04]} />
                        </TouchableOpacity>

                        {/* TouchableOpacity - Withdrawals Button Click Event */}
                        {/* <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.topMargin_02]}
                                onPress={() => props.navigation.navigate(Constants.nav_withdrawal)} activeOpacity={0.5}>

                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>{Constants.action_withdraw}</Text>

                                <RightArrowBlackImage style={[positionStyles.absolute, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_04]} />
                            </TouchableOpacity> */}

                        {/* Wrapper - Mobile & Email, PAN & Bank */}
                        <View style={[flexDirectionStyles.row, marginStyles.topMargin_04, { justifyContent: 'space-between' }]}>

                            {/* Wrapper - Reward Store */}
                            <View style={[widthStyles.width_31p]}>

                                {/* TouchableOpacity - Reward Store Button Click Event */}
                                <TouchableOpacity style={[{ minHeight: 90 }, alignmentStyles.alignItemsCenter, justifyContentStyles.flexEnd, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_12, paddingStyles.padding_03]}
                                    onPress={() => props.navigation.navigate(Constants.nav_reward_store)} activeOpacity={0.5}>

                                    {/* Text - Reward Store */}
                                    <Text style={[fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, marginStyles.bottomMargin_005]}>Reward Store</Text>

                                </TouchableOpacity>

                                {/* Wrapper - Reward Store Rounded View */}
                                <View style={[positionStyles.absolute, widthStyles.width_100p, alignmentStyles.alignItemsCenter]}>

                                    {/* Wrapper - Reward Store Rounded View */}
                                    <View style={[widthStyles.width_13, heightStyles.height_06, borderStyles.borderRadius_50, colorStyles.yellowBackgroundColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignTextCenter, justifyContentStyles.center, marginStyles.top_10_minus]}>

                                    </View>
                                </View>
                            </View>

                            {/* Wrapper - Mobile & Email */}
                            <View style={[widthStyles.width_31p]}>

                                {/* TouchableOpacity - Mobile & Email Button Click Event */}
                                <TouchableOpacity style={[{ minHeight: 90 }, alignmentStyles.alignItemsCenter, justifyContentStyles.flexEnd, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_12, paddingStyles.padding_03]}
                                    onPress={() => props.navigation.navigate(Constants.nav_mobile_add_email_verification, { setAccountsData: setAccountsData })} activeOpacity={0.5}>

                                    {/* Text - Mobile & Email */}
                                    <Text style={[fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, marginStyles.bottomMargin_005]}>{Constants.header_mobile_add_email_verification}</Text>

                                    {/* Text - Mobile & Email Verified*/}
                                    {
                                        wallet.isEmailVerified && wallet.isMobileVerified &&
                                        <Text style={[fontFamilyStyles.bold, colorStyles.whiteBackgroundColor, colorStyles.blackColor, borderStyles.borderRadius_5, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, paddingStyles.leftPadding_01, paddingStyles.rightPadding_01]}>{Constants.text_verified}</Text>
                                    }
                                </TouchableOpacity>

                                {/* Wrapper - Mobile & Email Rounded View */}
                                <View style={[positionStyles.absolute, widthStyles.width_100p, alignmentStyles.alignItemsCenter]}>

                                    {/* Wrapper - Mobile & Email Rounded View */}
                                    <View style={[widthStyles.width_13, heightStyles.height_06, borderStyles.borderRadius_50, colorStyles.yellowBackgroundColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignTextCenter, justifyContentStyles.center, marginStyles.top_10_minus]}>

                                        {/* Image - Mobile & Email */}
                                        <Image style={[heightStyles.height_24, widthStyles.width_19, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_mobile_email.png')} />
                                    </View>
                                </View>
                            </View>

                            {/* Wrapper - PAN */}
                            {hidePANView ? (
                                <View style={[widthStyles.width_31p]}>

                                    {/* TouchableOpacity - PAN Button Click Event */}
                                    <TouchableOpacity style={[heightStyles.height_80, alignmentStyles.alignItemsCenter, justifyContentStyles.flexEnd, colorStyles.redBackgroundColor, borderStyles.borderRadius_12, paddingStyles.padding_03]}
                                        onPress={() => props.navigation.navigate(Constants.nav_pan_verification)} activeOpacity={0.5}>

                                        {/* Text - PAN */}
                                        <Text style={[fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, marginStyles.bottomMargin_005]}>{Constants.header_pan_verification}</Text>
                                    </TouchableOpacity>

                                    {/* Wrapper - PAN Rounded View */}
                                    <View style={[positionStyles.absolute, widthStyles.width_100p, alignmentStyles.alignItemsCenter]}>

                                        {/* Wrapper - PAN Rounded View */}
                                        <View style={[widthStyles.width_13, heightStyles.height_06, borderStyles.borderRadius_50, colorStyles.yellowBackgroundColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignTextCenter, justifyContentStyles.center, marginStyles.top_10_minus]}>

                                            {/* Image - PAN */}
                                            <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_pan_card.png')} />
                                        </View>
                                    </View>
                                </View>
                            ) : null}

                            {/* Wrapper - withdrawal */}
                            {!wallet.WithdrawalEnabled &&
                                <View style={[widthStyles.width_31p]}>

                                    {/* TouchableOpacity - withdrawal Button Click Event */}
                                    <TouchableOpacity style={[{ minHeight: 90 }, alignmentStyles.alignItemsCenter, justifyContentStyles.flexEnd, colorStyles.primaryBackgroundColor, borderStyles.borderRadius_12, paddingStyles.padding_03]}
                                        onPress={() => checkValidationForWithdraw()} activeOpacity={0.5}>

                                        {/* Text - withdrawal */}
                                        <Text style={[fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, marginStyles.bottomMargin_005]}>{Constants.action_withdraw}</Text>

                                        {/* Text - Withdraw Verified Verified*/}
                                        {
                                            global.profile && global.profile.bankInfo && global.profile.bankInfo.status && global.profile.bankInfo.status == 'validate' &&
                                            <Text style={[fontFamilyStyles.bold, colorStyles.whiteBackgroundColor, colorStyles.blackColor, borderStyles.borderRadius_5, fontSizeStyles.fontSize_03, alignmentStyles.alignTextCenter, paddingStyles.leftPadding_01, paddingStyles.rightPadding_01]}>{Constants.text_verified}</Text>
                                        }
                                    </TouchableOpacity>

                                    {/* Wrapper - withdrawal Rounded View */}
                                    <View style={[positionStyles.absolute, widthStyles.width_100p, alignmentStyles.alignItemsCenter]}>

                                        {/* Wrapper - withdrawal Rounded View */}
                                        <View style={[widthStyles.width_13, heightStyles.height_06, borderStyles.borderRadius_50, colorStyles.yellowBackgroundColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignTextCenter, justifyContentStyles.center, marginStyles.top_10_minus]}>

                                            {/* Image - withdrawal */}
                                            <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} source={require('../../assets/images/ic_bonus_cash.png')} />
                                        </View>
                                    </View>
                                </View>
                            }
                        </View>
                    </ScrollView>

                    {checkIsSponsorAdsEnabled('account') &&
                        <SponsorBannerAds screenCode={'account'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('account')} />
                    }
                </View>
            </View>

            <Modal
                isVisible={isOpenBonusInfoPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <CommonInfoPopup infoType={bonusInfo} setOpenInfoPopup={openBonusInfoPopup} />
            </Modal>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default Account;