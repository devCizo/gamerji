import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, Dimensions, ScrollView, Share, StatusBar, RefreshControl, Alert } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew, heightStyles } from '../../appUtils/commonStyles';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Constants } from '../../appUtils/constants';
import { LineChart } from "react-native-chart-kit";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog, checkIsSponsorAdsEnabled, convertNumberToMillions, safeAreaTopHeight, showErrorToastMessage } from '../../appUtils/commonUtlis';
import * as generalSetting from '../../webServices/generalSetting';
import moment from 'moment';
import ModalDropdown from 'react-native-modal-dropdown';
import { AppConstants } from '../../appUtils/appConstants';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import LevelInfoPopup from './LevelInfoPopup';
import { ProgressBar } from 'react-native-paper';
import CustomListPicker from '../../appUtils/customListPicker';
import Modal from 'react-native-modal';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import dynamicLinks from '@react-native-firebase/dynamic-links';

const ProfileScreen = (props) => {


    let activeProfile = 'stats';
    if (props.route.params != null) {
        const prof = props.route.params.activeProfile;
        activeProfile = prof ? 'collegiate' : 'stats';

    }

    // Variable Declarations
    const dispatch = useDispatch();
    const insets = useSafeAreaInsets();
    const [activeTab, setActiveTab] = useState(activeProfile);
    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const [profile, setProfile] = useState(undefined)
    const [statsArr, setStatsArr] = useState([]);
    const [gamesIndex, setGamesIndex] = useState(0);

    const [collegeList, setCollegeList] = useState([]);
    const [collegeToSubmit, setCollegeToSubmit] = useState(undefined)
    const [selectedCollege, setSelectedCollege] = useState(undefined)

    const [isOpenCustomeListPicker, setOpenCustomListPicker] = useState(false)
    const [isOpenLevelList, setOpenLevelList] = useState(false)


    // addLog("props.route.params===>", props);
    //Initialize
    useEffect(() => {

        setLoading(true)
        // addLog("fetchUserProfile==>");
        fetchUserProfile()

        // return () => {
        // }
    }, [props.route]);

    const fetchUserProfile = () => {
        dispatch(actionCreators.requestProfile({}))
    }

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.profile.model }),
        shallowEqual
    );
    var { profileResponse, statsListResponse } = currentState

    const { currentStateCollegiate } = useSelector(
        (state) => ({ currentStateCollegiate: state.collegiate.model }),
        shallowEqual
    );
    var { collegeListResponse, submitCollegeResponse } = currentStateCollegiate

    // Pull To Refresh
    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setStatsArr([])
        setGamesIndex(0)
        fetchUserProfile()

    }, [refreshing]);

    // Profile Response
    useEffect(() => {
        if (profileResponse) {
            setLoading(false)
            setRefreshing(false)
            setProfile(profileResponse)
            addLog("profileResponse.college==>", profileResponse.collegeDetails)
            if (profileResponse && profileResponse.collegeDetails) {
                setSelectedCollege(profileResponse.collegeDetails)
            }

            if (statsArr.length == 0 && profileResponse.gameNames && profileResponse.gameNames.length != 0) {
                requestGameStats(gamesIndex)
            }
            if (collegeList.length == 0) {
                dispatch(actionCreators.requestCollegeList())
            }
        }
    }, [profileResponse])

    //College List Response
    useEffect(() => {
        if (collegeListResponse) {
            setLoading(false)
            if (collegeListResponse.list) {
                setCollegeList(collegeListResponse.list)

                if (profileResponse && profileResponse.college) {
                    let index = collegeListResponse.list.findIndex(obj => obj._id == profileResponse.college)
                    if (index > -1) {
                        setSelectedCollege(collegeListResponse.list[index])
                    }
                }
            }
        }
        collegeListResponse = undefined
    }, [collegeListResponse])

    // Submit College Response
    useEffect(() => {
        if (submitCollegeResponse) {

            setLoading(false)
            if (submitCollegeResponse.item) {
                var profile = { ...submitCollegeResponse.item }
                global.profile = profile
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(profile))

                let index = collegeList.findIndex(obj => obj._id == collegeToSubmit._id)
                if (index > -1) {
                    setSelectedCollege(collegeList[index])

                    RootNavigation.navigate(Constants.nav_college_detail, { collegeDetail: collegeList[index], collegeList: collegeList })
                }
            }
        }
        submitCollegeResponse = undefined
    }, [submitCollegeResponse]);

    //Fetch Game Wise Data
    const requestGameStats = (index) => {

        if (profileResponse && profileResponse.gameNames && profileResponse.gameNames.length > index) {
            var payload = {
                game: profileResponse.gameNames[index].game,
            }
            if (profileResponse.gameNames[index].uniqueIGN != "") {
                dispatch(actionCreators.requestStats(payload))
            }

        }
    }

    //Stats List Response
    useEffect(() => {
        if (statsListResponse && statsListResponse.item) {
            if (profile && profile.gameNames && profile.gameNames.length > gamesIndex) {


                var tempObj = { ...statsListResponse.item }
                tempObj['uniqueIGN'] = profile.gameNames[gamesIndex].uniqueIGN

                var tempArr = [...statsArr]
                tempArr.push(tempObj)

                setStatsArr(tempArr);

                setGamesIndex()
                requestGameStats(gamesIndex + 1)
                setGamesIndex(gamesIndex + 1)
            }
        }
        statsListResponse = undefined
    }, [statsListResponse])

    //  console.log("statsListResponse==>", statsListResponse);

    //Tab select
    let selectTabButton = (value) => {
        setActiveTab(value)
    }

    const clickToShareProfile = () => {
        generateDynamicLink();
    }

    const generateDynamicLink = async () => {
        //  addLog('profile==>', profile);
        const link = await dynamicLinks().buildShortLink({
            link: 'https://www.gamerji.com/userDetail?referralCode=' + profile.referralCode,
            domainUriPrefix: 'https://gamerji.page.link',
            ios: {
                bundleId: 'com.gamerji',
                appStoreId: '1466052584'
            },
            android: {
                packageName: 'com.esports.gamerjipro'
            },
            analytics: {
                campaign: 'banner',
            },
        });

        if (link) {
            addLog('link', link)
            Share.share({
                message: 'Ready to track the competition? Tap  ' + link + ' to start following user on Gamerji now!',
                url: link,
                title: ''
            }, {
                // Android only:
                dialogTitle: '',
                // iOS only:
                excludedActivityTypes: [
                ]
            })
        }
    }

    //Collegiate

    const submitCollege = () => {

        return Alert.alert(
            "Select this college",
            "Are you sure you want to select this college?",
            [
                // The "Yes" button
                {
                    text: "Yes",
                    onPress: () => {
                        if (collegeToSubmit) {
                            setLoading(true)
                            dispatch(actionCreators.requestSubmitCollege({ 'college': collegeToSubmit._id }))
                        } else {
                            showErrorToastMessage('Please select college')
                        }
                    },
                },
                // The "No" button
                // Does nothing but dismiss the dialog when tapped
                {
                    text: "No",
                },
            ]
        );

    }

    const viewCollegeDetails = () => {
        if (selectedCollege) {
            addLog("selectedCollege==>", selectedCollege);
            RootNavigation.navigate(Constants.nav_college_detail, { collegeDetail: selectedCollege, collegeList: collegeList })
        }
    }

    const setCustomListPickerSelectedItem = item => {
        setCollegeToSubmit(item)
    }

    const getLevelProgressValue = () => {
        let currentLevelPoints = profile.level.level.endPoint - profile.level.level.startPoint - 1
        let userPointsAsPerLevel = profile.level.points - profile.level.level.startPoint - 1

        return userPointsAsPerLevel / currentLevelPoints
    }

    return (
        <>
            {/* StatusBar - Show the status bar as per the theme color */}
            {/* <StatusBar backgroundColor={'#FFC609'} /> */}

            <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

                <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, height: safeAreaTopHeight(), backgroundColor: colors.yellow }} />

                {profile &&
                    <ScrollView style={{ backgroundColor: '#FFC609' }}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        bounces={false}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >

                        {/* Navigation Bar */}
                        <View style={{ height: 50, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >


                            <View style={{ marginRight: 15, flexDirection: 'row' }}>
                                {/* Search */}
                                <TouchableOpacity style={{ height: 50, width: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => props.navigation.navigate(Constants.nav_search_user,
                                    { setAccountsData: fetchUserProfile })} >
                                    <Image style={{ height: 24, width: 24 }} source={require('../../assets/images/search-rounded.png')} />
                                    <Text style={{ marginTop: 3, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }} >Search</Text>
                                </TouchableOpacity>
                                {/* Share */}
                                <TouchableOpacity style={{ height: 50, width: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => clickToShareProfile()} >
                                    <Image style={{ height: 24, width: 24 }} source={require('../../assets/images/share-profile-icon.png')} />
                                    <Text style={{ marginTop: 3, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }} >Share</Text>
                                </TouchableOpacity>

                                {/* Edit */}
                                <TouchableOpacity style={{ height: 50, width: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => props.navigation.navigate(Constants.nav_edit_profile,
                                    { setAccountsData: fetchUserProfile })} >
                                    <Image style={{ height: 24, width: 24 }} source={require('../../assets/images/edit-profile-icon.png')} />
                                    <Text style={{ marginTop: 3, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }} >Edit</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ height: 120 }} >

                            {/* Gamerji Name */}
                            <Text style={{ marginTop: 8, marginLeft: 20, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18 }} >{profile.gamerjiName}</Text>

                            <View style={{ marginTop: 16, marginLeft: 20, flexDirection: 'row' }} >

                                {/* Firends */}
                                <TouchableOpacity style={{ height: 30, width: 100, borderRadius: 15, borderColor: "#E4B226", borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_friend_list)} >
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }} >Followers ({profile.followers})</Text>
                                </TouchableOpacity>

                            </View>

                        </View>

                        {/* Medal */}
                        <View style={{ height: 190, backgroundColor: '#FFD23A', justifyContent: 'center' }} >

                            {profile.level && <>
                                <View style={{ width: 166, height: 30, justifyContent: 'center' }} >

                                    <Text style={{ alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Level - {profile.level && profile.level.level && profile.level.level.num}</Text>

                                    {/* View All */}


                                </View>

                                {/* Medal */}
                                <View style={{ marginTop: 5, width: 166, flexDirection: 'row', justifyContent: 'center' }} >
                                    <View>
                                        <Image style={{ height: 57, width: 57, resizeMode: 'contain' }} source={profile.level && profile.level.level && profile.level.level.featuredImage && profile.level.level.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + profile.level.level.featuredImage.default }} />
                                        <Text style={{ marginTop: 6, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{profile.level && profile.level.level && profile.level.level.name}</Text>
                                    </View>
                                </View>

                                {/* Progress Bar */}
                                <View style={{ marginTop: 10, marginLeft: 10, width: 156 }} >
                                    <ProgressBar style={{ height: 5, width: '100%', backgroundColor: 'white', borderRadius: 2.5 }} progress={getLevelProgressValue()} color={colors.black} />
                                </View>

                                {/* Level Points */}
                                <View style={{ marginTop: 4, marginLeft: 10, width: 156, height: 20, flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 12 }} >{profile.level.level.startPoint} Points</Text>
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 12 }} >{profile.level.level.endPoint} Points</Text>
                                </View>

                                <Text style={{ width: 166, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Points - {profile.level && profile.level.points}</Text>

                                {/* {profile.medals && profile.medals.length > 0 ?
                                <View style={{ marginTop: 20, marginLeft: 20, width: 166, flexDirection: 'row' }} >

                                    {profile.medals[0] &&
                                        <View>
                                            <Image style={{ height: 47, width: 47, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.medals[0].img.default }} />
                                            <Text style={{ alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 9 }} >{profile.medals[0].name}</Text>
                                        </View>
                                    }

                                    {profile.medals[1] &&
                                        <View style={{ marginLeft: 8 }} >
                                            <Image style={{ height: 47, width: 47, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.medals[1].img.default }} />
                                            <Text style={{ alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 9 }} >{profile.medals[1].name}</Text>
                                        </View>
                                    }

                                    {profile.medals[2] &&
                                        <View style={{ marginLeft: 8 }} >
                                            <Image style={{ height: 47, width: 47, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.medals[2].img.default }} />
                                            <Text style={{ alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 9 }} >{profile.medals[2].name}</Text>
                                        </View>
                                    }

                                </View>
                                :
                                <Text style={{ marginTop: 18, marginLeft: 20, width: 166, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 18 }} >You haven\'t earned any medal</Text>
                            } */}
                            </>
                            }



                        </View>
                        <TouchableOpacity style={{ position: 'absolute', top: 185, left: 160, height: 15, width: 15, borderRadius: 15, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => setOpenLevelList(true)}>
                            <Image style={{ width: 14, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/Info-new-icon.png')} />
                        </TouchableOpacity>
                        {/* Avatar Image */}
                        {profile.avatar && profile.avatar.img && profile.avatar.img.default &&
                            <Image style={{ position: 'absolute', top: 50, left: 190, width: Constants.SCREEN_WIDTH - 190, height: 365, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.avatar.img.default }} />
                        }

                        {/* Stats */}
                        <View style={{ marginTop: 52, backgroundColor: colors.black, borderTopLeftRadius: 60, borderTopRightRadius: 60 }} >

                            {activeTab == 'stats' &&
                                <>
                                    {statsArr.length > 0 ?
                                        <FlatList
                                            style={{ marginTop: 40, marginBottom: 40, flex: 1 }}
                                            data={statsArr}
                                            renderItem={({ item, index }) => <ProfileStatsItem item={item} />}
                                        />
                                        :
                                        <View>
                                            <Text style={{ marginTop: 100, marginLeft: 20, marginBottom: 300, width: 166, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, alignSelf: 'center' }} >No stats available</Text>
                                        </View>
                                    }
                                </>
                            }

                            {/* Collegiate */}
                            {activeTab == 'collegiate' &&

                                <>
                                    {!selectedCollege ?

                                        //Select College
                                        <View style={{ marginTop: 30, marginLeft: 20, marginRight: 20, marginBottom: 300 }} >

                                            <View style={{ marginTop: 18 }}>

                                                <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, }}>Select your college</Text>

                                                <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between' }} onPress={() => setOpenCustomListPicker(true)} activeOpacity={0.7} >
                                                    <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: colors.white }}>{collegeToSubmit ? collegeToSubmit.name : 'Select college'}</Text>

                                                    <Image style={{ marginRight: 18, height: 12, width: 12, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_arrow_state.png')} />
                                                </TouchableOpacity>
                                            </View>

                                            {/* Submit Button */}
                                            <TouchableOpacity style={{ marginTop: 60, height: 46, flexDirection: 'row', backgroundColor: colors.red, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => submitCollege()} >

                                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >SUBMIT</Text>
                                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                                            </TouchableOpacity>

                                        </View>
                                        :
                                        <View style={{}} >

                                            {/* College Name */}
                                            <Text style={{ marginTop: 60, alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 24, textAlign: 'center' }} >{selectedCollege.name}</Text>

                                            <View style={{ marginTop: 18, marginLeft: 20, width: Constants.SCREEN_WIDTH - 40, flexDirection: 'row', justifyContent: 'space-between' }} >

                                                {/* Rank */}
                                                <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                                    <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                                        <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollege.rank}</Text>

                                                        <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Rank</Text>
                                                    </View>

                                                    <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                                                </View>

                                                {/* Members */}
                                                <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                                    <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: '#43F2FF', borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                                        <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollege.members}</Text>

                                                        <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Members</Text>
                                                    </View>

                                                    <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/members-collegiate.png')} />
                                                </View>
                                            </View>

                                            {/* Points */}
                                            <View style={{ marginTop: 20, width: '48%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>
                                                <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                                    <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollege.points}</Text>

                                                    <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Points</Text>
                                                </View>

                                                <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                                            </View>

                                            <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 60, height: 46, flexDirection: 'row', backgroundColor: colors.red, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => viewCollegeDetails()} >
                                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >VIEW DETAILS</Text>
                                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </>
                            }
                        </View>

                        {/* Tabs View */}
                        <View style={{ position: 'absolute', top: 392, left: 52, right: 52, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.black, borderRadius: 20, borderColor: colors.yellow, borderWidth: 1 }} >

                            {/* Stats */}
                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'stats' ? colors.red : 'transparent' }} onPress={() => selectTabButton('stats')}>
                                <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Stats</Text>
                            </TouchableOpacity>

                            {/* Collegiate */}
                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'collegiate' ? colors.red : 'transparent' }} onPress={() => selectTabButton('collegiate')}>
                                <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Collegiate</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                }

                {profile && checkIsSponsorAdsEnabled('profile') &&
                    <View style={{ marginBottom: 25, backgroundColor: colors.black }}>
                        <SponsorBannerAds screenCode={'profile'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('profile')} />
                    </View>
                }

                {isOpenCustomeListPicker &&
                    <Modal
                        isVisible={true}
                        coverScreen={true}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <CustomListPicker list={collegeList} setCustomListPickerSelectedItem={setCustomListPickerSelectedItem} setOpenCustomListPicker={setOpenCustomListPicker} title={'Select College'} />
                    </Modal>
                }
                {isOpenLevelList &&
                    <Modal
                        isVisible={isOpenLevelList}
                        coverScreen={true}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0, height: 400 }}
                    >
                        <LevelInfoPopup setOpenLevelList={setOpenLevelList}></LevelInfoPopup>

                    </Modal>
                }
            </SafeAreaView>

            {isLoading && <CustomProgressbar />}
            <Image style={{ marginBottom: 0, width: '100%', height: 10, backgroundColor: colors.black }} />
        </>
    )
}

export default ProfileScreen;

const ProfileStatsItem = (props) => {

    //  addLog('ProfileStatsItem', props)

    const item = props.item
    const [selectedGraph, setSelectedGraph] = useState('played')

    const getChartLabels = () => {
        let obj = item.stateatistic.map(element => {
            var day = moment(element.date, ["DD/MM/YYYY", "DD"])
            // return moment().format('lll')
            return moment(day).format('DD')
        })
        return obj
    }

    const getChartValues = () => {
        let obj = item.stateatistic.map(element => {
            if (selectedGraph == 'played') {
                return element.played
            } else {
                return element.ranks
            }
        })
        return obj
    }

    const moveToInsightStatsScreen = () => {
        addLog(item)
        RootNavigation.navigate(Constants.nav_insights_stats_screen, { state: item })
    }

    return (
        <View>
            <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, justifyContent: 'space-between', flexDirection: 'row' }} >

                {/* Game Name */}
                <View style={{ justifyContent: 'center', width: "60%", }} >
                    <Text style={{ color: colors.yellow, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} >{item.game && item.game.name}</Text>
                    <Text style={{ marginTop: 2, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={2} >{item.uniqueIGN}</Text>
                </View>
                <View style={{ marginLeft: 0, justifyContent: 'center', width: "20%", }} >
                    <Text style={{ color: colors.yellow, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} >Rank</Text>
                    <Text style={{ marginTop: 2, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >{item.avgRates}</Text>
                </View>

                {/* Insights */}
                <>
                    {/* <View style={{ overflow: 'hidden' }}>
                        <AnimatedCircularProgress
                            style={{ transform: [{ rotate: '-180deg' }] }}
                            size={150}
                            width={8}
                            fill={item.avgRates / 2}
                            tintColor={colors.yellow}
                            onAnimationComplete={() => console.log('onAnimationComplete')}
                            backgroundColor="#232958"
                            arcSweepAngle={180}
                        />

                        <Text style={{ position: 'absolute', top: 20, alignSelf: 'center', color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >{item.avgRates}</Text>
                        <Text style={{ position: 'absolute', top: 36, alignSelf: 'center', color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >Rank</Text>
                    </View> */}

                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => moveToInsightStatsScreen()} >
                        <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={require('../../assets/images/insights-icon.png')} />
                        <Text style={{ color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} >Insights</Text>
                    </TouchableOpacity>
                </>
            </View>

            <View style={{ marginTop: 16, marginLeft: 10, marginRight: 10, backgroundColor: '#1F2237', borderRadius: 20, overflow: 'hidden' }} >

                <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15, flexDirection: 'row', justifyContent: 'space-between' }} >

                    {/* Played */}
                    <TouchableOpacity onPress={() => setSelectedGraph('played')} >
                        <Text style={{ color: selectedGraph == 'played' ? colors.yellow : colors.white, fontSize: 14, fontFamily: selectedGraph == 'played' ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >Played : {convertNumberToMillions(item.played)}</Text>
                    </TouchableOpacity>

                    {/* Kills */}
                    {/* <TouchableOpacity onPress={() => setSelectedGraph('kills')} >
                        <Text style={{ color: selectedGraph == 'kills' ? colors.yellow : colors.white, fontSize: 14, fontFamily: selectedGraph == 'kills' ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >Rank</Text>
                    </TouchableOpacity> */}
                </View>

                <LineChart
                    style={{}}
                    data={{
                        labels: getChartLabels(),
                        datasets: [
                            {
                                data: getChartValues()
                            }
                        ]
                    }}
                    width={Constants.SCREEN_WIDTH}
                    height={220}
                    yAxisLabel=''
                    yAxisSuffix=''
                    yAxisInterval={1}
                    chartConfig={{
                        backgroundColor: null,
                        backgroundGradientFrom: '#1F2237',
                        backgroundGradientTo: null,
                        decimalPlaces: 0,
                        color: (opacity = 1) => colors.yellow,//`rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => colors.white,
                        style: {
                            borderRadius: 16
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "0",
                            // stroke: colors.blue
                        },

                        fillShadowGradient: colors.yellow,
                        fillShadowGradientOpacity: 0.5
                    }}
                    bezier
                    style={{
                        marginLeft: -10,
                        marginTop: 20
                    }}

                    // withDots={false}
                    withInnerLines={false}
                    withOuterLines={false}
                // withHorizontalLabels={false}
                />

                <Text style={{ position: 'absolute', top: 120, left: selectedGraph == 'played' ? -10 : -2, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >{selectedGraph == 'played' ? 'Played' : 'Rank'}</Text>

                {/* <Text style={{ position: 'absolute', top: 120, left: -2, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >Kills</Text> */}
                <View style={{ position: 'relative', padding: 10, backgroundColor: '#1F2237', alignItems: "center" }}>
                    <Text style={{ color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, alignContent: "center" }} >Last 14 Days</Text>

                </View>

            </View>
        </View>
    )
}