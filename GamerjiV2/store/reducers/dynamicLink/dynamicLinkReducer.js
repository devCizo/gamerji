import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/dynamicLink/dynamicLinkAction';

const initialState = {
    model: {
        joinViaDynamicLinkResponse: undefined,
    },
    error: false
};

const getContestTournamentByDynamicLink = (state, action) => {
    return produce(state, (draft) => {
        draft.model.joinViaDynamicLinkResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getContestTournamentByDynamicLinkFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.joinViaDynamicLinkResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK:
            return getContestTournamentByDynamicLink(state, action);
        case actionTypes.GET_CONTEST_TOURNAMENT_BY_DYNAMIC_LINK_FAILED:
            return getContestTournamentByDynamicLinkFailed(state, action);
        case actionTypes.RESET_DYNAMIC_LINK_STATE:
            return resetDynamicLinkState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetDynamicLinkState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.joinViaDynamicLinkResponse = undefined;
    })
}