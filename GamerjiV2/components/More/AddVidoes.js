import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, TextInput, ScrollView, Keyboard } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import WebFields from '../../webServices/webFields.json';
import { showSuccessToastMessage, showErrorToastMessage, addLog } from '../../appUtils/commonUtlis';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';

const AddVidoes = (props, navigation) => {

    // Variable Declarations
    const data = props.route.params;
    const dispatch = useDispatch();
    const [videoTitle, setVideoTitle] = useState('');
    const [videoLink, setVideoLink] = useState('');
    const [isLoading, setLoading] = useState(false);

    // This function should check the validation first and then call the Add Videos API
    const checkValidation = () => {
        Keyboard.dismiss();
        if (!videoTitle.trim()) {
            showErrorToastMessage(Constants.error_enter_video_title)
            return;
        } else if (!videoLink.trim()) {
            showErrorToastMessage(Constants.error_enter_video_link)
            return;
        }
        callToAddVideosAPI(props);
    };

    // This function should call the Add Vidoes Request API
    const callToAddVideosAPI = (props) => {
        let payload = {
            videoRequest: data.id,
            youTubeName: videoTitle,
            youTubeLink: videoLink
        }
        setLoading(true)
        dispatch(actionCreators.requestAddVideos(payload))
    }

    // This function sets the Add Videos Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.addVideos.model }),
        shallowEqual
    );
    var { addVideosResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (addVideosResponse) {
            setLoading(false)
            if (addVideosResponse.success) {
                showSuccessToastMessage(Constants.success_video_submitted)
                setVideoTitle('');
                setVideoLink('');

                return () => {
                    dispatch(actionCreators.resetAddVideosState())
                }
            } else if (addVideosResponse.errors) {
                if (addVideosResponse.errors[0] && addVideosResponse.errors[0].msg) {
                    showErrorToastMessage(addVideosResponse.errors[0].msg)
                    setVideoTitle('');
                    setVideoLink('');

                    return () => {
                        dispatch(actionCreators.resetAddVideosState())
                    }
                }
            }
        }
        addVideosResponse = undefined
    }, [addVideosResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5} >

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Add Videos Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_more_add_videos} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Text - Video Title */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_25]}> {Constants.text_video_title} </Text>

                        {/* Text Input - Video Title  */}
                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                            placeholder={Constants.validation_video_title}
                            autoFocus={true}
                            value={videoTitle}
                            onChangeText={text => setVideoTitle(text)} />

                        {/* Text - Video Link */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_video_link} </Text>

                        {/* Text Input - Video Link */}
                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                            placeholder={Constants.validation_video_link}
                            value={videoLink}
                            onChangeText={text => setVideoLink(text)} />
                    </ScrollView>

                    {/* TouchableOpacity - Submit Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20]}
                        onPress={() => checkValidation()} activeOpacity={0.5}>

                        {/* Text - Button (Submit) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default AddVidoes;