import React, { useEffect, useState, useRef } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, Image, Dimensions, Platform, FlatList } from 'react-native';
import * as actionCreators from '../store/actions/index';
import { addLog } from '../appUtils/commonUtlis';
import { onConnectionStateUpdate } from './SocketIO';
import SponsorAdsItem from '../componentsItem/SponsorAdsItem';
import * as generalSetting from '../webServices/generalSetting'
import { Constants } from '../appUtils/constants';
import { AppConstants } from '../appUtils/appConstants';


const SponsorBannerAds = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [sponsorAds, setSponsorAds] = useState([]);
    const [bannersFlatListIndex, setBannersFlatListIndex] = useState(0)
    var bannersFlatListIndexTemp = 0
    var bannersCount = 0
    var bannersFlatListRef = React.useRef(null);
    var tempBannercount = 0
    var currentIndex = 0
    // const socket = io(generalSetting.SOCKET_URL)

    // This Use-Effect function should call the Sponsor Ads API
    useEffect(() => {
        if (props.checkIsSponsorAdsEnabled) {
            if (props.checkIsSponsorAdsEnabled.isGamerjiAds && props.checkIsSponsorAdsEnabled.isGamerjiAds == true) {
                let payload = {
                    skip: 0,
                    limit: 10,
                    sort: "asc",
                    screen: props.checkIsSponsorAdsEnabled._id
                }
                dispatch(actionCreators.requestSponsorAds(payload, props.checkIsSponsorAdsEnabled.code))

                return () => {
                    dispatch(actionCreators.resetSponsorAdsState(props.checkIsSponsorAdsEnabled.code))
                    // socket.close
                }
            } else if (props.checkIsSponsorAdsEnabled.isFbAds && props.checkIsSponsorAdsEnabled.isFbAds == true) {
                let tempArr = [{ isFbAds: true }]
                setSponsorAds(tempArr)
            }
        }
    }, []);

    // This function sets the Sponsor Ads API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.sponsorAds.model }),
        shallowEqual
    );
    var { sponsorAdsListResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code] && sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list && sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list.length > 0) {

            var tempArr = [...sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list]
            if (props.checkIsSponsorAdsEnabled.isFbAds && props.checkIsSponsorAdsEnabled.isFbAds == true) {
                tempArr.push({ isFbAds: true })
            }

            if (props.checkIsSponsorAdsEnabled.isFbAds && props.checkIsSponsorAdsEnabled.isFbAds == true) {
                bannersCount = sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list.length + 1
            } else {
                bannersCount = sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list.length
                tempBannercount = sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list.length
            }
            setSponsorAds(tempArr)

            if (tempBannercount > 1 || (props.checkIsSponsorAdsEnabled.isFbAds && props.checkIsSponsorAdsEnabled.isFbAds == true)) {
                setTimeout(() => {
                    scheduleTimerTask()
                }, 5000)
            } else {
                sendAdsAnalytics(sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code].list[0]._id)
            }
        }
        sponsorAdsListResponse[props.checkIsSponsorAdsEnabled.code] = undefined
    }, [sponsorAdsListResponse])

    const scheduleTimerTask = () => {
        if (currentIndex == tempBannercount) {
            currentIndex = 0;
        }

        setInterval(() => {
            setAutoScrollBanner()
        }, 5000)
    }

    const sendAdsAnalytics = (adId) => {
        var params = {
            type: AppConstants.TYPE_SPONSOR_ADS_IMPRESSIONS,
            user: global.profile && global.profile._id,
            screen: props.checkIsSponsorAdsEnabled && props.checkIsSponsorAdsEnabled._id,
            sponsorAd: adId
        }

        dispatch(actionCreators.requestSponsorAdsLog(params));
        // socket.emit("gsaLog", params)
    }

    const setAutoScrollBanner = () => {
        if (bannersFlatListIndexTemp >= bannersCount) {
            bannersFlatListIndexTemp = 0
        }
        bannersFlatListIndexTemp = bannersFlatListIndexTemp + 1
        setBannersFlatListIndex(bannersFlatListIndexTemp)
    }

    let onBannersFlatListScrollEnd = (e) => {
        let pageNumber = Math.min(Math.max(Math.floor(e.nativeEvent.contentOffset.x / (Constants.SCREEN_WIDTH - 40) + 0.5), 0), sponsorAds.length);
        bannersFlatListIndexTemp = pageNumber
        setBannersFlatListIndex(pageNumber)
    }

    useEffect(() => {
        if (sponsorAds.length == 0) {
            return
        }
        if (bannersFlatListIndex >= sponsorAds.length) {
            setBannersFlatListIndex(0)
            return
        }
        if (bannersFlatListRef) {
            bannersFlatListRef.scrollToIndex({ animated: true, index: bannersFlatListIndex })

            if (!sponsorAds[bannersFlatListIndex].isFbAds) {
                sendAdsAnalytics(sponsorAds[bannersFlatListIndex]._id)
            }
        }
    }, [bannersFlatListIndex])

    return (
        <View>
            <View style={{ width: '100%', flexDirectionStyles: 'row', height: 60 }}>

                <FlatList
                    data={sponsorAds}
                    renderItem={({ item, index }) => <SponsorAdsItem item={item} index={index} checkIsSponsorAdsEnabled={props.checkIsSponsorAdsEnabled} />}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    ref={(ref) => bannersFlatListRef = ref}
                    pagingEnabled={true}
                    // scrollEnabled={false}
                    onMomentumScrollEnd={onBannersFlatListScrollEnd} />
            </View>
        </View>
    )
}

export default SponsorBannerAds;