import produce from 'immer';
import * as actionTypes from '../../actions/customerCare/customerCareAction';

const initialState = {
    model: {
        ticketListResponse: undefined,
    },
    error: false
};

const ticketList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.ticketListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const ticketListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.ticketListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TICKET_LIST:
            return ticketList(state, action);
        case actionTypes.TICKET_LIST_FAILED:
            return ticketListFailed(state, action);
        case actionTypes.RESET_CUSTOMER_CARE_STATE:
            return resetCustomerCareState(state);
        default:
            return state;
    }
};

export default reducer;

const resetCustomerCareState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.ticketListResponse = undefined;
    })
}