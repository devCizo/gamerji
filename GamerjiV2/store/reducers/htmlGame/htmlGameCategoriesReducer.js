import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/htmlGame/htmlGameCategoriesAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        html5CategoriesResponse: undefined,
    },
    error: false
};

{ /* Request - Game Types List */ }
const html5CategoriesList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.html5CategoriesResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const html5CategoriesListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.html5CategoriesResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HTML5GAME_CATEGORIES_LIST:
            return html5CategoriesList(state, action);
        case actionTypes.HTML5GAME_CATEGORIES_FAILED:
            return html5CategoriesListFailed(state, action);
        case actionTypes.RESET_HTML5GAME_CATEGORIES_STATE:
            return resetHtml5CategoriesState(state);
        default:
            return state;
    }
};

export default (reducer);

const resetHtml5CategoriesState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.html5CategoriesResponse = undefined;
    })
}