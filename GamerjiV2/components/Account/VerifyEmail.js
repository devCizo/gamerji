import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, ScrollView, SafeAreaView, Keyboard, Image, TextInput } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, fontStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import WebFields from '../../webServices/webFields.json';
import { Constants } from '../../appUtils/constants';
import OtpInputs from '../../commonComponents/OtpInputs';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import { showSuccessToastMessage, showErrorToastMessage, addLog } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import { AppConstants } from '../../appUtils/appConstants';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import CustomProgressbar from '../../appUtils/customProgressBar';

const VerifyEmail = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const data = props.route.params
    const [otp, setOtp] = useState('')
    const [timerCount, setTimer] = useState(Constants.INT_OTP_TIMEOUT);
    const [isLoading, setLoading] = useState(false)

    const onChangeOtpText = otp => {
        setOtp(otp)
        if (otp.length == 6) {
            addLog('success')
            callToOTPVerficationAPI(otp)
        }
    }

    // This function should call the OTP API to be verified
    const callToOTPVerficationAPI = (otp) => {
        if (otp.length >= 6) {
            Keyboard.dismiss();
            let payload = {
                type: WebFields.EMAIL_VERIFY_OTP.REQUEST_VALIDATE_OTP,
                email: data.email,
                otp: otp
            }
            setLoading(true)
            dispatch(actionCreators.verifyEmailRequestOTP(payload))
        }
    }

    // This function should call the Resend OTP API
    const callToResendOTPAPI = () => {
        Keyboard.dismiss();
        let payload = {
            type: WebFields.EMAIL_VERIFY_OTP.REQUEST_OTP_REQUEST,
            email: data.email,
        }
        setLoading(true)
        dispatch(actionCreators.verifyEmailRequestOTP(payload))
        showHideResendOTP();
    };

    // This function sets the OTP Request API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.verifyEmail.model }),
        shallowEqual
    );
    var { verifyEmailResponse } = currentState

    // This function checks the response and then it should be redirected to the OTP screen
    useEffect(() => {
        if (verifyEmailResponse) {
            setLoading(false)
            if (verifyEmailResponse.success && verifyEmailResponse.type == 'validateOTP') {

                let profile = { ...global.profile }
                profile['isEmailVerified'] = true;

                global.profile = profile;
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(profile));
                props.navigation.pop(2)
                data.setAccountsData()

                return () => {
                    dispatch(actionCreators.resetVerifyEmailState())
                }
            } else if (verifyEmailResponse.errors) {
                if (verifyEmailResponse.errors[0] && verifyEmailResponse.errors[0].msg) {
                    showErrorToastMessage(verifyEmailResponse.errors[0].msg)
                }
            }
        }
        verifyEmailResponse = undefined
    }, [verifyEmailResponse]);

    // This function should set the OTP Timer of 30 seconds.
    const setCountDownTimer = () => {
        setTimer(timerCount - 1);
    }

    // This function should show/hide Resend OTP Button Visibility.
    const showHideResendOTP = () => {
        setTimer(Constants.INT_OTP_TIMEOUT)
    }

    // This function should check if the timer is not 0 then it should set the OTP Timer again
    useEffect(() => {
        if (timerCount !== 0) {
            setTimeout(() => {
                setCountDownTimer();
            }, 1000);
        }
    }, [timerCount]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Verify OTP Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_verify_email} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Image - User Authentication */}
                        {/* <Image style={[heightStyles.height_190, widthStyles.width_301, alignmentStyles.alignSelfCenter, marginStyles.topMargin_40, marginStyles.bottomMargin_10]} source={require('../../assets/images/otp_bg.png')} /> */}

                        {/* Text - Enter OTP Header */}
                        <Text style={[fontFamilyStyles.bold, colorStyles.blackColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_30, marginStyles.topMargin_38]}> {Constants.text_enter_otp} </Text>

                        {/* Text - We have sent an OTP to your email */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]}> {Constants.text_sent_otp_on_email} </Text>

                        {/* Text - Email */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.bold, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_10]}> {data.email} </Text>

                        {/* Text - Enter the OTP you received */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]}> {Constants.text_otp_received} </Text>

                        {/* Wrapper - OTP Inputs */}
                        <View style={[containerStyles.container, flexDirectionStyles.row, alignmentStyles.alignSelfCenter, marginStyles.topMargin_20]}>

                            {/* Text Input - Array Class Component */}
                            <TextInput style={{ marginTop: 20, height: 44, width: 200, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', alignSelf: 'center', textAlign: 'center', fontSize: 18, fontFamily: fontFamilyStyleNew.bold, color: colors.black }}
                                placeholder={'Enter OTP'}
                                onChangeText={text => onChangeOtpText(text)}
                                keyboardType='number-pad'
                                maxLength={6}
                            >
                                {otp}
                            </TextInput>
                            {/* <OtpInputs getOtp={(otp) => callToOTPVerficationAPI(otp)} /> */}
                        </View>

                        {/* Text - Didn't receive the OTP? */}
                        <Text style={[colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_20]}> {Constants.text_otp_did_not_received} </Text>

                        {/* Text - Request for a new one in 30 Seconds */}
                        {timerCount != 0 ? <Text style={[alignmentStyles.alignSelfCenter, colorStyles.blackColor, fontFamilyStyles.regular, fontSizeStyles.fontSize_14, marginStyles.topMargin_10]}> {Constants.text_request_new_otp} {timerCount} Seconds</Text> : null
                        }

                        {/* TouchableOpacity - Resend OTP Button Click Event */}
                        <TouchableOpacity style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, marginStyles.topMargin_15]} disabled={timerCount == 0 ? false : true} onPress={callToResendOTPAPI} activeOpacity={0.5}>

                            {/* Text - Resend OTP */}
                            <Text style={[fontFamilyStyles.bold, timerCount == 0 ? colorStyles.redColor : colorStyles.silverColor, fontSizeStyles.fontSize_16, fontStyles.underline, timerCount == 0 ? colorStyles.redUnderlineColor : colorStyles.silverUnderlineColor]} > {Constants.action_resend_otp} </Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default VerifyEmail;