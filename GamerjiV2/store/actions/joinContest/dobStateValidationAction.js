
import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const updateProfile = (viewModel) => {
    return {
        type: UPDATE_PROFILE,
        model: viewModel
    };
};

export const updateProfileFailed = (viewModel) => {
    return {
        type: UPDATE_PROFILE_FAILED,
        model: viewModel
    };
};

export const stateList = (viewModel) => {
    return {
        type: STATE_LIST,
        model: viewModel
    };
};

export const stateListFailed = (viewModel) => {
    return {
        type: STATE_LIST_FAILED,
        model: viewModel
    };
};

export const requestUpdateProfile = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(updateProfile(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(updateProfileFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const requestStateList = (payload) => {

    addLog('requestStateList')

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.STATE_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(stateList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(stateListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_PROFILE_FAILED = 'UPDATE_PROFILE_FAILED';
export const STATE_LIST = 'STATE_LIST';
export const STATE_LIST_FAILED = 'STATE_LIST_FAILED';


export const resetDobStateValidationState = () => {
    return {
        type: RESET_DOB_STATE_VALIDATION_STATE,
    };
};

export const RESET_DOB_STATE_VALIDATION_STATE = 'RESET_DOB_STATE_VALIDATION_STATE';