import produce from 'immer';
import * as actionTypes from '../../actions/joinContest/JoinContestAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        joinContestResponse: undefined,
        success: true,
        errorMessage: ''
    },
    error: false
};

{ /* Request - Game Types List */ }
const joinContest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.joinContestResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const joinContestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.joinContestResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.JOIN_CONTEST:
            return joinContest(state, action);
        case actionTypes.JOIN_CONTEST_FAILED:
            return joinContestFailed(state, action);
        case actionTypes.RESET_JOIN_CONTEST_STATE:
            return resetJoinContestState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetJoinContestState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.joinContestResponse = undefined;
    })
}