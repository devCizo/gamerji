import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, Platform, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar';
import { PermissionsAndroid } from 'react-native';
import Contacts from 'react-native-contacts';
import ContactsListItem from '../../componentsItem/ContactsListItem';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';

const FriendList = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();

    const [followerList, setFollowerList] = useState([])
    const [followerListFetchingStatus, setFollowerListFetchingStatus] = useState(false);

    const [followingList, setFollowingList] = useState([])
    const [followingListFetchingStatus, setFollowingListFetchingStatus] = useState(false);

    const [isLoading, setLoading] = useState(false);
    const [activeTab, setActiveTab] = useState('followers');
    const [refreshing, setRefreshing] = useState(false);

    const [followerCount, setFollowerCount] = useState(global.profile.followers ? global.profile.followers : 0)
    const [followingCount, setFollowingCount] = useState(global.profile.followings ? global.profile.followings : 0)


    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        if (activeTab == 'followers') {
            setFollowerList([])
            getFollowerList()
        }
        if (activeTab == 'following') {
            setFollowingList([])
            getFollowingList()
        }

        return () => {
            dispatch(actionCreators.resetFriendsState())
        }
    }, [refreshing]);

    useEffect(() => {
        setLoading(true)
        getFollowerList()

        return () => {
            dispatch(actionCreators.resetFriendsState())
        }
    }, [])

    const getFollowerList = () => {

        // if (followerList.length == 0) {
        //     setLoading(true)
        // }
        setFollowerListFetchingStatus(true)

        let payload = {
            skip: followerList.length,
            limit: 20
        }
        dispatch(actionCreators.requestFollowerList(payload))
    }

    const getFollowingList = () => {

        // if (followingList.length == 0) {
        //     setLoading(true)
        // }
        setFollowingListFetchingStatus(true)

        let payload = {
            skip: followingList.length,
            limit: 20
        }
        dispatch(actionCreators.requestFollowingList(payload))
    }

    // API Response
    const { currentStateFriends } = useSelector(
        (state) => ({ currentStateFriends: state.friendsState.model }),
        shallowEqual
    );
    var { followerListResponse, followingListResponse, followUserResponse } = currentStateFriends

    // Follower List Response
    useEffect(() => {
        if (followerListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (followerListResponse.list) {
                setFollowerListFetchingStatus(followerListResponse.list.length == 0)

                var tempArr = followerListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setFollowerList([...followerList, ...tempArr])
            }
        }
        followerListResponse = undefined
    }, [followerListResponse])

    // Following List Response
    useEffect(() => {
        if (followingListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (followingListResponse.list) {
                setFollowingListFetchingStatus(followingListResponse.list.length == 0)

                var tempArr = followingListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setFollowingList([...followingList, ...tempArr])
            }
        }
        followingListResponse = undefined
    }, [followingListResponse])

    // Tab select
    let selectTabButton = (value) => {
        setActiveTab(value)
        if (value == 'followers') {
            if (followerList.length == 0) {
                setLoading(true)
                getFollowerList()
            }
        } else if (value == 'following') {
            if (followingList.length == 0) {
                setLoading(true)
                getFollowingList()
            }
        }
    }

    const removeUnFollowUser = arrIndex => {

        var payload = {}

        if (activeTab == 'followers') {
            payload['action'] = 'removefollow'
            payload['followingUser'] = followerList[arrIndex].user._id

            var tempArr = [...followerList]
            tempArr.splice(arrIndex, 1)
            setFollowerList(tempArr)
            setFollowerCount(followerCount - 1)
        } else {
            payload['action'] = 'unfollow'
            payload['followingUser'] = followingList[arrIndex].followingUser._id

            var tempArr = [...followingList]
            tempArr.splice(arrIndex, 1)
            setFollowingList(tempArr)
            setFollowingCount(followingCount - 1)
        }
        dispatch(actionCreators.requestFollowUser(payload))
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }} >

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', justifyContent: 'flex-start' }} >

                {/* Back Button */}
                {<TouchableOpacity style={{ width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={{ width: 25, height: 23 }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: 'white', fontFamily: fontFamilyStyleNew.bold, fontSize: 20, textAlign: 'center', alignSelf: 'center', }} numberOfLines={1} >FRIENDS</Text>
            </View>

            {/* Container View */}
            <View style={{ flex: 1, marginTop: 0, backgroundColor: 'white', borderTopStartRadius: 40, borderTopEndRadius: 40, justifyContent: 'space-around', }}>

                {/* Tabs View */}
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.black, borderRadius: 20 }} >

                    {/* Followers */}
                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'followers' ? colors.red : 'transparent' }} onPress={() => selectTabButton('followers')}>
                        <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Followers ({followerCount})</Text>
                    </TouchableOpacity>

                    {/* Following */}
                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'following' ? colors.red : 'transparent' }} onPress={() => selectTabButton('following')}>
                        <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Following ({followingCount})</Text>
                    </TouchableOpacity>

                    {/* Contacts */}
                    {/* <TouchableOpacity style={{ width: '33%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'contacts' ? colors.red : 'transparent' }} onPress={() => selectTabButton('contacts')}>
                        <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Contacts</Text>
                    </TouchableOpacity> */}
                </View>

                {/* {activeTab != 'contacts' ? */}
                <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }} >
                    <FlatList
                        style={{}}
                        data={activeTab == 'followers' ? followerList : followingList}
                        renderItem={({ item, index }) => <FriendListItem item={item} index={index} selectedTab={activeTab} removeUnFollowUser={removeUnFollowUser} />}
                        onEndReachedThreshold={0.1}
                        onEndReached={() => {
                            addLog('onEndReached')
                            if (activeTab == 'followers' && !followerListFetchingStatus) {
                                getFollowerList()
                            }
                            if (activeTab == 'following' && !followingListFetchingStatus) {
                                getFollowingList()
                            }
                        }}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    />
                </View>
                {/* :
                    <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }} >
                        <FlatList
                            data={activeTab == 'followers' ? followerList : followingList}
                            renderItem={({ item, index }) => <ContactsListItem item={item} />}
                            numColumns={1}
                            keyExtractor={(item, index) => index}
                        />
                    </View>
                } */}
            </View>

            {checkIsSponsorAdsEnabled('friends') &&
                <SponsorBannerAds screenCode={'friends'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('friends')} />
            }

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const FriendListItem = (props) => {

    const user = props.item

    const removeUnFollowUser = () => {
        props.removeUnFollowUser(props.index)
    }

    return (
        <View style={{ marginTop: 6, marginLeft: 20, marginRight: 20, marginBottom: 6, height: 50, borderRadius: 25, borderColor: '#D5D7E3', borderWidth: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end' }} >

            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10, flex: 1 }}>
                {/* <Image style={{ marginLeft: 12, width: 30, height: 30 }} source={require('../../assets/images/badge_dummy_player.png')} /> */}
                {user.level && user.level.featuredImage ?
                    <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + user.level.featuredImage.default }} />
                    :
                    <Text></Text>
                }
                <View style={{ marginLeft: 12, flex: 1 }}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: user.user._id })} >
                        {props.selectedTab == 'followers' ?
                            <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.black }} value={(user.user && user.user.gamerjiName)} />
                            :
                            <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.black }} value={(user.followingUser && user.followingUser.gamerjiName)} />
                        }
                    </TouchableOpacity>
                </View>

            </View>

            <TouchableOpacity style={{ marginRight: 12, width: 90, height: 30, borderRadius: 15, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', alignItems: 'center' }} onPress={() => removeUnFollowUser()} activeOpacity={0.5} >
                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, color: colors.black }} numberOfLines={1} >{props.selectedTab == 'followers' ? 'Remove' : 'Remove'}</Text>
            </TouchableOpacity>
        </View >
    )
}

export default FriendList;