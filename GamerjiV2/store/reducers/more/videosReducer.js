import produce from 'immer';
import * as actionTypes from '../../actions/more/videosAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        videosResponse: undefined
    },
    error: false
};

{ /* Request - Video List */ }
const videoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.videosResponse = action.model;
    });
};

{ /* Request Failure - Video List */ }
const videoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.videosResponse = action.model;
    });
};

{ /* Reset State - Videos List */ }
const resetVideosListState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.videosResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIDEO_LIST:
            return videoList(state, action);
        case actionTypes.VIDEO_LIST_FAILED:
            return videoListFailed(state, action);
        case actionTypes.RESET_VIDEOS_LIST_STATE:
            return resetVideosListState(state);
        default:
            return state;
    }
};

export default reducer;