import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Image, ScrollView, SafeAreaView, TextInput, Keyboard, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import WebFields from '../../webServices/webFields.json';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import { addLog, showSuccessToastMessage, showErrorToastMessage, validateEmail, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const MobileAndEmailVerification = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const data = props.route.params;
    const [email, setEmail] = useState(global.profile.email);
    const [isLoading, setLoading] = useState(false)
    const { height } = Dimensions.get('window');

    // This function should check the validation first and then redirected to the Verify OTP Screen.
    const checkValidation = () => {
        if (email == '') {
            showErrorToastMessage(Constants.error_enter_email);
            return;
        } else if (!validateEmail(email)) {
            showErrorToastMessage(Constants.error_enter_valid_email);
            return;
        }
        callToVerifyOTPRequestAPI(props);
    };

    // This function should call the Verify OTP Request API for an Email
    const callToVerifyOTPRequestAPI = () => {
        Keyboard.dismiss();
        let payload = {
            type: WebFields.EMAIL_VERIFY_OTP.REQUEST_OTP_REQUEST,
            email: email,
        }
        setLoading(true)
        dispatch(actionCreators.verifyEmailRequestOTP(payload))
    };

    // This function sets the OTP Request API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.verifyEmail.model }),
        shallowEqual
    );
    var { verifyEmailResponse } = currentState

    // This function checks the response and then it should be redirected to the OTP screen
    useEffect(() => {
        if (verifyEmailResponse) {
            setLoading(false)
            if (verifyEmailResponse.success) {
                props.navigation.navigate(Constants.nav_verify_email, {
                    email: email,
                    setAccountsData: data.setAccountsData
                });
            } else if (verifyEmailResponse.errors) {
                if (verifyEmailResponse.errors[0] && verifyEmailResponse.errors[0].msg) {
                    showErrorToastMessage(verifyEmailResponse.errors[0].msg)
                }
            }
        }
        verifyEmailResponse = undefined
    }, [verifyEmailResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Mobile & Email Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_05]}> {Constants.header_mobile_add_email_verification} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView style={[scrollViewStyles.wrapperFullScrollView, marginStyles.bottomMargin_02, marginStyles.margin_20]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Phone Number Verified */}
                        <View style={[flexDirectionStyles.row, heightStyles.height_07, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackgroundForView, borderStyles.buttonRoundedCorner, alignmentStyles.alignItemsCenter, marginStyles.topMargin_02, paddingStyles.leftPadding_02, marginStyles.margin_2, { justifyContent: 'center' }]}>

                            {/* Image - Phone Number */}
                            <View style={{ width: 40, alignItems: 'center' }}>
                                <Image style={[heightStyles.height_32, widthStyles.width_18, justifyContentStyles.center, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_mobile.png')} />
                            </View>

                            { /* Wrapper - Phone Number is Verified */}
                            <View style={[containerStyles.container, flexDirectionStyles.row, marginStyles.leftMargin_20, justifyContentStyles.spaceBetween]}>

                                {/* Wrapper - Phone Number */}
                                <View style={[flexDirectionStyles.column, marginStyles.rightMargin_40]}>

                                    {/* Text - Phone Number is Verified */}
                                    <Text style={[fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_04]}>{Constants.text_mobile_verified}</Text>

                                    {/* Text - Phone Number */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.greenColor, fontSizeStyles.fontSize_035, marginStyles.topMargin_005]}>{(global.profile.phoneCode ? global.profile.phoneCode + ' - ' : '') + global.mobile}</Text>
                                </View>

                                {global.profile.isMobileVerified &&
                                    <Image style={{ right: 20, height: 20, width: 20, alignItems: 'center', alignSelf: 'center' }} source={require('../../assets/images/ic_phone_verified.png')} />
                                }
                            </View>
                        </View>

                        {/* Wrapper - Email Verified */}
                        <View style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackgroundForView, borderStyles.buttonRoundedCorner, alignmentStyles.alignItemsCenter, marginStyles.topMargin_02, marginStyles.bottomMargin_03, paddingStyles.leftPadding_02, alignmentStyles.alignSelfStretch, paddingStyles.topPadding_10, paddingStyles.bottomPadding_10, marginStyles.margin_2, { minHeight: height * 0.07 }]}>

                            {/* Image - Email */}
                            <View style={{ width: 40, alignItems: 'center' }}>
                                <Image style={[heightStyles.height_32, widthStyles.width_30_new, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_email.png')} />
                            </View>

                            { /* Wrapper - Email is Verified */}
                            <View style={[containerStyles.container, flexDirectionStyles.row, marginStyles.leftMargin_20, justifyContentStyles.spaceBetween]}>

                                {/* Wrapper - Email */}
                                <View style={[flexDirectionStyles.column, marginStyles.rightMargin_40]}>

                                    {/* Text - Email is Verified */}
                                    <Text style={[fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_04]}>{!global.profile.isEmailVerified ? Constants.text_verify_your_email : Constants.text_email_verified}</Text>

                                    {/* Text - Email */}
                                    {global.profile.isEmailVerified ? <Text style={[fontFamilyStyles.regular, colorStyles.greenColor, fontSizeStyles.fontSize_035, marginStyles.topMargin_002]}>{global.profile.email}</Text> : null}
                                </View>

                                {global.profile.isEmailVerified &&
                                    <Image style={{ right: 20, height: 20, width: 20, alignItems: 'center', alignSelf: 'center' }} source={require('../../assets/images/ic_phone_verified.png')} />
                                }
                            </View>
                        </View>

                        {/* isEmailVerified - Hide input text along with data */}
                        {!global.profile.isEmailVerified ? (
                            <View>
                                {/* Text - We won’t post anything without your permission */}
                                {/* <Text style={[positionStyles.relative, alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_035]}> {Constants.text_we_wont_post_anything} </Text> */}

                                {/* Wrapper - OR */}
                                <View style={[flexDirectionStyles.row, marginStyles.topMargin_02, marginStyles.bottomMargin_04, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter]}>

                                    {/* Text - Left Separator */}
                                    <Text style={[heightStyles.height_001, widthStyles.width_8, colorStyles.greyBackgroundColor]} />

                                    {/* Text - OR */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_02, marginStyles.rightMargin_02]}>{Constants.text_or}</Text>

                                    {/* Text - Right Separator */}
                                    <Text style={[heightStyles.height_001, widthStyles.width_8, colorStyles.greyBackgroundColor]} />
                                </View>

                                {/* Wrapper - Email */}
                                <View style={[flexDirectionStyles.column, positionStyles.relative]}>

                                    {/* Text - Email */}
                                    <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035]}>{Constants.text_email}</Text>

                                    {/* Wrapper - Email Form */}
                                    <View style={[positionStyles.relative, marginStyles.topMargin_01]}>

                                        {/* Text Input - Email */}
                                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04]}
                                            autoCorrect={false}
                                            value={email}
                                            placeholder={Constants.validation_email}
                                            onChangeText={text => setEmail(text)} />
                                    </View>
                                </View>

                                {/* Text - We will send you an OTP on this email */}
                                <Text style={[positionStyles.relative, alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_035, marginStyles.topMargin_03]}> {Constants.text_we_will_send_you_an_otp} </Text>
                            </View>
                        ) : null}
                    </ScrollView>

                    <View style={{ width: '100%' }}>

                        <View style={{ padding: 20 }}>
                            {/* TouchableOpacity - Verify Button Click Event */}
                            {!global.profile.isEmailVerified &&
                                <TouchableOpacity style={[flexDirectionStyles.row, widthStyles.width_100p, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.topMargin_04, alignmentStyles.alignSelfCenter, marginStyles.bottomMargin_02]}
                                    onPress={() => checkValidation()} activeOpacity={0.5}>

                                    {/* Text - Button (Verify) */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>{Constants.action_verify}</Text>

                                    {/* Image - Right Arrow */}
                                    <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_03]} />
                                </TouchableOpacity>
                            }
                        </View>

                        {checkIsSponsorAdsEnabled('mobileAndEmailVerify') &&
                            <SponsorBannerAds screenCode={'mobileAndEmailVerify'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('mobileAndEmailVerify')} />
                        }
                    </View>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default MobileAndEmailVerification;