import produce from 'immer';
import * as actionTypes from '../../actions/account/updateEmailAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        updateEmailResponse: undefined
    },
    error: false
};

{ /* Request - Update Email */ }
const updateEmailRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.updateEmailResponse = action.model;
    });
};

{ /* Request Failure - Update Email */ }
const updateEmailRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.updateEmailResponse = action.model;
    });
};

{ /* Reset State - Update Email */ }
const resetUpdateEmailState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.updateEmailResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_UPDATE_EMAIL:
            return updateEmailRequest(state, action);
        case actionTypes.REQUEST_UPDATE_EMAIL_FAILED:
            return updateEmailRequestFailed(state, action);
        case actionTypes.RESET_UPDATE_EMAIL_STATE:
            return resetUpdateEmailState(state);
        default:
            return state;
    }
};

export default reducer;