import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, TextInput, Image, ScrollView, Keyboard, FlatList } from 'react-native'
import CheckBox from '@react-native-community/checkbox';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import { showSuccessToastMessage, showErrorToastMessage, addLog, checkIsSponsorAdsEnabled, isValidURL } from '../../appUtils/commonUtlis';
import { AppConstants } from '../../appUtils/appConstants';
import * as actionCreators from '../../store/actions/index';
import CountryJSON from '../Authentication/countries.json';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { getUserCountry } from '../../appUtils/globleMethodsForUser';
import * as generalSetting from '../../webServices/generalSetting';

const StreamOnGamerji = (props, navigation) => {

    // Variable Declarations
    const data = props.route.params;
    const dispatch = useDispatch();
    const [games, setGames] = useState(undefined);
    const [gender, setGender] = useState(Constants.text_male);
    const [youtubeChannelName, setYoutubeChannelName] = useState('');
    const [youtubeChannelLink, setYoutubeChannelLink] = useState('');
    const [youtubeChecked, setYoutubeChecked] = useState(true);
    const [twitchChecked, setTwitchChecked] = useState(false);
    const [facebookChecked, setFacebookChecked] = useState(false);
    const [selectedGames, setSelectedGames] = useState([]);
    const [countryFlag, setCountryFlag] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [showPlatforms, setShowPlatforms] = useState(false);
    let arrPlatforms = [];
    var tempArr;
    addLog('Props:::> ', props)
    addLog('Status:::> ', data.status)

    // This function should call the Country Data Json to fetch the Flag image
    useEffect(() => {
        getFlagImageFromCountryData();
    }, []);

    // This function shiuld set the flag image as per the profile data 
    const getFlagImageFromCountryData = () => {

        var countryArr = CountryJSON.map(item => {
            if ('+' + item.callingCode === global.profile.phoneCode) {
                return setCountryFlag(item.flag);
            }
        })
    }

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToGamesListAPI();

        return () => {
            dispatch(actionCreators.resetStreamOnGamerjiState())
        }
    }, [])

    // This Use-Effect function should call the Games API
    const callToGamesListAPI = () => {
        dispatch(actionCreators.requestGamesSearch())
    }

    // This function sets the Videos API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.streamOnGamerji.model }),
        shallowEqual
    );
    var { gamesSearchResponse, addStreamerResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (gamesSearchResponse) {
            setLoading(false)
            if (gamesSearchResponse.list) {
                var tempArr = gamesSearchResponse.list.map(item => {
                    let tempItem = { ...item };
                    tempItem.isSelected = false;
                    return tempItem;
                })
                setGames(tempArr)
            }
        }
        gamesSearchResponse = undefined
    }, [gamesSearchResponse]);

    const getGamesData = (index) => {
        var tempContestList = [...games]
        tempContestList[index].isSelected = !tempContestList[index].isSelected
        setSelectedGames(tempContestList);
    }

    // This function should set and unset the platforms checkbox values 
    /*const handlePlatformsCheckbox = (type) => {
        if (type === Constants.text_youtube) {
            setYoutubeChecked(!youtubeChecked);
        } else if (type === Constants.text_twitch) {
            setTwitchChecked(!twitchChecked);
        } else {
            setFacebookChecked(!facebookChecked);
        }
    }*/

    // This function should check the validation first and then call the Add Videos API
    const checkValidation = () => {

        var arrIds = []
        games.forEach(element => {
            if (element.isSelected == true) {
                arrIds.push(element.name)
            }
        });
        tempArr = games.map(item => {
            if (item.isSelected) {
                return item.name;
            }
        })

        if (youtubeChecked) {
            arrPlatforms.push(Constants.text_youtube)
        }
        /*if (twitchChecked) {
            arrPlatforms.push(Constants.text_twitch)
        }
        if (facebookChecked) {
            arrPlatforms.push(Constants.text_facebook)
        }*/

        Keyboard.dismiss();
        if (!youtubeChannelName.trim()) {
            showErrorToastMessage(Constants.error_enter_youtube_channel_name)
        } else if (youtubeChannelLink == '') {
            showErrorToastMessage(Constants.error_enter_youtube_channel_link)
        } else if (!validateChannelLink()) {
            showErrorToastMessage("Please enter valid youtube channel link")
        } else if (arrIds.length < 1) {
            showErrorToastMessage(Constants.error_select_one_game)
        } else {
            callToAddStreamerAPI(props);
        }
        /*else if (arrPlatforms.length < 1) {
            showErrorToastMessage(Constants.error_select_one_platform)
            return;
        }*/
    };

    const validateChannelLink = () => {
        if (!isValidURL(youtubeChannelLink)) {
            return false
        } else if (!youtubeChannelLink.includes('youtube.com/channel/') && (!youtubeChannelLink.includes('youtube.com/c/'))) {
            return false
        }
        return true
    }

    // This function should call the Add Streamer API
    const callToAddStreamerAPI = (props) => {

        var arrIds = []
        games.forEach(element => {
            if (element.isSelected == true) {
                arrIds.push(element._id)
            }
        });
        tempArr = games.map(item => {
            if (item.isSelected) {
                return item._id;
            }
        })

        let payload = {
            name: global.profile.name,
            email: global.profile.email,
            phone: global.profile.phone,
            phoneCode: global.profile.phoneCode,
            gender: gender === Constants.text_male ? 'M' : 'F',
            youTubeChannelName: youtubeChannelName,
            youTubeChannelLink: youtubeChannelLink,
            games: arrIds,
            platforms: arrPlatforms,
            isActive: true
        }
        dispatch(actionCreators.requestAddStreamer(payload))
    }

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (addStreamerResponse) {
            setLoading(false)
            if (addStreamerResponse.success) {
                showSuccessToastMessage(Constants.success_streamer_request)
                props.navigation.goBack();
                data.checkStreamerStatus();

                return () => {
                    dispatch(actionCreators.resetStreamOnGamerjiState())
                }
            } else if (addStreamerResponse.errors) {
                if (addStreamerResponse.errors[0] && addStreamerResponse.errors[0].msg) {
                    showErrorToastMessage(addStreamerResponse.errors[0].msg)

                    return () => {
                        dispatch(actionCreators.resetStreamOnGamerjiState())
                    }
                }
            }
        }
        addStreamerResponse = undefined
    }, [addStreamerResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Stream On Gamerji Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_more_stream_on_gamerji} </Text>
                </View>

                {/* Scroll View */}
                {data.count === 0 ? (

                    // Wrapper - Body Content (Streamer Request)
                    <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                        <ScrollView style={marginStyles.bottomMargin_60}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            keyboardShouldPersistTaps='always'>

                            {/* Text - Name */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_25]}> {Constants.text_name} </Text>

                            {/* Text Input - Name */}
                            <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, colorStyles.lightSilverBackgroundColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                autoCorrect={false}
                                editable={false}> {global.profile.name} </TextInput>

                            {/* Wrapper - Mobile No */}
                            <View style={[flexDirectionStyles.row, positionStyles.relative, marginStyles.topMargin_22]}>

                                {/* Text - Country Code Header */}
                                <Text style={[positionStyles.relative, fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14]}> {Constants.text_country_code} </Text>

                                {/* Text - Mobile Number Header */}
                                <Text style={[positionStyles.relative, fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_16]}> {Constants.text_mobile_no} </Text>
                            </View>

                            {/* Wrapper - Input Texts */}
                            <View style={[flexDirectionStyles.row, positionStyles.relative, marginStyles.topMargin_7]}>

                                {/* Wrapper - Input Text Country Code */}
                                {/* <View style={[flexDirectionStyles.row, heightStyles.height_46, widthStyles.width_100, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}> */}

                                {/* Image - Indian Flag */}
                                {/* <Image style={[heightStyles.height_15, widthStyles.width_24]} source={{ uri: countryFlag }} /> */}

                                {/* Input Text - Country Code (+91) */}
                                {/* <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, alignmentStyles.alignTextCenter, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_16, marginStyles.leftMargin_5]}> {global.profile.phoneCode} </Text>
                                </View> */}

                                {getUserCountry() &&
                                    <View style={[flexDirectionStyles.row, heightStyles.height_46, widthStyles.width_100, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.lightSilverBackgroundColor]}>

                                        {/* Image - Indian Flag */}

                                        <Image style={[heightStyles.height_15, widthStyles.width_24]} source={getUserCountry().flag && getUserCountry().flag.default && { uri: generalSetting.UPLOADED_FILE_URL + getUserCountry().flag.default }} />
                                        {/* <Image style={[heightStyles.height_15, widthStyles.width_24]} source={{ uri: countryFlag }} /> */}

                                        {/* Input Text - Country Code (+91) */}
                                        <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, alignmentStyles.alignTextCenter, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_16, marginStyles.leftMargin_4]}> {getUserCountry().dialingCode} </Text>
                                    </View>
                                }

                                {/* Wrapper - Input Text Mobile No */}
                                <View style={[containerStyles.container, marginStyles.leftMargin_10]}>

                                    {/* Text Input - Mobile No */}
                                    <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_15, colorStyles.lightSilverBackgroundColor, alignmentStyles.alignTextLeft, paddingStyles.leftPadding_15]}
                                        autoCorrect={false}
                                        editable={false}> {global.profile.phone} </TextInput>
                                </View>
                            </View>

                            {/* Text - Email */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_email} </Text>

                            {/* Text Input - Email */}
                            <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_15, colorStyles.lightSilverBackgroundColor, alignmentStyles.alignTextLeft, marginStyles.topMargin_7, paddingStyles.leftPadding_15]}
                                autoCorrect={false}
                                editable={false}> {global.profile.email} </TextInput>

                            {/* Text - Gender */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_gender} </Text>

                            {/* Wrapper - Gender */}
                            <View style={[heightStyles.height_46, flexDirectionStyles.row, colorStyles.greyColor, alignmentStyles.alignItemsCenter, marginStyles.topMargin_7, paddingStyles.leftPadding_10]}>

                                {/* TouchableOpacity - Male Radio Buttom Click Event */}
                                <TouchableOpacity style={[heightStyles.height_20, widthStyles.width_20_new, borderStyles.borderRadius_100, borderStyles.borderWidth_2, gender == Constants.text_male ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                    onPress={() => setGender(Constants.text_male)} activeOpacity={0.5}>

                                    {gender == Constants.text_male && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                </TouchableOpacity>

                                {/* Text - Male */}
                                <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_5]}> {Constants.text_male} </Text>

                                {/* TouchableOpacity - Female Radio Buttom Click Event */}
                                <TouchableOpacity style={[heightStyles.height_20, widthStyles.width_20_new, borderStyles.borderRadius_100, borderStyles.borderWidth_2, gender == Constants.text_female ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, marginStyles.leftMargin_20]}
                                    onPress={() => setGender(Constants.text_female)} activeOpacity={0.5}>

                                    {gender == Constants.text_female && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                </TouchableOpacity>

                                {/* Text - Female */}
                                <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_5]}> {Constants.text_female} </Text>
                            </View>

                            {/* Text - Youtube Channel Name */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_youtube_channel_name} </Text>

                            {/* Text Input - Youtube Channel Name */}
                            <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                placeholder={Constants.validation_youtube_channel_name}
                                onChangeText={text => setYoutubeChannelName(text)} keyboardType={'url'} />

                            {/* Text - Youtube Channel Link */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_youtube_channel_link} </Text>

                            {/* Text Input - Youtube Channel Link */}
                            <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                placeholder={Constants.validation_youtube_channel_link}
                                onChangeText={text => setYoutubeChannelLink(text)} />

                            {/* Text - Games */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_games} </Text>

                            {/* Wrapper - Games */}
                            <View style={[flexDirectionStyles.column, borderStyles.borderWidth_1, borderStyles.borderRadius_20, colorStyles.greyBorderColor, colorStyles.greyColor, paddingStyles.leftPadding_15, paddingStyles.topPadding_10, paddingStyles.bottomPadding_10, marginStyles.topMargin_7]}>

                                {/* Flatlist - Games */}
                                <FlatList
                                    data={games}
                                    renderItem={({ item, index }) => <GamesItem item={item} index={index} getGamesData={getGamesData} />}
                                    keyExtractor={(item, index) => index.toString()}
                                    horizontal={false}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false} />
                            </View>

                            {showPlatforms &&
                                <View>
                                    {/* Text - Platforms */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_platforms} </Text>

                                    {/* Wrapper - Platforms */}

                                    <View style={[flexDirectionStyles.column, borderStyles.borderWidth_1, borderStyles.borderRadius_20, colorStyles.greyBorderColor, colorStyles.greyColor, paddingStyles.leftPadding_15, paddingStyles.topPadding_10, paddingStyles.bottomPadding_10, marginStyles.topMargin_7]}>

                                        {/* Wrapper - Youtube */}
                                        <View style={flexDirectionStyles.row}>

                                            {/* Checkbox - Youtube */}
                                            <CheckBox
                                                value={youtubeChecked}
                                                tintColors={{ true: colors.red, false: colors.grey }}
                                                onChange={() => handlePlatformsCheckbox(Constants.text_youtube)} />

                                            {/* Text - Youtube */}
                                            <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter]}> {Constants.text_youtube}</Text>
                                        </View>

                                        {/* Wrapper - Twitch */}
                                        <View style={flexDirectionStyles.row}>

                                            {/* Checkbox - Twitch */}
                                            <CheckBox
                                                value={twitchChecked}
                                                tintColors={{ true: colors.red, false: colors.grey }}
                                                onChange={() => handlePlatformsCheckbox(Constants.text_twitch)} />

                                            {/* Text - Twitch */}
                                            <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter]}> {Constants.text_twitch}</Text>
                                        </View>

                                        {/* Wrapper - Facebook */}
                                        <View style={flexDirectionStyles.row}>

                                            {/* Checkbox - Facebook */}
                                            <CheckBox
                                                value={facebookChecked}
                                                tintColors={{ true: colors.red, false: colors.grey }}
                                                onChange={() => handlePlatformsCheckbox(Constants.text_facebook)} />

                                            {/* Text - Facebook */}
                                            <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter]}> {Constants.text_facebook}</Text>
                                        </View>
                                    </View>
                                </View>
                            }
                        </ScrollView>

                        {/* TouchableOpacity - Submit Button Click Event */}
                        <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20]}
                            onPress={() => checkValidation()} activeOpacity={0.5}>

                            {/* Text - Button (Submit) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                            {/* Image - Right Arrow */}
                            <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                        </TouchableOpacity>
                    </View>
                ) :
                    // Wrapper - Body Content (Check Streamer Status)
                    <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                        {/* Wrapper - Check Streamer Status */}
                        <View style={[containerStyles.container, justifyContentStyles.center, marginStyles.leftMargin_15, marginStyles.rightMargin_15]}>

                            {/* Wrapper - Streamer Status */}
                            <View style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, paddingStyles.leftPadding_15,
                            (data.status === '1' ? colorStyles.yellowBackgroundColor : (data.status === '3' ? colorStyles.blueBackgroundColor : colorStyles.redBackgroundColor))]}>

                                {/* Text - Streamer Status */}
                                <Text style={[fontFamilyStyles.semiBold, data.status === '1' ? colorStyles.blackColor : colorStyles.whiteColor, fontSizeStyles.fontSize_20, alignmentStyles.alignTextCenter, otherStyles.capitalize]}>{(data.status === '1' ? AppConstants.STREAMER_STATUS_PENDING : (data.status === '3' ? AppConstants.STREAMER_STATUS_UNDER_REVIEW : AppConstants.STREAMER_STATUS_REJECTED))}</Text>
                            </View>

                            {/* Text - Streamer Status Notes */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_20, marginStyles.leftMargin_15, marginStyles.rightMargin_15]}>{Constants.text_streamer_status_notes}</Text>
                        </View>
                    </View>
                }

                {checkIsSponsorAdsEnabled('streamOnGamerji') &&
                    <SponsorBannerAds screenCode={'streamOnGamerji'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('streamOnGamerji')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

function GamesItem(props) {

    // Variable Declarations
    var item = props.item

    // This function should set and unset the games checkbox values 
    const handleGamesCheckbox = (index) => {
        props.getGamesData(index);
    }

    return (
        <View style={flexDirectionStyles.row}>

            {/* Checkbox - Games */}
            <CheckBox center titleProps={{ color: 'black', fontWeight: 'bold' }}
                value={item.isSelected}
                tintColors={{ true: colors.red, false: colors.grey }}
                onChange={() => handleGamesCheckbox(props.index)} />

            {/* Text - Game Name */}
            < Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter]}> {item.name}</Text>
        </View >
    )
}

export default StreamOnGamerji;