import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const walletUsageLimitForSingleContest = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST,
        model: viewModel
    };
};

export const walletUsageLimitForSingleContestFailed = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST_FAILED,
        model: viewModel
    };
};


export const requestWalletUsageLimitForSingleContest = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.GET_WALLET_USAGE_LIMIT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GET_WALLET_USAGE_LIMIT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(walletUsageLimitForSingleContest(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(walletUsageLimitForSingleContestFailed(error.response.data));
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetSingleContestState = () => {
    return {
        type: RESET_SINGLE_CONTEST_STATE,
    };
};

export const WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST = 'WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST';
export const WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST_FAILED = 'WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST_FAILED';
export const RESET_SINGLE_CONTEST_STATE = 'RESET_SINGLE_CONTEST_STATE';