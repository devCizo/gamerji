import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinContest/SquadRegistrationAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        checkSquadResponse: undefined,
        joinContestSquadResponse: undefined,
        success: true,
        errorMessage: ''
    },
};

{ /* Request - Game Types List */ }
const checkSquad = (state, action) => {
    return produce(state, (draft) => {
        draft.model.checkSquadResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const checkSquadFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.checkSquadResponse = action.model;
    });
};

{ /* Request - Game Types List */ }
const joinContestWithSquad = (state, action) => {
    return produce(state, (draft) => {
        draft.model.joinContestSquadResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const joinContestWithSquadFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.joinContestSquadResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHECK_SQUAD:
            return checkSquad(state, action);
        case actionTypes.CHECK_SQUAD_FAILED:
            return checkSquadFailed(state, action);
        case actionTypes.JOIN_CONTEST_WITH_SQUAD:
            return joinContestWithSquad(state, action);
        case actionTypes.JOIN_CONTEST_WITH_SQUAD_FAILED:
            return joinContestWithSquadFailed(state, action);
        case actionTypes.RESET_SQUAD_REGISTRATION_STATE:
            return resetSquadRegistrationState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetSquadRegistrationState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.gameTypesResponse = undefined;
        draft.model.checkSquadResponse = undefined
        draft.model.joinContestSquadResponse = undefined
        draft.model.success = true
        draft.model.errorMessage = ''
    })
}