import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Contest List */ }
export const joinContest = (viewModel) => {
    return {
        type: JOIN_CONTEST,
        model: viewModel
    };
};

{ /* Request Failure - All Games Banner List */ }
export const joinContestFailed = (viewModel) => {
    return {
        type: JOIN_CONTEST_FAILED,
        model: viewModel
    };
};

{ /* Request - All Games Banner */ }
export const requestJoinContest = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_CONTEST_TOURNAMENT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(joinContest(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(joinContestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetJoinContestState = () => {
    return {
        type: RESET_JOIN_CONTEST_STATE,
    };
};


export const JOIN_CONTEST = 'JOIN_CONTEST';
export const JOIN_CONTEST_FAILED = 'JOIN_CONTEST_FAILED';
export const RESET_JOIN_CONTEST_STATE = 'RESET_JOIN_CONTEST_STATE';
