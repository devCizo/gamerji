const walletData = [
    { id: '4007', name: 'Paytm' },
    { id: '4009', name: 'PhonePe' },
    { id: '4008', name: 'Amazon Pay' },
    { id: '4001', name: 'FreeCharge' },
    { id: '4002', name: 'MobiKwik' },
    { id: '4003', name: 'OLA Money' },
    { id: '4004', name: 'Reliance Jio Money' },
    { id: '4006', name: 'Airtel Money' },

];

export default walletData;