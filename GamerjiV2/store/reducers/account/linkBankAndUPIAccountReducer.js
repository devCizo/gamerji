import produce from 'immer';
import * as actionTypes from '../../actions/account/linkBankAndUPIAccountAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        linkBankAndUPIAccountResponse: undefined
    },
    error: false
};

{ /* Request - Link Bank & UPI Account */ }
const linkBankAndUPIAccountRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.linkBankAndUPIAccountResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Link Bank & UPI Account */ }
const linkBankAndUPIAccountRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.linkBankAndUPIAccountResponse = action.model;
    });
};

{ /* Reset State - Link Bank & UPI Account */ }
const resetLinkBankAndUPIAccountState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.linkBankAndUPIAccountResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_LINK_BANK_AND_UPI_ACCOUNT:
            return linkBankAndUPIAccountRequest(state, action);
        case actionTypes.REQUEST_LINK_BANK_AND_UPI_ACCOUNT_FAILED:
            return linkBankAndUPIAccountRequestFailed(state, action);
        case actionTypes.RESET_LINK_BANK_AND_UPI_ACCOUNT_STATE:
            return resetLinkBankAndUPIAccountState(state);
        default:
            return state;
    }
};

export default reducer;