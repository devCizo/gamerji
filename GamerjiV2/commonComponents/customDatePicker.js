import React, { useState } from 'react'
import DatePicker from 'react-native-date-picker'
import { addLog } from '../appUtils/commonUtlis'

const CustomDatePicker = (props) => {

    const [date, setDate] = useState(new Date())


    return (
        <DatePicker
            date={date}
            onDateChange={setDate}
        />
    )
}

export default CustomDatePicker;