import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const ticketDetail = (viewModel) => {
    return {
        type: TICKET_DETAIL,
        model: viewModel
    };
};

export const ticketDetailFailed = (viewModel) => {
    return {
        type: TICKET_DETAIL_FAILED,
        model: viewModel
    };
};

export const requestTicketDetail = (ticketId) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.TICKET_DETAIL + "/" + ticketId)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(ticketDetail(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(ticketDetailFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const updateTicket = (viewModel) => {
    return {
        type: TICKET_UPDATE,
        model: viewModel
    };
};

export const updateTicketFailed = (viewModel) => {
    return {
        type: TICKET_UPDATE_FAILED,
        model: viewModel
    };
};

export const requestUpdateTicket = (ticketId, payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CUSTOMER_CARE.TICKET_UPDATE + "/" + ticketId, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(updateTicket(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(updateTicketFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetTicketDetailState = () => {
    return {
        type: RESET_TICKET_DETAIL_STATE,
    };
};

export const TICKET_DETAIL = 'TICKET_DETAIL';
export const TICKET_DETAIL_FAILED = 'TICKET_DETAIL_FAILED';
export const TICKET_UPDATE = 'TICKET_UPDATE';
export const TICKET_UPDATE_FAILED = 'TICKET_UPDATE_FAILED';
export const RESET_TICKET_DETAIL_STATE = 'RESET_TICKET_DETAIL_STATE';