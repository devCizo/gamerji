import React, { useState } from 'react'
import { BackHandler } from "react-native";

let screen = "";

export const componentWillMount = (screenName, props) => {
    screen = screenName;
    BackHandler.addEventListener('hardwareBackPress', handleBackButton(props));
};

export const componentWillUnmount = (screenName, props) => {
    screen = screenName;
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton(props));
};

export const handleBackButton = (props) => {
    if (screen == 'login') {
        BackHandler.exitApp();
        return true;
    } else if (screen == 'otp') {
        props.navigation.goBack();
        return true;
    }
};