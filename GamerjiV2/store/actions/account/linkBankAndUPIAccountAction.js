import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { Constants } from '../../../appUtils/constants';

{ /* Action Types */ }
export const REQUEST_LINK_BANK_AND_UPI_ACCOUNT = 'REQUEST_LINK_BANK_AND_UPI_ACCOUNT';
export const REQUEST_LINK_BANK_AND_UPI_ACCOUNT_FAILED = 'REQUEST_LINK_BANK_AND_UPI_ACCOUNT_FAILED';
export const RESET_LINK_BANK_AND_UPI_ACCOUNT_STATE = 'RESET_LINK_BANK_AND_UPI_ACCOUNT_STATE';

{ /* Request - Link Bank & UPI Account */ }
export const linkBankAndUPIAccountRequest = (viewModel) => {
    return {
        type: REQUEST_LINK_BANK_AND_UPI_ACCOUNT,
        model: viewModel
    };
};

{ /* Request Failure - Link Bank & UPI Account */ }
export const linkBankAndUPIAccountRequestFailed = (viewModel) => {
    return {
        type: REQUEST_LINK_BANK_AND_UPI_ACCOUNT_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Link Bank & UPI Account */ }
export const resetLinkBankAndUPIAccountState = () => {
    return {
        type: RESET_LINK_BANK_AND_UPI_ACCOUNT_STATE,
    };
};

{ /* Request - Link Bank / UPI Account */ }
export const requestLinkBankAndUPIAccount = (payload, withdrawalType) => {
    let url;
    if (withdrawalType === Constants.text_bank_account) {
        url = WebFields.LINK_BANK_ACCOUNT.MODE;
    } else {
        url = WebFields.LINK_UPI_ACCOUNT.MODE;
    }
    addLog("url==>", url);
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(url, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(linkBankAndUPIAccountRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(linkBankAndUPIAccountRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};