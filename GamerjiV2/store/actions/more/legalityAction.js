import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { Constants } from '../../../appUtils/constants';

{ /* Action Types */ }
export const REQUEST_LEGALITY = 'REQUEST_LEGALITY';
export const REQUEST_LEGALITY_FAILED = 'REQUEST_LEGALITY_FAILED';
export const RESET_LEGALITY_STATE = 'RESET_LEGALITY_STATE';

{ /* Request - Legality */ }
export const legalityRequest = (viewModel) => {
    return {
        type: REQUEST_LEGALITY,
        model: viewModel
    };
};

{ /* Request Failure - Legality */ }
export const legalityRequestFailed = (viewModel) => {
    return {
        type: REQUEST_LEGALITY_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Legality */ }
export const resetLegalityState = () => {
    return {
        type: RESET_LEGALITY_STATE,
    };
};

{ /* Request - Legality */ }
export const requestLegality = (legalityType) => {
    let url;
    if (legalityType === Constants.nav_more_terms_of_use) {
        url = WebFields.TERMS_AND_CONDITIONS.MODE;
    } else {
        url = WebFields.PRIVACY_POLICY.MODE;
    }

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(url, {})
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(legalityRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(legalityRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};