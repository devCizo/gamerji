import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const contestDetail = (viewModel) => {
    return {
        type: CONTEST_DETAIL,
        model: viewModel
    };
};

export const contestDetailFailed = (viewModel) => {
    return {
        type: CONTEST_DETAIL_FAILED,
        model: viewModel
    };
};

export const requestContestDetail = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CONTEST_DETAIL.MODE + '/' + payload, {})
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(contestDetail(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(contestDetailFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const CONTEST_DETAIL = 'CONTEST_DETAIL';
export const CONTEST_DETAIL_FAILED = 'CONTEST_DETAIL_FAILED';




export const rateContest = (viewModel) => {
    return {
        type: RATE_CONTEST,
        model: viewModel
    };
};

export const rateContestFailed = (viewModel) => {
    return {
        type: RATE_CONTEST_FAILED,
        model: viewModel
    };
};

export const requestRateContest = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.RATE_CONTEST_TOURNAMENT.RATE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(rateContest(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(rateContestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const RATE_CONTEST = 'RATE_CONTEST';
export const RATE_CONTEST_FAILED = 'RATE_CONTEST_FAILED';


export const resetContestDetailState = () => {
    return {
        type: RESET_CONTEST_DETAIL_STATE,
    };
};

export const RESET_CONTEST_DETAIL_STATE = 'RESET_CONTEST_DETAIL_STATE';
