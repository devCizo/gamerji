import produce from 'immer';
import * as actionTypes from '../../actions/joinContest/ContestListAction';

const initialState = {
    model: {
        contestListResponse: undefined,
        walletUsageLimitResponse: undefined,
    },
    error: false
};

const contestList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.contestListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const contestListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.contestListResponse = action.model;
    });
};

const walletUsageLimit = (state, action) => {
    return produce(state, (draft) => {
        draft.model.walletUsageLimitResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const walletUsageLimitFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.walletUsageLimitResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTEST_LIST:
            return contestList(state, action);
        case actionTypes.CONTEST_LIST_FAILED:
            return contestListFailed(state, action);
        case actionTypes.WALLET_USAGE_LIMIT:
            return walletUsageLimit(state, action);
        case actionTypes.WALLET_USAGE_LIMIT_FAILED:
            return walletUsageLimitFailed(state, action);
        case actionTypes.RESET_CONTEST_LIST_STATE:
            return resetContestListState(state);
        default:
            return state;
    }
};

export default reducer;

const resetContestListState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.contestListResponse = undefined
        draft.model.walletUsageLimitResponse = undefined
    })
}