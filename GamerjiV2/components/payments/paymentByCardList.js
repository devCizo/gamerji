import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, BackHandler, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import AddCardPopup from './addCardPopup';
import { AppConstants, cashFreeClientId, cashFreeSecretId } from '../../appUtils/appConstants';

const PaymentByCardList = (props) => {

    const dispatch = useDispatch();
    const [isOpenAddCardPopup, setOpenAddCardPopup] = useState(false)

    useEffect(() => {
        dispatch(actionCreators.requestGetSavedCards({ appId: cashFreeClientId(), secretKey: cashFreeSecretId(), phone: global.profile && global.profile.phone }))
    }, [])

    const saveCardFromPopupAndProceed = params => {

        var paramsNew = { ...params }

        paramsNew['amount'] = props.route.params.amount
        paramsNew['paymentTypeForCreateTransaction'] = props.route.params.paymentTypeForCreateTransaction

        if (props.route.params.coinStore) {
            paramsNew['coinStore'] = props.route.params.coinStore
        }

        if (props.route.params.avatarId) {
            paramsNew['avatarId'] = props.route.params.avatarId
        }
        props.navigation.push(Constants.nav_payment_gateway_new, paramsNew)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={() => RootNavigation.goBack()}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center', tintColor: colors.white }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >PAYMENT OPTIONS</Text>
            </View>

            {/* Main Content View */}
            <View style={{ flex: 1, backgroundColor: colors.white, borderTopLeftRadius: 40, borderTopRightRadius: 40 }} >

                {/* Current Balance */}
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, backgroundColor: colors.red, borderRadius: 25, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 24, height: 16, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_currency.png')} />
                        <Text style={{ marginLeft: 12, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Balance to Add</Text>
                    </View>

                    {/* Ballance */}
                    <Text style={{ marginRight: 24, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹500</Text>
                </View>

                {/* Card List */}
                <View style={{ flex: 1 }}>
                    <FlatList
                        style={{ marginTop: 20, flex: 1 }}
                        data={[{}, {}, {}, {}]}
                        renderItem={({ item, index }) => <PaymentByCardListItem item={item} index={index} />}
                    />

                </View>

                {/* Add Card Details */}
                <TouchableOpacity style={{ marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, borderRadius: 23, backgroundColor: colors.black, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} activeOpacity={0.8} onPress={() => setOpenAddCardPopup(true)}>

                    <Text style={{ marginLeft: 22, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >ADD CARD DETAILS</Text>
                    <Image style={{ marginRight: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                </TouchableOpacity>
            </View>

            <Modal
                isVisible={isOpenAddCardPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <AddCardPopup setOpenAddCardPopup={setOpenAddCardPopup} saveCardFromPopupAndProceed={saveCardFromPopupAndProceed} />
            </Modal>

        </SafeAreaView>
    )
}

const PaymentByCardListItem = () => {

    const [expiryDate, setExpiryDate] = useState('')
    const [cvv, setCvv] = useState('')

    const handleExpiryDateChange = (text) => {

        let textWithoutSpace = text.replace('/', '').replace(' ', '')
        var count = 0
        var tempStr = ''

        textWithoutSpace.split('').forEach(element => {
            count += 1
            if (count == 2) {
                tempStr += (element + '/')
            } else {
                tempStr += element
            }
        });
        setExpiryDate(tempStr)
    }

    return (
        <View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 10, borderRadius: 20, backgroundColor: colors.black }}>

            {/* Card Number */}
            <View style={{ marginTop: 24, marginLeft: 30, flexDirection: 'row', alignItems: 'center' }} >

                <Image style={{ width: 44, height: 32, resizeMode: 'contain' }} source={require('../../assets/images/card-chip-icon.png')} />
                <Text style={{ marginLeft: 22, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.semiBold }} >**** **** **** 1234</Text>
            </View>

            <View style={{ marginTop: 24, marginLeft: 30, flexDirection: 'row' }}>

                {/* Expiry Date */}
                <View >
                    <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Expiry Date</Text>
                    <TextInput style={{ height: 40, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, paddingHorizontal: 0 }}
                        placeholder='MM/YY'
                        placeholderTextColor={'#BEBEBE'}
                        keyboardType={'numeric'}
                        onChangeText={(text) => handleExpiryDateChange(text)}
                        maxLength={5}>{expiryDate}
                    </TextInput>
                </View>

                {/* CVV */}
                <View style={{ marginLeft: 20 }} >
                    <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >CVV</Text>
                    <TextInput style={{ height: 40, width: 50, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, paddingHorizontal: 0 }}
                        placeholder='CVV'
                        placeholderTextColor={'#BEBEBE'}
                        keyboardType={'numeric'}
                        onChangeText={(text) => setCvv(text)}
                        maxLength={3}>{cvv}
                    </TextInput>
                </View>
            </View>
        </View>
    )
}

export default PaymentByCardList