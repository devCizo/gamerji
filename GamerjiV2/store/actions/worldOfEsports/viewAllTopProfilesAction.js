import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIEW_ALL_TOP_PROFILE_LIST = 'VIEW_ALL_TOP_PROFILE_LIST';
export const VIEW_ALL_TOP_PROFILE_LIST_FAILED = 'VIEW_ALL_TOP_PROFILE_LIST_FAILED';
export const RESET_VIEW_ALL_TOP_PROFILE_STATE = 'RESET_VIEW_ALL_TOP_PROFILE_STATE';

{ /* Request - View All Top Profile List */ }
export const viewAllTopProfileList = (viewModel) => {
    return {
        type: VIEW_ALL_TOP_PROFILE_LIST,
        model: viewModel
    };
};

{ /* Request Failure - View All Top Profile List */ }
export const viewAllTopProfileListFailed = (viewModel) => {
    return {
        type: VIEW_ALL_TOP_PROFILE_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - View All Top Profiles List */ }
export const resetViewAllTopProfilesState = () => {
    return {
        type: RESET_VIEW_ALL_TOP_PROFILE_STATE,
    };
};

{ /* Request - View All Top Profiles */ }
export const requestViewAllTopProfiles = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.WORLD_OF_ESPORTS.MODE_TOP_PROFILE_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(viewAllTopProfileList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(viewAllTopProfileListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};