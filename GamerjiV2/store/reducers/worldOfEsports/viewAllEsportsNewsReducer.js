import produce from 'immer';
import * as actionTypes from '../../actions/worldOfEsports/viewAllEsportsNewsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        viewAllBlogsListResponse: undefined,
    },
    error: false
};

{ /* Request - View All Blogs List */ }
const viewAllBlogsList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.viewAllBlogsListResponse = action.model;
    });
};

{ /* Request Failure - View All Blogs List */ }
const viewAllBlogsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.viewAllBlogsListResponse = action.model;
    });
};

{ /* Reset State - View All Blogs List */ }
const resetViewAllBlogsState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.viewAllBlogsListResponse = undefined;
    })
}

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIEW_ALL_BLOGS_LIST:
            return viewAllBlogsList(state, action);
        case actionTypes.VIEW_ALL_BLOGS_LIST_FAILED:
            return viewAllBlogsListFailed(state, action);
        case actionTypes.RESET_VIEW_ALL_BLOGS_STATE:
            return resetViewAllBlogsState(state);
        default:
            return state;
    }
};

export default reducer;