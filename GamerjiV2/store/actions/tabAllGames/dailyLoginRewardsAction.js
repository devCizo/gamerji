import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const DAILY_LOGIN_REWARDS_LIST = 'DAILY_LOGIN_REWARDS_LIST'
export const DAILY_LOGIN_REWARDS_LIST_FAILED = 'DAILY_LOGIN_REWARDS_LIST_FAILED'
export const RESET_DAILY_LOGIN_REWARDS_STATE = 'RESET_DAILY_LOGIN_REWARDS_STATE'

export const DAILY_LOGIN_REWARDS_STATUS_CHECK = 'DAILY_LOGIN_REWARDS_STATUS_CHECK'
export const DAILY_LOGIN_REWARDS_STATUS_CHECK_FAILED = 'DAILY_LOGIN_REWARDS_STATUS_CHECK_FAILED'

{ /* Request - Daily Login Rewards Status Check */ }
export const dailyLoginRewardsStatusCheck = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_STATUS_CHECK,
        model: viewModel
    };
};

{ /* Request Failure - Daily Login Rewards Status Check */ }
export const dailyLoginRewardsStatusCheckFailed = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_STATUS_CHECK_FAILED,
        model: viewModel
    };
};

{ /* Request - Daily Login Rewards Status Check */ }
export const requestDailyLoginRewardsStatusCheckMore = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_REWARDS_CHECK.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(dailyLoginRewardsStatusCheck(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(dailyLoginRewardsStatusCheckFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Daily Login Rewards List */ }
export const dailyLoginRewardsList = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Daily Login Rewards List */ }
export const dailyLoginRewardsListFailed = (viewModel) => {
    return {
        type: DAILY_LOGIN_REWARDS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Daily Login Rewards List */ }
export const resetDailyLoginRewardsState = () => {
    return {
        type: RESET_DAILY_LOGIN_REWARDS_STATE,
    };
};

{ /* Request - Daily Login Rewards */ }
export const requestDailyLoginRewards = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_LOGIN_REWARDS_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(dailyLoginRewardsList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(dailyLoginRewardsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};