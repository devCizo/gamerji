import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, BackHandler, Image, ScrollView, TouchableOpacity, SafeAreaView, FlatList, Platform, TextInput, Input, Alert, Keyboard, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import RightArrowWhiteImage from '../../assets/images/ic_right_arrow_black.svg';
import colors from '../../assets/colors/colors';
import { addLog, showSuccessToastMessage, showErrorToastMessage, getUpiPattern, compareCurrentYear, checkSameYear, checkMonthOfTheCurrentYear, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import bankData from '../../assets/data/bankData';
import walletData from '../../assets/data/walletData';
import NetBankingItem from '../../componentsItem/NetBankingItem';
import { AppConstants } from '../../appUtils/appConstants';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const PaymentOptions = (props, navigation) => {

    // Variable Declarations
    const amount = props.route.params.amount;
    const [debitCreditCardView, setDebitCreditCardView] = useState(false);
    const [netBankingView, setNetBankingView] = useState(false);
    const [walletView, setWalletView] = useState(false);
    const [upiPaymentView, setUpiPaymentView] = useState(false);
    const [paytmView, setPaytmView] = useState(false);
    const [cardData, setCardData] = useState({
        cardNumber: '',
        cardExpiryDate: '',
        cardExpiryMonth: '',
        cardExpiryYear: '',
        cardCVV: '',
        cardHolderName: ''
    });
    const [searchBankName, setSearchBankName] = useState('');
    const [upiAddress, setUPIAddress] = useState('');
    const [text, setText] = useState('');
    const [data, setData] = useState(bankData);
    const [showPaytm] = useState(false);

    let depositCash = parseFloat(global.profile.wallet && global.profile.wallet.depositedAmount && global.profile.wallet.depositedAmount);
    let winningCash = parseFloat(global.profile.wallet && global.profile.wallet.winningAmount && global.profile.wallet.winningAmount);
    let bonusCash = parseFloat(global.profile.wallet && global.profile.wallet.bonusAmount && global.profile.wallet.bonusAmount);
    let totalCurrentBalance = depositCash + winningCash + bonusCash;
    // let formattedExpiryDate, formattedCardNumber;

    // This function should Show / Hide Debit - Credit Card View when clicking on the Debit / Credit Card Button
    const showHideDebitCreditCardView = () => {
        // addLog("Debit / Credit Card:::> ", debitCreditCardView);
        setNetBankingView(false);
        setWalletView(false);
        setUpiPaymentView(false);
        setPaytmView(false);
        setDebitCreditCardView(!debitCreditCardView);
        eraseData(AppConstants.PAYMENT_TYPE_NET_BANKING);
        eraseData(AppConstants.PAYMENT_TYPE_UPI);
    }

    // This function should Show / Hide Net Banking View when clicking on the Net Banking Button
    const showHideNetBankingView = () => {
        // addLog("Net Banking:::> ", netBankingView);
        setDebitCreditCardView(false);
        setWalletView(false);
        setUpiPaymentView(false);
        setPaytmView(false);
        setNetBankingView(!netBankingView);
        eraseData(AppConstants.PAYMENT_TYPE_CARD);
        eraseData(AppConstants.PAYMENT_TYPE_UPI);
    }

    // This function should Show / Hide Wallet View when clicking on the Wallet Button
    const showHideWalletView = () => {
        // addLog("Wallet:::> ", walletView);
        setDebitCreditCardView(false);
        setNetBankingView(false);
        setUpiPaymentView(false);
        setPaytmView(false);
        setWalletView(!walletView);
        eraseData(AppConstants.PAYMENT_TYPE_CARD);
        eraseData(AppConstants.PAYMENT_TYPE_NET_BANKING);
        eraseData(AppConstants.PAYMENT_TYPE_UPI);
    }

    // This function should Show / Hide UPI Payment View when clicking on the UPI Payment Button
    const showHideUpiPaymentView = () => {
        // addLog("UPI Payment:::> ", upiPaymentView);
        setDebitCreditCardView(false);
        setNetBankingView(false);
        setWalletView(false);
        setPaytmView(false);
        setUpiPaymentView(!upiPaymentView);
        eraseData(AppConstants.PAYMENT_TYPE_CARD);
        eraseData(AppConstants.PAYMENT_TYPE_NET_BANKING);
    }

    // This function should Show / Hide Paytm View when clicking on the Paytm Button
    const showHidePaytmView = () => {
        // addLog("Paytm:::> ", paytmView);
        setDebitCreditCardView(false);
        setNetBankingView(false);
        setWalletView(false);
        setUpiPaymentView(false);
        setPaytmView(!paytmView);
        eraseData(AppConstants.PAYMENT_TYPE_CARD);
        eraseData(AppConstants.PAYMENT_TYPE_NET_BANKING);
        eraseData(AppConstants.PAYMENT_TYPE_UPI);
    }

    // This function should set the Card Data to the main state
    const setCardDataToMainState = (event, type) => {
        if (type === 'cardNumber') {
            // cardData.cardNumber = formattedCardNumber;
            cardData.cardNumber = event.nativeEvent.text;
        } else if (type === 'cardExpiryDate') {
            // cardData.cardExpiryDate = formattedExpiryDate;
            // cardData.cardExpiryMonth = cardData.cardExpiryDate.split(' / ')[0];
            // cardData.cardExpiryYear = cardData.cardExpiryDate.split(' / ')[1];
            cardData.cardExpiryDate = event.nativeEvent.text;
        } else if (type === 'cardCVV') {
            cardData.cardCVV = event.nativeEvent.text;
        } else if (type === 'cardHolderName') {
            cardData.cardHolderName = event.nativeEvent.text;
        }
        setCardData(cardData);
    }

    // This function should check the validation first and then is should be redirect to the Payment Gateway screen along with the Card Data
    const checkValidationForCard = () => {
        Keyboard.dismiss();
        if (!cardData.cardNumber) {
            showErrorToastMessage(Constants.error_enter_card_number);
            return;
        } else if (cardData.cardNumber.length < 16) {
            showErrorToastMessage(Constants.error_enter_correct_card_number);
            return;
        } else if (!cardData.cardExpiryDate.trim()) {
            showErrorToastMessage(Constants.error_enter_expiry_date);
            return;
        } else if (cardData.cardExpiryDate.length < 4) {
            showErrorToastMessage(Constants.error_enter_valid_expiry_date);
            return;
        } else if (cardData.cardExpiryDate.slice(0, 2) == '0' ||
            cardData.cardExpiryDate.slice(0, 2) == '00') {
            showErrorToastMessage(Constants.error_month_not_zero);
            return;
        } else if (cardData.cardExpiryDate.slice(0, 2) > '12') {
            showErrorToastMessage(Constants.error_enter_valid_month);
            return;
        } else if (!compareCurrentYear(cardData.cardExpiryDate.slice(2, 4))) {
            showErrorToastMessage(Constants.error_enter_valid_year);
            return;
        } else if (checkSameYear(cardData.cardExpiryDate.slice(2, 4))) {
            if (!checkMonthOfTheCurrentYear(cardData.cardExpiryDate.slice(0, 2))) {
                showErrorToastMessage(Constants.error_enter_valid_month);
                return;
            }
        } else if (!cardData.cardCVV.trim()) {
            showErrorToastMessage(Constants.error_enter_cvv);
            return;
        } else if (cardData.cardCVV.length < 3) {
            showErrorToastMessage(Constants.error_enter_correct_cvv);
            return;
        } else if (!cardData.cardHolderName.trim()) {
            showErrorToastMessage(Constants.error_enter_card_holder_name);
            return;
        }
        var params = {
            cardNumber: cardData.cardNumber,
            cardExpiryMonth: cardData.cardExpiryDate.slice(0, 2),
            cardExpiryYear: 20 + cardData.cardExpiryDate.slice(2, 4),
            cardCVV: cardData.cardCVV,
            cardHolderName: cardData.cardHolderName,
            paymentType: AppConstants.PAYMENT_TYPE_CARD
        }

        moveToPaymentGateway(params)
    }

    const moveToPaymentGateway = (params) => {

        var paramsNew = { ...params }

        paramsNew['amount'] = amount
        paramsNew['paymentTypeForCreateTransaction'] = props.route.params.paymentTypeForCreateTransaction

        if (props.route.params.coinStore) {
            paramsNew['coinStore'] = props.route.params.coinStore
        }

        if (props.route.params.avatarId) {
            paramsNew['avatarId'] = props.route.params.avatarId
        }

        props.navigation.push(Constants.nav_payment_gateway, paramsNew)
    }

    // This function should erase the data according to the Payment Type
    const eraseData = (type) => {
        if (type === AppConstants.PAYMENT_TYPE_CARD) {
            cardData.cardNumber = '';
            cardData.cardExpiryDate = '';
            cardData.cardExpiryMonth = '';
            cardData.cardExpiryYear = '';
            cardData.cardCVV = '';
            cardData.cardHolderName = '';
            setCardData(cardData);
        } else if (type === AppConstants.PAYMENT_TYPE_NET_BANKING) {
            setSearchBankName(undefined);
        } else if (type === AppConstants.PAYMENT_TYPE_UPI) {
            setUPIAddress(undefined);
        }
    }

    // This function should find the banks from the Bank Data Listing
    function handleSearch(text) {
        debugger
        addLog("Text:::> ", text);
        const newData = bankData.filter(item => {
            const itemData = item.name.toLowerCase();
            const textData = text.toLowerCase();
            addLog("itemData:::> ", itemData);
            addLog("textData:::> ", textData);
            return itemData.indexOf(textData) > -1
        });
        addLog("newData:::> ", newData);
        setData(newData);
        setText(text);
    }

    // This function should check the validation first and then is should be redirect to the Payment Gateway screen along with the UPI Data
    const checkValidationForUPI = () => {
        Keyboard.dismiss();
        if (!upiAddress.trim()) {
            showErrorToastMessage(Constants.error_enter_upi_address);
            return;
        } else if (!getUpiPattern().test(upiAddress)) {
            showErrorToastMessage(Constants.error_enter_correct_upi_address);
            return;
        }

        var params = {
            amount: amount,
            paymentType: AppConstants.PAYMENT_TYPE_UPI,
            upiAddress: upiAddress,
        }
        moveToPaymentGateway(params)
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        }

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        // onPress={() => props.navigation.goBack()} activeOpacity={0.5}>
                        onPress={() => props.navigation.pop()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Payment Options Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_05]}> {Constants.header_payment_options} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor, alignmentStyles.oveflow]}>

                    {/* Scroll View */}
                    <ScrollView style={marginStyles.margin_20}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Current Balance */}
                        <View style={[flexDirectionStyles.row, colorStyles.redBackgroundColor, shadowStyles.shadowBackgroundForView, colorStyles.redShadowColor, borderStyles.buttonRoundedCorner, paddingStyles.padding_005, marginStyles.bottomMargin_02]}>

                            {/* Image - Currency */}
                            <Image style={[alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05, { height: 16, width: 24 }]} source={require('../../assets/images/ic_currency.png')} />

                            {/* Text - Current Balance */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_035, paddingStyles.padding_03]}>Balance To Add</Text>

                            {/* Text - Total Current Balance */}
                            {/* <Text style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05, fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_05]} > {Constants.text_rupees}{totalCurrentBalance && totalCurrentBalance != null ? totalCurrentBalance : '0'} </Text> */}
                            <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, { flexDirection: 'row', alignItems: 'center' }]} >
                                {/* <Image style={{ height: 18, width: 18, marginRight: 4 }} source={require('../../assets/images/usable_gamerji_coin_popup.png')} /> */}
                                {global.addBalanceCurrencyType == Constants.coin_payment ?
                                    <Image style={{ height: 18, width: 18, marginRight: 4 }} source={require('../../assets/images/usable_gamerji_coin_popup.png')} />
                                    :
                                    <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_05, { marginRight: 2 }]}>₹</Text>
                                }
                                <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, paddingStyles.rightPadding_17, fontSizeStyles.fontSize_05]}>{amount}</Text>
                            </View>
                        </View>

                        {/* Wrapper - Debit / Credit Card */}
                        <View style={[flexDirectionStyles.column, { margin: 2, marginTop: 10 }]}>

                            {/* TouchableOpacity - Debit / Credit Card Button Click Event */}
                            <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, otherStyles.zIndex_1]}
                                onPress={() => showHideDebitCreditCardView()} activeOpacity={1}>

                                {/* Image - Debit / Credit Card */}
                                <Image style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05, marginStyles.topMargin_005, { height: 18, width: 24 }]} source={require('../../assets/images/ic_credit_card.png')} />

                                {/* Text - Debit / Credit Card */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, paddingStyles.padding_04, fontSizeStyles.fontSize_035]}>{Constants.text_debit_credit_card}</Text>

                                {/* Image - Right Arrow */}
                                {!debitCreditCardView ?
                                    <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                                    :
                                    <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} tintColor={colors.black} />
                                }
                            </TouchableOpacity>

                            {/* Wrapper - Debit / Credit Card Details */}
                            {debitCreditCardView ?
                                <View style={[colorStyles.greyBorderColor, borderStyles.borderRadius_15, borderStyles.borderWidth_1, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_25_minus, marginStyles.bottomMargin_04_minus]}>

                                    {/* Wrapper - Debit / Credit Card Form Details */}
                                    <View style={[flexDirectionStyles.column]}>

                                        {/* Text - Card Number */}
                                        <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035, marginStyles.topMargin_02]}> {Constants.text_card_number} </Text>

                                        {/* Text Input - Card Number */}
                                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                            keyboardType="numeric"
                                            underlineColorAndroid='transparent'
                                            maxLength={16}
                                            placeholder={Constants.validation_card_number}
                                            onChange={(e) => setCardDataToMainState(e, 'cardNumber')}
                                        />

                                        {/* Wrapper - Expiry Date & CVV */}
                                        <View style={[flexDirectionStyles.row, marginStyles.topMargin_03]}>

                                            {/* Wrapper - Expiry Date */}
                                            <View style={[flexDirectionStyles.column, widthStyles.width_55p, marginStyles.rightMargin_02]}>

                                                {/* Text - Expiry (MM/YY) */}
                                                <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035]}> {Constants.text_expiry_date} </Text>

                                                {/* Text Input - Expiry Date */}
                                                <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                                    keyboardType="numeric"
                                                    maxLength={4}
                                                    underlineColorAndroid='transparent'
                                                    placeholder={Constants.validation_expiry_date}
                                                    onChange={(e) => setCardDataToMainState(e, 'cardExpiryDate')}
                                                />
                                            </View>

                                            {/* Wrapper - CVV */}
                                            <View style={[flexDirectionStyles.column, widthStyles.width_40p, marginStyles.leftMargin_02]}>

                                                {/* Text - Expiry (MM/YY) */}
                                                <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035]}> {Constants.text_cvv} </Text>

                                                {/* Text Input - Expiry Date */}
                                                <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                                    keyboardType="numeric"
                                                    maxLength={3}
                                                    underlineColorAndroid='transparent'
                                                    placeholder={Constants.validation_cvv}
                                                    onChange={(e) => setCardDataToMainState(e, 'cardCVV')} />
                                            </View>
                                        </View>

                                        {/* Text - Card Holder Name */}
                                        <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035, marginStyles.topMargin_02]}> {Constants.text_card_holder_name} </Text>

                                        {/* Text Input - Card Holder Name */}
                                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                            underlineColorAndroid='transparent'
                                            placeholder={Constants.validation_card_holder_name}
                                            onChange={(e) => setCardDataToMainState(e, 'cardHolderName')}
                                        />

                                        {/* Wrapper - CVV Notes */}
                                        <View style={[flexDirectionStyles.row, alignmentStyles.alignSelfCenter, marginStyles.topMargin_03]}>

                                            {/* Image - CVV */}
                                            <Image style={[]} source={require('../../assets/images/ic_lock.png')} />

                                            {/* Text - Your CVV will not be saved */}
                                            <Text style={[fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_035, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_02]}> {Constants.text_cvv_notes} </Text>
                                        </View>
                                    </View>

                                    {/* TouchableOpacity - Save Card & Proceed Button Click Event */}
                                    <TouchableOpacity type="submit" style={[widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.topMargin_03, alignmentStyles.alignSelfCenter]} onPress={() => checkValidationForCard()} activeOpacity={0.5}>

                                        {/* Text - Button (Save Card & Proceed) */}
                                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>{Constants.action_save_card_and_proceed}</Text>

                                        {/* Image - Right Arrow */}
                                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_03]} />
                                    </TouchableOpacity>
                                </View>
                                : null}
                        </View>

                        {/* Wrapper - Net Banking */}
                        <View style={[flexDirectionStyles.column, { margin: 2, marginTop: 10 }]}>

                            {/* TouchableOpacity - Net Banking Button Click Event */}
                            <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, otherStyles.zIndex_1]} activeOpacity={1}
                                onPress={() => showHideNetBankingView()} activeOpacity={1}>

                                {/* Image - Net Banking */}
                                <Image style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05, marginStyles.topMargin_005, { height: 24, width: 24 }]} source={require('../../assets/images/ic_net_banking.png')} />

                                {/* Text - Net Banking */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, paddingStyles.padding_04, fontSizeStyles.fontSize_035]}>{Constants.text_net_banking}</Text>

                                {/* Image - Right Arrow */}
                                {!netBankingView ?
                                    <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                                    :
                                    <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} tintColor={colors.black} />
                                }
                            </TouchableOpacity>

                            {/* Wrapper - Net Banking Details */}
                            {netBankingView ?
                                <View style={[colorStyles.greyBorderColor, borderStyles.borderRadius_15, borderStyles.borderWidth_1, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_25_minus, marginStyles.bottomMargin_04_minus]}>

                                    {/* Wrapper - Net Banking Form Details */}
                                    <View style={[flexDirectionStyles.column, marginStyles.topMargin_02]}>

                                        {/* Input - Search by Bank Name */}
                                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                            underlineColorAndroid='transparent'
                                            placeholder={Constants.validation_search_by_bank_name}
                                            autoCorrect={false}
                                            value={text}
                                            onChangeText={(text) => handleSearch(text)}
                                        />
                                    </View>

                                    {/* Wrapper - Bank Lists */}
                                    <View style={[positionStyles.relative, heightStyles.height_30, flexDirectionStyles.column, borderStyles.borderWidth_1, colorStyles.whiteBackgroundColor, borderStyles.borderRadius_10, colorStyles.greyColor, colorStyles.greyBorderColor, marginStyles.topMargin_02, paddingStyles.padding_02]}>

                                        <FlatList
                                            data={data}
                                            renderItem={({ item, index }) => <NetBankingItem item={item} index={index} amount={amount} navigation={props.navigation} paymentType={AppConstants.PAYMENT_TYPE_NET_BANKING} moveToPaymentGateway={moveToPaymentGateway} />}
                                            keyExtractor={(item, index) => index.toString()}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                    </View>
                                </View>
                                : null}
                        </View>

                        {/* Wrapper - Wallet */}
                        <View style={[flexDirectionStyles.column, { margin: 2, marginTop: 10 }]}>

                            {/* TouchableOpacity - Wallet Button Click Event */}
                            <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, otherStyles.zIndex_1]} activeOpacity={1}
                                onPress={() => showHideWalletView()} activeOpacity={1}>

                                {/* Image - Wallet */}
                                <Image style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05, marginStyles.topMargin_005, { height: 24, width: 24 }]} source={require('../../assets/images/ic_wallet.png')} />

                                {/* Text - Wallet */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, paddingStyles.padding_04, fontSizeStyles.fontSize_035]}>{Constants.text_wallet}</Text>

                                {/* Image - Right Arrow */}
                                {!walletView ?
                                    <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                                    :
                                    <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} tintColor={colors.black} />
                                }
                            </TouchableOpacity>

                            {/* Wrapper - Wallet Details */}
                            {walletView ?
                                <View style={[colorStyles.greyBorderColor, borderStyles.borderRadius_15, borderStyles.borderWidth_1, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_25_minus, marginStyles.bottomMargin_04_minus]}>

                                    {/* Wrapper - Wallet Lists */}
                                    <View style={[positionStyles.relative, heightStyles.height_30, flexDirectionStyles.column, borderStyles.borderWidth_1, colorStyles.whiteBackgroundColor, borderStyles.borderRadius_10, colorStyles.greyColor, colorStyles.greyBorderColor, marginStyles.topMargin_02, paddingStyles.padding_02]}>

                                        <FlatList
                                            data={walletData}
                                            renderItem={({ item, index }) => <NetBankingItem item={item} index={index} amount={amount} navigation={props.navigation} paymentType={AppConstants.PAYMENT_TYPE_WALLET} />}
                                            keyExtractor={(item, index) => index.toString()}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                    </View>
                                </View>
                                : null}
                        </View>

                        {/* Wrapper - UPI Payment */}
                        <View style={[flexDirectionStyles.column, { margin: 2, marginTop: 10 }]}>

                            {/* TouchableOpacity - UPI Payment Button Click Event */}
                            <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, otherStyles.zIndex_1]} activeOpacity={1}
                                onPress={() => showHideUpiPaymentView()} activeOpacity={1}>

                                {/* Image - UPI Payment */}
                                <Image style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05, marginStyles.topMargin_005, { height: 9, width: 30 }]} source={require('../../assets/images/ic_upi.png')} />

                                {/* Text - UPI Payment */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, paddingStyles.padding_04, fontSizeStyles.fontSize_035]}>{Constants.text_upi_payment}</Text>

                                {/* Image - Right Arrow */}
                                {!upiPaymentView ?
                                    <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                                    :
                                    <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} tintColor={colors.black} />
                                }
                            </TouchableOpacity>

                            {/* Wrapper - UPI Payment Details */}
                            {upiPaymentView ?
                                <View style={[colorStyles.greyBorderColor, borderStyles.borderRadius_15, borderStyles.borderWidth_1, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_25_minus, marginStyles.bottomMargin_04_minus]}>

                                    {/* Wrapper - UPI Payment Form Details */}
                                    <View style={[flexDirectionStyles.column, marginStyles.topMargin_02]}>

                                        {/* Text - UPI Address */}
                                        <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035]}> {Constants.text_upi_address} </Text>

                                        {/* Text Input - UPI Address */}
                                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                                            placeholder={Constants.validation_upi_address}
                                            onChangeText={(value) => setUPIAddress(value)} />
                                    </View>

                                    {/* TouchableOpacity - Proceed Button Click Event */}
                                    <TouchableOpacity style={[widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.topMargin_03, alignmentStyles.alignSelfCenter]} onPress={() => checkValidationForUPI()} activeOpacity={0.5}>

                                        {/* Text - Button (Proceed) */}
                                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>{Constants.action_proceed}</Text>

                                        {/* Image - Right Arrow */}
                                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_03]} />
                                    </TouchableOpacity>
                                </View>
                                : null}
                        </View>

                        {/* Wrapper - Paytm Payment */}
                        {showPaytm &&
                            <View style={[flexDirectionStyles.column, marginStyles.margin_01, marginStyles.topMargin_01]}>

                                {/* TouchableOpacity - Paytm Button Click Event */}
                                <TouchableOpacity style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, otherStyles.zIndex_1]}
                                    onPress={() => showHidePaytmView()} activeOpacity={1}>

                                    {/* Image - Paytm */}
                                    <Image style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_05, marginStyles.topMargin_005]} source={require('../../assets/images/ic_paytm.png')} />

                                    {/* Text - Paytm */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, paddingStyles.padding_04, fontSizeStyles.fontSize_035]}>{Constants.text_paytm}</Text>

                                    {/* Image - Right Arrow */}
                                    {!paytmView ?
                                        <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                                        :
                                        <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} tintColor={colors.black} />
                                    }
                                </TouchableOpacity>

                                {/* Wrapper - Paytm Details */}
                                {paytmView ?
                                    <View style={[colorStyles.greyBorderColor, borderStyles.borderRadius_15, borderStyles.borderWidth_1, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_25_minus, marginStyles.bottomMargin_04_minus]}>

                                        {/* Wrapper - Paytm List Details */}
                                        <View style={[flexDirectionStyles.column, marginStyles.topMargin_02]}>

                                            {/* Text - Paytm */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_paytm}</Text>
                                        </View>
                                    </View>
                                    : null}
                            </View>
                        }
                    </ScrollView>

                    {checkIsSponsorAdsEnabled('paymentOptions') &&
                        <SponsorBannerAds screenCode={'paymentOptions'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('paymentOptions')} />
                    }
                </View>
            </View>
        </SafeAreaView>
    )
}

export default PaymentOptions;