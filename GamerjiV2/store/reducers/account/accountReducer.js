import produce from 'immer';
import * as actionTypes from '../../actions/account/accountAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        accountResponse: undefined
    },
    error: false
};

{ /* Request - Account */ }
const accountRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.accountResponse = action.model;
    });
};

{ /* Request Failure - Account */ }
const accountRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.accountResponse = action.model;
    });
};

{ /* Reset State - Account */ }
const resetAccountState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.accountResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_ACCOUNT:
            return accountRequest(state, action);
        case actionTypes.REQUEST_ACCOUNT_FAILED:
            return accountRequestFailed(state, action);
        case actionTypes.RESET_ACCOUNT_STATE:
            return resetAccountState(state);
        default:
            return state;
    }
};

export default reducer;