import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Image, Text, TouchableOpacity, StyleSheet, Keyboard, TextInput, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { showSuccessToastMessage, showErrorToastMessage, addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const BottomSheetUpdateEmail = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [isLoading, setLoading] = useState(false);

    // This function should check the validation first and then call the Apply Promo Code API
    const checkValidation = () => {
        Keyboard.dismiss();
        if (!email.trim()) {
            showErrorToastMessage(Constants.error_enter_email)
            return;
        }
        callToUpdateEmailAPI();
    };

    // This function should call the Update Email API
    const callToUpdateEmailAPI = (props) => {
        let payload = {
            email: email
        }
        setLoading(true)
        dispatch(actionCreators.requestUpdateEmail(payload))
    }

    // This function sets the Update Email Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.updateEmail.model }),
        shallowEqual
    );
    var { updateEmailResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (updateEmailResponse) {
            addLog('API SUCCESSSSSSSSSSSSSSS::::', '')
            setLoading(false)
            showSuccessToastMessage(Constants.success_email_updated)
            setEmail('');
            props.openUpdateEmailPopup();
            props.checkIfEmailExists();

            return () => {
                dispatch(actionCreators.resetUpdateEmailState())
            }
        }
        updateEmailResponse = undefined
    }, [updateEmailResponse])

    return (
        <View style={{ flex: 1 }}>

            {/* Main Bottom Container */}
            <View style={styles.mainContainer}>

                {/* Top curved header */}
                <View style={styles.topRoundCorve}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, marginLeft: 50, marginRight: 50, textAlign: 'center' }}>{Constants.header_update_email}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() =>
                        props.openUpdateEmailPopup()} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Input Container */}
                <View style={styles.bottomContainerView}>

                    <View style={styles.newUserNameContainer}>

                        {/* Text - Invite Code */}
                        <Text style={{ left: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}>{Constants.text_email}</Text>

                        {/* Text Input - Invite Code */}
                        <TextInput
                            style={[styles.textInputs]}
                            placeholder={Constants.validation_email}
                            onChangeText={text => setEmail(text)}
                            spellCheck={false}
                            autoCorrect={false}>
                        </TextInput>
                    </View>

                    {/* TouchableOpacity - Update Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20, { left: 20, right: 20 }]}
                        onPress={() => checkValidation()} activeOpacity={0.5}>

                        {/* Text - Button (Update) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_update}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>

                {checkIsSponsorAdsEnabled('updateEmailPopup') &&
                    <SponsorBannerAds screenCode={'updateEmailPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('updateEmailPopup')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}
        </View>
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        // height: 280,
    },
    topRoundCorve: {
        height: 65,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        backgroundColor: '#FFC609',
        left: 0,
        right: 0,
        position: 'relative',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeButton: {
        height: 65,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 0
    },
    bottomContainerView: {
        backgroundColor: 'white',
        height: 270,
        flexDirection: 'column',
    },
    textInputs: {
        position: 'absolute',
        height: 46,
        borderRadius: 23,
        borderWidth: 1,
        borderColor: '#082240',
        top: 24,
        left: 20,
        right: 20,
        paddingHorizontal: 15,
        fontWeight: '600',
        fontFamily: fontFamilyStyleNew.regular
    },
    newUserNameContainer: {
        top: 36,
        height: 70,
    },
})

export default BottomSheetUpdateEmail;