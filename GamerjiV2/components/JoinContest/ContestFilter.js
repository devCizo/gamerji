import React, { useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput } from 'react-native'
import { addLog } from '../../appUtils/commonUtlis'
import colors from '../../assets/colors/colors'

const ContestFilter = (props) => {

    var contestList = props.contestList

    return (
        <SafeAreaView style={{ flex: 1 }}>

            {/* Main View */}
            <View style={styles.mainContainer} >

                {/* Header View */}
                <View style={styles.headerContainer}>

                    <Text style={{ marginLeft: 30, color: colors.black, fontSize: 18, fontWeight: 'bold' }} >Filters</Text>
                    <View style={styles.headerCloseButtonContaier} >

                        {/* Clear All */}
                        <TouchableOpacity style={styles.closeButton} onPress={
                            null
                        } activeOpacity={0.5} >
                            <Text style={{ color: colors.red, fontWeight: '600', fontSize: 12 }} >Clear All</Text>
                        </TouchableOpacity>

                        {/* Close Button */}
                        <TouchableOpacity style={styles.closeButton} onPress={() => props.setFilterPopopVisibility(false)} activeOpacity={0.5} >
                            <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* Separator Line */}
                <Image style={{ backgroundColor: colors.red, height: 1, marginLeft: 28, marginRight: 28 }} ></Image>

                {/* Center Content */}
                <View style={styles.centerConentContainer} >

                    {/* Filter Types */}
                    <View style={styles.filterTypesContainer} >

                        <FlatList
                            style={{ flex: 1 }}
                            data={FilterTypes}
                            renderItem={({ item }) => <FilterTypesItem item={item} />}
                        />
                    </View>

                    {/* Search by name view */}
                    <View style={styles.searchByNameContainer} >

                        {/* BG Image */}
                        <Image style={{ position: 'absolute', backgroundColor: 'white', top: 0, left: 0, right: 0, bottom: 0, opacity: 0.3 }} />

                        {/* Search bar */}
                        <View style={{ marginTop: 20, height: 40, alignItems: 'center', flexDirection: 'row' }} >
                            <Image source={require('../../assets/images/search_icon_filter.png')} style={{ left: 25, height: 12, width: 12 }} />
                            <TextInput style={{ alignSelf: 'stretch', flex: 1, marginLeft: 40, fontSize: 12 }} placeholder='Search' placeholderTextColor={colors.black} ></TextInput>
                            <Image style={{ position: 'absolute', left: 25, right: 25, bottom: 0, height: 0.5, backgroundColor: colors.black }} />
                        </View>

                        {/* Contest Name List */}
                        <FlatList
                            // style = {{ backgroundColor: 'red' }}
                            data={contestList}
                            renderItem={({ item }) => <FilterContestListItem item={item} />}
                        />
                    </View>
                </View>

                {/* Filter Button */}
                <TouchableOpacity style={styles.applyFilterButton} onPress={() => navigation.navigate(Constants.nav_contest_filter)} activeOpacity={0.5} >
                    <Text style={{ marginLeft: 22, color: colors.white, fontWeight: 'bold', fontSize: 16 }} >APPLY</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                </TouchableOpacity>

            </View>
        </SafeAreaView>
    )
}

function FilterTypesItem({ item }) {
    return (
        <View style={{ height: 52, flexDirection: 'row', }} >

            {item.selected ?
                <Image style={{ position: 'absolute', top: 0, bottom: 0, left: 0, width: 3, backgroundColor: 'white' }} /> :
                <Image />
            }
            <Text style={item.selected ? styles.filterSelectedTitle : styles.filterDeSelectedTitle}  >{item.title}</Text>

            {/* <TouchableOpacity style = {{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }} onPress = { () => selectFilterType(item) } >
                
            </TouchableOpacity> */}

        </View>
    )
}

function FilterContestListItem({ item }) {

    let contest = { ...item }
    const [selected, setContestSelected] = useState(false)

    let select = () => {
        if (selected) {
            setContestSelected(true)
        } else {
            setContestSelected(false)
        }
        // contest['selected'] = selected
        // addLog('item[selected]', contest)
    }

    return (
        <TouchableOpacity style={{ marginLeft: 24, height: 50, alignItems: 'center', flexDirection: 'row' }} onPress={() => select()} activeOpacity={0.5} >
            {selected ?
                <Image source={require('../../assets/images/check_box_filter.png')} style={{ height: 12, width: 12 }} /> :
                <Image source={require('../../assets/images/uncheck_box_filter.png')} style={{ height: 12, width: 12 }} />
            }
            <Text style={{ color: colors.black, fontSize: 12, marginLeft: 5, flex: 1 }} numberOfLines={2} >{contest.title}</Text>
        </TouchableOpacity>
    )
}

function selectFilterType({ item }) {
    item.selected = true
}

const styles = StyleSheet.create({

    mainContainer: {
        height: 450,
        backgroundColor: colors.yellow,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerCloseButtonContaier: {
        flexDirection: 'row'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerConentContainer: {
        height: 260,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    filterTypesContainer: {
        width: 90,
        backgroundColor: colors.red,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
    },
    filterDeSelectedTitle: {
        marginLeft: 12, alignSelf: 'center', fontWeight: '600', fontSize: 14, color: 'white'
    },
    filterSelectedTitle: {
        marginLeft: 12, alignSelf: 'center', fontWeight: '600', fontSize: 14, color: colors.yellow
    },
    searchByNameContainer: {
        alignSelf: 'stretch',
        flex: 1,
        marginLeft: 20,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        overflow: 'hidden'
    },
    applyFilterButton: {
        height: 46,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})


const FilterTypes = [
    {
        selected: true,
        title: 'Entry Fee',
        id: 0
    },
    {
        selected: false,
        title: 'By Host',
        id: 1
    },
    {
        selected: false,
        title: 'Date',
        id: 2
    },
    {
        selected: false,
        title: 'Time',
        id: 3
    },
    {
        selected: false,
        title: 'Prize Pool',
        id: 4
    },
];

export default ContestFilter;