import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, ScrollView, Image, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import moment from 'moment';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import BottomSheetDailyLoginRewardsItem from '../../componentsItem/BottomSheetDailyLoginRewardsItem';
import { AppConstants } from '../../appUtils/appConstants';
import { retrieveDataFromLocalStorage } from '../../appUtils/sessionManager';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomProgressbar from '../../appUtils/customProgressBar';
import Modal from 'react-native-modal';
import DailyLoginAvatarPopup from './dailyLoginAvatarPopup';
import * as generalSetting from '../../webServices/generalSetting';

const BottomSheetDailyLoginRewards = (props) => {

    //  addLog('props.global.profile:::> ', global.profile)
    ///sdsdsdd
    // Variable Declarations
    const dispatch = useDispatch();
    const [dailyLoginRewards, setDailyLoginRewards] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [isOpenDailyLoginAvatarPopup, setOpenDailyLoginAvatarPopup] = useState(false)
    const [isOneTimeBonusCollected, setOneTimeBonusCollected] = useState(false);
    // This Use-Effect function should call the method for the first time
    const [dailyLoginRewardItem, setDailyLoginRewardItem] = useState(undefined);
    let openDailyLoginAvatarPopup = (item) => {
        addLog('openHtmlGamePopup', item)
        setDailyLoginRewardItem(item);
        setOpenDailyLoginAvatarPopup(true);
    }
    useEffect(() => {
        callToDailyLoginRewardsListAPI();

        return () => {
            dispatch(actionCreators.resetBottomSheetDailyLoginRewardsState())
        }
    }, [])

    // This Use-Effect function should call the Daily Login Rewards API
    const callToDailyLoginRewardsListAPI = () => {
        addLog('pppppppppprops.currentDay:::> ', props.currentDay)
        let numbers = '';
        if (props.currentDay <= 4 || props.currentDay == 2) {
            numbers = [1, 2, 3, 4, 5, 6, 7]
        } else {
            numbers = [(props.currentDay - 3), (props.currentDay - 2), (props.currentDay - 1), props.currentDay, props.currentDay + 1, props.currentDay + 2, props.currentDay + 3]
        }

        let payload = {
            skip: 0,
            day: props.currentDay,
            days: numbers,
            sortBy: 'day',
            sort: 'asc'
        }
        addLog('Payload:::> ', payload)
        dispatch(actionCreators.requestBottomSheetDailyLoginRewards(payload))
    }

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.bottomSheetDailyLoginRewards.model }),
        shallowEqual
    );
    var { bottomSheetDailyLoginRewardsResponse, claimRewardResponse } = currentState

    // This function should return the Daily Login Rewards API Response
    useEffect(() => {
        if (bottomSheetDailyLoginRewardsResponse && bottomSheetDailyLoginRewardsResponse.list) {
            setDailyLoginRewards(bottomSheetDailyLoginRewardsResponse.list)
        }
        bottomSheetDailyLoginRewardsResponse = undefined
    }, [bottomSheetDailyLoginRewardsResponse])

    // This function should be redirected to the View All Daily Login Rewards Screen 

    const getCurrentDayItem = () => {

        let index = dailyLoginRewards.findIndex(element => {
            if (element.day == props.currentDay) {
                return true
            }
        })

        if (index > -1) {
            return dailyLoginRewards[index]
        }
    }

    // Claim Reward
    const claimReward = () => {
        const rewardToClaim = getCurrentDayItem()

        if (rewardToClaim) {
            setLoading(true)
            dispatch(actionCreators.requestClaimReward({ dailyReward: rewardToClaim._id }))
            setTimeout(function () { props.openDailyRewardsPopup(false) }, 500);
        }
    }
    const claimOneTimeBonus = () => {
        dispatch(actionCreators.requestClaimOneTimeBonus({ amount: props.oneTimeBonusAmount }))
        setTimeout(function () {
            props.setOneTimeBonus(false);
            setOneTimeBonusCollected(true);
        }, 500)
    }

    useEffect(() => {
        if (claimRewardResponse) {

            setLoading(false)
            if (claimRewardResponse.item) {

                let tempIndex = dailyLoginRewards.findIndex(element => {
                    if (element.day == props.currentDay) {
                        return true
                    }
                })

                var tempDailyLoginRewards = []
                dailyLoginRewards.forEach((item, index) => {
                    var tempItem = { ...item }
                    if (index == tempIndex) {
                        tempItem.isClaimed = true
                    }
                    tempDailyLoginRewards.push(tempItem)
                })

                setDailyLoginRewards(tempDailyLoginRewards)

                if (claimRewardResponse.item.msg) {
                    showErrorToastMessage(claimRewardResponse.item.msg)
                }
            } else if (claimRewardResponse.errors) {
                if (claimRewardResponse.errors[0] && claimRewardResponse.errors[0].msg) {
                    showErrorToastMessage(claimRewardResponse.errors[0].msg)
                }
            }
        }
    }, [claimRewardResponse])

    return (
        <>
            <View style={styles.mainContainer}>

                {/* Close Button */}
                <TouchableOpacity style={styles.closeButton} onPress={() => props.openDailyRewardsPopup(false)} activeOpacity={0.5}>
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
                {/* </View> */}
                <View style={{

                    marginTop: 20,
                    marginLeft: 20,
                    marginRight: 20,
                    alignSelf: 'center'
                }}><Text style={{ textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 26 }}>{Constants.header_daily_login_rewards}</Text></View>

                <ScrollView style={{ marginTop: 10, marginBottom: 60 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyboardShouldPersistTaps='always'>

                    {props.isOneTimeBonus ?
                        global.profile && global.profile.isOneTimeBonus == false ?
                            <View style={{

                                marginTop: 10,
                                marginLeft: 20,
                                marginRight: 20,
                                alignSelf: 'center'
                            }}>
                                {/* <Text style={{ textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 26 }}>One Time Bonus</Text> */}
                                <TouchableOpacity onPress={() => claimOneTimeBonus()} activeOpacity={0.5}>
                                    {/* <Image style={{ width: 250, resizeMode: 'contain' }} source={{ uri: props.oneTimeBonusImg && props.oneTimeBonusImg.default && generalSetting.UPLOADED_FILE_URL + props.oneTimeBonusImg.default }} /> */}
                                    <Image style={{ resizeMode: 'contain', height: 100, borderRadius: 10 }} source={require('../../assets/images/free_coins.png')} />
                                </TouchableOpacity>
                            </View> :
                            <View></View>
                        : <View></View>
                    }
                    {isOneTimeBonusCollected ?

                        <View style={{

                            marginTop: 10,
                            marginLeft: 20,
                            marginRight: 20,
                            alignSelf: 'center'
                        }}>
                            <Image style={{ resizeMode: 'contain', height: 90, borderRadius: 10 }} source={require('../../assets/images/congatulations-for-coins.jpg')} />
                        </View>

                        : <View></View>
                    }

                    {/* Center Content */}
                    <View style={{
                        marginTop: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        alignSelf: 'center',

                        width: "92%"

                    }}>

                        {/* Image - Daily Rewards */}
                        {/* <Image style={{ height: 95, width: 95, alignSelf: 'center' }} source={require('../../assets/images/ic_daily_rewards.png')} /> */}

                        {/* Text - Daily Login Rewards */}


                        {/* Text - Today Date */}
                        {/* <Text style={{ textAlign: 'center', marginTop: 7, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>{Constants.text_date_header} {moment(Date()).format('DD/MM/YYYY')}</Text> */}

                        {/* Text - Rewards */}
                        {/* <Text style={{ textAlign: 'center', marginTop: 10, fontFamily: fontFamilyStyleNew.bold, fontSize: 23, color: colors.red }}>{Constants.text_rewards}</Text> */}

                        {/* FlastList */}
                        <FlatList style={{ marginTop: 18 }}
                            data={dailyLoginRewards}
                            renderItem={({ item, index }) => <BottomSheetDailyLoginRewardsItem item={item} currentDay={props.currentDay} setOpenDailyLoginAvatarPopup={setOpenDailyLoginAvatarPopup} openDailyLoginAvatarPopup={openDailyLoginAvatarPopup} />}
                            keyExtractor={(item) => item.id}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        />


                    </View>
                </ScrollView>
                {/* TouchableOpacity - View More Details Button Click Event */}
                {getCurrentDayItem() && getCurrentDayItem().isClaimed && getCurrentDayItem().isClaimed == true ? <Text></Text> :

                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_90p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, { marginBottom: 10 }]}
                        onPress={() => claimReward()} activeOpacity={0.5}>

                        {/* Text - Button (View More Details) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>COLLECT REWARDS</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                }

                {checkIsSponsorAdsEnabled('dailyLoginRewardsPopup') &&
                    <SponsorBannerAds screenCode={'dailyLoginRewardsPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('dailyLoginRewardsPopup')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}

            <Modal
                isVisible={isOpenDailyLoginAvatarPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <DailyLoginAvatarPopup reward={dailyLoginRewardItem} setOpenDailyLoginAvatarPopup={setOpenDailyLoginAvatarPopup} claimReward={claimReward} />
            </Modal>
        </>
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        height: 550,
        backgroundColor: colors.yellow,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerContentContainer: {
        marginTop: 40,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: 'center'
    },
})

export default BottomSheetDailyLoginRewards;