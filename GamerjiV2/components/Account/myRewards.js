import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import RewardStoreRedeemPopup from './rewardStoreRedeemPopup';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const MyRewards = (props) => {

    const dispatch = useDispatch()
    const [rewards, setRewards] = useState([])
    const [isLoading, setLoading] = useState(false)
    const [isOpenRedeemPopup, setOpenRedeemPopup] = useState(false)
    const [selectedVoucher, setSelectedVoucher] = useState(undefined)

    useEffect(() => {

        setLoading(true);
        dispatch(actionCreators.requestMyRewardList({}))

        return () => {
            dispatch(actionCreators.resetMyRewardsState())
        }
    }, []);

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.myRewards.model }),
        shallowEqual
    );
    var { myRewardListResponse } = currentState
    addLog('currentState', currentState)

    // Category Product List Response
    useEffect(() => {
        if (myRewardListResponse) {
            setLoading(false)

            if (myRewardListResponse.list) {
                setRewards(myRewardListResponse.list)
            }
        }
        myRewardListResponse = undefined
    }, [myRewardListResponse])

    const infoPresses = item => {
        setSelectedVoucher(item)
        setOpenRedeemPopup(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            <View style={{ flex: 1, backgroundColor: colors.black }}>

                {/* Navigation Bar */}
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} >

                    {/* Back Button */}
                    {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={RootNavigation.goBack}>
                        <Image style={{ width: 25, height: 23, alignSelf: 'center' }} source={require('../../assets/images/back_icon.png')} />
                    </TouchableOpacity>}

                    {/* Game Name */}
                    <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >MY REWARDS</Text>
                </View>

                <View style={{ flex: 1 }}>
                    <FlatList style={{ flex: 1 }}

                        style={{ marginTop: 8, marginLeft: 7.5, marginRight: 7.5 }}
                        data={rewards}
                        renderItem={({ item, index }) => <MyRewardsItem item={item} infoPresses={infoPresses} />}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

                {isLoading && <CustomProgressbar />}

                {isOpenRedeemPopup &&
                    <Modal
                        isVisible={isOpenRedeemPopup}
                        coverScreen={false}
                        testID={'modal'}
                        style={{ justifyContent: 'flex-end', margin: 0 }}
                    >
                        <RewardStoreRedeemPopup setOpenRedeemPopup={setOpenRedeemPopup} voucher={selectedVoucher} />
                    </Modal>
                }

                {checkIsSponsorAdsEnabled('myRewards') &&
                    <SponsorBannerAds screenCode={'myRewards'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('myRewards')} />
                }
            </View>
        </SafeAreaView>
    )
}

export default MyRewards

const MyRewardsItem = (props) => {

    const width = (Constants.SCREEN_WIDTH - 45) / 2

    return (
        <View style={{ marginTop: 7.5, marginLeft: 7.5, marginRight: 7.5, marginBottom: 7.5, width: width, height: width + 55, borderRadius: 9, borderColor: colors.white, borderWidth: 1, overflow: 'hidden' }} >

            <Image style={{ flex: 1, resizeMode: 'contain' }} source={{ uri: props.item.img && props.item.img.default && generalSetting.UPLOADED_FILE_URL + props.item.img.default }} ></Image>

            <View style={{ height: 80, backgroundColor: colors.white, borderBottomLeftRadius: 8, borderBottomRightRadius: 8 }}>

                <View style={{ marginTop: 8 }} >
                    {/* Voucher Name */}
                    <CustomMarquee style={{ marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={0} value={props.item.name} />
                </View>

                <Text style={{ marginTop: 5, marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, }} >{props.item.rewardCategory && props.item.rewardCategory.name}</Text>

                <View style={{ marginTop: 8, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }} >

                    <View style={{ height: 20, alignItems: 'center', flexDirection: 'row' }}>
                        {/* Discount Image */}
                        <Image style={{ marginLeft: 6, height: 15, width: 14, resizeMode: 'contain' }} source={require('../../assets/images/discount-reward.png')} />
                        <Text style={{ marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, }} >{props.item.coinAmount}</Text>
                    </View>

                    <TouchableOpacity style={{ marginRight: 4, height: 20, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.yellow, borderRadius: 10 }} activeOpacity={0.6} onPress={() => props.infoPresses(props.item)}>
                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, }} >Info</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View>
    )
}