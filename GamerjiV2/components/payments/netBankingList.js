import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, BackHandler, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import AddCardPopup from './addCardPopup';
import { AppConstants } from '../../appUtils/appConstants';
import bankData from '../../assets/data/bankData';
import PaymentStatusPopup from './paymentStatusPopup'

const NetBankingList = (props) => {

    const [bankList, setBankList] = useState(bankData)
    const [searchBank, setSearchBank] = useState('')

    const [isOpenPaymentStatusPopup, setOpenPaymentStatusPopup] = useState(false)
    const [paymentGatewayResult, setPaymentGatewayResult] = useState(undefined)

    // Search Bank List
    const handleBankSearch = text => {
        const newData = bankData.filter(item => {
            const itemData = item.name.toLowerCase()
            const textData = text.toLowerCase()
            return itemData.indexOf(textData) > -1
        })
        setBankList(newData)
        setSearchBank(text)
    }

    const moveToPaymentGateway = bankData => {
        var params = {
            paymentType: AppConstants.PAYMENT_TYPE_NET_BANKING,
            bankId: bankData.id,
            amount: props.route.params.amount,
            paymentTypeForCreateTransaction: props.route.params.paymentTypeForCreateTransaction
        }

        if (props.route.params.coinStore) {
            params['coinStore'] = props.route.params.coinStore
        }

        if (props.route.params.avatarId) {
            params['avatarId'] = props.route.params.avatarId
        }
        props.navigation.push(Constants.nav_payment_gateway_new, { params, paymentStatusCallBack })
    }

    const paymentStatusCallBack = (paymentGatewayResult) => {
        setPaymentGatewayResult(paymentGatewayResult)
        setOpenPaymentStatusPopup(true)
    }

    const paymentStatusPopupClosed = () => {
        setOpenPaymentStatusPopup(false)

        if (global.paymentSuccessCompletion) {
            global.paymentSuccessCompletion()
        }

        global.paymentSuccessCompletion = undefined

        if (global.addBalanceScreenType == Constants.contest_coin_payment_type || global.addBalanceScreenType == Constants.contest_money_payment_type) {
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.tournament_coin_payment_type || global.addBalanceScreenType == Constants.tournament_money_payment_type) {
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.all_games_add_money) {
            props.navigation.navigate(Constants.nav_account_new)
        } else {
            props.navigation.navigate(Constants.nav_account_new)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={() => RootNavigation.goBack()}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center', tintColor: colors.white }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >NET BANKING</Text>
            </View>

            {/* Main Content View */}
            <View style={{ flex: 1, backgroundColor: colors.white, borderTopLeftRadius: 40, borderTopRightRadius: 40 }} >

                {/* Current Balance */}
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, backgroundColor: colors.red, borderRadius: 25, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 24, height: 16, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_currency.png')} />
                        <Text style={{ marginLeft: 12, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Balance to Add</Text>
                    </View>

                    {/* Ballance */}
                    <Text style={{ marginRight: 24, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹{props.route.params.amount}</Text>
                </View>

                {/* Search bar */}
                <View style={{ marginTop: 32, marginLeft: 20, marginRight: 20, height: 44, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row', alignItems: 'center' }}>

                    {/* Search Icon */}
                    <Image style={{ marginLeft: 15, height: 18, width: 18 }} source={require('../../assets/images/search-net-banking.png')} />

                    {/* Input */}
                    <TextInput style={{ height: 44, flex: 1, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                        placeholder='Search by bank name'
                        placeholderTextColor={colors.black}
                        autoCorrect={false}
                        onChangeText={(text) => handleBankSearch(text)}
                    >{searchBank}
                    </TextInput>
                </View>

                <FlatList
                    style={{ marginTop: 16, marginLeft: 20, marginRight: 20, marginBottom: 20, flex: 1, borderRadius: 10, borderWidth: 1, borderColor: '#D5D7E3' }}
                    data={bankList}
                    renderItem={({ index, item }) => <NetBankingListItem item={item} index={index} moveToPaymentGateway={moveToPaymentGateway} />}
                    showsVerticalScrollIndicator={false}
                />

            </View>

            <Modal
                isVisible={isOpenPaymentStatusPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <PaymentStatusPopup paymentStatusPopupClosed={paymentStatusPopupClosed} paymentGatewayResult={paymentGatewayResult} />
            </Modal>

        </SafeAreaView>
    )
}

const NetBankingListItem = props => {

    const item = props.item

    return (
        <TouchableOpacity style={{ height: 44, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => props.moveToPaymentGateway(item)} activeOpacity={0.5} >
            <Text style={{ marginLeft: 14, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >{item.name}</Text>
            <Image style={{ marginRight: 14, width: 16, height: 14, resizeMode: 'contain', tintColor: colors.black }} source={require('../../assets/images/right_arrow.png')}></Image>
            <Image style={{ position: 'absolute', left: 14, right: 14, bottom: 0, height: 1, backgroundColor: '#E4E5ED' }} />
        </TouchableOpacity>
    )
}

export default NetBankingList