import axios from '../../../webServices/axios-api';
import { AppConstants, cashFreeClientId, cashFreeSecretId } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import * as generalSetting from '../../../webServices/generalSetting';

{ /* Request - GET SAVED CARDS */ }
export const requestGetSavedCards = (payload) => {

    addLog("URL:::> ", generalSetting.CASHFREE_GET_SAVED_CARDS_URL);
    addLog("Payload:::> ", payload);

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.CONTENT_TYPE] = WebFields.HEADERS.CONTENT_TYPE_VALUE;
        axios.defaults.headers.post[WebFields.HEADERS.X_CLIENT_ID] = cashFreeClientId();
        axios.defaults.headers.post[WebFields.HEADERS.X_CLIENT_SECRET] = cashFreeSecretId();
        axios
            .post(generalSetting.CASHFREE_GET_SAVED_CARDS_URL, payload)
            .then((response) => {
                addLog("URL:::> ", generalSetting.CASHFREE_GET_SAVED_CARDS_URL);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                // dispatch(generatePaymentTokenRequest(response.data));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                // dispatch(generatePaymentTokenRequestFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};