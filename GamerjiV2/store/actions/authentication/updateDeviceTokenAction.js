
import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const updateDeviceToken = (viewModel) => {
    return {
        type: UPDATE_DEVICE_TOKEN,
        model: viewModel
    };
};

export const updateDeviceTokenFailed = (viewModel) => {
    return {
        type: UPDATE_DEVICE_TOKEN_FAILED,
        model: viewModel
    };
};

export const requestUpdateDeviceToken = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(updateDeviceToken(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(updateDeviceTokenFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetUpdateDeviceTokenState = () => {
    return {
        type: RESET_UPDATE_DEVICE_TOKEN_STATE,
    };
};

export const UPDATE_DEVICE_TOKEN = 'UPDATE_DEVICE_TOKEN';
export const UPDATE_DEVICE_TOKEN_FAILED = 'UPDATE_DEVICE_TOKEN_FAILED';
export const RESET_UPDATE_DEVICE_TOKEN_STATE = 'RESET_UPDATE_DEVICE_TOKEN_STATE';