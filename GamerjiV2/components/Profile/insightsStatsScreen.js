import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, Dimensions, ScrollView, BackHandler, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew, heightStyles } from '../../appUtils/commonStyles';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Constants } from '../../appUtils/constants';
import { LineChart, BarChart, PieChart } from "react-native-chart-kit";
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import moment from 'moment';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomProgressbar from '../../appUtils/customProgressBar';

const InsightsStatsScreen = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();

    var state = props.route.params.state
    var gameId = state.game._id
    const [isLoading, setLoading] = useState(false)

    const [currentFormTblArr, setCurrentFormTblArr] = useState([])
    const [currentFormArr, setCurrentFormArr] = useState([])
    const [rankSummaryArr, setRankSummaryArr] = useState([])

    const [showCurrentForm, setShowCurrentForm] = useState(false)
    const [showCurrentFormWithKill, setShowCurrentFormWithKill] = useState(false)
    const [showKillPerformance, setShowKillPerformance] = useState(false)
    const [showRank, setShowRank] = useState(false)
    const [showRankSummary, setShowRankSummary] = useState(false)
    const [showWinLose, setShowWinLose] = useState(true)


    const [refreshing, setRefreshing] = useState(false);


    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setCurrentFormArr([])
        setRankSummaryArr([])
        setCurrentFormTblArr([])

        dispatch(actionCreators.requestGameDetail(gameId))

        return () => {
            dispatch(actionCreators.resetInsightStatsState())
        }

    }, [refreshing]);

    //Initialize
    useEffect(() => {

        setLoading(true)
        dispatch(actionCreators.requestGameDetail(gameId))

        return () => {
            dispatch(actionCreators.resetInsightStatsState())
        }
    }, []);

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.insightsStats.model }),
        shallowEqual
    );
    var { gameDetailResponse, contestStatsListResponse, rankSummaryResponse } = currentState

    // Profile Response
    useEffect(() => {
        if (gameDetailResponse) {
            if (gameDetailResponse && gameDetailResponse.item && gameDetailResponse.item.settings) {

                dispatch(actionCreators.requestContestStatList({ game: gameId, limit: 10 }))

                if (gameDetailResponse.item.settings.isCurrentFormShow) {
                    setShowCurrentForm(gameDetailResponse.item.settings.isCurrentFormShow)
                }
                if (gameDetailResponse.item.settings.isCurrentFormShowWithKill) {
                    setShowCurrentFormWithKill(gameDetailResponse.item.settings.isCurrentFormShowWithKill)
                }
                if (gameDetailResponse.item.settings.isKillPerformanceShow) {
                    setShowKillPerformance(gameDetailResponse.item.settings.isKillPerformanceShow)
                }
                if (gameDetailResponse.item.settings.isRankShow) {
                    setShowRank(gameDetailResponse.item.settings.isRankShow)
                }
                if (gameDetailResponse.item.settings.isRankSummaryShow) {
                    setShowRankSummary(gameDetailResponse.item.settings.isRankSummaryShow)
                    dispatch(actionCreators.requestRankSummary({ game: gameId, isShowWinLose: false }))
                }
                if (gameDetailResponse.item.settings.isWinLoseShow) {
                    setShowWinLose(gameDetailResponse.item.settings.isWinLoseShow)
                    dispatch(actionCreators.requestRankSummary({ game: gameId, isShowWinLose: true }))
                }
            } else {
                setLoading(false)
                setRefreshing(false)

            }
        }
    }, [gameDetailResponse])

    // Profile Response
    useEffect(() => {
        if (contestStatsListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (contestStatsListResponse.list) {
                if (contestStatsListResponse.list.length > 5) {
                    setCurrentFormTblArr(contestStatsListResponse.list.slice(0, 5))
                } else {
                    setCurrentFormTblArr(contestStatsListResponse.list)
                }
                setCurrentFormArr(contestStatsListResponse.list)
            }
        }
        contestStatsListResponse = undefined
    }, [contestStatsListResponse])

    // Rank Summary Response
    useEffect(() => {
        if (rankSummaryResponse && rankSummaryResponse.list) {

            var tempArr = []

            if (showRankSummary) {
                let temp = rankSummaryResponse.list.map(element => {

                    var data = {
                        population: element.value,
                        legendFontColor: "white",
                        legendFontSize: 15,
                        name: '(' + element.name + ')'
                    }

                    if (tempArr.length == 0) {
                        data['color'] = '#CD23EE'
                    } else if (tempArr.length == 1) {
                        data['color'] = '#F25E81'
                    } else if (tempArr.length == 2) {
                        data['color'] = '#FFFFFF'
                    } else if (tempArr.length == 3) {
                        data['color'] = '#D88201'
                    } else if (tempArr.length == 4) {
                        data['color'] = '#098880'
                    }
                    tempArr.push(data)
                })
            } else {
                var temp = Object.keys(rankSummaryResponse.list).map(function (key) {

                    var data = {
                        population: rankSummaryResponse.list[key],
                        legendFontColor: "white",
                        legendFontSize: 15,
                    }

                    if (key == 'totalWin') {
                        data['name'] = '% Win'
                    } else {
                        data['name'] = '% Lose'
                    }

                    if (tempArr.length == 0) {
                        data['color'] = '#CD23EE'
                    } else if (tempArr.length == 1) {
                        data['color'] = '#F25E81'
                    }
                    tempArr.push(data)
                })
            }
            setRankSummaryArr(tempArr)
        }
        rankSummaryResponse = undefined
    }, [rankSummaryResponse])

    const getKillsChartLabels = () => {
        let obj = currentFormArr.map(element => {
            if (element.contest && element.contest.date) {
                return moment(element.contest.date).format('DD')
            }
        })

        return obj
    }

    const getKillsChartValues = () => {
        let obj = currentFormArr.map(element => {
            return element.kills
        })
        return obj
    }

    const getRankGraphLabels = () => {
        let obj = currentFormArr.map(element => {
            if (element.contest && element.contest.date) {
                return moment(element.contest.date).format('DD')
            }
        })
        return obj
    }

    const getRankChartValues = () => {
        let obj = currentFormArr.map(element => {
            return element.rank || 0
        })
        return obj
    }

    const showRankSummaryGraph = () => {

        if (showRankSummary && rankSummaryArr.length > 0) {
            return true
        }
        if (showWinLose && rankSummaryArr.length > 0) {
            return true
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }} >

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', justifyContent: 'flex-start' }} >

                {/* Back Button */}
                {<TouchableOpacity style={{ width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={{ width: 25, height: 23 }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: 'white', fontFamily: fontFamilyStyleNew.bold, fontSize: 20, textAlign: 'center', alignSelf: 'center', }} numberOfLines={1} >{state.game && state.game.name} Stats</Text>
            </View>

            <ScrollView style={{ flex: 1 }}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >

                {/* Current Form */}
                {currentFormTblArr && currentFormTblArr.length > 0 &&
                    <View style={{ marginTop: 30 }} >
                        <Text style={{ alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 18 }} >Current Form</Text>

                        {/* Current Form Chart */}
                        <View style={{ marginTop: 15, marginLeft: 10, marginRight: 10, height: 40, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, flexDirection: 'row', alignItems: 'center' }} >

                            {/* Date */}
                            <Text style={{ width: 70, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Date</Text>

                            {/* Game Type */}
                            <Text style={{ flex: 1, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Game Type</Text>

                            {/* Rank */}
                            <Text style={{ width: 56, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Rank</Text>

                            {/* Kill */}
                            {showCurrentFormWithKill == true &&
                                <Text style={{ width: 56, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Kill</Text>
                            }
                        </View>

                        <FlatList
                            style={{ marginLeft: 10, marginRight: 10, borderWidth: 1, borderColor: '#393D54', borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}
                            data={currentFormTblArr}
                            renderItem={({ item, index }) => <InsightStatsCurrentFormItem item={item} index={index} isLastIndex={currentFormTblArr.length == index + 1} showCurrentFormWithKill={showCurrentFormWithKill} />}
                        />

                        {/* Load More */}
                        {currentFormArr.length > 5 && currentFormArr.length != currentFormTblArr.length &&
                            <TouchableOpacity style={{ width: 100, marginTop: 10, marginRight: 10, height: 30, justifyContent: 'center', alignSelf: 'center' }} activeOpacity={0.5} onPress={() => setCurrentFormTblArr(currentFormArr)} >
                                <Text style={{ textAlign: 'center', color: colors.yellow, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Load More</Text>
                                {/* <Image style={{ height: 1, backgroundColor: colors.yellow }} /> */}
                            </TouchableOpacity>
                        }
                    </View>
                }

                {/* Kills Performance */}
                {showKillPerformance && currentFormArr.length > 0 &&
                    <View style={{ marginTop: 25, marginBottom: 15 }} >
                        <Text style={{ alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 18 }} >Kills Performance</Text>

                        <View style={{ marginTop: 16, marginLeft: 10, marginRight: 10, backgroundColor: '#1F2237', borderRadius: 20, overflow: 'hidden' }} >

                            <BarChart
                                data={{
                                    labels: getKillsChartLabels(),
                                    datasets: [
                                        {
                                            data: getKillsChartValues()
                                        }
                                    ]
                                }}
                                width={Constants.SCREEN_WIDTH - 20}
                                height={220}
                                yAxisLabel=""
                                chartConfig={{
                                    backgroundColor: null,
                                    backgroundGradientFrom: '#1F2237',
                                    backgroundGradientTo: null,
                                    decimalPlaces: 0,
                                    color: (opacity = 1) => colors.yellow,
                                    labelColor: (opacity = 1) => colors.white,
                                    style: {
                                        borderRadius: 16
                                    },
                                    fillShadowGradient: colors.yellow,
                                    fillShadowGradientOpacity: 1,
                                    barPercentage: 0.6,
                                    // barRadius: 10
                                }}
                                // segments={8}
                                verticalLabelRotation={0}

                                style={{
                                    marginLeft: -10
                                }}

                                withDots={false}
                                withInnerLines={false}
                                withOuterLines={false}

                            // withHorizontalLabels={false}
                            />
                        </View>

                        <Text style={{ position: 'absolute', top: 140, left: 10, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >Kills</Text>

                        <Text style={{ marginTop: 8, alignSelf: 'center', color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Date</Text>
                    </View>
                }

                {/* Rank Graph */}
                {showRank && currentFormArr.length > 0 &&
                    <View style={{ marginTop: 25, marginBottom: 15 }} >
                        <Text style={{ alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 18 }} >Rank Performance</Text>

                        <View style={{ marginTop: 16, marginLeft: 10, marginRight: 10, backgroundColor: '#1F2237', borderRadius: 20, overflow: 'hidden' }} >

                            <LineChart
                                style={{}}
                                data={{
                                    labels: getRankGraphLabels(),
                                    datasets: [
                                        {
                                            data: getRankChartValues()
                                        }
                                    ]
                                }}
                                width={Constants.SCREEN_WIDTH}
                                height={220}
                                yAxisLabel=''
                                yAxisSuffix=''
                                yAxisInterval={1}
                                chartConfig={{
                                    backgroundColor: null,
                                    backgroundGradientFrom: '#1F2237',
                                    backgroundGradientTo: null,
                                    decimalPlaces: 0,
                                    color: (opacity = 1) => colors.yellow,
                                    labelColor: (opacity = 1) => colors.white,
                                    style: {
                                        borderRadius: 16
                                    },
                                    propsForDots: {
                                        r: "6",
                                        strokeWidth: "0",
                                        // stroke: colors.blue
                                    },

                                    fillShadowGradient: colors.yellow,
                                    fillShadowGradientOpacity: 0.5
                                }}
                                bezier
                                style={{
                                    marginLeft: -10
                                }}

                                // withDots={false}
                                withInnerLines={false}
                                withOuterLines={false}
                            // withHorizontalLabels={false}

                            />
                        </View>

                        <Text style={{ position: 'absolute', top: 140, left: 10, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >Rank</Text>

                        <Text style={{ marginTop: 8, alignSelf: 'center', color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Last 14 Matches</Text>
                    </View>
                }

                {/* Rank Summary */}
                {showRankSummaryGraph() &&
                    <View style={{ marginTop: 25, marginBottom: 20 }} >
                        <Text style={{ alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 18 }} >Rank Summary</Text>

                        <View style={{ marginTop: 16, marginLeft: 10, marginRight: 10, backgroundColor: '#1F2237', borderRadius: 20 }} >

                            <PieChart
                                data={rankSummaryArr}
                                width={Constants.SCREEN_WIDTH}
                                height={220}
                                chartConfig={{
                                    backgroundColor: null,
                                    backgroundGradientFrom: null,
                                    backgroundGradientTo: null,
                                    decimalPlaces: 0,
                                    color: (opacity = 1) => colors.yellow,
                                    labelColor: (opacity = 1) => colors.white,
                                    style: {
                                        borderRadius: 16
                                    },
                                    fillShadowGradient: colors.yellow,
                                    fillShadowGradientOpacity: 1,
                                    // barRadius: 10
                                }}
                                accessor={"population"}
                                backgroundColor={"transparent"}
                                paddingLeft={"-10"}
                                center={[10, 0]}
                                absolute
                            />
                        </View>
                    </View>
                }
            </ScrollView>

            {checkIsSponsorAdsEnabled('profileInsights') &&
                <SponsorBannerAds screenCode={'profileInsights'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('profileInsights')} />
            }

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const InsightStatsCurrentFormItem = (props) => {

    const item = props.item

    const getDate = () => {
        if (item.contest && item.contest.date) {
            return moment(item.contest.date).format('DD/MM/YY')
        }
    }

    return (
        <View style={{ flex: 1, height: 36, flexDirection: 'row' }} >

            <View style={{ height: 36, justifyContent: 'center' }} >
                <Text style={{ width: 70, textAlign: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >{getDate()}</Text>
                <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
            </View>

            {/* Game Type */}
            <View style={{ flex: 1, width: 70, height: 36, justifyContent: 'center' }} >
                <Text style={{ textAlign: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >{item.contest && item.contest.gameType && item.contest.gameType.name}</Text>
                <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
            </View>

            {/* Rank */}
            <View style={{ marginRight: 0, width: 56, height: 36, justifyContent: 'center' }} >
                <Text style={{ textAlign: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >{item.rank}</Text>
                <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
            </View>

            {/* Kill */}
            {props.showCurrentFormWithKill == true &&
                <View style={{ marginRight: 0, width: 56, height: 36, justifyContent: 'center' }} >
                    <Text style={{ textAlign: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >{item.kills}</Text>
                    <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
                </View>
            }

            {/* Won */}
            {/* <View style={{ marginRight: 0, width: 72, height: 36, justifyContent: 'center' }} >
                <Text style={{ textAlign: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >1</Text>
            </View> */}

            {/* <Text style={{ marginLeft: 58, flex: 1, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Game Type</Text> */}

            {/* <Image style={{ position: 'absolute', top: 0, left: 57, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
            <Image style={{ position: 'absolute', top: 0, left: 57, bottom: 0, width: 1, backgroundColor: '#393D54' }} />
            <Image style={{ position: 'absolute', top: 0, left: 57, bottom: 0, width: 1, backgroundColor: '#393D54' }} /> */}

            {!props.isLastIndex &&
                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#393D54' }} />
            }
        </View>
    )
}

export default InsightsStatsScreen;

// const RankData = [{
//     population: 10,
//     legendFontColor: "white",
//     legendFontSize: 15,
//     name: '(1 Rank)',
//     color: '#CD23EE'
// }, {
//     population: 40,
//     legendFontColor: "white",
//     legendFontSize: 15,
//     name: '(2-5 Rank)',
//     color: '#F25E81'
// }, {
//     population: 20,
//     legendFontColor: "white",
//     legendFontSize: 15,
//     name: '(6-10 Rank)',
//     color: '#FFFFFF'
// }, {
//     population: 5,
//     legendFontColor: "white",
//     legendFontSize: 15,
//     name: '(11-20 Rank)',
//     color: '#D88201'
// }, {
//     population: 25,
//     legendFontColor: "white",
//     legendFontSize: 15,
//     name: '(20+ Rank)',
//     color: '#098880'
// }]