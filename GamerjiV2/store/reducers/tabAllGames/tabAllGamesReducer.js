import produce from 'immer';
import * as actionTypes from '../../actions/tabAllGames/tabAllGamesAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        versionInfoListResponse: undefined,
        bannerListResponse: undefined,
        featuredTournamentListResponse: undefined,
        allGamesListResponse: undefined,
        getProfileResponse: undefined,
        dailyLoginRewardsStatusCheckResponse: undefined,
        featuredTournamentDetailResponse: undefined,
        screensListResponse: undefined,
        syncContactsResponse: undefined,
        infoPopupListResponse: undefined,
        html5GameSettingResponse: undefined,
        customerCareMessageListResponse: undefined,
    },
    error: false
};

{ /* Request - Banner List */ }
const versionInfoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.versionInfoListResponse = action.model;
    });
};

{ /* Request Failure - Banner List */ }
const versionInfoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.versionInfoListResponse = action.model;
    });
};

{ /* Request - Banner List */ }
const html5GameSettingList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.html5GameSettingResponse = action.model;
    });
};

{ /* Request Failure - Banner List */ }
const html5GameSettingListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.html5GameSettingResponse = action.model;
    });
};


{ /* Request - Banner List */ }
const bannerList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.bannerListResponse = action.model;
    });
};

{ /* Request Failure - Banner List */ }
const bannerListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.bannerListResponse = action.model;
    });
};

{ /* Request - Featured Tournament List */ }
const featuredTournamentList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.featuredTournamentListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Featured Tournament List */ }
const featuredTournamentListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.featuredTournamentListResponse = action.model;
    });
};

{ /* Request - All Games List */ }
const allGamesList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.allGamesListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - All Games List */ }
const allGamesListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.allGamesListResponse = action.model;
    });
};

{ /* Request - Get Profile */ }
const getProfileRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.getProfileResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Get Profile */ }
const getProfileRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.getProfileResponse = action.model;
    });
};

{ /* Request - Daily Login Rewards Status Check */ }
const dailyLoginRewardsStatusCheckRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.dailyLoginRewardsStatusCheckResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Daily Login Rewards Status Check */ }
const dailyLoginRewardsStatusCheckRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.dailyLoginRewardsStatusCheckResponse = action.model;
    });
};

const featuredTournamentDetail = (state, action) => {
    return produce(state, (draft) => {
        draft.model.featuredTournamentDetailResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const featuredTournamentDetailFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.featuredTournamentDetailResponse = action.model;
    });
};

{ /* Request - Screens List */ }
const screensList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.screensListResponse = action.model;
    });
};

{ /* Request Failure - Screens List */ }
const screensListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.screensListResponse = action.model;
    });
};

const syncContacts = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.screensListResponse = action.model;
    });
};

const syncContactsFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.infoPopupListResponse = action.model;
    });
};

const infoPupupList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.screensListResponse = action.model;
    });
};

const infoPupupListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.infoPopupListResponse = action.model;
    });
};

{ /* Reset State - All Games */ }
const resetAllGamesState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.bannerListResponse = undefined;
        draft.model.featuredTournamentListResponse = undefined;
        draft.model.allGamesListResponse = undefined;
        draft.model.getProfileResponse = undefined;
        draft.model.featuredTournamentDetailResponse = undefined;
        draft.model.screensListResponse = undefined;
        draft.model.infoPopupListResponse = undefined;
        draft.model.html5GameSettingResponse = undefined;
        draft.model.customerCareMessageListResponse = undefined;
    });
};

// ===============
const customerCareMessageList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.customerCareMessageListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const customerCareMessageListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.customerCareMessageListResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HTML5_GAME_SETTING_LIST:
            return html5GameSettingList(state, action);
        case actionTypes.HTML5_GAME_SETTING_LIST_FAILED:
            return html5GameSettingListFailed(state, action);
        case actionTypes.BANNER_LIST:
            return bannerList(state, action);
        case actionTypes.BANNER_LIST_FAILED:
            return bannerListFailed(state, action);
        case actionTypes.FEATURED_TOURNAMENT_LIST:
            return featuredTournamentList(state, action);
        case actionTypes.FEATURED_TOURNAMENT_LIST_FAILED:
            return featuredTournamentListFailed(state, action);
        case actionTypes.ALL_GAMES_LIST:
            return allGamesList(state, action);
        case actionTypes.ALL_GAMES_LIST_FAILED:
            return allGamesListFailed(state, action);
        case actionTypes.PROFILE_REQUEST:
            return getProfileRequest(state, action);
        case actionTypes.PROFILE_REQUEST_FAILED:
            return getProfileRequestFailed(state, action);
        case actionTypes.DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST:
            return dailyLoginRewardsStatusCheckRequest(state, action);
        case actionTypes.DAILY_LOGIN_REWARDS_STATUS_CHECK_REQUEST_FAILED:
            return dailyLoginRewardsStatusCheckRequestFailed(state, action);
        case actionTypes.FEATURED_TOURNAMENT_DETAIL:
            return featuredTournamentDetail(state, action);
        case actionTypes.FEATURED_TOURNAMENT_DETAIL_FAILED:
            return featuredTournamentDetailFailed(state, action);
        case actionTypes.SCREENS_LIST:
            return screensList(state, action);
        case actionTypes.SCREENS_LIST_FAILED:
            return screensListFailed(state, action);
        case actionTypes.SYNC_CONTACTS:
            return syncContacts(state, action);
        case actionTypes.SYNC_CONTACTS_FAILED:
            return syncContactsFailed(state, action);
        case actionTypes.INFO_POPUP_LIST:
            return infoPupupList(state, action);
        case actionTypes.INFO_POPUP_LIST_FAILED:
            return infoPupupListFailed(state, action);
        case actionTypes.CUSTOMER_CARE_MESSAGE_LIST:
            return customerCareMessageList(state, action);
        case actionTypes.CUSTOMER_CARE_MESSAGE_LIST_FAILED:
            return customerCareMessageListFailed(state, action);
        case actionTypes.RESET_ALL_GAMES_STATE:
            return resetAllGamesState(state, action);
        case actionTypes.VERSION_INFO_POPUP_LIST:
            return versionInfoList(state, action);
        case actionTypes.VERSION_INFO_POPUP_LIST_FAILED:
            return versionInfoListFailed(state, action);
        default:
            return state;
    }
};

export default reducer;