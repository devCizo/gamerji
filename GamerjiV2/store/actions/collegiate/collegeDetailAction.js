import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const collegeMembers = (viewModel) => {
    return {
        type: COLLEGE_MEMBERS,
        model: viewModel
    };
};

export const collegeMembersFailed = (viewModel) => {
    return {
        type: COLLEGE_MEMBERS_FAILED,
        model: viewModel
    };
};

export const requestCollegeMembers = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.COLLEGIATE.COLLEGE_MEMBERS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(collegeMembers(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(collegeMembersFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const COLLEGE_MEMBERS = 'COLLEGE_MEMBERS';
export const COLLEGE_MEMBERS_FAILED = 'COLLEGE_MEMBERS_FAILED';




export const resetCollegeDetailState = () => {
    return {
        type: RESET_COLLEGE_DETAIL_STATE,
    };
};
export const RESET_COLLEGE_DETAIL_STATE = 'RESET_COLLEGE_DETAIL_STATE';