import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Image, Text, TouchableOpacity, StyleSheet, Keyboard, TextInput } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { showSuccessToastMessage, showErrorToastMessage, addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const BottomSheetApplyPromoCode = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [promoCode, setPromoCode] = useState('');
    const [isLoading, setLoading] = useState(false);

    addLog('BottomSheetApplyPromoCode')

    // This function should check the validation first and then call the Apply Promo Code API
    const checkValidation = () => {
        Keyboard.dismiss();
        if (!promoCode.trim()) {
            showErrorToastMessage(Constants.error_enter_promo_code)
            return;
        }
        callToApplyPromoCodeAPI();
    };

    // This function should call the Apply Promo Code API
    const callToApplyPromoCodeAPI = (props) => {
        let payload = {
            code: promoCode,
        }
        setLoading(true)
        dispatch(actionCreators.requestApplyPromoCode(payload))
    }

    // This function sets the Legality Apply Promo Code Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.applyPromoCode.model }),
        shallowEqual
    );
    var { applyPromoCodeResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (applyPromoCodeResponse) {
            setLoading(false)
            if (applyPromoCodeResponse.success) {
                showSuccessToastMessage(Constants.success_promo_code_applied)
                setPromoCode('');
                props.openApplyPromoCodePopup();

                return () => {
                    dispatch(actionCreators.resetApplyPromoCodeState())
                }
            } else if (applyPromoCodeResponse.errors) {
                if (applyPromoCodeResponse.errors[0] && applyPromoCodeResponse.errors[0].msg) {
                    showErrorToastMessage(applyPromoCodeResponse.errors[0].msg)
                    setPromoCode('');
                    props.openApplyPromoCodePopup();

                    return () => {
                        dispatch(actionCreators.resetApplyPromoCodeState())
                    }
                }
            }
        }
        applyPromoCodeResponse = undefined
    }, [applyPromoCodeResponse])

    return (

        <>
            <View style={{ borderTopStartRadius: 50, borderTopEndRadius: 50, overflow: 'hidden' }}>

                {/* Top curved header */}
                <View style={{ height: 65, backgroundColor: '#FFC609', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>

                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, marginLeft: 50, marginRight: 50, textAlign: 'center' }}>{Constants.header_apply_promo_code}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() =>
                        props.openApplyPromoCodePopup()} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Input Container */}
                <View style={styles.bottomContainerView}>

                    <View style={styles.newUserNameContainer}>

                        {/* Text - Invite Code */}
                        <Text style={{ left: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}>{Constants.text_promo_code}</Text>

                        {/* Text Input - Invite Code */}
                        <TextInput
                            style={[styles.textInputs]}
                            placeholder={Constants.validation_promo_code}
                            onChangeText={text => setPromoCode(text)}
                            spellCheck={false}
                            autoCorrect={false}
                            autoCapitalize={'characters'}>
                        </TextInput>
                    </View>

                    {/* TouchableOpacity - Submit Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_50, { left: 20, right: 20 }]}
                        onPress={() => checkValidation()} activeOpacity={0.5}>

                        {/* Text - Button (Submit) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>

                {checkIsSponsorAdsEnabled('applyPromoCodePopup') &&
                    <SponsorBannerAds screenCode={'applyPromoCodePopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('applyPromoCodePopup')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}
        </>
    );
}

const styles = StyleSheet.create({

    closeButton: {
        height: 65,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 0
    },
    bottomContainerView: {
        backgroundColor: 'white',
        height: 250,
        flexDirection: 'column',
    },
    textInputs: {
        position: 'absolute',
        height: 46,
        borderRadius: 23,
        borderWidth: 1,
        borderColor: '#082240',
        top: 24,
        left: 20,
        right: 20,
        paddingHorizontal: 15,
        fontWeight: '600',
        fontFamily: fontFamilyStyleNew.regular
    },
    newUserNameContainer: {
        top: 36,
        height: 70,
    },
})

export default BottomSheetApplyPromoCode;