import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList, Linking } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import ViewAllFeaturedVideosItem from '../../componentsItem/ViewAllFeaturedVideosItem';
import VideoPopup from '../WorldOfEsports/videoPopup';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import { addLog } from '../../appUtils/commonUtlis';
import Modal from 'react-native-modal';


const ViewAllFeaturedVideos = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(true);
    const [featuredVideos, setFeaturedVideos] = useState(undefined);
    const [featuredVideoItem, setFeaturedVideoItem] = useState(undefined);
    const [totalCounts, setTotalCounts] = useState(undefined);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isOpenVideoPopup, setIsOpenVideoPopup] = useState(false);


    const openFeaturedVideoPopup = (item) => {
        setFeaturedVideoItem(item)
        setIsOpenVideoPopup(true)
        // addLog("isOpenFeaturedVideoPopup==>", isOpenFeaturedVideoPopup);
        // addLog("featuredVideoId==>", featuredVideoId);
    }
    // This Use-Effect function should call the All Videos API
    useEffect(() => {
        let payload = {
            skip: 0,
            limit: 10,
            sortBy: 'order',
            sort: 'asc',
            filter: {
                isLive: false
            }
        }
        setLoading(true)
        dispatch(actionCreators.requestViewAllFeaturedVideos(payload))

        return () => {
            dispatch(actionCreators.resetViewAllFeaturedVideosState())
        }
    }, []);

    // This function sets the View All Featured Videos API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.viewAllFeaturedVideos.model }),
        shallowEqual
    );
    var { viewAllFeaturedVideoListResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (viewAllFeaturedVideoListResponse && viewAllFeaturedVideoListResponse.list) {
            setLoading(false)
            setFeaturedVideos(viewAllFeaturedVideoListResponse.list)
            setTotalCounts(viewAllFeaturedVideoListResponse.count)

            if (viewAllFeaturedVideoListResponse.count == 0) {
                setTotalRecords(true)
            }
        }
        viewAllFeaturedVideoListResponse = undefined
    }, [viewAllFeaturedVideoListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Wrapper - Main View */}
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Esports Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.text_videos} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>


                    {/* Text - Total Videos */}
                    <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_16, marginStyles.topMargin_5]}>{totalCounts} {totalCounts > 1 ? Constants.text_videos : Constants.text_video}</Text>

                    {/* Wrapper - View All Video Lists */}
                    {!totalRecords ?
                        <FlatList style={marginStyles.topMargin_15}
                            data={featuredVideos}
                            renderItem={({ item, index }) => <ViewAllFeaturedVideosItem item={item} openFeaturedVideoPopup={openFeaturedVideoPopup} index={index} navigation={props.navigation} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false} />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }
                </View>
            </View>
            {
                isOpenVideoPopup &&
                <Modal
                    isVisible={isOpenVideoPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <VideoPopup featuredVideoItem={featuredVideoItem} setIsOpenVideoPopup={setIsOpenVideoPopup} />
                </Modal>
            }
            {isLoading && <CustomProgressbar />}
        </SafeAreaView >
    )
}
export default ViewAllFeaturedVideos;