import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Search Users List */ }
export const searchUserList = (viewModel) => {
    return {
        type: SEARCH_USER_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Search Users List */ }
export const searchUserListFailed = (viewModel) => {
    return {
        type: SEARCH_USER_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestSearchUsers = (payload) => {

    // addLog("searchUserList payload :::> ", payload);

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.SEARCH_USER.MODE, payload)
            .then((response) => {
                //       addLog("searchUserList:::> ", response.data);
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(searchUserList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(searchUserListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetSearchUserListState = () => {
    return {
        type: RESET_SEARCH_USER_LIST_STATE,
    };
};

export const SEARCH_USER_LIST = 'SEARCH_USER_LIST';
export const SEARCH_USER_LIST_FAILED = 'SEARCH_USER_LIST_FAILED';
export const RESET_SEARCH_USER_LIST_STATE = 'RESET_SEARCH_USER_LIST_STATE';