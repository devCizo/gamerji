import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const Leaderboard = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const [activeTab, setActiveTab] = useState('today');
    const [skipNo, setSkipNo] = useState(0);
    const [leaderboardList, setLeaderboardList] = useState([])
    const [isLoading, setLoading] = useState(true);
    const [leaderboardListFetchingStatus, setLeaderboardListFetchingStatus] = useState(false);
    const [isLoadMoreBtn, setLoadMoreBtn] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        let type = 'today'
        console.log("activeTab==>", activeTab);

        if (activeTab === 'today') {
            type = 'today'
        } else if (activeTab === 'byWeek') {
            type = 'byWeek'
        } else if (activeTab === 'monthly') {
            type = 'monthly'
        } else if (activeTab === 'allTime') {
            type = 'allTime'
        }
        setLeaderboardList([])

        getLeaderboardData(type)
        return () => {
            dispatch(actionCreators.resetLeaderboardState())
        }
    }, [refreshing]);

    //Fetch Leaderboard
    useEffect(() => {
        setLoading(true)
        getLeaderboardData('today', 0)
        return () => {
            dispatch(actionCreators.resetLeaderboardState())
        }
    }, [])

    let getLeaderboardData = (type, skip) => {
        setLoadMoreBtn(true)
        // if (!skip) {
        //     setLoading(true)
        // }
        // setSkipNo(skip + 10);
        setLeaderboardListFetchingStatus(true)
        // if (skip === 0) {
        //     var skipNew = 0;
        // } else {
        //     var skipNew = skipNo;
        // }
        var payload = { skip: skip, limit: 10 }
        // , sortBy: 'wallet.rank', sort: 'acs'
        if (!type) {
            if (activeTab == 'today') {
                payload['type'] = 'today'
            } else if (activeTab == 'byWeek') {
                payload['type'] = 'byWeek'
            } else if (activeTab == 'monthly') {
                payload['type'] = 'monthly'
            } else if (activeTab == 'allTime') {
                payload['type'] = 'allTime'
            }
        } else {
            payload['type'] = type
        }
        console.log("payload==>", payload);

        dispatch(actionCreators.requestLeaderboardList(payload))
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.leaderboard.model }),
        shallowEqual
    );
    var { leaderboardListResponse } = currentState

    // Leaderboard List Response
    useEffect(() => {
        if (leaderboardListResponse) {
            setLoading(false)
            setRefreshing(false)
            //setLeaderboardListFetchingStatus(false)
            if (leaderboardListResponse.list) {
                setLeaderboardList([...leaderboardList, ...leaderboardListResponse.list])
                setSkipNo(skipNo + 10);
                setLoadMoreBtn(false)

            }
        }

        leaderboardListResponse = undefined

    }, [leaderboardListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    //Tab select
    let selectTabButton = (value) => {
        setSkipNo(0)
        setActiveTab(value)
        setLeaderboardList([])
        getLeaderboardData(value, 0);

    }

    const FlatListHeaderComponent = () => {
        return (
            <>
                {/* Tabs View */}
                <View style={{ marginTop: 15, height: 34, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 17 }} >

                    {/* Today */}
                    <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'today' ? colors.black : 'transparent' }} onPress={() => selectTabButton('today')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Today</Text>
                        <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                    </TouchableOpacity>

                    {/* By Week */}
                    <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'byWeek' ? colors.black : 'transparent' }} onPress={() => selectTabButton('byWeek')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >By Week</Text>
                        <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                    </TouchableOpacity>

                    {/* Monthly */}
                    <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'monthly' ? colors.black : 'transparent' }} onPress={() => selectTabButton('monthly')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Monthly</Text>
                        <Image style={{ position: 'absolute', top: 7, right: 0, bottom: 7, width: 1, backgroundColor: '#D10000' }} />
                    </TouchableOpacity>

                    {/* All Time */}
                    <TouchableOpacity style={{ width: '25%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'allTime' ? colors.black : 'transparent' }} onPress={() => selectTabButton('allTime')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >All Time</Text>
                    </TouchableOpacity>
                </View>

                {/* First 3 rank data */}
                {/* <View style={{ flexDirection: 'row', height: 190 }} > */}

                {/* Left Badge */}
                {/* <View style={{ width: '33.33%' }} >

                        {leaderboardList && leaderboardList[1] &&
                            <View style={{ width: 80, flex: 1, alignItems: 'center' }}> */}

                {/* Background */}
                {/* <Image style={{ position: 'absolute', top: 60, width: 80, height: 80, aspectRatio: 1, borderRadius: 20, backgroundColor: '#43F2FF' }} /> */}

                {/* Badge */}
                {/* <Image style={{ marginTop: 35, height: 56, width: 56, aspectRatio: 1, resizeMode: 'contain' }} source={require('../../assets/images/badge_dummy2.png')} /> */}

                {/* Gamerji Name */}
                {/* <Text style={{ marginTop: 3, marginLeft: 0, marginRight: 0, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{leaderboardList[1].gamerjiName}</Text>
                                <Text style={{ marginLeft: 0, marginRight: 0, color: colors.black, fontSize: 8, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >xxxxx {leaderboardList[1].phone.substr(leaderboardList[1].phone.length - 5)}</Text> */}

                {/* Points */}
                {/* <View style={{ marginTop: 6, width: '100%', height: 22, backgroundColor: colors.black, alignItems: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ marginLeft: 0, marginRight: 0, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >P: {leaderboardList[1].points}</Text>
                                </View>
                            </View>
                        }
                    </View> */}

                {/* Center Badge */}
                {/* <View style={{ width: '33.33%' }} >

                        {leaderboardList && leaderboardList[0] &&
                            <View style={{ width: "100%", flex: 1, alignItems: 'center' }}> */}

                {/* Background */}
                {/* <Image style={{ position: 'absolute', top: 60, left: 0, right: 0, aspectRatio: 1, borderRadius: 50, backgroundColor: colors.red }} /> */}

                {/* Badge */}
                {/* <Image style={{ marginTop: 20, height: 78, width: 82, aspectRatio: 1, resizeMode: 'contain' }} source={require('../../assets/images/badge_dummy1.png')} /> */}

                {/* Gamerji Name */}
                {/* <Text style={{ marginTop: 8, marginLeft: 0, marginRight: 0, color: colors.white, fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{leaderboardList[0].gamerjiName}</Text>
                                <Text style={{ marginTop: 3, marginLeft: 0, marginRight: 0, color: colors.white, fontSize: 9, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >xxxxx {leaderboardList[0].phone.substr(leaderboardList[0].phone.length - 5)}</Text> */}

                {/* Points */}
                {/* <View style={{ marginTop: 12, width: '100%', height: 33, backgroundColor: colors.black, alignItems: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ marginLeft: 0, marginRight: 0, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >P: {leaderboardList[0].points}</Text>
                                </View>
                            </View>
                        }
                    </View> */}

                {/* Right Badge */}
                {/* <View style={{ width: '33.33%', flexDirection: 'row', justifyContent: 'flex-end' }} >

                        {leaderboardList && leaderboardList[2] &&
                            <View style={{ width: 80, alignItems: 'center' }}> */}

                {/* Background */}
                {/* <Image style={{ position: 'absolute', top: 60, width: 80, height: 80, aspectRatio: 1, borderRadius: 20, backgroundColor: '#09FFC4' }} /> */}

                {/* Badge */}
                {/* <Image style={{ marginTop: 35, height: 56, width: 56, aspectRatio: 1, resizeMode: 'contain' }} source={require('../../assets/images/badge_dummy2.png')} />
                                <Text style={{ marginTop: 3, marginLeft: 0, marginRight: 0, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{leaderboardList[2].gamerjiName}</Text> */}

                {/* Gamerji Name */}
                {/* <Text style={{ marginLeft: 0, marginRight: 0, color: colors.black, fontSize: 8, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >xxxxx {leaderboardList[2].phone.substr(leaderboardList[2].phone.length - 5)}</Text> */}

                {/* Points */}
                {/* <View style={{ marginTop: 6, width: '100%', height: 22, backgroundColor: colors.black, alignItems: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ marginLeft: 0, marginRight: 0, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >P: {leaderboardList[2].points}</Text>
                                </View>
                            </View>
                        }
                    </View>
                </View> */}

                {/* Top Container */}
                <View style={{ marginTop: 16, marginBottom: 15, height: 30, borderRadius: 25, backgroundColor: colors.yellow, flexDirection: 'row' }} >

                    {/* Team Name */}
                    <View style={{ flex: 1, justifyContent: 'center' }} >
                        <View style={{ width: 92, alignItems: 'center' }} >
                            <Text style={{ color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.extraBold }} >Team Name</Text>
                        </View>
                    </View>

                    {/* Points */}
                    <View style={{ width: 64, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.extraBold }} >Points</Text>
                        <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
                    </View>

                    {/* Level */}
                    <View style={{ right: 0, width: 64, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.extraBold }} >Level</Text>
                        <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
                    </View>

                    {/* Rank */}
                    <View style={{ right: 0, width: 64, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.extraBold }} >Rank</Text>
                        <Image style={{ position: 'absolute', top: 10, left: 0, bottom: 10, width: 1, backgroundColor: '#DEAB03' }} />
                    </View>
                </View>
            </>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >LEADERBOARD</Text>

                {/* Search */}
                {/* {<TouchableOpacity style={{ right: 0, width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={null}>
                    <Image style={{ width: 23.17, height: 24, }} source={require('../../assets/images/search_icon.png')} />
                </TouchableOpacity>} */}
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                <FlatList
                    ListHeaderComponent={
                        FlatListHeaderComponent()
                    }
                    style={{ marginLeft: 20, marginRight: 20, marginBottom: 20 }}
                    data={leaderboardList}
                    renderItem={({ item, index }) => <LeaderboardPlayerItem item={item} index={index} isLastIndex={leaderboardList.length - 1 == index} />}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.1}
                    onEndReached={() => {
                        addLog('onEndReached')
                        if (!leaderboardListFetchingStatus) {
                            getLeaderboardData(leaderboardList.count)
                        }
                    }}

                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                >
                </FlatList>
                <TouchableOpacity disabled={isLoadMoreBtn} style={{ width: '90%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', alignSelf: "center", backgroundColor: colors.black }} onPress={() => getLeaderboardData(activeTab, skipNo)}>
                    <Text style={{ color: 'white', fontSize: 18, marginBottom: 7, marginTop: 7, fontFamily: fontFamilyStyleNew.semiBold }} >Load More</Text>

                </TouchableOpacity>


                {checkIsSponsorAdsEnabled('leaderboard') &&
                    <SponsorBannerAds screenCode={'leaderboard'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('leaderboard')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

const LeaderboardPlayerItem = (props) => {

    const player = props.item
    let isFirstIndex = props.index == 0
    let isLastIndex = props.isLastIndex

    return (

        <View>
            {player &&
                <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center', }} activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: player.user._id })} >

                    {isFirstIndex &&
                        <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, borderWidth: 1, borderColor: '#DFE4E9', borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomColor: 'white' }} />
                    }

                    {isLastIndex &&
                        <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, borderWidth: 1, borderColor: '#DFE4E9', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderTopWidth: 0 }} />
                    }

                    {!isFirstIndex && !isLastIndex &&
                        <>
                            <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, width: 1, backgroundColor: '#DFE4E9' }} />
                            <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 1, backgroundColor: '#DFE4E9' }} />
                        </>
                    }

                    {/* Badge */}
                    {player.level && player.level.featuredImage ?
                        <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                        :
                        <Text></Text>

                    }

                    <View style={{ marginLeft: 8, flex: 1 }}>

                        {/* Username */}
                        <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} numberOfLines={1} >{player.user && player.user.gamerjiName}</Text>
                        {/* Mobile number */}
                        <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.black }} >xxxxx {player.user && player.user.phone.substr(player.user.phone.length - 5)}</Text>
                    </View>

                    {/* Points */}
                    <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.points}</Text>

                    {/* Level */}
                    <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.level && player.level.num}</Text>

                    {/* Rank */}
                    <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{player.rank}</Text>

                    {/* Separator line */}
                    {!isLastIndex &&
                        <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} />
                    }

                </TouchableOpacity>
            }
        </View>
    );
}

export default Leaderboard;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 50,
        flex: 1,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
})