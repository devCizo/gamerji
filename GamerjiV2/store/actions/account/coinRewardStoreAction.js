import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const coinPackList = (viewModel) => {
    return {
        type: COIN_PACK_LIST,
        model: viewModel
    };
};

export const coinPackListFailed = (viewModel) => {
    return {
        type: COIN_PACK_LIST_FAILED,
        model: viewModel
    };
};

export const requestCoinPackList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.COIN_STORE.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(coinPackList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(coinPackListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

// =======

export const avatarCategoryList = (viewModel) => {
    return {
        type: AVATAR_CATEGORY_LIST,
        model: viewModel
    };
};

export const avatarCategoryListFailed = (viewModel) => {
    return {
        type: AVATAR_CATEGORY_LIST_FAILED,
        model: viewModel
    };
};

export const requestAvatarCategoryList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.AVATAR_CATEGORY.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(avatarCategoryList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(avatarCategoryListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


// =======

export const buyAvatar = (viewModel) => {
    return {
        type: BUY_AVATAR,
        model: viewModel
    };
};

export const buyAvatarFailed = (viewModel) => {
    return {
        type: BUY_AVATAR_FAILED,
        model: viewModel
    };
};

export const requestBuyAvatar = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("Payload:::> ", payload);
        addLog("API:::> ", WebFields.AVATAR_CATEGORY.BUY_AVATAR);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.AVATAR_CATEGORY.BUY_AVATAR, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(buyAvatar(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(buyAvatarFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

// ======

export const resetCoinRewardStoreState = () => {
    return {
        type: RESET_COIN_REWARD_STORE_STATE,
    };
};

export const COIN_PACK_LIST = 'COIN_PACK_LIST';
export const COIN_PACK_LIST_FAILED = 'COIN_PACK_LIST_FAILED';
export const AVATAR_CATEGORY_LIST = 'AVATAR_CATEGORY_LIST';
export const AVATAR_CATEGORY_LIST_FAILED = 'AVATAR_CATEGORY_LIST_FAILED';
export const BUY_AVATAR = 'BUY_AVATAR';
export const BUY_AVATAR_FAILED = 'BUY_AVATAR_FAILED';
export const RESET_COIN_REWARD_STORE_STATE = 'RESET_COIN_REWARD_STORE_STATE';