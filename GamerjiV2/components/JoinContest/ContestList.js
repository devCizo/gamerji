
import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, fontFamilyStyles } from '../../appUtils/commonStyles';
import ContestCommonDetail from './ContestCommonDetail';
import Modal from 'react-native-modal';
import JoinContestWalletValidation from './JoinContestWalletValidation';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import ContestFilter from './ContestFilter'
import JoinViaInviteCode from './joinViaInviteCode'
import CustomProgressbar from '../../appUtils/customProgressBar';
import FlatListLoadingIndicator from '../../appUtils/flatListLoadingIndicator'
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';

import LottieView from 'lottie-react-native';

const ContestList = (props, navigation) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const gameType = props.route.params.gameType;
    const [contestList, setContestList] = useState([]);
    const [selectedContest, setSelectedContest] = useState(undefined);
    const [isVissibleWalletPopup, setJoinContestWalletPopup] = useState(false);
    const [isVissibleFilterPopup, setFilterPopup] = useState(false);
    const [walletUsageLimit, setWalletUsageLimit] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [contestListFetchingStatus, setContestListFetchingStatus] = useState(false);
    const [announcement, setAnnouncement] = useState(undefined)
    const [refreshing, setRefreshing] = useState(false);

    var selectedContestTemp = undefined

    useEffect(() => {

        setLoading(true)
        getContestList()

        return () => {
            dispatch(actionCreators.resetContestListState())
            dispatch(actionCreators.resetJoinViaInviteCodeState())
        }
    }, [])

    //Get Contest List
    const getContestList = (skip) => {
        let payload = {
            skip: skip,
            limit: 10,
            sort: "date",
            sortBy: "desc",
            filter: {
                "gameType": gameType._id
            }
        }
        setContestListFetchingStatus(true)
        dispatch(actionCreators.requestContestList(payload))
    }

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setContestList([])
        getContestList()

    }, [refreshing]);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.contestList.model }),
        shallowEqual
    );
    var { contestListResponse, walletUsageLimitResponse } = currentState

    useEffect(() => {
        if (contestListResponse) {
            setLoading(false)
            setRefreshing(false)
            if (contestListResponse.list) {
                setContestListFetchingStatus(contestListResponse.list.length == 0)
                var tempArr = contestListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setContestList([...contestList, ...tempArr])
            }

            if (contestListResponse.announcement) {
                setAnnouncement(contestListResponse.announcement)
            }
        }
        contestListResponse = undefined
    }, [contestListResponse]);


    useEffect(() => {
        if (walletUsageLimitResponse) {
            setLoading(false)
            if (walletUsageLimitResponse.success == true && walletUsageLimitResponse.item) {
                setWalletUsageLimit(walletUsageLimitResponse.item)
                setJoinContestWalletPopup(true)
            }
        }
        walletUsageLimitResponse = undefined
    }, [walletUsageLimitResponse]);


    const joinContestWalletValidation = (contest) => {

        // RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })
        // RootNavigation.navigate(Constants.nav_dob_state_validation, { game: contest.game })
        // return

        setSelectedContest(contest)
        selectedContestTemp = contest

        if (contest.isJoined) {
            RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })
        } else if (!checkForDobStateGameName(contest)) {
            RootNavigation.navigate(Constants.nav_dob_state_validation, { game: contest.game, dobStateUpdateSuccess: dobStateUpdateSuccess })
        } else {
            getWalletUsageLimit(contest)
        }
    }

    const checkForDobStateGameName = (contest) => {

        var isValidToJoin = true

        if (global.profile) {
            if (global.profile.gamerjiName) {
                if (global.profile.gamerjiName == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.dateOfBirth) {
                if (global.profile.dateOfBirth == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.address) {
                if (global.profile.address.state == null) {
                    isValidToJoin = false
                }
            }

            if (global.profile.gameNames && contest.game) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == contest.game._id)
                if (index < 0) {
                    isValidToJoin = false
                }
            }
        }
        return isValidToJoin
    }

    const dobStateUpdateSuccess = () => {
        addLog('selectedContest', selectedContestTemp)
        getWalletUsageLimit(selectedContestTemp)
    }

    const getWalletUsageLimit = (contest) => {
        let payload = {
            contest: contest._id,
            type: 'contest'
        }
        setLoading(true)
        dispatch(actionCreators.requestWalletUsageLimit(payload))
    }

    const closeWalletValidationPopup = () => {
        setJoinContestWalletPopup(false)
    }

    const setFilterPopopVisibility = (visible) => {
        setFilterPopup(visible)
    }

    const moveToSquadRegistration = () => {
        RootNavigation.navigate(Constants.nav_squad_registration, { contestTournament: selectedContest, isContest: true, setContestAsJoined: setContestAsJoined })
    }

    const moveToAddBalance = (amountToAdd, isCoin) => {
        setWalletUsageLimit(undefined)
        addLog("isCoin===>", isCoin);
        if (isCoin) {
            global.addBalanceScreenType = Constants.contest_coin_payment_type
            global.addBalanceCurrencyType = Constants.coin_payment
        } else {
            global.addBalanceScreenType = Constants.contest_money_payment_type
            global.addBalanceCurrencyType = Constants.money_payment
        }
        global.paymentSuccessCompletion = reOpenWalletPaymentPopup
        if (isCoin) {
            RootNavigation.navigate(Constants.nav_coin_reward_store, { amountToAdd: amountToAdd })
        } else {
            RootNavigation.navigate(Constants.nav_coin_reward_store, { amountToAdd: amountToAdd })
        }

    }

    const setContestAsJoined = () => {
        addLog("selectedContest==>", selectedContest);
        if (selectedContest) {
            RootNavigation.navigate(Constants.nav_contest_detail, { contestId: selectedContest._id })
            let index = contestList.findIndex(obj => obj._id == selectedContest._id)
            if (index > -1) {
                var tempArr = [...contestList]
                tempArr[index].isJoined = true
                setContestList(tempArr)
            }
        }
    }

    // Re-Open Wallet Payment After Remaining Payment Done
    const reOpenWalletPaymentPopup = () => {
        if (selectedContest) {
            getWalletUsageLimit(selectedContest)
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Game Name */}
                <Text style={[fontFamilyStyles.extraBold, styles.gameTypeName]} numberOfLines={1} >{gameType.name}</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {/* Contest List View */}
                {contestList.length > 0 ?
                    <View style={styles.contestListView}>
                        <FlatList

                            ListHeaderComponent={
                                <>
                                    {/* Join Via Invite Code */}
                                    <View style={{ marginBottom: 10 }} >
                                        <JoinViaInviteCode setLoading={setLoading} />
                                    </View>

                                    {/* Announcement View */}
                                    {announcement &&
                                        <View style={styles.announcementView}>
                                            <Image source={require('../../assets/images/promotion_icon.png')} style={styles.announcementImage} />
                                            <View style={styles.announemetNotesView}>
                                                <Text style={styles.announemetNotesTitle} >Announcement</Text>
                                                <Text style={styles.announemetNotesDescription} >{announcement}</Text>
                                            </View>
                                        </View>
                                    }
                                </>
                            }

                            data={contestList}
                            keyExtractor={(item, index) => index.toString()}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!contestListFetchingStatus) {
                                    getContestList(contestList.length)
                                }
                            }}
                            renderItem={({ item }) => <ContestListItem item={item} joinContestWalletValidation={joinContestWalletValidation} />}
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            }
                        />
                    </View>
                    : <NoRecordsFound></NoRecordsFound>}
                {/* </ScrollView> */}
                {/* Filter Button */}
                {/* <TouchableOpacity style={styles.filterButton} onPress={() => setFilterPopopVisibility(true)} activeOpacity={0.5} >
                    <Text style={{ marginLeft: 22, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >FILTERS</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/up_arrow.png')}></Image>
                </TouchableOpacity> */}

                {checkIsSponsorAdsEnabled('contestsList') &&
                    <SponsorBannerAds screenCode={'contestsList'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('contestsList')} />
                }
            </View>

            {walletUsageLimit &&
                <Modal isVisible={isVissibleWalletPopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <JoinContestWalletValidation contest={selectedContest} walletUsageLimit={walletUsageLimit} closeWalletValidationPopup={closeWalletValidationPopup} setContestAsJoined={setContestAsJoined} moveToSquadRegistration={moveToSquadRegistration} moveToAddBalance={moveToAddBalance} />
                </Modal>
            }

            <Modal isVisible={isVissibleFilterPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <ContestFilter contestList={contestList} setFilterPopopVisibility={setFilterPopopVisibility} />
            </Modal>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const ContestListItem = (props) => {

    const contest = props.item
    const id = contest._id

    return (

        // Main Container
        <View style={styles.contestListItemContainer}>

            <ContestCommonDetail item={contest} />

            {/* Bottom View */}
            <View style={styles.bottomContainer} >

                {/* Hosted By View */}
                <TouchableOpacity style={{ width: '50%', backgroundColor: colors.yellow, justifyContent: 'center' }} activeOpacity={1} >
                    <Text style={{ marginLeft: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }}>Hosted By</Text>

                    <View style={{ marginTop: 2, marginLeft: 10, marginRight: 10, flexDirection: 'row', alignItems: 'center', overflow: 'hidden' }} >
                        {/* <CustomMarquee style={{ flex: 1, marginRight: 2, color: colors.black, fontSize: 10, fontWeight: 'bold' }} value={contest.host && contest.host.name} /> */}
                        {/* <Image source={require('../../assets/images/star_rating.png')} style={{ height: 12, width: 12, tintColor: colors.black }} /> */}
                        <CustomMarquee style={{ color: colors.black, fontSize: 10, fontWeight: 'bold' }} value={(contest.host && contest.host.name) + ' (' + (contest.host && contest.host.rate.toFixed(2)) + '★)'} />
                    </View>

                    <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 0, bottom: 0, right: 0, width: 1 }} />
                </TouchableOpacity>


                {/* Join Now View */}
                {contest.totalPlayers - contest.totalJoinedPlayers > 0 ?
                    <TouchableOpacity style={{ width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.yellow, flexDirection: 'row' }} onPress={() => props.joinContestWalletValidation(contest)} activeOpacity={1} >
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: colors.black }} >{!contest.isJoined ? 'JOIN NOW' : 'JOINED'}</Text>
                        {contest.isJoined &&
                            <View style={{ marginLeft: 4, height: 16, width: 16 }}>
                                <LottieView
                                    source={require('../../assets/jsonFiles/success.json')}
                                    autoPlay
                                    loop
                                />
                            </View>
                        }
                    </TouchableOpacity>
                    : <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.yellow, flexDirection: 'row' }}   >
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: colors.black }} >FULL</Text>

                    </View>}

            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
    announcementView: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        backgroundColor: colors.red,
        borderRadius: 32.5,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-around',
        minHeight: 65
    },
    announcementImage: {
        width: 40,
        height: 37,
        marginLeft: 20,
        marginTop: 14,
        alignSelf: 'stretch',
    },
    announemetNotesView: {
        alignSelf: 'stretch',
        marginLeft: 12,
        marginTop: 8,
        marginBottom: 8,
        marginRight: 8,
        flex: 1,
    },
    announemetNotesTitle: {
        fontSize: 14,
        color: 'white',
        fontFamily: fontFamilyStyleNew.bold
    },
    announemetNotesDescription: {
        marginTop: 5,
        fontSize: 12,
        color: 'white',
        marginBottom: 0,
        fontFamily: fontFamilyStyleNew.semiBold
    },
    contestListView: {
        flex: 1,
        alignSelf: 'stretch',
        overflow: 'hidden',
        marginTop: 10
    },
    contestListItemContainer: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 15,
    },
    bottomContainer: {
        position: 'absolute',
        height: 44,
        left: 8,
        right: 8,
        bottom: 0,
        borderColor: colors.yellow,
        // borderWidth: 1,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent: 'space-between',
        flexDirection: 'row',
        overflow: 'hidden',
    },
    filterButton: {
        height: 46,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default ContestList;