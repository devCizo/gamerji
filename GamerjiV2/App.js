import React, { useEffect, useState } from 'react'
import { ThemeProvider } from './appUtils/themeContext'
import { RootNavigation } from './appUtils/rootNavigation'

import authenticationReducer from './store/reducers/authentication/authenticationReducer';
import gameTypesReducer from './store/reducers/joinContest/gameTypesReducer';
import contestListReducer from './store/reducers/joinContest/ContestListReducer';
import joinContestReducer from './store/reducers/joinContest/JoinContestReducer';
import contestDetailReducer from './store/reducers/myContests/ContestDetailReducer';
import squadRegistrationReducer from './store/reducers/joinContest/SquadRegistrationReducer';
import commonApiRequestReducer from './store/reducers/commonApiRequest';
import dobStateValidationReducer from './store/reducers/joinContest/dobStateValidationReducer';
import changeUserNameReducer from './store/reducers/joinContest/changeUserNameReducer';
import tournamentListReducer from './store/reducers/joinTournament/tournamentListReducer';
import joinTournamentWalletValidationReducer from './store/reducers/joinTournament/JoinTournamentWalletValidationReducer';
import tournamentDetailReducer from './store/reducers/myContests/tournamentDetailReducer';
import myContestsReducer from './store/reducers/myContests/myContestsReducer';
import joinViaInviteCodeReducer from './store/reducers/joinContest/joinViaInviteCodeReducer';
import leaderboardReducer from './store/reducers/more/leaderboardReducer';
import moreReducer from './store/reducers/more/moreReducer';
import raiseComplaintReducer from './store/reducers/customerCare/raiseComplaintReducer';
import customerCareReducer from './store/reducers/customerCare/customerCareReducer';
import reportIssueReducer from './store/reducers/myContests/reportIssueReducer';
import howToPlayReducer from './store/reducers/more/howToPlayReducer';
import profileReducer from './store/reducers/profile/profileReducer';
import collegiateReducer from './store/reducers/collegiate/collegiateReducer';
import worldOfEsportsReducer from './store/reducers/worldOfEsports/worldOfEsportsReducer';
import viewAllFeaturedVideosReducer from './store/reducers/worldOfEsports/viewAllFeaturedVideosReducer';
import viewAllTopProfilesReducer from './store/reducers/worldOfEsports/viewAllTopProfilesReducer';
import viewAllEsportsNewsReducer from './store/reducers/worldOfEsports/viewAllEsportsNewsReducer';
import singleTournamentToJoinReducer from './store/reducers/joinTournament/singleTournamentToJoinReducer';
import singleContestToJoinReducer from './store/reducers/joinContest/singleContestToJoinReducer';
import legalityReducer from './store/reducers/more/legalityReducer';
import applyPromoCodeReducer from './store/reducers/more/applyPromoCodeReducer';
import gamerjiPointsReducer from './store/reducers/more/gamerjiPointsReducer';
import videosReducer from './store/reducers/more/videosReducer';
import viewAllVideosReducer from './store/reducers/more/viewAllVideosReducer';
import streamOnGamerjiReducer from './store/reducers/more/streamOnGamerjiReducer';
import addVideosReducer from './store/reducers/more/addVideosReducer';
import updateEmailReducer from './store/reducers/account/updateEmailReducer';
import profileInfoReducer from './store/reducers/authentication/profileInfoReducer'
import myRecentTransactionsReducer from './store/reducers/account/myRecentTransactionsReducer';
import collegeDetailReducer from './store/reducers/collegiate/collegeDetailReducer';
import tabAllGamesReducer from './store/reducers/tabAllGames/tabAllGamesReducer';
import dailyLoginRewardsReducer from './store/reducers/tabAllGames/dailyLoginRewardsReducer';
import bottomSheetDailyLoginRewardsReducer from './store/reducers/tabAllGames/bottomSheetDailyLoginRewardsReducer';
import viewAllMedalsReducer from './store/reducers/profile/viewAllMedalsReducer';
import accountReducer from './store/reducers/account/accountReducer';
import verifyEmailReducer from './store/reducers/account/verifyEmailReducer';
import linkBankAndUPIAccountReducer from './store/reducers/account/linkBankAndUPIAccountReducer';
import withdrawReducer from './store/reducers/account/withdrawReducer';
import paymentGatewayReducer from './store/reducers/payments/paymentGatewayReducer';
import editProfileReducer from './store/reducers/profile/editProfileReducer';
import insightsStatsReducer from './store/reducers/profile/insightsStatsReducer';
import otherUserProfileReducer from './store/reducers/profile/otherUserProfileReducer';
import levelReducer from './store/reducers/profile/levelReducer';

import coinStoreReducer from './store/reducers/account/coinStoreReducer';
import updateDeviceTokenReducer from './store/reducers/authentication/updateDeviceTokenReducer';
import dynamicLinkReducer from './store/reducers/dynamicLink/dynamicLinkReducer';
import rewardStoreReducer from './store/reducers/account/rewardStoreReducer';
import rewardStoreRedeemReducer from './store/reducers/account/rewardStoreRedeemReducer';
import myRewardsReducer from './store/reducers/account/myRewardsReducer';
import ticketDetailReducer from './store/reducers/customerCare/ticketDetailReducer';
import updateFavoriteGamesPopupReducer from './store/reducers/tabAllGames/updateFavoriteGamesPopupReducer';
import sponsorAdsReducer from './store/reducers/sponsorAds/sponsorAdsReducer';
import sponsorAdsLogReducer from './store/reducers/sponsorAds/sponsorAdsLogReducer';
import howToJoinReducer from './store/reducers/joinContest/howToJoinReducer';
import friendsReducer from './store/reducers/friends/friendsReducer';
import coinRewardStoreReduce from './store/reducers/account/coinRewardStoreReduce';
import htmlGameCategoriesReducer from './store/reducers/htmlGame/htmlGameCategoriesReducer';
import htmlGameReducer from './store/reducers/htmlGame/htmlGameReducer';
import searchUsersReducer from './store/reducers/search/searchUsersReducer';
import paymentOptionsReducer from './store/reducers/payments/paymentOptionsReducer';
import earnCoinsReducer from './store/reducers/account/earnCoinsReducer';
import claimCoinsReducer from './store/reducers/account/claimCoinsReducer';

import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Platform, StatusBar, Text, View, Image, TouchableOpacity, ScrollView, Linking } from 'react-native';
import KeyboardManager from 'react-native-keyboard-manager';
import Toast from 'react-native-toast-message';
import { Constants } from './appUtils/constants';
import { retrieveDataFromLocalStorage } from './appUtils/sessionManager';
import { adjustEnvironment, AppConstants } from './appUtils/appConstants';
import { fontFamilyStyleNew } from './appUtils/commonStyles'

import { addLog } from './appUtils/commonUtlis';
import SplashScreen from 'react-native-splash-screen';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import firebase from '@react-native-firebase/app';
import DynamicLinkExtractor from './components/dynamicLink/dynamicLinkExtractor';
import FlashMessage from "react-native-flash-message";
import colors from './assets/colors/colors';
import crashlytics from '@react-native-firebase/crashlytics';
//import { TouchableOpacity } from 'react-native-gesture-handler';
import { Adjust, AdjustConfig } from 'react-native-adjust';

import { Settings } from 'react-native-fbsdk-next';



import OneSignal from 'react-native-onesignal';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const App = () => {


    const [shortCodeForContestTournament, setShortCodeForContestTournament] = useState(undefined)

    useEffect(() => {

        SplashScreen.hide()
        // firebase.initializeApp()
        configureFirebase()
        checkPushNotificationPermission()
        dynamicLinks().onLink(handleDynamicLink);
        setupAdjust()
        Settings.setAppID('595073537650722');
        // Ask for consent first if necessary
        // Possibly only do this for iOS if no need to handle a GDPR-type flow
        Settings.initializeSDK();
        crashlytics().log('App mounted.');
        //OneSignal Init Code
        OneSignal.setLogLevel(6, 0);
        OneSignal.setAppId(AppConstants.ONE_SIGNAL_APP_ID);
        //END OneSignal Init Code

        //Prompt for push on iOS
        OneSignal.promptForPushNotificationsWithUserResponse(response => {
            console.log("Prompt response:", response);
        });

        //Method for handling notifications received while app in foreground
        OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
            console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
            let notification = notificationReceivedEvent.getNotification();
            console.log("notification: ", notification);
            const data = notification.additionalData
            console.log("additionalData: ", data);
            // Complete with null means don't show a notification.
            notificationReceivedEvent.complete(notification);
        });

        //Method for handling notifications opened
        OneSignal.setNotificationOpenedHandler(notification => {
            console.log("OneSignal: notification opened:", notification);
        });
    }, [])

    const appReducer = combineReducers({
        authentication: authenticationReducer,
        account: accountReducer,
        gameTypes: gameTypesReducer,
        contestList: contestListReducer,
        joinContest: joinContestReducer,
        contestDetail: contestDetailReducer,
        squadRegistration: squadRegistrationReducer,
        data: commonApiRequestReducer,
        dobStateValidation: dobStateValidationReducer,
        changeUserName: changeUserNameReducer,
        tournamentList: tournamentListReducer,
        joinTournamentWalletValidation: joinTournamentWalletValidationReducer,
        tournamentDetail: tournamentDetailReducer,
        myContests: myContestsReducer,
        joinViaInviteCode: joinViaInviteCodeReducer,
        leaderboard: leaderboardReducer,
        more: moreReducer,
        raiseComplaint: raiseComplaintReducer,
        customerCare: customerCareReducer,
        reportIssue: reportIssueReducer,
        howToPlay: howToPlayReducer,
        profile: profileReducer,
        collegiate: collegiateReducer,
        worldOfEsports: worldOfEsportsReducer,
        viewAllFeaturedVideos: viewAllFeaturedVideosReducer,
        viewAllTopProfiles: viewAllTopProfilesReducer,
        viewAllEsportsNews: viewAllEsportsNewsReducer,
        singleTournamentToJoin: singleTournamentToJoinReducer,
        singleContestToJoin: singleContestToJoinReducer,
        legality: legalityReducer,
        applyPromoCode: applyPromoCodeReducer,
        gamerjiPoints: gamerjiPointsReducer,
        videos: videosReducer,
        viewAllVideos: viewAllVideosReducer,
        streamOnGamerji: streamOnGamerjiReducer,
        addVideos: addVideosReducer,
        updateEmail: updateEmailReducer,
        profileInfo: profileInfoReducer,
        myRecentTransactions: myRecentTransactionsReducer,
        collegeDetail: collegeDetailReducer,
        tabAllGames: tabAllGamesReducer,
        dailyLoginRewards: dailyLoginRewardsReducer,
        bottomSheetDailyLoginRewards: bottomSheetDailyLoginRewardsReducer,
        viewAllMedals: viewAllMedalsReducer,
        verifyEmail: verifyEmailReducer,
        linkBankAndUPIAccount: linkBankAndUPIAccountReducer,
        withdraw: withdrawReducer,
        paymentGateway: paymentGatewayReducer,
        editProfile: editProfileReducer,
        insightsStats: insightsStatsReducer,
        otherUserProfile: otherUserProfileReducer,
        coinStore: coinStoreReducer,
        updateDeviceToken: updateDeviceTokenReducer,
        dynamicLink: dynamicLinkReducer,
        rewardStore: rewardStoreReducer,
        rewardStoreRedeem: rewardStoreRedeemReducer,
        myRewards: myRewardsReducer,
        ticketDetail: ticketDetailReducer,
        updateFavoriteGamesPopup: updateFavoriteGamesPopupReducer,
        sponsorAds: sponsorAdsReducer,
        sponsorAdsLog: sponsorAdsLogReducer,
        howToJoin: howToJoinReducer,
        friendsState: friendsReducer,
        coinRewardStore: coinRewardStoreReduce,
        html5Categorie: htmlGameCategoriesReducer,
        html5Games: htmlGameReducer,
        searchUsers: searchUsersReducer,
        levelList: levelReducer,
        paymentOptions: paymentOptionsReducer,
        earnCoins: earnCoinsReducer,
        claimCoin: claimCoinsReducer

    });

    const rootReducer = (state, action) => {

        if (action.type === 'LOGOUT') {
            state = undefined
        }
        return appReducer(state, action)
    }

    // Be sure to ONLY add this middleware in development!
    const middleware = process.env.NODE_ENV !== 'production' ? [require('redux-immutable-state-invariant').default(), thunk] : [thunk];

    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));

    if (Platform.OS === 'ios') {
        KeyboardManager.setEnable(true);
        KeyboardManager.setEnableDebugging(false);
        KeyboardManager.setKeyboardDistanceFromTextField(10);
        KeyboardManager.setEnableAutoToolbar(true);
        KeyboardManager.setToolbarDoneBarButtonItemText("Done");
        KeyboardManager.setToolbarManageBehaviourBy("subviews");
        KeyboardManager.setToolbarPreviousNextButtonEnable(false);
        KeyboardManager.setToolbarTintColor('#0000FF');
        KeyboardManager.setToolbarBarTintColor('#FFFFFF');
        KeyboardManager.setShouldShowToolbarPlaceholder(true);
        KeyboardManager.setOverrideKeyboardAppearance(false);
        KeyboardManager.setKeyboardAppearance("default");
        KeyboardManager.setShouldResignOnTouchOutside(true);
        KeyboardManager.setShouldPlayInputClicks(true);
        KeyboardManager.resignFirstResponder();
        KeyboardManager.isKeyboardShowing()
            .then((isShowing) => {
            });
    }

    const configureFirebase = () => {

        const iosConfig = {
            apiKey: 'AIzaSyBw-xOChQWFv8oS8HF56HffCyc9FaW0I9w',
            databaseURL: 'https://fantasyji-esports.firebaseio.com',
            storageBucket: 'fantasyji-esports.appspot.com',
            messagingSenderId: '454874517092',
            projectId: 'fantasyji-esports',
            persistence: true,
            authDomain: "fantasyji-esports.firebaseapp.com",
            appId: "1:454874517092:ios:18d91115f592bfa854d971",
        };

        const androidConfig = {
            //clientId: 'x',
            appId: "1:454874517092:ios:18d91115f592bfa854d971",
            apiKey: 'AIzaSyBw-xOChQWFv8oS8HF56HffCyc9FaW0I9w',
            databaseURL: 'https://fantasyji-esports.firebaseio.com',
            storageBucket: 'fantasyji-esports.appspot.com',
            messagingSenderId: '454874517092',
            projectId: 'fantasyji-esports',
            persistence: true,
        };

        if (Platform.OS === 'ios') {
            addLog('iosConfig')
            firebase.initializeApp(
                iosConfig,
            );
        }
        if (Platform.OS === 'android') {
            addLog('androidConfig')
            firebase.initializeApp(
                androidConfig,
            );
        }
    }

    const checkPushNotificationPermission = async () => {

        const authStatus = await messaging().requestPermission();
        const enabled =
            authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
            authStatus === messaging.AuthorizationStatus.PROVISIONAL;

        if (enabled) {
            getFirebaseToken();
        }
    }

    const getFirebaseToken = async () => {
        const fcmToken = await messaging().getToken();
        if (fcmToken) {
            addLog('fcmToken =>', fcmToken);
            global.fcmToken = fcmToken
        }
    }

    const handleDynamicLink = link => {
        addLog(link.url)
        if (link.url) {
            if (link.url.includes('contestTournamentDetail')) {
                let seperate1 = link.url.split('shortCode=')
                if (seperate1.length > 1) {
                    setShortCodeForContestTournament(seperate1[1])
                }
            }
        }
    }

    const setupAdjust = () => {
        const adjustConfig = new AdjustConfig(AppConstants.ADJUST_APP_TOKEN, adjustEnvironment())
        Adjust.create(adjustConfig)
    }

    return (
        <Provider store={store}>

            <RootNavigation />
            {Platform.OS === 'ios' && <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#070B28'} translucent={true} />}
            <>
                <Toast ref={(ref) => Toast.setRef(ref)} />
            </>

            {shortCodeForContestTournament &&
                <DynamicLinkExtractor shortCode={shortCodeForContestTournament} setShortCodeForContestTournament={setShortCodeForContestTournament} />
            }

            <FlashMessage position="top" style={{ backgroundColor: "#E29618" }} />

        </Provider>
    );
};

export default App;