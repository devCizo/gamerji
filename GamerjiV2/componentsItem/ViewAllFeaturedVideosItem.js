import React, { useState, useCallback, useRef } from "react";
import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import { addLog } from '../appUtils/commonUtlis';
import YoutubePlayer from "react-native-youtube-iframe";
import getVideoId from 'get-video-id';
import CustomMarquee from '../appUtils/customMarquee';

const ViewAllFeaturedVideosItem = (props) => {

    // Variable Declarations
    const width = (Dimensions.get('window').width - 45) / 2;
    const [playing, setPlaying] = useState(false);

    // This function should set the youtube player video play state
    const onStateChange = useCallback((state) => {

        if (state === "ended") {
            setPlaying(false);
            addLog("Video has finished playing!");
        }
    }, []);

    // const togglePlaying = useCallback(() => {
    //     setPlaying((prev) => !prev);
    // }, []);

    return (
        // Wrapper - Main View
        <TouchableOpacity onPress={() => props.openFeaturedVideoPopup(props.item)}>
            <View style={[heightStyles.height_140, { width: width }, borderStyles.borderRadius_10, marginStyles.topMargin_5, marginStyles.bottomMargin_5, paddingStyles.padding_2, colorStyles.silverBackgroundColor, marginStyles.rightMargin_5]}>

                {/* Wrapper - Video */}
                <View style={[containerStyles.container, flexDirectionStyles.column, borderStyles.borderRadius_10, colorStyles.whiteBackgroundColor, alignmentStyles.oveflow]}>

                    {/* Wrapper - Video Main View */}
                    <View style={[{ width: width }, flexDirectionStyles.column, borderStyles.borderRadius_10, marginStyles.rightMargin_5, colorStyles.whiteBackgroundColor, alignmentStyles.oveflow]}>

                        {/* Wrapper - Video */}
                        <View style={{ height: 100 }}>
                            <YoutubePlayer
                                height={200}
                                play={playing}
                                videoId={getVideoId(props.item.url).id}
                                onChangeState={onStateChange} />
                        </View>

                        {/* Wrapper - Video Title */}
                        <View style={[heightStyles.height_37, flexDirectionStyles.column, paddingStyles.leftPadding_10, paddingStyles.rightPadding_10, justifyContentStyles.center]}>

                            {/* Text - Video Title */}
                            <CustomMarquee value={props.item.name} style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, alignmentStyles.alignItemsCenter, justifyContentStyles.center, marginStyles.leftMargin_10, marginStyles.rightMargin_10]} />

                            {/* Text - Subscribers */}

                        </View>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default ViewAllFeaturedVideosItem;