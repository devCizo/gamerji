import axios from '../../../webServices/axios-api';
import { saveDataToLocalStorage, clearDataFromLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { Constants } from '../../../appUtils/constants';
import * as actionCreators from '../../actions';
{ /* Action Types */ }
export const REQUEST_ACCOUNT = 'REQUEST_ACCOUNT';
export const REQUEST_ACCOUNT_FAILED = 'REQUEST_ACCOUNT_FAILED';
export const RESET_ACCOUNT_STATE = 'RESET_ACCOUNT_STATE';
import { CommonActions } from '@react-navigation/native';

{ /* Request - Account */ }
export const accountRequest = (viewModel) => {
    return {
        type: REQUEST_ACCOUNT,
        model: viewModel
    };
};

{ /* Request Failure - Account */ }
export const accountRequestFailed = (viewModel) => {
    return {
        type: REQUEST_ACCOUNT_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Account */ }
export const resetAccountState = () => {
    return {
        type: RESET_ACCOUNT_STATE,
    };
};

{ /* Request - Account */ }
export const requestAccount = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ACCOUNT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.profile = response.data.item
                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(accountRequest(response.data.item));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                addLog("error.response.data.errors[0]:::> ", error.response.data.errors[0].code);

                dispatch(accountRequestFailed(error.response.data));


            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};