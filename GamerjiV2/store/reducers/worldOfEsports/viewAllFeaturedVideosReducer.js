import produce from 'immer';
import * as actionTypes from '../../actions/worldOfEsports/viewAllFeaturedVideosAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        viewAllFeaturedVideoListResponse: undefined,
    },
    error: false
};

{ /* Request - View All Featured Video List */ }
const viewAllFeaturedVideoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.viewAllFeaturedVideoListResponse = action.model;
    });
};

{ /* Request Failure - View All Top Profile List */ }
const viewAllFeaturedVideoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.viewAllFeaturedVideoListResponse = action.model;
    });
};

{ /* Reset State - View All Featured Videos List */ }
const resetViewAllFeaturedVideosState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.viewAllFeaturedVideoListResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIEW_ALL_FEATURED_VIDEO_LIST:
            return viewAllFeaturedVideoList(state, action);
        case actionTypes.VIEW_ALL_FEATURED_VIDEO_LIST_FAILED:
            return viewAllFeaturedVideoListFailed(state, action);
        case actionTypes.RESET_VIEW_ALL_FEATURED_VIDEO_STATE:
            return resetViewAllFeaturedVideosState(state);
        default:
            return state;
    }
};

export default reducer;