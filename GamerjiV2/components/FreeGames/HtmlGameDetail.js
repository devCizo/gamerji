import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView, Image, SafeAreaView, Dimensions, Linking } from 'react-native'
import { scrollViewStyles, justifyContentStyles, containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, fontFamilyStyleNew, positionStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import { WebView } from 'react-native-webview';

import colors from '../../assets/colors/colors';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';

import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, checkIsSponsorAdsEnabled, convertNumberToMillions } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import ChangeUserName from '../JoinContest/ChangeUserName'
import Modal from 'react-native-modal';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import CustomMarquee from '../../appUtils/customMarquee';
import LinearGradient from 'react-native-linear-gradient';
import Orientation from 'react-native-orientation'


const HtmlGameDetail = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    var [html5Games, setHtml5Gameshtml5Games] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [isVissibleChangeUserNamePopup, setChangeUserNamePopup] = useState(false);
    const htmlGame = props.route.params.htmlGame;
    const width = (Dimensions.get('window').width - 40);
    var [html5GameLog, setHtml5GameLog] = useState(undefined);

    // This Use-Effect function should call the Game Types API
    useEffect(() => {

        const initial = Orientation.getInitialOrientation();
        Orientation.unlockAllOrientations();
        Orientation.lockToPortrait();


        addLog("htmlGame==>", htmlGame);
        if (htmlGame && htmlGame._id) {
            let payload = {
                htmlGame: htmlGame._id,
                html5Category: htmlGame.html5Category._id,
                status: "start"

            }
            // setLoading(true)
            addLog("requestHtml5GamesUpdateView==>", payload);
            dispatch(actionCreators.requestHtml5GamesUpdateView(payload))
        }

        // return () => {
        //     dispatch(actionCreators.resetHtml5GamesState())
        // }
    }, []);

    //  API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.html5Games.model }),
        shallowEqual
    );
    var { html5GameDetailsResponse } = currentState

    // This Use-Effect function should set the Game Types Data
    useEffect(() => {
        addLog("useEffect == html5GameDetailsResponse==> ", html5GameDetailsResponse)
        if (html5GameDetailsResponse) {

            setHtml5GameLog(html5GameDetailsResponse)
        }

        html5GameDetailsResponse = undefined
    }, [html5GameDetailsResponse]);

    let clickToEndUserSession = () => {
        addLog('html5GameLog==>', html5GameLog);
        let payload = {
            htmlGame: htmlGame._id,
            logId: html5GameLog.item._id,
            status: "end"

        }
        // setLoading(true)
        addLog("payload==>", payload);
        dispatch(actionCreators.requestHtml5GamesUpdateView(payload))
        props.navigation.goBack()
    }
    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let openChangeUserNamePopup = () => {
        addLog('openChangeUserNamePopup')
        setChangeUserNamePopup(true)
    }

    let closeUserNamePopup = () => {
        setChangeUserNamePopup(false)
    }

    let moveToTournamentList = () => {
        //  RootNavigation.navigate(Constants.nav_tournament_list, { game });
        // RootNavigation.navigate(Constants.nav_chart)
    }

    let openApp = () => {
        Linking.canOpenURL('market://details?id=com.pubg.imobile').then(supported => {
            if (supported) {
                console.log('accepted')
                return Linking.openURL('market://details?id=com.pubg.imobile')
            } else {
                console.log('an error occured')
            }
        }).catch(
            console.log('an error occured')
        )
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} onPress={() => clickToEndUserSession()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Game Name Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_05]} numberOfLines={1} ellipsizeMode='tail'> {htmlGame && htmlGame.name} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {htmlGame && htmlGame.url ?
                        <WebView source={{ uri: htmlGame.url }} />
                        : "URL not Found"}



                    <View style={{ width: '100%' }}>



                        {checkIsSponsorAdsEnabled('gameType') &&
                            <SponsorBannerAds screenCode={'gameType'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('gameType')} />
                        }
                    </View>
                </View>
            </View>



            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}



export default HtmlGameDetail;