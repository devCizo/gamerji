import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Image, ScrollView, SafeAreaView, TextInput, Keyboard, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import WebFields from '../../webServices/webFields.json';
import { showSuccessToastMessage, showErrorToastMessage, addLog, getUpiPattern, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const BankTransfer = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const withdrawalType = props.route.params && props.route.params.withdrawalType;
    const [accountNumber, setAccountNumber] = useState('');
    const [repeatAccountNumber, setRepeatAccountNumber] = useState('');
    const [bankName, setBankName] = useState('');
    const [ifscCode, setIfscCode] = useState('');
    const [accountHolderName, setAccountHolderName] = useState('');
    const [upiId, setUpiId] = useState('');
    const [repeatUpiId, setRepeatUpiId] = useState('');
    const [isLoading, setLoading] = useState(false);

    // This function should check the validation first and then call the Bank Details / UPI Details API
    const checkValidation = () => {
        Keyboard.dismiss();
        if (!accountNumber.trim() && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_enter_account_number)
            return;
        } else if (!repeatAccountNumber.trim() && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_enter_repeat_account_number)
            return;
        } else if (!upiId.trim() && withdrawalType === Constants.text_upi_payment) {
            showErrorToastMessage(Constants.error_enter_upi_id)
            return;
        } else if (!repeatUpiId.trim() && withdrawalType === Constants.text_upi_payment) {
            showErrorToastMessage(Constants.error_enter_repeat_upi_id)
            return;
        } else if (!bankName.trim()) {
            showErrorToastMessage(Constants.error_enter_bank_name)
            return;
        } else if (!ifscCode.trim() && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_enter_ifsc_code)
            return;
        } else if (!accountHolderName.trim()) {
            showErrorToastMessage(Constants.error_enter_account_holder_name)
            retur
        } else if (accountNumber.length < 7 && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_enter_valid_account_number)
            return; 67
        } else if (repeatAccountNumber.length < 7 && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_enter_valid_repeat_account_number)
            return;
        } else if (accountNumber !== repeatAccountNumber && withdrawalType === Constants.text_bank_account) {
            showErrorToastMessage(Constants.error_account_number_and_repeat_account_number_not_matched)
            return;
        } else if (!getUpiPattern().test(upiId) && withdrawalType === Constants.text_upi_payment) {
            showErrorToastMessage(Constants.error_enter_correct_upi_id);
            return;
        } else if (!getUpiPattern().test(repeatUpiId) && withdrawalType === Constants.text_upi_payment) {
            showErrorToastMessage(Constants.error_enter_correct_repeat_upi_id);
            return;
        } else if (upiId !== repeatUpiId && withdrawalType === Constants.text_upi_payment) {
            showErrorToastMessage(Constants.error_upi_id_and_repeat_upi_id_not_matched)
            return;
        }
        callToBankAndUPITransferAPI();
    };

    // This function should call the Bank & UPI Transfer Account Request API
    const callToBankAndUPITransferAPI = () => {
        var payload = {}

        if (withdrawalType === Constants.text_bank_account) {
            payload.accountNumber = accountNumber
            payload.ifsc = ifscCode
        } else {
            payload.upiID = upiId
        }
        payload.accountName = accountHolderName
        payload.bankName = bankName
        addLog("payload==>", payload);
        addLog("withdrawalType==>", withdrawalType);
        setLoading(true)
        dispatch(actionCreators.requestLinkBankAndUPIAccount(payload, withdrawalType))
    }

    // This function sets the Link Bank And UPI Account API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.linkBankAndUPIAccount.model }),
        shallowEqual
    );
    var { linkBankAndUPIAccountResponse } = currentState

    // This function should return the Link Bank And UPI Account API Response
    useEffect(() => {
        if (linkBankAndUPIAccountResponse) {
            setLoading(false)
            props.navigation.navigate(Constants.nav_withdrawal, {
                withdrawalType: withdrawalType,
                accountHolderName: accountHolderName,
                bankName: bankName,
                accountNumber: accountNumber,
                upiId: upiId
            });

            return () => {
                dispatch(actionCreators.resetLinkBankAndUPIAccountState())
            }
        }
        linkBankAndUPIAccountResponse = undefined
    }, [linkBankAndUPIAccountResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Bank Transfer Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {withdrawalType === Constants.text_bank_account ? Constants.header_bank_transfer : Constants.header_upi_transfer} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Text - Bank Details */}
                        <Text style={[alignmentStyles.alignSelfCenter, fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_18]}> {withdrawalType === Constants.text_bank_account ? Constants.text_bank_details : Constants.text_upi_details} </Text>

                        {/* Wrapper - Bank Details */}
                        <View style={[flexDirectionStyles.column]}>

                            {/* Wrapper - Bank Details */}
                            <View style={[positionStyles.relative]}>

                                {withdrawalType === Constants.text_bank_account ? (
                                    <View>
                                        {/* Text - Account Number */}
                                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_25]}> {Constants.text_account_number} </Text>

                                        {/* Text Input - Account Number */}
                                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                            keyboardType="numeric"
                                            placeholder={Constants.validation_account_number}
                                            autoFocus={true}
                                            maxLength={14}
                                            onChangeText={text => setAccountNumber(text)} />

                                        {/* Text - Repeat Account Number */}
                                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_repeat_account_number} </Text>

                                        {/* Text Input - Repeat Account Number */}
                                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                            keyboardType="numeric"
                                            maxLength={14}
                                            placeholder={Constants.validation_repeat_account_number}
                                            onChangeText={text => setRepeatAccountNumber(text)} />

                                    </View>
                                ) :
                                    <View>
                                        {/* Text - UPI ID */}
                                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_25]}> {Constants.text_upi_id} </Text>

                                        {/* Text Input - UPI ID */}
                                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                            placeholder={Constants.validation_upi_id}
                                            autoFocus={true}
                                            onChangeText={text => setUpiId(text)} />

                                        {/* Text - Repeat UPI ID */}
                                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_repeat_upi_id} </Text>

                                        {/* Text Input - Repeat UPI ID */}
                                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                            placeholder={Constants.validation_repeat_upi_id}
                                            onChangeText={text => setRepeatUpiId(text)} />
                                    </View>}

                                {/* Text - Bank Name */}
                                <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_bank_name} </Text>

                                {/* Text Input - Bank Name */}
                                <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                    placeholder={Constants.validation_bank_name}
                                    onChangeText={text => setBankName(text)} />

                                {withdrawalType === Constants.text_bank_account ? (
                                    <View>
                                        {/* Text - IFSC Code */}
                                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_ifsc_code} </Text>

                                        {/* Text Input - IFSC Code */}
                                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                            placeholder={Constants.validation_ifsc_code}
                                            autoCapitalize='characters'
                                            onChangeText={text => setIfscCode(text)} />
                                    </View>
                                ) : null}

                                {/* Text - Account Holder's Name */}
                                <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_account_holders_name} </Text>

                                {/* Text Input - Account Holder's Name */}
                                <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                    placeholder={Constants.validation_account_holders_name}
                                    onChangeText={text => setAccountHolderName(text)} />
                            </View>

                            {/* Text - Minimum ₹ 200 and Maximum ₹ 2,00,000 allowed per day */}
                            <Text style={[colorStyles.greyColor, fontSizeStyles.fontSize_14, alignmentStyles.alignTextCenter, marginStyles.topMargin_22, marginStyles.leftMargin_10, marginStyles.rightMargin_10]}> {Constants.text_withdraw_warning} </Text>

                            {/* Wrapper - Note Details */}
                            <View style={[colorStyles.yellowBackgroundColor, borderStyles.borderRadius_10, alignmentStyles.alignSelfStretch, marginStyles.topMargin_22, paddingStyles.leftPadding_15, paddingStyles.rightPadding_15, paddingStyles.topPadding_20, paddingStyles.bottomPadding_20]}>

                                {/* Text - Note Header */}
                                <Text style={[fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_18]}>{Constants.text_note}</Text>

                                {/* Text - Note */}
                                <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_12, marginStyles.topMargin_16]}>{Constants.text_withdraw_notes}</Text>
                            </View>
                        </View>
                    </ScrollView>

                    <View style={{ width: '100%' }}>

                        <View style={{ padding: 20 }}>

                            {/* TouchableOpacity - Link Bank Account / Link UPI Button Click Event */}
                            <TouchableOpacity style={[heightStyles.height_46, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter]}
                                onPress={() => checkValidation()} activeOpacity={0.5}>

                                {/* Text - Button (Proceed) */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{withdrawalType === Constants.text_bank_account ? Constants.action_link_bank_account : Constants.action_link_upi}</Text>

                                {/* Image - Right Arrow */}
                                <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                            </TouchableOpacity>
                        </View>

                        {checkIsSponsorAdsEnabled('linkBankOrUpiAccount') &&
                            <SponsorBannerAds screenCode={'linkBankOrUpiAccount'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('linkBankOrUpiAccount')} />
                        }
                    </View>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default BankTransfer;