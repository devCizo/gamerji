import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinTournament/singleTournamentToJoinAction';

const initialState = {
    model: {
        walletUsageLimitForSingleTournamentResponse: undefined
    },
    error: false
};

const walletUsageLimitForSingleTournament = (state, action) => {
    return produce(state, (draft) => {
        draft.model.walletUsageLimitForSingleTournamentResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const walletUsageLimitForSingleTournamentFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.walletUsageLimitForSingleTournamentResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT:
            return walletUsageLimitForSingleTournament(state, action);
        case actionTypes.WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT_FAILED:
            return walletUsageLimitForSingleTournamentFailed(state, action);
        case actionTypes.RESET_SINGLE_TOURNAMENT:
            return resetSingleTournamentState(state);
        default:
            return state;
    }
};

export default reducer;

const resetSingleTournamentState = (state) => {
    return produce(state, (draft) => {
        addLog('cleared')
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.walletUsageLimitForSingleTournamentResponse = undefined
    })
}