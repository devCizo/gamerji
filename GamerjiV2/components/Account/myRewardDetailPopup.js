import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import { checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import Clipboard from '@react-native-clipboard/clipboard';
import moment from 'moment';
const MyRewardDetailPopup = (props) => {
    const copyRewardCodeToClipboard = (rewardCode) => {
        if (rewardCode) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(rewardCode)
        }
    }
    const myReward = props.myReward

    return (
        <>
            <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, alignItems: 'center' }} >

                {/* Close Button */}
                <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1 }} onPress={() => props.setOpenMyRewardDetailPopup(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>

                <View>

                    {/* Name */}
                    <View style={{ marginTop: 24, marginLeft: 20, marginRight: 20, alignItems: 'center' }}>
                        <CustomMarquee style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} value={myReward.name} />
                    </View>
                    {/* Voucher Code */}
                    <View style={{ marginTop: 24, marginLeft: 20, flexDirection: 'row' }}>

                        {/* Voucher Icon */}
                        <View style={{ height: 46, width: 46, borderRadius: 23, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 27, height: 27, resizeMode: 'contain' }} source={require('../../assets/images/voucher-code.png')} />
                        </View>

                        {/* Coin Title */}
                        <View style={{ marginLeft: 10 }}>
                            <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }}>{myReward.redeemVoucherCode}

                            </Text>


                            <Text style={{ marginTop: 2, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}>Voucher Code</Text>

                            {/* Coin Amount */}
                        </View>
                        <View style={{ marginLeft: 25, height: 46, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{ height: 46, width: 75, flexDirection: 'row', borderRadius: 23, alignItems: 'center', alignSelf: "center", justifyContent: "center", borderWidth: 1, borderColor: colors.white, backgroundColor: colors.black, }} onPress={() => copyRewardCodeToClipboard(myReward.redeemVoucherCode)} activeOpacity={0.5} >

                                <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Copy</Text>

                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* Description */}
                    <Text style={{ marginTop: 24, marginLeft: 20, marginRight: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }}>{myReward.description} </Text>

                    {/* Bottom Content */}
                    <View style={{ marginTop: 24, marginLeft: 10, marginRight: 10, marginBottom: 30, flexDirection: 'row' }}>

                        {/* Coin */}
                        <View style={{ width: "50%", justifyContent: 'center', alignItems: 'center' }}>

                            {/* Coin Icon */}
                            <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center' }}>
                                <Image style={{ width: 16, height: 18, resizeMode: 'contain' }} source={require('../../assets/images/total-coin-icon.png')} />
                            </View>

                            {/* Coin Title */}
                            <Text style={{ marginTop: 8, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }}>Coin</Text>

                            {/* Coin Amount */}
                            <Text style={{ marginTop: 2, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }}>{myReward.coinAmount}</Text>

                            {/* Separator Line */}
                            <Image style={{ position: 'absolute', top: 0, bottom: 0, right: 0, width: 1, backgroundColor: '#DBDBDB' }} />
                        </View>

                        {/* Date & Time */}
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                            {/* Coin Icon */}
                            <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#FFEEB4', justifyContent: 'center', alignItems: 'center' }}>
                                <Image style={{ width: 18, height: 18, resizeMode: 'contain' }} source={require('../../assets/images/calendar-icon.png')} />
                            </View>

                            {/* Date Time Title */}
                            <Text style={{ marginTop: 8, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }}>Date & Time</Text>

                            {/* Date Time */}
                            <Text style={{ marginTop: 2, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }}>{moment(myReward.redeemtime).format('DD/MM/yyyy hh:mm A')}</Text>

                            {/* Separator Line */}
                            <Image style={{ position: 'absolute', top: 0, bottom: 0, right: 0, width: 1, backgroundColor: '#DBDBDB' }} />
                        </View>


                    </View>

                </View>
            </View>
        </>
    )
}

export default MyRewardDetailPopup