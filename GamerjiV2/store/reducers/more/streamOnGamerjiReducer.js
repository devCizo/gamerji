import produce from 'immer';
import * as actionTypes from '../../actions/more/streamOnGamerjiAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        gamesSearchResponse: undefined,
        addStreamerResponse: undefined,
    },
    error: false
};

{ /* Request - Games Search List */ }
const gamesSearchList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.gamesSearchResponse = action.model;
    });
};

{ /* Request Failure - Games Search List */ }
const gamesSearchListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gamesSearchResponse = action.model;
    });
};

{ /* Request - Add Streamer */ }
const addStreamerRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.addStreamerResponse = action.model;
    });
};

{ /* Request Failure - Add Streamer */ }
const addStreamerRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.addStreamerResponse = action.model;
    });
};

{ /* Reset State - Stream On Gamerji */ }
const resetStreamOnGamerjiState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.gamesSearchResponse = undefined;
        draft.model.addStreamerResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GAMES_SEARCH_LIST:
            return gamesSearchList(state, action);
        case actionTypes.GAMES_SEARCH_LIST_FAILED:
            return gamesSearchListFailed(state, action);
        case actionTypes.REQUEST_ADD_STREAMER:
            return addStreamerRequest(state, action);
        case actionTypes.REQUEST_ADD_STREAMER_FAILED:
            return addStreamerRequestFailed(state, action);
        case actionTypes.RESET_STREAM_ON_GAMERJI_STATE:
            return resetStreamOnGamerjiState(state);
        default:
            return state;
    }
};

export default reducer;