import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, SafeAreaView, Image, Dimensions, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import { containerStyles, heightStyles, positionStyles, alignmentStyles, colorStyles, marginStyles, paddingStyles, borderStyles, widthStyles, otherStyles, fontFamilyStyleNew, shadowStyle } from '../../appUtils/commonStyles';
import colors from '../../assets/colors/colors';
import ModalDropdown from 'react-native-modal-dropdown';
import { addLog, showErrorToastMessage, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomProgressbar from '../../appUtils/customProgressBar';
import { AppConstants } from '../../appUtils/appConstants';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

export default function CollegeLeagues({ props }) {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [collegeList, setCollegeList] = useState([]);
    const [selectedCollege, setSelectedCollege] = useState('Select college')
    const [selectedCollegeId, setSelectedCollegeId] = useState(undefined)
    const [selectedCollegeDetail, setSelectedCollegeDetail] = useState(undefined)

    //Initial
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestCollegeList())
        return () => {
            dispatch(actionCreators.resetCollegiateState())
        }
    }, []);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.collegiate.model }),
        shallowEqual
    );
    var { collegeListResponse, submitCollegeResponse } = currentState

    //College List Response
    useEffect(() => {
        if (collegeListResponse) {
            setLoading(false)
            if (collegeListResponse.list) {
                setCollegeList(collegeListResponse.list)

                if (global.profile.college) {
                    let index = collegeListResponse.list.findIndex(obj => obj._id == global.profile.college)
                    if (index > -1) {
                        setSelectedCollegeDetail(collegeListResponse.list[index])
                    }
                }
            }
        }
        collegeListResponse = undefined
    }, [collegeListResponse]);

    //Submit College
    submitCollege = () => {
        if (selectedCollegeId) {
            setLoading(true)
            dispatch(actionCreators.requestSubmitCollege({ 'college': selectedCollegeId }))
        } else {
            showErrorToastMessage('Please select college')
        }
    }

    //Submit College Response
    useEffect(() => {
        if (submitCollegeResponse) {

            setLoading(false)
            if (submitCollegeResponse.item) {
                var profile = { ...submitCollegeResponse.item }
                global.profile = profile
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(profile))

                let index = collegeList.findIndex(obj => obj._id == selectedCollegeId)
                if (index > -1) {
                    setSelectedCollegeDetail(collegeList[index])

                    RootNavigation.navigate(Constants.nav_college_detail, { collegeDetail: collegeList[index], collegeList: collegeList })
                }
            }
        }
        submitCollegeResponse = undefined
    }, [submitCollegeResponse]);

    //View College Detail
    viewCollegeDetails = () => {
        if (selectedCollegeDetail) {
            RootNavigation.navigate(Constants.nav_college_detail, { collegeDetail: selectedCollegeDetail, collegeList: collegeList })
        }
    }

    const collegeSelectedDropDown = (idx, value) => {
        addLog(`idx=${idx}, value='${value}'`);
        setSelectedCollegeId(collegeList[idx]._id)
        setSelectedCollege(value)
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Gamerji Logo */}
                <View style={[marginStyles.topMargin_20, otherStyles.zIndex_1]}>

                    {/* Image - GamerJi Logo */}
                    <Image style={[heightStyles.height_60, widthStyles.width_75, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/logo.png')} />
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderRadius_40, colorStyles.whiteBackgroundColor, marginStyles.bottomMargin_10, marginStyles.topMargin_23_minus, otherStyles.zIndex_0]}>

                    {/* Scroll View */}
                    <ScrollView style={[marginStyles.bottomMargin_10]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {!selectedCollegeDetail ?

                            //Select College
                            <View style={{ marginTop: 30, marginLeft: 20, marginRight: 20 }} >

                                <View style={{ marginTop: 18 }}>

                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, }}>Select your college</Text>


                                    <ModalDropdown
                                        style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}
                                        textStyle={{ width: Dimensions.get('window').width - 40, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: 'black' }}
                                        dropdownStyle={{ marginTop: 12, marginLeft: 0, width: Dimensions.get('window').width - 40, borderWidth: 1, borderColor: '#D5D7E3' }}
                                        dropdownTextStyle={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: '#70717A' }}
                                        defaultValue={selectedCollege}
                                        options={collegeList.map(function (item) { return item.name })}
                                        defaultIndex={-1}
                                        animated={true}
                                        isFullWidth={true}
                                        onSelect={(idx, value) => collegeSelectedDropDown(idx, value)}
                                    />

                                    <Image style={{ position: 'absolute', top: 40, right: 18, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />

                                </View>

                                {/* Submit Button */}
                                <TouchableOpacity style={styles.submitButton} onPress={() => submitCollege()/*navigation.navigate(Constants.nav_squad_registration)*/} >
                                    <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >SUBMIT</Text>
                                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                                </TouchableOpacity>

                            </View>

                            :

                            //College Detail

                            <View style={{}} >

                                {/* College Name */}
                                <Text style={{ marginTop: 60, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 24, textAlign: 'center' }} >{selectedCollegeDetail.name}</Text>

                                <View style={{ marginTop: 18, marginLeft: 20, width: Constants.SCREEN_WIDTH - 40, flexDirection: 'row', justifyContent: 'space-between' }} >

                                    {/* Rank */}
                                    <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                        <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                            <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollegeDetail.rank}</Text>

                                            <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Rank</Text>
                                        </View>

                                        <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                                    </View>

                                    {/* Members */}
                                    <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                        <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: '#43F2FF', borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                            <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollegeDetail.members}</Text>

                                            <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Members</Text>
                                        </View>

                                        <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/members-collegiate.png')} />
                                    </View>


                                </View>

                                <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 60, height: 46, flexDirection: 'row', backgroundColor: colors.red, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => viewCollegeDetails()} >
                                    <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >VIEW DETAILS</Text>
                                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                                </TouchableOpacity>
                            </View>
                        }
                    </ScrollView>

                    {checkIsSponsorAdsEnabled('collegeLeagues') &&
                        <View style={{ marginLeft: 20, marginRight: 20, marginBottom: 30 }}>
                            <SponsorBannerAds screenCode={'collegeLeagues'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('collegeLeagues')} />
                        </View>
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    submitButton: {
        marginTop: 40,
        height: 46,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
})