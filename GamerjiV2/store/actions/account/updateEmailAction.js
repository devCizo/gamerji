import axios from '../../../webServices/axios-api';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_UPDATE_EMAIL = 'REQUEST_UPDATE_EMAIL';
export const REQUEST_UPDATE_EMAIL_FAILED = 'REQUEST_UPDATE_EMAIL_FAILED';
export const RESET_UPDATE_EMAIL_STATE = 'RESET_UPDATE_EMAIL_STATE';

{ /* Request - Update Email */ }
export const updateEmailRequest = (viewModel) => {
    return {
        type: REQUEST_UPDATE_EMAIL,
        model: viewModel
    };
};

{ /* Request Failure - Update Email */ }
export const updateEmailRequestFailed = (viewModel) => {
    return {
        type: REQUEST_UPDATE_EMAIL_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Update Email */ }
export const resetUpdateEmailState = () => {
    return {
        type: RESET_UPDATE_EMAIL_STATE,
    };
};

{ /* Request - Update Email */ }
export const requestUpdateEmail = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.profile = response.data.item
                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(updateEmailRequest(response.data.item));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(updateEmailRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};