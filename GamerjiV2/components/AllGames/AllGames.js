import React, { useCallback, useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, ScrollView, Text, StatusBar, BackHandler, Alert, FlatList, Image, TouchableOpacity, ActivityIndicator, SafeAreaView, Dimensions, Linking, PermissionsAndroid, Platform, RefreshControl } from 'react-native'
import { containerStyles, widthStyles, heightStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import WalletImage from '../../assets/images/ic_wallet.svg';
import * as actionCreators from '../../store/actions/index';
import colors from '../../assets/colors/colors';
import BannersItem from '../../componentsItem/BannersItem';
import FeaturedTournamentsItem from '../../componentsItem/FeaturedTournamentsItem';
import HomeFreeGamesItem from '../../componentsItem/HomeFreeGamesItem';
import AllGamesItem from '../../componentsItem/AllGamesItem';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { ProgressBar } from 'react-native-paper';
import Modal from 'react-native-modal';
import BottomSheetDailyLoginRewards from './BottomSheetDailyLoginRewards';
import * as generalSetting from '../../webServices/generalSetting'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { retrieveDataFromLocalStorage, saveDataToLocalStorage } from '../../appUtils/sessionManager';
import { AppConstants } from '../../appUtils/appConstants';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import SingleTournamentToJoin from '../joinTournament/singleTournamentToJoin';
import { BannerView } from 'react-native-fbads';
import BottomSheetApplyPromoCode from '../More/BottomSheetApplyPromoCode';
import Contacts from 'react-native-contacts';
import Carousel from 'react-native-anchor-carousel';
import { Button } from 'native-base';

import VersionInfo from 'react-native-version-info';

import RNFS from 'react-native-fs'
import RNApkInstallerN from 'react-native-apk-installer-n';

import { clearDataFromLocalStorage } from '../../appUtils/sessionManager';

const AllGames = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [featuredTournaments, setFeaturedTournaments] = useState(undefined);
    const [allGames, setAllGames] = useState(undefined);
    const [isLoading, setLoading] = useState(false);
    const [dailyLoginRewardsStatus, setDailyLoginRewardsStatus] = useState(undefined);
    const [day, setDay] = useState(undefined);
    const [isOpenDailyRewardsPopup, setIsOpenDailyRewardsPopup] = useState(false);
    const [bannersFlatListIndex, setBannersFlatListIndex] = useState(0)
    const [isOpenSingleTournamentPopup, setOpenSingleTournamentPopup] = useState(false)
    const [featuredTournamentDetail, setFeaturedTournamentDetail] = useState(undefined);
    const [isOpenApplyPromoCodePopup, setIsOpenApplyPromoCodePopup] = useState(false);

    const [html5GameSetting, setHtml5GameSetting] = useState(undefined);
    const [html5GameSettingTotalJoinedPlayers, setHtml5GameSettingTotalJoinedPlayers] = useState(0);

    const [refreshing, setRefreshing] = useState(false);

    const [oneTimeBonusAmount, setOneTimeBonusAmount] = useState(0);
    const [isOneTimeBonus, setOneTimeBonus] = useState(false);

    const [oneTimeBonusImg, setOneTimeBonusImg] = useState(undefined);


    // Banner
    const [banners, setBanners] = useState([]);
    var bannersFlatListIndexTemp = 0
    var bannersCount = 0
    const bannersFlatListRef = React.useRef(null);

    // Socket
    // const socket = io(generalSetting.SOCKET_URL); //Commented
    // const [connected, setConnected] = useState(socket.connected);//Commented
    // const [currentTransport, setCurrentTransport] = useState(socket.connected ? socket.io.engine.transport.name : '-');//Commented
    const [lastMessage, setLastMessage] = useState('');

    const [customerMessageList, setCustomerMessageList] = useState([]);
    const [isOpenAppUpdatePopup, setIsOpenAppUpdatePopup] = useState(false);
    const [appVersionData, setAppVersionData] = useState(false);
    const [progressValue, setProgressValue] = useState(0)
    const [isProgressShow, setProgressShow] = useState(false)
    // This Use-Effect function should call the Banners, Featured Tournaments, All Games, Get Profile & Daily Login Rewards Status Check API
    useEffect(() => {
        // socket.on('connect', () => onConnectionStateUpdate());

        // socket.on('disconnect', () => onConnectionStateUpdate());
        // socket.on('message', (content) => onMessage(content));
        addLog("VersionInfo=====>", VersionInfo.appVersion);


        setLoading(true)
        getfeaturedTournament()
        getAllGames()
        dispatch(actionCreators.requestVersionInfoList({
            "filter": {
                "isNewAppVersion": true,
                "osType": "android"
            }
        }))

        dispatch(actionCreators.requestHtml5GameSettings(payload))

        let payload = {
            skip: 0,
            limit: 100
        }

        dispatch(actionCreators.requestBanners({
            skip: 0, limit: 100, sortBy: 'order', sort: 'asc'
        }))
        dispatch(actionCreators.requestDailyLoginRewardsStatusCheck())



        payload = {
            skip: 0,
            limit: 100,
        }
        dispatch(actionCreators.requestScreens(payload))
        dispatch(actionCreators.requestInfoPupupList(payload))

        setTimeout(() => {
            if (global.fcmToken) {
                dispatch(actionCreators.requestUpdateDeviceToken({ 'firebaseToken': global.fcmToken, 'currentVersion': VersionInfo.appVersion, 'isContactSynced': true }))
                addLog("global.profile===>", global.profile);
                if (global.profile.isContactSynced === false) {
                    addLog("fetchContacts===> isContactSynced ===>", global.profile.isContactSynced);
                    fetchContacts()
                }
            }
        }, 5000)

        getProfileData()
        dispatch(actionCreators.requestCustomerCareMessageList({ "filter": { "isLastMessageRead": false } }))
        // isContactSynced

        return () => {
            dispatch(actionCreators.resetAllGamesState())
            dispatch(actionCreators.resetUpdateDeviceTokenState)

            // socket.on('disconnect', () => onConnectionStateUpdate());//Commented
        }
    }, [])

    const getfeaturedTournament = () => {
        dispatch(actionCreators.requestFeaturedTournaments({
            skip: 0,
            limit: 100,
            filter: {
                isFeatured: true
            }
        }))
    }

    const getAllGames = () => {
        dispatch(actionCreators.requestAllGames({
            skip: 0,
            limit: 100, sortBy: 'order', sort: 'asc'
        }))
    }


    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        getfeaturedTournament()
        getAllGames()
        dispatch(actionCreators.requestHtml5GameSettings(payload))

    }, [refreshing]);

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.tabAllGames.model }),
        shallowEqual
    );
    var { bannerListResponse, featuredTournamentListResponse, allGamesListResponse, dailyLoginRewardsStatusCheckResponse, featuredTournamentDetailResponse, syncContactsResponse, html5GameSettingResponse, customerCareMessageListResponse, versionInfoListResponse } = currentState
    // addLog("versionInfoListResponse==>", versionInfoListResponse);
    // This function should return the Banners API Response
    useEffect(() => {
        if (versionInfoListResponse && versionInfoListResponse.list) {
            setLoading(false)

            setAppVersionData(versionInfoListResponse.list[0]);
            if (versionInfoListResponse.list[0].appVersion == VersionInfo.appVersion) {
                // addLog("VersionInfo==", VersionInfo.appVersion);
            } else {
                setIsOpenAppUpdatePopup(true);
                setIsOpenDailyRewardsPopup(false);
                //addLog("VersionInfo!==", VersionInfo.appVersion);
            }

        } else {
            if (versionInfoListResponse && versionInfoListResponse.errors[0] && versionInfoListResponse.errors[0].code === 'middlewares.token_parser.validation.token_invalid') {
                clearDataFromLocalStorage()
                dispatch(actionCreators.logoutUser())
                setTimeout(function () { props.navigation.replace(Constants.nav_login); }, 500);
                // 
            }
        }
        versionInfoListResponse = undefined
    }, [versionInfoListResponse])
    //Device Token
    const { currentStateUpdateDeviceToken } = useSelector(
        (state) => ({ currentStateUpdateDeviceToken: state.updateDeviceToken.model }),
        shallowEqual
    );
    var { updateDeviceTokenResponse } = currentStateUpdateDeviceToken

    // This function should return the Banners API Response
    useEffect(() => {
        if (bannerListResponse && bannerListResponse.list) {
            setLoading(false)

            bannersCount = bannerListResponse.list.length
            setBanners(bannerListResponse.list);

            setInterval(() => {
                setAutoScrollBanner()
            }, 5000)
        } else {
            if (bannerListResponse && bannerListResponse.errors[0] && bannerListResponse.errors[0].code === 'middlewares.token_parser.validation.token_invalid') {
                clearDataFromLocalStorage()
                dispatch(actionCreators.logoutUser())
                setTimeout(function () { props.navigation.replace(Constants.nav_login); }, 500);
                // 
            }
        }
        bannerListResponse = undefined
    }, [bannerListResponse])

    // This function should return the Featured Videos API Response
    useEffect(() => {
        if (featuredTournamentListResponse && featuredTournamentListResponse.list) {
            setLoading(false)
            setRefreshing(false)
            setFeaturedTournaments(featuredTournamentListResponse.list)
        }
        featuredTournamentListResponse = undefined
    }, [featuredTournamentListResponse])

    // This function should return the All Games API Response
    //   addLog("html5GameSettingResponse==>", html5GameSettingResponse);

    // This function should return the Featured Videos API Response
    useEffect(() => {
        if (html5GameSettingResponse && html5GameSettingResponse.list) {

            setHtml5GameSetting(html5GameSettingResponse.list[0])
            setHtml5GameSettingTotalJoinedPlayers(html5GameSettingResponse.totalJoinedPlayers)
        }
        html5GameSettingResponse = undefined
    }, [html5GameSettingResponse])

    useEffect(() => {
        if (allGamesListResponse && allGamesListResponse.list) {
            setLoading(false)
            setRefreshing(false)
            var tempGameTypes = []
            if (html5GameSetting) {
                // tempGameTypes.push({ _id: "freeGame001", name: "Free Game", totalJoinedPlayers: "10000", 'featuredImage': { "default": "GH5FI-17092020224528_601a3b3f76b6e5252d2f4690_1630183949496.png", "small": "GH5FI-17092020224528_601a3b3f76b6e5252d2f4690_1630183949496_small.png", "medium": "GH5FI-17092020224528_601a3b3f76b6e5252d2f4690_1630183949496_medium.png", "large": "GH5FI-17092020224528_601a3b3f76b6e5252d2f4690_1630183949496_large.png" } })
                tempGameTypes.push({ _id: "freeGame001", name: html5GameSetting.title, totalJoinedPlayers: html5GameSettingTotalJoinedPlayers, featuredImage: html5GameSetting.featuredImage })
                tempGameTypes.push(...allGamesListResponse.list)

                setAllGames(tempGameTypes)
            }
        }
        allGamesListResponse = undefined
    }, [allGamesListResponse])

    // Update Device Token Response
    useEffect(() => {
        if (updateDeviceTokenResponse) {
            addLog('updateDeviceTokenResponse', updateDeviceTokenResponse)
        }
        updateDeviceTokenResponse = undefined
    }, [updateDeviceTokenResponse])

    // Sync Contacts Response
    useEffect(() => {
        if (syncContactsResponse) {
            addLog('syncContactsResponse', syncContactsResponse)
        }
        syncContactsResponse = undefined
    }, [syncContactsResponse])

    // This function should return the Daily Login Rewards Status API Response
    useEffect(() => {
        if (dailyLoginRewardsStatusCheckResponse && isOpenAppUpdatePopup == false) {

            // setDay(3)
            // openDailyRewardsPopup(true);
            // return

            setLoading(false)
            if (dailyLoginRewardsStatusCheckResponse.item) {
                dailyLoginRewardsStatusCheckResponse.item.options && dailyLoginRewardsStatusCheckResponse.item.options.forEach((element) => {
                    if (element.field == "isOneTimeBonus") {
                        setOneTimeBonus(element.data);
                    }
                    if (element.field == "oneTimeBonusAmount") {
                        setOneTimeBonusAmount(element.data);
                    }
                    if (element.field == "oneTimeBonusImage") {
                        setOneTimeBonusImg(element.data);
                    }

                });

                setDailyLoginRewardsStatus(dailyLoginRewardsStatusCheckResponse.item)
                if (dailyLoginRewardsStatusCheckResponse.item.isDailyRewardPopup) {
                    setDay(dailyLoginRewardsStatusCheckResponse.item.currentDay);
                    openDailyRewardsPopup(true);
                }
            }
        }
        dailyLoginRewardsStatusCheckResponse = undefined
    }, [dailyLoginRewardsStatusCheckResponse])


    useEffect(() => {
        if (customerCareMessageListResponse) {

            if (customerCareMessageListResponse.list && customerCareMessageListResponse.list.length > 0) {
                setCustomerMessageList(customerCareMessageListResponse.list)
            }
        }
    }, [customerCareMessageListResponse])

    // This function calls the API when coming to this screen for the first time
    // Also calls when coming from the Withdrawal Screen
    const getProfileData = () => {
        addLog("getProfileData===>");
        dispatch(actionCreators.requestGetProfile())
    };

    // This function should open up the popup for the Daily Rewards
    let openDailyRewardsPopup = (visible) => {
        setIsOpenDailyRewardsPopup(visible);
    }

    const downloadApp = (url) => {
        const filePath = RNFS.DocumentDirectoryPath + '/GamerJi-Pro.apk';
        // addLog("filePath",filePath);
        setProgressShow(true);
        const download = RNFS.downloadFile({
            fromUrl: url,
            toFile: filePath,
            progress: res => {
                setProgressValue((res.bytesWritten / res.contentLength).toFixed(2) * 1);
                console.log((res.bytesWritten / res.contentLength).toFixed(2) * 1);
            },
            progressDivider: 1
        });

        download.promise.then(result => {
            if (result.statusCode == 200) {
                RNApkInstallerN.install(filePath)
            }
        });
        // Linking.openURL(url)
    }
    // Socket // Commented
    // const onConnectionStateUpdate = () => {
    //     addLog('onConnectionStateUpdate', '')
    //     setConnected(socket.connected)
    //     addLog('onConnectionStateUpdate:::connected:::> ', socket.connected)
    //     setCurrentTransport(socket.connected ? socket.io.engine.transport.name : '-')
    //     if (socket.connected) {
    //         socket.io.engine.on('upgrade', () => onUpgrade());
    //     } else {
    //         socket.io.engine.off('upgrade');
    //     }
    // }

    // Commented
    // const onMessage = (content) => {
    //     addLog('onMessage', '')
    //     addLog('onMessage:::content:::> ', content)
    //     setLastMessage(content)
    //     addLog('onMessage:::lastMessage:::> ', lastMessage)
    // }

    // Commented
    // const onUpgrade = () => {
    //     addLog('onUpgrade', '')
    //     setCurrentTransport(socket.io.engine.transport.name)
    //     addLog('onUpgrade:::currentTransport:::> ', socket.io.engine.transport.name)
    // }

    // This Use-Effect function should call an alert and ask you to exit from the app.
    useEffect(() => {

        const backAction = () => {
            Alert.alert(
                Constants.app_name,
                Constants.alert_exit_app, // <- this part is optional, you can pass an empty string
                [
                    { text: Constants.action_cancel, onPress: () => null },
                    { text: Constants.action_ok, onPress: () => BackHandler.exitApp() }
                ]);
            return true;
        };

        const backHandler = BackHandler.addEventListener("hardwareBackPress", backAction);
        return () => backHandler.remove();
    }, []);
    const clickToBanner = (banner) => {
        if (global.profile) {
            dispatch(actionCreators.requestBannerAdsLog({ user: global.profile._id, banner: banner, type: 2 }))
            // socket.emit("gbaLog", { user: global.profile._id, banner: item._id, type: 2 }) // type = 2 = Click banner
        }
    }
    // This function should open featured tournament
    const moveToSingleTournament = (shortCode) => {
        let payload = {
            code: shortCode
        }
        setLoading(true)
        dispatch(actionCreators.requestFeaturedTournamentDetail(payload))
    }

    useEffect(() => {
        if (featuredTournamentDetailResponse) {
            setLoading(false)
            if (featuredTournamentDetailResponse.success && featuredTournamentDetailResponse.success == true) {
                //RootNavigation.navigate(Constants.nav_single_tournament_to_join, { tournament: featuredTournamentDetailResponse.item })
                addLog('featuredTournamentDetailResponse.item', featuredTournamentDetailResponse.item)
                setFeaturedTournamentDetail(featuredTournamentDetailResponse.item)
                setOpenSingleTournamentPopup(true)
            }
        }
    }, [featuredTournamentDetailResponse])

    const setAutoScrollBanner = () => {
        if (bannersFlatListIndexTemp >= bannersCount) {
            bannersFlatListIndexTemp = 0
        }
        bannersFlatListIndexTemp = bannersFlatListIndexTemp + 1
        setBannersFlatListIndex(bannersFlatListIndexTemp)
    }

    useEffect(() => {
        if (banners.length == 0) {
            return
        }
        if (bannersFlatListIndex >= banners.length) {
            setBannersFlatListIndex(0)
            return
        }
        if (bannersFlatListRef) {
            bannersFlatListRef.current.scrollToIndex(bannersFlatListIndex)
            if (global.profile && banners.length > 0) {
                dispatch(actionCreators.requestBannerAdsLog({ user: global.profile._id, banner: banners[bannersFlatListIndex]._id, type: 1 }))
                // socket.emit("gbaLog", { user: global.profile._id, banner: banners[bannersFlatListIndex]._id, type: 1 }) // type = 1 = View banner // Commented
            }
        }
    }, [bannersFlatListIndex])

    let openApplyPromoCodePopup = (visible) => {
        setIsOpenApplyPromoCodePopup(visible)
    }

    const fetchContacts = () => {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'This app would like to get your contacts to match joined contest with your friends.',
                    'buttonPositive': 'Please accept'
                }
            )
                .then(() => {
                    Contacts.getAll().then(contacts => {
                        formatContacts(contacts)
                    })
                })
        }
    }

    const formatContacts = (fetchedContacts) => {

        var formattedContacts = []

        for (let index = 0; index < fetchedContacts.length; index++) {
            const element = fetchedContacts[index];

            var contact = {}
            contact['name'] = element.displayName || ''

            if (element.emailAddresses && element.emailAddresses.length > 0) {
                contact['email'] = element.emailAddresses[0].email || ''
            } else {
                contact['email'] = ''
            }

            if (element.phoneNumbers && element.phoneNumbers.length > 0) {

                if (element.phoneNumbers[0] && element.phoneNumbers[0].number) {
                    let separatedPhoneNumber = element.phoneNumbers[0].number.split(' ')
                    if (separatedPhoneNumber.length > 0) {

                        var phone = ''
                        var phoneCode = ''

                        for (let i = 0; i < separatedPhoneNumber.length; i++) {
                            const number = separatedPhoneNumber[i];
                            if (number.includes('+')) {
                                phoneCode = number
                            } else {
                                phone = phone + number
                            }
                        }
                        contact['phone'] = phone
                        contact['phoneCode'] = phoneCode
                    }
                }
            }

            formattedContacts.push(contact)
        }

        dispatch(actionCreators.requestSyncContacts({ contacts: formattedContacts }))
    }

    const customerCareMessagePressed = index => {

        RootNavigation.navigate(Constants.nav_ticket_detail, { ticketId: customerMessageList[index]._id })

        var tempArr = [...customerMessageList]
        tempArr.splice(index, 1)
        setCustomerMessageList(tempArr)
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} hidden={false} translucent={false} />

                {/* Wrapper - Gamerji Logo */}
                <View style={[marginStyles.topMargin_20, otherStyles.zIndex_1]}>

                    {/* Image - GamerJi Logo */}
                    <Image style={[heightStyles.height_60, widthStyles.width_75, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/logo.png')} />
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderRadius_40, colorStyles.whiteBackgroundColor, marginStyles.bottomMargin_10, otherStyles.zIndex_0, alignmentStyles.overflow, { marginTop: -18, overflow: 'hidden' }]}>

                    {/* Scroll View */}
                    <ScrollView style={[heightStyles.height_100p, positionStyles.relative, marginStyles.bottomMargin_10, alignmentStyles.overflow]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >

                        {/* Wrapper - Banner Items */}
                        {banners && banners.length != 0 &&
                            <View style={[alignmentStyles.bottom_0, marginStyles.topMargin_20]}>

                                <Carousel
                                    ref={bannersFlatListRef}
                                    data={banners}
                                    renderItem={({ index, item }) => <AllGamesBannerItem item={item} clickToBanner={clickToBanner} openApplyPromoCodePopup={openApplyPromoCodePopup} getProfileData={getProfileData} index={index} isLastIndex={banners.length - 1 == index} />}
                                    style={{ marginTop: 10, height: 100 }}
                                    itemWidth={Constants.SCREEN_WIDTH - 40}
                                    containerWidth={Constants.SCREEN_WIDTH}
                                    separatorWidth={10}
                                />

                                {/* <FlatList
                                    style={{ marginTop: 10, height: 10, alignSelf: 'center' }}
                                    data={banners}
                                    renderItem={({ index, item }) => <AllGamesBannerIndicatorItem item={item} selectedIndex={bannersFlatListIndex} currentIndex={index} />}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                /> */}
                            </View>
                        }

                        {/* Wrapper - Featured Tournaments */}
                        {featuredTournaments && featuredTournaments.length != 0 &&
                            <>
                                <View style={[flexDirectionStyles.row, marginStyles.topMargin_30, { zIndex: 0, marginLeft: 20, marginRight: 20 }]}>

                                    {/* Image - Featured Tournaments */}
                                    <Image style={[heightStyles.height_24, widthStyles.width_18, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/ic_home_trophy.png')} />

                                    {/* Text - Featured Tournaments Separator */}
                                    <Text style={[heightStyles.height_17, widthStyles.width_2, colorStyles.yellowBackgroundColor, marginStyles.leftMargin_10, marginStyles.rightMargin_10, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - Featured Tournaments */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_18, alignmentStyles.alignSelfCenter]}> {Constants.text_featured_tournaments} </Text>
                                </View>

                                {/* Wrapper - Featured Tournament Items */}
                                <View style={[alignmentStyles.bottom_0, marginStyles.topMargin_10, { marginLeft: 10, marginRight: 10 }]}>
                                    <FlatList
                                        data={featuredTournaments}
                                        renderItem={({ item, index }) => <FeaturedTournamentsItem item={item} index={index} moveToSingleTournament={moveToSingleTournament} />}
                                        keyExtractor={(item, index) => index.toString()}
                                        horizontal={true}
                                        showsVerticalScrollIndicator={false}
                                        showsHorizontalScrollIndicator={false} />
                                </View>
                            </>
                        }

                        {/* Customer Care Message List */}
                        {
                            customerMessageList && customerMessageList.length > 0 &&
                            <FlatList
                                style={{ marginTop: 20 }}
                                data={customerMessageList}
                                renderItem={({ index, item }) => <CustomerMessageListItem item={item} index={index} customerCareMessagePressed={customerCareMessagePressed} />}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                pagingEnabled={true}
                            />
                        }
                        {/* UnClaimed coin */}
                        {/* <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, width: Constants.SCREEN_WIDTH - 20, height: 75, alignItems: 'center' }}  > */}


                        {/* <View style={{ backgroundColor: colors.black, borderRadius: 5, height: 55, width: "100%", paddingTop: 15, paddingLeft: 10, flexDirection: 'row' }}> */}
                        {/* Message */}
                        {/* <Image style={{ width: 25, height: 25, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')}></Image>
                                <Text style={{ marginLeft: 10, fontSize: 14, color: colors.white, marginBottom: 0, fontFamily: fontFamilyStyleNew.semiBold }} >You have</Text>
                                <Text style={{ fontSize: 16, color: colors.yellow, marginBottom: 0, fontFamily: fontFamilyStyleNew.semiBold }} > 500 </Text>
                                <Text style={{ fontSize: 14, color: colors.white, marginBottom: 0, fontFamily: fontFamilyStyleNew.semiBold }} >unclaimed Coins!</Text>

                            </View> */}
                        {/* Climed */}

                        {/* <TouchableOpacity style={{
                                backgroundColor: colors.red, borderColor: colors.black, borderWidth: 3, borderRadius: 5, position: 'absolute', height: 30, bottom: 0, top: 40, justifyContent: "center",
                            }} activeOpacity={0.8} onPress={() => navigateToEarnCoin()} >
                                <Text style={{ marginLeft: 20, marginRight: 20, fontSize: 14, color: 'white', fontFamily: fontFamilyStyleNew.bold }} > Claim Now</Text>
                            </TouchableOpacity>

                        </View> */}
                        {allGames && allGames.length != 0 &&
                            <>
                                {/* Wrapper - All Games */}
                                <View style={[flexDirectionStyles.row, marginStyles.topMargin_20, { marginLeft: 20, marginRight: 20 }]}>

                                    {/* Image - All Games */}
                                    <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/how_to_join.png')} />

                                    {/* Text - All Games Separator */}
                                    <Text style={[heightStyles.height_17, widthStyles.width_2, colorStyles.yellowBackgroundColor, marginStyles.leftMargin_10, marginStyles.rightMargin_10, alignmentStyles.alignSelfCenter]} />

                                    {/* Text - All Games */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_18, alignmentStyles.alignSelfCenter]}> {Constants.text_all_games} </Text>
                                </View>

                                {/* Wrapper - All Game Items */}
                                <View style={[containerStyles.container, marginStyles.topMargin_10, { marginLeft: 10, marginRight: 10, marginBottom: 10 }]}>
                                    {/* <HomeFreeGamesItem navigation={props.navigation} item={{ _id: "12331112222", name: "Free Game", totalJoinedPlayers: "10000" }}></HomeFreeGamesItem> */}

                                    <FlatList
                                        data={allGames}
                                        renderItem={({ item }) => <AllGamesItem item={item} navigation={props.navigation} />}
                                        keyExtractor={(item, index) => index.toString()}
                                        numColumns={3}
                                    />
                                </View>
                            </>
                        }
                    </ScrollView>

                    {checkIsSponsorAdsEnabled('allGames') &&
                        <View style={{ marginBottom: 25 }}>
                            <SponsorBannerAds screenCode={'allGames'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('allGames')} />
                        </View>
                    }
                </View>
            </View>

            <Modal
                isVisible={isOpenDailyRewardsPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <BottomSheetDailyLoginRewards setOneTimeBonus={setOneTimeBonus} currentDay={day} openDailyRewardsPopup={openDailyRewardsPopup} oneTimeBonusAmount={oneTimeBonusAmount} isOneTimeBonus={isOneTimeBonus} oneTimeBonusImg={oneTimeBonusImg} />
            </Modal>
            <Modal
                isVisible={isOpenAppUpdatePopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >

                <View style={{
                    height: 300,
                    backgroundColor: colors.yellow,
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    borderTopStartRadius: 50,
                    borderTopEndRadius: 50,
                }}>


                    {/* <Image style={{ height: 95, width: 95, alignSelf: 'center' }} source={require('../../assets/images/ic_daily_rewards.png')} /> */}
                    <ScrollView style={{ marginTop: 10, marginBottom: 5 }}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>
                        <Text style={{ marginTop: 10, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 26, textAlign: 'center', }} numberOfLines={2} >{appVersionData.title}</Text>
                        {appVersionData.details && appVersionData.details.map((detailData) =>
                            <View style={{ marginLeft: 24, marginRight: 10, marginTop: 10, flex: 1, flexDirection: 'row' }}>
                                <Image style={{ resizeMode: 'contain', height: 12, width: 12, marginTop: 3, }} source={require('../../assets/images/profile_red_circle_bg.png')} />
                                <Text style={{ marginLeft: 5, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 16, textAlign: 'left', }} numberOfLines={3} > {detailData}</Text>
                            </View>
                        )}



                    </ScrollView>
                    {isProgressShow ?
                        <View style={{ marginTop: 10, width: 350, marginBottom: 10 }}  >
                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, marginBottom: 5, textAlign: 'center', }} numberOfLines={1} > Downloading...</Text>
                            <ProgressBar style={{ marginLeft: 20, marginRight: 20, height: 10, width: '100%', backgroundColor: 'white', borderRadius: 8 }} progress={progressValue} color={colors.red} />
                        </View> : <View></View>
                    }
                    <TouchableOpacity style={{ bottom: 0, marginTop: 5, marginLeft: 20, marginRight: 20, marginBottom: 10, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => downloadApp(appVersionData.downloadLink)} activeOpacity={0.5} >
                        <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >UPDATE NOW</Text>
                        <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                    </TouchableOpacity>
                </View>
            </Modal>
            {isLoading && <CustomProgressbar />}

            {
                isOpenSingleTournamentPopup && featuredTournamentDetail &&
                <Modal isVisible={true}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <SingleTournamentToJoin setFeaturedTournamentDetail={setFeaturedTournamentDetail} tournamentDetail={featuredTournamentDetail} setOpenSingleTournamentPopup={setOpenSingleTournamentPopup} />
                </Modal>
            }

            <Modal
                isVisible={isOpenApplyPromoCodePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <BottomSheetApplyPromoCode openApplyPromoCodePopup={openApplyPromoCodePopup} />
            </Modal>

        </SafeAreaView >
    )
}

export default AllGames;

const AllGamesBannerItem = (props) => {

    let item = props.item
    // const socket = io(generalSetting.SOCKET_URL)

    let screen = props.item && props.item.screen && props.item.screen.code;;
    const onPressBanner = () => {

        props.clickToBanner(item._id);


        switch (screen) {

            case 'contestsList':
                let gameType = item.gameType
                RootNavigation.navigate(Constants.nav_contest_list, { gameType })
                break;

            case 'profile':
                RootNavigation.navigate(Constants.nav_profile_screen)
                break;

            case 'applyPromoCode':
                props.openApplyPromoCodePopup(true)
                break;

            case 'addABalanceWallet':
                global.addBalanceScreenType = Constants.all_games_add_money
                global.addBalanceCurrencyType = Constants.coin_payment
                global.paymentSuccessCompletion = props.getProfileData
                RootNavigation.navigate(Constants.nav_coin_reward_store)
                break;

            case 'URLRedirect':
                Linking.canOpenURL(item.url).then(supported => {
                    if (supported) {
                        Linking.openURL(item.url)
                    }
                })
                break;

            case 'tournamentLists':
                let game = item.game
                RootNavigation.navigate(Constants.nav_tournament_list, { game });
                break;

            case 'coinStore':
                RootNavigation.navigate(Constants.nav_coin_reward_store);

                break;

            // case 'paymentGateway':
            //     RootNavigation.navigate(Constants.nav_payment_gateway_new);//getting error
            //     break;

            case 'leaderboard':
                RootNavigation.navigate(Constants.nav_more_leaderboard);
                break;

            case 'applyPromoCodePopup':
                RootNavigation.navigate(Constants.nav_more_apply_promo_code);
                break;

            case 'dailyLoginRewards':
                RootNavigation.navigate(Constants.nav_daily_login_rewards);
                break;

            case 'editProfile':
                RootNavigation.navigate(Constants.nav_edit_profile);
                break;

            case 'esportsNewsDetails':
                RootNavigation.navigate(Constants.nav_view_all_esports_news);
                break;

            case 'gamerjiPoints':
                RootNavigation.navigate(Constants.nav_more_gamerji_points);
                break;

            case 'more':
                RootNavigation.navigate(Constants.nav_more);
                break;

            case 'viewAllTopProfiles':
                RootNavigation.navigate(Constants.nav_view_all_top_profiles);
                break;

            case 'rewardStore':
                let rewards = 'rewards';
                let myRewards = 'rewards';

                RootNavigation.navigate(Constants.nav_coin_reward_store, { rewards, myRewards });
                break;

            case 'myRewards':
                rewards = 'rewards';
                myRewards = 'myRewards';
                RootNavigation.navigate(Constants.nav_coin_reward_store, { rewards, myRewards });
                break;

            case 'collegeLeagues':
                let activeProfile = true;
                RootNavigation.navigate(Constants.nav_profile_screen, { activeProfile });
                break;

            case 'worldOfEsports':
                RootNavigation.navigate(Constants.nav_world_of_esports);
                break;

                // case 'htmlGames':
                //     RootNavigation.navigate(Constants.nav_html_game_type, { game: item });

                break;
            default:
                break;
        }
    }

    return (
        <TouchableOpacity style={{ marginLeft: props.index == 0 ? 20 : 0, marginRight: props.isLastIndex ? 20 : 0, height: 100, borderRadius: 5, backgroundColor: colors.silver }} onPress={() => onPressBanner()} activeOpacity={0.9} >
            <Image style={{ flex: 1, resizeMode: 'cover', borderRadius: 5 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} />
        </TouchableOpacity>
    )
}

const AllGamesBannerIndicatorItem = (props) => {

    return (
        <View style={{ marginLeft: 5 }} >
            {props.selectedIndex == props.currentIndex ?
                <Image style={{ width: 20, height: 10, flex: 1, backgroundColor: colors.black, borderRadius: 5 }} />
                :
                <Image style={{ width: 10, height: 10, flex: 1, backgroundColor: '#E5E5EE', borderRadius: 5 }} />
            }
        </View>
    )
}

const CustomerMessageListItem = props => {

    const item = props.item

    return (
        <TouchableOpacity style={[shadowStyles.shadowProp, { marginLeft: 10, marginRight: 10, width: Constants.SCREEN_WIDTH - 20, height: 100, alignItems: 'center', }]} activeOpacity={0.8} onPress={() => props.customerCareMessagePressed(props.index)}  >


            {/* <View style={shadowStyles.shadowProp,{ backgroundColor: colors.black, borderRadius: 5, height: 80, width: "100%", marginTop: 18, paddingTop: 20, paddingLeft: 10, flexDirection: 'row' }}> */}
            <View style={{ backgroundColor: colors.black, borderRadius: 5, height: 80, width: Constants.SCREEN_WIDTH - 20, marginTop: 18, paddingTop: 20, paddingLeft: 10, flexDirection: 'row' }}>
                {/* Message */}
                <Image style={{ width: 25, height: 25, resizeMode: 'contain' }} source={require('../../assets/images/chatting.png')}></Image>
                <View style={{ marginLeft: 10, marginRight: 10, width: "100%" }}>
                    <Text style={{ fontSize: 14, color: colors.white, marginBottom: 0, fontFamily: fontFamilyStyleNew.semiBold }} >Message From Gamerji</Text>
                    <Text style={{ fontSize: 12, color: colors.grey, marginBottom: 0, fontFamily: fontFamilyStyleNew.semiBold }} numberOfLines={2} ellipsizeMode='tail'>{item.lastMessage}
                    </Text>
                </View>
            </View>

            <View style={{
                backgroundColor: colors.red, borderColor: colors.black, borderWidth: 3, borderRadius: 5, position: 'absolute', height: 35, bottom: 0, top: 0, justifyContent: "center",
            }}>
                {/* Ticket Id */}
                <CustomMarquee value={'Ticket No : #' + item.code} style={{ marginLeft: 20, marginRight: 20, fontSize: 14, color: 'white', fontFamily: fontFamilyStyleNew.bold }} />

            </View>
        </TouchableOpacity>
    )
}