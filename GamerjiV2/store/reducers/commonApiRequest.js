import produce from 'immer';
import * as actionTypes from '../actions/apiClient/requestActionTypes';

{ /* Initialise - State */ }
const initialState = {
    model: {
        data: {},
    },
    error: false
};

{ /* Request */ }
const request = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.data = action.model;
    });
};

{ /* Request Failure */ }
const requestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.data = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST:
            return request(state, action);
        case actionTypes.REQUEST_FAILED:
            return requestFailed(state, action);
        default:
            return state;
    }
};

export default reducer;