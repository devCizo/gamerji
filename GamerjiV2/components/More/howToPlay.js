import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, shadowStyle } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const HowToPlay = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [activeTab, setActiveTab] = useState('english');
    const [englishData, setEnglishData] = useState(undefined);
    const [hindiData, setHindiData] = useState(undefined);

    //Fetch How to play list
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestHowToPlayList({}))

        return () => {
            dispatch(actionCreators.resetHowToPlayState())
        }
    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.howToPlay.model }),
        shallowEqual
    );
    var { howToPlayListResponse } = currentState

    // How To Play List Response
    useEffect(() => {

        setLoading(false)
        if (howToPlayListResponse && howToPlayListResponse.item) {

            for (let index = 0; index < howToPlayListResponse.item.length; index++) {
                if (howToPlayListResponse.item[index].type) {
                    if (howToPlayListResponse.item[index].type == 'English') {
                        var tempArr = howToPlayListResponse.item[index].data.map(item => {
                            let tempItem = { ...item };
                            tempItem.isSelected = false;
                            return tempItem;
                        })
                        setEnglishData(tempArr)
                    } else if (howToPlayListResponse.item[index].type == 'हिंदी') {
                        var tempArr = howToPlayListResponse.item[index].data.map(item => {
                            let tempItem = { ...item };
                            tempItem.isSelected = false;
                            return tempItem;
                        })
                        setHindiData(tempArr)
                    }
                }
            }
        }

    }, [howToPlayListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let getCurrentTabData = () => {
        debugger
        if (activeTab == 'english' && englishData) {
            return englishData
        } else if (activeTab == 'हिंदी' && hindiData) {
            return hindiData
        }
        return []
    }

    let openCloseSection = (index) => {
        if (activeTab == 'english') {
            var tempArr = [...englishData]
            tempArr[index].isSelected = !tempArr[index].isSelected
            setEnglishData(tempArr)
        } else {
            var tempArr = [...hindiData]
            tempArr[index].isSelected = !tempArr[index].isSelected
            setHindiData(tempArr)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >HOW TO PLAY</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {/* Tabs View */}
                <View style={{ marginTop: 15, marginLeft: 40, marginRight: 40, height: 34, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 17 }} >

                    {/* Today */}
                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'english' ? colors.black : 'transparent' }} onPress={() => setActiveTab('english')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >English</Text>
                    </TouchableOpacity>

                    {/* By Week */}
                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'हिंदी' ? colors.black : 'transparent' }} onPress={() => setActiveTab('हिंदी')}>
                        <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >हिंदी</Text>
                    </TouchableOpacity>

                </View>

                <FlatList style={{ marginTop: 10 }}
                    data={getCurrentTabData()}
                    renderItem={({ item, index }) => <HowToPlayItem item={item} index={index} openCloseSection={openCloseSection} />}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                />

                {checkIsSponsorAdsEnabled('howToPlay') &&
                    <SponsorBannerAds screenCode={'howToPlay'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('howToPlay')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const HowToPlayItem = (props) => {

    let item = props.item

    return (
        <View style={[shadowStyle.shadow, { marginTop: 8, marginLeft: 20, marginRight: 20, marginBottom: 8, borderRadius: 10, borderWidth: 1, borderColor: '#DFE4E9', backgroundColor: colors.white, }]} >

            {/* Title */}
            <TouchableOpacity style={{ height: 48, justifyContent: 'center' }} onPress={() => props.openCloseSection(props.index)} >
                <Text style={{ marginLeft: 12, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >{item.title}</Text>
                {!item.isSelected ?
                    <Image style={{ position: 'absolute', right: 18, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />
                    :
                    <Image style={{ position: 'absolute', right: 18, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_up_arrow.png')} />
                }
            </TouchableOpacity>

            {item.isSelected &&
                <FlatList
                    data={item.content}
                    renderItem={({ item }) => <HowToPlaySubItem item={item} />}
                />
            }

        </View>
    )
}

const HowToPlaySubItem = (props) => {

    let item = props.item

    return (
        <View>
            {item.desc &&
                <Text style={{ marginTop: 7, marginLeft: 12, marginRight: 12, marginBottom: 12, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >{item.desc}</Text>
            }

            {item.image &&
                <View>
                    <Image style={{ marginLeft: 8, marginRight: 8, marginBottom: 20, height: 300, resizeMode: 'contain' }} source={item.image && { uri: item.image }} />
                </View>
                // 
            }

        </View>
    )
}

export default HowToPlay;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        paddingBottom: 20
    },
})