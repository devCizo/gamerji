import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, FlatList, TouchableOpacity, Image, SafeAreaView, BackHandler, ScrollView } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import DailyLoginRewardsItem from '../../componentsItem/DailyLoginRewardsItem';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import { AppConstants } from '../../appUtils/appConstants';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import Modal from 'react-native-modal';
import DailyLoginAvatarPopup from './dailyLoginAvatarPopup';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';


const DailyLoginRewards = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [dailyLoginRewards, setDailyLoginRewards] = useState([]);
    const [isOpenDailyLoginAvatarPopup, setOpenDailyLoginAvatarPopup] = useState(false)
    const [isLoading, setLoading] = useState(false);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    const [totalGamerjiPoints, setTotalGamerjiPoints] = useState(undefined);
    const [totalBonusAmount, setTotalBonusAmount] = useState(undefined);
    const [totalRewardDeposit, setTotalRewardDeposit] = useState(undefined);
    const [currentDay, setCurrentDay] = useState(undefined);
    const [dailyLoginRewardItem, setDailyLoginRewardItem] = useState(undefined);

    var gamerjiPoints = 0, bonus = 0, rewardDeposit = 0, pageLimit = 100;

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestDailyLoginRewardsStatusCheckMore())


        return () => {
            dispatch(actionCreators.resetDailyLoginRewardsState())
        }
    }, [])


    // This Use-Effect function should call the Daily Login Rewards API
    const callToDailyLoginRewardsListAPI = (currentDay) => {
        let payload = {
            skip: 0,
            limit: pageLimit,
            day: currentDay,
            sortBy: 'day',
            sort: 'asc'
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestDailyLoginRewards(payload))
    }

    // This function sets the Daily Login Rewards API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.dailyLoginRewards.model }),
        shallowEqual
    );
    var { dailyLoginRewardsResponse, dailyLoginRewardsStatusCheckResponse } = currentState

    // This function should set the API Response to Model
    // const { currentStateDailyLogin } = useSelector(
    //     (state) => ({ currentStateDailyLogin: state.dailyLoginRewards.model }),
    //     shallowEqual
    // );
    // var { dailyLoginRewardsStatusCheckResponse } = currentStateDailyLogin


    // This function should set the API Response to Model
    const { currentStateReward } = useSelector(
        (state) => ({ currentStateReward: state.bottomSheetDailyLoginRewards.model }),
        shallowEqual
    );
    var { claimRewardResponse } = currentStateReward

    // addLog("dailyLoginRewardsResponse===>", dailyLoginRewardsResponse);

    // This function should return the Daily Login Rewards Status API Response
    useEffect(() => {
        if (dailyLoginRewardsStatusCheckResponse) {

            // setDay(3)
            // openDailyRewardsPopup(true);
            // return

            setLoading(false)
            //     addLog("dailyLoginRewardsStatusCheckResponse===>", dailyLoginRewardsStatusCheckResponse);
            if (dailyLoginRewardsStatusCheckResponse.item) {
                //   setDailyLoginRewardsStatus(dailyLoginRewardsStatusCheckResponse.item)

                setCurrentDay(dailyLoginRewardsStatusCheckResponse.item.currentDay);
                callToDailyLoginRewardsListAPI(dailyLoginRewardsStatusCheckResponse.item.currentDay);
            }
        }
        dailyLoginRewardsStatusCheckResponse = undefined
    }, [dailyLoginRewardsStatusCheckResponse])
    // This function should return the Daily Login Rewards API Response
    useEffect(() => {
        if (dailyLoginRewardsResponse) {
            setLoading(false)

            if (dailyLoginRewardsResponse.count == 0) {
                setTotalRecords(true)
            }

            if (dailyLoginRewardsResponse.list && dailyLoginRewardsResponse.list != 0) {
                setIsFooterLoading(dailyLoginRewardsResponse.list.length == 0)
                // var tempArr = dailyLoginRewardsResponse.list.map(item => {
                //     let tempItem = { ...item }
                //     return tempItem
                // })
                var tempArr = dailyLoginRewardsResponse.list;

                // setDailyLoginRewards([...dailyLoginRewards, ...tempArr])
                setDailyLoginRewards(dailyLoginRewardsResponse.list);

                for (let i = 0; i < dailyLoginRewardsResponse.list.length; i++) {
                    if (dailyLoginRewardsResponse.list[i].amountType == '1') {
                        gamerjiPoints += dailyLoginRewardsResponse.list[i].amount
                    }
                    if (dailyLoginRewardsResponse.list[i].amountType == '2') {
                        bonus += dailyLoginRewardsResponse.list[i].amount
                    }
                    if (dailyLoginRewardsResponse.list[i].amountType == '3') {
                        rewardDeposit += dailyLoginRewardsResponse.list[i].amount
                    }
                }
                setTotalGamerjiPoints(gamerjiPoints);
                setTotalBonusAmount(bonus);
                setTotalRewardDeposit(rewardDeposit);
            }
        }
        dailyLoginRewardsResponse = undefined
    }, [dailyLoginRewardsResponse])


    // This function should be redirected to the View All Daily Login Rewards Screen 

    const getCurrentDayItem = () => {

        let index = dailyLoginRewards.findIndex(element => {
            if (element.day == currentDay) {
                return true
            }
        })

        if (index > -1) {
            return dailyLoginRewards[index]
        }
    }

    // Claim Reward
    const claimReward = () => {
        const rewardToClaim = getCurrentDayItem()

        if (rewardToClaim) {
            setLoading(true)
            dispatch(actionCreators.requestClaimReward({ dailyReward: rewardToClaim._id }))
        }
    }

    useEffect(() => {
        if (claimRewardResponse) {

            setLoading(false)
            if (claimRewardResponse.item) {

                let tempIndex = dailyLoginRewards.findIndex(element => {
                    if (element.day == currentDay) {
                        return true
                    }
                })

                var tempDailyLoginRewards = []
                dailyLoginRewards.forEach((item, index) => {
                    var tempItem = { ...item }
                    if (index == tempIndex) {
                        tempItem.isClaimed = true
                    }
                    tempDailyLoginRewards.push(tempItem)
                })

                setDailyLoginRewards(tempDailyLoginRewards)

            } else if (claimRewardResponse.errors) {
                if (claimRewardResponse.errors[0] && claimRewardResponse.errors[0].msg) {
                    showErrorToastMessage(claimRewardResponse.errors[0].msg)
                }
            }
        }
    }, [claimRewardResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    let openDailyLoginAvatarPopup = (item) => {
        addLog('openHtmlGamePopup', item)
        setDailyLoginRewardItem(item);
        setOpenDailyLoginAvatarPopup(true);
    }
    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Daily Login Rewards Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_daily_login_rewards} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {!totalRecords ?
                        <View>
                            {/* Wrapper - Statastics */}
                            <View style={{ flexDirection: 'row', borderTopLeftRadius: 50, borderTopRightRadius: 50, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, marginTop: 20, backgroundColor: colors.light_green, justifyContent: 'center' }}>



                                {/* Wrapper - Bonus */}

                            </View>
                            <View style={{ position: "relative", height: getCurrentDayItem() && getCurrentDayItem().isClaimed && getCurrentDayItem().isClaimed == true ? "97%" : "90%" }}>
                                <ScrollView style={marginStyles.bottomMargin_10}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyboardShouldPersistTaps='always'>
                                    {/* Wrapper - Rewards Data */}
                                    <FlatList style={[marginStyles.topMargin_01]}
                                        data={dailyLoginRewards}
                                        renderItem={({ item, index }) => <DailyLoginRewardsItem item={item} currentDay={currentDay} setOpenDailyLoginAvatarPopup={setOpenDailyLoginAvatarPopup} openDailyLoginAvatarPopup={openDailyLoginAvatarPopup} />}
                                        keyExtractor={(item) => item.id}
                                        horizontal={false}
                                        showsVerticalScrollIndicator={false}
                                        showsHorizontalScrollIndicator={false}
                                        onEndReachedThreshold={0.1}
                                    />
                                </ScrollView>
                            </View>
                            {/* TouchableOpacity - View More Details Button Click Event */}
                            {getCurrentDayItem() && getCurrentDayItem().isClaimed && getCurrentDayItem().isClaimed == true ? <Text></Text> :

                                <TouchableOpacity style={[heightStyles.height_46, positionStyles.relative, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, { marginBottom: 10 }]}
                                    onPress={() => claimReward()} activeOpacity={0.5}>

                                    {/* Text - Button (View More Details) */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>COLLECT REWARDS</Text>

                                    {/* Image - Right Arrow */}
                                    <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                                </TouchableOpacity>
                            }
                        </View>
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('dailyLoginRewards') &&
                        <SponsorBannerAds screenCode={'dailyLoginRewards'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('dailyLoginRewards')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}

            <Modal
                isVisible={isOpenDailyLoginAvatarPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <DailyLoginAvatarPopup reward={dailyLoginRewardItem} currentDay={currentDay} setOpenDailyLoginAvatarPopup={setOpenDailyLoginAvatarPopup} claimReward={claimReward} />
            </Modal>
        </SafeAreaView>
    )
}

export default DailyLoginRewards;