import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const joinViaInviteCode = (viewModel) => {
    return {
        type: JOIN_VIA_INVITE_CODE,
        model: viewModel
    };
};

export const joinViaInviteCodeFailed = (viewModel) => {
    return {
        type: JOIN_VIA_INVITE_CODE_FAILED,
        model: viewModel
    };
};

export const requestJoinViaInviteCode = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.JOIN_VIA_INVITE_CODE.GET);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_VIA_INVITE_CODE.GET, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(joinViaInviteCode(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(joinViaInviteCodeFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetJoinViaInviteCodeState = () => {
    return {
        type: RESET_JOIN_VIA_INVITE_CODE_STATE,
    };
};

export const JOIN_VIA_INVITE_CODE = 'JOIN_VIA_INVITE_CODE';
export const JOIN_VIA_INVITE_CODE_FAILED = 'JOIN_VIA_INVITE_CODE_FAILED';
export const RESET_JOIN_VIA_INVITE_CODE_STATE = 'RESET_JOIN_VIA_INVITE_CODE_STATE';