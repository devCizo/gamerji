import produce from 'immer';
import * as actionTypes from '../../actions/account/withdrawAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        getProfileResponse: undefined,
        withdrawNowResponse: undefined
    },
    error: false
};

{ /* Request - Get Profile Data For Withdraw */ }
const getProfileDataForWithdrawRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.getProfileResponse = action.model;
    });
};

{ /* Request Failure - Get Profile Data For Withdraw */ }
const getProfileDataForWithdrawRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.getProfileResponse = action.model;
    });
};

{ /* Request - Withdraw Now */ }
const withdrawNowRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.withdrawNowResponse = action.model;
    });
};

{ /* Request Failure - Withdraw Now */ }
const withdrawNowRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.withdrawNowResponse = action.model;
    });
};

{ /* Reset State - Withdraw */ }
const resetWithdrawState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.getProfileResponse = undefined;
        draft.model.withdrawNowResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW:
            return getProfileDataForWithdrawRequest(state, action);
        case actionTypes.REQUEST_GET_PROFILE_DATA_FOR_WITHDRAW_FAILED:
            return getProfileDataForWithdrawRequestFailed(state, action);
        case actionTypes.REQUEST_WITHDRAW_NOW:
            return withdrawNowRequest(state, action);
        case actionTypes.REQUEST_WITHDRAW_NOW_FAILED:
            return withdrawNowRequestFailed(state, action);
        case actionTypes.RESET_WITHDRAW_STATE:
            return resetWithdrawState(state);
        default:
            return state;
    }
};

export default reducer;