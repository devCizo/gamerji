import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const AVATAR_LIST = 'AVATAR_LIST';
export const AVATAR_LIST_FAILED = 'AVATAR_LIST_FAILED';
export const PROFILE_BANNER_LIST = 'PROFILE_BANNER_LIST';
export const PROFILE_BANNER_LIST_FAILED = 'PROFILE_BANNER_LIST_FAILED';
export const REQUEST_EDIT_PROFILE = 'REQUEST_EDIT_PROFILE';
export const REQUEST_EDIT_PROFILE_FAILED = 'REQUEST_EDIT_PROFILE_FAILED';
export const RESET_EDIT_PROFILE_STATE = 'RESET_EDIT_PROFILE_STATE'

{ /* Request - Avatar List */ }
export const avatarList = (viewModel) => {
    return {
        type: AVATAR_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Avatar List */ }
export const avatarListFailed = (viewModel) => {
    return {
        type: AVATAR_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Profile Banner List */ }
export const profileBannerList = (viewModel) => {
    return {
        type: PROFILE_BANNER_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Profile Banner List */ }
export const profileBannerListFailed = (viewModel) => {
    return {
        type: PROFILE_BANNER_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Edit Profile */ }
export const editProfileRequest = (viewModel) => {
    return {
        type: REQUEST_EDIT_PROFILE,
        model: viewModel
    };
};

{ /* Request Failure - Edit Profile */ }
export const editProfileRequestFailed = (viewModel) => {
    return {
        type: REQUEST_EDIT_PROFILE_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Edit Profile */ }
export const resetEditProfileState = () => {
    return {
        type: RESET_EDIT_PROFILE_STATE,
    };
};

{ /* Request - Avatars */ }
export const requestAvatars = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_FREE_AVATAR_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(avatarList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(avatarListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Profile Banners */ }
export const requestProfileBanners = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_BANNER_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(profileBannerList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(profileBannerListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Edit Profile */ }
export const requestEditProfile = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_EDIT_PROFILE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(editProfileRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(editProfileRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};