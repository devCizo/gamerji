import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView, BackHandler, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import { ProgressBar } from 'react-native-paper';
import { addLog, checkIsSponsorAdsEnabled, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import ContestCommonDetail from '../JoinContest/ContestCommonDetail'
import TournamentCommonDetail from '../joinTournament/tournamentCommonDetail'
import { Constants } from '../../appUtils/constants';
import CustomProgressbar from '../../appUtils/customProgressBar';
import moment from 'moment';
import Modal from 'react-native-modal';
import WiningPrizePool from '../JoinContest/WiningPrizePool'
import TournamentRulesPopup from '../joinTournament/tournamentRulesPopup'
import Clipboard from '@react-native-clipboard/clipboard';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const MyContests = (props) => {

    const dispatch = useDispatch();
    let game = props.route.params.game;

    const [selectedTopTab, setSelectedTopTab] = useState('contests')
    const [selectedDetailTypeTab, setSelectedDetailTypeTab] = useState('joined')

    const [myContests, setMyContests] = useState([])
    const [contestFetchingStatus, setContestFetchingStatus] = useState(false);

    const [myTournaments, setMyTournaments] = useState([])
    const [tournamentFetchingStatus, setTournamentFetchingStatus] = useState(false);

    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);


    //Fetch tournaments
    useEffect(() => {

        setLoading(true)
        fetchContestTournaments(selectedTopTab, selectedDetailTypeTab)

        return () => {
            dispatch(actionCreators.resetMyContestTournamentState())
        }
    }, [])

    let selectTopTab = (value) => {
        setSelectedTopTab(value)

        if ((value == 'contests' && myContests.length == 0) || (value == 'tournament' && myTournaments.length == 0)) {
            fetchContestTournaments(value, selectedDetailTypeTab)
        }
    }
    let selectDetailTypeTab = (value) => {
        setSelectedDetailTypeTab(value)
        fetchContestTournaments(selectedTopTab, value)

        if (selectedTopTab == 'contests') {
            setMyContests([])
        } else if (selectedTopTab == 'tournament') {
            setMyTournaments([])
        }
    }

    let fetchContestTournaments = (value1, value2, skip) => {
        var payload = {
            "game": game._id,
            "limit": 100,
            "skip": skip
        }

        if (value2 == 'joined') {
            payload['type'] = 'joined'
        } else if (value2 == 'completed') {
            payload['type'] = 'completed'
        }

        if (value1 == 'contests') {

            setContestFetchingStatus(true)
            dispatch(actionCreators.requestMyContestList(payload))

        } else if (value1 == 'tournament') {

            setTournamentFetchingStatus(true)
            dispatch(actionCreators.requestMyTournamentList(payload))
        }
    }

    const onRefresh = useCallback(async () => {
        setRefreshing(true)

        if (selectedTopTab == 'contests') {
            setMyContests([])
        } else {
            setMyTournaments([])
        }
        fetchContestTournaments(selectedTopTab, selectedDetailTypeTab)

    }, [refreshing]);

    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.myContests.model }),
        shallowEqual
    );
    var { myContestListResponse, myTournamentListResponse } = currentState

    //myContestListResponse
    useEffect(() => {
        if (myContestListResponse) {
            setLoading(false)
            setRefreshing(false)
            if (myContestListResponse.list) {
                setContestFetchingStatus(myContestListResponse.list.length == 0)
                setMyContests([...myContests, ...myContestListResponse.list])
            }
            myContestListResponse = undefined
        }
    }, [myContestListResponse])

    //myTournamentListResponse
    useEffect(() => {
        if (myTournamentListResponse) {
            setLoading(false)
            setRefreshing(false)
            if (myTournamentListResponse.list) {
                setTournamentFetchingStatus(myTournamentListResponse.list.length == 0)
                setMyTournaments([...myTournaments, ...myTournamentListResponse.list])
            }
            myTournamentListResponse = undefined
        }
    }, [myTournamentListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (

        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50 }}>
                    {/* Back Button */}
                    {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                        <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                    </TouchableOpacity>}

                    {/* Navigation Title */}
                    <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >{game.name} Matches</Text>
                </View>

                {/* Top Tabs */}
                <View style={{ flex: 1, flexDirection: 'row' }} >

                    {/* Contests Tab */}
                    <View style={{ width: '50%' }}>
                        <TouchableOpacity style={{ marginLeft: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} onPress={() => selectTopTab('contests')} activeOpacity={0.5} >

                            <Text style={{ color: 'white', fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >CONTESTS</Text>
                            {selectedTopTab == 'contests' &&
                                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 4, borderRadius: 2, backgroundColor: colors.yellow }} />
                            }
                        </TouchableOpacity>
                    </View>

                    {/* Tournament Tab */}
                    <View style={{ width: '50%' }}>
                        <TouchableOpacity style={{ marginRight: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} onPress={() => selectTopTab('tournament')} activeOpacity={0.5} >
                            <Text style={{ color: 'white', fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >TOURNAMENTS</Text>
                            {selectedTopTab == 'tournament' &&
                                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 4, borderRadius: 2, backgroundColor: colors.yellow }} />
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>
                {/* Tabs View */}
                <View style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 34, flexDirection: 'row', overflow: 'hidden', borderRadius: 17, borderWidth: 1, borderColor: '#D5D7E3' }} >

                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: selectedDetailTypeTab == 'joined' ? colors.black : 'transparent' }} onPress={() => selectDetailTypeTab('joined')} activeOpacity={0.5} >
                        <Text style={{ color: selectedDetailTypeTab == 'joined' ? 'white' : colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Joined</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: selectedDetailTypeTab == 'completed' ? colors.black : 'transparent' }} onPress={() => selectDetailTypeTab('completed')} activeOpacity={0.5} >
                        <Text style={{ color: selectedDetailTypeTab == 'completed' ? 'white' : colors.black, fontSize: 13, fontFamily: fontFamilyStyleNew.semiBold }} >Completed</Text>
                    </TouchableOpacity>

                </View>

                <FlatList style={{ flex: 1, marginTop: 18, marginLeft: 10, marginRight: 10 }}
                    data={selectedTopTab == 'contests' ? myContests : myTournaments}
                    // data={dataaa}
                    renderItem={({ item, index }) => selectedTopTab == 'contests' ? <MyContestItems item={item} /> : <MyTournamentItems item={item} index={index} />}
                    keyExtractor={(item, index) => index.toString()}
                    onEndReachedThreshold={0.1}
                    onEndReached={() => {
                        addLog('onEndReached')
                        if (selectedTopTab == 'contests' && !contestFetchingStatus) {
                            fetchContestTournaments(selectedTopTab, selectedDetailTypeTab, myContests.length)
                        }
                        if (selectedTopTab == 'tournament' && !tournamentFetchingStatus) {
                            fetchContestTournaments(selectedTopTab, selectedDetailTypeTab, myTournaments.length)
                        }
                    }}

                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                />

                {checkIsSponsorAdsEnabled('myContests') &&
                    <SponsorBannerAds screenCode={'myContests'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('myContests')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const MyContestItems = (props) => {

    const contest = props.item

    const isRoomIdAvailable = () => {
        if (contest.roomId && contest.roomId != '') {
            return true
        }
        return false
    }

    const isPasswordAvailable = () => {
        if (contest.roomPassword && contest.roomPassword != '') {
            return true
        }
        return false
    }

    const copyRoomIdToClipboard = () => {
        if (isRoomIdAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contest.roomId)
        }
    }

    const copyPasswordToClipboard = () => {
        if (isPasswordAvailable()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(contest.roomPassword)
        }
    }

    const displayColumn = () => {
        if (contest && contest.column && contest.column != '') {
            return contest.column
        }
    }

    const getRoomIdPasswordWidth = () => {
        if (displayColumn()) {
            return '38%'
        }
        return '50%'
    }

    return (
        <TouchableOpacity onPress={() => RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })} activeOpacity={0.9} >
            <ContestCommonDetail item={contest} isFromMyContest={true} />

            {/* Bottom View */}
            <View style={{ position: 'absolute', height: 44, left: 18, right: 18, bottom: 12, backgroundColor: colors.red, borderTopLeftRadius: 5, borderTopRightRadius: 5, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, justifyContent: 'space-between', flexDirection: 'row' }} >

                {/* Room ID */}
                <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} activeOpacity={1} onPress={() => copyRoomIdToClipboard()} >

                    <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Room ID</Text>

                    <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                        <View style={{ marginLeft: 10 }}>
                            <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1}>{isRoomIdAvailable() ? contest.roomId : '-'}</Text>
                            {/* <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>asdfasdfasdfasdfasdfasdfasdfasdf</Text> */}
                        </View>
                        {isRoomIdAvailable() &&
                            <Image style={{ marginLeft: 4, marginRight: 10, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                        }
                    </View>
                </TouchableOpacity>

                {/* Password */}
                <TouchableOpacity style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} activeOpacity={1} onPress={() => copyPasswordToClipboard()} >

                    <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Password</Text>

                    <View style={{ marginTop: 2, flexDirection: 'row', alignItems: 'center' }} >

                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{isPasswordAvailable() ? contest.roomPassword : '-'}</Text>
                        {isPasswordAvailable() &&
                            <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                        }
                    </View>

                    <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, left: 0, bottom: 8, width: 1 }} />
                </TouchableOpacity>

                {/* Slot */}
                {displayColumn() &&
                    <View style={{ flex: 1, justifyContent: 'center' }} >
                        <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, left: 0, bottom: 8, width: 1 }} />

                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Slot</Text>
                        <Text style={{ marginTop: 2, marginLeft: 10, height: 14, width: 30, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold, backgroundColor: colors.black, borderRadius: 3, textAlign: 'center', overflow: 'hidden' }}>{displayColumn()}</Text>
                    </View>
                }
            </View>

        </TouchableOpacity>

    )
}

const MyTournamentItems = (props) => {

    var tournament = props.item
    const [contest, setContest] = useState(undefined)

    const [isVisibleWinningPrizePoolPopup, setWinningPrizePoolPopup] = useState(false)
    const [isVisibleTournamentRulesPopup, setTournamentRulesOpen] = useState(false)

    let closeWiningPrizePool = () => {
        setWinningPrizePoolPopup(false)
    }

    let getProgressValue = () => {
        if (tournament.totalJoinedPlayers && tournament.totalPlayers) {
            return tournament.totalJoinedPlayers / tournament.totalPlayers
        } else {
            return 0
        }
    }

    let getTitlesSubViewWidth = () => {

        let counts = findEnabledTitles().length
        if (counts >= 3) {
            return '33.33%'
        }
        if (counts >= 2) {
            return '50%'
        }
        return '100%'
    }

    let findEnabledTitles = () => {
        if (tournament.titles) {
            let filtered = tournament.titles.filter(item => {
                if (item.isSelection && item.isSelection == true) {
                    return item
                }
            })
            return filtered ? filtered : []
        }
        return []
    }

    let findTitleKeyAndValue = (index, type) => {

        let titles = findEnabledTitles()

        if (titles.length >= index + 1) {
            if (type == 'title') { return titles[index].name }
            if (type == 'value') { return titles[index].value }
        }
    }

    let totalRemainingPlayers = () => {
        let players = tournament.totalPlayers - tournament.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' remaining'
    }

    let totalJoinedPlayersPlayers = () => {
        let players = tournament.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' joined'
    }

    const getLastContest = () => {
        if (tournament.contestsList.length > 0) {
            // return tournament.contestsList[tournament.contestsList.length - 1]
            return tournament.contestsList[0]
        }
        return undefined
    }

    const getRoundTitle = () => {
        let contest = getLastContest()
        if (contest) {
            var title = ''
            var date = ''
            var time = ''

            if (contest.title) {
                title = contest.title
            }
            if (contest.date) {
                date = moment(contest.date).format('DD/MM/yyyy')
            }
            if (contest.time) {
                time = moment(contest.time).format('hh:mm A')
            }
            return title + " - " + date + " - " + time
        }
    }

    const getRoomId = () => {
        let contest = getLastContest()
        if (contest) {
            if (contest.roomId && contest.roomId != '') {
                return contest.roomId
            }
        }
    }

    const getRoomPassword = () => {
        let contest = getLastContest()
        if (contest) {
            if (contest.roomPassword && contest.roomPassword != '') {
                return contest.roomPassword
            }
        }
    }

    const copyRoomIdToClipboard = () => {
        if (getRoomId()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(getRoomId())
        }
    }

    const copyPasswordToClipboard = () => {
        if (getRoomPassword()) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(getRoomPassword())
        }
    }

    const getCurrencyImage = () => {
        return (
            tournament.currency && tournament.currency.code == 'coin' ?
                <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={tournament.currency && tournament.currency.img && tournament.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.currency.img.default }} />
                :
                <Text style={{ fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.currency.symbol}</Text>
        )
    }

    const displayColumn = () => {
        let contest = getLastContest()
        if (contest.roomId && contest.roomId != "") {
            // addLog("contest.contests==roomId===>", contest.contests)

        }


        if (contest && contest.column && contest.column != '') {
            return contest.column
        }
    }

    const getRoomIdPasswordWidth = () => {
        if (displayColumn()) {
            return '38%'
        }
        return '50%'
    }

    return (
        <TouchableOpacity style={{}} onPress={() => RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })} activeOpacity={0.9} >

            <View style={{ flex: 1, alignSelf: 'stretch', marginBottom: 54 }}>

                {/* Title View */}
                <View style={{ height: 40, backgroundColor: colors.black, borderTopLeftRadius: 20, borderTopRightRadius: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>

                    <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', marginLeft: 8, marginRight: 8 }}>
                        {/* <CustomMarquee style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginLeft: 8 }} value={contest.title} /> */}
                        <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, color: colors.white }} value={tournament.title} />
                    </View>
                    {/* <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginLeft: 8, flex: 1, alignSelf: 'center', color: colors.white }} numberOfLines={1} ellipsizeMode='tail' >{tournament.title}</Text> */}


                    {/* Title View */}
                    <TouchableOpacity style={{ height: 24, width: 50, backgroundColor: colors.yellow, marginRight: 0, borderTopRightRadius: 12, borderBottomLeftRadius: 12, borderBottomRightRadius: 12, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }} onPress={() => setTournamentRulesOpen(true)} activeOpacity={0.5} >
                        <Image style={{ width: 14, height: 14, marginRight: 5 }} source={require('../../assets/images/rules_eye_icon_blue.png')} ></Image>
                        <Text style={{ color: colors.black, textAlign: 'center', fontSize: 10, fontFamily: fontFamilyStyleNew.semiBold }} >Rules</Text>
                    </TouchableOpacity>
                </View>

                {/* Center Content */}
                <View style={{ height: 170, backgroundColor: colors.black, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }} >

                    <View style={{ marginLeft: 108, height: 100 }}>

                        <View style={{ height: 36, flexDirection: 'row', alignItems: 'center' }} >

                            {/* Date */}
                            <View style={{ flex: 0.6 }} >
                                <Text style={{ marginLeft: 12, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Date</Text>
                                <Text style={{ marginLeft: 12, marginTop: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.startDate && moment(tournament.startDate).format('DD/MM/yyyy')}</Text>
                            </View>

                            {/* Rounds */}
                            <View style={{ flex: 0.5 }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Rounds</Text>
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.rounds && tournament.rounds.length}</Text>
                                <Image style={{ position: 'absolute', top: 3, left: 0, bottom: 3, width: 1, backgroundColor: colors.white }} />
                            </View>

                            {/* ID */}
                            <View style={{ width: 74 }} >
                                <Image style={{ position: 'absolute', top: 3, left: 0, bottom: 3, width: 1, backgroundColor: colors.white }} />
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Game#</Text>
                                <Text style={{ marginLeft: 8, marginTop: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.shortCode}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 5, height: 36, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, flexDirection: 'row' }} >

                            {/* Title1 */}
                            {findEnabledTitles().length >= 1 &&
                                <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                    <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(0, 'title')}</Text>
                                    <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(0, 'value')}</Text>
                                </View>
                            }
                            {/* Title2 */}
                            {findEnabledTitles().length >= 2 &&
                                <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                    <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(1, 'title')}</Text>
                                    <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(1, 'value')}</Text>
                                    <Image style={{ position: 'absolute', top: 5, left: 0, bottom: 5, width: 1, backgroundColor: colors.black, opacity: 0.4 }} />
                                </View>
                            }
                            {/* Round */}
                            {findEnabledTitles().length >= 3 &&
                                <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                    <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(2, 'title')}</Text>
                                    <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(2, 'value')}</Text>
                                    <Image style={{ position: 'absolute', top: 5, left: 0, bottom: 5, width: 1, backgroundColor: colors.black, opacity: 0.4 }} />
                                </View>
                            }
                        </View>

                        <View style={{ marginLeft: 5, height: 36, flexDirection: 'row' }} >

                            {/* Winning */}
                            <View style={{ height: 36, width: '33.33%', justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Prize Pool</Text>

                                <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                    {getCurrencyImage()}
                                    <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.prizePool}</Text>
                                </View>

                                <Image style={{ position: 'absolute', top: 5, right: 0, bottom: 5, width: 1, backgroundColor: colors.white }} />
                            </View>

                            {/* Winners */}
                            <TouchableOpacity style={{ height: 36, width: '33.33%', justifyContent: 'center' }} onPress={() => setWinningPrizePoolPopup(true)} activeOpacity={0.5} >
                                <Text style={{ marginLeft: 6, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Winners</Text>

                                <View style={{ marginLeft: 6, flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.totalWinners}</Text>
                                    <Image style={{ marginLeft: 3, width: 7, height: 10, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                                </View>

                                <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                            </TouchableOpacity>

                            {/* Entry Fee */}
                            <View style={{ height: 36, width: '33.33%', justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Entry Fee</Text>
                                {/* <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.entryFee != '0' ? '₹' + tournament.entryFee : '-'}</Text> */}

                                {tournament.entryFee == '0' ?
                                    <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >Free</Text>
                                    :
                                    <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                        {getCurrencyImage()}
                                        <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.entryFee}</Text>
                                    </View>
                                }
                            </View>
                        </View>

                        <View>
                            {/* Spots Progress Indicator */}
                            <View style={{ marginLeft: 12, marginRight: 12, height: 7, flex: 1 }} >
                                <ProgressBar style={{ marginTop: 4, height: 5, width: '100%', backgroundColor: colors.white, borderRadius: 2.5 }} progress={getProgressValue()} color={colors.yellow} />
                            </View>

                            {/* Spots Available View */}
                            <View style={{ marginLeft: 12, marginTop: 12, marginRight: 12, justifyContent: 'space-between', flexDirection: 'row' }} >
                                {/* Spots Available */}
                                <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalRemainingPlayers()}</Text>

                                {/* Spots Available Left */}
                                <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalJoinedPlayersPlayers()}</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </View>

            {/* Tournament Image */}
            <View style={{ position: 'absolute', top: 40, left: 8, width: 100, height: 130, borderRadius: 5, overflow: 'hidden' }} >

                <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, width: 100, height: 130 }} source={tournament.featuredImage && tournament.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.featuredImage.default }} />
                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, width: 100, height: 70 }} source={require('../../assets/images/tournament_image_layer.png')} />
                <Text style={{ position: 'absolute', left: 0, right: 0, bottom: 6, color: 'white', textAlign: 'center', fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} >{tournament.gameType && tournament.gameType.name}</Text>

            </View>

            {/* Bottom View */}
            <View style={{ position: 'absolute', height: 74, left: 18, right: 18, bottom: 10, backgroundColor: colors.red, borderTopLeftRadius: 5, borderTopRightRadius: 5, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, }} >

                <View style={{ height: 30, justifyContent: 'space-between', flexDirection: 'row' }} >

                    {/* Round Detail */}
                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
                        <Text style={{ marginLeft: 4, marginRight: 4, fontSize: 13, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{getRoundTitle()}</Text>
                    </View>

                    <Image style={{ position: 'absolute', backgroundColor: colors.white, left: 0, right: 0, bottom: 0, height: 1 }} />
                </View>

                <View style={{ height: 44, justifyContent: 'space-between', flexDirection: 'row' }} >

                    {/* Room ID */}
                    <View style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} >

                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Room ID</Text>

                        <TouchableOpacity style={{ marginTop: 2, flexDirection: 'row' }} onPress={() => copyRoomIdToClipboard()} activeOpacity={1} >
                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{getRoomId() ? getRoomId() : '-'}</Text>
                            {getRoomId() &&
                                <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                            }
                        </TouchableOpacity>

                        <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, bottom: 8, right: 0, width: 1 }} />
                    </View>

                    {/* Password */}
                    <View style={{ width: getRoomIdPasswordWidth(), justifyContent: 'center' }} >
                        <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Password</Text>

                        <TouchableOpacity style={{ marginTop: 2, flexDirection: 'row' }} onPress={() => copyPasswordToClipboard()} activeOpacity={1} >
                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold }}>{getRoomPassword() ? getRoomPassword() : '-'}</Text>
                            {getRoomPassword() &&
                                <Image style={{ marginLeft: 4, width: 13, height: 13, resizeMode: 'contain' }} source={require('../../assets/images/copy_icon.png')} />
                            }
                        </TouchableOpacity>
                        <Image style={{ position: 'absolute', backgroundColor: 'white', top: 8, bottom: 8, right: 0, width: 1 }} />
                    </View>

                    {/* Slot */}
                    {displayColumn() &&
                        <View style={{ flex: 1, justifyContent: 'center' }} >
                            <Text style={{ marginLeft: 10, color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular, }}>Slot</Text>
                            <Text style={{ marginTop: 2, marginLeft: 10, height: 14, width: 30, color: 'white', fontSize: 13, fontFamily: fontFamilyStyleNew.bold, backgroundColor: colors.black, borderRadius: 3, textAlign: 'center', overflow: 'hidden' }}>{displayColumn()}</Text>
                        </View>
                    }
                </View>
            </View>

            {/* Round Suggetion View */}
            {/* <View style={{ position: 'absolute', height: 30, left: 18, right: 18, bottom: 11, alignItems: 'center' }} >

                <View style={{ backgroundColor: colors.red, height: 20, justifyContent: 'center', borderBottomLeftRadius: 8, borderBottomRightRadius: 8, }} >

                    <Text style={{ marginLeft: 20, marginRight: 20, color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }}>Round 1</Text>
                </View>

            </View> */}

            <Modal isVisible={isVisibleWinningPrizePoolPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <WiningPrizePool contest={tournament} closeWiningPrizePool={closeWiningPrizePool} />
            </Modal>

            <Modal isVisible={isVisibleTournamentRulesPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <TournamentRulesPopup tournament={tournament} setTournamentRulesOpen={setTournamentRulesOpen} />
            </Modal>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    navigationView: {
        height: 90,
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
})

export default MyContests;

var dataaa = [
    {
        id: 0
    }, {
        id: 1
    }
]