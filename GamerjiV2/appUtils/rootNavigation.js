import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Constants } from '../appUtils/constants';
import Splash from '../components/Authentication/Splash';
import Login from '../components/Authentication/Login';
import OTP from '../components/Authentication/OTP';
import BottomNavigation from '../components/BottomTabs/BottomNavigation';
import DailyLoginRewards from '../components/AllGames/DailyLoginRewards';
import GameType from '../components/JoinContest/GameType';
import HtmlGame from '../components/FreeGames/HtmlGame';
import HtmlGameType from '../components/FreeGames/HtmlGameType';
import HtmlGameDetail from '../components/FreeGames/HtmlGameDetail';
import Leaderboard from '../components/More/leaderboard';
import StreamOnGamerji from '../components/More/StreamOnGamerji';
import AddVidoes from '../components/More/AddVidoes';
import Notifications from '../components/More/Notifications';
import ReferAFriend from '../components/More/ReferAFriend';
import ApplyPromoCode from '../components/More/ApplyPromoCode';
import HowToPlay from '../components/More/howToPlay';
import GamerjiPoints from '../components/More/GamerjiPoints';
import CustomerCare from '../components/customerCare/customerCare';
import Legality from '../components/More/Legality';
import Videos from '../components/More/Videos';
import ViewAllVideos from '../components/More/ViewAllVideos';
import Account from '../components/Account/Account';
import AddBalance from '../components/payments/addBalance';
import PaymentOptions from '../components/payments/PaymentOptionsOld';
import PaymentGateway from '../components/payments/paymentGateway';
import MyRecentTransactions from '../components/Account/MyRecentTransactions';
import Withdrawal from '../components/Account/Withdrawal';
import WithdrawalPaytm from '../components/Account/WithdrawalPaytm';
import BankTransfer from '../components/Account/BankTransfer';
import VerifyEmail from '../components/Account/VerifyEmail';
import MobileAndEmailVerification from '../components/Account/MobileAndEmailVerification';
import ChangeUserName from '../components/JoinContest/ChangeUserName';
import ContestList from '../components/JoinContest/ContestList';
import ContestFilter from '../components/JoinContest/ContestFilter';
import JoinContestWalletValidation from '../components/JoinContest/JoinContestWalletValidation';
import DobStateValidation from '../components/JoinContest/DobStateValidation';
import SquadRegistration from '../components/JoinContest/SquadRegistration';
import ContestDetail from '../components/myContests/contestDetail';
import WiningPrizePool from '../components/JoinContest/WiningPrizePool';
import TournamentList from '../components/joinTournament/tournamentList';
import TournamentDetail from '../components/myContests/tournamentDetail';
import MyContests from '../components/myContests/myContests';
import SingleContestToJoin from '../components/JoinContest/singleContestToJoin';
import SingleTournamentToJoin from '../components/joinTournament/singleTournamentToJoin';
import RaiseComplaint from '../components/customerCare/raiseComplaint';
import PurchaseCoins from '../components/Account/coinStore';
import EditProfile from '../components/Profile/EditProfile';
import SearchUserList from '../components/search/searchUserList';
import ViewAllMedals from '../components/Profile/ViewAllMedals';
import ViewAllStats from '../components/Profile/ViewAllStats';
import CollegeDetail from '../components/collegiate/collegeDetail';
import ViewAllFeaturedVideos from '../components/WorldOfEsports/ViewAllFeaturedVideos';
import ViewAllEsportsNews from '../components/WorldOfEsports/ViewAllEsportsNews';
import ViewAllTopProfiles from '../components/WorldOfEsports/ViewAllTopProfiles';
import Chart from '../components/Profile/chart';
import Temp from '../components/Account/Temp';
import UpdateProfileAfterSignUp from '../components/Authentication/updateProfileAfterSignUp';
import EsportsNewsDetails from '../components/WorldOfEsports/esportsNewsDetails';
import InsightsStatsScreen from '../components/Profile/insightsStatsScreen'
import FriendList from '../components/friends/friendList'
import ProfileScreen from '../components/Profile/myProfileNew'
import OtherUserProfile from '../components/Profile/otherUserProfile'
import rewardStore from '../components/Account/rewardStore'
import myRewards from '../components/Account/myRewards'
import ticketDetail from '../components/customerCare/ticketDetail'
import howToJoin from '../components/JoinContest/howToJoin'
import accountNew from '../components/Account/accountNew'
import coinRewardStore from '../components/Account/coinRewardStore'
import paymentOptionsNew from '../components/payments/paymentOptionsNew'
import paymentByCardList from '../components/payments/paymentByCardList'
import netBankingList from '../components/payments/netBankingList'
import paymentGatewayNew from '../components/payments/paymentGatewayNew'

import { addLog } from './commonUtlis';
import DynamicLinkExtractor from '../components/dynamicLink/dynamicLinkExtractor'

const Stack = createStackNavigator();

export const RootNavigation = (props) => {

    return (

        <NavigationContainer ref={navigationRef} >
            <Stack.Navigator screenOptions={{
                headerShown: false
            }}>
                {/* <Stack.Screen name={Constants.nav_splash} component={Splash} /> */}
                <Stack.Screen name={Constants.nav_login} component={Login} />
                <Stack.Screen name={Constants.nav_otp} component={OTP} />
                <Stack.Screen name={Constants.nav_update_profile_after_signUp} component={UpdateProfileAfterSignUp} />
                <Stack.Screen name={Constants.nav_bottom_navigation} component={BottomNavigation} />
                <Stack.Screen name={Constants.nav_daily_login_rewards} component={DailyLoginRewards} />
                <Stack.Screen name={Constants.nav_game_type} component={GameType} />
                <Stack.Screen name={Constants.nav_html_game_type} component={HtmlGameType} />
                <Stack.Screen name={Constants.nav_html_game} component={HtmlGame} />
                <Stack.Screen name={Constants.nav_html_game_detail} component={HtmlGameDetail} />
                <Stack.Screen name={Constants.nav_more_leaderboard} component={Leaderboard} />
                <Stack.Screen name={Constants.nav_more_stream_on_gamerji} component={StreamOnGamerji} />
                <Stack.Screen name={Constants.nav_more_add_videos} component={AddVidoes} />
                <Stack.Screen name={Constants.nav_more_notifications} component={Notifications} />
                <Stack.Screen name={Constants.nav_more_refer_a_friend} component={ReferAFriend} />
                <Stack.Screen name={Constants.nav_more_apply_promo_code} component={ApplyPromoCode} />
                <Stack.Screen name={Constants.nav_more_how_to_play} component={HowToPlay} />
                <Stack.Screen name={Constants.nav_more_gamerji_points} component={GamerjiPoints} />
                <Stack.Screen name={Constants.nav_more_customer_care} component={CustomerCare} />
                <Stack.Screen name={Constants.nav_more_legality} component={Legality} />
                <Stack.Screen name={Constants.nav_more_videos} component={Videos} />
                <Stack.Screen name={Constants.nav_view_all_videos} component={ViewAllVideos} />
                <Stack.Screen name={Constants.nav_account} component={Account} />
                <Stack.Screen name={Constants.nav_add_balance} component={AddBalance} />
                <Stack.Screen name={Constants.nav_payment_options} component={PaymentOptions} />
                <Stack.Screen name={Constants.nav_payment_gateway} component={PaymentGateway} />
                <Stack.Screen name={Constants.nav_my_recent_transactions} component={MyRecentTransactions} />
                <Stack.Screen name={Constants.nav_withdrawal} component={Withdrawal} />
                <Stack.Screen name={Constants.nav_withdrawal_paytm} component={WithdrawalPaytm} />
                <Stack.Screen name={Constants.nav_bank_transfer} component={BankTransfer} />
                <Stack.Screen name={Constants.nav_verify_email} component={VerifyEmail} />
                <Stack.Screen name={Constants.nav_mobile_add_email_verification} component={MobileAndEmailVerification} />
                <Stack.Screen name={Constants.nav_change_user_name} component={ChangeUserName} />
                <Stack.Screen name={Constants.nav_contest_list} component={ContestList} />
                <Stack.Screen name={Constants.nav_contest_filter} component={ContestFilter} />
                <Stack.Screen name={Constants.nav_join_contest_wallet_validation} component={JoinContestWalletValidation} />
                <Stack.Screen name={Constants.nav_dob_state_validation} component={DobStateValidation} />
                <Stack.Screen name={Constants.nav_squad_registration} component={SquadRegistration} />
                <Stack.Screen name={Constants.nav_contest_detail} component={ContestDetail} />
                <Stack.Screen name={Constants.nav_wining_prize_pool} component={WiningPrizePool} />
                <Stack.Screen name={Constants.nav_tournament_list} component={TournamentList} />
                <Stack.Screen name={Constants.nav_tournament_detail} component={TournamentDetail} />
                <Stack.Screen name={Constants.nav_my_contests} component={MyContests} />
                <Stack.Screen name={Constants.nav_single_contest_to_join} component={SingleContestToJoin} />
                <Stack.Screen name={Constants.nav_single_tournament_to_join} component={SingleTournamentToJoin} />
                <Stack.Screen name={Constants.nav_raise_complaint} component={RaiseComplaint} />
                <Stack.Screen name={Constants.nav_coin_store} component={PurchaseCoins} />
                <Stack.Screen name={Constants.nav_edit_profile} component={EditProfile} />
                <Stack.Screen name={Constants.nav_search_user} component={SearchUserList} />
                <Stack.Screen name={Constants.nav_view_all_medals} component={ViewAllMedals} />
                <Stack.Screen name={Constants.nav_view_all_stats} component={ViewAllStats} />
                <Stack.Screen name={Constants.nav_college_detail} component={CollegeDetail} />
                <Stack.Screen name={Constants.nav_view_all_featured_videos} component={ViewAllFeaturedVideos} />
                <Stack.Screen name={Constants.nav_view_all_esports_news} component={ViewAllEsportsNews} />
                <Stack.Screen name={Constants.nav_view_all_top_profiles} component={ViewAllTopProfiles} />
                <Stack.Screen name={Constants.nav_chart} component={Chart} />
                <Stack.Screen name={Constants.nav_esports_news_details} component={EsportsNewsDetails} />
                <Stack.Screen name={Constants.nav_insights_stats_screen} component={InsightsStatsScreen} />
                <Stack.Screen name={Constants.nav_friend_list} component={FriendList} />
                <Stack.Screen name={Constants.nav_profile_screen} component={ProfileScreen} />
                <Stack.Screen name={Constants.nav_other_user_profile} component={OtherUserProfile} />
                <Stack.Screen name={Constants.nav_reward_store} component={rewardStore} />
                <Stack.Screen name={Constants.nav_my_rewards} component={myRewards} />
                <Stack.Screen name={Constants.nav_ticket_detail} component={ticketDetail} />
                <Stack.Screen name={Constants.nav_how_to_join} component={howToJoin} />
                <Stack.Screen name={Constants.nav_account_new} component={accountNew} />
                <Stack.Screen name={Constants.nav_coin_reward_store} component={coinRewardStore} />
                <Stack.Screen name={Constants.nav_payment_options_new} component={paymentOptionsNew} />
                <Stack.Screen name={Constants.nav_payment_by_card_list} component={paymentByCardList} />
                <Stack.Screen name={Constants.nav_net_banking_list} component={netBankingList} />
                <Stack.Screen name={Constants.nav_payment_gateway_new} component={paymentGatewayNew} />

            </Stack.Navigator>
        </NavigationContainer>
    );
}

export const navigationRef = React.createRef();

export function navigate(name, params) {
    navigationRef.current?.navigate(name, params);
}

export function goBack() {
    navigationRef.current?.goBack()
}

export function pushScreen(name, params) {
    navigationRef.current?.push(name);
}

export function popScreen() {
    navigationRef.current?.pop()
}