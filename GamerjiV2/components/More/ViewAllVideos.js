import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList, Linking } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import ViewAllVideosItem from '../../componentsItem/ViewAllVideosItem';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const ViewAllVideos = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [videoChannelData, setVideoChannelData] = useState(false);
    const [viewAllVideos, setViewAllVideos] = useState([]);
    const [totalCounts, setTotalCounts] = useState(undefined);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    const [youTubeChannelLink, setyouTubeChannelLink] = useState(youTubeChannelLink);
    var pageLimit = 10
    const isFromMyVideos = props.route.params.isFromMyVideos ? props.route.params.isFromMyVideos : false

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToViewAllVideosListAPI();

        return () => {
            dispatch(actionCreators.resetViewAllVideosState())
        }
    }, [])

    // This Use-Effect function should call the View All Videos API
    const callToViewAllVideosListAPI = () => {
        const ytch = require('yt-channel-info');

        addLog('Youtube Link 9090:::> ', props)
        var url = props.route.params.youTubeChannelLink
        const myArr = url.split("/")

        var channelId = myArr[myArr.length - 1];
        addLog('Channel Id:::> ', channelId)
        let channelIdType = "";

        ytch.getChannelInfo(channelId, channelIdType).then((response) => {
            addLog('Response:::> ', response)
            setVideoChannelData(response)
            const sortBy = 'newest'

            ytch.getChannelVideos(channelId, sortBy, channelIdType).then((responseVideos) => {
                setLoading(false)
                addLog('Response Videos:::> ', responseVideos)
                addLog('Total Videos:::> ', responseVideos.items.length)
                setTotalCounts(responseVideos.items.length)

                if (responseVideos) {
                    setViewAllVideos(responseVideos.items)
                }


            }).catch((err) => {
                console.log(err)
            })
        }).catch((err) => {
            console.log(err)
        })
    }

    // const callToViewAllVideosListAPI = () => {
    //     let payload = {
    //         skip: viewAllVideos.length,
    //         limit: pageLimit
    //     }
    //     setIsFooterLoading(true)
    //     dispatch(actionCreators.requestViewAllVideos(payload))
    // }

    // This function sets the View All Videos API Response to the current state
    // const { currentState } = useSelector(
    //     (state) => ({ currentState: state.viewAllVideos.model }),
    //     shallowEqual
    // );
    // var { viewAllVideosResponse } = currentState

    // This function checks the response and then sets the data to the UI
    // useEffect(() => {
    //     if (viewAllVideosResponse) {
    //         setLoading(false)

    //         setTotalCounts(viewAllVideosResponse.count)
    //         if (viewAllVideosResponse.count == 0) {
    //             setTotalRecords(true)
    //         }

    //         if (viewAllVideosResponse.list) {
    //             setIsFooterLoading(viewAllVideosResponse.list.length == 0)
    //             var tempArr = viewAllVideosResponse.list.map(item => {
    //                 let tempItem = { ...item }
    //                 return tempItem
    //             })
    //             setViewAllVideos([...viewAllVideos, ...tempArr])
    //         }
    //     }
    //     viewAllVideosResponse = undefined
    // }, [viewAllVideosResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Videos Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {isFromMyVideos ? Constants.header_my_videos : Constants.header_videos} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    <View style={[flexDirectionStyles.row, justifyContentStyles.spaceBetween]}>

                        {/* Text - Channel Name */}
                        <Text style={[widthStyles.width_70p, fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_18]}>{videoChannelData && videoChannelData.author}</Text>

                        {/* TouchableOpacity - Subscribe Button Click Event*/}
                        <TouchableOpacity style={[heightStyles.height_30_new, widthStyles.width_75, borderStyles.borderRadius_20, alignmentStyles.alignItemsCenter, shadowStyles.shadowBackground, justifyContentStyles.center, colorStyles.primaryBackgroundColor, marginStyles.bottomMargin_5]}
                            onPress={() => { Linking.openURL(videoChannelData && videoChannelData.authorUrl) }} activeOpacity={0.5}>

                            {/* Text - Subscribe */}
                            <Text style={[fontFamilyStyles.regular, colorStyles.whiteColor, fontSizeStyles.fontSize_9, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.text_subsciribe}</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Text - Total Videos */}
                    <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_16]}>{videoChannelData && videoChannelData.subscriberText}</Text>

                    {/* <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_16]}>{totalCounts} {totalCounts > 1 ? Constants.text_videos : Constants.text_video}</Text> */}

                    {/* Wrapper - View All Video Lists */}
                    {!totalRecords ?
                        <FlatList style={marginStyles.topMargin_15}
                            data={viewAllVideos}
                            renderItem={({ item, index }) => <ViewAllVideosItem item={item} index={index} navigation={props.navigation} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    callToViewAllVideosListAPI()
                                }
                            }} />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('viewAllVideos') &&
                        <SponsorBannerAds screenCode={'viewAllVideos'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('viewAllVideos')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView >
    )
}

export default ViewAllVideos;