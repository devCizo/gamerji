import React, { useRef } from 'react'
import { View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import RBSheet from "react-native-raw-bottom-sheet";
const { height, width } = Dimensions.get('window');

const CustomBottomSheet = (props) => {

    // Variable Declarations
    const refRBSheet = useRef('');

    return (
        <View>

            {/* Raw Bottom Sheet */}
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                openDuration={350}
                customStyles={{
                    draggableIcon: {
                        backgroundColor: colors.black
                    },
                    container: {
                        height: 200,
                        backgroundColor: colors.yellow,
                        borderTopEndRadius: width * 0.15,
                        borderTopStartRadius: width * 0.15
                    }
                }}>

                {/* Wrapper - Unlink Paytm Wallet Bottom Sheet */}
                <View style={[containerStyles.container, justifyContentStyles.center]}>

                    {/* Image - Cancel */}
                    {/* <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.top_0, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_cancel.png')}
                         onPress={() => refRBSheet.current.close()} /> */}

                    {/* Text - Do you want to Unlink your PayTM wallet? */}
                    <Text style={[fontFamilyStyles.extraBold, colorStyles.blackColor, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_05, marginStyles.leftMargin_05, marginStyles.rightMargin_05, marginStyles.bottomMargin_06]}>{Constants.text_unlink_paytm_wallet}</Text>

                    {/* Wrapper - Action Buttons */}
                    <View style={[positionStyles.absolute, flexDirectionStyles.row, justifyContentStyles.spaceBetween, alignmentStyles.bottom_0, alignmentStyles.alignSelfCenter, marginStyles.bottomMargin_02]}>

                        {/* TouchableOpacity - Yes Button Click Event */}
                        <TouchableOpacity style={[widthStyles.width_45p, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, alignmentStyles.alignItemsCenter]}>

                            {/* Text - Button (Yes) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, otherStyles.capitalize]}>{Constants.action_yes}</Text>
                        </TouchableOpacity>

                        {/* TouchableOpacity - No Button Click Event */}
                        <TouchableOpacity style={[widthStyles.width_45p, colorStyles.redBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, alignmentStyles.alignItemsCenter]}
                            onPress={() => refRBSheet.current.close()}>

                            {/* Text - Button (No) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, otherStyles.capitalize]}>{Constants.action_no}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </RBSheet>
        </View>
    );
}

export default CustomBottomSheet;



