import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import * as generalSetting from '../webServices/generalSetting'

const MyRecentTransactionsItem = (props) => {

    return (
        <View style={marginStyles.margin_01} >

            { /* Text - Transaction Amount */}
            <Text style={[widthStyles.width_25p, fontFamilyStyles.extraBold, colorStyles.redColor, fontSizeStyles.fontSize_045, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_03]}>Hello</Text>
        </View>
    );
}

export default MyRecentTransactionsItem;