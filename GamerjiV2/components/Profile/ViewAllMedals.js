import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList, Linking } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import ViewAllMedalsItem from '../../componentsItem/ViewAllMedalsItem';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const ViewAllMedals = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [medals, setMedals] = useState([]);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 10

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToMedalsListAPI();

        return () => {
            dispatch(actionCreators.resetViewAllMedalsState())
        }
    }, [])

    // This Use-Effect function should call the Medals API
    const callToMedalsListAPI = () => {
        let payload = {
            skip: medals.length,
            limit: pageLimit,
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestViewAllMedals(payload))
    }

    // This function sets the Medals API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.viewAllMedals.model }),
        shallowEqual
    );
    var { viewAllMedalsListResponse } = currentState

    // This function should return the Medals API Response
    useEffect(() => {
        if (viewAllMedalsListResponse) {
            setLoading(false)

            if (viewAllMedalsListResponse.count == 0) {
                setTotalRecords(true)
            }

            if (viewAllMedalsListResponse.list) {
                setIsFooterLoading(viewAllMedalsListResponse.list.length == 0)
                var tempArr = viewAllMedalsListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setMedals([...medals, ...tempArr])
            }
        }
        viewAllMedalsListResponse = undefined
    }, [viewAllMedalsListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Medals Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_medals} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Wrapper - View All Medals Lists */}
                    {!totalRecords ?
                        <FlatList style={[marginStyles.margin_20]}
                            data={medals}
                            renderItem={({ item, index }) => <ViewAllMedalsItem item={item} index={index} navigation={props.navigation} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    callToMedalsListAPI()
                                }
                            }} />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('viewAllMedals') &&
                        <SponsorBannerAds screenCode={'viewAllMedals'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('viewAllMedals')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView >
    )
}

export default ViewAllMedals;