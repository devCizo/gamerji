import produce from 'immer';
import * as actionTypes from '../../actions/profile/profileAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        profileResponse: undefined,
        statsListResponse: undefined,
    },
    error: false
};

{ /* Request - Profile */ }
const profileRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.profileResponse = action.model;
    });
};

{ /* Request Failure - Profile */ }
const profileRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.profileResponse = action.model;
    });
};

{ /* Request - Stat List */ }
const statList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.statsListResponse = action.model;
    });
};

{ /* Request Failure - Stat List */ }
const statListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.statsListResponse = action.model;
    });
};

{ /* Reset State - Profile */ }
const resetProfileState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.profileResponse = undefined;
        draft.model.statsListResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_PROFILE:
            return profileRequest(state, action);
        case actionTypes.REQUEST_PROFILE_FAILED:
            return profileRequestFailed(state, action);
        case actionTypes.STAT_LIST:
            return statList(state, action);
        case actionTypes.STAT_LIST_FAILED:
            return statListFailed(state, action);
        case actionTypes.RESET_PROFILE_STATE:
            return resetProfileState(state, action);
        default:
            return state;
    }
};

export default reducer;