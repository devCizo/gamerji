import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';


export const walletUsageLimitForSingleTournament = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT,
        model: viewModel
    };
};

export const walletUsageLimitForSingleTournamentFailed = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT_FAILED,
        model: viewModel
    };
};

export const requestWalletUsageLimitForSingleTournament = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.GET_WALLET_USAGE_LIMIT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GET_WALLET_USAGE_LIMIT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(walletUsageLimitForSingleTournament(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(walletUsageLimitForSingleTournamentFailed(error.response.data));
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetSingleTounamentState = () => {
    return {
        type: RESET_SINGLE_TOURNAMENT,
    };
};

export const WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT = 'WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT';
export const WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT_FAILED = 'WALLET_USAGE_LIMIT_FOR_SINGLE_TOURNAMENT_FAILED';
export const RESET_SINGLE_TOURNAMENT = 'RESET_SINGLE_TOURNAMENT';
