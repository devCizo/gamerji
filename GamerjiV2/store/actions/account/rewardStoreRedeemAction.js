import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const applyReward = (viewModel) => {
    return {
        type: APPLY_REWARD,
        model: viewModel
    };
};

export const applyRewardFailed = (viewModel) => {
    return {
        type: APPLY_REWARD_FAILED,
        model: viewModel
    };
};

export const requestApplyReward = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.REWARD.APPLY_REWARD);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.REWARD.APPLY_REWARD, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(applyReward(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(applyRewardFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const resetRewardStoreRedeemState = () => {
    return {
        type: RESET_REWARD_STORE_REDEEM_STATE,
    };
};


export const APPLY_REWARD = 'APPLY_REWARD';
export const APPLY_REWARD_FAILED = 'APPLY_REWARD_FAILED';
export const RESET_REWARD_STORE_REDEEM_STATE = 'RESET_REWARD_STORE_REDEEM_STATE';