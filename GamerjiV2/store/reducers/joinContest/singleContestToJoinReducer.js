import produce from 'immer';
import * as actionTypes from '../../actions/joinContest/singleContestToJoinAction';

const initialState = {
    model: {
        walletUsageLimitForSingleContestResponse: undefined,
    },
    error: false
};


const walletUsageLimitForSingleContest = (state, action) => {
    return produce(state, (draft) => {
        draft.model.walletUsageLimitForSingleContestResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const walletUsageLimitForSingleContestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.walletUsageLimitForSingleContestResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST:
            return walletUsageLimitForSingleContest(state, action);
        case actionTypes.WALLET_USAGE_LIMIT_FOR_SINGLE_CONTEST_FAILED:
            return walletUsageLimitForSingleContestFailed(state, action);
        case actionTypes.RESET_SINGLE_CONTEST_STATE:
            return resetSingleContestState(state);
        default:
            return state;
    }
};

export default reducer;

const resetSingleContestState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.walletUsageLimitForSingleContestResponse = undefined
    })
}