import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, Dimensions, ScrollView, Share, StatusBar } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew, heightStyles } from '../../appUtils/commonStyles';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Constants } from '../../appUtils/constants';
import { LineChart } from "react-native-chart-kit";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog, convertNumberToMillions } from '../../appUtils/commonUtlis';
import * as generalSetting from '../../webServices/generalSetting';
import moment from 'moment';
import ModalDropdown from 'react-native-modal-dropdown';
import { AppConstants } from '../../appUtils/appConstants';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import { ProgressBar } from 'react-native-paper';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

const OtherUserProfile = (props) => {

    addLog(props)

    // Variable Declarations
    const dispatch = useDispatch();
    const insets = useSafeAreaInsets();
    const [activeTab, setActiveTab] = useState('stats');
    const [isLoading, setLoading] = useState(true);

    const [profile, setProfile] = useState(undefined)
    const [statsArr, setStatsArr] = useState([]);
    const [gamesIndex, setGamesIndex] = useState(0);

    const [collegeList, setCollegeList] = useState([]);
    const [selectedCollegeDetail, setSelectedCollegeDetail] = useState(undefined)

    var otherUserId = props.route.params && props.route.params.otherUserId ? props.route.params.otherUserId : undefined
    const [isFollowing, setIsFollowing] = useState(undefined)

    //Initialize
    useEffect(() => {

        fetchUserProfile()

        return () => {
            dispatch(actionCreators.resetOtherProfileState())
            dispatch(actionCreators.resetFriendsState())
        }
    }, []);

    const fetchUserProfile = () => {
        setLoading(true);
        var payload = {}
        if (otherUserId) {
            payload['user'] = otherUserId
        }

        dispatch(actionCreators.requestOtherUserProfile(payload))
    }

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.otherUserProfile.model }),
        shallowEqual
    );
    var { otherProfileResponse, otherProfileStatsListResponse } = currentState

    const { currentStateCollegiate } = useSelector(
        (state) => ({ currentStateCollegiate: state.collegiate.model }),
        shallowEqual
    );
    var { collegeListResponse } = currentStateCollegiate

    const { currentStateFriends } = useSelector(
        (state) => ({ currentStateFriends: state.friendsState.model }),
        shallowEqual
    );
    var { followUserResponse } = currentStateFriends

    // Profile Response
    // useEffect(() => {
    //     if (otherProfileResponse) {
    //         addLog('otherProfileResponse===>', otherProfileResponse);
    //         setLoading(false)

    //         if (otherProfileResponse.item) {
    //             setProfile({ ...otherProfileResponse.item })

    //             if (otherProfileResponse.item.isFollow) {
    //                 setIsFollowing(otherProfileResponse.item.isFollow)
    //             }
    //             addLog('otherProfileResponse.item.gameNames.length===>', otherProfileResponse.item.gameNames.length);

    //             if (statsArr.length == 0 && otherProfileResponse.item.gameNames.length != 0) {



    //                 requestGameStats(otherProfileResponse.item.gameNames[0].game)
    //             }
    //             if (collegeList.length > 0) {
    //                 dispatch(actionCreators.requestCollegeList())
    //             }
    //         }
    //     }
    //     otherProfileResponse = undefined
    // }, [otherProfileResponse])

    // Profile Response
    useEffect(() => {
        if (otherProfileResponse) {
            setLoading(false)

            setProfile(otherProfileResponse.item)
            if (otherProfileResponse.item.isFollow) {
                setIsFollowing(otherProfileResponse.item.isFollow)
            }
            if (statsArr.length == 0 && otherProfileResponse.item.gameNames && otherProfileResponse.item.gameNames.length != 0) {
                requestGameStats(gamesIndex)
            }
            if (collegeList.length == 0) {
                dispatch(actionCreators.requestCollegeList())
            }
        }
    }, [otherProfileResponse])

    //r  addLog('otherProfileResponse===>', otherProfileResponse)
    //College List Response
    useEffect(() => {
        if (collegeListResponse) {
            setLoading(false)
            if (collegeListResponse.list) {
                setCollegeList(collegeListResponse.list)

                if (profile && profile.college) {
                    let index = collegeListResponse.list.findIndex(obj => obj._id == profile.college)
                    if (index > -1) {
                        setSelectedCollegeDetail(collegeListResponse.list[index])
                    }
                }
            }
        }
        collegeListResponse = undefined
    }, [collegeListResponse]);

    //Fetch Game Wise Data
    // const requestGameStats = (gameId) => {
    //     var payload = {
    //         game: gameId,
    //     }
    //     if (otherUserId) {
    //         payload['user'] = otherUserId
    //     }

    //     dispatch(actionCreators.requestOtherProfileStats(payload))
    // }

    // //Stats List Response

    // useEffect(() => {
    //     if (otherProfileStatsListResponse && otherProfileStatsListResponse.item) {

    //         if (profile && profile.gameNames && profile.gameNames.length > gamesIndex) {

    //             var tempObj = { ...otherProfileStatsListResponse.item }
    //             tempObj['uniqueIGN'] = profile.gameNames[gamesIndex].uniqueIGN

    //             var tempArr = [...statsArr]
    //             tempArr.push(tempObj)

    //             setStatsArr(tempArr);

    //             setGamesIndex(gamesIndex + 1)
    //             requestGameStats(profile.gameNames[gamesIndex].game)
    //         }
    //     }
    //     otherProfileStatsListResponse = undefined
    // }, [otherProfileStatsListResponse])

    //Fetch Game Wise Data
    const requestGameStats = (index) => {

        if (otherProfileResponse && otherProfileResponse.item.gameNames && otherProfileResponse.item.gameNames.length > index) {
            var payload = {
                game: otherProfileResponse.item.gameNames[index].game._id,
                user: otherUserId
            }
            dispatch(actionCreators.requestOtherProfileStats(payload))
        }
    }

    //Stats List Response
    useEffect(() => {
        if (otherProfileStatsListResponse && otherProfileStatsListResponse.item) {
            if (profile && profile.gameNames && profile.gameNames.length > gamesIndex) {


                var tempObj = { ...otherProfileStatsListResponse.item }
                tempObj['uniqueIGN'] = profile.gameNames[gamesIndex].uniqueIGN

                var tempArr = [...statsArr]
                tempArr.push(tempObj)

                setStatsArr(tempArr);

                setGamesIndex()
                requestGameStats(gamesIndex + 1)
                setGamesIndex(gamesIndex + 1)
            }
        }
        otherProfileStatsListResponse = undefined
    }, [otherProfileStatsListResponse])

    console.log("otherProfileStatsListResponse==>", otherProfileStatsListResponse);



    //Tab select
    let selectTabButton = (value) => {
        setActiveTab(value)
    }

    const generateDynamicLink = async () => {
        //  addLog('profile==>', profile);
        const link = await dynamicLinks().buildShortLink({
            link: 'https://www.gamerji.com/userDetail?referralCode=' + profile.referralCode,
            domainUriPrefix: 'https://gamerji.page.link',
            ios: {
                bundleId: 'com.gamerji',
                appStoreId: '1466052584'
            },
            android: {
                packageName: 'com.esports.gamerjipro'
            },
            analytics: {
                campaign: 'banner',
            },
        });

        if (link) {
            addLog('link', link)
            Share.share({
                message: 'Ready to track the competition? Tap  ' + link + ' to start following user on Gamerji now!',
                url: link,
                title: ''
            }, {
                // Android only:
                dialogTitle: '',
                // iOS only:
                excludedActivityTypes: [
                ]
            })
        }
    }
    const getLevelProgressValue = () => {
        let currentLevelPoints = profile.level.level.endPoint - profile.level.level.startPoint - 1
        let userPointsAsPerLevel = profile.level.points - profile.level.level.startPoint - 1

        return userPointsAsPerLevel / currentLevelPoints
    }
    const clickToShareProfile = () => {
        // Share.share({
        //     message: 'BAM: we\'re helping your business with awesome React Native apps',
        //     url: 'http://bam.tech',
        //     title: 'Wow, did you see that?'
        // }, {
        //     // Android only:
        //     dialogTitle: 'Share BAM goodness',
        //     // iOS only:
        //     excludedActivityTypes: [
        //         'com.apple.UIKit.activity.PostToTwitter'
        //     ]
        // })
        generateDynamicLink();

    }

    const viewCollegeDetails = () => {
        if (selectedCollegeDetail) {
            RootNavigation.navigate(Constants.nav_college_detail, { collegeDetail: selectedCollegeDetail, collegeList: collegeList })
        }
    }

    const followUser = () => {

        if (otherUserId) {
            var data = { ...profile }
            if (isFollowing) {
                data.followers = data.followers - 1
            } else {
                data.followers = data.followers + 1
            }
            setProfile(data)

            dispatch(actionCreators.requestFollowUser({ action: !isFollowing ? 'follow' : 'unfollow', followingUser: otherUserId }))
            setIsFollowing(!isFollowing)
        }
    }

    // Follow User Response
    useEffect(() => {
        if (followUserResponse) {
            addLog('followUserResponse', followUserResponse)
        }
    }, [followUserResponse])

    return (
        <>
            {/* StatusBar - Show the status bar as per the theme color */}
            {/* <StatusBar backgroundColor={'#FFC609'} /> */}

            <SafeAreaView style={{ flex: 1, backgroundColor: '#FFC609' }}>

                {profile &&
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        bounces={false}
                    >

                        {/* Navigation Bar */}
                        <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >

                            <TouchableOpacity style={{ width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                                <Image style={{ width: 25, height: 23, tintColor: colors.black }} source={require('../../assets/images/back_icon.png')} />
                            </TouchableOpacity>

                            {/* Share */}
                            <TouchableOpacity style={{ height: 50, width: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => clickToShareProfile()} >
                                <Image style={{ height: 24, width: 24 }} source={require('../../assets/images/share-profile-icon.png')} />
                                <Text style={{ marginTop: 3, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }} >Share</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 120 }} >

                            {/* Gamerji Name */}
                            <Text style={{ marginTop: 8, marginLeft: 20, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18 }} >{profile.gamerjiName}</Text>

                            <View style={{ marginTop: 16, marginLeft: 20, flexDirection: 'row' }} >

                                {/* Firends */}
                                {/* <TouchableOpacity style={{ height: 30, width: 100, borderRadius: 15, borderColor: "#E4B226", borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_friend_list)} >
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }} >Followers ({profile.followers})</Text>
                                </TouchableOpacity> */}

                                {/* Follow */}
                                <TouchableOpacity style={{ /*marginLeft: 5,*/ height: 30, width: 100, borderRadius: 15, backgroundColor: colors.black, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => followUser()} >
                                    <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 13 }} >{isFollowing ? 'Following' : 'Follow'}</Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                        {/* Medal */}
                        {/* Medal */}
                        <View style={{ height: 190, backgroundColor: '#FFD23A', justifyContent: 'center' }} >

                            {profile.level && <>
                                <View style={{ width: 166, height: 30, justifyContent: 'center' }} >

                                    <Text style={{ alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Level - {profile.level.level.num}</Text>

                                </View>

                                {/* Medal */}
                                <View style={{ marginTop: 5, width: 166, flexDirection: 'row', justifyContent: 'center' }} >
                                    <View>
                                        <Image style={{ height: 57, width: 57, resizeMode: 'contain' }} source={profile.level && profile.level.level && profile.level.level.featuredImage && profile.level.level.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + profile.level.level.featuredImage.default }} />
                                        <Text style={{ marginTop: 6, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{profile.level.level.name}</Text>
                                    </View>
                                </View>

                                {/* Progress Bar */}
                                <View style={{ marginTop: 10, marginLeft: 10, width: 156 }} >
                                    <ProgressBar style={{ height: 5, width: '100%', backgroundColor: 'white', borderRadius: 2.5 }} progress={getLevelProgressValue()} color={colors.black} />
                                </View>

                                {/* Level Points */}
                                <View style={{ marginTop: 4, marginLeft: 10, width: 156, height: 30, flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 12 }} >{profile.level.level.startPoint} Points</Text>
                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 12 }} >{profile.level.level.endPoint} Points</Text>
                                </View>

                                <Text style={{ width: 166, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >Points - {profile.level && profile.level.points}</Text>

                            </>
                            }



                        </View>

                        {profile.avatar && profile.avatar.img && profile.avatar.img.default &&
                            <Image style={{ position: 'absolute', top: 50, left: 190, width: Constants.SCREEN_WIDTH - 190, height: 365, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.avatar.img.default }} />
                        }

                        {/* Stats */}
                        <View style={{ marginTop: 52, backgroundColor: colors.black, borderTopLeftRadius: 60, borderTopRightRadius: 60 }} >

                            {activeTab == 'stats' &&
                                <>
                                    {statsArr.length > 0 ?
                                        <FlatList
                                            style={{ marginTop: 40, marginBottom: 40, flex: 1 }}
                                            data={statsArr}
                                            renderItem={({ item, index }) => <ProfileStatsItem item={item} />}
                                        />
                                        :
                                        <View>
                                            <Text style={{ marginTop: 100, marginLeft: 20, marginBottom: 300, width: 166, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, alignSelf: 'center' }} >No stats available</Text>
                                        </View>
                                    }
                                </>
                            }

                            {/* Collegiate */}
                            {activeTab == 'collegiate' &&

                                <>
                                    {!selectedCollegeDetail ?

                                        //Select College
                                        <Text style={{ marginTop: 100, marginLeft: 20, marginBottom: 300, width: 166, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, alignSelf: 'center' }} >No college selected</Text>

                                        :
                                        <View style={{}} >

                                            {/* College Name */}
                                            <Text style={{ marginTop: 60, alignSelf: 'center', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 24, textAlign: 'center' }} >{selectedCollegeDetail.name}</Text>

                                            <View style={{ marginTop: 18, marginLeft: 20, width: Constants.SCREEN_WIDTH - 40, flexDirection: 'row', justifyContent: 'space-between' }} >

                                                {/* Rank */}
                                                <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                                    <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                                        <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollegeDetail.rank}</Text>

                                                        <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Rank</Text>
                                                    </View>

                                                    <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                                                </View>

                                                {/* Members */}
                                                <View style={{ width: '48%', flexDirection: 'row', justifyContent: 'center' }}>
                                                    <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: '#43F2FF', borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 60, borderBottomRightRadius: 60 }} >

                                                        <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{selectedCollegeDetail.members}</Text>

                                                        <Text style={{ marginTop: 2, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Members</Text>
                                                    </View>

                                                    <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/members-collegiate.png')} />
                                                </View>


                                            </View>

                                            <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 60, height: 46, flexDirection: 'row', backgroundColor: colors.red, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => viewCollegeDetails()} >
                                                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >VIEW DETAILS</Text>
                                                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </>
                            }
                        </View>


                        {/* Tabs View */}
                        <View style={{ position: 'absolute', top: 392, left: 52, right: 52, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.black, borderRadius: 20, borderColor: colors.yellow, borderWidth: 1 }} >

                            {/* Stats */}
                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'stats' ? colors.red : 'transparent' }} onPress={() => selectTabButton('stats')}>
                                <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Stats</Text>
                            </TouchableOpacity>

                            {/* Collegiate */}
                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === 'collegiate' ? colors.red : 'transparent' }} onPress={() => selectTabButton('collegiate')}>
                                <Text style={{ color: 'white', fontSize: 15, fontFamily: fontFamilyStyleNew.bold }} >Collegiate</Text>
                            </TouchableOpacity>
                        </View>

                    </ScrollView>
                }

            </SafeAreaView>

            {isLoading && <CustomProgressbar />}
        </>
    )
}

export default OtherUserProfile;

const ProfileStatsItem = (props) => {

    const item = props.item
    const [selectedGraph, setSelectedGraph] = useState('played')

    const getChartLabels = () => {
        let obj = item.stateatistic.map(element => {
            var day = moment(element.date, ["DD/MM/YYYY", "DD"])
            // return moment().format('lll')
            return moment(day).format('DD')
        })
        return obj
    }

    const getChartValues = () => {
        let obj = item.stateatistic.map(element => {
            if (selectedGraph == 'played') {
                return element.played
            } else {
                return element.kills
            }
        })
        return obj
    }

    const moveToInsightStatsScreen = () => {
        addLog(item)
        RootNavigation.navigate(Constants.nav_insights_stats_screen, { state: item })
    }

    return (
        <View>

            <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, justifyContent: 'space-between', flexDirection: 'row' }} >

                {/* Game Name */}
                <View style={{ justifyContent: 'center', width: "60%", }} >
                    <Text style={{ color: colors.yellow, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} >{item.game && item.game.name}</Text>
                    <Text style={{ marginTop: 2, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={2} >{item.uniqueIGN}</Text>
                </View>
                <View style={{ marginLeft: 0, justifyContent: 'center', width: "20%", }} >
                    <Text style={{ color: colors.yellow, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} >Rank</Text>
                    <Text style={{ marginTop: 2, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >{item.avgRates}</Text>
                </View>
                <>
                    {/* <View style={{ overflow: 'hidden' }}>
                        <AnimatedCircularProgress
                            style={{ transform: [{ rotate: '-180deg' }] }}
                            size={100}
                            width={8}
                            fill={item.avgRates / 2}
                            tintColor={colors.yellow}
                            onAnimationComplete={() => console.log('onAnimationComplete')}
                            backgroundColor="#232958"
                            arcSweepAngle={180}
                        />

                        <Text style={{ position: 'absolute', top: 20, alignSelf: 'center', color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >{item.avgRates}%</Text>
                        <Text style={{ position: 'absolute', top: 36, alignSelf: 'center', color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >Rank</Text>
                    </View> */}
                    {/* Insights */}
                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => moveToInsightStatsScreen()} >
                        <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={require('../../assets/images/insights-icon.png')} />
                        <Text style={{ color: 'white', fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} >Insights</Text>
                    </TouchableOpacity>
                </>
            </View>

            <View style={{ marginTop: 16, marginLeft: 10, marginRight: 10, backgroundColor: '#1F2237', borderRadius: 20, overflow: 'hidden' }} >

                <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15, flexDirection: 'row', justifyContent: 'space-between' }} >

                    {/* Played */}
                    <TouchableOpacity onPress={() => setSelectedGraph('played')} >
                        <Text style={{ color: selectedGraph == 'played' ? colors.yellow : colors.white, fontSize: 14, fontFamily: selectedGraph == 'played' ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >Played : {convertNumberToMillions(item.played)}</Text>
                    </TouchableOpacity>

                    {/* Kills */}
                    {/* <TouchableOpacity onPress={() => setSelectedGraph('kills')} >
                        <Text style={{ color: selectedGraph == 'kills' ? colors.yellow : colors.white, fontSize: 14, fontFamily: selectedGraph == 'kills' ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >Kills : {convertNumberToMillions(item.kills)}</Text>
                    </TouchableOpacity> */}
                </View>

                <LineChart
                    style={{}}
                    data={{
                        labels: getChartLabels(),
                        datasets: [
                            {
                                data: getChartValues()
                            }
                        ]
                    }}
                    width={Constants.SCREEN_WIDTH}
                    height={220}
                    yAxisLabel=''
                    yAxisSuffix=''
                    yAxisInterval={1}
                    chartConfig={{
                        backgroundColor: null,
                        backgroundGradientFrom: '#1F2237',
                        backgroundGradientTo: null,
                        decimalPlaces: 0,
                        color: (opacity = 1) => colors.yellow,//`rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => colors.white,
                        style: {
                            borderRadius: 16
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "0",
                            // stroke: colors.blue
                        },

                        fillShadowGradient: colors.yellow,
                        fillShadowGradientOpacity: 0.5
                    }}
                    bezier
                    style={{
                        marginLeft: -10,
                        marginTop: 20
                    }}

                    // withDots={false}
                    withInnerLines={false}
                    withOuterLines={false}
                // withHorizontalLabels={false}
                />

                <Text style={{ position: 'absolute', top: 120, left: selectedGraph == 'played' ? -10 : -2, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >{selectedGraph == 'played' ? 'Played' : 'Kills'}</Text>

                {/* <Text style={{ position: 'absolute', top: 120, left: -2, color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, transform: [{ rotate: '-90deg' }] }} >Kills</Text> */}
                <View style={{ position: 'relative', padding: 10, backgroundColor: '#1F2237', alignItems: "center" }}>
                    <Text style={{ color: colors.yellow, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, alignContent: "center" }} >Last 14 Days</Text>

                </View>
            </View>
        </View>
    )
}