import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import moment from 'moment';
import { AppConstants } from '../../appUtils/appConstants';
import { addLog, getUpiPattern, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";

const PaymentStatusPopup = (props) => {

    const dispatch = useDispatch();
    const [paymentStatus, setPaymentStatus] = useState(Constants.pending)
    const [paymentGatewayResult, setPaymentGatewayResult] = useState(props.paymentGatewayResult)

    useEffect(() => {

        if (paymentGatewayResult && paymentGatewayResult.txStatus && paymentGatewayResult.txStatus == 'SUCCESS') {
            setPaymentStatus(Constants.pending)
            checkPaymentStatus()
        } else {
            setPaymentStatus(Constants.failed)
        }
        return () => {
            dispatch(actionCreators.resetPaymentGatewayState())
        }
    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.paymentGateway.model }),
        shallowEqual
    );
    var { checkTransactionsResponse } = currentState

    // API Call
    const checkPaymentStatus = () => {
        let payload = {
            user: global.profile._id,
            transactionUniqueID: paymentGatewayResult.orderId,
            amount: paymentGatewayResult.orderAmount,
            paymentType: paymentGatewayResult.paymentType
        }

        dispatch(actionCreators.requestCheckTransactions(payload, paymentGatewayResult.transactionId))
    }

    // Check Transactions Response
    useEffect(() => {
        if (checkTransactionsResponse) {

            if (checkTransactionsResponse.paymentStatus) {
                if (checkTransactionsResponse.paymentStatus == 'SUCCESS') {
                    setPaymentStatus(Constants.success)
                } else if (checkTransactionsResponse.paymentStatus == 'PENDING') {

                    setPaymentStatus(Constants.pending)
                    setTimeout(() => {
                        checkPaymentStatus()
                    }, 2000);

                } else {
                    setPaymentStatus(Constants.failed)
                }

            } else {
                setPaymentStatus(Constants.failed)
            }
        }
        checkTransactionsResponse = undefined
    }, [checkTransactionsResponse])

    const getPopuBackgroundColor = () => {
        if (paymentStatus == Constants.pending) {
            return '#FF9544'
        }
        if (paymentStatus == Constants.success) {
            return '#36DE4C'
        }
        if (paymentStatus == Constants.failed) {
            return '#FF4646'
        }
    }

    const getStatusWiseImage = () => {
        if (paymentStatus == Constants.pending) {
            return <Image style={{ marginTop: 42, height: 100, width: 100 }} source={require('../../assets/images/pending-payment.png')} />
        }
        if (paymentStatus == Constants.success) {
            return <Image style={{ marginTop: 42, height: 100, width: 100 }} source={require('../../assets/images/success-payment.png')} />
        }
        if (paymentStatus == Constants.failed) {
            return <Image style={{ marginTop: 42, height: 100, width: 100 }} source={require('../../assets/images/fail-payment.png')} />
        }
    }

    const getStatusWiseTitle = () => {
        if (paymentStatus == Constants.pending) {
            return 'Pending'
        }
        if (paymentStatus == Constants.success) {
            return 'Success!'
        }
        if (paymentStatus == Constants.failed) {
            return 'Failure!'
        }
    }

    return (
        <>
            {paymentGatewayResult &&
                <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, backgroundColor: getPopuBackgroundColor(), alignItems: 'center' }} >

                    {/* Close Button */}
                    <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }} onPress={() => props.paymentStatusPopupClosed()} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>

                    {/* Status Icon */}
                    {getStatusWiseImage()}

                    {/* Title */}
                    <Text style={{ marginTop: 34, color: colors.black, fontSize: 36, fontFamily: fontFamilyStyleNew.bold }} >{getStatusWiseTitle()}</Text>

                    {/* Amount */}
                    {paymentGatewayResult.orderAmount &&
                        <Text style={{ marginTop: 25, color: colors.black, fontSize: 30, fontFamily: fontFamilyStyleNew.bold }} >₹{paymentGatewayResult.orderAmount}</Text>
                    }

                    {/* Date Time */}
                    <Text style={{ marginTop: 8, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >{moment(paymentGatewayResult.txTime).format('DD/MM/yyyy')}   |   {moment(paymentGatewayResult.txTime).format('hh:mm A')}</Text>

                    {/* Transaction ID */}
                    {paymentGatewayResult.orderId &&
                        <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Transaction ID : #{paymentGatewayResult.orderId}</Text>
                    }
                    {/* <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Your coin balance will be credited soon! {paymentStatus}</Text> */}
                    {/* Payment Status */}
                    {paymentStatus == Constants.pending || paymentStatus == Constants.success ?
                        <Text style={{ marginTop: 16, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Your coin balance will be credited soon!</Text>
                        : <Text></Text>
                    }

                    {/* Done */}
                    <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, borderRadius: 23, backgroundColor: colors.black, flexDirection: 'row', justifyContent: 'center', width: 200, alignItems: 'center' }} activeOpacity={0.8} onPress={() => props.paymentStatusPopupClosed()}>

                        <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >Done</Text>

                    </TouchableOpacity>

                </View>
            }
        </>
    )
}

export default PaymentStatusPopup