import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/sponsorAds/sponsorAdsLogAction'

{ /* Initialise - State */ }
const initialState = {
    model: {
        sponsorAdsLogResponse: {},
    },
    error: false
};

{ /* Request - Sponsor Ads List */ }
const sponsorAdsLog = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.sponsorAdsLogResponse[action.screenCode] = action.model;
        addLog('Success Response:::> ', action.screenCode)
    });
};

{ /* Request Failure - Sponsor Ads List */ }
const sponsorAdsLogFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.sponsorAdsLogResponse[action.screenCode] = action.model;
    });
};

{ /* Reset State - Sponsor Ads List */ }
const resetSponsorAdsLogState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.sponsorAdsLogResponse[action.screenCode] = undefined;
    })
}

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SPONSOR_ADS_LOG:
            return sponsorAdsLog(state, action);
        case actionTypes.SPONSOR_ADS_LIST_FAILED:
            return sponsorAdsLogFailed(state, action);
        case actionTypes.RESET_SPONSOR_ADS_LOG_STATE:
            return resetSponsorAdsLogState(state);
        default:
            return state;
    }
};

export default reducer;