import produce from 'immer';
import * as actionTypes from '../../actions/collegiate/collegiateAction';

const initialState = {
    model: {
        collegeListResponse: undefined,
        submitCollegeResponse: undefined
    },
    error: false
};

const collegeList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.collegeListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const collegeListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.collegeListResponse = action.model;
    });
};

const submitCollege = (state, action) => {
    return produce(state, (draft) => {
        draft.model.submitCollegeResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const submitCollegeFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.submitCollegeResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COLLEGE_LIST:
            return collegeList(state, action);
        case actionTypes.COLLEGE_LIST_FAILED:
            return collegeListFailed(state, action);
        case actionTypes.SUBMIT_COLLEGE:
            return submitCollege(state, action);
        case actionTypes.SUBMIT_COLLEGE_FAILED:
            return submitCollegeFailed(state, action);
        case actionTypes.RESET_COLLEGIATE_STATE:
            return resetCollegiateState(state);
        default:
            return state;
    }
};

export default reducer;

const resetCollegiateState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.collegeListResponse = undefined;
        draft.model.submitCollegeResponse = undefined;
    })
}