import React from 'react'

const statsData = [{
    id: '1',
    name: 'Free Fire',
    subTitle: 'Mortel007',
    played: '150',
    kills: '560',
    averageRate: '28.30%',
    winPercentage: '25'
},
{
    id: '2',
    name: 'Asphalt 9',
    subTitle: 'Mortel007',
    played: '180',
    kills: '125',
    averageRate: '32.90%',
    winPercentage: '40'
},
{
    id: '3',
    name: 'Call Of Duty',
    subTitle: 'Mortel007',
    played: '900',
    kills: '970',
    averageRate: '50%',
    winPercentage: '98'
},
{
    id: '4',
    name: 'Clash Royale',
    subTitle: 'Mortel007',
    played: '78',
    kills: '12',
    averageRate: '7.89%',
    winPercentage: '50'
}
];

export default statsData;