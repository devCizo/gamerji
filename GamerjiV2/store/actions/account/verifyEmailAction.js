import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VERIFY_EMAIL_REQUEST_OTP = 'VERIFY_EMAIL_REQUEST_OTP';
export const VERIFY_EMAIL_REQUEST_OTP_FAILED = 'VERIFY_EMAIL_REQUEST_OTP_FAILED';
export const VERIFY_EMAIL_VALIDATE_OTP_FAILED = 'VERIFY_EMAIL_VALIDATE_OTP_FAILED';
export const RESET_VERIFY_EMAIL_STATE = 'RESET_VERIFY_EMAIL_STATE';

{ /* Request - Verify Email OTP */ }
export const verifyEmailOTPRequest = (viewModel) => {
    return {
        type: VERIFY_EMAIL_REQUEST_OTP,
        model: viewModel
    };
};

{ /* Request Failure - Verify Email OTP */ }
export const verifyEmailOTPRequestFailed = (viewModel) => {
    return {
        type: VERIFY_EMAIL_REQUEST_OTP_FAILED,
        model: viewModel
    };
};

{ /* Validate Failure - Verify Email Validate OTP */ }
export const verifyEmailValidateOTPRequestFailed = () => {
    return {
        type: VERIFY_EMAIL_VALIDATE_OTP_FAILED
    };
};

{ /* Reset State - Verify Email */ }
export const resetVerifyEmailState = () => {
    return {
        type: RESET_VERIFY_EMAIL_STATE,
    };
};

{ /* Request - verify Email OTP */ }
export const verifyEmailRequestOTP = (payload) => {
    addLog("URL URL:::> ", WebFields.EMAIL_VERIFY_OTP.MODE);
    addLog("Request Request:::> ", payload);
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.USER_TYPE] = WebFields.HEADERS.USER_TYPE_VALUE;
        axios
            .post(WebFields.EMAIL_VERIFY_OTP.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                if (payload.type === WebFields.EMAIL_VERIFY_OTP.REQUEST_VALIDATE_OTP) {
                    addLog("IF OTP");
                    response.data[WebFields.EMAIL_VERIFY_OTP.RESPONSE_TYPE] = payload.type;
                    dispatch(verifyEmailOTPRequest(response.data));
                } else {
                    addLog("ELSE OTP");
                    response.data[WebFields.EMAIL_VERIFY_OTP.RESPONSE_TYPE] = payload.type;
                    dispatch(verifyEmailOTPRequest(response.data));
                }
            })
            .catch((error) => {
                addLog("CATCH OTP");
                addLog(payload.type);
                error.response.data[WebFields.EMAIL_VERIFY_OTP.RESPONSE_TYPE] = payload.type;
                addLog(error.response.data);
                dispatch(verifyEmailOTPRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
}