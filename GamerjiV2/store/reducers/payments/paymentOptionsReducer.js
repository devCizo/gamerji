import produce from 'immer';
import * as actionTypes from '../../actions/payments/paymentOptionsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        paymentWalletListResponse: undefined
    },
    error: false
};

{ /* Request - Generate Payment Token */ }
const paymentWalletList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.paymentWalletListResponse = action.model;
    });
};

{ /* Request Failure - Generate Payment Token */ }
const paymentWalletListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.paymentWalletListResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.PAYMENT_WALLET_LIST:
            return paymentWalletList(state, action);
        case actionTypes.PAYMENT_WALLET_LIST_FAILED:
            return paymentWalletListFailed(state, action);
        case actionTypes.RESET_PAYMENT_OPTIONS_STATE:
            return resetPaymentOptionsState(state);
        default:
            return state;
    }
};

export default reducer;

const resetPaymentOptionsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.paymentWalletListResponse = undefined;
    });
};