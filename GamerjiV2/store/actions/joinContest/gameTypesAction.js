import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Game Types List */ }
export const gameTypesList = (viewModel) => {
    return {
        type: GAME_TYPES_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Game Types List */ }
export const gameTypesListFailed = (viewModel) => {
    return {
        type: GAME_TYPES_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestGameTypes = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GAME_TYPES.MODE, payload)
            .then((response) => {
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(gameTypesList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(gameTypesListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetGameTypesState = () => {
    return {
        type: RESET_GAME_TYPE_STATE,
    };
};

export const GAME_TYPES_LIST = 'GAME_TYPES_LIST';
export const GAME_TYPES_LIST_FAILED = 'GAME_TYPES_LIST_FAILED';
export const RESET_GAME_TYPE_STATE = 'RESET_GAME_TYPE_STATE';