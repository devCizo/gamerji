const videosData = [
    { id: '1', title: 'Free Fire', count: 1, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 1', video2: 'Video 2' },
    { id: '2', title: 'Clash Royale', count: 2, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 3', video2: 'Video 4' },
    { id: '3', title: 'Asphalt', count: 3, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 5', video2: 'Video 6' },
    { id: '4', title: 'Free Fire', count: 1, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 1', video2: 'Video 2' },
    { id: '5', title: 'Clash Royale', count: 2, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 3', video2: 'Video 4' },
    { id: '6', title: 'Asphalt', count: 3, link: 'https://www.youtube.com/watch?v=TZWPHJVRedI', video1: 'Video 5', video2: 'Video 6' },
];

export default videosData;