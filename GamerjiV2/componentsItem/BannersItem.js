import React from 'react'
import { View, Image } from 'react-native'
import { heightStyles, widthStyles, resizeModeStyles, marginStyles, borderStyles, colorStyles, alignmentStyles, } from '../appUtils/commonStyles';
import * as generalSetting from '../webServices/generalSetting'

const BannersItem = (props) => {

    return (
        <View style={[marginStyles.rightMargin_10]}>

            {/* Image - Banner */}
            <Image style={[widthStyles.width_270, heightStyles.height_90, borderStyles.borderRadius_5]}
                source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item }} />
        </View>
    )
}

export default BannersItem;