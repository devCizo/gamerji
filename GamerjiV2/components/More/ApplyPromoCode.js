import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, Image, TextInput, ScrollView, Keyboard, Alert } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { showSuccessToastMessage, showErrorToastMessage, addLog } from '../../appUtils/commonUtlis';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

const ApplyPromoCode = (props, navigation) => {

    // Variable Declarations
    const [promoCode, setPromoCode] = useState('');
    const [checkAPICall, setCheckAPICall] = useState(undefined);

    addLog('ApplyPromoCode')

    // This function should check the validation first and then call the Apply Promo Code API
    const checkValidation = () => {
        Keyboard.dismiss();
        if (!promoCode.trim()) {
            showErrorToastMessage(Constants.error_enter_promo_code)
            return;
        }
        callToApplyPromoCodeAPI(props);
    };

    // This function should call the Apply Promo Code API
    const callToApplyPromoCodeAPI = (props) => {
        let payload = {
            code: promoCode,
        }
        setCheckAPICall('applyPromoCode');
        props.requestApplyPromoCode(payload);
    }

    // This Use-Effect function should set the Apply Promo Code Data
    useEffect(() => {
        if (props.isSucess && checkAPICall === 'applyPromoCode') {
            showSuccessToastMessage(Constants.success_promo_code_applied)
            setCheckAPICall(undefined);
            setPromoCode('');
        }

        if (props.isError && checkAPICall === 'applyPromoCode') {
            addLog("Props:::> ", props);
            showErrorToastMessage(props.list.applyPromoCode.errors[0].msg);
            setCheckAPICall(undefined);
            setPromoCode('');
        }
    }, [props]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Apply Promo Code Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_more_apply_promo_code} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Image - Banner */}
                        <Image style={[heightStyles.height_112, widthStyles.width_315, borderStyles.borderRadius_10, alignmentStyles.alignSelfStretch, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/promo_code_bg.png')} />

                        {/* Text - Enter your promo code */}
                        <Text style={[alignmentStyles.alignTextCenter, fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_18, marginStyles.topMargin_35]}> {Constants.text_apply_promo_code} </Text>

                        {/* Text - Invite Code */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_30]}> {Constants.text_promo_code} </Text>

                        {/* Text Input - Invite Code */}
                        <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                            placeholder={Constants.validation_promo_code}
                            value={promoCode}
                            onChangeText={text => setPromoCode(text)} />
                    </ScrollView>

                    {/* TouchableOpacity - Submit Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_20]}
                        onPress={() => checkValidation()} activeOpacity={0.5}>

                        {/* Text - Button (Submit) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_submit}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

{ /* State - Map State To Props */ }
function mapStateToProps(state) {
    return {
        list: state.more.model,
        isSucess: state.more.sucess,
        isError: state.more.error,
        errorMessage: state.more.errorMessage,
    };
}

{ /* Dispatch - Map Dispatch To Props */ }
const mapDispatchToProps = dispatch => ({
    requestApplyPromoCode: data => dispatch(actionCreators.requestApplyPromoCode(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplyPromoCode);