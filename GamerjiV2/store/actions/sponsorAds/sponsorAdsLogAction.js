import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const SPONSOR_ADS_LOG = 'SPONSOR_ADS_LOG'
export const SPONSOR_ADS_LOG_FAILED = 'SPONSOR_ADS_LOG_FAILED'
export const RESET_SPONSOR_ADS_LOG_STATE = 'RESET_SPONSOR_ADS_LOG_STATE'

{ /* Request - Sponsor Ads List */ }
export const sponsorAdsLog = (viewModel, code) => {
    return {
        type: SPONSOR_ADS_LOG,
        model: viewModel,
        screenCode: code
    };
};

{ /* Request Failure - Sponsor Ads Log */ }
export const sponsorAdsLogFailed = (viewModel, code) => {
    return {
        type: SPONSOR_ADS_LOG_FAILED,
        model: viewModel,
        screenCode: code
    };
};

{ /* Reset State - Sponsor Ads List */ }
export const resetSponsorAdsLogState = (code) => {
    return {
        type: RESET_SPONSOR_ADS_LOG_STATE,
        screenCode: code
    };
};

{ /* Request - Sponsor Ads */ }
export const requestSponsorAdsLog = (payload, code) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.SPONSOR_ADS_LOG.MODE, payload)
            .then((response) => {
                // addLog("URL ADS:::> ", response.config.baseURL + response.config.url);
                // addLog("Request ADS:::> ", response.config.data);
                // addLog("Response ADS:::> ", response.data);
                //  dispatch(sponsorAdsList(response.data, code));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                //  dispatch(sponsorAdsListFailed(error.response.data, code));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const requestBannerAdsLog = (payload, code) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.BANNER_ADS_LOG.MODE, payload)
            .then((response) => {
                // addLog("URL ADS:::> ", response.config.baseURL + response.config.url);
                // addLog("Request ADS:::> ", response.config.data);
                // addLog("Response ADS:::> ", response.data);
                //   dispatch(sponsorAdsList(response.data, code));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                //   dispatch(sponsorAdsListFailed(error.response.data, code));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};