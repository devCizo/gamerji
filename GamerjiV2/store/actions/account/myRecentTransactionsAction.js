import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_MY_RECENT_TRANSACTIONS = 'REQUEST_MY_RECENT_TRANSACTIONS';
export const REQUEST_MY_RECENT_TRANSACTIONS_FAILED = 'REQUEST_MY_RECENT_TRANSACTIONS_FAILED';
export const REQUEST_EMAIL_INVOICE = 'REQUEST_EMAIL_INVOICE';
export const REQUEST_EMAIL_INVOICE_FAILED = 'REQUEST_EMAIL_INVOICE_FAILED';
export const RESET_MY_RECENT_TRANSACTIONS_STATE = 'RESET_MY_RECENT_TRANSACTIONS_STATE';

{ /* Request - My Recent Transactions */ }
export const myRecentTransactionsRequest = (viewModel) => {
    return {
        type: REQUEST_MY_RECENT_TRANSACTIONS,
        model: viewModel
    };
};

{ /* Request Failure - My Recent Transactions */ }
export const myRecentTransactionsRequestFailed = (viewModel) => {
    return {
        type: REQUEST_MY_RECENT_TRANSACTIONS_FAILED,
        model: viewModel
    };
};

{ /* Request - Email Invoice */ }
export const emailInvoiceRequest = (viewModel) => {
    return {
        type: REQUEST_EMAIL_INVOICE,
        model: viewModel
    };
};

{ /* Request Failure - Email Invoice */ }
export const emailInvoiceRequestFailed = (viewModel) => {
    return {
        type: REQUEST_EMAIL_INVOICE_FAILED,
        model: viewModel
    };
};

{ /* Reset State - My Recent Transactions */ }
export const resetMyRecentTransactionsState = () => {
    return {
        type: RESET_MY_RECENT_TRANSACTIONS_STATE,
    };
};

{ /* Request - My Recent Transactions */ }
export const requestMyRecentTransactions = (payload) => {
    return (dispatch) => {
        debugger
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.MY_RECENT_TRANSACTIONS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(myRecentTransactionsRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(myRecentTransactionsRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Email Invoice */ }
export const requestEmailInvoice = (payload, transactionId) => {
    addLog("transactionId:::> ", transactionId);
    return (dispatch) => {
        const url = WebFields.EMAIL_INVOICE.MODE + transactionId
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(url, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(emailInvoiceRequest(response.data.item));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(emailInvoiceRequestFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};