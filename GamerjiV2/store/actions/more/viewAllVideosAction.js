import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIEW_ALL_VIDEO_LIST = 'VIEW_ALL_VIDEO_LIST';
export const VIEW_ALL_VIDEO_LIST_FAILED = 'VIEW_ALL_VIDEO_LIST_FAILED';
export const RESET_VIEW_ALL_VIDEOS_LIST_STATE = 'RESET_VIEW_ALL_VIDEOS_LIST_STATE';

{ /* Request - View All Video List */ }
export const viewAllVideoList = (viewModel) => {
    return {
        type: VIEW_ALL_VIDEO_LIST,
        model: viewModel
    };
};

{ /* Request Failure - View All Video List */ }
export const viewAllVideoListFailed = (viewModel) => {
    return {
        type: VIEW_ALL_VIDEO_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - View All Videos List */ }
export const resetViewAllVideosState = () => {
    return {
        type: RESET_VIEW_ALL_VIDEOS_LIST_STATE,
    };
};

{ /* Request - View All Videos */ }
export const requestViewAllVideos = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIEW_ALL_VIDEO_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(viewAllVideoList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(viewAllVideoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};