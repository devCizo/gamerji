import produce from 'immer';
import * as actionTypes from '../../actions/profile/insightsStatsAction';

const initialState = {
    model: {
        gameDetailResponse: undefined,
        contestStatsListResponse: undefined,
        rankSummaryResponse: undefined,
    },
    error: false
};

// ======================

const gameDetail = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.gameDetailResponse = action.model;
    });
};

const gameDetailFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gameDetailResponse = action.model;
    });
};

// ======================

const contestStatList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.contestStatsListResponse = action.model;
    });
};

const contestStatListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.contestStatsListResponse = action.model;
    });
};

// ======================

const rankSummary = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.rankSummaryResponse = action.model;
    });
};

const rankSummaryFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.rankSummaryResponse = action.model;
    });
};

// =========================

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTEST_STAT_LIST:
            return contestStatList(state, action);
        case actionTypes.CONTEST_STAT_LIST_FAILED:
            return contestStatListFailed(state, action);
        case actionTypes.RANK_SUMMARY:
            return rankSummary(state, action);
        case actionTypes.RANK_SUMMARY_FAILED:
            return rankSummaryFailed(state, action);
        case actionTypes.GAME_DETAIL:
            return gameDetail(state, action);
        case actionTypes.GAME_DETAIL_FAILED:
            return gameDetailFailed(state, action);
        case actionTypes.RESET_INSIGHT_STATS_STATE:
            return resetInsightStatsState(state, action);
        default:
            return state;
    }
};

export default reducer;


{ /* Reset State - Profile */ }
const resetInsightStatsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.contestStatsListResponse = undefined;
        draft.model.rankSummaryResponse = undefined;
        draft.model.gameDetailResponse = undefined;
    });
};