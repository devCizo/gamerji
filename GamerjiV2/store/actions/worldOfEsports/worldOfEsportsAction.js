import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const FEATURED_VIDEO_LIST = 'FEATURED_VIDEO_LIST';
export const FEATURED_VIDEO_LIST_FAILED = 'FEATURED_VIDEO_LIST_FAILED';
export const LIVE_FEATURED_VIDEO_LIST = 'LIVE_FEATURED_VIDEO_LIST';
export const LIVE_FEATURED_VIDEO_LIST_FAILED = 'LIVE_FEATURED_VIDEO_LIST_FAILED';
export const BLOGS_LIST = 'BLOGS_LIST';
export const BLOGS_LIST_FAILED = 'BLOGS_LIST_FAILED';
export const TOP_PROFILE_LIST = 'TOP_PROFILE_LIST';
export const TOP_PROFILE_LIST_FAILED = 'TOP_PROFILE_LIST_FAILED';
export const RESET_WORLD_OF_ESPORTS_STATE = 'RESET_WORLD_OF_ESPORTS_STATE';

{ /* Request - Featured Video List */ }
export const featuredVideoList = (viewModel) => {
    return {
        type: FEATURED_VIDEO_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Featured Video List */ }
export const featuredVideoListFailed = (viewModel) => {
    return {
        type: FEATURED_VIDEO_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Live Featured Video List */ }
export const liveFeaturedVideoList = (viewModel) => {
    return {
        type: LIVE_FEATURED_VIDEO_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Live Featured Video List */ }
export const liveFeaturedVideoListFailed = (viewModel) => {
    return {
        type: LIVE_FEATURED_VIDEO_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Blogs List */ }
export const blogsList = (viewModel) => {
    return {
        type: BLOGS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Blogs List */ }
export const blogsListFailed = (viewModel) => {
    return {
        type: BLOGS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Top Profile List */ }
export const topProfileList = (viewModel) => {
    return {
        type: TOP_PROFILE_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Top Profile List */ }
export const topProfileListFailed = (viewModel) => {
    return {
        type: TOP_PROFILE_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - World Of Esports */ }
export const resetWorldOfEsportsState = () => {
    return {
        type: RESET_WORLD_OF_ESPORTS_STATE,
    };
};

{ /* Request - Live Featured Videos */ }
export const requestLiveFeaturedVideos = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIEW_ALL_VIDEO_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(liveFeaturedVideoList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(liveFeaturedVideoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};
{ /* Request - Featured Videos */ }
export const requestFeaturedVideos = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIEW_ALL_VIDEO_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(featuredVideoList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(featuredVideoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Blogs */ }
export const requestBlogs = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.WORLD_OF_ESPORTS.MODE_BLOGS_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(blogsList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(blogsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Top Profile */ }
export const requestTopProfile = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.WORLD_OF_ESPORTS.MODE_TOP_PROFILE_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(topProfileList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(topProfileListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};