import produce from 'immer';
import * as actionTypes from '../../actions/profile/otherUserProfileAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        otherProfileResponse: undefined,
        otherProfileStatsListResponse: undefined
    },
    error: false
};

const otherProfileRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.otherProfileResponse = action.model;
    });
};

const otherProfileRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.otherProfileResponse = action.model;
    });
};

const otherProfileStatList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.otherProfileStatsListResponse = action.model;
    });
};

const otherProfileStatListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.otherProfileStatsListResponse = action.model;
    });
};

const resetOtherProfileState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.otherProfileResponse = undefined;
        draft.model.otherProfileStatsListResponse = undefined;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_OTHER_PROFILE:
            return otherProfileRequest(state, action);
        case actionTypes.REQUEST_OTHER_PROFILE_FAILED:
            return otherProfileRequestFailed(state, action);
        case actionTypes.OTHER_PROFILE_STAT_LIST:
            return otherProfileStatList(state, action);
        case actionTypes.OTHER_PROFILE_STAT_LIST_FAILED:
            return otherProfileStatListFailed(state, action);
        case actionTypes.RESET_OTHER_USER_PROFILE_STATE:
            return resetOtherProfileState(state, action);
        default:
            return state;
    }
};

export default reducer;