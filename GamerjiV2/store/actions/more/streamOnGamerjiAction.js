import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const GAMES_SEARCH_LIST = 'GAMES_SEARCH_LIST';
export const GAMES_SEARCH_LIST_FAILED = 'GAMES_SEARCH_LIST_FAILED';
export const REQUEST_ADD_STREAMER = 'REQUEST_ADD_STREAMER';
export const REQUEST_ADD_STREAMER_FAILED = 'REQUEST_ADD_STREAMER_FAILED';
export const RESET_STREAM_ON_GAMERJI_STATE = 'RESET_STREAM_ON_GAMERJI_STATE';

{ /* Request - Games Search List */ }
export const gamesSearchList = (viewModel) => {
    return {
        type: GAMES_SEARCH_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Games Search List */ }
export const gamesSearchListFailed = (viewModel) => {
    return {
        type: GAMES_SEARCH_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Add Streamer */ }
export const addStreamerRequest = (viewModel) => {
    return {
        type: REQUEST_ADD_STREAMER,
        model: viewModel
    };
};

{ /* Request Failure - Add Streamer */ }
export const addStreamerRequestFailed = (viewModel) => {
    return {
        type: REQUEST_ADD_STREAMER_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Stream On Gamerji */ }
export const resetStreamOnGamerjiState = () => {
    return {
        type: RESET_STREAM_ON_GAMERJI_STATE,
    };
};

{ /* Request - Games Search */ }
export const requestGamesSearch = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GAMES_SEARCH_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gamesSearchList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(gamesSearchListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Add Streamer */ }
export const requestAddStreamer = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ADD_STREAMER_FORM.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(addStreamerRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(addStreamerRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};