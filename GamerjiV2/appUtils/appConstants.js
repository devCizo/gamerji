import { Constants } from "./constants";
import { Adjust, AdjustConfig } from 'react-native-adjust';

export class AppConstants {

    static GOOGLE_PLACE_API_KEY = ''
    static FB_BANNER_ADS = ''

    // Session Manager Keys
    static key_is_login = 'IsLogin';
    static key_token = 'Token';
    static key_username = 'Username';
    static key_user_data = 'UserData';
    static key_user_profile = 'UserProfile';
    static key_user_profile_full = 'UserProfileFull';
    static key_daily_login_day = 'DailyLoginDay';
    static key_screens_data = 'ScreensData';

    // Session Manager Key Values
    static token = '';
    static mobile = '';
    static day = '';

    // Payment Types
    static PAYMENT_TYPE_CARD = "card";
    static PAYMENT_TYPE_NET_BANKING = "nb";
    static PAYMENT_TYPE_WALLET = "wallet";
    static PAYMENT_TYPE_UPI = "upi";
    static PAYMENT_TYPE_PAYTM = "paytm";

    // Streamer Status
    static STREAMER_STATUS_PENDING = "Pending";
    static STREAMER_STATUS_UNDER_REVIEW = "Under Review";
    static STREAMER_STATUS_REJECTED = "Rejected";

    // Currency
    static CURRENCY_INR = 'INR';

    // Sponsor Ads Type
    static TYPE_SPONSOR_ADS_IMPRESSIONS = '1';
    static TYPE_SPONSOR_ADS_CLICK = '2';


    // One Signal App Id
    static ONE_SIGNAL_APP_ID = ""

    // One Signal App Id
    static ADJUST_APP_TOKEN = ""

}

export const cashFreeClientId = () => {
    // Live
    return global.addBalanceCurrencyType == Constants.coin_payment ? "" : "";
}

export const cashFreeSecretId = () => {
    return global.addBalanceCurrencyType == Constants.coin_payment ? "" : "";
}

export const cashFreeAPIMode = () => {
    // Test
    //  return "TEST"
    // Live
    return "PROD"
}

export const adjustEnvironment = () => {

    // Test
    // return AdjustConfig.EnvironmentSandbox

    // Live
    return AdjustConfig.EnvironmentProduction
}