import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, alignmentStyles, justifyContentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, otherStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import * as generalSetting from '../webServices/generalSetting'
import LinearGradient from 'react-native-linear-gradient';
import CustomMarquee from '../appUtils/customMarquee';
import { setCurrencyFormat } from '../appUtils/commonUtlis';

const FeaturedTournamentsItem = (props) => {

    let colors = ['#FFC609', '#43F2FF', '#09FFC4'];

    return (
        <TouchableOpacity style={[marginStyles.rightMargin_10]} activeOpacity={0.9} onPress={() => props.moveToSingleTournament(props.item.shortCode)} >

            {/* Wrapper - All Tournaments Main View */}
            <View style={[positionStyles.relative, heightStyles.height_125, widthStyles.width_150, flexDirectionStyles.column, resizeModeStyles.cover, borderStyles.borderRadius_5, marginStyles.topMargin_16, { backgroundColor: colors[props.index % colors.length] }]}>

                {/* Wrapper - All Tournaments Main View */}
                <View style={[containerStyles.container, positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.bottom_0, alignmentStyles.left_0, alignmentStyles.right_0, marginStyles.bottomMargin_4, alignmentStyles.alignItemsCenter, { justifyContent: 'center', marginLeft: 20, marginRight: 20 }]}>



                    {/* <Image style={[heightStyles.height_11, widthStyles.width_17]} source={require('../assets/images/ic_currency.png')} /> */}

                    {/* Text - Prizepool */}
                    <Text style={[fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_5]} numberOfLines={1} ellipsizeMode={'tail'}>
                        {/* Image - Currency */}
                        {props.item.currency && props.item.currency.code == 'coin' ?
                            <Image style={{ height: 12, width: 12, resizeMode: 'contain' }} source={props.item.currency && props.item.currency.img && props.item.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + props.item.currency.img.default }} />
                            :
                            // <Image style={{ height: 12, width: 12, tintColor: colors.white }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                            <Text style={{ fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{props.item.currency.symbol}</Text>
                        }{setCurrencyFormat(props.item.prizePool)}</Text>
                </View>
            </View>

            {/* Wrapper - Image Featured Tournements */}
            <View style={[positionStyles.absolute, alignmentStyles.top_0, alignmentStyles.left_0, alignmentStyles.right_0, marginStyles.leftMargin_6, marginStyles.rightMargin_6, otherStyles.zIndex_0, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}>

                {/* Image - Featured Tournements */}
                <Image style={[heightStyles.height_110, widthStyles.width_138, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.topMargin_005]}
                    source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.featuredImage.default }} />

                {/* Linear Gradient - Shadow  */}
                <LinearGradient colors={['#00000000', '#000000']} style={[positionStyles.absolute, heightStyles.height_115, widthStyles.width_138, borderStyles.borderRadius_5, resizeModeStyles.cover, marginStyles.bottom_0, otherStyles.zIndex_1, otherStyles.opacity_1]} />

                {/* Wrapper - Featured Tournements Game Content */}
                <View style={[positionStyles.absolute, flexDirectionStyles.column, otherStyles.zIndex_1, { left: 10, right: 10, bottom: 10 }]}>

                    {/* Text - Game Name + Game Type */}
                    <CustomMarquee value={props.item.game.name + ' - ' + props.item.gameType.name} style={[fontFamilyStyles.bold, alignmentStyles.alignTextCenter, colorStyles.whiteColor, fontSizeStyles.fontSize_13, { marginTop: 3 }]} />

                    {/* Text - Game Name + Game Type Separator */}
                    <Text style={[heightStyles.height_2, widthStyles.width_30_new, marginStyles.topMargin_5, marginStyles.bottomMargin_5, { backgroundColor: colors[props.index % colors.length] }]} />

                    {/* Text - Tournement Title */}
                    <CustomMarquee value={props.item.title} style={[containerStyles.container, fontFamilyStyles.regular, colorStyles.silverColor, fontSizeStyles.fontSize_10]} />

                    <View style={[flexDirectionStyles.row]}>
                        {/* Text - Earn Per Kill Amount */}
                        {
                            props.item.killPoints ? <Text style={[containerStyles.container, fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_10]} numberOfLines={1} ellipsizeMode='tail'> {Constants.text_earn_per_kill} {Constants.text_rupees}{props.item.killPoints} </Text>
                                : null
                        }

                        {/* Image - Gun */}
                        {
                            props.item.killPoints ? <Image source={require('../assets/images/ic_gun.png')} />
                                : null
                        }
                    </View>
                </View>
            </View>
        </TouchableOpacity >
    )
}

export default FeaturedTournamentsItem;