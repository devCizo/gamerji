import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import { Constants } from '../../appUtils/constants';
import { scrollViewStyles, justifyContentStyles, containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, fontFamilyStyleNew, positionStyles, otherStyles } from '../../appUtils/commonStyles';

import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import { connect } from 'react-redux';
import moment from 'moment';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';

import Modal from 'react-native-modal';
import CommonInfoPopup from '../../commonComponents/commonInfoPopup';
import * as generalSetting from '../../webServices/generalSetting'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import HtmlGame from './HtmlGame';

const JoinHtmlGameValidation = (props) => {

    const dispatch = useDispatch();
    const [contest, setContest] = useState(props.contest)
    const [isLoading, setLoading] = useState(false);
    const [openCommonInfoPopup, setOpenCommonInfoPopup] = useState(false);

    var walletUsageLimit = props.walletUsageLimit

    useEffect(() => {

        return () => {
            dispatch(actionCreators.resetJoinContestState())
        }
    }, [])



    let JoinContestValidation = () => {

        // let amountToAdd = 40
        // props.closeWalletValidationPopup()
        // props.moveToAddBalance(amountToAdd)
        // return

        if (walletUsageLimit && walletUsageLimit.joinFlag == false) {

            var amountToAdd = 0
            if (!isCoinContest()) {
                amountToAdd = walletUsageLimit.entryFee - walletUsageLimit.walletBalance
            } else {
                amountToAdd = walletUsageLimit.toBuyCoinAmount
            }
            addLog('amountToAdd', amountToAdd)
            props.moveToAddBalance(amountToAdd, isCoinContest())
            props.closeWalletValidationPopup()

        } else if (contest.gameType && contest.gameType.players && Number(contest.gameType.players) > 1) {
            props.moveToSquadRegistration()
            props.closeWalletValidationPopup()
        } else {
            let payload = {
                type: 'contest',
                contest: contest._id,
                isActive: contest.isActive
            }
            if (global.profile && global.profile.gameNames.length > 0) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == contest.game._id)
                if (index > -1) {
                    payload['uniqueIGN'] = global.profile.gameNames[index].uniqueIGN
                }
            }
            setLoading(true)
            dispatch(actionCreators.requestJoinContest(payload))
        }
    }

    // CheckSquad API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.joinContest.model }),
        shallowEqual
    );
    var { joinContestResponse } = currentState

    useEffect(() => {

        if (joinContestResponse) {
            setLoading(false)
            if (joinContestResponse.item) {
                props.setContestAsJoined()
                props.closeWalletValidationPopup()
            } else if (joinContestResponse.errors) {
                if (joinContestResponse.errors[0] && joinContestResponse.errors[0].msg) {
                    showErrorToastMessage(joinContestResponse.errors[0].msg)
                }
            }
            joinContestResponse = undefined
            currentState = undefined
        }
    }, [joinContestResponse]);

    const getUtilizeAndWinnings = () => {

        if (walletUsageLimit) {
            if (isCoinContest()) {
                if (walletUsageLimit.coinAmount != null) {
                    return walletUsageLimit.coinAmount
                }
            } else {
                if (walletUsageLimit.walletBalance != null) {
                    return walletUsageLimit.walletBalance
                }
            }
        }
        return 0
    }

    const isFreeContest = () => {
        if (walletUsageLimit.entryFee == '0' || walletUsageLimit.entryFee == '') {
            return true
        }
        return false
    }

    const toPayAmount = () => {
        if (walletUsageLimit.toPay == '0' || walletUsageLimit.toPay == '' || walletUsageLimit.toPay == null) {
            return undefined
        }
        return walletUsageLimit.toPay
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const isCoinContest = () => {
        if (contest.currency && contest.currency.code == 'coin') {
            return true
        }
        return false
    }

    const getCurrencyImage = (isWhite) => {
        return (
            isCoinContest() ?
                <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                :
                <Text style={{ color: isWhite ? colors.white : colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >{contest.currency.symbol}</Text>
        )
    }

    let redirectToGameTypeScreen = (htmlGame) => {

        props.navigation.navigate(Constants.nav_html_game_detail, { htmlGame });
        props.closeHtmlGamePopup()
    }

    return (
        <>
            <View style={{ height: 300, backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

                <View style={styles.headerContainer}>

                    <Text style={{ marginLeft: 60, color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center', }} numberOfLines={2} >{props.htmlGame && props.htmlGame.html5Category && props.htmlGame.html5Category.name} - {props.htmlGame && props.htmlGame.name}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ alignSelf: 'flex-end', marginTop: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1 }} onPress={() => props.closeHtmlGamePopup()} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>
                {/* Center Content */}
                <View style={styles.centerContentContainer} >



                    {/* Confimation View */}
                    <View style={{ backgroundColor: colors.red, marginTop: 12 }} >
                        <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, marginTop: 10, marginBottom: 10, marginLeft: 12 }}
                        >Confirmation</Text>

                    </View>

                    {/* Entry Fee */}
                    <View style={{ marginTop: 14, flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginBottom: 0 }} >Entry Fee</Text>

                        <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Free</Text>

                    </View>





                    {/* Separator Line */}
                    <Image style={{ marginTop: 14, height: 1, backgroundColor: colors.black, opacity: 0.2 }} />

                    <Text style={{ marginTop: 14, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }} > Enjoy Free Games on Gamerji</Text>





                </View>

                {/* Join Contest Button */}
                <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => redirectToGameTypeScreen(props.htmlGame)} activeOpacity={0.5} >

                    <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >JOIN GAME</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                </TouchableOpacity>

                {/* Logo */}
                {/* <Image style={{ marginTop: 20, height: 50, width: 200, resizeMode: 'contain' }} source={{ uri: voucher.img && voucher.img.default && generalSetting.UPLOADED_FILE_URL + voucher.img.default }} /> */}


            </View>

            {isLoading && <CustomProgressbar />}
        </>
    )
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.yellow,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    centerContentContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
    },
})

export default JoinHtmlGameValidation;