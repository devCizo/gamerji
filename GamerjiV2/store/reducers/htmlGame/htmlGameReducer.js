import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/htmlGame/htmlGameAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        html5GameResponse: undefined,
    },
    error: false
};

{ /* Request - Game Types List */ }
const html5GamesList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.html5GameResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const html5GamesListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.html5GameResponse = action.model;
    });
};


{ /* Request - Game Types List */ }
const html5GamesUpdateView = (state, action) => {
    return produce(state, (draft) => {
        draft.model.html5GameDetailsResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const html5GamesUpdateViewFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.html5GameDetailsResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HTML5_GAME_LIST:
            return html5GamesList(state, action);
        case actionTypes.HTML5_GAME_LIST_FAILED:
            return html5GamesListFailed(state, action);
        case actionTypes.RESET_HTML5_GAME_STATE:
            return resetHtml5GamesState(state);
        case actionTypes.HTML5_GAME_UPDATE_VIEW:
            return html5GamesUpdateView(state, action);
        case actionTypes.HTML5_GAME_UPDATE_VIEW_FAILED:
            return html5GamesUpdateViewFailed(state, action);
        default:
            return state;
    }
};

export default (reducer);

const resetHtml5GamesState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.html5GameResponse = undefined;
    })
}