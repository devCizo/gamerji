import produce from 'immer';
import * as actionTypes from '../../actions/myContests/myContestsAction';

const initialState = {
    model: {
        myContestListResponse: undefined,
        myTournamentListResponse: undefined,
    },
    error: false
};

const myContestList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.myContestListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const myContestListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.myContestListResponse = action.model;
    });
};

const myTournamentList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.myTournamentListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const myTournamentListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.myTournamentListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.MY_CONTEST_LIST:
            return myContestList(state, action);
        case actionTypes.MY_CONTEST_LIST_FAILED:
            return myContestListFailed(state, action);
        case actionTypes.MY_TOURNAMENT_LIST:
            return myTournamentList(state, action);
        case actionTypes.MY_TOURNAMENT_LIST_FAILED:
            return myTournamentListFailed(state, action);
        case actionTypes.RESET_MY_CONTEST_TOURNAMENT_STATE:
            return resetMyContestTournamentState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetMyContestTournamentState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.myContestListResponse = undefined;
        draft.model.myTournamentListResponse = undefined;
    })
}