import React, { useEffect, useRef, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Dimensions, Image, ScrollView, SafeAreaView, TextInput, Keyboard, Alert } from 'react-native'
import { RadioButton } from 'react-native-paper';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
const { height, width } = Dimensions.get('window');
import { showSuccessToastMessage, showErrorToastMessage, addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const Withdrawal = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const data = props.route.params;
    const refRBSheet = useRef('');
    const [amount, setAmount] = useState('');
    const [isAmazonPayLinked, setIsAmazonPayLinked] = useState(true);
    const [isBankAccountLinked, setIsBankAccountLinked] = useState(false);
    const [isUPIPaymentLinked, setIsUPIPaymentLinked] = useState(false);
    const [isPaytmLinked, setIsPaytmLinked] = useState(true);
    const [paymentMode, setPaymenMode] = useState('');
    const [isLoading, setLoading] = useState(false);
    let mode;
    let depositCash = parseFloat(global.profile.wallet && global.profile.wallet.depositedAmount);
    let winningCash = parseFloat(global.profile.wallet && global.profile.wallet.winningAmount);
    let bonusCash = parseFloat(global.profile.wallet && global.profile.wallet.bonusAmount);
    let totalCurrentBalance = depositCash + winningCash + bonusCash;

    // This Use-Effect function should call the Wallet API
    useEffect(() => {
        if (global.profile1.AmazonPayEnabled) {
            setPaymenMode(Constants.text_amazon_pay);
        } else {
            setPaymenMode(Constants.text_bank_account);
        }
        setLoading(true)
        dispatch(actionCreators.requestProfileDataForWithdraw())

        return () => {
            dispatch(actionCreators.resetWithdrawState())
        }
    }, []);

    // This function sets the Get Profile Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.withdraw.model }),
        shallowEqual
    );
    var { getProfileResponse, withdrawNowResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (getProfileResponse) {
            setLoading(false)
            if (getProfileResponse.bankInfo) {
                setIsBankAccountLinked(getProfileResponse.bankInfo)
            }

            if (getProfileResponse.upiInfo) {
                setIsUPIPaymentLinked(getProfileResponse.upiInfo)
            }
        }
        getProfileResponse = undefined
    }, [getProfileResponse])

    // This Use-Effect function should set the radio button as per their withsrawal type
    useEffect(() => {
        if (data && data.withdrawalType === Constants.text_bank_account) {
            setIsBankAccountLinked(true);
            setPaymenMode(Constants.text_bank_account);
        }

        if (data && data.withdrawalType === Constants.text_upi_payment) {
            setIsUPIPaymentLinked(true);
            setPaymenMode(Constants.text_upi_payment);
        }
    }, [props]);

    // This function should check the validation first and then call the Withdrawal API
    const checkValidation = () => {
        Keyboard.dismiss();
        let winningAmount = parseFloat(global.profile.wallet && global.profile.wallet.winningAmount && global.profile.wallet.winningAmount);
        let minAmount = global.profile1.WithdrawalMinAmount && global.profile1.WithdrawalMinAmount;
        let maxAmount = global.profile1.WithdrawalMaxAmount && global.profile1.WithdrawalMaxAmount;

        // let winningAmount = '200000';
        addLog('COND:::> ', amount >= winningAmount);
        addLog('Amount:::> ', amount);
        addLog('Winning Amount:::> ', winningAmount);
        addLog('parseFloat(amount)', parseFloat(amount))
        addLog('parseFloat(maxAmount)', parseFloat(maxAmount))
        addLog('parseFloat(amount) < parseFloat(maxAmount)', parseFloat(amount) < parseFloat(maxAmount))

        if (!amount.trim()) {
            showErrorToastMessage(Constants.error_enter_amount);
            return;
        } else if (amount == 0) {
            showErrorToastMessage(Constants.error_amount_should_not_zero);
            return;
        } else if (parseFloat(amount) > parseFloat(winningAmount)) {
            showErrorToastMessage(Constants.error_withdraw_more_than_the_winning_amount);
            return;
        } else if (parseFloat(amount) < parseFloat(minAmount)) {
            showErrorToastMessage(`${Constants.error_minimum_withdrawal_amount}${Constants.text_rupees}${global.profile1.WithdrawalMinAmount}`);
            return;
        } else if (parseFloat(amount) > parseFloat(maxAmount)) {
            showErrorToastMessage(`${Constants.error_maximum_withdrawal_amount}${Constants.text_rupees}${global.profile1.WithdrawalMaxAmount}`);
            return;
        }
        // else if (!mode.trim()) {
        //     showErrorToastMessage(Constants.error_select_payment_mode);
        //     return;
        // }
        callToWithdrawNowAPI(props);
    };

    // This function should call the Withdraw Now API
    const callToWithdrawNowAPI = (props) => {
        switch (paymentMode) {
            case Constants.text_bank_account:
                mode = '1'
                break;
            case Constants.text_upi_payment:
                mode = '2'
                break;
            case Constants.text_paytm:
                mode = '3'
                break;
            case Constants.text_amazon_pay:
                mode = '4'
                break;
            default:
                Alert.alert(Constants.error_invalid_payment_mode);
        }

        let payload = {
            amount: amount,
            mode: mode
        }
        setLoading(true)
        dispatch(actionCreators.requestWithdrawNow(payload))

        return () => {
            dispatch(actionCreators.resetWithdrawState())
        }
    }

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (withdrawNowResponse) {
            setLoading(false)
            // addLog("withdrawNowResponse==>", withdrawNowResponse);
            if (withdrawNowResponse.success === true) {
                showSuccessToastMessage(Constants.success_withdrawal)
                setAmount('');
                props.navigation.goBack();
                data.setAccountsData();
            } else {
                showSuccessToastMessage(withdrawNowResponse.errors[0].msg)
            }


        }
        withdrawNowResponse = undefined
    }, [withdrawNowResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Withdrawal Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_05]}> {Constants.header_withdrawal} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}
                    <ScrollView style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 10 }}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Current Balance */}
                        <View style={[flexDirectionStyles.row, colorStyles.redBackgroundColor, shadowStyles.shadowBackgroundForView, colorStyles.redShadowColor, borderStyles.buttonRoundedCorner, paddingStyles.padding_005, marginStyles.bottomMargin_02]}>

                            {/* Image - Winning Trophy */}
                            <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_winning_trophy.png')} />

                            {/* Text - Your Winnings */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_035, paddingStyles.padding_03]}>{Constants.text_your_winnings}</Text>

                            {/* Text - Your Winnings Balance */}
                            <Text style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05, fontFamilyStyles.extraBold, colorStyles.whiteColor, fontSizeStyles.fontSize_05]}>{Constants.text_rupees}{winningCash} </Text>
                        </View>

                        {/* Text - Amount */}
                        <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignTextLeft, alignmentStyles.alignTextFromLeftRight, fontSizeStyles.fontSize_035, marginStyles.topMargin_02]}> {Constants.text_amount_header} </Text>

                        {/* Text Input - Amount */}
                        <TextInput style={[borderStyles.inputTextRoundedCorner, inputTextStyles.inputText, fontFamilyStyles.semiBold, colorStyles.greyColor, colorStyles.greyBorderColor, alignmentStyles.alignTextLeft, fontSizeStyles.fontSize_04, marginStyles.topMargin_01]}
                            keyboardType="numeric"
                            placeholder={Constants.validation_amount}
                            value={amount}
                            onChangeText={text => setAmount(text)} />

                        {/* Text - Minimum ₹ 200 and Maximum ₹ 2,00,000 allowed per day */}
                        <Text style={[positionStyles.relative, alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_035, alignmentStyles.alignTextCenter, marginStyles.topMargin_03, marginStyles.leftMargin_05, marginStyles.rightMargin_05]}> Minimum {Constants.text_rupees} {global.profile1.WithdrawalMinAmount} and Maximum {Constants.text_rupees} {global.profile1.WithdrawalMaxAmount} allowed per day. </Text>

                        {/* Text - Withdrawal Method */}
                        <Text style={[positionStyles.relative, alignmentStyles.alignSelfCenter, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_045, marginStyles.topMargin_03, !global.profile1.AmazonPayEnabled && marginStyles.bottomMargin_03]}> {Constants.text_withdrawal_method} </Text>

                        {/* Wrapper - Amazon Pay */}
                        {global.profile1.AmazonPayEnabled &&
                            <View style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.margin_01, marginStyles.topMargin_03]}>

                                {/* Wrapper - Amazon Pay Image */}
                                <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                    {/* Image - Amazon Pay */}
                                    <Image style={[heightStyles.height_21, widthStyles.width_30_new, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_amazon_pay.png')} />
                                </View>

                                {/* Text - Amazon Pay */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_amazon_pay}</Text>

                                {/* Wrapper - Amazon Pay Link Account */}
                                <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                    {!isAmazonPayLinked ?
                                        // TouchableOpacity - Amazon Pay (Link Account) Button Click Event
                                        <TouchableOpacity style={[alignmentStyles.alignItemsCenter,]} activeOpacity={0.5}>

                                            {/* Text - Button (Link Account) */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_03]}>{Constants.action_link_account}</Text>
                                        </TouchableOpacity>
                                        :
                                        // TouchableOpacity - Amazon Pay Radio Buttom Click Event
                                        <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_amazon_pay ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                            onPress={() => setPaymenMode(Constants.text_amazon_pay)} activeOpacity={0.5}>

                                            {paymentMode == Constants.text_amazon_pay && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        }

                        {/* Wrapper - Bank Account */}
                        {isBankAccountLinked.status && isBankAccountLinked.status == 'validate' && paymentMode == Constants.text_bank_account ? (
                            <View style={[flexDirectionStyles.column, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_10, marginStyles.margin_01, marginStyles.topMargin_01, paddingStyles.bottomPadding_01]}>

                                {/* Wrapper - Bank Account Details */}
                                <View style={[flexDirectionStyles.row]}>

                                    {/* Wrapper - Bank Account Image */}
                                    <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                        {/* Image - Bank Account */}
                                        <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_bank_account.png')} />
                                    </View>

                                    {/* Text - Bank Account */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, isBankAccountLinked.status && isBankAccountLinked.status == 'validate' ? fontSizeStyles.fontSize_045 : fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_bank_account}</Text>

                                    {/* Wrapper - Bank Account Link Account */}
                                    <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                        {/* TouchableOpacity - Bank Account Radio Buttom Click Event */}
                                        <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_bank_account ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                            onPress={() => setPaymenMode(Constants.text_bank_account)} activeOpacity={0.5}>

                                            {paymentMode == Constants.text_bank_account && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                {/* Wrapper - Bank Account Details */}
                                <View style={[marginStyles.leftMargin_60, marginStyles.rightMargin_5]}>

                                    {/* Text - Name */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14]}>{Constants.text_name_header} {isBankAccountLinked.accountName}</Text>

                                    {/* Text - Bank Name */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_10]}>{Constants.text_bank_name_header} {isBankAccountLinked.bankName}</Text>

                                    {/* Text - Account Number */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_10]} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_bank_account_header} {isBankAccountLinked.accountNumber}</Text>
                                </View>
                            </View>
                        ) :
                            // Wrapper - Bank Account Selection
                            <View style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.margin_01, marginStyles.topMargin_01]}>

                                {/* Wrapper - Bank Account Image */}
                                <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                    {/* Image - Bank Account */}
                                    <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_bank_account.png')} />
                                </View>

                                {/* Text - Bank Account */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_bank_account}</Text>

                                {/* Wrapper - Bank Account Link Account */}
                                <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                    {!isBankAccountLinked ?
                                        // TouchableOpacity - Bank Account (Link Account) Button Click Event
                                        <TouchableOpacity style={[alignmentStyles.alignItemsCenter]} activeOpacity={0.5}
                                            onPress={() => props.navigation.navigate(Constants.nav_bank_transfer, { withdrawalType: Constants.text_bank_account })}>

                                            {/* Text - Button (Link Account) */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_03]}>{Constants.action_link_account}</Text>
                                        </TouchableOpacity>
                                        :
                                        isBankAccountLinked.status && isBankAccountLinked.status == 'validate' ?
                                            // TouchableOpacity - Bank Account Radio Buttom Click Event
                                            <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_bank_account ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                                onPress={() => setPaymenMode(Constants.text_bank_account)} activeOpacity={0.5}>

                                                {paymentMode === Constants.text_bank_account && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                            </TouchableOpacity>
                                            :
                                            // Text - Pending 
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_12, alignmentStyles.alignItemsCenter]}>{Constants.text_pending}</Text>
                                    }
                                </View>
                            </View>
                        }

                        {/* Wrapper - UPI Payment */}
                        {isUPIPaymentLinked.status && isUPIPaymentLinked.status == 'validate' && paymentMode == Constants.text_upi_payment ? (
                            <View style={[flexDirectionStyles.column, colorStyles.yellowBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_10, marginStyles.margin_01, marginStyles.topMargin_01, paddingStyles.bottomPadding_01]}>

                                {/* Wrapper - UPI Payment Details */}
                                <View style={[flexDirectionStyles.row]}>

                                    {/* Wrapper - UPI Payment Image */}
                                    <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                        {/* Image - UPI Payment */}
                                        <Image style={[heightStyles.height_8_new, widthStyles.width_30_new, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_upi.png')} />
                                    </View>

                                    {/* Text - UPI Payment */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, isUPIPaymentLinked.status && isUPIPaymentLinked.status == 'validate' ? fontSizeStyles.fontSize_045 : fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_upi_payment}</Text>

                                    {/* Wrapper - UPI Payment Link Account */}
                                    <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                        {/* TouchableOpacity - UPI Payment Radio Buttom Click Event */}
                                        <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_upi_payment ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                            onPress={() => setPaymenMode(Constants.text_upi_payment)} activeOpacity={0.5}>

                                            {paymentMode == Constants.text_upi_payment && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                {/* Wrapper - UPI Payment Details */}
                                <View style={[marginStyles.leftMargin_65, marginStyles.rightMargin_5]}>

                                    {/* Text - UPI Id */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14]}>{Constants.text_upi_id_header} {isUPIPaymentLinked.upiID}</Text>
                                </View>
                            </View>
                        ) :
                            // Wrapper - UPI Payment Selection
                            <View style={[flexDirectionStyles.column, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_30, marginStyles.margin_01, marginStyles.topMargin_01]}>

                                {/* Wrapper - UPI Payment Selection */}
                                <View style={[flexDirectionStyles.row]}>

                                    {/* Wrapper - UPI Payment Image */}
                                    <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                        {/* Image - UPI Payment */}
                                        <Image style={[heightStyles.height_8_new, widthStyles.width_30_new, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_upi.png')} />
                                    </View>

                                    {/* Text - UPI Payment */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, isUPIPaymentLinked.status && isUPIPaymentLinked.status == 'validate' && paymentMode == Constants.text_upi_payment ? fontSizeStyles.fontSize_045 : fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_upi_payment}</Text>

                                    {/* Wrapper - UPI Payment Link Account */}
                                    <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                        {!isUPIPaymentLinked ?
                                            // TouchableOpacity - UPI Payment (Link Account) Button Click Event
                                            <TouchableOpacity style={[alignmentStyles.alignItemsCenter]} activeOpacity={0.5}
                                                onPress={() => props.navigation.navigate(Constants.nav_bank_transfer, { withdrawalType: Constants.text_upi_payment })}>

                                                <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_03]}>{Constants.action_link_account}</Text>
                                            </TouchableOpacity>
                                            :
                                            isUPIPaymentLinked.status && isUPIPaymentLinked.status == 'validate' ?
                                                // TouchableOpacity - UPI Payment Radio Buttom Click Event
                                                <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_upi_payment ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]}
                                                    onPress={() => setPaymenMode(Constants.text_upi_payment)} activeOpacity={0.5}>

                                                    {paymentMode == Constants.text_upi_payment && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                                </TouchableOpacity>
                                                :
                                                // Text - Pending
                                                <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_12, alignmentStyles.alignItemsCenter]}>{Constants.text_pending}</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                        }

                        {/* Wrapper - Paytm */}
                        {global.profile1.PaytmEnabled &&
                            <View style={[flexDirectionStyles.row, colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, marginStyles.margin_01, marginStyles.topMargin_01]}>

                                {/* Wrapper - Paytm Image */}
                                <View style={[widthStyles.width_50, alignmentStyles.alignSelfCenter]}>

                                    {/* Image - Paytm */}
                                    <Image style={[heightStyles.height_10_new, widthStyles.width_30_new, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_05]} source={require('../../assets/images/ic_paytm.png')} />
                                </View>

                                {/* Text - Paytm */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_035, paddingStyles.padding_04]}>{Constants.text_paytm}</Text>

                                {/* Wrapper - Paytm Link Account */}
                                <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]}>

                                    {!isPaytmLinked ?
                                        // TouchableOpacity - Paytm (Link Account) Button Click Event
                                        <TouchableOpacity style={[alignmentStyles.alignItemsCenter,]} activeOpacity={0.5}
                                            onPress={() => props.navigation.navigate(Constants.nav_withdrawal_paytm)}>

                                            {/* Text - Button (Link Account) */}
                                            <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_03]}>{Constants.action_link_account}</Text>
                                        </TouchableOpacity>
                                        :
                                        // TouchableOpacity - Paytm Radio Buttom Click Event
                                        <TouchableOpacity style={[heightStyles.height_16, widthStyles.width_16, borderStyles.borderRadius_100, borderStyles.borderWidth_2, paymentMode == Constants.text_paytm ? colorStyles.redBorderColor : colorStyles.darkGreyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} onPress={() => setPaymenMode(Constants.text_paytm)} activeOpacity={0.5}>

                                            {paymentMode == Constants.text_paytm && <View style={[heightStyles.height_6, widthStyles.width_6, borderStyles.borderRadius_100, colorStyles.redBackgroundColor]} />}
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        }
                    </ScrollView>

                    <View style={{ width: '100%' }}>

                        <View style={{ marginLeft: 20, marginRight: 20 }}>

                            {/* TouchableOpacity - Withdraw Now Button Click Event */}
                            <TouchableOpacity style={[widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, alignmentStyles.alignSelfCenter, { marginBottom: 20 }]}
                                onPress={() => checkValidation()} activeOpacity={0.5}>

                                {/* Text - Button (Withdraw Now) */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_045, paddingStyles.padding_03, marginStyles.leftMargin_02, otherStyles.capitalize]}>{Constants.action_withdraw_now}</Text>

                                {/* Image - Right Arrow */}
                                <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_03]} />
                            </TouchableOpacity>
                        </View>

                        {checkIsSponsorAdsEnabled('withdrawal') &&
                            <SponsorBannerAds screenCode={'withdrawal'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('withdrawal')} />
                        }
                    </View>
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default Withdrawal;