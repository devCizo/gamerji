import produce from 'immer';
import * as actionTypes from '../../actions/more/legalityAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        legalityResponse: undefined
    },
    error: false
};

{ /* Request - Legality */ }
const legalityRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.legalityResponse = action.model;
    });
};

{ /* Request Failure - Legality */ }
const legalityRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.legalityResponse = action.model;
    });
};

{ /* Reset State - Legality */ }
const resetLegalityState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.legalityResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REQUEST_LEGALITY:
            return legalityRequest(state, action);
        case actionTypes.REQUEST_LEGALITY_FAILED:
            return legalityRequestFailed(state, action);
        case actionTypes.RESET_LEGALITY_STATE:
            return resetLegalityState(state);
        default:
            return state;
    }
};

export default reducer;