import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Check Squad */ }
export const checkSquad = (viewModel) => {
    return {
        type: CHECK_SQUAD,
        model: viewModel
    };
};

{ /* Request Failure - All Games Banner List */ }
export const checkSquadFailed = (viewModel) => {
    return {
        type: CHECK_SQUAD_FAILED,
        model: viewModel
    };
};

{ /* Request - Contest List */ }
export const joinContestWithSquad = (viewModel) => {
    return {
        type: JOIN_CONTEST_WITH_SQUAD,
        model: viewModel
    };
};

{ /* Request Failure - All Games Banner List */ }
export const joinContestWithSquadFailed = (viewModel) => {
    return {
        type: JOIN_CONTEST_WITH_SQUAD_FAILED,
        model: viewModel
    };
};

{ /* Request - All Games Banner */ }
export const requestCheckSquad = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.USER_CHECK.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(checkSquad(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(checkSquadFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - All Games Banner */ }
export const requestJoinContestWithSquad = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.JOIN_CONTEST_TOURNAMENT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(joinContestWithSquad(response.data));
            })
            .catch((error) => {
                addLog("URL:::> ", error.response.config.baseURL + error.response.config.url);
                addLog(error.response.data);
                dispatch(joinContestWithSquadFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetSquadRegistrationState = () => {
    return {
        type: RESET_SQUAD_REGISTRATION_STATE,
    };
};


export const CHECK_SQUAD = 'CHECK_SQUAD';
export const CHECK_SQUAD_FAILED = 'CHECK_SQUAD_FAILED';
export const JOIN_CONTEST_WITH_SQUAD = 'JOIN_CONTEST_WITH_SQUAD';
export const JOIN_CONTEST_WITH_SQUAD_FAILED = 'JOIN_CONTEST_WITH_SQUAD_FAILED';
export const RESET_SQUAD_REGISTRATION_STATE = 'RESET_SQUAD_REGISTRATION_STATE';
