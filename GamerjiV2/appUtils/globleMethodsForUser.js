

export const isIndianUser = () => {
    if (global.profile1 && global.profile1.country && global.profile1.country.code == 'IN') {
        return true
    }
    return false
}

export const getUserCountry = () => {
    if (global.profile1 && global.profile1.country) {
        return global.profile1.country
    }
    return undefined
}