import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { Constants } from '../../../appUtils/constants';

{ /* Action Types */ }
export const GAMERJI_POINTS_LIST = 'GAMERJI_POINTS_LIST';
export const GAMERJI_POINTS_LIST_FAILED = 'GAMERJI_POINTS_LIST_FAILED';
export const RESET_GAMERJI_POINTS_LIST_STATE = 'RESET_GAMERJI_POINTS_LIST_STATE';

export const GAMERJI_POINTS_CATEGORY_LIST = 'GAMERJI_POINTS_CATEGORY_LIST';
export const GAMERJI_POINTS_CATEGORY_LIST_FAILED = 'GAMERJI_POINTS_CATEGORY_LIST_FAILED';
export const RESET_GAMERJI_POINTS_CATEGORY_LIST_STATE = 'RESET_GAMERJI_POINTS_CATEGORY_LIST_STATE';

{ /* Request - Gamerji Points List */ }
export const gamerjiPointsList = (viewModel) => {
    return {
        type: GAMERJI_POINTS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Gamerji Points List */ }
export const gamerjiPointsListFailed = (viewModel) => {
    return {
        type: GAMERJI_POINTS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Gamerji Points List */ }
export const resetGamerjiPointsListState = () => {
    return {
        type: RESET_GAMERJI_POINTS_LIST_STATE,
    };
};

{ /* Request - Gamerji Points */ }
export const requestGamerjiPoints = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GAMERJI_POINTS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gamerjiPointsList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(gamerjiPointsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Gamerji Points Category List */ }
export const gamerjiPointCategoryList = (viewModel) => {
    return {
        type: GAMERJI_POINTS_CATEGORY_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Gamerji Points Category List */ }
export const gamerjiPointCategoryListFailed = (viewModel) => {
    return {
        type: GAMERJI_POINTS_CATEGORY_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Gamerji Points Category List */ }
export const resetGamerjiPointCategoryListState = () => {
    return {
        type: RESET_GAMERJI_POINTS_CATEGORY_LIST_STATE,
    };
};

{ /* Request - Gamerji Points Category */ }
export const requestGamerjiPointCategories = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GAMERJI_POINTS_CATEGORY.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gamerjiPointCategoryList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(gamerjiPointCategoryListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};