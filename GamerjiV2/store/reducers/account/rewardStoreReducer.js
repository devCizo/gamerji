import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/account/rewardStoreAction';

const initialState = {
    model: {
        rewardCategoryListResponse: undefined,
        categoryProductListResponse: undefined,
    },
    error: false
};

const getRewardCategory = (state, action) => {
    return produce(state, (draft) => {
        draft.model.rewardCategoryListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getRewardCategoryFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.rewardCategoryListResponse = action.model;
    });
};

const getRewardCategoryProduct = (state, action) => {
    return produce(state, (draft) => {
        draft.model.categoryProductListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getRewardCategoryProductFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.categoryProductListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REWARD_CATEGORY_LIST:
            return getRewardCategory(state, action);
        case actionTypes.REWARD_CATEGORY_LIST_FAILED:
            return getRewardCategoryFailed(state, action);
        case actionTypes.REWARD_CATEGORY_PRODUCT_LIST:
            return getRewardCategoryProduct(state, action);
        case actionTypes.REWARD_CATEGORY_PRODUCT_LIST_FAILED:
            return getRewardCategoryProductFailed(state, action);
        case actionTypes.RESET_REWARD_STORE_STATE:
            return resetRewardStoreState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetRewardStoreState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.rewardCategoryListResponse = undefined;
        draft.model.categoryProductListResponse = undefined;
    })
}