import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinTournament/tournamentListAction';

const initialState = {
    model: {
        tournamentListResponse: undefined,
        walletUsageLimitForTournamentResponse: undefined
    },
    error: false
};

const tournamentList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.tournamentListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const tournamentListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.tournamentListResponse = action.model;
    });
};

const walletUsageLimitForTournament = (state, action) => {
    return produce(state, (draft) => {
        draft.model.walletUsageLimitForTournamentResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const walletUsageLimitForTournamentFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.walletUsageLimitForTournamentResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOURNAMENT_LIST:
            return tournamentList(state, action);
        case actionTypes.TOURNAMENT_LIST_FAILED:
            return tournamentListFailed(state, action);
        case actionTypes.WALLET_USAGE_LIMIT_FOR_TOURNAMENT:
            return walletUsageLimitForTournament(state, action);
        case actionTypes.WALLET_USAGE_LIMIT_FOR_TOURNAMENT_FAILED:
            return walletUsageLimitForTournamentFailed(state, action);
        case actionTypes.RESET_TOURNAMENT_LIST_STATE:
            return resetTounamentListState(state);
        default:
            return state;
    }
};

export default reducer;

const resetTounamentListState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.tournamentListResponse = undefined
        draft.model.walletUsageLimitForTournamentResponse = undefined
    })
}