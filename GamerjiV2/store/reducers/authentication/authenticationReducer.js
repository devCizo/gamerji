import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/authentication/authenticationAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        authenticationResponse: undefined,
        countryListResponse: undefined,
    }
};

{ /* Request - User Login OTP */ }
const userLoginOTPRequest = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.authenticationResponse = action.model;
    });
};

{ /* Request Failure - User Login OTP */ }
const userLoginOTPRequestFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.authenticationResponse = action.model;
    });
};

{ /* Set - User Data */ }
const setUserData = (state, action) => {
    return produce(state, (draft) => {
        draft.authenticationResponse = action.model;
        draft.error = false;
    });
};

{ /* Validate Failure - User Login OTP */ }
const userLoginOTPValidateFailed = (state) => {
    return produce(state, (draft) => {
        draft.error = true;
    });
};

const countryList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.countryListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const countryListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.countryListResponse = action.model;
    });
};



{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_LOGIN_REQUEST_OTP:
            return userLoginOTPRequest(state, action);
        case actionTypes.USER_LOGIN_REQUEST_OTP_FAILED:
            return userLoginOTPRequestFailed(state, action);
        case actionTypes.SET_USER_DATA:
            return setUserData(state, action);
        case actionTypes.USER_LOGIN_VALIDATE_OTP_FAILED:
            return userLoginOTPValidateFailed(state, action);
        case actionTypes.COUNTRY_LIST:
            return countryList(state, action);
        case actionTypes.COUNTRY_LIST_FAILED:
            return countryListFailed(state, action);
        case actionTypes.RESET_AUTHENTICATION_STATE:
            return resetAuthenticationState(state);
        default:
            return state;
    }
};

export default reducer;
{ /* Reset State - Authentication */ }
const resetAuthenticationState = (state) => {
    addLog('cleaned')
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.authenticationResponse = undefined;
        draft.model.countryListResponse = undefined;
    })
}