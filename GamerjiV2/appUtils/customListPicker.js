import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet, Platform, FlatList, TextInput } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import * as actionCreators from '../store/actions/index';

import colors from '../assets/colors/colors';
import { fontFamilyStyleNew } from './commonStyles';
import { addLog } from './commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";


const CustomListPicker = (props) => {

    const dispatch = useDispatch();
    const [searchText, setSearchText] = useState('')
    const [collegeList, setCollegeList] = useState([]);

    var list = props.list
    var title = props.title
    const fetchCollegeList = () => {
        dispatch(actionCreators.requestCollegeList({ q: searchText, skip: 0, limit: 100 }))
    }
    const { currentStateCollegiate } = useSelector(
        (state) => ({ currentStateCollegiate: state.collegiate.model }),
        shallowEqual
    );
    var { collegeListResponse, submitCollegeResponse } = currentStateCollegiate

    //College List Response
    useEffect(() => {
        if (collegeListResponse) {
            // setLoading(false)
            if (collegeListResponse.list) {
                setCollegeList(collegeListResponse.list)

                // if (profileResponse && profileResponse.college) {
                //     let index = collegeListResponse.list.findIndex(obj => obj._id == profileResponse.college)
                //     if (index > -1) {
                //         setSelectedCollege(collegeListResponse.list[index])
                //     }
                // }
            }
        }
        collegeListResponse = undefined
    }, [collegeListResponse])

    const selectItem = item => {
        props.setCustomListPickerSelectedItem(item)
        props.setOpenCustomListPicker(false)
    }
    useEffect(() => {
        return () => {
            dispatch(actionCreators.resetCollegiateState())
        }
    }, [])
    return (
        <>
            <View style={{ height: "70%", backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50 }} >

                {/* Close Button */}
                <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                    <Text style={{ flex: 1, marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} >{title}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.setOpenCustomListPicker(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>
                <View style={{ marginTop: 30, marginBottom: 30 }}>



                    <View style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, flexDirection: 'row' }} >
                        <TextInput
                            style={{ flex: 1, paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: colors.black }}
                            placeholder='Search...' placeholderTextColor={'#70717A'}
                            onChangeText={text => setSearchText(text)}
                            autoCorrect={false}
                            editable={true}
                        >

                        </TextInput>

                        <TouchableOpacity style={{ marginRight: 0, height: 44, width: 84, backgroundColor: colors.black, borderRadius: 23, alignItems: 'center', justifyContent: 'center' }} activeOpacity={0.5} onPress={() => fetchCollegeList()} >

                            <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }} >Search</Text>

                        </TouchableOpacity>
                    </View>



                </View>
                {collegeList ?
                    <FlatList
                        style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, flex: 1 }}
                        data={collegeList}
                        renderItem={({ item, index }) => <CustomListPickerItem item={item} selectItem={selectItem} />}
                        showsVerticalScrollIndicator={false}
                    />
                    : <></>

                }

            </View>
        </>
    )
}

const CustomListPickerItem = (props) => {

    let item = props.item

    return (
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.5} onPress={() => props.selectItem(item)} >

            <Text style={{ marginLeft: 12, color: colors.black, fontSize: 15, fontFamily: fontFamilyStyleNew.semiBold, }} >{item.name}</Text>
            <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#D5D7E3' }} />

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.white,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
})

export default CustomListPicker;