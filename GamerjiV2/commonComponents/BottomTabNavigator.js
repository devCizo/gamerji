import React from 'react';
import { StyleSheet, Dimensions, Image } from 'react-native';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AllGames from '../components/AllGames/AllGames';
import CollegeLeagues from '../components/collegiate/collegiate';
import Profile from '../components/Profile/Profile';
import ProfileScreen from '../components/Profile/myProfileNew';
import WorldOfEsports from '../components/WorldOfEsports/WorldOfEsports';
import More from '../components/More/More';
import WalletImage from '../assets/images/ic_wallet.svg';

import { useSafeAreaInsets } from 'react-native-safe-area-context';
import accountNew from '../components/Account/accountNew';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {

    const insets = useSafeAreaInsets();

    return (
        <Tab.Navigator
            tabBarOptions={{
                style: [styles.tabBar, { paddingBottom: insets.bottom + 5 }],
                activeTintColor: colors.yellow,
                inactiveTintColor: colors.color_input_text,
                showIcon: true,
                showLabel: true,
            }}>

            {/* Navigation Menu - All Games */}
            <Tab.Screen
                name={Constants.nav_all_games}
                component={AllGames}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Image style={{ height: 22, width: 22, tintColor: color, marginBottom: 5 }} source={require('../assets/images/ic_joystick.png')} />
                    ),
                }} />

            {/* Navigation Menu - World of Esports */}
            <Tab.Screen
                name={Constants.nav_world_of_esports}
                component={WorldOfEsports}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Image style={{ height: 22, width: 22, tintColor: color, marginBottom: 3 }}
                            source={require('../assets/images/ic_world.png')} />
                    ),
                }} />

            {/* Navigation Menu - Profile */}
            <Tab.Screen
                name={'Profile'}
                component={ProfileScreen}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Image style={{ marginTop: -35, height: 60, width: 60 }}
                            source={require('../assets/images/ic_profile_tab.png')} />
                    ),
                }} />

            {/* Navigation Menu - Account */}
            <Tab.Screen
                // name={Constants.nav_college_leagues}
                // component={CollegeLeagues}
                name={'Account'}
                component={accountNew}
                options={{
                    tabBarIcon: ({ color }) => (
                        // <Image style={{ height: 22, width: 22, tintColor: color, marginBottom: 3 }} source={require('../assets/images/college-leagues-tab-icon.png')} />
                        <Image style={{ height: 22, width: 22, tintColor: color, marginBottom: 3 }} source={require('../assets/images/account-tab-icon.png')} />
                        // <WalletImage style={{ height: 22, width: 22, tintColor: color, marginBottom: 5 }} />
                    ),
                }} />

            {/* Navigation Menu - More */}
            <Tab.Screen
                name={Constants.nav_more}
                component={More}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Image style={{ height: 22, width: 22, tintColor: color, marginBottom: 3 }}
                            source={require('../assets/images/ic_more.png')} />
                    ),
                }} />
        </Tab.Navigator>
    );
};

const styles = StyleSheet.create({
    tabBar: {
        backgroundColor: colors.black,
        borderTopWidth: 0,
        // paddingBottom: 5
    },
});

export default BottomTabNavigator;