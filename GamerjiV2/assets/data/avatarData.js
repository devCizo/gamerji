import React from 'react'

const avatarData = [
    {
        id: '1',
        image: require('../../assets/images/avatar_dummy1.png'),
    },
    {
        id: '2',
        image: require('../../assets/images/avatar_dummy2.png'),
    },
    {
        id: '3',
        image: require('../../assets/images/avatar_dummy3.png'),
    },
    {
        id: '4',
        image: require('../../assets/images/avatar_dummy1.png'),
    },
    {
        id: '5',
        image: require('../../assets/images/avatar_dummy2.png'),
    },
    {
        id: '6',
        image: require('../../assets/images/avatar_dummy3.png'),
    },
];

export default avatarData;