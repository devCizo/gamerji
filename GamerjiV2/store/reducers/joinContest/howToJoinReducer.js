import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinContest/howToJoinAction';

const initialState = {
    model: {
        howToJoinListResponse: undefined,
    },
    error: false
};

const getHowToJoinList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.howToJoinListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getHowToJoinListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.howToJoinListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HOW_TO_JOIN_LIST:
            return getHowToJoinList(state, action);
        case actionTypes.HOW_TO_JOIN_LIST_FAILED:
            return getHowToJoinListFailed(state, action);
        case actionTypes.RESET_HOW_TO_JOIN_STATE:
            return resetHowToJoinState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetHowToJoinState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.howToJoinListResponse = undefined;
    })
}