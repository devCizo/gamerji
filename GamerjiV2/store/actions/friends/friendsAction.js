import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const followUser = (viewModel) => {
    return {
        type: FOLLOW_USER,
        model: viewModel
    };
};

export const followUserFailed = (viewModel) => {
    return {
        type: FOLLOW_USER_FAILED,
        model: viewModel
    };
};

export const requestFollowUser = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.FRIENDS.FOLLOW_USER);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.FRIENDS.FOLLOW_USER, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(followUser(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(followUserFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const followerList = (viewModel) => {
    return {
        type: FOLLOWER_LIST,
        model: viewModel
    };
};

export const followerListFailed = (viewModel) => {
    return {
        type: FOLLOWER_LIST_FAILED,
        model: viewModel
    };
};

export const requestFollowerList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.FRIENDS.FOLLOWERS);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.FRIENDS.FOLLOWERS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(followerList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(followerListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const followingList = (viewModel) => {
    return {
        type: FOLLOWING_LIST,
        model: viewModel
    };
};

export const followingListFailed = (viewModel) => {
    return {
        type: FOLLOWING_LIST_FAILED,
        model: viewModel
    };
};

export const requestFollowingList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.FRIENDS.FOLLOWINGS);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.FRIENDS.FOLLOWINGS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(followingList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(followingListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const resetFriendsState = () => {
    return {
        type: RESET_FRIENDS_STATE,
    };
};

export const FOLLOW_USER = 'FOLLOW_USER';
export const FOLLOW_USER_FAILED = 'FOLLOW_USER_FAILED';
export const FOLLOWER_LIST = 'FOLLOWER_LIST';
export const FOLLOWER_LIST_FAILED = 'FOLLOWER_LIST_FAILED';
export const FOLLOWING_LIST = 'FOLLOWING_LIST';
export const FOLLOWING_LIST_FAILED = 'FOLLOWING_LIST_FAILED';
export const RESET_FRIENDS_STATE = 'RESET_FRIENDS_STATE';