import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/account/rewardStoreRedeemAction';

const initialState = {
    model: {
        applyRewardResponse: undefined,
    },
    error: false
};

const applyReward = (state, action) => {
    return produce(state, (draft) => {
        draft.model.applyRewardResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const applyRewardFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.applyRewardResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.APPLY_REWARD:
            return applyReward(state, action);
        case actionTypes.APPLY_REWARD_FAILED:
            return applyRewardFailed(state, action);
        case actionTypes.RESET_REWARD_STORE_REDEEM_STATE:
            return resetRewardStoreRedeemState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetRewardStoreRedeemState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.applyRewardResponse = undefined;
    })
}