import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions, FlatList } from 'react-native'
import { showMessage } from 'react-native-flash-message';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { checkIsSponsorAdsEnabled, showSuccessToastMessage, addLog } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import colors from '../../assets/colors/colors';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomMarquee from '../../appUtils/customMarquee';
import * as generalSetting from '../../webServices/generalSetting';
const LevelInfoPopup = (props) => {

    const [levelList, setLevelList] = useState([]);
    const [isLoading, setLoading] = useState(false);

    const dispatch = useDispatch();
    useEffect(() => {

        setLoading(true)
        // addLog("fetchUserProfile==>");
        let payload = {
            skip: 0,
            limit: 100,
            sort: "asc",
            sortBy: "num"
        }
        dispatch(actionCreators.requestLevelList(payload))

        // return () => {
        // }
    }, []);
    const { currentState } = useSelector(
        (state) => ({ currentState: state.levelList.model }),
        shallowEqual
    );
    var { levelListResponse } = currentState
    // Profile Response
    useEffect(() => {
        if (levelListResponse) {
            setLoading(false)

            addLog("levelListResponse==>", levelListResponse)
            setLevelList(levelListResponse.list)


        }
    }, [levelListResponse])
    return (
        <View style={styles.mainContainer}>
            {/* Main View */}
            <View style={styles.mainContainer}>

                {/* Header View */}
                <View style={styles.headerContainer}>

                    <Text style={{ marginLeft: 60, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center' }}>Levels</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() => props.setOpenLevelList(false)} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Center Content */}
                <View style={styles.centerContentContainer} >

                    <View style={{ width: "94%", backgroundColor: colors.black, marginTop: 6, marginBottom: 6, marginLeft: 20, marginRight: 20, height: 50, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end' }} >
                        <View style={{ marginLeft: 0, flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white, }}  > Badge</Text>
                        </View>
                        <View style={{ marginLeft: 0, flex: 1, width: 40, alignItems: "center", justifyContent: "center" }}>
                            <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white, }}  > Level</Text>
                        </View>


                        <View style={{ marginLeft: 0, flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white }}  >Points</Text>
                        </View>


                    </View >
                    <View style={{ marginTop: 0, marginLeft: 20, marginRight: 20, flex: 1, overflow: 'hidden' }}>
                        <FlatList
                            style={{}}
                            data={levelList}
                            renderItem={({ item, index }) => <LevelItem item={item} index={index} />}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </View>

            {
                checkIsSponsorAdsEnabled('infoPopup') &&
                <SponsorBannerAds screenCode={'infoPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('infoPopup')} />
            }
        </View >
    );
}
const LevelItem = (props) => {

    const item = props.item

    const removeUnFollowUser = () => {
        props.removeUnFollowUser(props.index)
    }

    return (
        <View style={{ width: "100%", marginTop: 2, marginBottom: 2, height: 47, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end', borderBottomColor: colors.black, borderBottomWidth: 0.5 }} >

            <View style={{ marginLeft: 0, flex: 1, width: 40, alignItems: "center", justifyContent: "center" }}>
                {item.featuredImage ?
                    <Image style={{ marginLeft: 8, width: 40, height: 40, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + item.featuredImage.default }} />
                    :
                    <Text></Text>
                }
            </View>
            <View style={{ marginLeft: 0, flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, color: colors.black, }}  > {item.num}</Text>
            </View>


            <View style={{ marginLeft: 0, flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14, color: colors.black }}  > {item.startPoint} - {item.endPoint}</Text>
            </View>


        </View >
    )
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.yellow,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        height: 400,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    centerContentContainer: {
        marginTop: 5,
        alignSelf: 'center',
        marginBottom: 65
    },
})

export default LevelInfoPopup;