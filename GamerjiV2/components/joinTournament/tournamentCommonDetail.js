import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import { addLog } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { ProgressBar } from 'react-native-paper';
import moment from 'moment';
import Modal from 'react-native-modal';
import WiningPrizePool from '../JoinContest/WiningPrizePool'
import TournamentRulesPopup from './tournamentRulesPopup'
import CustomMarquee from '../../appUtils/customMarquee'
import * as generalSetting from '../../webServices/generalSetting'

const TournamentCommonDetail = (props) => {

    var tournament = props.tournament
    var isFromTournamentDetail = props.isFromTournamentDetail
    var currency = props.currency
    const [isVisibleWinningPrizePoolPopup, setWinningPrizePoolPopup] = useState(false)
    const [isVisibleTournamentRulesPopup, setTournamentRulesOpen] = useState(false)

    let getProgressValue = () => {
        if (tournament.totalJoinedPlayers && tournament.totalPlayers) {
            return tournament.totalJoinedPlayers / tournament.totalPlayers
        } else {
            return 0
        }
    }

    let getTitlesSubViewWidth = () => {

        let counts = findEnabledTitles().length
        if (counts >= 3) {
            return '33.33%'
        }
        if (counts >= 2) {
            return '50%'
        }
        return '100%'
    }

    let findEnabledTitles = () => {
        if (tournament.titles) {
            let filtered = tournament.titles.filter(item => {
                if (item.isSelection && item.isSelection == true) {
                    return item
                }
            })
            return filtered ? filtered : []
        }
        return []
    }

    let findTitleKeyAndValue = (index, type) => {

        let titles = findEnabledTitles()

        if (titles.length >= index + 1) {
            if (type == 'title') { return titles[index].name }
            if (type == 'value') { return titles[index].value }
        }
    }

    let totalRemainingPlayers = () => {
        let players = tournament.totalPlayers - tournament.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' remaining'
    }

    let totalJoinedPlayersPlayers = () => {
        let players = tournament.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' joined'
    }

    let closeWiningPrizePool = () => {
        setWinningPrizePoolPopup(false)
    }

    const getCurrencyImage = () => {

        return (
            currency && currency.code == 'coin' ?
                <Image style={{ height: 12, width: 12, resizeMode: 'contain' }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                :
                // <Image style={{ height: 12, width: 12, tintColor: colors.white }} source={currency && currency.img && currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + currency.img.default }} />
                <Text style={{ fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{currency.symbol}</Text>
        )
    }

    return (

        <View style={{ flex: 1, alignSelf: 'stretch', marginBottom: 35 }}>

            {/* Title View */}
            <View style={styles.itemTitleView}>

                <View style={{ marginLeft: 8, marginRight: 8, flex: 1, alignItems: 'center', flexDirection: 'row', overflow: 'hidden' }}>
                    <CustomMarquee style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, color: colors.white }} value={tournament.title} />
                </View>


                {/* Title View */}
                {!isFromTournamentDetail &&
                    <TouchableOpacity style={{ height: 24, width: 60, backgroundColor: colors.yellow, borderTopRightRadius: 12, borderBottomLeftRadius: 12, borderBottomRightRadius: 12, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }} onPress={() => setTournamentRulesOpen(true)} activeOpacity={0.5} >
                        <Image style={{ width: 14, height: 14, marginRight: 5 }} source={require('../../assets/images/rules_eye_icon_blue.png')} ></Image>
                        <Text style={{ color: colors.black, textAlign: 'center', fontSize: 10, fontFamily: fontFamilyStyleNew.semiBold }} >Rules</Text>
                    </TouchableOpacity>
                }
            </View>

            {/* Center Content */}
            <View style={{ height: !isFromTournamentDetail ? 170 : 115, backgroundColor: colors.black, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }} >

                <View style={{ marginLeft: 108, height: 100 }}>

                    <View style={{ height: 36, flexDirection: 'row', alignItems: 'center' }} >

                        {/* Date */}
                        <View style={{ flex: 0.6 }} >
                            <Text style={{ marginLeft: 12, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Date</Text>
                            <Text style={{ marginLeft: 12, marginTop: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.startDate && moment(tournament.startDate).format('DD/MM/yyyy')}</Text>
                            <Image style={{ position: 'absolute', top: 3, right: 0, bottom: 3, width: 1, backgroundColor: colors.white }} />
                        </View>

                        {/* Rounds */}
                        <View style={{ flex: 0.5 }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Rounds</Text>
                            <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.rounds && tournament.rounds.length}</Text>
                            <Image style={{ position: 'absolute', top: 3, right: 0, bottom: 3, width: 1, backgroundColor: colors.white }} />
                        </View>

                        {/* ID */}
                        <View style={{ width: 74 }} >
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >ID</Text>
                            <CustomMarquee style={{ marginLeft: 8, marginTop: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} value={tournament.shortCode} />
                        </View>
                    </View>

                    <View style={{ marginLeft: 5, height: 36, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, flexDirection: 'row' }} >

                        {/* Title1 */}
                        {findEnabledTitles().length >= 1 &&
                            <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(0, 'title')}</Text>
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(0, 'value')}</Text>
                            </View>
                        }
                        {/* Title2 */}
                        {findEnabledTitles().length >= 2 &&
                            <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(1, 'title')}</Text>
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(1, 'value')}</Text>
                                <Image style={{ position: 'absolute', top: 5, left: 0, bottom: 5, width: 1, backgroundColor: colors.black, opacity: 0.4 }} />
                            </View>
                        }
                        {/* Round */}
                        {findEnabledTitles().length >= 3 &&
                            <View style={{ height: 36, width: getTitlesSubViewWidth(), justifyContent: 'center' }}>
                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(2, 'title')}</Text>
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(2, 'value')}</Text>
                                <Image style={{ position: 'absolute', top: 5, left: 0, bottom: 5, width: 1, backgroundColor: colors.black, opacity: 0.4 }} />
                            </View>
                        }
                    </View>

                    <View style={{ marginLeft: 5, height: 36, flexDirection: 'row' }} >

                        {/* Winning */}
                        <View style={{ height: 36, width: '33.33%', justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Prize Pool</Text>
                            {/* <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >₹{tournament.prizePool}</Text> */}
                            <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                {getCurrencyImage()}
                                <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.prizePool}</Text>
                            </View>
                            <Image style={{ position: 'absolute', top: 5, right: 0, bottom: 5, width: 1, backgroundColor: colors.white }} />
                        </View>

                        {/* Winners */}
                        <TouchableOpacity style={{ height: 36, width: '33.33%', justifyContent: 'center' }} onPress={() => setWinningPrizePoolPopup(true)} activeOpacity={0.5} >
                            <Text style={{ marginLeft: 6, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Winners</Text>

                            <View style={{ marginLeft: 6, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.totalWinners}</Text>
                                <Image style={{ marginLeft: 3, width: 7, height: 10, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                            </View>

                            <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                        </TouchableOpacity>

                        {/* Entry Fee */}
                        <View style={{ height: 36, width: '33.33%', justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Join Using</Text>

                            {tournament.entryFee == '0' ?
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >Free</Text>
                                :
                                <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                    {getCurrencyImage()}
                                    <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{tournament.entryFee}</Text>
                                </View>
                            }
                        </View>
                    </View>

                    {!isFromTournamentDetail &&
                        <View>
                            {/* Spots Progress Indicator */}
                            <View style={{ marginLeft: 12, marginRight: 12, height: 7, flex: 1 }} >
                                <ProgressBar style={{ marginTop: 4, height: 5, width: '100%', backgroundColor: colors.white, borderRadius: 2.5 }} progress={getProgressValue()} color={colors.yellow} />
                            </View>

                            {/* Spots Available View */}
                            <View style={{ marginLeft: 12, marginTop: 12, marginRight: 12, justifyContent: 'space-between', flexDirection: 'row' }} >
                                {/* Spots Available */}
                                <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalRemainingPlayers()}</Text>

                                {/* Spots Available Left */}
                                <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalJoinedPlayersPlayers()}</Text>
                            </View>
                        </View>
                    }
                </View>
            </View>

            <Modal isVisible={isVisibleWinningPrizePoolPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <WiningPrizePool contest={tournament} closeWiningPrizePool={closeWiningPrizePool} currency={currency} />
            </Modal>

            <Modal isVisible={isVisibleTournamentRulesPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <TournamentRulesPopup tournament={tournament} setTournamentRulesOpen={setTournamentRulesOpen} />
            </Modal>

        </View>
    )
}

export default TournamentCommonDetail

const styles = StyleSheet.create({
    itemTitleView: {
        height: 40,
        backgroundColor: colors.black,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
})