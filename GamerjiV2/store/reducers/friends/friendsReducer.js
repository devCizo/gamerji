import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/friends/friendsAction';

const initialState = {
    model: {
        followUserResponse: undefined,
        followerListResponse: undefined,
        followingListResponse: undefined,
    },
    error: false
};

const followUser = (state, action) => {
    return produce(state, (draft) => {
        draft.model.followUserResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const followUserFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.followUserResponse = action.model;
    });
};


const followerList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.followerListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const followerListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.followerListResponse = action.model;
    });
};


const followingList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.followingListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const followingListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.followingListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FOLLOW_USER:
            return followUser(state, action);
        case actionTypes.FOLLOW_USER_FAILED:
            return followUserFailed(state, action);
        case actionTypes.FOLLOWER_LIST:
            return followerList(state, action);
        case actionTypes.FOLLOWER_LIST_FAILED:
            return followerListFailed(state, action);
        case actionTypes.FOLLOWING_LIST:
            return followingList(state, action);
        case actionTypes.FOLLOWING_LIST_FAILED:
            return followingListFailed(state, action);
        case actionTypes.RESET_FRIENDS_STATE:
            return resetFriendsState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetFriendsState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.followUserResponse = undefined;
        draft.model.followerListResponse = undefined;
        draft.model.followingListResponse = undefined;
    })
}