import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import VideosItem from '../../componentsItem/VideosItem';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const Videos = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [videos, setVideos] = useState([]);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 10

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToVideosListAPI();

        return () => {
            dispatch(actionCreators.resetVideosListState())
        }
    }, [])

    // This Use-Effect function should call the Videos API
    const callToVideosListAPI = () => {
        let payload = {
            skip: videos.length,
            limit: pageLimit
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestVideos(payload))
    }

    // This function sets the Videos API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.videos.model }),
        shallowEqual
    );
    var { videosResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (videosResponse) {
            setLoading(false)

            if (videosResponse.count == 0) {
                setTotalRecords(true)
            }

            if (videosResponse.list) {
                setIsFooterLoading(videosResponse.list.length == 0)
                var tempArr = videosResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setVideos([...videos, ...tempArr])
            }
            videosResponse = undefined
        }
    }, [videosResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Videos Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.text_videos} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                    {/* Wrapper - Video Lists */}
                    <FlatList
                        data={videos}
                        renderItem={({ item, index }) => <VideosItem item={item} index={index} navigation={props.navigation} />}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={false}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        onEndReachedThreshold={0.1}
                        onEndReached={() => {
                            if (!isFooterLoading) {
                                callToVideosListAPI()
                            }
                        }} />

                    {checkIsSponsorAdsEnabled('videos') &&
                        <SponsorBannerAds screenCode={'videos'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('videos')} />
                    }
                </View>
            </View>

            {isLoading && videos == null && <NoRecordsFound />}

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default Videos;