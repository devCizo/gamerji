import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import RightArrowWhiteImage from '../../assets/images/ic_right_arrow_black.svg';
import UpiPaymentPopup from './upiPaymentPopup'
import AddCardPopup from './addCardPopup'
import PaymentStatusPopup from './paymentStatusPopup'
import WalletPaymentPopup from './walletPaymentPopup'
import { AppConstants } from '../../appUtils/appConstants';

const PaymentOptionsNew = (props) => {

    const dispatch = useDispatch()
    const amount = props.route.params.amount;
    const [isLoading, setLoading] = useState(false)

    const [isOpenAddCardPopup, setOpenAddCardPopup] = useState(false)
    const [isOpenUpiPaymentPopup, setOpenUpiPaymentPopup] = useState(false)

    const [isOpenPaymentStatusPopup, setOpenPaymentStatusPopup] = useState(false)
    const [paymentGatewayResult, setPaymentGatewayResult] = useState(undefined)

    const [isOpenWalletListPopup, setOpenWalletListPopup] = useState(false)

    useEffect(() => {

        return () => {
            dispatch(actionCreators.resetPaymentOptionsState())
        }
    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.paymentOptions.model }),
        shallowEqual
    );
    var { paymentWalletListResponse } = currentState

    // Payment Wallet List Response
    useEffect(() => {
        if (paymentWalletListResponse) {
            setLoading(false)
            if (paymentWalletListResponse.list && paymentWalletListResponse.list.length > 0) {
                setOpenWalletListPopup(true)
            }
        }
    }, [paymentWalletListResponse])


    const generateParams = () => {

        var params = {
            amount: amount,
            paymentTypeForCreateTransaction: props.route.params.paymentTypeForCreateTransaction
        }

        if (props.route.params.coinStore) {
            params['coinStore'] = props.route.params.coinStore
        }

        if (props.route.params.avatarId) {
            params['avatarId'] = props.route.params.avatarId
        }

        return params
    }

    const saveCardFromPopupAndProceed = cardParams => {

        var params = { ...cardParams, ...generateParams() }
        moveToPaymentGateway(params)
    }

    const moveToNetbankingListScreen = () => {
        let params = generateParams()
        RootNavigation.navigate(Constants.nav_net_banking_list, params)
    }

    const proceedWithUpiPayment = (upiAddress) => {
        var params = generateParams()
        params['paymentType'] = AppConstants.PAYMENT_TYPE_UPI
        params['upiAddress'] = upiAddress

        moveToPaymentGateway(params)
    }

    const walletOptionPressed = () => {
        if (paymentWalletListResponse && paymentWalletListResponse.list && paymentWalletListResponse.list.length > 0) {
            setOpenWalletListPopup(true)
        } else {
            setLoading(true)
            dispatch(actionCreators.requestPaymentWalletList())
        }
    }

    const walletSelectedFromPopup = walletParam => {
        var params = { ...walletParam, ...generateParams() }
        moveToPaymentGateway(params)
    }

    const moveToPaymentGateway = params => {
        props.navigation.push(Constants.nav_payment_gateway_new, { params, paymentStatusCallBack })
    }

    const paymentStatusCallBack = (paymentGatewayResult) => {
        setPaymentGatewayResult(paymentGatewayResult)
        setOpenPaymentStatusPopup(true)
    }

    const paymentStatusPopupClosed = () => {
        setOpenPaymentStatusPopup(false)

        if (global.paymentSuccessCompletion) {
            global.paymentSuccessCompletion()
        }

        global.paymentSuccessCompletion = undefined

        if (global.addBalanceScreenType == Constants.contest_coin_payment_type || global.addBalanceScreenType == Constants.contest_money_payment_type) {
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.tournament_coin_payment_type || global.addBalanceScreenType == Constants.tournament_money_payment_type) {
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.all_games_add_money) {
            props.navigation.navigate(Constants.nav_bottom_navigation)
        } else {
            props.navigation.navigate(Constants.nav_account_new)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={() => RootNavigation.goBack()}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center', tintColor: colors.white }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >PAYMENT OPTIONS</Text>
            </View>

            {/* Main Content View */}
            <View style={{ flex: 1, backgroundColor: colors.white, borderTopLeftRadius: 40, borderTopRightRadius: 40 }} >

                {/* Current Balance */}
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, backgroundColor: colors.red, borderRadius: 25, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 24, height: 16, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_currency.png')} />
                        <Text style={{ marginLeft: 12, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Balance to Add</Text>
                    </View>

                    {/* Ballance */}
                    <Text style={{ marginRight: 24, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >₹{amount}</Text>
                </View>

                {/* Debit  / Credit Card */}
                <TouchableOpacity style={{ marginTop: 32, marginLeft: 20, marginRight: 20, height: 44, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => setOpenAddCardPopup(true)} activeOpacity={0.5}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 18, height: 24, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_credit_card.png')} />
                        <Text style={{ marginLeft: 12, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Debit  / Credit Card</Text>
                    </View>

                    {/* Right Arrow */}
                    <RightArrowWhiteImage style={{ marginRight: 15 }} />
                </TouchableOpacity>

                {/* Net Banking */}
                <TouchableOpacity style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 44, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} activeOpacity={0.5} onPress={() => moveToNetbankingListScreen()}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 18, height: 24, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_net_banking.png')} />
                        <Text style={{ marginLeft: 12, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Net Banking</Text>
                    </View>

                    {/* Right Arrow */}
                    <RightArrowWhiteImage style={{ marginRight: 15 }} />
                </TouchableOpacity>

                {/* Wallet */}
                <TouchableOpacity style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 44, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => walletOptionPressed()} activeOpacity={0.5}>

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 18, height: 24, width: 24, resizeMode: 'contain' }} source={require('../../assets/images/ic_wallet.png')} />
                        <Text style={{ marginLeft: 12, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Wallet</Text>
                    </View>

                    {/* Right Arrow */}
                    <RightArrowWhiteImage style={{ marginRight: 15 }} />
                </TouchableOpacity>

                {/* UPI Payment */}
                <TouchableOpacity style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 44, borderRadius: 22, borderWidth: 1, borderColor: '#D5D7E3', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => setOpenUpiPaymentPopup(true)} activeOpacity={0.5} >

                    {/* Title */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ marginLeft: 18, height: 24, width: 30, resizeMode: 'contain' }} source={require('../../assets/images/ic_upi.png')} />
                        <Text style={{ marginLeft: 8, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >UPI Payment</Text>
                    </View>

                    {/* Right Arrow */}
                    <RightArrowWhiteImage style={{ marginRight: 15 }} />
                </TouchableOpacity>

            </View>

            <Modal
                isVisible={isOpenUpiPaymentPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <UpiPaymentPopup setOpenUpiPaymentPopup={setOpenUpiPaymentPopup} proceedWithUpiPayment={proceedWithUpiPayment} />
            </Modal>

            <Modal
                isVisible={isOpenAddCardPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <AddCardPopup setOpenAddCardPopup={setOpenAddCardPopup} saveCardFromPopupAndProceed={saveCardFromPopupAndProceed} />
            </Modal>

            <Modal
                isVisible={isOpenWalletListPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <WalletPaymentPopup setOpenWalletListPopup={setOpenWalletListPopup} walletList={paymentWalletListResponse && paymentWalletListResponse.list} walletSelectedFromPopup={walletSelectedFromPopup} />
            </Modal>

            <Modal
                isVisible={isOpenPaymentStatusPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <PaymentStatusPopup paymentStatusPopupClosed={paymentStatusPopupClosed} paymentGatewayResult={paymentGatewayResult} />
            </Modal>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

export default PaymentOptionsNew