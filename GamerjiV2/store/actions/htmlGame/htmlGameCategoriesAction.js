import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Game Types List */ }
export const html5CategoriesList = (viewModel) => {
    return {
        type: HTML5GAME_CATEGORIES_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Game Types List */ }
export const html5CategoriesListFailed = (viewModel) => {
    return {
        type: HTML5GAME_CATEGORIES_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestHtml5Categories = (payload) => {

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HTML5GAME_CATEGORIES.MODE, payload)
            .then((response) => {
                addLog("requestHtml5Categories:::> ", response.data);
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(html5CategoriesList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(html5CategoriesListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetHtml5CategoriesState = () => {
    return {
        type: RESET_HTML5GAME_CATEGORIES_STATE,
    };
};

export const HTML5GAME_CATEGORIES_LIST = 'HTML5GAME_CATEGORIES_LIST';
export const HTML5GAME_CATEGORIES_FAILED = 'HTML5GAME_CATEGORIES_FAILED';
export const RESET_HTML5GAME_CATEGORIES_STATE = 'RESET_HTML5GAME_CATEGORIES_STATE';