import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/search/searchUsersAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        searchUserResponse: undefined,
    },
    error: false
};

{ /* Request - Game Types List */ }
const searchUserList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.searchUserResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const searchUserListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.searchUserResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEARCH_USER_LIST:
            return searchUserList(state, action);
        case actionTypes.SEARCH_USER_LIST_FAILED:
            return searchUserListFailed(state, action);
        case actionTypes.RESET_SEARCH_USER_LIST_STATE:
            return resetSearchUserListState(state);
        default:
            return state;
    }
};

export default (reducer);

const resetSearchUserListState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.searchUserResponse = undefined;
    })
}