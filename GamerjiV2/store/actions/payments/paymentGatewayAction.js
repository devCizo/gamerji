import axios from '../../../webServices/axios-api';
import { AppConstants, cashFreeClientId, cashFreeSecretId } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import * as generalSetting from '../../../webServices/generalSetting';

{ /* Action Types */ }
export const REQUEST_GENERATE_PAYMENT_TOKEN = 'REQUEST_GENERATE_PAYMENT_TOKEN';
export const REQUEST_GENERATE_PAYMENT_TOKEN_FAILED = 'REQUEST_GENERATE_PAYMENT_TOKEN_FAILED';
export const REQUEST_CREATE_TRANSACTION = 'REQUEST_CREATE_TRANSACTION';
export const REQUEST_CREATE_TRANSACTION_FAILED = 'REQUEST_CREATE_TRANSACTION_FAILED';
export const REQUEST_CHECK_TRANSACTIONS = 'REQUEST_CHECK_TRANSACTIONS';
export const REQUEST_CHECK_TRANSACTIONS_FAILED = 'REQUEST_CHECK_TRANSACTIONS_FAILED';
export const RESET_PAYMENT_GATEWAY_STATE = 'RESET_PAYMENT_GATEWAY_STATE';

{ /* Request - Generate Payment Token */ }
export const generatePaymentTokenRequest = (viewModel) => {
    return {
        type: REQUEST_GENERATE_PAYMENT_TOKEN,
        model: viewModel
    };
};

{ /* Request Failure - Generate Payment Token */ }
export const generatePaymentTokenRequestFailed = (viewModel) => {
    return {
        type: REQUEST_GENERATE_PAYMENT_TOKEN_FAILED,
        model: viewModel
    };
};

{ /* Request - Create Transactions */ }
export const createTransactionsRequest = (viewModel) => {
    return {
        type: REQUEST_CREATE_TRANSACTION,
        model: viewModel
    };
};

{ /* Request Failure - Create Transactions */ }
export const createTransactionsRequestFailed = (viewModel) => {
    return {
        type: REQUEST_CREATE_TRANSACTION_FAILED,
        model: viewModel
    };
};

{ /* Request - Check Transactions */ }
export const checkTransactionsRequest = (viewModel) => {
    return {
        type: REQUEST_CHECK_TRANSACTIONS,
        model: viewModel
    };
};

{ /* Request Failure - Check Transactions */ }
export const checkTransactionsRequestFailed = (viewModel) => {
    return {
        type: REQUEST_CHECK_TRANSACTIONS_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Payment Gateway */ }
export const resetPaymentGatewayState = () => {
    return {
        type: RESET_PAYMENT_GATEWAY_STATE,
    };
};

{ /* Request - Generate Payment Token */ }
export const requestGeneratePaymentToken = (payload) => {
    return (dispatch) => {

        addLog('AppId', cashFreeClientId())
        addLog('Secret', cashFreeSecretId())
        addLog('addBalanceCurrencyType', global.addBalanceCurrencyType)

        axios.defaults.headers.post[WebFields.HEADERS.CONTENT_TYPE] = WebFields.HEADERS.CONTENT_TYPE_VALUE;
        axios.defaults.headers.post[WebFields.HEADERS.X_CLIENT_ID] = cashFreeClientId();
        axios.defaults.headers.post[WebFields.HEADERS.X_CLIENT_SECRET] = cashFreeSecretId();
        axios
            .post(generalSetting.CASHFREE_GENERATE_TOKEN_URL, payload)
            .then((response) => {
                addLog("URL:::> ", generalSetting.CASHFREE_GENERATE_TOKEN_URL);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(generatePaymentTokenRequest(response.data));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(generatePaymentTokenRequestFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Create Transactions */ }
export const requestCreateTransactions = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CREATE_TRANSACTIONS.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(createTransactionsRequest(response.data.item));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(createTransactionsRequestFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Update Transactions */ }
export const requestCheckTransactions = (payload, transactionId) => {
    return (dispatch) => {
        debugger
        addLog("PARAMS:::> ", payload);
        const url = WebFields.CHECK_TRANSACTION.MODE + transactionId
        addLog("ABOVE URL:::> ", url);
        addLog("API TRANSACTION ID:::> ", transactionId);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(url, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(checkTransactionsRequest(response.data.item));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(checkTransactionsRequestFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};