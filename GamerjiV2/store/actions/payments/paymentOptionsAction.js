import axios from '../../../webServices/axios-api';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import * as generalSetting from '../../../webServices/generalSetting';


export const paymentWalletList = (viewModel) => {
    return {
        type: PAYMENT_WALLET_LIST,
        model: viewModel
    };
};

export const paymentWalletListFailed = (viewModel) => {
    return {
        type: PAYMENT_WALLET_LIST_FAILED,
        model: viewModel
    };
};

export const requestPaymentWalletList = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PAYMENT_WALLET.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(paymentWalletList(response.data));
                return true;
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(paymentWalletListFailed(error.response.data));
                return false;
            })
            .finally(() => {
                return false;
                // $('#overlay').hide();
            });
    };
};

// ========

export const resetPaymentOptionsState = () => {
    return {
        type: RESET_PAYMENT_OPTIONS_STATE,
    };
};

export const PAYMENT_WALLET_LIST = 'PAYMENT_WALLET_LIST';
export const PAYMENT_WALLET_LIST_FAILED = 'PAYMENT_WALLET_LIST_FAILED';
export const RESET_PAYMENT_OPTIONS_STATE = 'RESET_PAYMENT_OPTIONS_STATE';