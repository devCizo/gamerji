import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, shadowStyle } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as generalSetting from '../../webServices/generalSetting'
import moment from 'moment';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const CustomerCare = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const [ticketList, setTicketList] = useState(undefined)
    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setTicketList([])
        getTicketList()

        return () => {
            dispatch(actionCreators.resetCustomerCareState())
        }
    }, [refreshing]);

    //Fetch Leaderboard
    useEffect(() => {
        setLoading(true)

        getTicketList()

        return () => {
            dispatch(actionCreators.resetCustomerCareState())
        }
    }, [])

    const getTicketList = () => {
        dispatch(actionCreators.requestTicketList({}))
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.customerCare.model }),
        shallowEqual
    );
    var { ticketListResponse } = currentState
    addLog('currentState', currentState)

    useEffect(() => {
        if (ticketListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (ticketListResponse.list) {
                setTicketList(ticketListResponse.list)
            }
            if (ticketListResponse.list.length == 0) {
                RootNavigation.navigate(Constants.nav_raise_complaint, { getTicketList: getTicketList })
            }
        }
        ticketListResponse = undefined
    }, [ticketListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >CUSTOMER CARE</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                <View style={{ flex: 1 }} >
                    {/* Raise Complaint */}
                    <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 46, backgroundColor: colors.black, borderRadius: 23, justifyContent: 'center', alignItems: 'center' }} onPress={() => RootNavigation.navigate(Constants.nav_raise_complaint, { getTicketList: getTicketList })} >
                        <Text style={{ color: colors.white, fontSize: 17, fontFamily: fontFamilyStyleNew.bold }} >RAISE A COMPLAINT</Text>
                    </TouchableOpacity>

                    {ticketList &&
                        <FlatList
                            style={{ flex: 1, marginTop: 22 }}
                            data={ticketList}
                            renderItem={({ item }) => <CustomerCareTicketItem item={item} />}
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            }
                        />
                    }
                </View>

                {checkIsSponsorAdsEnabled('customerCare') &&
                    <SponsorBannerAds screenCode={'customerCare'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('customerCare')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export const CustomerCareTicketItem = (props) => {

    let ticket = props.item

    let getTicketStatus = () => {
        if (ticket.status) {
            switch (ticket.status) {
                case '1':
                    return 'PENDING'
                case '2':
                    return 'APPROVED'
                case '3':
                    return 'ON HOLD'
                case '4':
                    return 'REJECTED'
                default:
                    break;
            }
        } else {
            return 'PENDING'
        }
    }

    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => RootNavigation.navigate(Constants.nav_ticket_detail, { ticketId: ticket._id })}>
            <View style={[shadowStyle.shadow, { marginLeft: 12, marginRight: 12, marginBottom: 15, borderRadius: 10, borderWidth: 1, borderColor: '#DFE4E9', backgroundColor: 'white', }]}>
                {/* Top View */}
                <View style={{ marginTop: 10, marginLeft: 7.5, marginRight: 7.5, flexDirection: 'row', }}>

                    {/* Ticket No */}
                    <View style={{ width: '33.33%', height: 84 }}>

                        <View style={{ flex: 1, marginLeft: 4.5, marginRight: 4.5, backgroundColor: colors.yellow, borderRadius: 10, alignItems: 'center' }}>

                            {/* Image */}
                            <View style={{ marginTop: -9, height: 42, width: 42, backgroundColor: colors.white, borderRadius: 21, alignItems: 'center', justifyContent: 'center' }} >
                                <Image style={{ height: 20, width: 18, resizeMode: 'contain' }} source={require('../../assets/images/ticket_number.png')} />
                            </View>
                            {/* Ticket No Title */}
                            <Text style={{ marginTop: 7, color: colors.black, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >Ticket No</Text>
                            {/* Ticket No Title */}
                            <Text style={{ marginTop: 6, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >#{ticket.code}</Text>
                        </View>
                    </View>

                    {/* Date */}
                    <View style={{ width: '33.33%', height: 84 }}>

                        <View style={{ flex: 1, marginLeft: 4.5, marginRight: 4.5, backgroundColor: colors.yellow, borderRadius: 10, alignItems: 'center' }}>

                            {/* Image */}
                            <View style={{ marginTop: -9, height: 42, width: 42, backgroundColor: colors.white, borderRadius: 21, alignItems: 'center', justifyContent: 'center' }} >
                                <Image style={{ height: 20, width: 19, resizeMode: 'contain' }} source={require('../../assets/images/calendar_icon.png')} />
                            </View>
                            {/* Date Title */}
                            <Text style={{ marginTop: 7, color: colors.black, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >Date</Text>
                            {/* Ticket No Title */}
                            <Text style={{ marginTop: 6, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{moment(ticket.createdAt).format('DD/MM/yyyy')}</Text>
                        </View>
                    </View>

                    {/* Status */}
                    <View style={{ width: '33.33%', height: 84 }}>

                        <View style={{ flex: 1, marginLeft: 4.5, marginRight: 4.5, backgroundColor: colors.yellow, borderRadius: 10, alignItems: 'center' }}>

                            {/* Image */}
                            <View style={{ marginTop: -9, height: 42, width: 42, backgroundColor: colors.white, borderRadius: 21, alignItems: 'center', justifyContent: 'center' }} >
                                <Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/images/ticket_status.png')} />
                            </View>
                            {/* Status */}
                            <Text style={{ marginTop: 7, color: colors.black, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >Status</Text>
                            {/* Ticket No Title */}
                            <Text style={{ marginTop: 6, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{getTicketStatus()}</Text>
                        </View>
                    </View>
                </View>

                {/* Subject */}
                <View style={{
                    marginTop: 13, marginLeft: 12, marginRight: 12, marginBottom: 11, backgroundColor: colors.black, borderBottomStartRadius: 10, borderBottomEndRadius: 10,
                }}>
                    <Text style={{ marginTop: 10, marginLeft: 12, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Subject: {ticket.category && ticket.category.name}</Text>
                    <Text style={{ marginTop: 5, marginLeft: 12, marginRight: 12, marginBottom: 14, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} >{ticket.description}</Text>
                </View>
                {ticket.screenShot ?
                    <View style={{
                        marginTop: 2, marginLeft: 12, marginRight: 12, marginBottom: 11
                    }}>


                        <Image style={{ height: 100, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + ticket.screenShot.default }} />
                    </View>
                    : <View></View>
                }
            </View>
        </TouchableOpacity>
    )
}

export default CustomerCare;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})