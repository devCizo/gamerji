import React, { useState, useCallback, useEffect } from "react";
import { View, Text, TouchableOpacity, Linking, Dimensions, Image } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import { addLog } from '../appUtils/commonUtlis';
import YoutubePlayer from "react-native-youtube-iframe";
import getVideoId from 'get-video-id';
import CustomMarquee from '../appUtils/customMarquee';
import CustomProgressbar from '../appUtils/customProgressBar';
const ytch = require('yt-channel-info');
const VideosItem = (props) => {

    // Variable Declarations
    let colors = ['#FFC609', '#43F2FF', '#09FFC4'];
    const [playing, setPlaying] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [videoChannelData, setVideoChannelData] = useState(undefined);
    const [videoData, setVideoData] = useState(undefined);
    const width = (Dimensions.get('window').width - 70) / 2;

    // This function should set the youtube player video play state
    const onStateChange = useCallback((state) => {
        if (state === "ended") {
            setPlaying(false);
            addLog("Video has finished playing!");
        }
    }, []);

    // const togglePlaying = useCallback(() => {
    //     setPlaying((prev) => !prev);
    // }, []);

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToViewAllVideosListAPI();
    }, [])

    // This Use-Effect function should call the View All Videos API
    const callToViewAllVideosListAPI = () => {


        var url = props.item.youTubeChannelLink
        const myArr = url.split("/")

        var channelId = myArr[myArr.length - 1];
        addLog('Channel Id:::> ', channelId)
        let channelIdType = "";

        ytch.getChannelInfo(channelId, channelIdType).then((response) => {
            addLog('Response N:::> ', response)
            setVideoChannelData(response)
            const sortBy = 'newest'

            ytch.getChannelVideos(channelId, sortBy, channelIdType).then((responseVideos) => {
                setLoading(false)
                addLog('Response Videos:::> ', responseVideos)
                addLog('Total Videos:::> ', responseVideos.items.length)
                addLog('videoThumbnails:::> ', responseVideos.items[0].videoThumbnails)
                setVideoData(responseVideos)

                // return responder.success(res, {
                //     item: response,
                //     item1: responseVideos
                // });
            }).catch((err) => {
                console.log(err)
            })
        }).catch((err) => {
            console.log(err)
        })
    }

    return (
        // Wrapper - Main View
        <View style={[{ height: 200 }, borderStyles.borderRadius_15, marginStyles.topMargin_5, marginStyles.bottomMargin_10, paddingStyles.padding_10, { backgroundColor: colors[props.index % colors.length] }]}>

            <View style={[heightStyles.height_24, flexDirectionStyles.row]}>

                {/* Wrapper - Channel Name */}
                <View style={{ width: Dimensions.get('window').width - 230 }}>

                    {/* Text - Channel Name */}
                    <CustomMarquee value={videoChannelData && videoChannelData.author} style={[fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_18, marginStyles.leftMargin_2]} />
                </View>

                {/* Wrapper - Subscribe / View All */}
                <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.right_0]}>

                    {/* TouchableOpacity - Subscribe Button Click Event*/}
                    <TouchableOpacity style={[heightStyles.height_24, widthStyles.width_70, borderStyles.borderTopLeftRadius_20, borderStyles.borderBottomLeftRadius_20, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.primaryBackgroundColor, { width: 85 }]}
                        onPress={() => { Linking.openURL(props.item.youTubeChannelLink) }} activeOpacity={0.5}>

                        {/* Text - Subscribe */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_12, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.text_subsciribe}</Text>
                    </TouchableOpacity>

                    {/* Text - Separator */}
                    <View style={[heightStyles.height_1, widthStyles.width_05, colorStyles.whiteBackgroundColor]} />

                    {/* TouchableOpacity - View All Button Click Event*/}
                    <TouchableOpacity style={[heightStyles.height_24, widthStyles.width_70, borderStyles.borderTopRightRadius_20, borderStyles.borderBottomRightRadius_20, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.primaryBackgroundColor]}
                        onPress={() => props.navigation.navigate(Constants.nav_view_all_videos, { youTubeChannelLink: props.item.youTubeChannelLink })} activeOpacity={0.5}>

                        {/* Text - View All */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_12, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.text_view_all}</Text>
                    </TouchableOpacity>
                </View>
            </View>

            {/* <Text style={[fontFamilyStyles.semiBold, colorStyles.redColor, fontSizeStyles.fontSize_14]}>{videoData && videoData.items && videoData.items.length > 1 ? (videoData && videoData.items && videoData.items.length + ' ' + Constants.text_videos) : (videoData && videoData.items && videoData.items.length + ' ' + Constants.text_video)}</Text> */}

            {/* Text - Total Videos */}
            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_14]}>{videoChannelData && videoChannelData.subscriberText}</Text>

            {/* Wrapper - Videos */}
            <View style={[containerStyles.container, flexDirectionStyles.row, marginStyles.topMargin_10]}>

                {/* Wrapper - Video 1 */}
                <View style={[{ width: width }, flexDirectionStyles.column, borderStyles.borderRadius_10, marginStyles.rightMargin_5, colorStyles.whiteBackgroundColor, alignmentStyles.oveflow]}>

                    {/* YoutubePlayer - Video 1 */}
                    <View style={{ height: 95 }}>
                        <YoutubePlayer
                            height={300}
                            play={playing}
                            videoId={videoData && videoData.items && videoData.items[0] && videoData.items[0].videoId}
                            onChangeState={onStateChange} />

                    </View>

                    {/* Wrapper - Video 1 Title */}
                    <View style={[heightStyles.height_37, flexDirectionStyles.column, paddingStyles.leftPadding_10, paddingStyles.rightPadding_10]}>

                        {/* Text - Video 1 Title */}
                        <CustomMarquee value={videoData && videoData.items && videoData.items[0] && videoData.items[0].title} style={[fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_14]} />

                        {/* Text - Subscribers */}
                        <View style={{ flexDirection: 'row', width: '70%' }}>

                            <Image style={{ height: 10, width: 10, borderRadius: 5, alignSelf: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_views.png')} />

                            {/* <Text style={{ fontFamily: fontFamilyStyleNew.regular, color: colors.black, fontSize: 11, marginLeft: 2, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{'10230000' ? convertNumberToMillions('10230000') : 0} {Constants.text_views}</Text> */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_12, { marginLeft: 5 }]}>{videoData && videoData.items && videoData.items[0] && videoData.items[0].viewCountText}</Text>
                        </View>

                    </View>
                </View>

                {/* Wrapper - Video 2 */}
                {videoData && videoData.items && videoData.items[1] && videoData.items[1].videoId &&
                    <View style={[{ width: width }, flexDirectionStyles.column, borderStyles.borderRadius_10, marginStyles.leftMargin_5, colorStyles.whiteBackgroundColor, alignmentStyles.oveflow]}>

                        {/* YoutubePlayer - Video 2 */}
                        <View style={{ height: 95 }}>
                            <YoutubePlayer style={borderStyles.borderRadius_10}
                                height={300}
                                play={playing}
                                videoId={videoData && videoData.items && videoData.items[1] && videoData.items[1].videoId}
                                onChangeState={onStateChange} />
                        </View>

                        {/* Wrapper - Video 2 Title */}
                        <View style={[heightStyles.height_37, flexDirectionStyles.column, paddingStyles.leftPadding_10, paddingStyles.rightPadding_10]}>

                            {/* Text - Video 2 Title */}
                            <CustomMarquee value={videoData && videoData.items && videoData.items[1] && videoData.items[1].title} style={[fontFamilyStyles.bold, colorStyles.blackColor, fontSizeStyles.fontSize_14]} />

                            {/* Text - Subscribers */}
                            <View style={{ flexDirection: 'row', width: '70%' }}>

                                <Image style={{ height: 10, width: 10, borderRadius: 5, alignSelf: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_views.png')} />
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_12, { marginLeft: 5 }]}>{videoData && videoData.items && videoData.items[1] && videoData.items[1].viewCountText}</Text>
                            </View>
                        </View>
                    </View>
                }
            </View>

            {isLoading && <CustomProgressbar />}
        </View >
    )
}

export default VideosItem;