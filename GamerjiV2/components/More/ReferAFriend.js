import React, { useEffect } from 'react'
import { View, Image, Text, Linking, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, Share } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import Clipboard from '@react-native-clipboard/clipboard';
import { showSuccessToastMessage } from '../../appUtils/commonUtlis';

const ReferAFriend = (props, navigation) => {

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    const clickToShareReferralCode = () => {
        var text = 'Hey, download this fantastic app GamerJi  and get 150 coins bonus using my referral code. ' + global.profile.referralCode + ' You can download it by visiting https://www.gamerji.com  ';
        Share.share({
            message: text,
            url: 'http://bam.tech',
            title: 'Wow, did you see that?'
        }, {
            // Android only:
            dialogTitle: 'Share Referral Code',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }
    const copyReferralCodeToClipboard = () => {
        if (global.profile.referralCode) {
            showSuccessToastMessage('Copied')
            Clipboard.setString(global.profile.referralCode)
        }
    }

    const clickToShareReferralCodeToWhatsApp = () => {

        var text = 'Hey, download this fantastic app GamerJi  and get 150 coins bonus using my referral code. ' + global.profile.referralCode + ' You can download it by visiting https://www.gamerji.com  ';
        Linking.openURL(`whatsapp://send?text=${text}`);
        // Clipboard.setString(global.profile.phone)

    }
    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.yellow }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={() => handleBackButton()}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center', tintColor: colors.black }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >REFER A FRIEND</Text>
            </View>

            {/* Main Content View */}
            <View style={{ flex: 1, backgroundColor: colors.yellow }} >
                <Image style={{ flex: 1, resizeMode: 'contain' }} source={require('../../assets/images/refer-friend-bg.png')} />
                <View style={{ backgroundColor: colors.black, borderTopLeftRadius: 40, borderTopRightRadius: 40 }}>
                    <Text style={{ marginTop: 10, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }} >Invite your friends & earn</Text>

                    <View style={{ flexDirection: "row", alignSelf: "center", alignItems: "center", justifyContent: "center", marginTop: 15 }}>
                        <Image style={{ height: 32, width: 32, resizeMode: 'contain', }} source={require('../../assets/images/total-coin-icon.png')} />

                        <Text style={{ marginTop: 10, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'left' }} >1000</Text>
                    </View>

                    <View style={{ marginLeft: 10, marginRight: 10, flexDirection: "row", marginTop: 15 }}>

                        <View style={{ alignSelf: "center", alignItems: "center", justifyContent: "center", width: '50%' }}>
                            <Image style={{ height: 85, width: 85, resizeMode: 'contain' }} source={require('../../assets/images/refer-friend-left-icon.png')} />

                            <Text style={{ marginTop: 10, marginRight: 10, marginLeft: 10, color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }} >Invite your friends with
                                referral code</Text>
                        </View>
                        <View style={{
                            alignSelf: "center", alignItems: "center", justifyContent: "center", width: '50%'
                        }}>
                            < Image style={{ height: 85, width: 85, resizeMode: 'contain', }} source={require('../../assets/images/refer-friend-right-icon.png')} />

                            <Text style={{ marginTop: 10, marginRight: 10, marginLeft: 10, color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular, textAlign: 'center' }} >Friend sign up, you earns</Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Image style={{ height: 10, width: 9, resizeMode: 'contain', }} source={require('../../assets/images/total-coin-icon.png')} />
                                <Text style={{ marginLeft: 2, color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >150 & friends earns</Text>
                                <Image style={{ marginLeft: 2, height: 10, width: 9, resizeMode: 'contain', }} source={require('../../assets/images/total-coin-icon.png')} />
                                <Text style={{ marginLeft: 2, color: colors.white, fontSize: 13, fontFamily: fontFamilyStyleNew.regular }} >150</Text>
                            </View>

                        </View>
                    </View>

                    <Text style={{ marginTop: 20, marginLeft: 20, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Your Referral Code</Text>

                    <View style={{ marginLeft: 20, marginRight: 20, height: 40, flexDirection: "row", marginTop: 10 }}>
                        <View style={{ height: '100%', alignSelf: "center", alignItems: "center", justifyContent: "center", flexDirection: "row", flex: 1 }}>
                            <Image style={{ position: "absolute", top: 0, left: 0, width: '100%', height: '100%', resizeMode: "stretch" }} source={require('../../assets/images/referal-code-box.png')} />

                            <Text style={{ marginRight: 10, marginLeft: 10, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }} > {global.profile.referralCode}</Text>
                        </View>

                        <TouchableOpacity style={{ height: '100%', marginLeft: 25, width: 90, flexDirection: 'row', borderRadius: 23, alignItems: 'center', alignSelf: "center", justifyContent: "center", borderWidth: 1, borderColor: colors.white }} onPress={() => copyReferralCodeToClipboard()} activeOpacity={0.5} >

                            <Text style={{ marginLeft: 8, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Copy</Text>

                        </TouchableOpacity>

                    </View>
                    <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 10, height: 46, width: 220, flexDirection: 'row', backgroundColor: colors.red, borderRadius: 23, alignItems: 'center', alignSelf: "center", justifyContent: "center" }} onPress={() => clickToShareReferralCodeToWhatsApp()} activeOpacity={0.5} >
                        <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/whatsapp-icon.png')}></Image>
                        <Text style={{ marginLeft: 8, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Invite Via Whatsapp</Text>

                    </TouchableOpacity>
                    <TouchableOpacity style={{ marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 10, height: 46, flexDirection: 'row', backgroundColor: colors.yellow, borderRadius: 23, alignItems: 'center', justifyContent: "center", alignSelf: "center", width: 175 }} onPress={() => clickToShareReferralCode()} activeOpacity={0.5} >
                        <Image style={{ width: 24, height: 24, resizeMode: 'contain' }} source={require('../../assets/images/share-refer-friend.png')}></Image>
                        <Text style={{ marginLeft: 8, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Refer friend</Text>

                    </TouchableOpacity>

                </View>
            </View>
        </SafeAreaView >
    )
}

export default ReferAFriend;