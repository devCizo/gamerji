import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const tournamentDetail = (viewModel) => {
    return {
        type: TOURNAMENT_DETAIL,
        model: viewModel
    };
};

export const tournamentDetailFailed = (viewModel) => {
    return {
        type: TOURNAMENT_DETAIL_FAILED,
        model: viewModel
    };
};

export const requestTournamentDetail = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.TOURNAMENT.GET + '/' + payload, {})
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(tournamentDetail(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(tournamentDetailFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const tournamentPlayers = (viewModel) => {
    return {
        type: TOURNAMENT_PLAYER,
        model: viewModel
    };
};

export const tournamentPlayersFailed = (viewModel) => {
    return {
        type: TOURNAMENT_PLAYER_FAILED,
        model: viewModel
    };
};

export const requestTournamentPlayers = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.TOURNAMENT.PLAYERS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(tournamentPlayers(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(tournamentPlayersFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const TOURNAMENT_DETAIL = 'TOURNAMENT_DETAIL';
export const TOURNAMENT_DETAIL_FAILED = 'TOURNAMENT_DETAIL_FAILED';
export const TOURNAMENT_PLAYER = 'TOURNAMENT_PLAYER';
export const TOURNAMENT_PLAYER_FAILED = 'TOURNAMENT_PLAYER_FAILED';

export const resetTournamentDetailState = () => {
    return {
        type: RESET_TOURNAMENT_DETAIL_STATE,
    };
};

export const RESET_TOURNAMENT_DETAIL_STATE = 'RESET_TOURNAMENT_DETAIL_STATE';