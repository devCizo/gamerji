import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import TournamentCommonDetail from './tournamentCommonDetail'
import * as generalSetting from '../../webServices/generalSetting'
import TournamentTimingPopup from './tournamentTimingPopup'
import JoinTournamentWalletValidation from './joinTournamentWalletValidation'
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import LottieView from 'lottie-react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
const SingleTournamentToJoin = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const [tournament, setTournament] = useState({ ...props.tournamentDetail })
    const [isVissibleTournamentTimingPopup, setTournamentTimingPopup] = useState(false)
    const [selectedTournamentTiming, setSelectedTournamentTiming] = useState(undefined)
    const [walletUsageLimit, setWalletUsageLimit] = useState(undefined);
    const [isVissibleWalletPopup, setJoinTournamentWalletPopup] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [mainViewHeight, setMainViewHeight] = useState(336)
    const insets = useSafeAreaInsets();
    useEffect(() => {
        return () => {
            dispatch(actionCreators.resetSingleTounamentState())
        }

    }, [])
    useEffect(() => {
        var height = 200 + (5 * 40) + insets.bottom
        if (height >= Constants.SCREEN_HEIGHT) {
            height = Constants.SCREEN_HEIGHT - 100
        }
        setMainViewHeight(height)


    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.singleTournamentToJoin.model }),
        shallowEqual
    );
    var { walletUsageLimitForSingleTournamentResponse } = currentState

    //Tournament joining validation
    let joinNowValidation = () => {

        // RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })
        // return

        if (!checkForDobStateGameName()) {
            RootNavigation.navigate(Constants.nav_dob_state_validation, { game: tournament.game })
        } else if (tournament.isJoined && tournament.isJoined == true) {
            RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })

            // if (props.setFeaturedTournamentDetail) {
            //     props.setFeaturedTournamentDetail(undefined)
            // }
            props.setOpenSingleTournamentPopup(false)
            // if (props.setOpenSingleTournamentPopup) {
            //     props.setOpenSingleTournamentPopup(false)
            // }
        } else {
            setTournamentTimingPopup(true)
            // props.setOpenSingleTournamentPopup(false)
        }
    }

    // DOB State Validation
    let checkForDobStateGameName = () => {

        var isValidToJoin = true

        if (global.profile) {
            if (global.profile.gamerjiName) {
                if (global.profile.gamerjiName == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.dateOfBirth) {
                if (global.profile.dateOfBirth == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.address) {
                if (global.profile.address.state == null) {
                    isValidToJoin = false
                }
            }

            if (global.profile.gameNames, tournament.game) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == tournament.game._id)
                if (index < 0) {
                    isValidToJoin = false
                }
            }
        }
        return isValidToJoin
    }

    //Tournament Timing
    let tournamentTimingSelected = (contest) => {
        setSelectedTournamentTiming(contest)
        getWalletUsageLimit()
    }

    let closeTournamentTimingPopup = () => {
        setTournamentTimingPopup(false)
    }

    //Wallet validation
    let getWalletUsageLimit = () => {
        let payload = {
            event: tournament._id,
            type: 'event'
        }
        setLoading(true)
        dispatch(actionCreators.requestWalletUsageLimitForSingleTournament(payload))
    }

    useEffect(() => {
        if (walletUsageLimitForSingleTournamentResponse) {
            setLoading(false)
            if (walletUsageLimitForSingleTournamentResponse.success == true && walletUsageLimitForSingleTournamentResponse.item) {
                setWalletUsageLimit(walletUsageLimitForSingleTournamentResponse.item)

                setTimeout(function () {
                    setJoinTournamentWalletPopup(true)
                }, 500);
            } else {
                if (walletUsageLimitForSingleTournamentResponse.errors && walletUsageLimitForSingleTournamentResponse.errors[0] && walletUsageLimitForSingleTournamentResponse.errors[0].msg) {
                    showErrorToastMessage(walletUsageLimitForSingleTournamentResponse.errors[0].msg)
                }
            }
            walletUsageLimitForSingleTournamentResponse = undefined
        }
    }, [walletUsageLimitForSingleTournamentResponse]);

    let closeWalletValidationPopup = () => {
        setJoinTournamentWalletPopup(false)
    }

    let moveToSquadRegistration = (contest) => {
        RootNavigation.navigate(Constants.nav_squad_registration, { contestTournament: tournament, roundContest: contest, isContest: false, setTournamentAsJoined: setTournamentAsJoined })
        props.setOpenSingleTournamentPopup(false)
    }

    let setTournamentAsJoined = () => {
        if (tournament) {
            RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })
            var tempTournament = { ...tournament }
            tempTournament.isJoined = true
            setTournament(tempTournament)
            // props.setFeaturedTournamentDetail(undefined)
            props.setOpenSingleTournamentPopup(false)
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    let moveToAddBalance = (amountToAdd, isCoin) => {
        if (isCoin) {
            global.addBalanceScreenType = Constants.tournament_coin_payment_type
            global.addBalanceCurrencyType = Constants.coin_payment
        } else {
            global.addBalanceScreenType = Constants.tournament_money_payment_type
            global.addBalanceCurrencyType = Constants.money_payment
        }
        global.paymentSuccessCompletion = reOpenWalletPaymentPopupForSingleTournament
        RootNavigation.navigate(Constants.nav_coin_reward_store, { amountToAdd: amountToAdd })

        ///props.setFeaturedTournamentDetail(undefined)
        props.setOpenSingleTournamentPopup(false)
    }

    const reOpenWalletPaymentPopupForSingleTournament = () => {

    }


    return (
        <View style={{ height: 500, backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, }} >

            <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                <View style={{ flex: 1, marginLeft: 60, alignItems: 'center' }}>
                    <CustomMarquee style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} value={tournament.game && tournament.game.name} />
                </View>

                {/* Close Button */}
                <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.setOpenSingleTournamentPopup(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>


            <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 40, height: 248 }}>
                <TournamentCommonDetail tournament={tournament} currency={tournament.currency} isFromTournamentDetail={false} />

                {/* Tournament Image */}
                <View style={{ position: 'absolute', top: 40, left: 8, width: 100, height: 130, borderRadius: 5, overflow: 'hidden' }} >

                    <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, width: 100, height: 130 }} source={tournament.featuredImage && tournament.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.featuredImage.default }} />
                    <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, width: 100, height: 70 }} source={require('../../assets/images/tournament_image_layer.png')} />
                    <Text style={{ position: 'absolute', left: 0, right: 0, bottom: 6, color: 'white', textAlign: 'center', fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} >{tournament.gameType && tournament.gameType.name}</Text>

                </View>

                <View style={{ position: 'absolute', flexDirection: 'row', left: 8, bottom: 20, right: 8, height: 44, backgroundColor: colors.yellow, borderRadius: 5, overflow: 'hidden', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }} >

                    {/* Hosted By View */}
                    <View style={{ width: '50%', backgroundColor: colors.yellow, overflow: 'hidden', justifyContent: 'center' }} >

                        <Text style={{ marginLeft: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }}>Hosted By</Text>

                        <View style={{ marginTop: 2, marginLeft: 10, marginRight: 10, flexDirection: 'row', alignItems: 'center', overflow: 'hidden' }} >

                            <CustomMarquee style={{ color: colors.black, fontSize: 10, fontWeight: 'bold' }} value={(tournament.host && tournament.host.name) + ' (' + (tournament.host && tournament.host.rate.toFixed(2)) + '★)'} />
                        </View>

                        <Image style={{ position: 'absolute', backgroundColor: colors.black, top: 0, bottom: 0, right: 0, width: 1 }} />
                    </View>

                    {/* Join Now View */}
                    <TouchableOpacity style={{ width: '50%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }} onPress={() => joinNowValidation()} activeOpacity={1} >
                        <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, color: colors.black }} >{tournament.isJoined ? 'JOINED' : 'JOIN NOW'}</Text>
                        {tournament.isJoined &&
                            <View style={{ marginLeft: 4, height: 16, width: 16 }}>
                                <LottieView
                                    source={require('../../assets/jsonFiles/success.json')}
                                    autoPlay
                                    loop
                                />
                            </View>
                        }
                    </TouchableOpacity>

                </View>
            </View>

            {checkIsSponsorAdsEnabled('singleTournament') &&
                <SponsorBannerAds screenCode={'singleTournament'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('singleTournament')} />
            }

            {tournament &&
                <Modal isVisible={isVissibleTournamentTimingPopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <TournamentTimingPopup tournament={tournament} closeTournamentTimingPopup={closeTournamentTimingPopup} tournamentTimingSelected={tournamentTimingSelected} />
                </Modal>
            }

            {isVissibleWalletPopup && //tournament && walletUsageLimit &&
                <Modal isVisible={isVissibleWalletPopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <JoinTournamentWalletValidation tournament={tournament} contest={selectedTournamentTiming} walletUsageLimit={walletUsageLimit} closeWalletValidationPopup={closeWalletValidationPopup} setTournamentAsJoined={setTournamentAsJoined} moveToSquadRegistration={moveToSquadRegistration} moveToAddBalance={moveToAddBalance} />
                </Modal>
            }

            {isLoading && <CustomProgressbar />}
        </View>
    )
}

export default SingleTournamentToJoin;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})