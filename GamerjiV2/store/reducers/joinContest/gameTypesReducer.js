import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinContest/gameTypesAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        gameTypesResponse: undefined,
    },
    error: false
};

{ /* Request - Game Types List */ }
const gameTypesList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.gameTypesResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Game Types List */ }
const gameTypesListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gameTypesResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GAME_TYPES_LIST:
            return gameTypesList(state, action);
        case actionTypes.GAME_TYPES_LIST_FAILED:
            return gameTypesListFailed(state, action);
        case actionTypes.RESET_GAME_TYPE_STATE:
            return resetGameTypesState(state);
        default:
            return state;
    }
};

export default (reducer);

const resetGameTypesState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.gameTypesResponse = undefined;
    })
}