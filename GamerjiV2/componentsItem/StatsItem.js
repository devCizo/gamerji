import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import ProgressCircle from 'react-native-progress-circle'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { addLog } from '../appUtils/commonUtlis';

const StatsItem = (props) => {

  // Variable Declarations
  const width = (Dimensions.get('window').width - 60) / 2;

  return (
    <TouchableOpacity activeOpacity={1}>

      {/* Wrapper - Border Main View */}
      <View style={{ height: 155, width: width, marginRight: 15, marginBottom: 25 }}>

        {/* Text - Game Info */}
        <View style={{ flexDirection: 'row', height: 35, width: width, borderRadius: 30, borderWidth: 2, borderColor: '#38393B', justifyContent: 'center', paddingTop: -20, zIndex: 2, backgroundColor: '#0C123C' }}>

          {/* Text - Game Name */}
          <Text style={{ width: 100, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white, marginTop: 2, marginRight: 5, marginLeft: 5 }} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.game.name}</Text>

          {/* Image - Top Rank */}
          <Image style={{ position: 'absolute', height: 17, width: 17, right: 0, alignSelf: 'center', marginRight: 5 }} source={require('../assets/images/ic_top_rank.png')} />

          {/* Wrapper - Sub Title */}
          <View style={{ position: 'absolute', height: 14, maxWidth: 70, backgroundColor: '#202230', justifyContent: 'center', borderRadius: 20, top: 25, paddingLeft: 5, paddingRight: 5, bottom: 0, zIndex: 1 }}>

            {/* Text - Sub Title */}
            <Text style={{ fontFamily: fontFamilyStyleNew.regular, fontSize: 8, color: colors.white, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.uniqueIGN}</Text>
          </View>
        </View>

        {/* Wrapper - Border Main View */}
        <View style={{ position: 'absolute', height: 140, width: width, marginRight: 15, marginBottom: 13, borderRadius: 30, borderWidth: 2, borderColor: '#38393B', zIndex: 1, borderTopWidth: 0 }}>

          {/* Waper - Played, Kills & Avg. Rate  */}
          <View style={{ flexDirection: 'row', marginTop: 50 }}>

            {/* Wrapper - Played */}
            <View style={{ flexDirection: 'column', height: '100%', width: '33%' }}>

              {/* Image - Played */}
              <Image style={{ height: 16, width: 16, alignSelf: 'center', justifyContent: 'center' }} source={require('../assets/images/ic_played.png')} />

              {/* Text - Played Header */}
              <Text style={{ fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{Constants.text_played}</Text>

              {/* Text - Played */}
              <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.played}</Text>
            </View>

            {/* Wrapper - Kills */}
            <View style={{ flexDirection: 'column', height: '100%', width: '33%' }}>

              {/* Image - Kills */}
              <Image style={{ height: 16, width: 16, alignSelf: 'center' }} source={require('../assets/images/ic_kills.png')} />

              {/* Text - Kills Header */}
              <Text style={{ fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{Constants.text_kills}</Text>

              {/* Text - Kills */}
              <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.kills}</Text>
            </View>

            {/* Wrapper - Avg Rate */}
            <View style={{ flexDirection: 'column', height: '100%', width: '33%' }}>

              {/* Image - Avg Rate */}
              <Image style={{ height: 16, width: 16, alignSelf: 'center' }} source={require('../assets/images/ic_average_rate.png')} />

              {/* Text - Avg Rate Header */}
              <Text style={{ fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{Constants.text_average_rate}</Text>

              {/* Text - Avg Rate */}
              <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, fontSize: 11, color: colors.white, alignSelf: 'center', marginTop: 3 }} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.avgRates}</Text>
            </View>
          </View>

          {/* Wrapper - Circular Progressbar */}
          <View style={{ alignSelf: 'center', top: 10 }}>

            {/* ProgressCircle */}
            <ProgressCircle
              percent={props.item.stateatistic}
              radius={30}
              borderWidth={5}
              color={Colors.red}
              shadowColor="#38393B"
              bgColor="#221E32"
            >

              {/* Text - Win Percentage */}
              <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }}>{`${props.item.stateatistic}%`}</Text>
            </ProgressCircle>
          </View>
        </View>
      </View>

    </TouchableOpacity>
  )
}

export default StatsItem;