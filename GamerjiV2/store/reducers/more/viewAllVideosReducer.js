import produce from 'immer';
import * as actionTypes from '../../actions/more/viewAllVideosAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        viewAllVideosResponse: undefined
    },
    error: false
};

{ /* Request - View All Video List */ }
const viewAllVideoList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.viewAllVideosResponse = action.model;
    });
};

{ /* Request Failure - View All Video List */ }
const viewAllVideoListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.viewAllVideosResponse = action.model;
    });
};

{ /* Reset State - View All Videos List */ }
const resetViewAllVideosState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.viewAllVideosResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIEW_ALL_VIDEO_LIST:
            return viewAllVideoList(state, action);
        case actionTypes.VIEW_ALL_VIDEO_LIST_FAILED:
            return viewAllVideoListFailed(state, action);
        case actionTypes.RESET_VIEW_ALL_VIDEOS_LIST_STATE:
            return resetViewAllVideosState(state);
        default:
            return state;
    }
};

export default reducer;