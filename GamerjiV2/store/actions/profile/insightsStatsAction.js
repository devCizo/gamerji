import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';

// =======================

export const gameDetail = (viewModel) => {
    return {
        type: GAME_DETAIL,
        model: viewModel
    };
};

export const gameDetailFailed = (viewModel) => {
    return {
        type: GAME_DETAIL_FAILED,
        model: viewModel
    };
};


export const requestGameDetail = (payload) => {
    return (dispatch) => {
        addLog("API:::> ", WebFields.ALL_GAMES.SINGLE_GAME + "/" + payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ALL_GAMES.SINGLE_GAME + "/" + payload, {})
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(gameDetail(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(gameDetailFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};
// =======================

export const contestStatList = (viewModel) => {
    return {
        type: CONTEST_STAT_LIST,
        model: viewModel
    };
};

export const contestStatListFailed = (viewModel) => {
    return {
        type: CONTEST_STAT_LIST_FAILED,
        model: viewModel
    };
};

export const requestContestStatList = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_CONTEST_STATS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(contestStatList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(contestStatListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

// =======================

export const rankSummary = (viewModel) => {
    return {
        type: RANK_SUMMARY,
        model: viewModel
    };
};

export const rankSummaryFailed = (viewModel) => {
    return {
        type: RANK_SUMMARY_FAILED,
        model: viewModel
    };
};


export const requestRankSummary = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_RANK_SUMMARY_BY_GAME_STATS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(rankSummary(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(rankSummaryFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


// =======================

export const resetInsightStatsState = () => {
    return {
        type: RESET_INSIGHT_STATS_STATE,
    };
};


export const CONTEST_STAT_LIST = 'CONTEST_STAT_LIST';
export const CONTEST_STAT_LIST_FAILED = 'CONTEST_STAT_LIST_FAILED';
export const RANK_SUMMARY = 'RANK_SUMMARY';
export const RANK_SUMMARY_FAILED = 'RANK_SUMMARY_FAILED';
export const GAME_DETAIL = 'GAME_DETAIL';
export const GAME_DETAIL_FAILED = 'GAME_DETAIL_FAILED';
export const RESET_INSIGHT_STATS_STATE = 'RESET_INSIGHT_STATS_STATE'