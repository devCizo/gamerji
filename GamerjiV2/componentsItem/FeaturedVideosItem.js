import React, { useEffect, useState, useCallback } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions, Share, StyleSheet } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, justifyContentStyles, positionStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import { addLog, convertNumberToMillions } from '../appUtils/commonUtlis';
//import YoutubePlayer from "react-native-youtube-iframe";
import getVideoId from 'get-video-id';
import CustomMarquee from '../appUtils/customMarquee';
// import { YoutubeData } from 'node-youtube-data';
import YoutubePlayer from "react-native-youtube-iframe";
import Modal from 'react-native-modal';

const FeaturedVideosItem = (props) => {

    // Variable Declarations
    let color = ['#43F2FF', '#09FFC4', '#EB98FF', '#FFC609', '#FF923E', '#85C6FF'];

    const [playing, setPlaying] = useState(false);
    const { id } = getVideoId(props.item.url);
    // const api = new YoutubeData();
    // if (id) {
    //     api.getVideoInfo(id).then(res => {
    //         if (res) {
    //             const data = res.data;
    //             addLog(data)
    //         }
    //     })
    // }

    // This function should set the youtube player video play state
    const onStateChange = useCallback((state) => {
        if (state === "ended") {
            setPlaying(false);
            addLog("Video has finished playing!");
        }
    }, []);

    const shareVideo = () => {
        Share.share({
            title: props.item.name,
            message: props.item.url
        }, {
            // Android only:
            dialogTitle: 'Share BAM goodness',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }

    return (
        <>
            <TouchableOpacity onPress={() => props.openFeaturedVideoPopup(props.item)} style={{ flexDirection: 'row', justifyContent: 'center' }} activeOpacity={0.5} >
                <View style={{ height: 115, width: 160, borderRadius: 5, backgroundColor: color[props.index % color.length], zIndex: 0, marginRight: 10 }}>

                    <View style={{ position: 'absolute', flexDirection: 'column', height: 30, borderRadius: 5, bottom: 0, left: 0, right: 0, paddingLeft: 5, paddingRight: 5, zIndex: 1 }}>

                        <CustomMarquee value={props.item.name} style={{ marginTop: 10, marginLeft: 7, fontFamily: fontFamilyStyleNew.bold, color: colors.black, fontSize: 14 }} />

                        {/* <View style={{ flexDirection: 'row', marginTop: 5, marginLeft: 7, marginRight: 7 }}>

                            <View style={{ flexDirection: 'row', width: '70%' }}>

                                <Image style={{ height: 10, width: 10, borderRadius: 5, alignSelf: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_views.png')} />

                                <Text style={{ fontFamily: fontFamilyStyleNew.regular, color: colors.black, fontSize: 11, marginLeft: 2, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{'10230000' ? convertNumberToMillions('10230000') : 0} {Constants.text_views}</Text>
                            </View>

                        

                            <TouchableOpacity onPress={shareVideo} style={{ flexDirection: 'row', width: '30%', justifyContent: 'center' }} activeOpacity={0.5} >

                                <Image style={{ height: 10, width: 10, borderRadius: 5, alignSelf: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_share.png')} />
                                <Text style={{ fontFamily: fontFamilyStyleNew.regular, color: colors.black, fontSize: 11, marginLeft: 2, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_share}</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>

                    <View style={{ margin: 5, borderRadius: 10 }}>
                        <YoutubePlayer height={350} play={playing} videoId={id} onChangeState={onStateChange} />

                    </View>
                </View>
            </TouchableOpacity>

        </>

    )
}
const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
})
export default FeaturedVideosItem;