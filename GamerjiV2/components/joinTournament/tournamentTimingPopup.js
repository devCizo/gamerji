import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, Dimensions } from 'react-native'
import colors from '../../assets/colors/colors'
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles'
import moment from 'moment';
import { Constants } from '../../appUtils/constants'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds'

const TournamentTimingPopup = (props) => {

    var tournament = props.tournament
    const [contestList, setContestList] = useState(undefined)
    const [selectedTournamentTiming, setSelectedTournamentTiming] = useState(undefined)
    const [mainViewHeight, setMainViewHeight] = useState(336)
    const [nextRoundTimings, setNextRoundsTimings] = useState([])
    const insets = useSafeAreaInsets();
    var nextRoundTimingsFlatListRef = null
    var nextRoundTimingsFlatListIndex = 0
    var tempNextRoundTimings = []

    //Initial
    useEffect(() => {

        var tempArr = []

        for (let i = 0; i < props.tournament.contestsList.length; i++) {
            var tempContest = { ...props.tournament.contestsList[i] }
            tempContest.isSelected = false

            if (props.tournament.rounds && props.tournament.rounds.length > 0) {
                let round1 = props.tournament.rounds[0]

                for (let j = 0; j < round1.matches.length; j++) {
                    const tempRound = round1.matches[j]
                    if (tempRound.matchId == tempContest.code) {
                        var today = new Date();
                        var cdate = moment(tempRound.date).format('y-MM-DD');
                        var ctime = moment(tempRound.time).format('HH:mm:ss');
                        var contestDateTime = moment(cdate + ' ' + ctime);
                        // var contDate = new Date(match.date);
                        // var contTime = new Date(match.time);
                        // var contestDateTime = contDate.getFullYear() + "-" + (contDate.getMonth() + 1) + "-" + contDate.getDate() + " " + contTime.getHours() + ":" + contTime.getMinutes() + ":" + contTime.getSeconds();
                        // addLog("today==>", today);
                        // addLog("contDate==>", cdate);
                        // addLog("contTime==>", ctime);

                        addLog("contestDateTime==>", new Date(contestDateTime));
                        if (today < new Date(contestDateTime)) {
                            //addLog("tempContest==>", tempContest);
                            tempArr.push(tempContest)
                        }

                    }
                }
            }
        }
        addLog("tempArr==>", tempArr);
        // tempArr.sort(function (a, b) {
        //     // Turn your strings into dates, and then subtract them
        //     // to get a value that is either negative, positive, or zero.
        //     // var cdate = moment(tempRound.date).format('y-MM-DD');
        //     // var ctime = moment(tempRound.time).format('HH:mm:ss');
        //     // var contestDateTime = moment(cdate + ' ' + ctime);
        //     return a.time - b.time;
        // });

        setContestList(tempArr)

        // var tempArr = props.tournament.contestsList.map(item => {
        //     let tempItem = { ...item }
        //     tempItem.isSelected = false
        //     return tempItem
        // })
        // setContestList(tempArr)

        var height = 170 + (tempArr.length * 40) + insets.bottom
        if (height >= Constants.SCREEN_HEIGHT) {
            height = Constants.SCREEN_HEIGHT - 100
        }
        setMainViewHeight(height)
    }, [])

    //Select time from list
    let selectTournamentTime = (index) => {

        nextRoundTimingsFlatListIndex = 0

        var tempContestList = [...contestList]
        tempContestList.forEach((item, ind) => {
            item.isSelected = false
        })
        tempContestList[index]['isSelected'] = true
        //const tempContestList1 = tempContestList.sort((a, b) => parseFloat(b.code) - parseFloat(a.code));
        setContestList(tempContestList);
        setSelectedTournamentTiming(tempContestList[index])

        if (tournament.rounds && tournament.rounds.length > 1) {

            let rounds = tournament.rounds

            for (let i = 0; i < rounds.length; i++) {
                if (i != 0) {

                    if (tempNextRoundTimings.length == 0) {
                        findMatchFromRound(rounds[i], tempContestList[index].code)
                    } else {
                        findMatchFromRound(rounds[i], tempNextRoundTimings[tempNextRoundTimings.length - 1].matchId)
                    }
                }
            }
            setNextRoundsTimings(tempNextRoundTimings)
            tempNextRoundTimings = []
            if (nextRoundTimingsFlatListRef) {
                nextRoundTimingsFlatListRef.scrollToIndex({ animated: true, index: 0 })
            }
        }
    }

    //Find match from rounds on select of time from list
    let findMatchFromRound = (round, code) => {
        if (round.matches) {
            let roundMatches = round.matches

            for (let j = 0; j < roundMatches.length; j++) {
                var match = { ...roundMatches[j] }
                match.name = round.name
                let matchIndex = match.contest.findIndex(obj => obj == code)
                if (matchIndex > -1) {
                    tempNextRoundTimings.push(match)
                }
            }
        }
    }

    //Next to join tournament
    let nextToJoin = () => {
        let index = contestList.findIndex(obj => obj.isSelected == true)
        if (!selectedTournamentTiming) {
            showErrorToastMessage('Please select timeslot')
        } else {
            props.tournamentTimingSelected(selectedTournamentTiming)
            props.closeTournamentTimingPopup()
        }
    }

    // find_dimesions = (layout) => {
    //     const { x, y, width, height } = layout;
    //     console.warn(x);
    //     console.warn(y);
    //     console.warn(width);
    //     console.warn(height);
    // onLayout = {(event) => { find_dimesions(event.nativeEvent.layout) }}
    // }

    //Rounds timing scroll end
    let onNextRoundTimingsScrollEnd = (e) => {
        let pageNumber = Math.min(Math.max(Math.floor(e.nativeEvent.contentOffset.x / (Constants.SCREEN_WIDTH - 84) + 0.5), 0), contestList.length);
        nextRoundTimingsFlatListIndex = pageNumber
    }

    //Rounds timing previous pressed
    let previousBtnPressed = () => {
        if (nextRoundTimingsFlatListIndex != 0) {
            nextRoundTimingsFlatListIndex = nextRoundTimingsFlatListIndex - 1
            nextRoundTimingsFlatListRef.scrollToIndex({ animated: true, index: nextRoundTimingsFlatListIndex })
        }
    }

    //Rounds timing next pressed
    let nextBtnPressed = () => {
        if (nextRoundTimingsFlatListIndex < nextRoundTimings.length - 1) {
            nextRoundTimingsFlatListIndex = nextRoundTimingsFlatListIndex + 1
            nextRoundTimingsFlatListRef.scrollToIndex({ animated: true, index: nextRoundTimingsFlatListIndex })
        }
    }

    //Find final round date
    let findFinalRoundDate = () => {
        if (tournament.rounds) {
            let index = tournament.rounds.findIndex(obj => obj.isFinalRound == 'yes')
            if (index > -1) {

                if (tournament.rounds[index].matches && tournament.rounds[index].matches[0]) {
                    let match = tournament.rounds[index].matches[0]
                    return moment(match.date).format('DD MMMM yyyy') + ', ' + moment(match.time).format('hh:mm A')


                }
            }
        }
    }
    //  console.log("contestList==>", contestList);
    return (

        /* Main View */
        <View style={{ height: 500, backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50, }} >
            {/* Header View */}
            <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                <Text style={{ flex: 1, marginLeft: 60, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} numberOfLines={2} >{tournament.title}</Text>

                {/* Close Button */}
                <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.closeTournamentTimingPopup()} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>

            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, textAlign: 'center', }} >{tournament.game && tournament.game.name}-{tournament.gameType && tournament.gameType.name}</Text>

            <Text style={{ marginTop: 8, color: colors.red, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, textAlign: 'center', }} >{moment(tournament.startDate).format('DD MMMM yyyy')} to {moment(tournament.endDate).format('DD MMMM yyyy')}</Text>

            <View style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.red }}>
                <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, }} >Select 1st Round Time</Text>
            </View>

            <FlatList
                style={{ marginTop: 4, marginLeft: 20, marginRight: 20, flexGrow: 0 }}
                data={contestList}
                renderItem={({ item, index }) => <TournamentTimingItem item={item} index={index} selectTournamentTime={selectTournamentTime} />}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
            />

            {selectedTournamentTiming ?
                <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center' }} >

                    {nextRoundTimings.length > 1 &&
                        <TouchableOpacity style={{ marginLeft: 8, height: 34, width: 34, borderRadius: 17, backgroundColor: colors.black, justifyContent: 'center', alignItems: 'center' }} onPress={previousBtnPressed} activeOpacity={0.5} >
                            <Image style={{ width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/left_arrow.png')}></Image>
                        </TouchableOpacity>
                    }

                    <View style={{ flex: 1, alignItems: 'center' }} >

                        <FlatList
                            ref={(ref) => nextRoundTimingsFlatListRef = ref}
                            data={nextRoundTimings}
                            renderItem={({ item, index }) => <TournamentSelectedMatchNextRoundTimings item={item} />}
                            keyExtractor={(item, index) => index.toString()}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            pagingEnabled={true}
                            onMomentumScrollEnd={onNextRoundTimingsScrollEnd}
                        />
                    </View>

                    {nextRoundTimings.length > 1 &&
                        <TouchableOpacity style={{ marginRight: 8, height: 34, width: 34, borderRadius: 17, backgroundColor: colors.black, justifyContent: 'center', alignItems: 'center' }} onPress={nextBtnPressed} activeOpacity={0.5} >
                            <Image style={{ width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                        </TouchableOpacity>
                    }
                </View>
                :
                <Text style={{ marginTop: 20, marginLeft: 20, marginRight: 20, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'center' }}>Select match to check next round timings</Text>
            }

            <Text style={{ marginTop: 20, marginLeft: 20, marginRight: 20, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, textAlign: 'center' }}>FINALE will be scheduled at {findFinalRoundDate()}</Text>

            <TouchableOpacity style={{ height: 46, marginTop: 20, marginLeft: 20, marginRight: 20, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => nextToJoin()} activeOpacity={0.5} >
                <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >NEXT</Text>
                <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
            </TouchableOpacity>

            {checkIsSponsorAdsEnabled('tournamentTimingPopup') &&
                <View style={{ marginTop: 10 }}>
                    <SponsorBannerAds screenCode={'tournamentTimingPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('tournamentTimingPopup')} />
                </View>
            }
        </View>
    )
}

const TournamentTimingItem = (props) => {

    var contest = props.item
    let index = props.index

    return (
        <View style={{ height: 40 }} >

            <TouchableOpacity style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }} onPress={() => props.selectTournamentTime(index)} activeOpacity={0.5} >
                {contest.isSelected && contest.isSelected == true ?
                    <Image style={{ width: 16, height: 16 }} source={require('../../assets/images/red_radio_selected.png')} />
                    :
                    <Image style={{ width: 16, height: 16 }} source={require('../../assets/images/radio_deselected.png')} />
                }
                <Text style={{ marginLeft: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, }} >{moment(contest.date).format('DD MMMM yyyy')}, {moment(contest.time).format('hh:mm A')}</Text>

            </TouchableOpacity>

            <Image style={{ marginBottom: 0, height: 1, backgroundColor: '#000000', opacity: 0.2 }} />
        </View>
    )
}

const TournamentSelectedMatchNextRoundTimings = (props) => {

    let match = props.item

    return (
        <View style={{ width: Constants.SCREEN_WIDTH - 84, alignItems: 'center' }}>
            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }}>{match.name}</Text>
            <Text style={{ marginTop: 6, color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 13 }}>{moment(match.date).format('DD MMMM yyyy')}, {moment(match.time).format('hh:mm A')}</Text>
        </View>
    )
}


export default TournamentTimingPopup;
