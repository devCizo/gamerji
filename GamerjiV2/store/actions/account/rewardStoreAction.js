import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const getRewardCategory = (viewModel) => {
    return {
        type: REWARD_CATEGORY_LIST,
        model: viewModel
    };
};

export const getRewardCategoryFailed = (viewModel) => {
    return {
        type: REWARD_CATEGORY_LIST_FAILED,
        model: viewModel
    };
};

export const requestRewardCategoryList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.REWARD.CATEGORY);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.REWARD.CATEGORY_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getRewardCategory(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getRewardCategoryFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const getRewardCategoryProduct = (viewModel) => {
    return {
        type: REWARD_CATEGORY_PRODUCT_LIST,
        model: viewModel
    };
};

export const getRewardCategoryProductFailed = (viewModel) => {
    return {
        type: REWARD_CATEGORY_PRODUCT_LIST_FAILED,
        model: viewModel
    };
};

export const requestRewardCategoryProductList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.REWARD.CATEGORY);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.REWARD.PRODUCT_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getRewardCategoryProduct(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getRewardCategoryProductFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetRewardStoreState = () => {
    return {
        type: RESET_REWARD_STORE_STATE,
    };
};

export const REWARD_CATEGORY_LIST = 'REWARD_CATEGORY_LIST';
export const REWARD_CATEGORY_LIST_FAILED = 'REWARD_CATEGORY_LIST_FAILED';
export const REWARD_CATEGORY_PRODUCT_LIST = 'REWARD_CATEGORY_PRODUCT_LIST';
export const REWARD_CATEGORY_PRODUCT_LIST_FAILED = 'REWARD_CATEGORY_PRODUCT_LIST_FAILED';
export const RESET_REWARD_STORE_STATE = 'RESET_DYNAMIC_LINK_STATE';