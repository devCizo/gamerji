import produce from 'immer';
import * as actionTypes from '../../actions/customerCare/ticketDetailAction';

const initialState = {
    model: {
        ticketDetailResponse: undefined,
        updateTicketResponse: undefined,
    },
    error: false
};

const ticketDetail = (state, action) => {
    return produce(state, (draft) => {
        draft.model.ticketDetailResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const ticketDetailFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.ticketDetailResponse = action.model;
    });
};

const updateTicket = (state, action) => {
    return produce(state, (draft) => {
        draft.model.updateTicketResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const updateTicketFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.updateTicketResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TICKET_DETAIL:
            return ticketDetail(state, action);
        case actionTypes.TICKET_DETAIL_FAILED:
            return ticketDetailFailed(state, action);
        case actionTypes.TICKET_UPDATE:
            return updateTicket(state, action);
        case actionTypes.TICKET_UPDATE_FAILED:
            return updateTicketFailed(state, action);
        case actionTypes.RESET_TICKET_DETAIL_STATE:
            return resetTicketDetailState(state);
        default:
            return state;
    }
};

export default reducer;

const resetTicketDetailState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.ticketDetailResponse = undefined;
        draft.model.updateTicketResponse = undefined;
    })
}