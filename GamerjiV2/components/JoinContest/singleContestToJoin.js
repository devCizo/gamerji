import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, fontFamilyStyles } from '../../appUtils/commonStyles';
import ContestCommonDetail from './ContestCommonDetail';
import Modal from 'react-native-modal';
import JoinContestWalletValidation from './JoinContestWalletValidation';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import LottieView from 'lottie-react-native';

const SingleContestToJoin = (props) => {

    const dispatch = useDispatch();
    const [contest, setContestDetail] = useState(props.contestDetail)
    const [isVissibleWalletPopup, setJoinContestWalletPopup] = useState(false);
    const [walletUsageLimit, setWalletUsageLimit] = useState(undefined);

    useEffect(() => {
        return () => {
            dispatch(actionCreators.resetSingleContestState())
        }
    }, [])

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.singleContestToJoin.model }),
        shallowEqual
    );
    var { walletUsageLimitForSingleContestResponse } = currentState

    useEffect(() => {
        if (walletUsageLimitForSingleContestResponse && walletUsageLimitForSingleContestResponse.success == true && walletUsageLimitForSingleContestResponse.item) {
            setWalletUsageLimit(walletUsageLimitForSingleContestResponse.item)
            setJoinContestWalletPopup(true)
            walletUsageLimitForSingleContestResponse = undefined
        }
    }, [walletUsageLimitForSingleContestResponse]);

    let joinContestWalletValidation = () => {

        // RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })
        // return

        if (!checkForDobStateGameName()) {
            RootNavigation.navigate(Constants.nav_dob_state_validation, { game: contest.game })
        } else if (!contest.isJoined) {
            getWalletUsageLimit()
        } else {
            RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })
        }
    }

    let checkForDobStateGameName = () => {

        var isValidToJoin = true

        if (global.profile) {
            if (global.profile.gamerjiName) {
                if (global.profile.gamerjiName == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.dateOfBirth) {
                if (global.profile.dateOfBirth == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.address) {
                if (global.profile.address.state == null) {
                    isValidToJoin = false
                }
            }

            if (global.profile.gameNames, contest.game) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == contest.game._id)
                if (index < 0) {
                    isValidToJoin = false
                }
            }
        }
        return isValidToJoin
    }

    let getWalletUsageLimit = () => {
        let payload = {
            contest: contest._id,
            type: 'contest'
        }
        dispatch(actionCreators.requestWalletUsageLimitForSingleContest(payload))
    }

    let closeWalletValidationPopup = () => {
        setJoinContestWalletPopup(false)
    }

    let moveToSquadRegistration = () => {
        RootNavigation.navigate(Constants.nav_squad_registration, { contestTournament: selectedContest, isContest: true, setContestAsJoined: setContestAsJoined })
    }

    let setContestAsJoined = () => {
        addLog("setContestAsJoined===>", contest);
        if (contest) {

            RootNavigation.navigate(Constants.nav_contest_detail, { contestId: contest._id })
            var tempContest = { ...contest }
            tempContest.isJoined = true
            setContestDetail(contest)
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, }} >

            <View style={{ height: 60, flexDirection: 'row', alignItems: 'center' }}>

                <View style={{ flex: 1, marginLeft: 60, alignItems: 'center' }}>
                    <CustomMarquee style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} value={contest.game && contest.game.name} />
                </View>

                {/* Close Button */}
                <TouchableOpacity style={{ height: 60, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', marginRight: 0 }} onPress={() => props.setOpenSingleContestPopup(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>


            <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 40, height: 190 }}>

                <ContestCommonDetail item={contest} />

                {/* Bottom View */}
                <View style={{ position: 'absolute', height: 44, left: 8, right: 8, bottom: 0, borderTopLeftRadius: 5, borderTopRightRadius: 5, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, justifyContent: 'space-between', flexDirection: 'row', overflow: 'hidden', }} >

                    {/* Hosted By View */}
                    <View style={{ width: '50%', backgroundColor: colors.yellow, justifyContent: 'center' }} >

                        <Text style={{ marginLeft: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }}>Hosted By</Text>

                        <View style={{ marginTop: 2, marginLeft: 10, marginRight: 10, flexDirection: 'row', alignItems: 'center', overflow: 'hidden' }} >
                            <CustomMarquee style={{ color: colors.black, fontSize: 10, fontWeight: 'bold' }} value={(contest.host && contest.host.name) + ' (' + (contest.host && contest.host.rate.toFixed(2)) + '★)'} />
                        </View>

                        <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 0, bottom: 0, right: 0, width: 1 }} />
                    </View>

                    {/* Join Now View */}
                    <TouchableOpacity style={{ width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.yellow, flexDirection: 'row' }} onPress={() => joinContestWalletValidation()} activeOpacity={1} >
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: colors.black }} >{!contest.isJoined ? 'JOIN NOW' : 'JOINED'}</Text>
                        {contest.isJoined &&
                            <View style={{ marginLeft: 4, height: 16, width: 16 }}>
                                <LottieView
                                    source={require('../../assets/jsonFiles/success.json')}
                                    autoPlay
                                    loop
                                />
                            </View>
                        }
                    </TouchableOpacity>

                </View>
            </View >

            {contest &&
                <Modal isVisible={isVissibleWalletPopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <JoinContestWalletValidation contest={contest && contest} walletUsageLimit={walletUsageLimit} closeWalletValidationPopup={closeWalletValidationPopup} setContestAsJoined={setContestAsJoined} moveToSquadRegistration={moveToSquadRegistration} />
                </Modal>
            }
        </View>
    )
}

export default SingleContestToJoin

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})