import produce from 'immer';
import * as actionTypes from '../../actions/authentication/updateDeviceTokenAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        updateDeviceTokenResponse: undefined,
    },
    error: false
};


const updateDeviceToken = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.updateDeviceTokenResponse = action.model;
    });
};

const updateDeviceTokenFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.updateDeviceTokenResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AVATAR_LIST_PROFILE_INFO:
            return updateDeviceToken(state, action);
        case actionTypes.AVATAR_LIST_PROFILE_INFO_FAILED:
            return updateDeviceTokenFailed(state, action);
        case actionTypes.RESET_PROFILE_INFO_STATE:
            return resetUpdateDeviceTokenState(state);
        default:
            return state;
    }
};

export default reducer;

const resetUpdateDeviceTokenState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.updateDeviceTokenResponse = undefined;
    })
}