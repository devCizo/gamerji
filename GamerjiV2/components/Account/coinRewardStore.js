import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, BackHandler, RefreshControl } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import PurchaseAvatarPopup from './purchaseAvatarPopup'
import RewardStoreRedeemPopup from './rewardStoreRedeemPopup';
import MyRewardDetailPopup from './myRewardDetailPopup';
import moment from 'moment';

const CoinRewardStore = (props) => {

    // Variable Declaration
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);


    let activeReward = 'coinStore';
    let activeMyReward = 'rewards';
    if (props.route.params != null) {
        const reward = props.route.params.rewards;
        const myReward = props.route.params.myRewards;
        activeReward = reward;
        activeMyReward = myReward;

    }

    const [topActiveTab, setTopActiveTab] = useState(activeReward)
    const [coinContentActiveTab, setCoinContentActiveTab] = useState('packs')
    const [rewardContentActiveTab, setRewardContentActiveTab] = useState(activeMyReward)
    const [isOpenMyRewardDetailPopup, setOpenMyRewardDetailPopup] = useState(false)
    const [packList, setPackList] = useState([])
    const [bundleList, setBundleList] = useState([])
    const [avatarList, setAvatarList] = useState([])


    const [rewardList, setRewardList] = useState([])
    const [myRewardList, setMyRewardList] = useState([])
    const [earnCoinsList, setEarnCoinsList] = useState([])
    const [accountData, setAccountData] = useState(false)

    const [isOpenPurchaseAvatarPopup, setOpenPurchaseAvatarPopup] = useState(false)
    const [selectedAvatar, setSelectedAvatar] = useState(undefined)
    const [purchaseType, setPurchaseType] = useState('')

    const [isOpenRedeemRewardPopup, setOpenRedeemRewardPopup] = useState(false)
    const [selectedRewardToRedeem, setSelectedRewardToRedeem] = useState(undefined)
    const [refreshing, setRefreshing] = useState(false);


    const onRefresh = useCallback(async () => {
        fetchAccountData()

        if (topActiveTab == 'coinStore') {
            if (coinContentActiveTab == 'packs') {
                setRefreshing(true)

                getPackList()
            }
            if (coinContentActiveTab == 'bundles') {
                setRefreshing(true)

                getBundleList()
            }
            if (coinContentActiveTab == 'avatars') {
                setRefreshing(true)

                getAvatarList()
            }
        } else if (topActiveTab == 'rewards') {
            if (rewardContentActiveTab == 'rewards') {
                setRefreshing(true)
                dispatch(actionCreators.requestRewardCategoryList({ sortby: 'order', sort: 'asc' }))
            }

            if (rewardContentActiveTab == 'myRewards') {
                if (myRewardList.length == 0) {
                    setRefreshing(true)
                }
                dispatch(actionCreators.requestMyRewardList({ "sort": "desc", "sortBy": "createdAt" }))
            }
        }
        else if (topActiveTab == "earnCoins") {
            setRefreshing(true)
            dispatch(actionCreators.requestEarnCoinList({ "filter": {}, "sort": "desc", "sortBy": "createdAt", "skip": 0, "limit": 10, "page": 1 }))

        }


        setBlogs([])
        callToBlogsListAPI();

        return () => {
            dispatch(actionCreators.resetViewAllBlogsState())
        }
    }, [refreshing]);


    // Init
    useEffect(() => {

        getPackList()

        return () => {
            dispatch(actionCreators.resetCoinRewardStoreState())
        }

    }, [])

    const getPackList = () => {
        dispatch(actionCreators.requestCoinPackList({
            'filter': { 'coinType': '1' }, "skip": 0,
            "limit": 100, "sort": "asc", "sortBy": "amount"
        }))
    }

    const getBundleList = () => {
        dispatch(actionCreators.requestCoinPackList({
            'filter': { 'coinType': '2' }, "skip": 0,
            "limit": 100, "sort": "asc", "sortBy": "amount"
        }))
    }

    const getAvatarList = () => {
        dispatch(actionCreators.requestAvatarCategoryList({ "filter": {}, "sort": "asc", "sortBy": "order", "skip": 0, "limit": 100, "sort": "asc", "sortBy": "amount", "page": 1 }))
    }

    const getEarnCoinList = () => {
        dispatch(actionCreators.requestEarnCoinList({ "filter": {}, "sort": "desc", "sortBy": "createdAt", "skip": 0, "limit": 10, "page": 1 }))
    }


    const fetchAccountData = () => {
        dispatch(actionCreators.requestAccount())
    };
    const { currentStateAccount } = useSelector(
        (state) => ({ currentStateAccount: state.account.model }),
        shallowEqual
    );
    const { accountResponse } = currentStateAccount

    // Account Response
    useEffect(() => {
        if (accountResponse) {
            setAccountData(accountResponse)
        }
    }, [accountResponse])

    //API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.coinRewardStore.model }),
        shallowEqual
    );
    var { coinPackListResponse, avatarCategoryListResponse, buyAvatarResponse } = currentState

    //API Response
    const { rewardCurrentState } = useSelector(
        (state) => ({ rewardCurrentState: state.rewardStore.model }),
        shallowEqual
    );
    var { rewardCategoryListResponse } = rewardCurrentState

    //API Response
    const { myRewardCurrentState } = useSelector(
        (state) => ({ myRewardCurrentState: state.myRewards.model }),
        shallowEqual
    );
    var { myRewardListResponse } = myRewardCurrentState

    //API Response
    const { earnCoinsCurrentState } = useSelector(
        (state) => ({ earnCoinsCurrentState: state.earnCoins.model }),
        shallowEqual
    );
    var { earnCoinListResponse } = earnCoinsCurrentState

    //claim Coin Response
    const { claimCoinState } = useSelector(
        (state) => ({ claimCoinState: state.claimCoin.model }),
        shallowEqual
    );
    var { claimCoinResponse } = claimCoinState

    // Coin Pack List Response
    useEffect(() => {
        if (coinPackListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (coinPackListResponse.list && coinPackListResponse.list.length > 0) {

                if (coinPackListResponse.list[0].coinType == '1') {
                    setPackList(coinPackListResponse.list)
                }
                if (coinPackListResponse.list[0].coinType == '2') {
                    setBundleList(coinPackListResponse.list)
                }
            }
        }
        coinPackListResponse = undefined
    }, [coinPackListResponse])

    // Avatar Category List Response
    useEffect(() => {
        if (avatarCategoryListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (avatarCategoryListResponse.list) {
                setAvatarList(avatarCategoryListResponse.list)
            }
        }
        avatarCategoryListResponse = undefined
    }, [avatarCategoryListResponse])

    // Buy Avatar Response
    useEffect(() => {
        if (buyAvatarResponse) {
            setLoading(false)
            setRefreshing(false)

            if (buyAvatarResponse.item) {
                showSuccessToastMessage('Avatar purchased successfully')
                getAvatarList()
            } else if (buyAvatarResponse.errors) {
                if (buyAvatarResponse.errors[0] && buyAvatarResponse.errors[0].msg) {
                    showErrorToastMessage(buyAvatarResponse.errors[0].msg)
                }
            }
        }
        buyAvatarResponse = undefined
    }, [buyAvatarResponse])

    // Category List Response
    useEffect(() => {
        if (rewardCategoryListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (rewardCategoryListResponse.list) {
                setRewardList(rewardCategoryListResponse.list)
            }
        }
        rewardCategoryListResponse = undefined
    }, [rewardCategoryListResponse])

    // My Reward List Response
    useEffect(() => {
        if (myRewardListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (myRewardListResponse.list) {
                //moment(item.redeemtime).format('DD/MM/yyyy hh:mm A')

                setMyRewardList(myRewardListResponse.list)
            }
        }
        myRewardListResponse = undefined
    }, [myRewardListResponse])

    // EarnCoins List Response
    useEffect(() => {
        setLoading(false)
        setRefreshing(false)

        if (earnCoinListResponse) {

            if (earnCoinListResponse.list) {

                // var sortedData = earnCoinListResponse.list;
                // sortedData = sortedData.sort(function (a, b) {
                //     return (a.amount < b.amount) ? -1 : (a.amount > b.amount) ? 1 : 0;
                // })

                setEarnCoinsList(earnCoinListResponse.list)
            }
        }
        earnCoinListResponse = undefined
    }, [earnCoinListResponse])

    // Claim Coins Response
    useEffect(() => {
        if (claimCoinResponse) {
            // setLoading(false)

            if (claimCoinResponse.success && claimCoinResponse.success == true) {
                showSuccessToastMessage("Coin Claimed successfully")
                dispatch(actionCreators.requestEarnCoinList({ "filter": {}, "sort": "desc", "sortBy": "createdAt", "skip": 0, "limit": 10, "page": 1 }))

            } else {
                setLoading(false)
                if (claimCoinResponse.errors[0] && claimCoinResponse.errors[0].msg) {
                    showErrorToastMessage(claimCoinResponse.errors[0].msg)

                }
            }
        }
        claimCoinResponse = undefined
    }, [claimCoinResponse])

    const selectTopTab = (value) => {
        setTopActiveTab(value)

        if (value == 'coinStore') {
            selectCoinContentTab(coinContentActiveTab)
        } else if (value == 'rewards') {
            selectRewardsContentTab(rewardContentActiveTab)
        }
        else if (value == 'earnCoins') {
            setLoading(true)
            getEarnCoinList()
        }
    }

    const selectCoinContentTab = (value) => {
        setCoinContentActiveTab(value)

        if (value == 'packs' && packList.length == 0) {
            setLoading(true)
            getPackList()
        }
        if (value == 'bundles' && bundleList.length == 0) {
            setLoading(true)
            getBundleList()
        }
        if (value == 'avatars' && avatarList.length == 0) {
            setLoading(true)
            getAvatarList()
        }
    }

    const selectRewardsContentTab = (value) => {
        setRewardContentActiveTab(value)

        if (value == 'rewards' && rewardList.length == 0) {
            setLoading(true)
            dispatch(actionCreators.requestRewardCategoryList({ sortby: 'order', sort: 'asc' }))
        }

        if (value == 'myRewards') {
            if (myRewardList.length == 0) {
                setLoading(true)
            }
            dispatch(actionCreators.requestMyRewardList({ "sort": "desc", "sortBy": "createdAt" }))
        }
    }

    const selectBundle = (item) => {
        setPurchaseType('bundle')
        setSelectedAvatar(item)
        setOpenPurchaseAvatarPopup(true)
    }

    const selectAvatar = (item) => {
        setPurchaseType('avatar')
        setSelectedAvatar(item)
        setOpenPurchaseAvatarPopup(true)
    }

    const handleBackButton = () => {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const purchaseItemClickedFromPopup = (selectedAvatarToPurchase, purchaseType) => {
        if (purchaseType == 'bundle') {

            purchaseCoinAvatar(selectedAvatarToPurchase, purchaseType)

        } else {
            let avatarAmount = selectedAvatarToPurchase.coinAmount
            let walletCoinAmount = global.profile && global.profile.wallet && global.profile.wallet.coinAmount
            if (walletCoinAmount < avatarAmount) {
                showErrorToastMessage('You don\'t have sufficient coins to purchase this avatar.')
            } else {
                setLoading(true)
                dispatch(actionCreators.requestBuyAvatar({ 'avatar': selectedAvatarToPurchase._id }))
            }
        }
    }

    const purchaseCoinAvatar = (item, type) => {

        var params = {
            amount: item.amount.toString(),
            coinStore: {
                coinStoreId: item._id,
                currencyId: item.currency && item.currency._id
            }
        }

        if (type == 'packs') {
            global.addBalanceScreenType = Constants.coin_pack_payment
            params['paymentTypeForCreateTransaction'] = 17
        } else {
            global.addBalanceScreenType = Constants.coin_avatar_bundle_payment
            params['paymentTypeForCreateTransaction'] = 20
            params['avatarId'] = item.avatar && item.avatar._id
        }
        global.addBalanceCurrencyType = Constants.coin_payment
        props.navigation.push(Constants.nav_payment_options_new, params)
    }

    const redeemRewardPressed = (item) => {
        setSelectedRewardToRedeem(item)
        setOpenRedeemRewardPopup(true)
    }
    const moveToAddCoinScreen = () => {
        global.addBalanceScreenType = Constants.wallet_payment_type
        //   global.paymentSuccessCompletion = refreshWallet
        props.navigation.navigate(Constants.nav_add_balance)
    }

    //Redirect Screens
    const redirectScreen = (prop) => {
        // showSuccessToastMessage(prop.code)
        switch (prop.code) {

            case 'contestsList':
                let gameType = item.gameType
                RootNavigation.navigate(Constants.nav_contest_list, { gameType })
                break;

            case 'profile':
                RootNavigation.navigate(Constants.nav_profile_screen)
                break;

            case 'applyPromoCode':
                props.openApplyPromoCodePopup(true)
                break;

            case 'addABalanceWallet':
                global.addBalanceScreenType = Constants.all_games_add_money
                global.addBalanceCurrencyType = Constants.coin_payment
                global.paymentSuccessCompletion = props.getProfileData
                RootNavigation.navigate(Constants.nav_coin_reward_store)
                break;

            case 'URLRedirect':
                Linking.canOpenURL(item.url).then(supported => {
                    if (supported) {
                        Linking.openURL(item.url)
                    }
                })
                break;

            case 'tournamentLists':
                let game = item.game
                RootNavigation.navigate(Constants.nav_tournament_list, { game });
                break;

            case 'coinStore':
                RootNavigation.navigate(Constants.nav_coin_reward_store);

                break;

            // case 'paymentGateway':
            //     RootNavigation.navigate(Constants.nav_payment_gateway_new);//getting error
            //     break;

            case 'leaderboard':
                RootNavigation.navigate(Constants.nav_more_leaderboard);
                break;

            case 'applyPromoCodePopup':
                RootNavigation.navigate(Constants.nav_more_apply_promo_code);
                break;

            case 'dailyLoginRewards':
                RootNavigation.navigate(Constants.nav_daily_login_rewards);
                break;

            case 'editProfile':
                RootNavigation.navigate(Constants.nav_edit_profile);
                break;

            case 'esportsNewsDetails':
                RootNavigation.navigate(Constants.nav_view_all_esports_news);
                break;

            case 'gamerjiPoints':
                RootNavigation.navigate(Constants.nav_more_gamerji_points);
                break;

            case 'more':
                RootNavigation.navigate(Constants.nav_more);
                break;

            case 'viewAllTopProfiles':
                RootNavigation.navigate(Constants.nav_view_all_top_profiles);
                break;

            case 'rewardStore':
                let rewards = 'rewards';
                let myRewards = 'rewards';

                RootNavigation.navigate(Constants.nav_coin_reward_store, { rewards, myRewards });
                break;

            case 'myRewards':
                rewards = 'rewards';
                myRewards = 'myRewards';
                RootNavigation.navigate(Constants.nav_coin_reward_store, { rewards, myRewards });
                break;

            case 'collegeLeagues':
                let activeProfile = true;
                RootNavigation.navigate(Constants.nav_profile_screen, { activeProfile });
                break;

            case 'worldOfEsports':
                RootNavigation.navigate(Constants.nav_world_of_esports);
                break;

                // case 'htmlGames':
                //     RootNavigation.navigate(Constants.nav_html_game_type, { game: item });

                break;
            default:
                break;
        }

    }

    //Claim Coin
    const claimCoin = (id, amount) => {

        setLoading(true)

        dispatch(actionCreators.requestClaimCoins({ 'amount': amount }, id + ""))

    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={() => RootNavigation.goBack()}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center', tintColor: colors.white }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >REWARD STORE</Text>
            </View>

            {/* Tabs */}
            <View style={{ marginLeft: 20, marginRight: 20, height: 35, flexDirection: 'row' }} >

                {/* Coin Store */}
                <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center' }} onPress={() => selectTopTab('coinStore')} activeOpacity={0.5} >

                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Coin Store</Text>
                    {topActiveTab == 'coinStore' &&
                        <Image style={{ position: 'absolute', bottom: 0, height: 2, width: '100%', backgroundColor: colors.yellow }} />
                    }
                    <Image style={{ position: 'absolute', top: 10, right: 0, bottom: 10, width: 1, backgroundColor: '#CDA122' }} />
                </TouchableOpacity>

                {/* Rewards */}
                <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center' }} onPress={() => selectTopTab('rewards')} activeOpacity={0.5} >

                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Rewards</Text>
                    {topActiveTab == 'rewards' &&
                        <Image style={{ position: 'absolute', bottom: 0, height: 2, width: '100%', backgroundColor: colors.yellow }} />
                    }
                    <Image style={{ position: 'absolute', top: 10, right: 0, bottom: 10, width: 1, backgroundColor: '#CDA122' }} />
                </TouchableOpacity>

                {/* Earn Coins */}
                <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center' }} onPress={() => selectTopTab('earnCoins')} activeOpacity={0.5} >

                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Earn<Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />Coins</Text>
                    {topActiveTab == 'earnCoins' &&
                        <Image style={{ position: 'absolute', bottom: 0, height: 2, width: '100%', backgroundColor: colors.yellow }} />
                    }
                </TouchableOpacity>
                {/* My Rewards */}
                {/* <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center' }} onPress={() => selectTopTab('myRewards')} activeOpacity={0.5} >

                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >My Rewards</Text>
                    {topActiveTab == 'myRewards' &&
                        <Image style={{ position: 'absolute', bottom: 0, height: 2, width: '100%', backgroundColor: colors.yellow }} />
                    }
                </TouchableOpacity> */}
                {/* My Rewards */}
                {/* <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center' }} onPress={() => selectTopTab('myRewards')} activeOpacity={0.5} >

                    <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >My Rewards</Text>
                    {topActiveTab == 'myRewards' &&
                        <Image style={{ position: 'absolute', bottom: 0, height: 2, width: '100%', backgroundColor: colors.yellow }} />
                    }
                </TouchableOpacity> */}
            </View>

            {/* Add Coin */}
            <View style={{ marginTop: 25, height: 50, width: 150, backgroundColor: '#EEBB14', alignSelf: 'center', justifyContent: 'center', borderRadius: 25, flexDirection: 'row' }} >



                {/* Coin Balance */}
                <Text style={{ marginLeft: 10, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, alignSelf: 'center', justifyContent: 'center', }} ><Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain' }} source={require('../../assets/images/COIN_3d.png')} /> {global.profile && global.profile.wallet && global.profile.wallet.coinAmount}</Text>

                {/* Add */}
                {/* <TouchableOpacity onPress={() => moveToAddCoinScreen()} style={{ marginLeft: 22, marginRight: 16, height: 34, width: 90, backgroundColor: colors.black, borderRadius: 17, justifyContent: 'center', alignItems: 'center' }} >
                    <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Add</Text>
                </TouchableOpacity> */}
            </View>

            <View style={{ flex: 1 }}>
                {/* Coin Store Content */}
                {topActiveTab == 'coinStore' &&
                    <>
                        <View style={{ marginTop: 48, flex: 1, backgroundColor: colors.white, borderTopLeftRadius: 60, borderTopRightRadius: 60, overflow: 'hidden' }}>

                            {/* Packs */}
                            {coinContentActiveTab == 'packs' &&
                                <View style={{ flex: 1 }} >
                                    <FlatList
                                        style={{ marginTop: 38, marginLeft: 5, marginRight: 5 }}
                                        numColumns={3}
                                        data={packList}
                                        renderItem={({ item, index }) => <StorePacksItem item={item} index={index} purchaseCoinAvatar={purchaseCoinAvatar} />}
                                        showsVerticalScrollIndicator={false}

                                        refreshControl={
                                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                        }
                                    />

                                </View>
                            }

                            {/* Packs */}
                            {coinContentActiveTab == 'bundles' &&
                                <View style={{ flex: 1 }} >
                                    <FlatList
                                        style={{ marginTop: 38, marginLeft: 5, marginRight: 5 }}
                                        numColumns={2}
                                        data={bundleList}
                                        renderItem={({ item, index }) => <StoreBundleItem item={item} index={index} selectBundle={selectBundle} />}
                                        showsVerticalScrollIndicator={false}

                                        refreshControl={
                                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                        }
                                    />

                                </View>
                            }

                            {/* Avatars */}
                            {coinContentActiveTab == 'avatars' &&
                                <View style={{ flex: 1 }} >
                                    <FlatList
                                        style={{ marginTop: 38 }}
                                        data={avatarList}
                                        renderItem={({ item, index }) => <StoreAvatarCategoryItem item={item} index={index} selectAvatar={selectAvatar} />}

                                        refreshControl={
                                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                        }
                                    />

                                </View>
                            }
                        </View>

                        {/* Coin Store Tabs */}
                        <View style={{ position: 'absolute', top: 28, left: 40, right: 40, height: 40, borderRadius: 20, backgroundColor: colors.yellow, flexDirection: 'row', overflow: 'hidden', borderWidth: 1, borderColor: colors.black }} >

                            {/* Packs */}
                            <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center', backgroundColor: coinContentActiveTab == 'packs' ? colors.red : 'transparent', borderRadius: 20 }} onPress={() => selectCoinContentTab('packs')} activeOpacity={0.5} >

                                <Text style={{ color: coinContentActiveTab == 'packs' ? colors.white : colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Packs</Text>
                            </TouchableOpacity>

                            {/* Bundles */}
                            <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center', backgroundColor: coinContentActiveTab == 'bundles' ? colors.red : 'transparent', borderRadius: 20 }} onPress={() => selectCoinContentTab('bundles')} activeOpacity={0.5} >
                                <Text style={{ color: coinContentActiveTab == 'bundles' ? colors.white : colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Bundles</Text>
                            </TouchableOpacity>

                            {/* Avatars */}
                            <TouchableOpacity style={{ width: '33.33%', justifyContent: 'center', backgroundColor: coinContentActiveTab == 'avatars' ? colors.red : 'transparent', borderRadius: 20 }} onPress={() => selectCoinContentTab('avatars')} activeOpacity={0.5} >
                                <Text style={{ color: coinContentActiveTab == 'avatars' ? colors.white : colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Avatars</Text>
                            </TouchableOpacity>

                        </View>
                    </>
                }

                {topActiveTab == 'rewards' &&
                    <>
                        <View style={{ marginTop: 48, flex: 1, backgroundColor: colors.white, borderTopLeftRadius: 60, borderTopRightRadius: 60, overflow: 'hidden' }}>
                            {/* Rewards */}
                            {rewardContentActiveTab == 'rewards' &&
                                <View style={{ flex: 1 }} >
                                    <FlatList
                                        style={{ marginTop: 38, marginLeft: 5, marginRight: 5 }}
                                        data={rewardList}
                                        renderItem={({ item, index }) => <RewardCategoryItem item={item} index={index} redeemRewardPressed={redeemRewardPressed} />}
                                        showsVerticalScrollIndicator={false}

                                        refreshControl={
                                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                        }
                                    />
                                </View>
                            }

                            {/* My Rewards */}
                            {rewardContentActiveTab == 'myRewards' &&
                                <View style={{ flex: 1, marginBottom: 30 }} >
                                    <FlatList
                                        style={{ marginTop: 38, marginLeft: 5, marginRight: 5 }}
                                        data={myRewardList}
                                        renderItem={({ item, index }) => <MyRewardItemNew item={item} index={index} />}
                                        showsVerticalScrollIndicator={false}

                                        refreshControl={
                                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                        }
                                    />
                                </View>
                            }
                        </View>

                        {/* Rewards Tabs */}
                        <View style={{ position: 'absolute', top: 28, left: 40, right: 40, height: 40, borderRadius: 20, backgroundColor: colors.yellow, flexDirection: 'row', overflow: 'hidden', borderWidth: 1, borderColor: colors.black }} >

                            {/* Rewards */}
                            <TouchableOpacity style={{ width: '50%', justifyContent: 'center', backgroundColor: rewardContentActiveTab == 'rewards' ? colors.red : 'transparent', borderRadius: 20 }} onPress={() => selectRewardsContentTab('rewards')} activeOpacity={0.5} >

                                <Text style={{ color: rewardContentActiveTab == 'rewards' ? colors.white : colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Rewards</Text>
                            </TouchableOpacity>

                            {/* My Rewards */}
                            <TouchableOpacity style={{ width: '50%', justifyContent: 'center', backgroundColor: rewardContentActiveTab == 'myRewards' ? colors.red : 'transparent', borderRadius: 20 }} onPress={() => selectRewardsContentTab('myRewards')} activeOpacity={0.5} >
                                <Text style={{ color: rewardContentActiveTab == 'myRewards' ? colors.white : colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >My Rewards</Text>
                            </TouchableOpacity>
                        </View>
                    </>
                }

                {/* Earn Coin */}
                {topActiveTab == 'earnCoins' &&
                    <>
                        <View style={{ marginTop: 10, flex: 1, marginBottom: 10, backgroundColor: colors.white, borderRadius: 60, overflow: 'hidden' }}>
                            {/* Rewards
                            {rewardContentActiveTab == 'rewards' && */}
                            <View style={{ flex: 1, marginBottom: 30 }} >
                                <FlatList
                                    style={{ marginTop: 38, marginLeft: 5, marginRight: 5 }}
                                    data={earnCoinsList}
                                    renderItem={({ item, index }) => <StoreEarnCoinsItem item={item} index={index} redirectScreen={redirectScreen} claimCoin={claimCoin} />}
                                    showsVerticalScrollIndicator={false}

                                    refreshControl={
                                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                                    }
                                />
                                {/* </View>
                            } */}


                            </View>

                        </View>
                    </>
                }
            </View>

            {isLoading && <CustomProgressbar />}

            {isOpenPurchaseAvatarPopup &&
                <Modal
                    isVisible={isOpenPurchaseAvatarPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <PurchaseAvatarPopup setOpenPurchaseAvatarPopup={setOpenPurchaseAvatarPopup} selectedAvatarToPurchase={selectedAvatar} avatarList={purchaseType == 'bundle' ? bundleList : avatarList} purchaseType={purchaseType} purchaseItemClickedFromPopup={purchaseItemClickedFromPopup} />
                </Modal>
            }

            {isOpenRedeemRewardPopup &&
                <Modal
                    isVisible={isOpenRedeemRewardPopup}
                    coverScreen={false}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <RewardStoreRedeemPopup setOpenMyRewardDetailPopup={setOpenMyRewardDetailPopup} setOpenRedeemPopup={setOpenRedeemRewardPopup} voucher={selectedRewardToRedeem} />
                </Modal>
            }

        </SafeAreaView>
    )
}

const StorePacksItem = props => {

    const item = props.item

    const moveToPaymentOptionScreen = () => {

        props.purchaseCoinAvatar(item, 'packs')
    }

    return (
        <TouchableOpacity style={{ marginLeft: 5, marginRight: 5, marginBottom: 10, width: (Constants.SCREEN_WIDTH - 40) / 3, height: 130 }} activeOpacity={0.8} onPress={() => moveToPaymentOptionScreen()} >

            <View style={{ height: 120, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, backgroundColor: colors.black, alignItems: 'center' }} >

                {/* Icon */}
                <Image style={{ marginTop: 10, height: 42, width: '100%', resizeMode: 'contain' }} source={require('../../assets/images/purchase-pack-icon.png')} />

                {/* Coin Amount */}
                <Text style={{ marginTop: 6, color: colors.white, fontSize: 18, fontFamily: fontFamilyStyleNew.bold }} >{item.coins}</Text>
                <Text style={{ marginTop: 2, color: colors.white, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} >Coins</Text>
            </View>

            <View style={{ position: 'absolute', left: 8, right: 8, bottom: 0, height: 28, borderRadius: 13, backgroundColor: colors.red, alignItems: 'center' }} >
                <Text style={{ marginTop: 6, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Buy {item.currency && item.currency.symbol}{item.amount}</Text>
            </View>
        </TouchableOpacity>
    )
}

const StoreBundleItem = props => {

    const item = props.item
    addLog("StoreBundleItem==", props.item)

    return (
        <TouchableOpacity style={{ marginLeft: 5, marginRight: 5, marginBottom: 10, width: (Constants.SCREEN_WIDTH - 30) / 2, height: 316 }} activeOpacity={0.8} onPress={() => props.selectBundle(item)} >

            <View style={{ marginTop: 74, height: 232, flexDirection: 'row', justifyContent: 'center' }} >

                {/* Background Image */}
                <Image style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', resizeMode: 'stretch' }} source={require('../../assets/images/inside-curved-bg-bundle.png')} />

                {/* Description */}
                <View style={{ marginTop: 160, marginLeft: 4, marginRight: 4 }} >

                    {/* Coins */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                        <Image style={{ width: 14, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                        <Text style={{ marginLeft: 5, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >{item.coins}</Text>
                        <Text style={{ marginLeft: 2, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} >Coins</Text>
                    </View>
                    <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.bold, alignSelf: 'center' }} >+</Text>

                    {/* Character Name */}
                    <View style={{ alignItems: 'center' }}>
                        <CustomMarquee style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} value={item.avatar && item.avatar.name} />
                    </View>
                </View>

            </View>

            {/* Amount */}
            <View style={{ position: 'absolute', left: 8, right: 8, bottom: 0, height: 28, borderRadius: 13, backgroundColor: colors.red, alignItems: 'center' }} >
                <Text style={{ marginTop: 6, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Buy {item.currency && item.currency.symbol}{item.amount}</Text>
            </View>

            {/* Character */}
            <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 232, width: '100%', resizeMode: 'contain' }} source={item.avatar && item.avatar.img && item.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.avatar.img.default }} />

        </TouchableOpacity>
    )
}

const StoreAvatarCategoryItem = props => {

    const item = props.item

    return (
        <View style={{ marginBottom: 24, height: 326 }} >

            {/* Category Title */}
            <View style={{ marginLeft: 26, flexDirection: 'row', alignItems: 'center' }} >
                <Image style={{ width: 36, height: 36, resizeMode: 'contain' }} source={item.icon && item.icon.default && { uri: generalSetting.UPLOADED_FILE_URL + item.icon.default }} />
                <Image style={{ marginLeft: 8, width: 2, height: 17, backgroundColor: colors.black }} />
                <Text style={{ marginLeft: 8, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >{item.name}</Text>
            </View>

            <FlatList
                style={{ marginTop: 8, marginLeft: 5, marginRight: 5 }}
                data={item.avatars}
                renderItem={({ item, index }) => <StoreAvatarSubItem item={item} index={index} selectAvatar={props.selectAvatar} />}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
        </View>
    )
}

const StoreAvatarSubItem = props => {
    const checkAvatars = (avatar_id) => {
        var avatarArr = [];
        addLog(" global.profile.avatars==>", global.profile.avatars);
        global.profile.avatars.forEach((entry) => {
            if (entry != null) {
                avatarArr.push(entry._id);
            }

        });

        return avatarArr.includes(avatar_id)
    }
    const item = props.item

    return (
        <TouchableOpacity style={{ marginLeft: 5, marginRight: 5, width: 150, height: 280 }} activeOpacity={0.8} onPress={() => props.selectAvatar(item)} >


            <View style={{ marginTop: 84, height: 186, flexDirection: 'row', justifyContent: 'center' }} >

                {/* Background Image */}
                <Image style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', resizeMode: 'stretch' }} source={require('../../assets/images/inside-curved-bg-bundle.png')} />
            </View>
            <View style={{ position: 'absolute', left: 8, right: 8, bottom: 0, height: 28, borderRadius: 13, backgroundColor: colors.red, alignItems: 'center' }} >

                {checkAvatars(item._id) == false ?
                    <Text style={{ marginTop: 4, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Buy <Image style={{ height: 15, width: 14, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} /> {item.coinAmount}</Text>
                    : <Text style={{ marginTop: 4, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Bought</Text>
                }
            </View>

            {/* Character */}
            <Text style={{ position: 'absolute', alignSelf: 'center', bottom: 30, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >{item.name}</Text>
            <Image style={{ position: 'absolute', top: 0, height: 232, width: '80%', resizeMode: 'contain', alignSelf: 'center' }} source={item.img && item.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} />


        </TouchableOpacity>
    )
}

const RewardCategoryItem = props => {

    const item = props.item

    return (
        <View style={{ marginBottom: 17 }} >

            {/* Category Title */}
            <View style={{ marginLeft: 26, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} >
                <Image style={{ width: 36, height: 36, borderRadius: 18, resizeMode: 'contain' }} source={item.img && item.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} />
                <Text style={{ marginLeft: 8, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >{item.name}</Text>
                <Image style={{ marginTop: 4, marginLeft: 8, height: 1, flex: 1, backgroundColor: '#E0DEDE' }} />
            </View>

            <FlatList
                style={{ marginTop: 8, marginLeft: 5, marginRight: 5 }}
                data={item.products}
                renderItem={({ item, index }) => <RewardSubCategoryItem item={item} index={index} redeemRewardPressed={props.redeemRewardPressed} />}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
        </View>
    )
}

const RewardSubCategoryItem = props => {

    const item = props.item

    return (
        <TouchableOpacity style={{ marginLeft: 5, marginRight: 5, width: 150, height: 192, borderRadius: 9, borderColor: colors.black, borderWidth: 1, overflow: 'hidden' }} activeOpacity={0.8} onPress={() => props.redeemRewardPressed(item)} >

            {/* Content Image */}
            <Image style={{ flex: 1, width: 150 }} source={item.img && item.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} />
            <View style={{ marginTop: 6, marginLeft: 6, }}>
                <CustomMarquee style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} value={item.name} />
            </View>
            {/* <Text style={{  color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >{item.name}</Text> */}
            {/* Content */}
            <View style={{ height: 42, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >

                <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                    {/* Coin Icon */}
                    <Image style={{ marginLeft: 6, width: 14, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />

                    {/* Coin Balance */}
                    <Text style={{ marginLeft: 6, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }} >{item.coinAmount}</Text>
                </View>

                {/* Reedem */}
                <TouchableOpacity style={{ marginRight: 6, height: 20, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.yellow, borderRadius: 10 }} activeOpacity={0.6} onPress={() => props.redeemRewardPressed(props.item)}>
                    <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, }} >Redeem</Text>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}

const MyRewardsItem = props => {

    const item = props.item
    const width = (Constants.SCREEN_WIDTH - 30) / 2
    const [isOpenMyRewardDetailPopup, setOpenMyRewardDetailPopup] = useState(false)

    return (
        <View style={{ marginLeft: 5, marginRight: 5, width: 180, height: 214, borderRadius: 9, borderColor: colors.black, borderWidth: 1, overflow: 'hidden' }} >
            {/* Content Image */}
            <Image style={{ flex: 1, backgroundColor: 'green' }} source={item.img && item.img.default && { uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} />
            {/* <Text style={{ marginTop: 6, marginLeft: 6, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >{item.name}</Text> */}
            <View style={{ marginTop: 6, marginLeft: 6, }}>
                <CustomMarquee style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} value={item.name} />
            </View>

            {/* Content */}
            <View style={{ height: 36, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >

                <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                    {/* Coin Icon */}
                    <Image style={{ marginLeft: 6, width: 14, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />

                    {/* Coin Balance */}
                    <Text style={{ marginLeft: 6, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }} >{item.coinAmount}</Text>
                </View>

                {/* Reedem */}
                <TouchableOpacity style={{ marginRight: 6, height: 20, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.yellow, borderRadius: 10 }} activeOpacity={0.6} onPress={() => setOpenMyRewardDetailPopup(true)}>
                    <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold, }} >Info</Text>
                </TouchableOpacity>
            </View>

            {isOpenMyRewardDetailPopup &&
                <Modal
                    isVisible={isOpenMyRewardDetailPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <MyRewardDetailPopup setOpenMyRewardDetailPopup={setOpenMyRewardDetailPopup} myReward={item} />
                </Modal>
            }

        </View>
    )
}



const StoreEarnCoinsItem = props => {

    const item = props.item
    addLog("StoreEarnCoinsItem==", props.item)

    return (
        <View style={{ marginLeft: 5, marginRight: 5, marginBottom: 10, height: 100 }} >

            <View style={{ marginTop: 10, height: 100, flexDirection: 'row', justifyContent: 'center' }} >

                {/* Background */}
                <View style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', resizeMode: 'stretch', backgroundColor: colors.black, borderRadius: 13 }} />

                {/* Main */}
                <View style={{ flex: 1, }}>
                    <View style={{ marginTop: 10, marginLeft: 10, flex: 1, flexDirection: "row", }}>
                        <Image source={{ uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} style={{ width: 26, height: 26, marginTop: 10 }} />
                        <View style={{ flex: 1, marginLeft: 10 }}>
                            <Text style={{ fontSize: 16, color: colors.white, fontFamily: fontFamilyStyleNew.semiBold }} numberOfLines={1} ellipsizeMode='tail'>{item.name}</Text>
                            <Text style={{ fontSize: 10, color: colors.white, marginTop: 6, fontFamily: fontFamilyStyleNew.semiBold }} numberOfLines={2} ellipsizeMode='tail'>{item.description}</Text>
                        </View>
                        {/* Redirect */}
                        {item.isRedirect &&
                            <TouchableOpacity style={{}} onPress={() => props.redirectScreen(item.screen)}>
                                <View style={{ paddingLeft: 10, paddingRight: 5, marginBottom: 10, flex: 1, flexDirection: 'row', width: 95, borderBottomLeftRadius: 13, borderTopLeftRadius: 13, backgroundColor: '#EEBB14', justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: colors.black, width: 60, fontSize: 12 }}>{item.redirectTitle}</Text>
                                    <Image source={require('../../assets/images/right_arrow.png')} style={{ width: 20, height: 16 }} tintColor="black" />

                                </View>
                            </TouchableOpacity>}

                    </View>

                    {item.isClaimed ?
                        // Claimed
                        <View style={{ borderRadius: 13, backgroundColor: '#EEBB14', height: 35, justifyContent: "center", }}>
                            <Text style={{ fontSize: 17, color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center', }}>{item.amount}  <Image style={{ marginLeft: 16, width: 20, height: 20, resizeMode: 'contain', }} source={{ uri: generalSetting.UPLOADED_FILE_URL + item.currency.img.default }} /> Collected</Text>

                        </View>
                        :
                        //Claim Now
                        <View style={{ height: 35, flexDirection: 'row', backgroundColor: '#EEBB14', borderRadius: 13 }} >

                            {/* Coin Store */}
                            <View style={{ width: '50%', justifyContent: 'center' }}  >

                                <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >GET <Image style={{ marginTop: 0, marginLeft: 16, width: 18, height: 18, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + item.currency.img.default }} /> {item.amount}</Text>

                                <Image style={{ position: 'absolute', top: 5, right: 0, bottom: 5, width: 2, backgroundColor: '#fff' }} />
                            </View>

                            {/* Rewards */}
                            <TouchableOpacity style={{ width: '50%', justifyContent: 'center' }} onPress={() => props.claimCoin(item._id, item.amount)}>

                                <Text style={{ color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Collect Now</Text>

                            </TouchableOpacity>

                        </View>}
                </View>
            </View>
        </View>
    )
}

const MyRewardItemNew = props => {

    const item = props.item
    addLog("MyRewardItemNew==", props.item)

    return (
        <View style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, height: 175 }} >

            <View style={{ marginTop: 10, height: 165, flexDirection: 'row', justifyContent: 'center' }} >

                {/* Background */}
                <View style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', resizeMode: 'stretch', backgroundColor: colors.black, borderRadius: 13 }} />

                {/* Main */}
                <View style={{ flex: 1, }}>
                    {/* Profile and Name */}
                    <View style={{ marginTop: 5, marginLeft: 10, flex: 1, flexDirection: "row", }}>
                        <Image source={{ uri: generalSetting.UPLOADED_FILE_URL + item.img.default }} style={{ width: 53, height: 53, marginTop: 8, borderRadius: 5, borderColor: colors.white, borderWidth: 1 }} />
                        <View style={{ flex: 1, marginLeft: 15, marginTop: 12 }}>
                            <Text style={{ fontSize: 18, color: 'white', fontFamily: fontFamilyStyleNew.bold }}>{item.name}</Text>
                            <Text style={{ fontSize: 12, color: 'white', marginTop: 6, fontFamily: fontFamilyStyleNew.regular }}>{moment(item.redeemtime).format('DD/MM/yyyy hh:mm A')}</Text>
                        </View>

                    </View>

                    <View style={{ height: 50, flexDirection: 'row', backgroundColor: '#EEBB14', }} >

                        {/* Coin */}
                        <View style={{ width: '50%', justifyContent: 'center' }}  >

                            <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} > <Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain', paddingBottom: 10 }} source={require('../../assets/images/COIN_3d.png')} /> {item.coinAmount}</Text>

                            <Image style={{ position: 'absolute', top: 0, right: 0, bottom: 0, width: 2, backgroundColor: colors.black, }} />
                        </View>

                        {/* Status */}
                        <View style={{ width: '50%', justifyContent: 'center' }} >

                            {item.deliveryStatus == 'delivered' &&
                                <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} ><Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain', }} source={require('../../assets/images/success-payment.png')} /> Success</Text>
                            }

                            {item.deliveryStatus == 'pending' &&
                                <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} ><Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain', }} source={require('../../assets/images/pending-payment.png')} /> Pending</Text>
                            }

                            {item.deliveryStatus == 'FAILED' &&
                                <Text style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} ><Image style={{ marginLeft: 16, width: 24, height: 21, resizeMode: 'contain', }} source={require('../../assets/images/fail-payment.png')} /> Failed</Text>
                            }
                        </View>


                    </View>

                    {/* Redeem Reward */}
                    <View style={{ height: 35, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 13, paddingLeft: 10 }} >

                        {/* Redeem Voucher code */}
                        <View style={{ width: '80%', flexDirection: 'row', alignItems: 'center' }}  >

                            {/* <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'left', marginLeft: 5, marginBottom: 5 }} > <Image style={{ marginRight: 10, width: 25, height: 25, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/discount.png')} /> {item.redeemVoucherCode}</Text> */}
                            <Image style={{ marginRight: 10, width: 25, height: 25, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/discount.png')} />
                            <CustomMarquee style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} value={item.redeemVoucherCode} />

                        </View>

                        {/* Copy */}
                        <TouchableOpacity style={{ width: '20%' }} onPress={() => copyCode(item.redeemVoucherCode)} disabled={item.deliveryStatus == 'delivered'} >
                            <View style={{ height: "100%", backgroundColor: 'red', justifyContent: 'center', borderBottomRightRadius: 13, }} >

                                <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >COPY</Text>
                            </View>
                        </TouchableOpacity>


                    </View>
                </View>
            </View>
        </View>
    )
}


// Copy Code
const copyCode = async (code) => {
    showSuccessToastMessage('Copied')
    Clipboard.setString(code)
}


// const StoreBundleItem = props => {

//     return (
//         <TouchableOpacity style={{ marginLeft: 5, marginRight: 5, marginBottom: 10, width: (Constants.SCREEN_WIDTH - 40) / 3, height: 240 }} >

//             <View style={{ marginTop: 54, height: 176, flexDirection: 'row', justifyContent: 'center' }} >

//                 {/* Background Image */}
//                 <Image style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', resizeMode: 'stretch' }} source={require('../../assets/images/inside-curved-bg-bundle.png')} />

//                 {/* Coin Icon */}
//                 <Image style={{ marginTop: 98, marginLeft: 5, width: 14, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/total-coin-icon.png')} />

//                 {/* Description */}
//                 <View style={{ marginTop: 98, marginLeft: 4, marginRight: 4 }} >

//                     {/* Coins */}
//                     <View style={{ flexDirection: 'row', alignItems: 'center' }} >
//                         <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >100000</Text>
//                         <Text style={{ marginLeft: 2, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} >Coins</Text>
//                     </View>
//                     <Text style={{ color: colors.black, fontSize: 10, fontFamily: fontFamilyStyleNew.semiBold, alignSelf: 'center' }} >+</Text>

//                     {/* Character Name */}
//                     <View>
//                         <CustomMarquee style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} value={'Bronze Avatar Avatar'} />
//                     </View>
//                 </View>

//             </View>

//             <View style={{ position: 'absolute', left: 8, right: 8, bottom: 0, height: 28, borderRadius: 13, backgroundColor: colors.red, alignItems: 'center' }} >
//                 <Text style={{ marginTop: 6, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >Buy ₹200</Text>
//             </View>

//             {/* Character */}
//             <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 146, width: '100%', resizeMode: 'contain' }} source={require('../../assets/images/dummy-character.png')} />

//         </TouchableOpacity>
//     )
// }

export default CoinRewardStore