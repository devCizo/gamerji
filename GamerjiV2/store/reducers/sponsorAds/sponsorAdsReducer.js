import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/sponsorAds/sponsorAdsAction'

{ /* Initialise - State */ }
const initialState = {
    model: {
        sponsorAdsListResponse: {},
    },
    error: false
};

{ /* Request - Sponsor Ads List */ }
const sponsorAdsList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.sponsorAdsListResponse[action.screenCode] = action.model;
        addLog('Success Response:::> ', action.screenCode)
    });
};

{ /* Request Failure - Sponsor Ads List */ }
const sponsorAdsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.sponsorAdsListResponse[action.screenCode] = action.model;
    });
};

{ /* Reset State - Sponsor Ads List */ }
const resetSponsorAdsState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.sponsorAdsListResponse[action.screenCode] = undefined;
    })
}

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SPONSOR_ADS_LIST:
            return sponsorAdsList(state, action);
        case actionTypes.SPONSOR_ADS_LIST_FAILED:
            return sponsorAdsListFailed(state, action);
        case actionTypes.RESET_SPONSOR_ADS_LIST_STATE:
            return resetSponsorAdsState(state);
        default:
            return state;
    }
};

export default reducer;