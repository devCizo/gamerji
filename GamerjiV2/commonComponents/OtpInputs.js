import React from 'react';
import { Content, Item, Input, View } from 'native-base';
import { containerStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, fontStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, heightStyles, widthStyles } from '../appUtils/commonStyles';
import { Grid, Col } from 'react-native-easy-grid';
import { addLog } from '../appUtils/commonUtlis';
// import RNOtpVerify from 'react-native-otp-verify';

class OtpInputs extends React.Component {
    state = { otp: [] };
    otpTextInput = [];

    componentDidMount() {
        this.otpTextInput[0]._root.focus();
    }

    nextPrevFocus = (event, index) => {
        const key = event.nativeEvent.key;
        if (key === 'Backspace') {
            if (index !== 0) {
                this.otpTextInput[index - 1]._root.focus();
            }
        } else {
            if (index !== 5) {
                this.otpTextInput[index + 1]._root.focus();
            }
        }
    }

    /*getHash = () => {
        addLog('getHash', '');
        RNOtpVerify.getHash()
            .then(console.log)
            .catch(console.log);
    }

    startListeningForOtp = () => {
        addLog('startListeningForOtp', '');
        RNOtpVerify.getOtp()
            .then(p => RNOtpVerify.addListener(this.otpHandler))
            .catch(p => console.log(p));
    }

    otpHandler = (message) => {
        addLog('otpHandler', '');
        const otp = /(\d{6})/g.exec(message)[1];
        this.setState({ otp });
        RNOtpVerify.removeListener();
        Keyboard.dismiss();
    }

    componentWillUnmount() {
        addLog('componentWillUnmount', '');
        RNOtpVerify.removeListener();
    }*/

    /* _onSmsListenerPressed = async () => {
        try {
            const registered = await SmsRetriever.startSmsRetriever();
            if (registered) {
                SmsRetriever.addSmsListener(this._onReceiveSms);
            }
            addLog('OTP SUCCESS:::> ', `SMS Listener Registered: ${registered}`);
        } catch (error) {
            addLog('OTP FAILURE:::> ', `SMS Listener Registered: ${JSON.stringify(error)}`);
        }
    };

    // Handlers
    _onReceiveSms = (event) => {
        alert(event.message);
        SmsRetriever.removeSmsListener();
    };*/

    setOtpValue(index, value) {
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
        this.props.getOtp(otp.join(''));
    }

    renderInputs() {
        const inputs = Array(6).fill(0);
        const txt = inputs.map(
            (i, j) =>

                <Item key={j} style={[shadowStyles.shadowBackground, heightStyles.height_48, widthStyles.width_48, colorStyles.whiteBackgroundColor, borderStyles.borderRadius_50, fontFamilyStyles.bold, colorStyles.greyColor, colorStyles.greyBorderColor, marginStyles.rightMargin_3, fontSizeStyles.fontSize_20]} >

                    <Input
                        style={[containerStyles.container, alignmentStyles.alignTextCenter, colorStyles.greyColor, alignmentStyles.alignSelfCenter, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor]}
                        keyboardType="numeric"
                        maxLength={1}
                        autoFocus={j === 0 ? true : false}
                        onChangeText={v => this.setOtpValue(j, v)}
                        onKeyPress={e => this.nextPrevFocus(e, j)}
                        ref={ref => this.otpTextInput[j] = ref} />
                </Item>
        );
        return txt;
    }


    render() {
        return (
            <Content >
                <Grid style={[containerStyles.container, justifyContentStyles.center, alignmentStyles.alignSelfCenter, marginStyles.topMargin_01, marginStyles.bottomMargin_01]}>
                    {this.renderInputs()}
                </Grid>
            </Content>
        );
    }
}

export default OtpInputs;