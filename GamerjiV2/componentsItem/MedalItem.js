import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import avatarData from '../assets/data/avatarData';
import * as generalSetting from '../webServices/generalSetting'

const MedalItem = (props) => {

  return (
    <TouchableOpacity activeOpacity={1}>

      {/* Wrapper - Border */}
      <View style={{ height: 80, width: 75, borderRadius: 5, backgroundColor: '#221E32', marginRight: 6, justifyContent: 'center', alignItems: 'center' }}>

        {/* Image - Avatar */}
        <Image style={{ height: 45, width: 50 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />

        {/* Text - Name */}
        <Text style={[fontFamilyStyles.regular, colorStyles.whiteColor, fontSizeStyles.fontSize_10, alignmentStyles.alignSelfCenter, marginStyles.topMargin_5]} numberOfLines={1} ellipsizeMode={'tail'}>{props.item.name}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default MedalItem;