import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST = 'BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST'
export const BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST_FAILED = 'BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST_FAILED'
export const COLLECT_REWARD = 'COLLECT_REWARD'
export const COLLECT_REWARD_FAILED = 'COLLECT_REWARD_FAILED'
export const RESET_BOTTOM_SHEET_DAILY_LOGIN_REWARDS_STATE = 'RESET_BOTTOM_SHEET_DAILY_LOGIN_REWARDS_STATE'
export const COLLECT_ONETIME_BONUS = 'COLLECT_ONETIME_BONUS'
export const COLLECT_ONETIME_BONUS_FAILED = 'COLLECT_ONETIME_BONUS_FAILED'

{ /* Request - Bottom Sheet Daily Login Rewards List */ }
export const bottomSheetDailyLoginRewardsList = (viewModel) => {
    return {
        type: BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Bottom Sheet Daily Login Rewards List */ }
export const bottomSheetDailyLoginRewardsListFailed = (viewModel) => {
    return {
        type: BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Bottom Sheet Daily Login Rewards List */ }
export const resetBottomSheetDailyLoginRewardsState = () => {
    return {
        type: RESET_BOTTOM_SHEET_DAILY_LOGIN_REWARDS_STATE
    };
};

{ /* Request - Bottom Sheet Daily Login Rewards */ }
export const requestBottomSheetDailyLoginRewards = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_LOGIN_REWARDS_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(bottomSheetDailyLoginRewardsList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(bottomSheetDailyLoginRewardsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const claimReward = (viewModel) => {
    return {
        type: COLLECT_REWARD,
        model: viewModel
    };
};

export const claimRewardFailed = (viewModel) => {
    return {
        type: COLLECT_REWARD_FAILED,
        model: viewModel
    };
};

export const requestClaimReward = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_LOGIN_REWARDS_LIST.COLLECT_DAILY_REWARD, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(claimReward(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(claimRewardFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const claimOneTimeBonus = (viewModel) => {
    return {
        type: COLLECT_ONETIME_BONUS,
        model: viewModel
    };
};

export const claimOneTimeBonusFailed = (viewModel) => {
    return {
        type: COLLECT_ONETIME_BONUS_FAILED,
        model: viewModel
    };
};

export const requestClaimOneTimeBonus = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.DAILY_LOGIN_REWARDS_LIST.COLLECT_ONETIME_BONUS, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(claimOneTimeBonus(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(claimOneTimeBonusFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};