import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList, Linking, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import TopProfilesItem from '../../componentsItem/TopProfilesItem';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';

const ViewAllTopProfiles = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [topProfiles, setTopProfiles] = useState([]);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 20

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToTopProfilesListAPI();

        return () => {
            dispatch(actionCreators.resetViewAllTopProfilesState())
        }
    }, []);

    // This Use-Effect function should call the Top Profiles API
    const callToTopProfilesListAPI = () => {
        let payload = {
            skip: topProfiles.length,
            limit: pageLimit,
            type: ["appUser"],
            sort: 'desc',
            sortBy: 'followers',
            action: "topProfile"
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestViewAllTopProfiles(payload))
    }

    // This function sets the View All Top Profiles API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.viewAllTopProfiles.model }),
        shallowEqual
    );
    var { viewAllTopProfileListResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (viewAllTopProfileListResponse) {
            setLoading(false)

            if (viewAllTopProfileListResponse.count == 0) {
                setTotalRecords(true)
            }

            if (viewAllTopProfileListResponse.list) {
                setIsFooterLoading(viewAllTopProfileListResponse.list.length == 0)
                var tempArr = viewAllTopProfileListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setTopProfiles([...topProfiles, ...tempArr])
            }
        }
        viewAllTopProfileListResponse = undefined
    }, [viewAllTopProfileListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Top Profiles Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.text_top_profiles} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Wrapper - View All Top Profiles Lists */}
                    {!totalRecords ?
                        <FlatList style={marginStyles.margin_20}
                            data={topProfiles}
                            renderItem={({ item, index }) => <TopProfilesItem item={item} isFromAllProfile={true} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    callToTopProfilesListAPI()
                                }
                            }} />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('viewAllTopProfiles') &&
                        <SponsorBannerAds screenCode={'viewAllTopProfiles'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('viewAllTopProfiles')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default ViewAllTopProfiles;