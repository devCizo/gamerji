import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, TextInput, ScrollView, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import * as generalSetting from '../../webServices/generalSetting';
import { Constants } from '../../appUtils/constants';
import CustomMarquee from '../../appUtils/customMarquee';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { CustomerCareTicketItem } from './customerCare';
import moment from 'moment';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const TicketDetail = (props) => {

    const dispatch = useDispatch()
    const [isLoading, setLoading] = useState(false)
    const [ticket, setTicket] = useState(undefined)
    const [chatMessage, setChatMessage] = useState('')
    const [conversations, setConversations] = useState([])
    const [messageFlatListRef, setMessageFlatListRef] = useState(null)

    var ticketId = props.route.params.ticketId

    var messageGetTimer = null

    useEffect(() => {

        setLoading(true);
        messageGetTimer = setInterval(() => {
            dispatch(actionCreators.requestTicketDetail(ticketId))
        }, 2000)

        setTimeout(() => {
            updateMessageAsRead()
        }, 2000);

        return () => {
            dispatch(actionCreators.resetTicketDetailState())
            clearInterval(messageGetTimer)
        }
    }, []);

    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.ticketDetail.model }),
        shallowEqual
    );
    var { ticketDetailResponse } = currentState

    // Category List Response
    useEffect(() => {
        if (ticketDetailResponse) {
            setLoading(false)

            if (ticketDetailResponse.item) {

                setTicket(ticketDetailResponse.item)

                var scrollToEnd = false

                if (ticketDetailResponse.item.conversations && ticketDetailResponse.item.conversations.length > 0) {
                    if (conversations.length < ticketDetailResponse.item.conversations.length) {
                        scrollToEnd = true
                    }
                    setConversations(ticketDetailResponse.item.conversations)
                }
                if (scrollToEnd) {
                    setTimeout(() => {
                        messageFlatListScrollToEnd(false)
                    }, 500);
                }
            }
        }
        ticketDetailResponse = undefined
    }, [ticketDetailResponse])

    const sendMessage = () => {
        if (chatMessage == '') {
            return
        }
        let message = {
            message: chatMessage,
            from: 'appUser',
            dateAndTime: new Date()
        }
        setChatMessage('')

        var tempConversations = [...conversations]
        tempConversations.push(message)
        setConversations(tempConversations)
        messageFlatListScrollToEnd(true)

        let payload = {
            conversations: tempConversations,
            status: 3
        }

        dispatch(actionCreators.requestUpdateTicket(ticketId, payload))
    }

    const updateMessageAsRead = () => {
        let payload = {
            "isLastMessageRead": true
        }
        dispatch(actionCreators.requestUpdateTicket(ticketId, payload))
    }

    const messageFlatListScrollToEnd = (animated, ref) => {
        if (messageFlatListRef) {
            messageFlatListRef.scrollToEnd({ animating: animated })
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} >

                {/* Back Button */}
                {<TouchableOpacity style={{ height: 50, width: 50, justifyContent: 'center' }} onPress={RootNavigation.goBack}>
                    <Image style={{ width: 25, height: 23, alignSelf: 'center' }} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={{ marginRight: 50, flex: 1, color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} numberOfLines={1} >{ticket && '#' + ticket.code}</Text>
            </View>

            {/* Container View */}
            <View style={{ flex: 1, backgroundColor: 'white', borderTopStartRadius: 40, borderTopEndRadius: 40, overflow: 'hidden' }}>

                {/* <ScrollView style={{ flex: 1 }}>

                        <View style={{ height: '100%' }}> */}
                {ticket &&
                    <>
                        <View style={{ marginTop: 20 }} >
                            <CustomerCareTicketItem item={ticket} />
                        </View>

                        <View style={{ flex: 1, justifyContent: 'flex-end' }} >

                            <FlatList
                                ref={(ref) => setMessageFlatListRef(ref)}
                                style={{ flex: 1 }}
                                data={conversations}
                                renderItem={({ item, index }) => <TicketDetailItem item={item} index={index} />}
                            >
                            </FlatList>

                            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} >

                                <TextInput
                                    style={{ flex: 1, paddingHorizontal: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, color: colors.black }}
                                    placeholder={'Type a message...'}
                                    placeholderTextColor={'#70717A'}
                                    onChangeText={text => setChatMessage(text)}
                                    autoCorrect={false}>{chatMessage}
                                </TextInput>

                                <TouchableOpacity style={{ marginRight: 12, height: 40, width: 40, borderRadius: 20, backgroundColor: colors.black, justifyContent: 'center', alignItems: 'center' }} onPress={() => sendMessage()} activeOpacity={0.5} >
                                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/send_message.png')} />
                                </TouchableOpacity>

                                <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 1, backgroundColor: '#DFE4E9' }} />

                            </View>
                        </View>
                        {/* </View>
                    </ScrollView> */}

                        {checkIsSponsorAdsEnabled('ticketDetails') &&
                            <SponsorBannerAds screenCode={'ticketDetails'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('ticketDetails')} />
                        }
                    </>
                }
            </View>

            {isLoading && <CustomProgressbar />}

        </SafeAreaView>
    )
}

const TicketDetailItem = (props) => {

    let item = props.item

    return (

        <View>
            {item.from == 'CRM' ?
                <>
                    {/* Admin */}
                    <View style={{ flexDirection: 'row' }} >

                        <View style={{ marginTop: 8, marginLeft: 14, borderRadius: 8, backgroundColor: '#EAECF2' }}>

                            {/* Message */}
                            <Text style={{ marginTop: 8, marginLeft: 10, marginRight: 10, marginBottom: 8, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular, maxWidth: Constants.SCREEN_WIDTH - 100 }} >{item.message}</Text>
                        </View>
                    </View>

                    {/* Date */}
                    <Text style={{ marginTop: 8, marginLeft: 14, marginBottom: 8, color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} >{moment(item.dateAndTime).format('DD MMM yyyy hh:mm A')}</Text>
                </>
                :
                <>
                    {/* Admin */}
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }} >

                        <View style={{ marginTop: 8, marginRight: 14, borderRadius: 8, backgroundColor: colors.black }}>

                            {/* Message */}
                            <Text style={{ marginTop: 8, marginLeft: 10, marginRight: 10, marginBottom: 8, color: colors.white, fontSize: 14, textAlign: 'right', fontFamily: fontFamilyStyleNew.regular, maxWidth: Constants.SCREEN_WIDTH - 100 }} >{item.message}</Text>
                        </View>
                    </View>

                    {/* Date */}
                    <Text style={{ marginTop: 8, marginRight: 14, marginBottom: 8, alignSelf: 'flex-end', color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} >{moment(item.dateAndTime).format('DD MMM yyyy hh:mm A')}</Text>
                </>
            }
        </View>
    )
}

export default TicketDetail