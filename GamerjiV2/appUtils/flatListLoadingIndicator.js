import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import colors from '../assets/colors/colors';


const FlatListLoadingIndicator = () => {
    return (
        <View style={{ backgroundColor: 'red' }}>
            {
                <ActivityIndicator size="large" color={colors.black} style={{ marginLeft: 6 }} />
                // <ActivityIndicator size="large" color={colors.black} style={{ marginLeft: 6 }} />
            }
        </View>


    )
}

export default FlatListLoadingIndicator