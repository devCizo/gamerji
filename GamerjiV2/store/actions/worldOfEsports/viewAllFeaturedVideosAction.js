import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIEW_ALL_FEATURED_VIDEO_LIST = 'VIEW_ALL_FEATURED_VIDEO_LIST';
export const VIEW_ALL_FEATURED_VIDEO_LIST_FAILED = 'VIEW_ALL_FEATURED_VIDEO_LIST_FAILED';
export const RESET_VIEW_ALL_FEATURED_VIDEO_STATE = 'RESET_VIEW_ALL_FEATURED_VIDEO_STATE';

{ /* Request - View All Featured Video List */ }
export const viewAllFeaturedVideoList = (viewModel) => {
    return {
        type: VIEW_ALL_FEATURED_VIDEO_LIST,
        model: viewModel
    };
};

{ /* Request Failure - View All Featured Video List */ }
export const viewAllFeaturedVideoListFailed = (viewModel) => {
    return {
        type: VIEW_ALL_FEATURED_VIDEO_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - View All Featured Videos List */ }
export const resetViewAllFeaturedVideosState = () => {
    return {
        type: RESET_VIEW_ALL_FEATURED_VIDEO_STATE,
    };
};

{ /* Request - View All Featured Videos */ }
export const requestViewAllFeaturedVideos = (payload) => {
    debugger
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIEW_ALL_VIDEO_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(viewAllFeaturedVideoList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(viewAllFeaturedVideoListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};