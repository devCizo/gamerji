import produce from 'immer';
import * as actionTypes from '../../actions/worldOfEsports/viewAllTopProfilesAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        viewAllTopProfileListResponse: undefined
    },
    error: false
};

{ /* Request - View All Top Profile List */ }
const viewAllTopProfileList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.viewAllTopProfileListResponse = action.model;
    });
};

{ /* Request Failure - View All Top Profile List */ }
const viewAllTopProfileListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.viewAllTopProfileListResponse = action.model;
    });
};

{ /* Reset State - View All Top Profile List */ }
const resetViewAllTopProfilesState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.viewAllTopProfileListResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIEW_ALL_TOP_PROFILE_LIST:
            return viewAllTopProfileList(state, action);
        case actionTypes.VIEW_ALL_TOP_PROFILE_LIST_FAILED:
            return viewAllTopProfileListFailed(state, action);
        case actionTypes.RESET_VIEW_ALL_TOP_PROFILE_STATE:
            return resetViewAllTopProfilesState(state);
        default:
            return state;
    }
};

export default reducer;