import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/more/howToPlayAction';

const initialState = {
    model: {
        howToPlayListResponse: undefined,
    },
    error: false
};

const howToPlayList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.howToPlayListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const howToPlayListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.howToPlayListResponse = action.model;
    });
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HOW_TO_PLAY_LIST:
            return howToPlayList(state, action);
        case actionTypes.HOW_TO_PLAY_LIST_FAILED:
            return howToPlayListFailed(state, action);
        case actionTypes.RESET_HOW_TO_PLAY_STATE:
            return resetHowToPlayState(state);
        default:
            return state;
    }
};

export default reducer;


const resetHowToPlayState = (state) => {
    return produce(state, (draft) => {
        addLog('clear')
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.howToPlayListResponse = undefined
    })
}