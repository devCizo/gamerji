const dailyLoginRewardsData = [{
        id: '1',
        title: 'Day-1',
        amount: '5',
        description: 'GamerJi Points',
        status: true
    },
    {
        id: '2',
        title: 'Day-2',
        amount: '₹10',
        description: 'Bonus',
        status: false
    },
    {
        id: '3',
        title: 'Day-3',
        amount: '₹15',
        description: 'Rewards Deposit',
        status: false
    },
    {
        id: '4',
        title: 'Day-4',
        amount: '5',
        description: 'GamerJi Points',
        status: false
    },
    {
        id: '5',
        title: 'Day-5',
        amount: '₹10',
        description: 'Bonus',
        status: false
    },
    {
        id: '6',
        title: 'Day-6',
        amount: '₹15',
        description: 'Rewards Deposit',
        status: false
    },
    {
        id: '7',
        title: 'Day-7',
        amount: '5',
        description: 'GamerJi Points',
        status: false
    },
    {
        id: '8',
        title: 'Day-8',
        amount: '₹10',
        description: 'Bonus',
        status: false
    },
    {
        id: '9',
        title: 'Day-9',
        amount: '₹15',
        description: 'Rewards Deposit',
        status: false
    },
    {
        id: '10',
        title: 'Day-10',
        amount: '5',
        description: 'GamerJi Points',
        status: false
    }
];

export default dailyLoginRewardsData;