import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import avatarData from '../assets/data/avatarData';
import * as generalSetting from '../webServices/generalSetting'
import CustomMarquee from '../appUtils/customMarquee';

const ViewAllMedalsItem = (props) => {

  const width = (Dimensions.get('window').width - 50) / 2;

  return (
    <TouchableOpacity activeOpacity={1}>

      {/* Wrapper - Border */}
      <View style={{ height: 120, width: width, borderRadius: 5, backgroundColor: '#221E32', marginRight: 10, marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>

        {/* Image - Avatar */}
        <Image style={{ height: 70, width: 70 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />

        {/* Text - Name */}
        <CustomMarquee value={props.item.name} style={[fontFamilyStyles.regular, colorStyles.whiteColor, fontSizeStyles.fontSize_14, alignmentStyles.alignSelfCenter, marginStyles.topMargin_10]} />

      </View>
    </TouchableOpacity>
  )
}

export default ViewAllMedalsItem;