import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const VIDEO_REQUESTS_LIST = 'VIDEO_REQUESTS_LIST';
export const VIDEO_REQUESTS_LIST_FAILED = 'VIDEO_REQUESTS_LIST_FAILED';
export const RESET_VIDEO_REQUESTS_LIST_STATE = 'RESET_VIDEO_REQUESTS_LIST_STATE';

{ /* Request - Video Requests List */ }
export const videoRequestsList = (viewModel) => {
    return {
        type: VIDEO_REQUESTS_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Video Requests List */ }
export const videoRequestsListFailed = (viewModel) => {
    return {
        type: VIDEO_REQUESTS_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Video Requests List */ }
export const resetVideoRequestsListState = () => {
    return {
        type: RESET_VIDEO_REQUESTS_LIST_STATE,
    };
};

{ /* Request - Video Requests */ }
export const requestVideoRequests = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.VIDEO_REQUESTS_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(videoRequestsList(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(videoRequestsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};