import produce from 'immer';
import * as actionTypes from '../../actions/tabAllGames/bottomSheetDailyLoginRewardsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        bottomSheetDailyLoginRewardsResponse: undefined,
        claimRewardResponse: undefined,
    },
    error: false
};

{ /* Request - Bottom Sheet Daily Login Rewards List */ }
const bottomSheetDailyLoginRewardsList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.bottomSheetDailyLoginRewardsResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Bottom Sheet Daily Login Rewards List */ }
const bottomSheetDailyLoginRewardsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.bottomSheetDailyLoginRewardsResponse = action.model;
    });
};


const claimReward = (state, action) => {
    return produce(state, (draft) => {
        draft.model.claimRewardResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const claimRewardFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimRewardResponse = action.model;
    });
};

{ /* Reset State - Bottom Sheet Daily Login Rewards List */ }
const resetBottomSheetDailyLoginRewardsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.bottomSheetDailyLoginRewardsResponse = undefined;
        draft.model.claimRewardResponse = undefined;
    });
};
const claimOneTimeBonus = (state, action) => {
    return produce(state, (draft) => {
        draft.model.claimOneTimeBonusResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const claimOneTimeBonusFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimOneTimeBonusResponse = action.model;
    });
};
{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST:
            return bottomSheetDailyLoginRewardsList(state, action);
        case actionTypes.BOTTOM_SHEET_DAILY_LOGIN_REWARDS_LIST_FAILED:
            return bottomSheetDailyLoginRewardsListFailed(state, action);
        case actionTypes.COLLECT_REWARD:
            return claimReward(state, action);
        case actionTypes.COLLECT_REWARD_FAILED:
            return claimRewardFailed(state, action);
        case actionTypes.COLLECT_ONETIME_BONUS:
            return claimOneTimeBonus(state, action);
        case actionTypes.COLLECT_ONETIME_BONUS_FAILED:
            return claimOneTimeBonusFailed(state, action);
        case actionTypes.RESET_BOTTOM_SHEET_DAILY_LOGIN_REWARDS_STATE:
            return resetBottomSheetDailyLoginRewardsState(state);
        default:
            return state;
    }
};

export default reducer;