import axios from 'axios';
import * as generalSetting from '../webServices/generalSetting'

const instance = axios.create({
    baseURL: generalSetting.API_URL,
});

export default instance;