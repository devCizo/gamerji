
import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView, BackHandler, Dimensions } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew, shadowStyle, otherStyles } from '../../appUtils/commonStyles'
import * as generalSetting from '../../webServices/generalSetting'
import * as RootNavigation from '../../appUtils/rootNavigation'
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis'
import moment from 'moment'
import HtmlText from 'react-native-html-to-text';
import { Constants } from '../../appUtils/constants'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds'

const EsportsNewsDetails = (props) => {

    // Variable Declarations
    let blog = props.route.params.blog

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle, otherStyles.capitalize]} numberOfLines={1} >Esports News</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>
                <ScrollView style={{ flex: 1, marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 20, borderWidth: 1, borderColor: '#DFE4E9' }} showsVerticalScrollIndicator={false} >
                    <Image style={{ marginTop: 10, marginLeft: 10, marginRight: 10, height: 120, borderRadius: 10 }} source={{ uri: generalSetting.UPLOADED_FILE_URL + blog.img.default }} />

                    <Text style={{ marginTop: 20, marginLeft: 10, marginRight: 10, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} >{blog.title}</Text>

                    <Text style={{ marginTop: 12, marginLeft: 10, marginRight: 10, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Published : {moment(blog.createdAt).format('DD MMM yyyy')}</Text>

                    <Text style={{ marginTop: 12, marginLeft: 10, marginRight: 10, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >{blog.shortDescription}</Text>

                    <HtmlText style={{ marginTop: 12, marginLeft: 10, marginRight: 10, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} html={blog.description} />
                </ScrollView>

                {checkIsSponsorAdsEnabled('esportsNewsDetails') &&
                    <SponsorBannerAds screenCode={'esportsNewsDetails'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('esportsNewsDetails')} />
                }
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around',
        overflow: 'hidden'
    },
})

export default EsportsNewsDetails;