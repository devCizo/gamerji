import produce from 'immer';
import * as actionTypes from '../../actions/more/moreAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        videoRequestsResponse: undefined,
    },
    error: false
};

{ /* Request - Video Requests List */ }
const videoRequestsList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.videoRequestsResponse = action.model;
    });
};

{ /* Request Failure - Video Requests List */ }
const videoRequestsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.videoRequestsResponse = action.model;
    });
};

{ /* Request - Video Requests List */ }
const resetVideoRequestsListState = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.videoRequestsResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIDEO_REQUESTS_LIST:
            return videoRequestsList(state, action);
        case actionTypes.VIDEO_REQUESTS_LIST_FAILED:
            return videoRequestsListFailed(state, action);
        case actionTypes.RESET_VIDEO_REQUESTS_LIST_STATE:
            return resetVideoRequestsListState(state, action);
        default:
            return state;
    }
};

export default reducer;