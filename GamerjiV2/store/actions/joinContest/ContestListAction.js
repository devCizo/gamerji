import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const contestList = (viewModel) => {
    return {
        type: CONTEST_LIST,
        model: viewModel
    };
};

export const contestListFailed = (viewModel) => {
    return {
        type: CONTEST_LIST_FAILED,
        model: viewModel
    };
};

export const walletUsageLimit = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT,
        model: viewModel
    };
};

export const walletUsageLimitFailed = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FAILED,
        model: viewModel
    };
};

export const requestContestList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.CONTEST_LIST.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(contestList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(contestListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const requestWalletUsageLimit = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.GET_WALLET_USAGE_LIMIT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GET_WALLET_USAGE_LIMIT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(walletUsageLimit(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(walletUsageLimitFailed(error.response.data));
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetContestListState = () => {
    return {
        type: RESET_CONTEST_LIST_STATE,
    };
};

export const CONTEST_LIST = 'CONTEST_LIST';
export const CONTEST_LIST_FAILED = 'CONTEST_LIST_FAILED';
export const WALLET_USAGE_LIMIT = 'WALLET_USAGE_LIMIT';
export const WALLET_USAGE_LIMIT_FAILED = 'WALLET_USAGE_LIMIT_FAILED';
export const RESET_CONTEST_LIST_STATE = 'RESET_CONTEST_LIST_STATE';