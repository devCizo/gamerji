import produce from 'immer';
import * as actionTypes from '../../actions/customerCare/raiseComplaintAction';

const initialState = {
    model: {
        complaintCategoryResponse: undefined,
        complaintSubCategoryResponse: undefined,
        gameListResponse: undefined,
        gameTypeListResponse: undefined,
        raiseComplaintResponse: undefined,
        uploadRaiseComplaintImageResponse: undefined
    },
    error: false
};

const complaintCategory = (state, action) => {
    return produce(state, (draft) => {
        draft.model.complaintCategoryResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const complaintCategoryFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.complaintCategoryResponse = action.model;
    });
};

const complaintSubCategory = (state, action) => {
    return produce(state, (draft) => {
        draft.model.complaintSubCategoryResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const complaintSubCategoryFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.complaintSubCategoryResponse = action.model;
    });
};

const gameList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.gameListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const gameListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gameListResponse = action.model;
    });
};

const gameTypeList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.gameTypeListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const gameTypeListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.gameTypeListResponse = action.model;
    });
};

const raiseComplaint = (state, action) => {
    return produce(state, (draft) => {
        draft.model.raiseComplaintResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const raiseComplaintFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.raiseComplaintResponse = action.model;
    });
};

const uploadRaiseComplaintImage = (state, action) => {
    return produce(state, (draft) => {
        draft.model.uploadRaiseComplaintImageResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const uploadRaiseComplaintImageFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.uploadRaiseComplaintImageResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COMPLAINT_CATEGORY:
            return complaintCategory(state, action);
        case actionTypes.COMPLAINT_CATEGORY_FAILED:
            return complaintCategoryFailed(state, action);
        case actionTypes.COMPLAINT_SUB_CATEGORY:
            return complaintSubCategory(state, action);
        case actionTypes.COMPLAINT_SUB_CATEGORY_FAILED:
            return complaintSubCategoryFailed(state, action);
        case actionTypes.GAME_LIST:
            return gameList(state, action);
        case actionTypes.GAME_LIST_FAILED:
            return gameListFailed(state, action);
        case actionTypes.GAME_TYPE_LIST:
            return gameTypeList(state, action);
        case actionTypes.GAME_TYPE_LIST_FAILED:
            return gameTypeListFailed(state, action);
        case actionTypes.RAISE_COMPLAINT:
            return raiseComplaint(state, action);
        case actionTypes.RAISE_COMPLAINT_FAILED:
            return raiseComplaintFailed(state, action);
        case actionTypes.UPLOAD_RAISE_COMPLAINT_IMAGE:
            return uploadRaiseComplaintImage(state, action);
        case actionTypes.UPLOAD_RAISE_COMPLAINT_IMAGE_FAILED:
            return uploadRaiseComplaintImageFailed(state, action);
        case actionTypes.RESET_RAISE_COMPLAINT_STATE:
            return resetRaiseComplaintState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetRaiseComplaintState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.complaintCategoryResponse = undefined
        draft.model.complaintSubCategoryResponse = undefined
        draft.model.gameListResponse = undefined
        draft.model.gameTypeListResponse = undefined
        draft.model.raiseComplaintResponse = undefined
        draft.model.uploadRaiseComplaintImageResponse = undefined
    })
}