import React from "react";
import { View, Image } from 'react-native'
import colors from "../assets/colors/colors";

const FlatListIndicatorItem = (props) => {

    return (
        <View style={{ marginLeft: 5 }} >
            {props.selectedIndex == props.currentIndex ?
                <Image style={{ width: 20, height: 10, flex: 1, backgroundColor: colors.black, borderRadius: 5 }} />
                :
                <Image style={{ width: 10, height: 10, flex: 1, backgroundColor: '#E5E5EE', borderRadius: 5 }} />
            }
        </View>
    )
}

export default FlatListIndicatorItem;