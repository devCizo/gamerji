import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const coinStoreList = (viewModel) => {
    return {
        type: COIN_STORE_LIST,
        model: viewModel
    };
};

export const coinStoreListFailed = (viewModel) => {
    return {
        type: COIN_STORE_LIST_FAILED,
        model: viewModel
    };
};

export const requestCoinStoreList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Token rGet:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.COIN_STORE.LIST, payload)
            .then((response) => {
                addLog("requestCoinPackList:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(coinStoreList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(coinStoreListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};
export const resetCoinStoreState = () => {
    return {
        type: RESET_COIN_STORE_STATE,
    };
};

export const COIN_STORE_LIST = 'COIN_STORE_LIST';
export const COIN_STORE_LIST_FAILED = 'COIN_STORE_LIST_FAILED';
export const RESET_COIN_STORE_STATE = 'RESET_COIN_STORE_STATE';