import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, ScrollView, TextInput, TouchableOpacity, SafeAreaView, Image, Button, FlatList, Dimensions, Share } from 'react-native'
import { containerStyles, heightStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, widthStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import colors from '../../assets/colors/colors';
import { Constants } from '../../appUtils/constants';
import * as actionCreators from '../../store/actions/index';
import MedalItem from '../../componentsItem/MedalItem';
import StatsItem from '../../componentsItem/StatsItem';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog } from '../../appUtils/commonUtlis';
import * as generalSetting from '../../webServices/generalSetting';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';

const Profile = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [stats, setStats] = useState([]);
    const [activeTab, setActiveTab] = useState(Constants.header_stats);
    const [isLoading, setLoading] = useState(true);
    const [profile, setProfile] = useState('');
    const [hideShare, setHideShare] = useState(false);
    const [totalStatRecords, setTotalStatRecords] = useState(undefined);
    const [totalCollegiateRecords, setTotalCollegiateRecords] = useState(undefined);
    const [gamesIndex, setGamesIndex] = useState(0);

    // This Use-Effect function should call the Accounts API
    useEffect(() => {
        setAccountsData();

        return () => {
            dispatch(actionCreators.resetProfileState())
        }
    }, []);

    // This function calls the API when coming to this screen for the first time
    // Also calls when coming from the Edit Profile Screen
    function setAccountsData() {
        setLoading(true);
        dispatch(actionCreators.requestProfile({}))
    };

    // This function should all the Stats API as per the Games
    const requestGameStats = (gameId) => {
        let payload = {
            game: gameId
        }
        dispatch(actionCreators.requestStats(payload))
    }

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.profile.model }),
        shallowEqual
    );
    var { profileResponse, statsListResponse } = currentState

    //addLog("profileResponse==>", profileResponse);

    // This function should return the Get Profile API Response
    useEffect(() => {
        setLoading(false)
        if (profileResponse) {
            setProfile(profileResponse)
            setTotalStatRecords(profileResponse.gameNames.length);
            if (profileResponse.gameNames.length != 0) {
                requestGameStats(profileResponse.gameNames && profileResponse.gameNames[gamesIndex] && profileResponse.gameNames[gamesIndex].game)
            }
        }
        profileResponse = undefined
    }, [profileResponse])

    // This function should return the Stats API Response
    useEffect(() => {
        setLoading(false)
        if (statsListResponse && statsListResponse.item) {
            if (profile.gameNames.length > gamesIndex) {

                var tempObj = { ...statsListResponse.item }
                tempObj['uniqueIGN'] = profile.gameNames[gamesIndex].uniqueIGN

                var tempArr = [...stats]
                tempArr.push(tempObj)

                setStats(tempArr);

                setGamesIndex(gamesIndex + 1)
                requestGameStats(profile.gameNames && profile.gameNames[gamesIndex] && profile.gameNames[gamesIndex].game && profile.gameNames[gamesIndex].game)
            }
        }
        statsListResponse = undefined
    }, [statsListResponse])

    // This function will alow user to share their profile with
    const clickToShareProfile = () => {
        Share.share({
            message: 'BAM: we\'re helping your business with awesome React Native apps',
            url: 'http://bam.tech',
            title: 'Wow, did you see that?'
        }, {
            // Android only:
            dialogTitle: 'Share BAM goodness',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Body Content */}
                <View style={{ flex: 1, backgroundColor: '#0C123C', borderRadius: 40, marginBottom: 10 }}>

                    {/* Image - Profile Pic */}
                    {!profile.userBanner || profile.userBanner == null ?
                        <Image style={{ height: 194, width: (Dimensions.get('window').width), resizeModeStyles: 'cover' }} source={require('../../assets/images/profile_bg.png')} />
                        :
                        <Image style={{ height: 194, width: (Dimensions.get('window').width), resizeModeStyles: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.userBanner.img.default }} />
                    }

                    <View style={[positionStyles.absolute, flexDirectionStyles.row, heightStyles.height_50, { left: 0, right: 0 }]}>

                        {/* Text - Profile Header */}
                        <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_profile} </Text>
                    </View>

                    {/* Wrapper - User Profile Pic Main */}
                    <View style={{ flexDirection: 'column', justifyContent: 'center' }}>

                        {/* Wrapper - User Profile Pic */}
                        <View style={{ width: '100%', flexDirection: 'row' }}>

                            {/* Wrapper - Share */}
                            <View style={{ width: '33%', alignSelf: 'center', justifyContent: 'center', left: 12 }}>

                                {hideShare &&

                                    // TouchableOpacity - Share Button Click Event
                                    <TouchableOpacity style={{ width: '33%', flexDirection: 'row', justifyContent: 'flex-start', bottom: 25, alignSelf: 'center' }}
                                        onPress={() => clickToShareProfile()} activeOpacity={0.5} >

                                        {/* Text - Share */}
                                        <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_10, marginStyles.rightMargin_10, alignmentStyles.alignSelfCenter]}>{Constants.text_share}</Text>

                                        {/* Wrapper - Share Image */}
                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} activeOpacity={1} >

                                            {/* Image - Red Circle Background */}
                                            <Image style={{ height: 30, width: 30, borderRadius: 5, backgroundColor: colors.red, borderRadius: 50, borderWidth: 2, borderColor: colors.white }} source={require('../../assets/images/profile_red_circle_bg.png')} />

                                            {/* Image - Share */}
                                            <Image style={{ position: 'absolute', height: 14, width: 14 }} source={require('../../assets/images/ic_share_white.png')} />
                                        </View>
                                    </TouchableOpacity>
                                }
                            </View>

                            {/* Wrapper - Top View */}
                            <View style={{ width: '34%', flexDirection: 'column', justifyContent: 'center', marginTop: -55, left: 0, right: 0 }}>

                                {/* <Image style={{ height: 102, width: 102, alignSelf: 'stretch', alignSelf: 'center' }} source={require('../../assets/images/profile_frame_bg.png')} /> */}

                                {/* Image - Frame */}
                                <Image style={{ height: 98, width: 98, alignSelf: 'center' }} source={require('../../assets/images/profile_frame_bg1.png')} />

                                {/* Image - Yellow Background */}
                                <Image style={{ position: 'absolute', height: 80, width: 80, alignSelf: 'center' }} source={require('../../assets/images/profile_frame_bg2.png')} />

                                {/* Image - Profile Pic */}
                                {!profile.avatar || profile.avatar == null ?
                                    <Image style={{ position: 'absolute', height: 76, width: 76, borderRadius: 5, alignSelf: 'center', resizeMode: 'cover' }} source={require('../../assets/images/profile_frame_bg3.png')} />
                                    :
                                    <Image style={{ position: 'absolute', height: 76, width: 76, borderRadius: 5, alignSelf: 'center', resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + profile.avatar.img.default }} />
                                }
                            </View>

                            {/* TouchableOpacity - Edit Button Click Event*/}
                            <TouchableOpacity style={{ width: '33%', flexDirection: 'row', justifyContent: 'flex-start', bottom: 25, alignSelf: 'center' }}
                                onPress={() => props.navigation.navigate(Constants.nav_edit_profile,
                                    { setAccountsData: setAccountsData })} activeOpacity={0.5}>

                                {/* ErapprWrapper - Edit Image */}
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} activeOpacity={1} >

                                    {/* Image - Red Circle Background */}
                                    <Image style={{ height: 30, width: 30, borderRadius: 5, backgroundColor: colors.red, borderRadius: 50, borderWidth: 2, borderColor: colors.white }} source={require('../../assets/images/profile_red_circle_bg.png')} />

                                    {/* Image - Edit */}
                                    <Image style={{ position: 'absolute', height: 14, width: 14 }} source={require('../../assets/images/ic_edit_white.png')} />
                                </View>

                                {/* Text - Edit */}
                                <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_10, marginStyles.leftMargin_10, alignmentStyles.alignSelfCenter]}>{Constants.text_edit}</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Text - Full Name */}
                        <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_16, marginStyles.topMargin_15]}> {profile.name} </Text>
                    </View>

                    {isLoading ? <CustomProgressbar /> : (
                        <ScrollView>

                            {/* Wrapper - Medal */}
                            <View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 20 }}
                                >
                                    {/* Text - Medal */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_16, marginStyles.topMargin_15]}> {Constants.text_medal} </Text>

                                    {/* TouchableOpacity - View All Button Click Event */}
                                    {/* {global.profile.medals > 0 && */}
                                    <TouchableOpacity style={{ flexDirection: 'column' }} activeOpacity={1}
                                        onPress={() => props.navigation.navigate(Constants.nav_view_all_medals)} activeOpacity={0.5}>

                                        {/* Text - View All */}
                                        <Text style={[fontFamilyStyles.bold, colorStyles.yellowColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontSizeStyles.fontSize_14, marginStyles.topMargin_15]}> {Constants.text_view_all} </Text>

                                        {/* Text - View All Separator */}
                                        <Text style={[heightStyles.height_1, widthStyles.width_50, colorStyles.yellowBackgroundColor, alignmentStyles.alignSelfCenter]} />
                                    </TouchableOpacity>
                                    {/* } */}
                                </View>

                                {/* Flatlist - Medal Lists */}
                                {global.profile.medals > 0 ?
                                    <FlatList
                                        contentContainerStyle={{ marginLeft: 25, marginRight: 25 }}
                                        data={global.profile.medals}
                                        renderItem={({ item, index }) => <MedalItem item={item} index={index} />}
                                        keyExtractor={(item) => item.id}
                                        horizontal={true}
                                        showsVerticalScrollIndicator={false}
                                        showsHorizontalScrollIndicator={false} />
                                    :
                                    <View style={{ justifyContent: 'center', alignSelf: 'center' }}>

                                        {/* Text - No Records Found */}
                                        <Text style={{ color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>
                                    </View>
                                }

                                {/* Tabs View */}
                                <View style={{ marginTop: 15, marginLeft: 20, marginRight: 20, marginTop: 30, height: 35, flexDirection: 'row', overflow: 'hidden', borderRadius: 5, borderWidth: 0.5, borderColor: colors.grey }} >

                                    {/* TouchableOpacity - Stats Button Click Event */}
                                    <TouchableOpacity style={{ width: '50%', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === Constants.header_stats ? colors.yellow : 'transparent' }} onPress={() => setActiveTab(Constants.header_stats)} activeOpacity={0.5}>

                                        {/* Text - Stats */}
                                        <Text style={{ color: activeTab === Constants.header_stats ? colors.black : colors.white, fontSize: 14, fontFamily: activeTab === Constants.header_stats ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >{Constants.header_stats}</Text>
                                    </TouchableOpacity>

                                    {/* TouchableOpacity - Collegiate Button Click Event */}
                                    <TouchableOpacity style={{ width: '50%', borderTopLeftRadius: 5, borderBottomRightRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: activeTab === Constants.header_collegiate ? colors.yellow : 'transparent' }} onPress={() => setActiveTab(Constants.header_collegiate)} activeOpacity={0.5}>

                                        {/* Text - Collegiate */}
                                        <Text style={{ color: activeTab === Constants.header_collegiate ? colors.black : colors.white, fontSize: 14, fontFamily: activeTab === Constants.header_collegiate ? fontFamilyStyleNew.bold : fontFamilyStyleNew.regular }} >{Constants.header_collegiate}</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ marginTop: 36, marginLeft: 20, marginRight: 20 }}>

                                    {/* Wrapper - Stats List */}
                                    {activeTab === Constants.header_stats && totalStatRecords > 0 ?

                                        // Flatlist - Stats Lists
                                        <FlatList
                                            data={stats}
                                            renderItem={({ item, index }) => <StatsItem item={item} index={index} />}
                                            keyExtractor={(item) => item.id}
                                            numColumns={2}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={false} />
                                        :
                                        activeTab === Constants.header_stats &&
                                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>

                                            {/* Text - No Records Found */}
                                            <Text style={{ color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }} >{Constants.text_no_records}</Text>
                                        </View>
                                    }

                                    {activeTab === Constants.header_collegiate && totalCollegiateRecords > 0 ?
                                        <View>
                                            <Text style={{ color: colors.white }}>Hello</Text>
                                        </View>
                                        :
                                        // Wrapper - Collegiate 
                                        activeTab === Constants.header_collegiate &&
                                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>

                                            {/* Text - No Records Found */}
                                            <Text style={{ color: colors.white, fontSize: 20, fontFamily: fontFamilyStyleNew.regular, alignSelf: 'center' }}>{Constants.text_no_records}</Text>
                                        </View>
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    )}
                </View>
            </View>
        </SafeAreaView >
    )
}

export default Profile;
