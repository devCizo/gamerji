import React from 'react'

const frameData = [
    {
        id: '1',
        image: require('../../assets/images/frame_dummy1.png'),
    },
    {
        id: '2',
        image: require('../../assets/images/frame_dummy2.png'),
    },
    {
        id: '3',
        image: require('../../assets/images/frame_dummy1.png'),
    },
    {
        id: '4',
        image: require('../../assets/images/frame_dummy2.png'),
    }
];

export default frameData;