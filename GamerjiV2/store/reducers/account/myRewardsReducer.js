import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/account/myRewardsAction';

const initialState = {
    model: {
        myRewardListResponse: undefined,
    },
    error: false
};

const getMyRewardList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.myRewardListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getMyRewardListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.myRewardListResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.MY_REWARD_LIST:
            return getMyRewardList(state, action);
        case actionTypes.MY_REWARD_LIST_FAILED:
            return getMyRewardListFailed(state, action);
        case actionTypes.RESET_MY_REWARDS_STATE:
            return resetMyRewardsState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetMyRewardsState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.myRewardListResponse = undefined;
    })
}