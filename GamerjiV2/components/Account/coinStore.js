import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, Dimensions } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, shadowStyle } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const CoinStore = (props) => {

    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [packageList, setPackageList] = useState([])
    const [bestOfferPackage, setBestOfferPackage] = useState(undefined)
    const [popularPackage, setPopularPackage] = useState(undefined)
    const [trendingPackage, setTrendingPackage] = useState(undefined)

    //Fetch list
    useEffect(() => {

        setLoading(true)
        dispatch(actionCreators.requestCoinPackList({}))

        return () => {
            dispatch(actionCreators.resetCoinStoreState())
        }
    }, [])

    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.coinStore.model }),
        shallowEqual
    );
    var { coinStoreListResponse } = currentState

    useEffect(() => {
        addLog('coinStoreListResponse', coinStoreListResponse)
        if (coinStoreListResponse) {
            setLoading(false)
            if (coinStoreListResponse.list) {

                var tempPackages = []
                for (let i = 0; i < coinStoreListResponse.list.length; i++) {
                    if (coinStoreListResponse.list[i].coinType && coinStoreListResponse.list[i].coinType == '1') {
                        tempPackages.push(coinStoreListResponse.list[i])
                    } else if (coinStoreListResponse.list[i].coinType && coinStoreListResponse.list[i].coinType == '2') {
                        setBestOfferPackage(coinStoreListResponse.list[i]) // Best Offer
                    } else if (coinStoreListResponse.list[i].coinType && coinStoreListResponse.list[i].coinType == '3') {
                        setPopularPackage(coinStoreListResponse.list[i]) //Popular
                    } else if (coinStoreListResponse.list[i].coinType && coinStoreListResponse.list[i].coinType == '4') {
                        setTrendingPackage(coinStoreListResponse.list[i]) //Trending
                    }
                }
                setPackageList(tempPackages)
            }
        }
        coinStoreListResponse = undefined
    }, [coinStoreListResponse])


    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const purchasePopularPackage = () => {
        if (popularPackage) {
            moveToPaymentOptionScreen(popularPackage)
        }
    }

    const purchaseBestOfferPackage = () => {
        if (bestOfferPackage) {
            moveToPaymentOptionScreen(bestOfferPackage)
        }
    }

    const purchaseTrendingPackage = () => {
        if (trendingPackage) {
            moveToPaymentOptionScreen(trendingPackage)
        }
    }

    const purchaseItemFromList = (item) => {
        moveToPaymentOptionScreen(item)
    }

    const moveToPaymentOptionScreen = (item) => {

        var params = {
            amount: item.amount.toString(),
            paymentTypeForCreateTransaction: 17,
            coinStore: {
                coinStoreId: item._id,
                currencyId: item.currency && item.currency._id
            }
        }
        RootNavigation.navigate(Constants.nav_payment_options_new, params);
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Navigation Title */}
                <Text style={[fontFamilyStyleNew.extraBold, styles.navigationTitle]} numberOfLines={1} >COIN STORE</Text>

                {/* Available Coins */}
                {<View style={{ position: 'absolute', top: 10, right: 20, height: 28, borderRadius: 14, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: '#182F48' }}>
                    <Image style={{ marginLeft: 10, width: 17, height: 17, resizeMode: 'contain' }} source={require('../../assets/images/coins_account.png')} />
                    <Text style={{ marginLeft: 3, marginRight: 10, color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} >{global.profile.wallet && global.profile.wallet.coinAmount ? global.profile.wallet.coinAmount : '0'}</Text>
                </View>}
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {/* Offers data */}
                {packageList.length != 0 &&
                    <View style={{ marginTop: 20, marginLeft: 10, marginRight: 10, flexDirection: 'row' }} >

                        {/* Popular */}
                        {popularPackage &&
                            <View style={{ width: '33.33%' }} >

                                <TouchableOpacity style={{ width: '95%', flex: 1, alignItems: 'center' }} onPress={() => purchasePopularPackage()} activeOpacity={0.6}>

                                    {/* Top Content */}
                                    <View style={{ marginTop: 28, height: 24, width: '85%', borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: '#4069C7', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Popular</Text>
                                    </View>

                                    {/* Center content */}
                                    <View style={{ height: 90, width: '85%', backgroundColor: '#43F2FF', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ marginTop: 5, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{popularPackage.coins}</Text>
                                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >Coins</Text>
                                        <Image style={{ marginTop: 4, height: 40, width: 40, resizeMode: 'contain' }} source={require('../../assets/images/popular_coin_store.png')} />
                                    </View>

                                    {/* Bottom contest */}
                                    <View style={{ height: 30, width: '100%', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, backgroundColor: '#24D0DC', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Buy at ₹{popularPackage.amount}</Text>
                                    </View>

                                    <Image style={{ position: 'absolute', top: 137, left: 0, height: 5, width: '7.5%', resizeMode: 'stretch' }} source={require('../../assets/images/left_cross_coin_store.png')} />

                                    <Image style={{ position: 'absolute', top: 137, right: 0, height: 5, width: '7.5%', resizeMode: 'stretch' }} source={require('../../assets/images/right_cross_coin_store.png')} />
                                </TouchableOpacity>
                            </View>
                        }

                        {/* Best Offer */}
                        {bestOfferPackage &&
                            <View style={{ width: '33.33%', alignItems: 'center' }} >

                                <TouchableOpacity style={{ width: '95%', flex: 1, alignItems: 'center' }} onPress={() => purchaseBestOfferPackage()} activeOpacity={0.6}>

                                    {/* Top Content */}
                                    <View style={{ marginTop: 0, height: 24, width: '85%', borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: '#FF412C', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Best Offer</Text>
                                    </View>

                                    {/* Center content */}
                                    <View style={{ height: 118, width: '85%', backgroundColor: '#FFE95D', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ marginTop: 4, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.extraBold }} numberOfLines={1} >{bestOfferPackage.coins}</Text>
                                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >Coins</Text>
                                        <Image style={{ marginTop: 8, height: 68, width: 61, resizeMode: 'contain' }} source={require('../../assets/images/best_offer_coin_store.png')} />
                                    </View>

                                    {/* Bottom contest */}
                                    <View style={{ height: 30, width: '100%', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, backgroundColor: '#F8DD36', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Buy at ₹{bestOfferPackage.amount}</Text>
                                    </View>

                                    <Image style={{ position: 'absolute', top: 137, left: 0, height: 5, width: '7.5%', resizeMode: 'stretch', tintColor: '#D9BF1C' }} source={require('../../assets/images/left_cross_coin_store.png')} />

                                    <Image style={{ position: 'absolute', top: 137, right: 0, height: 5, width: '7.5%', resizeMode: 'stretch', tintColor: '#D9BF1C' }} source={require('../../assets/images/right_cross_coin_store.png')} />
                                </TouchableOpacity>
                            </View>
                        }

                        {/* Trending */}
                        {trendingPackage &&
                            <View style={{ width: '33.33%', flexDirection: 'row', justifyContent: 'flex-end' }} >

                                <TouchableOpacity style={{ width: '95%', alignItems: 'center' }} onPress={() => purchaseTrendingPackage()} activeOpacity={0.6}>

                                    {/* Top Content */}
                                    <View style={{ marginTop: 28, height: 24, width: '85%', borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: '#FE8308', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.white, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Trending</Text>
                                    </View>

                                    {/* Center content */}
                                    <View style={{ height: 90, width: '85%', backgroundColor: '#08FEC3', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ marginTop: 5, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.extraBold }} numberOfLines={1} >{trendingPackage.coins}</Text>
                                        <Text style={{ color: colors.black, fontSize: 12, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >Coins</Text>
                                        <Image style={{ marginTop: 5, height: 40, width: 40, resizeMode: 'contain' }} source={require('../../assets/images/trending_coin_store.png')} />
                                    </View>

                                    {/* Bottom contest */}
                                    <View style={{ height: 30, width: '100%', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, backgroundColor: '#06E2AD', alignItems: 'center', justifyContent: 'center' }} >
                                        <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >Buy at ₹{trendingPackage.amount}</Text>
                                    </View>

                                    <Image style={{ position: 'absolute', top: 137, left: 0, height: 5, width: '7.5%', resizeMode: 'stretch', tintColor: '#03BA8E' }} source={require('../../assets/images/left_cross_coin_store.png')} />

                                    <Image style={{ position: 'absolute', top: 137, right: 0, height: 5, width: '7.5%', resizeMode: 'stretch', tintColor: '#03BA8E' }} source={require('../../assets/images/right_cross_coin_store.png')} />
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                }

                {packageList.length != 0 &&
                    <FlatList style={{ marginTop: 15, flex: 1, marginLeft: 10, marginRight: 10 }}
                        data={packageList}
                        renderItem={({ item, index }) => <CoinStoreItem item={item} index={index} purchaseItemFromList={purchaseItemFromList} />}
                        showsVerticalScrollIndicator={false}
                        numColumns={1}
                    />
                }

                {checkIsSponsorAdsEnabled('coinStore') &&
                    <SponsorBannerAds screenCode={'coinStore'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('coinStore')} />
                }
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const CoinStoreItem = (props) => {

    let item = props.item

    const buyClicked = () => {
        props.purchaseItemFromList(item)
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={[{ marginTop: 5, marginLeft: 2.5, marginRight: 2.5, marginBottom: 5, height: 44, borderRadius: 22, backgroundColor: colors.yellow, borderColor: colors.color_input_text_border, borderWidth: 0.5, alignItems: 'center', flexDirection: 'row' }, shadowStyle.shadow]}>

                {/* Coin Icon */}
                <View style={{ height: 39, width: 39, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF4CD', borderRadius: 19.5, marginLeft: 3 }}  >
                    <Image style={{ height: 30, width: 23, resizeMode: 'contain' }} source={require('../../assets/images/coin_ic_coin_list.png')} />
                </View>

                {/* Coin Count */}
                <Text style={{ marginLeft: 8, color: colors.black, fontSize: 17, fontFamily: fontFamilyStyleNew.bold }} numberOfLines={1} >{item.coins}</Text>
                <Text style={{ marginLeft: 2, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} numberOfLines={1} >Coins</Text>

                {/* Price */}
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <View style={{ width: 200, height: 44, borderRadius: 22, alignItems: 'center', flexDirection: 'row', backgroundColor: colors.black, justifyContent: 'space-between' }}>

                        <View style={{ flexDirection: 'row' }} >
                            <Image style={{ marginLeft: 20, width: 16, height: 16, resizeMode: 'contain' }} source={require('../../assets/images/cart_coin_list.png')} />
                            <Text style={{ marginLeft: 6, color: colors.white, fontSize: 17, fontFamily: fontFamilyStyleNew.bold }} >{item.currency && item.currency.symbol}{item.amount}</Text>
                        </View>
                        <TouchableOpacity style={{ width: 100, height: 44, borderRadius: 22, backgroundColor: colors.red, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.8} onPress={() => buyClicked()} >
                            <Text style={{ color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.bold }} >BUY</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        </View>
    )
}

export default CoinStore;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 50,
        flex: 1,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})