import produce from 'immer';
import * as actionTypes from '../../actions/myContests/reportIssueAction';

const initialState = {
    model: {
        reportIssueResponse: undefined
    },
    error: false
};

const reportIssue = (state, action) => {
    return produce(state, (draft) => {
        draft.model.reportIssueResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const reportIssueFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.reportIssueResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REPORT_ISSUE:
            return reportIssue(state, action);
        case actionTypes.REPORT_ISSUE_FAILED:
            return reportIssueFailed(state, action);
        case actionTypes.RESET_REPORT_ISSUE_STATE:
            return resetReportIssueState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetReportIssueState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.reportIssueResponse = undefined;
    })
}