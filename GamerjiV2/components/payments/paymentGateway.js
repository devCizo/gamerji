import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, TouchableOpacity, SafeAreaView, Dimensions, BackHandler } from 'react-native'
import { containerStyles, positionStyles, heightStyles, widthStyles, flexDirectionStyles, justifyContentStyles, alignmentStyles, colorStyles, marginStyles, paddingStyles, borderStyles, imageStyles, fontFamilyStyles, fontSizeStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { addLog, getPaymentTimeStamp } from '../../appUtils/commonUtlis';
import WebFields from '../../webServices/webFields.json';
import * as actionCreators from '../../store/actions/index';
import { AppConstants, cashFreeAPIMode } from '../../appUtils/appConstants';
import RNPgReactNativeSdk from 'react-native-pg-react-native-sdk';
import { Constants } from '../../appUtils/constants';
import * as generalSetting from '../../webServices/generalSetting';
import colors from '../../assets/colors/colors';
import moment from 'moment';
import CustomProgressbar from '../../appUtils/customProgressBar';

const PaymentGateway = (props) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [uniqueOrderId, setUniqueOrderId] = useState('');
    const [transactionId, setTransactionId] = useState('');
    const [response, setResponse] = useState('');
    const [isLoading, setLoading] = useState(false);
    const data = props.route.params;
    const height = (Dimensions.get('window').height);
    const coinStore = data.coinStore
    const avatarId = data.avatarId

    // This Use-Effect function should call the Generate Payment Token API
    useEffect(() => {

        const uniqueId = getPaymentTimeStamp();
        setUniqueOrderId(uniqueId);

        if (uniqueId != "") {
            let payload = {
                orderId: uniqueId,
                orderAmount: data.amount,
                orderCurrency: AppConstants.CURRENCY_INR
            }
            setLoading(true)
            dispatch(actionCreators.requestGeneratePaymentToken(payload))
        }

        return () => {
            dispatch(actionCreators.resetPaymentGatewayState())
        }
    }, [])

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.paymentGateway.model }),
        shallowEqual
    );
    var { generateTokenResponse, createTransactionsResponse, checkTransactionsResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (generateTokenResponse) {
            setLoading(false)

            addLog('generateTokenResponse', data)

            var payload = {
                user: global.profile._id,
                transactionUniqueID: uniqueOrderId,
                amount: data.amount,
                paymentType: data.paymentTypeForCreateTransaction
            }
            if (coinStore) {
                payload['coinStore'] = coinStore.coinStoreId
                payload['currency'] = coinStore.currencyId
            }
            if (avatarId) {
                payload['avatar'] = avatarId
            }

            addLog('payload', payload)

            setLoading(true)
            dispatch(actionCreators.requestCreateTransactions(payload))
        }
        generateTokenResponse = undefined
    }, [generateTokenResponse])

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (createTransactionsResponse) {
            setLoading(false)
            setTransactionId(createTransactionsResponse._id);
            addLog("Main TRNS ID:::> " + createTransactionsResponse._id);

            var params = {
                "paymentOption": data.paymentType,
                "orderId": uniqueOrderId,
                "orderAmount": data.amount,
                "appId": AppConstants.CASHFREE_CLIENT_ID,
                "tokenData": generateTokenResponse.cftoken,
                "orderCurrency": AppConstants.CURRENCY_INR,
                "orderNote": Constants.text_recharge,
                "notifyUrl": generalSetting.API_URL + WebFields.VERIFY_PAYMENT.MODE,
                "customerName": global.profile && global.profile.name,
                "customerPhone": global.profile && global.profile.phone,
                "customerEmail": global.profile && global.profile.email
            }

            if (data.paymentType === AppConstants.PAYMENT_TYPE_NET_BANKING || data.paymentType === AppConstants.PAYMENT_TYPE_WALLET) {
                params.paymentCode = data.bankId;
            } else if (data.paymentType === AppConstants.PAYMENT_TYPE_UPI) {
                params.upi_vpa = data.upiAddress;
            } else {
                params.card_number = data.cardNumber;
                params.card_expiryMonth = data.cardExpiryMonth;
                params.card_expiryYear = data.cardExpiryYear;
                params.card_cvv = data.cardCVV;
                params.card_holder = data.cardHolderName;
                params.card_save = data.card_save
            }
            addLog("Request:::> ", params);

            RNPgReactNativeSdk.startPaymentWEB(params, cashFreeAPIMode(), (result) => {
                addLog("Response:::> ", result);
                const res = JSON.parse(result);
                setResponse(res);

                let payload = {
                    user: global.profile._id,
                    transactionUniqueID: uniqueOrderId,
                    amount: data.amount,
                    paymentType: data.paymentType
                }

                addLog("INNER TRANSACTION ID:::> ", createTransactionsResponse && createTransactionsResponse._id);
                if (res.txStatus === 'SUCCESS') {
                    addLog("------------------------", "");
                    addLog("11111", "SUCCESS");
                    setLoading(true)
                    dispatch(actionCreators.requestCheckTransactions(payload, createTransactionsResponse && createTransactionsResponse._id));
                } else if (res.txStatus === 'PENDING') {
                    addLog("11111", "PENDING");
                    dispatch(actionCreators.requestCheckTransactions(payload, createTransactionsResponse && createTransactionsResponse._id));
                }
                setLoading(false);
            });
        }
        createTransactionsResponse = undefined
    }, [createTransactionsResponse])

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (checkTransactionsResponse) {
            setLoading(false)

            // if (checkTransactionsResponse.item) {
            //     setResponse(res);
            // }

            if (checkTransactionsResponse.paymentStatus == '' || checkTransactionsResponse.paymentStatus == null) {
                addLog("44444", "KEY NOT FOUND");
                setTimeout(() => {
                    let payload = {
                        user: global.profile._id,
                        transactionUniqueID: uniqueOrderId,
                        amount: data.amount,
                        paymentType: data.paymentType
                    }
                    dispatch(actionCreators.requestCheckTransactions(payload, createTransactionsResponse && createTransactionsResponse._id));
                }, 5000);
            } else {
                addLog("44444", "KEY FOUND");
            }
        }
        checkTransactionsResponse = undefined
    }, [checkTransactionsResponse])

    // useEffect(() => {
    //     if (props.isSucess && props.apicall === 'generatePaymentToken') {
    //         let payload = {
    //             user: global.profile._id,
    //             transactionUniqueID: uniqueOrderId,
    //             amount: data.amount,
    //             paymentType: 5
    //         }
    //         props.requestCreateTransactions(payload);
    //     }

    //     let payload;
    //     if (props.isSucess && props.apicall === 'createTransactions') {
    //         if (props.list.item) {
    //             addLog("Main TRNS ID:::> " + props.list.item._id);
    //             setTransactionId(props.list.item._id);
    //         }

    //         var params = {
    //             "paymentOption": data.paymentType,
    //             "orderId": uniqueOrderId,
    //             "orderAmount": data.amount,
    //             "appId": AppConstants.CASHFREE_CLIENT_ID,
    //             "tokenData": props.list && props.list.paymentToken && props.list.paymentToken.cftoken,
    //             "orderCurrency": AppConstants.CURRENCY_INR,
    //             "orderNote": Constants.text_recharge,
    //             "notifyUrl": generalSetting.API_URL + WebFields.VERIFY_PAYMENT.MODE,
    //             "customerName": global.profile && global.profile.name,
    //             "customerPhone": global.profile && global.profile.phone,
    //             "customerEmail": global.profile && global.profile.email
    //         }

    //         if (data.paymentType === AppConstants.PAYMENT_TYPE_NET_BANKING || data.paymentType === AppConstants.PAYMENT_TYPE_WALLET) {
    //             params.paymentCode = data.bankId;
    //         } else if (data.paymentType === AppConstants.PAYMENT_TYPE_UPI) {
    //             params.upi_vpa = data.upiAddress;
    //         } else {
    //             params.card_number = data.cardNumber;
    //             params.card_expiryMonth = data.cardExpiryMonth;
    //             params.card_expiryYear = 20 + data.cardExpiryYear;
    //             params.card_cvv = data.cardCVV;
    //             params.card_holder = data.cardHolderName;
    //         }
    //         addLog("Request:::> ", params);

    //         RNPgReactNativeSdk.startPaymentWEB(params, cashFreeAPIMode(), (result) => {
    //             addLog("Response:::> ", result);
    //             const res = JSON.parse(result);
    //             setResponse(res);

    //             payload = {
    //                 user: global.profile._id,
    //                 transactionUniqueID: uniqueOrderId,
    //                 amount: data.amount,
    //                 paymentType: data.paymentType
    //             }

    //             addLog("INNER TRANSACTION ID:::> ", props.list.item._id);
    //             if (res.txStatus === 'SUCCESS') {
    //                 addLog("------------------------", "");
    //                 addLog("11111", "SUCCESS");
    //                 props.requestCheckTransactions(payload, props.list.item._id);
    //             } else if (res.txStatus === 'PENDING') {
    //                 addLog("11111", "PENDING");
    //                 props.requestCheckTransactions(payload, props.list.item._id);
    //             }
    //             setLoading(false);
    //         });
    //     }

    //     if (props.isSucess && props.apicall === 'checkTransactions') {
    //         addLog("22222", "checkTransactions");
    //         addLog("::::::::::::::::PROPS::::::::::::::::", props);
    //         if (props.list.item) {
    //             addLog("33333", "ITEM");
    //             if (props.list.item.paymentStatus == '' || props.list.item.paymentStatus == null) {
    //                 addLog("44444", "KEY NOT FOUND");
    //                 setTimeout(() => {
    //                     props.requestCheckTransactions(payload, props.list.item._id);
    //                 }, 5000);
    //             } else {
    //                 addLog("44444", "KEY FOUND");
    //             }
    //         }
    //     }
    // }, [props]);

    // This function should be able to define colors as per their status.
    const values = {
        SUCCESS: colors.green,
        PENDING: colors.yellow,
        FAILED: colors.red,
        CANCELLED: colors.grey
    };

    const navigateUserAfterSuccess = () => {
        if (global.paymentSuccessCompletion) {
            global.paymentSuccessCompletion()
        }

        global.paymentSuccessCompletion = undefined

        if (global.addBalanceScreenType == Constants.contest_coin_payment_type || global.addBalanceScreenType == Constants.contest_money_payment_type) {
            //props.navigation.navigate(Constants.nav_contest_list)
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.tournament_coin_payment_type || global.addBalanceScreenType == Constants.tournament_money_payment_type) {
            //props.navigation.navigate(Constants.nav_tournament_list)
            props.navigation.pop(3)
        } else if (global.addBalanceScreenType == Constants.all_games_add_money) {
            props.navigation.navigate(Constants.nav_bottom_navigation)
            // props.navigation.pop(4)
        } else {
            //props.navigation.pop(4)
            props.navigation.navigate(Constants.nav_account_new)
            // props.navigation.popToTop()
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        navigateUserAfterSuccess();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.greyBackgroundColor]}>

            {/* {response.txStatus === 'SUCCESS' || response.txStatus === 'PENDING' || response.txStatus === 'FAILED' ? ( */}

            {isLoading ? <CustomProgressbar /> : (
                <View style={[containerStyles.container, colorStyles.greyBackgroundColor]}>

                    <View style={[heightStyles.height_210, alignmentStyles.alignItemsCenter, justifyContentStyles.center, borderStyles.borderTopLeftRadius_5, borderStyles.borderTopRightRadius_5, response.txStatus === 'PENDING' ? { backgroundColor: colors.yellow } : { backgroundColor: values[response.txStatus] }]}>

                        {/* Text - Status */}
                        <Text style={[fontFamilyStyles.semiBold, fontSizeStyles.fontSize_08, paddingStyles.padding_03, marginStyles.leftMargin_02, colorStyles.whiteColor, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{response.txStatus}</Text>
                    </View>

                    {/* Wrapper - Transaction Details */}
                    <View style={[colorStyles.whiteBackgroundColor, { height: height - 210, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 15 }]}>

                        {/* Wrapper - Amount */}
                        <View style={[flexDirectionStyles.row, marginStyles.topMargin_5]}>

                            {/* Text - Amount Header */}
                            <Text style={[{ width: '45%', fontSize: 18 }, fontFamilyStyles.extraBold, colorStyles.redColor]}>{Constants.text_amount}</Text>

                            {/* Text - Amount */}
                            <Text style={[{ width: '55%', textAlign: 'right', fontSize: 18 }, fontFamilyStyles.extraBold, colorStyles.redColor]}>{Constants.text_rupees}{response.orderAmount}</Text>
                        </View>

                        {/* Text - Amount Separator */}
                        <Text style={[{ height: 1, marginTop: 15, marginBottom: 15 }, colorStyles.silverBackgroundColor]} />

                        {/* Wrapper - Payment Mode */}
                        <View style={flexDirectionStyles.row}>

                            {/* Text - Payment Mode Header */}
                            <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_payment_mode}</Text>

                            {/* Text - Payment Mode */}
                            <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>{data.paymentType}</Text>
                        </View>

                        {/* Wrapper - Order Id */}
                        <View style={[flexDirectionStyles.row, marginStyles.topMargin_25]}>

                            {/* Text - Order Id Header */}
                            <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_order_id}</Text>

                            {/* Text - Order Id */}
                            <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>{uniqueOrderId}</Text>
                        </View>

                        {/* Wrapper - Order Date */}
                        <View style={[flexDirectionStyles.row, marginStyles.topMargin_25]}>

                            {/* Text - Order Date Header */}
                            <Text style={[widthStyles.width_45p, fontSizeStyles.fontSize_16, fontFamilyStyles.regular, colorStyles.blackColor]}>{Constants.text_order_date}</Text>

                            {/* Text - Order Date */}
                            <Text style={[widthStyles.width_55p, fontSizeStyles.fontSize_14, fontFamilyStyles.regular, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextRight, colorStyles.greyColor]}>{moment(Date(response.txTime)).format('MM/DD/YYYY HH:mm:ss')}</Text>
                        </View>

                        {/* TouchableOpacity - Done Button Click Event */}
                        <TouchableOpacity style={[heightStyles.height_46, positionStyles.absolute, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.bottomMargin_30]} onPress={() => navigateUserAfterSuccess()} activeOpacity={0.5}>

                            {/* Text - Button (Done) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_done}</Text>

                            {/* Image - Right Arrow */}
                            <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                        </TouchableOpacity>
                    </View>
                </View>
            )
            }
            {/* ) : null} */}
        </SafeAreaView >
    )
}

export default PaymentGateway;