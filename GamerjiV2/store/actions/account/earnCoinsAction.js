import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const getEarnCoinsList = (viewModel) => {
    return {
        type: EARN_LIST,
        model: viewModel
    };
};

export const getEarnCoinsListFailed = (viewModel) => {
    return {
        type: EARN_LIST_FAILED,
        model: viewModel
    };
};

export const requestEarnCoinList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.EARN_COIN.APPLIST);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;

        axios
            .post(WebFields.EARN_COIN.APPLIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getEarnCoinsList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getEarnCoinsListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const EARN_LIST = 'EARN_LIST';
export const EARN_LIST_FAILED = 'EARN_LIST_FAILED';

