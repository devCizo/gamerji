import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';

{ /* Action Types */ }
export const REQUEST_PROFILE = 'REQUEST_PROFILE';
export const REQUEST_PROFILE_FAILED = 'REQUEST_PROFILE_FAILED';
export const STAT_LIST = 'STAT_LIST';
export const STAT_LIST_FAILED = 'STAT_LIST_FAILED';
export const RESET_PROFILE_STATE = 'RESET_PROFILE_STATE'

{ /* Request - Profile */ }
export const profileRequest = (viewModel) => {
    return {
        type: REQUEST_PROFILE,
        model: viewModel
    };
};

{ /* Request Failure - Profile */ }
export const profileRequestFailed = (viewModel) => {
    return {
        type: REQUEST_PROFILE_FAILED,
        model: viewModel
    };
};

{ /* Request - Stat List */ }
export const statList = (viewModel) => {
    return {
        type: STAT_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Stat List */ }
export const statListFailed = (viewModel) => {
    return {
        type: STAT_LIST_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Profile */ }
export const resetProfileState = () => {
    return {
        type: RESET_PROFILE_STATE,
    };
};

{ /* Request - Profile */ }
export const requestProfile = (payload) => {
    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.ACCOUNT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ACCOUNT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                global.profile = response.data.item
                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(profileRequest(response.data.item));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(profileRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

{ /* Request - Stats */ }
export const requestStats = (payload) => {
    addLog("STATS PAYLOAD:::> ", payload);
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_STAT_LIST, payload)
            .then((response) => {
                addLog("STATS SUCCESS:::> ", '');
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(statList(response.data));
            })
            .catch((error) => {
                addLog("STATS ERROR:::> ", '');
                addLog("Error:::> ", error.response.data);
                dispatch(statListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};