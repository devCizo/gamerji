import { StyleSheet, Dimensions, Platform, StatusBar } from 'react-native';
import colors from '../assets/colors/colors';
const { height, width } = Dimensions.get('window');


{ /* Container Styles */ }
export const containerStyles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    containerFullBackground: {
        height: height * 0.15
    },
    headerHeight: {
        height: height * 0.05
    }
})


{ /* Height Styles */ }
export const heightStyles = StyleSheet.create({
    height_001: {
        height: height * 0.001
    },
    height_002: {
        height: height * 0.002
    },
    height_005: {
        height: height * 0.005
    },
    height_01: {
        height: height * 0.01
    },
    height_015: {
        height: height * 0.015
    },
    height_02: {
        height: height * 0.02
    },
    height_025: {
        height: height * 0.025
    },
    height_03: {
        height: height * 0.03
    },
    height_035: {
        height: height * 0.035
    },
    height_04: {
        height: height * 0.04
    },
    height_045: {
        height: height * 0.045
    },
    height_05: {
        height: height * 0.05
    },
    height_055: {
        height: height * 0.055
    },
    height_06: {
        height: height * 0.06
    },
    height_065: {
        height: height * 0.065
    },
    height_07: {
        height: height * 0.07
    },
    height_075: {
        height: height * 0.075
    },
    height_08: {
        height: height * 0.08
    },
    height_085: {
        height: height * 0.085
    },
    height_09: {
        height: height * 0.09
    },
    height_095: {
        height: height * 0.095
    },
    height_10: {
        height: height * 0.10
    },
    height_12: {
        height: height * 0.12
    },
    height_14: {
        height: height * 0.14
    },
    // height_15: {
    //     height: height * 0.15
    // },
    height_151: {
        height: height * 0.151
    },
    // height_16: {
    //     height: height * 0.16
    // },
    height_18: {
        height: height * 0.18
    },
    height_20: {
        height: height * 0.20
    },
    height_22: {
        height: height * 0.22
    },
    height_30: {
        height: height * 0.30
    },
    height_35: {
        height: height * 0.35
    },
    height_100p: {
        height: '100%'
    },

    // CHANGE - Height 
    height_05_new: {
        height: 0.5
    },
    height_1: {
        height: 1
    },
    height_2: {
        height: 2
    },
    height_5: {
        height: 5
    },
    height_6: {
        height: 6
    },
    height_8_new: {
        height: 8
    },
    height_15: {
        height: 15
    },
    height_10_new: {
        height: 10
    },
    height_11: {
        height: 11
    },
    height_14_new: {
        height: 14
    },
    height_16: {
        height: 16
    },
    height_17: {
        height: 17
    },
    height_20: {
        height: 20
    },
    height_21: {
        height: 21
    },
    height_24: {
        height: 24
    },
    height_30_new: {
        height: 30
    },
    height_32: {
        height: 32
    },
    height_37: {
        height: 37
    },
    height_40: {
        height: 40
    },
    height_44: {
        height: 44
    },
    height_46: {
        height: 46
    },
    height_48: {
        height: 48
    },
    height_50: {
        height: 50
    },
    height_51: {
        height: 51
    },
    height_60: {
        height: 60
    },
    height_65: {
        height: 65
    },
    height_70: {
        height: 70
    },
    height_80: {
        height: 80
    },
    height_90: {
        height: 90
    },
    height_95: {
        height: 95
    },
    height_100: {
        height: 100
    },
    height_103: {
        height: 103
    },
    height_110: {
        height: 110
    },
    height_112: {
        height: 112
    },
    height_115: {
        height: 115
    },
    height_117: {
        height: 117
    },
    height_125: {
        height: 125
    },
    height_130: {
        height: 130
    },
    height_150: {
        height: 150
    },
    height_170: {
        height: 170
    },
    height_190: {
        height: 190
    },
    height_200: {
        height: 200
    },
    height_210: {
        height: 210
    }
})


{ /* Width Styles */ }
export const widthStyles = StyleSheet.create({
    width_1: {
        width: 1
    },
    width_2: {
        width: 2
    },
    width_3: {
        width: 3
    },
    width_8: {
        width: width * 0.08
    },
    width_10: {
        width: width * 0.10
    },
    width_12: {
        width: width * 0.12
    },
    width_13: {
        width: width * 0.13
    },
    width_14: {
        width: width * 0.14
    },
    width_15: {
        width: width * 0.15
    },
    width_20: {
        width: width * 0.20
    },
    width_28: {
        width: width * 0.28
    },
    width_29: {
        width: width * 0.29
    },
    width_295: {
        width: width * 0.295
    },
    width_30: {
        width: width * 0.30
    },
    width_35: {
        width: width * 0.35
    },
    width_37: {
        width: width * 0.37
    },
    width_40: {
        width: width * 0.40
    },
    width_65: {
        width: width * 0.65
    },
    width_10p: {
        width: '10%'
    },
    width_20p: {
        width: '20%'
    },
    width_22p: {
        width: '22%'
    },
    width_25p: {
        width: '25%'
    },
    width_30p: {
        width: '30%'
    },
    width_31p: {
        width: '31%'
    },
    width_32p: {
        width: '32%'
    },
    width_35p: {
        width: '35%'
    },
    width_40p: {
        width: '40%'
    },
    width_45p: {
        width: '45%'
    },
    width_50p: {
        width: '50%'
    },
    width_55p: {
        width: '55%'
    },
    width_60p: {
        width: '60%'
    },
    width_65p: {
        width: '65%'
    },
    width_70p: {
        width: '70%'
    },
    width_80p: {
        width: '80%'
    },
    width_90p: {
        width: '90%'
    },
    width_100p: {
        width: '100%'
    },

    // CHANGE - Width 
    width_05: {
        width: 0.5
    },
    width_6: {
        width: 6
    },
    width_8: {
        width: 8
    },
    width_10: {
        width: 10
    },
    width_12_new: {
        width: 12
    },
    width_14_new: {
        width: 14
    },
    width_16: {
        width: 16
    },
    width_17: {
        width: 17
    },
    width_18: {
        width: 18
    },
    width_19: {
        width: 19
    },
    width_20_new: {
        width: 20
    },
    width_21: {
        width: 21
    },
    width_24: {
        width: 24
    },
    width_30_new: {
        width: 30
    },
    width_45: {
        width: 45
    },
    width_48: {
        width: 48
    },
    width_50: {
        width: 50
    },
    width_60: {
        width: 60
    },
    width_65: {
        width: 65
    },
    width_70: {
        width: 70
    },
    width_75: {
        width: 75
    },
    width_100: {
        width: 100
    },
    width_101: {
        width: 101
    },
    width_105: {
        width: 105
    },
    width_110: {
        width: 110
    },
    width_115: {
        width: 115
    },
    width_132: {
        width: 132
    },
    width_136: {
        width: 136
    },
    width_138: {
        width: 138
    },
    width_148: {
        width: 148
    },
    width_150: {
        width: 150
    },
    width_160: {
        width: 160
    },
    width_206: {
        width: 206
    },
    width_270: {
        width: 270
    },
    width_301: {
        width: 301
    },
    width_315: {
        width: 315
    },
    width_320: {
        width: 320
    },
    width_330: {
        width: 330
    }
})


{ /* Flex Direction Styles */ }
export const flexDirectionStyles = StyleSheet.create({
    row: {
        flexDirection: 'row'
    },
    column: {
        flexDirection: 'column'
    }
})


{ /* Position Styles */ }
export const positionStyles = StyleSheet.create({
    relative: {
        position: 'relative'
    },
    absolute: {
        position: 'absolute'
    }
})


{ /* Justify Content Styles */ }
export const justifyContentStyles = StyleSheet.create({
    center: {
        justifyContent: 'center'
    },
    spaceBetween: {
        justifyContent: 'space-between'
    },
    flexEnd: {
        justifyContent: 'flex-end'
    }
})


{ /* Alignment Styles */ }
export const alignmentStyles = StyleSheet.create({
    alignItemsCenter: {
        alignItems: 'center'
    },
    alignItemsEnd: {
        alignItems: 'flex-end'
    },
    alignSelfCenter: {
        alignSelf: 'center'
    },
    alignSelfStretch: {
        alignSelf: 'stretch'
    },
    alignTextLeft: {
        textAlign: 'left'
    },
    alignTextRight: {
        textAlign: 'right'
    },
    alignTextCenter: {
        textAlign: 'center'
    },
    alignTextFromLeftRight: {
        left: 0,
        right: 0
    },
    alignTextFromTopBottom: {
        top: 0,
        bottom: 0
    },
    alignViewToCenter: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    alignFooterContentCenter: {
        left: 'auto',
        right: 'auto'
    },
    top_0: {
        top: 0,
    },
    bottom_0: {
        bottom: 0
    },
    left_0: {
        left: 0,
    },
    right_0: {
        right: 0
    },
    oveflow: {
        overflow: 'hidden'
    }
})


{ /* Resize Mode Styles */ }
export const resizeModeStyles = StyleSheet.create({
    resizeMode: {
        resizeMode: 'contain'
    },
    cover: {
        resizeMode: 'cover'
    }
})


{ /* Body Content Styles */ }
export const bodyStyles = StyleSheet.create({
    wrapperBodyContent: {
        paddingStart: width * 0.05,
        paddingEnd: width * 0.05,
        paddingTop: height * 0.05,
        paddingBottom: height * 0.02
    },
    wrapperBodyContentWithoutLogo: {
        paddingStart: width * 0.05,
        paddingEnd: width * 0.05,
        // paddingTop: height * 0.05,
        paddingBottom: height * 0.02
    },
    wrapperFooterContent: {
        width: '100%',
        height: height * 0.085
    }
})


{ /* Scroll View Styles */ }
export const scrollViewStyles = StyleSheet.create({
    wrapperFullScrollView: {
        height: height
    }
})


{ /* Image Styles */ }
export const imageStyles = StyleSheet.create({
    imageGamerJiLogo: {
        maxHeight: 100,
        top: -72
    },
    imageGamerJiLogoBottomNavigaion: {
        maxHeight: 80,
        top: -57
    }
})


{ /* Font Family Styles */ }
export const fontFamilyStyles = StyleSheet.create({
    regular: {
        fontFamily: 'ProximaNova-Regular'
    },
    thin: {
        fontFamily: 'ProximaNova-Thin'
    },
    bold: {
        fontFamily: 'ProximaNova-Bold'
    },
    extraBold: {
        fontFamily: 'ProximaNova-Bold'
    },
    semiBold: {
        fontFamily: 'ProximaNova-Semibold'
    }
})

export const fontFamilyStyleNew = {
    regular: 'ProximaNova-Regular',
    thin: 'ProximaNova-Thin',
    bold: 'ProximaNova-Bold',
    extraBold: 'ProximaNova-Bold',
    semiBold: 'ProximaNova-Semibold'
}


{ /* Font Styles */ }
export const fontStyles = StyleSheet.create({
    underline: {
        borderBottomWidth: 1
    }
})


{ /* Input Text Styles */ }
export const inputTextStyles = StyleSheet.create({
    inputText: {
        paddingTop: height * 0.008,
        paddingBottom: height * 0.008,
        paddingStart: height * 0.02,
        paddingEnd: height * 0.02
    }
})

export const shadowStyle = StyleSheet.create({
    shadow: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 2
    }
})


{ /* Color Styles */ }
export const colorStyles = StyleSheet.create({

    // ToDo: Background Colors 
    primaryBackgroundColor: {
        backgroundColor: colors.black
    },
    whiteBackgroundColor: {
        backgroundColor: colors.white
    },
    blueBackgroundColor: {
        backgroundColor: colors.blue
    },
    orangeBackgroundColor: {
        backgroundColor: colors.orange
    },
    redBackgroundColor: {
        backgroundColor: colors.red
    },
    greenBackgroundColor: {
        backgroundColor: colors.green
    },
    yellowBackgroundColor: {
        backgroundColor: colors.yellow
    },
    greyBackgroundColor: {
        backgroundColor: colors.color_input_text
    },
    silverBackgroundColor: {
        backgroundColor: colors.silver
    },
    skyBlueBackgroundColor: {
        backgroundColor: colors.sky_blue
    },
    lightGreenBackgroundColor: {
        backgroundColor: colors.light_green
    },
    lightSilverBackgroundColor: {
        backgroundColor: colors.light_silver
    },

    // ToDo: Text Colors
    blackColor: {
        color: colors.black
    },
    whiteColor: {
        color: colors.white
    },
    redColor: {
        color: colors.red
    },
    greyColor: {
        color: colors.color_input_text
    },
    silverColor: {
        color: colors.silver
    },
    yellowColor: {
        color: colors.yellow
    },
    blueColor: {
        color: colors.blue
    },
    darkGreenColor: {
        color: colors.dark_green
    },
    greenColor: {
        color: colors.green
    },
    lightOrangeColor: {
        color: colors.light_orange
    },
    darkRedColor: {
        color: colors.dark_red
    },

    // ToDo: Border Colors
    greyBorderColor: {
        borderColor: colors.color_input_text_border
    },
    whiteBorderColor: {
        borderColor: colors.white
    },
    yellowBorderColor: {
        borderColor: colors.yellow
    },
    redBorderColor: {
        borderColor: colors.red
    },
    darkGreyBorderColor: {
        borderColor: colors.grey
    },

    // ToDo: Border Bottom Colors
    redUnderlineColor: {
        borderBottomColor: colors.red
    },
    silverUnderlineColor: {
        borderBottomColor: colors.silver
    },

    // ToDo: Shadow Colors
    primaryShadowColor: {
        shadowColor: colors.black
    },
    redShadowColor: {
        shadowColor: colors.red
    },
    yellowShadowColor: {
        shadowColor: colors.yellow
    },
    skyBlueShadowColor: {
        shadowColor: colors.sky_blue
    },
    lightGreenShadowColor: {
        shadowColor: colors.light_green
    }
})


{ /* Margin Styles */ }
export const marginStyles = StyleSheet.create({

    // ToDo: Static Top Spacing (Minus)
    top_10_minus: {
        top: -10
    },
    top_22_minus: {
        top: -22
    },
    top_25_minus: {
        top: -height * 0.04
    },
    top_30_minus: {
        top: -30
    },
    top_30_minus: {
        top: -30
    },
    top_45_minus: {
        top: -45
    },
    top_75_minus: {
        top: -75
    },
    top_78_minus: {
        top: -78
    },

    // ToDo: Static Margin Top Spacing (Minus)
    topMargin_23_minus: {
        marginTop: -23
    },

    // ToDo: Top Spacing
    top_01: {
        top: height * 0.01
    },
    top_02: {
        top: height * 0.02
    },
    top_03: {
        top: height * 0.03
    },
    top_04: {
        top: height * 0.04
    },
    top_05: {
        top: height * 0.05
    },

    // ToDo: Bottom Spacing
    bottom_005: {
        bottom: height * 0.005
    },
    bottom_01: {
        bottom: height * 0.01
    },

    // ToDo: Left Spacing
    left_005: {
        left: width * 0.005
    },
    left_01: {
        left: width * 0.01
    },

    // ToDo: Right Spacing
    right_008: {
        right: width * 0.008
    },
    right_01: {
        right: width * 0.01
    },

    // ToDo: Full Paddings
    margin_01: {
        margin: height * 0.01
    },
    margin_02: {
        margin: height * 0.02
    },
    margin_03: {
        margin: width * 0.03
    },
    margin_04: {
        margin: width * 0.04
    },
    margin_05: {
        margin: width * 0.05
    },

    // ToDo: Top Margins
    topMargin_005: {
        marginTop: height * 0.005
    },
    topMargin_01: {
        marginTop: height * 0.01
    },
    topMargin_02: {
        marginTop: height * 0.02
    },
    topMargin_025: {
        marginTop: height * 0.025
    },
    topMargin_03: {
        marginTop: height * 0.03
    },
    topMargin_04: {
        marginTop: height * 0.04
    },
    topMargin_05: {
        marginTop: height * 0.05
    },
    topMargin_06: {
        marginTop: height * 0.06
    },
    topMargin_07: {
        marginTop: height * 0.07
    },
    topMargin_08: {
        marginTop: height * 0.08
    },
    topMargin_09: {
        marginTop: height * 0.09
    },
    topMargin_1: {
        marginTop: height * 0.10
    },

    // ToDo: Bottom Margin (Minus)
    bottomMargin_04_minus: {
        marginBottom: -height * 0.04
    },

    // ToDo: Bottom Margins
    bottomMargin_005: {
        marginBottom: height * 0.005
    },
    bottomMargin_01: {
        marginBottom: height * 0.01
    },
    bottomMargin_02: {
        marginBottom: height * 0.02
    },
    bottomMargin_03: {
        marginBottom: height * 0.03
    },
    bottomMargin_04: {
        marginBottom: height * 0.04
    },
    bottomMargin_05: {
        marginBottom: height * 0.05
    },
    bottomMargin_06: {
        marginBottom: height * 0.06
    },
    bottomMargin_07: {
        marginBottom: height * 0.07
    },
    bottomMargin_08: {
        marginBottom: height * 0.08
    },
    bottomMargin_09: {
        marginBottom: height * 0.09
    },
    bottomMargin_1: {
        marginBottom: height * 0.10
    },

    // ToDo: Left Margins
    leftMargin_005: {
        marginLeft: width * 0.005
    },
    leftMargin_01: {
        marginLeft: width * 0.01
    },
    leftMargin_02: {
        marginLeft: width * 0.02
    },
    leftMargin_03: {
        marginLeft: width * 0.03
    },
    leftMargin_04: {
        marginLeft: width * 0.04
    },
    leftMargin_05: {
        marginLeft: width * 0.05
    },
    leftMargin_06: {
        marginLeft: width * 0.06
    },
    leftMargin_07: {
        marginLeft: width * 0.07
    },
    leftMargin_08: {
        marginLeft: width * 0.08
    },
    leftMargin_09: {
        marginLeft: width * 0.09
    },
    leftMargin_1: {
        marginLeft: width * 0.10
    },

    // ToDo: Right Margins
    rightMargin_01: {
        marginRight: width * 0.01
    },
    rightMargin_02: {
        marginRight: width * 0.02
    },
    rightMargin_03: {
        marginRight: width * 0.03
    },
    rightMargin_04: {
        marginRight: width * 0.04
    },
    rightMargin_05: {
        marginRight: width * 0.05
    },
    rightMargin_06: {
        marginRight: width * 0.06
    },
    rightMargin_07: {
        marginRight: width * 0.07
    },
    rightMargin_08: {
        marginRight: width * 0.08
    },
    rightMargin_09: {
        marginRight: width * 0.09
    },
    rightMargin_1: {
        marginRight: height * 0.10
    },

    // CHANGE - ToDo: Static Margin Bottom Spacing (Minus)
    bottomMargin_30_minus: {
        marginBottom: -30
    },

    // CHANGE - ToDo: Left Spacing
    left_20: {
        left: 20
    },

    // CHANGE - ToDo: Right Spacing
    right_20: {
        right: 20
    },

    // CHANGE - ToDo: Top Spacing
    top_2: {
        top: 2
    },
    top_150: {
        top: 150
    },
    top_200: {
        top: 200
    },
    top_250: {
        top: 250
    },
    top_400: {
        top: 400
    },

    // CHANGE - ToDo: Bottom Spacing
    bottom_22: {
        bottom: 22
    },
    bottom_32: {
        bottom: 32
    },
    bottom_50: {
        bottom: 50
    },

    // CHANGE - ToDo: Full Margins 
    margin_1: {
        margin: 1
    },
    margin_2: {
        margin: 2
    },
    margin_5: {
        margin: 5
    },
    margin_10: {
        margin: 10
    },
    margin_20: {
        margin: 20
    },

    //  CHANGE - ToDo: Left Margins
    leftMargin_2: {
        marginLeft: 2
    },
    leftMargin_4: {
        marginLeft: 4
    },
    leftMargin_5: {
        marginLeft: 5
    },
    leftMargin_6: {
        marginLeft: 6
    },
    leftMargin_7: {
        marginLeft: 7
    },
    leftMargin_8: {
        marginLeft: 8
    },
    leftMargin_10: {
        marginLeft: 10
    },
    leftMargin_12: {
        marginLeft: 12
    },
    leftMargin_13: {
        marginLeft: 13
    },
    leftMargin_14: {
        marginLeft: 14
    },
    leftMargin_15: {
        marginLeft: 15
    },
    leftMargin_16: {
        marginLeft: 16
    },
    leftMargin_18: {
        marginLeft: 18
    },
    leftMargin_20: {
        marginLeft: 20
    },
    leftMargin_24: {
        marginLeft: 24
    },
    leftMargin_35: {
        marginLeft: 35
    },
    leftMargin_58: {
        marginLeft: 58
    },
    leftMargin_60: {
        marginLeft: 60
    },
    leftMargin_65: {
        marginLeft: 65
    },

    //  CHANGE - ToDo: Right Margins
    rightMargin_3: {
        marginRight: 3
    },
    rightMargin_5: {
        marginRight: 5
    },
    rightMargin_6: {
        marginRight: 6
    },
    rightMargin_7: {
        marginRight: 7
    },
    rightMargin_9: {
        marginRight: 9
    },
    rightMargin_10: {
        marginRight: 10
    },
    rightMargin_12: {
        marginRight: 12
    },
    rightMargin_15: {
        marginRight: 15
    },
    rightMargin_20: {
        marginRight: 20
    },
    rightMargin_21: {
        marginRight: 21
    },
    rightMargin_25: {
        marginRight: 25
    },
    rightMargin_35: {
        marginRight: 35
    },
    rightMargin_40: {
        marginRight: 40
    },

    //  CHANGE - ToDo: Top Margins
    topMargin_4: {
        marginTop: 4
    },
    topMargin_5: {
        marginTop: 5
    },
    topMargin_7: {
        marginTop: 7
    },
    topMargin_10: {
        marginTop: 10
    },
    topMargin_14: {
        marginTop: 14
    },
    topMargin_15: {
        marginTop: 15
    },
    topMargin_16: {
        marginTop: 16
    },
    topMargin_17: {
        marginTop: 17
    },
    topMargin_18: {
        marginTop: 18
    },
    topMargin_19: {
        marginTop: 19
    },
    topMargin_20: {
        marginTop: 20
    },
    topMargin_22: {
        marginTop: 22
    },
    topMargin_25: {
        marginTop: 25
    },
    topMargin_26: {
        marginTop: 26
    },
    topMargin_30: {
        marginTop: 30
    },
    topMargin_32: {
        marginTop: 32
    },
    topMargin_33: {
        marginTop: 33
    },
    topMargin_35: {
        marginTop: 35
    },
    topMargin_38: {
        marginTop: 38
    },
    topMargin_40: {
        marginTop: 40
    },
    topMargin_43: {
        marginTop: 43
    },
    topMargin_50: {
        marginTop: 50
    },
    topMargin_55: {
        marginTop: 55
    },
    topMargin_60: {
        marginTop: 60
    },
    topMargin_65: {
        marginTop: 65
    },
    topMargin_70: {
        marginTop: 70
    },
    topMargin_80: {
        marginTop: 80
    },
    topMargin_70: {
        marginTop: 70
    },
    topMargin_100: {
        marginTop: 100
    },
    topMargin_120: {
        marginTop: 120
    },
    topMargin_300: {
        marginTop: 300
    },

    //  CHANGE - ToDo: Bottom Margins
    bottomMargin_4: {
        marginBottom: 4
    },
    bottomMargin_5: {
        marginBottom: 5
    },
    bottomMargin_6: {
        marginBottom: 6
    },
    bottomMargin_7: {
        marginBottom: 7
    },
    bottomMargin_10: {
        marginBottom: 10
    },
    bottomMargin_15: {
        marginBottom: 15
    },
    bottomMargin_20: {
        marginBottom: 20
    },
    bottomMargin_30: {
        marginBottom: 30
    },
    bottomMargin_40: {
        marginBottom: 40
    },
    bottomMargin_50: {
        marginBottom: 50
    },
    bottomMargin_60: {
        marginBottom: 60
    }
})


{ /* Padding Styles */ }
export const paddingStyles = StyleSheet.create({

    // ToDo: Full Paddings
    padding_003: {
        padding: height * 0.003
    },
    padding_005: {
        padding: height * 0.005
    },
    padding_007: {
        padding: height * 0.007
    },
    padding_008: {
        padding: height * 0.008
    },
    padding_009: {
        padding: height * 0.009
    },
    padding_01: {
        padding: height * 0.01
    },
    padding_015: {
        padding: height * 0.015
    },
    padding_02: {
        padding: height * 0.02
    },
    padding_03: {
        padding: width * 0.03
    },
    padding_04: {
        padding: width * 0.04
    },
    padding_05: {
        padding: width * 0.05
    },
    padding_008: {
        padding: width * 0.008
    },

    // ToDo: Top Paddings
    topPadding_005: {
        paddingTop: height * 0.005
    },
    topPadding_01: {
        paddingTop: height * 0.01
    },
    topPadding_02: {
        paddingTop: height * 0.02
    },
    topPadding_03: {
        paddingTop: height * 0.03
    },
    topPadding_04: {
        paddingTop: height * 0.04
    },
    topPadding_05: {
        paddingTop: height * 0.05
    },
    topPadding_06: {
        paddingTop: height * 0.06
    },
    topPadding_07: {
        paddingTop: height * 0.07
    },
    topPadding_08: {
        paddingTop: height * 0.08
    },
    topPadding_09: {
        paddingTop: height * 0.09
    },
    topPadding_1: {
        paddingTop: height * 0.10
    },

    // ToDo: Bottom Paddings
    bottomPadding_005: {
        paddingBottom: height * 0.005
    },
    bottomPadding_01: {
        paddingBottom: height * 0.01
    },
    bottomPadding_02: {
        paddingBottom: height * 0.02
    },
    bottomPadding_03: {
        paddingBottom: height * 0.03
    },
    bottomPadding_04: {
        paddingBottom: height * 0.04
    },
    bottomPadding_05: {
        paddingBottom: height * 0.05
    },
    bottomPadding_06: {
        paddingBottom: height * 0.06
    },
    bottomPadding_07: {
        paddingBottom: height * 0.07
    },
    bottomPadding_08: {
        paddingBottom: height * 0.08
    },
    bottomPadding_09: {
        paddingBottom: height * 0.09
    },
    bottomPadding_1: {
        paddingBottom: height * 0.10
    },

    // ToDo: Left Paddings
    leftPadding_01: {
        paddingLeft: width * 0.01
    },
    leftPadding_02: {
        paddingLeft: width * 0.02
    },
    leftPadding_03: {
        paddingLeft: width * 0.03
    },
    leftPadding_04: {
        paddingLeft: width * 0.04
    },
    leftPadding_05: {
        paddingLeft: width * 0.05
    },
    leftPadding_06: {
        paddingLeft: width * 0.06
    },
    leftPadding_07: {
        paddingLeft: width * 0.07
    },
    leftPadding_08: {
        paddingLeft: width * 0.08
    },
    leftPadding_09: {
        paddingLeft: width * 0.09
    },
    leftPadding_1: {
        paddingLeft: width * 0.10
    },

    // ToDo: Right Paddings
    rightPadding_01: {
        paddingRight: width * 0.01
    },
    rightPadding_02: {
        paddingRight: width * 0.02
    },
    rightPadding_03: {
        paddingRight: width * 0.03
    },
    rightPadding_04: {
        paddingRight: width * 0.04
    },
    rightPadding_05: {
        paddingRight: width * 0.05
    },
    rightPadding_06: {
        paddingRight: width * 0.06
    },
    rightPadding_07: {
        paddingRight: width * 0.07
    },
    rightPadding_08: {
        paddingRight: width * 0.08
    },
    rightPadding_09: {
        paddingRight: width * 0.09
    },
    rightMargin_1: {
        paddingRight: height * 0.10
    },

    // CHANGE - Full Paddings 
    padding_1: {
        padding: 1
    },
    padding_2: {
        padding: 2
    },
    padding_3: {
        padding: 3
    },
    padding_5: {
        padding: 5
    },
    padding_10: {
        padding: 10
    },
    padding_20: {
        padding: 20
    },

    // CHANGE - ToDo: Left Paddings
    leftPadding_10: {
        paddingLeft: 10
    },
    leftPadding_15: {
        paddingLeft: 15
    },
    leftPadding_24: {
        paddingLeft: 24
    },

    // CHANGE - ToDo: Right Paddings
    rightPadding_10: {
        paddingRight: 10
    },
    rightPadding_15: {
        paddingRight: 15
    },
    rightPadding_17: {
        paddingRight: 17
    },
    rightPadding_24: {
        paddingRight: 24
    },

    // CHANGE - ToDo: Top Paddings
    topPadding_10: {
        paddingTop: 10
    },
    topPadding_13: {
        paddingTop: 13
    },
    topPadding_15: {
        paddingTop: 15
    },
    topPadding_20: {
        paddingTop: 20
    },
    topPadding_30: {
        paddingTop: 30
    },
    topPadding_40: {
        paddingTop: 40
    },
    topPadding_45: {
        paddingTop: 45
    },
    topPadding_50: {
        paddingTop: 50
    },

    // CHANGE - ToDo: Right Paddings
    rightPadding_13: {
        paddingRight: 13
    },
    rightPadding_15: {
        paddingRight: 15
    },

    // CHANGE - ToDo: Bottom Paddings
    bottomPadding_10: {
        paddingBottom: 10
    },
    bottomPadding_20: {
        paddingBottom: 20
    },
})


{ /* Font Size Styles */ }
export const fontSizeStyles = StyleSheet.create({

    fontSize_01: {
        fontSize: width * 0.01
    },
    fontSize_015: {
        fontSize: width * 0.015
    },
    fontSize_02: {
        fontSize: width * 0.02
    },
    fontSize_025: {
        fontSize: width * 0.025
    },
    fontSize_03: {
        fontSize: width * 0.03
    },
    fontSize_035: {
        fontSize: width * 0.035
    },
    fontSize_04: {
        fontSize: width * 0.04
    },
    fontSize_045: {
        fontSize: width * 0.045
    },
    fontSize_05: {
        fontSize: width * 0.05
    },
    fontSize_055: {
        fontSize: width * 0.055
    },
    fontSize_06: {
        fontSize: width * 0.06
    },
    fontSize_065: {
        fontSize: width * 0.065
    },
    fontSize_07: {
        fontSize: width * 0.07
    },
    fontSize_075: {
        fontSize: width * 0.075
    },
    fontSize_08: {
        fontSize: width * 0.08
    },
    fontSize_085: {
        fontSize: width * 0.085
    },
    fontSize_09: {
        fontSize: width * 0.09
    },
    fontSize_095: {
        fontSize: width * 0.095
    },
    fontSize_1: {
        fontSize: width * 0.1
    },

    // CHANGE
    fontSize_4: {
        fontSize: 4
    },
    fontSize_5: {
        fontSize: 5
    },
    fontSize_6: {
        fontSize: 6
    },
    fontSize_7: {
        fontSize: 7
    },
    fontSize_8: {
        fontSize: 8
    },
    fontSize_9: {
        fontSize: 9
    },
    fontSize_10: {
        fontSize: 10
    },
    fontSize_11: {
        fontSize: 11
    },
    fontSize_12: {
        fontSize: 12
    },
    fontSize_13: {
        fontSize: 13
    },
    fontSize_14: {
        fontSize: 14
    },
    fontSize_15: {
        fontSize: 15
    },
    fontSize_16: {
        fontSize: 16
    },
    fontSize_18: {
        fontSize: 18
    },
    fontSize_20: {
        fontSize: 20
    },
    fontSize_30: {
        fontSize: 30
    },
})


{ /* Margin Styles */ }
export const borderStyles = StyleSheet.create({
    bodyRoundedCorner: {
        borderTopEndRadius: width * 0.17,
        borderTopStartRadius: width * 0.17
    },
    bodyRoundedCornerBottomNavigation: {
        borderTopEndRadius: width * 0.10,
        borderTopStartRadius: width * 0.10,
        borderBottomStartRadius: width * 0.10,
        borderBottomEndRadius: width * 0.10,
        marginTop: 80,
        marginBottom: 10
    },
    bodyRoundedCornerWithoutLogoBottomNavigation: {
        borderTopEndRadius: width * 0.10,
        borderTopStartRadius: width * 0.10,
        borderBottomStartRadius: width * 0.10,
        borderBottomEndRadius: width * 0.10,
        marginTop: 20,
        marginBottom: 10
    },
    bodyRoundedCornerForTop: {
        borderTopEndRadius: width * 0.10,
        borderTopStartRadius: width * 0.10,
        marginTop: 10
    },
    inputTextRoundedCorner: {
        borderWidth: 1,
        borderRadius: 20
    },
    buttonOTPRoundedCorner: {
        // height: 50,
        // width: 50,
        // borderRadius: 100
    },
    borderWidth: {
        borderWidth: 1,
        borderRadius: 10
    },

    // Border Width
    borderWidth_05: {
        borderWidth: 0.5
    },
    borderWidth_1: {
        borderWidth: 1
    },
    borderWidth_2: {
        borderWidth: 2
    },
    borderWidth_3: {
        borderWidth: 3
    },
    borderWidth_4: {
        borderWidth: 4
    },
    borderWidth_5: {
        borderWidth: 5
    },
    buttonRoundedCorner: {
        borderRadius: 30
    },

    // CHANGE - Border Rounded Corner 
    bodyTopRoundedCorner_60: {
        borderTopStartRadius: 60,
        borderTopEndRadius: 60
    },

    // CHANGE - Border Top Left Corner 
    borderTopLeftRadius_5: {
        borderTopLeftRadius: 5
    },
    borderTopLeftRadius_10: {
        borderTopLeftRadius: 10
    },
    borderTopLeftRadius_15: {
        borderTopLeftRadius: 15
    },
    borderTopLeftRadius_20: {
        borderTopLeftRadius: 20
    },
    borderTopLeftRadius_40: {
        borderTopLeftRadius: 40
    },

    // CHANGE - Border Bottom Left Corner 
    borderBottomLeftRadius_5: {
        borderBottomLeftRadius: 5
    },
    borderBottomLeftRadius_10: {
        borderBottomLeftRadius: 10
    },
    borderBottomLeftRadius_15: {
        borderBottomLeftRadius: 15
    },
    borderBottomLeftRadius_20: {
        borderBottomLeftRadius: 20
    },
    borderBottomLeftRadius_40: {
        borderBottomLeftRadius: 40
    },

    // CHANGE - Border Top Right Corner 
    borderTopRightRadius_5: {
        borderTopRightRadius: 5
    },
    borderTopRightRadius_10: {
        borderTopRightRadius: 10
    },
    borderTopRightRadius_15: {
        borderTopRightRadius: 15
    },
    borderTopRightRadius_20: {
        borderTopRightRadius: 20
    },
    borderTopRightRadius_40: {
        borderTopRightRadius: 40
    },

    // CHANGE - Border Bottom Right Corner 
    borderBottomRightRadius_5: {
        borderBottomRightRadius: 5
    },
    borderBottomRightRadius_10: {
        borderBottomRightRadius: 10
    },
    borderBottomRightRadius_15: {
        borderBottomRightRadius: 15
    },
    borderBottomRightRadius_20: {
        borderBottomRightRadius: 20
    },
    borderBottomRightRadius_40: {
        borderBottomRightRadius: 40
    },

    // CHANGE - Border Rounded Corner 
    borderRadius_5: {
        borderRadius: 5
    },
    borderRadius_10: {
        borderRadius: 10
    },
    borderRadius_12: {
        borderRadius: 12
    },
    borderRadius_10: {
        borderRadius: 10
    },
    borderRadius_15: {
        borderRadius: 15
    },
    borderRadius_20: {
        borderRadius: 20
    },
    borderRadius_30: {
        borderRadius: 30
    },
    borderRadius_40: {
        borderRadius: 40
    },
    borderRadius_50: {
        borderRadius: 50
    },
    borderRadius_60: {
        borderRadius: 60
    },
    borderRadius_70: {
        borderRadius: 70
    },
    borderRadius_100: {
        borderRadius: 100
    },
})


{ /* Shadow Styles */ }
export const shadowStyles = StyleSheet.create({
    shadowBackground: {
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.05,
        shadowRadius: 10,
        elevation: 3
    },
    shadowBackgroundOTP: {
        // width: '100%',
        // height: '100%',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    },
    shadowBackgroundForView: {
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 5
    },
})


{ /* Tab Bar Styles */ }
export const tabBarStyles = StyleSheet.create({
    tabBar: {
        backgroundColor: colors.black,
        borderTopWidth: 0,
        paddingBottom: height * 0.01
    }
})


{ /* Other Styles */ }
export const otherStyles = StyleSheet.create({

    // Z Index
    zIndex_0: {
        zIndex: 0
    },
    zIndex_1: {
        zIndex: 1
    },

    // Opacity
    opacity_05: {
        opacity: 0.5
    },
    opacity_1: {
        opacity: 1
    },

    // Text Cases
    capitalize: {
        textTransform: 'uppercase'
    },

    // Aspect Ratio
    aspectRatio_1: {
        aspectRatio: 1
    }
})


{ /* Splash Screen Styles */ }
export const splashStyles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerFullBackground: {
        flex: 1,
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    wrapperBodyContent: {
        alignSelf: 'center',
        justifyContent: 'center',
        top: height * 0.15,
        position: 'absolute',
    },
    imageGamerJiLogo: {
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    wrapperTopContent: {
        alignSelf: 'center',
        textAlign: 'center',
        top: height * 0.03,
    },
    textIndiasOnlyESports: {
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: width * 0.09,
        fontFamily: 'ProximaNova-Bold',
        color: colors.black,
        position: 'relative',
    },
    textTournamentPlatform: {
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: width * 0.06,
        top: height * 0.01,
        fontFamily: 'ProximaNova-Regular',
        color: colors.black,
        position: 'relative',
    },
    wrapperApprovedBy: {
        position: 'relative',
        left: 0,
        right: 0,
        bottom: height * 0.055,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textApprovedBy: {
        position: 'relative',
        left: 0,
        right: 0,
        bottom: 0,
        fontFamily: 'ProximaNova-Regular',
        color: colors.grey,
        fontSize: width * 0.04,
        textAlign: 'center',
        alignSelf: 'center',
    },
    wrapperApprovedByCompanies: {
        position: 'relative',
        left: 0,
        right: 0,
        top: height * 0.015,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    imageApprovedByPUBG: {
        right: width * 0.015,
        tintColor: colors.yellow,
    },
    imageApprovedByFreeFire: {
        left: width * 0.015,
    },
    wrapperFooterContent: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrapperFooterItems: {
        flexDirection: 'row',
        marginRight: width * 0.05,
        marginLeft: width * 0.035,
    },
    textFooterItems: {
        fontSize: width * 0.025,
        marginLeft: width * 0.02,
        fontFamily: 'ProximaNova-Regular',
        color: colors.grey,
    },
})


{ /* Country Code Picker Styles */ }
export const countryCodePickerStyles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    titleText: {
        color: '#000',
        fontSize: 25,
        marginBottom: 25,
        fontWeight: 'bold',
    },
    pickerTitleStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'center',
        fontWeight: 'bold',
        flex: 1,
        marginLeft: 10,
        fontSize: 16,
        color: '#000',
        paddingTop: Platform.OS === 'ios' ? 20 : 0
    },
    pickerStyle: {
        height: 60,
        width: 250,
        marginBottom: 10,
        justifyContent: 'center',
        padding: 10,
        borderWidth: 2,
        borderColor: '#303030',
        backgroundColor: 'white',
    },
    selectedCountryTextStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        color: '#000',
        textAlign: 'right',
    },

    countryNameTextStyle: {
        paddingLeft: 10,
        color: '#000',
        textAlign: 'right',
    },

    searchBarStyle: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        marginLeft: 8,
        marginRight: 10,
    },
});