import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { useDispatch } from 'react-redux';
import * as actionCreators from '../store/actions/index';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, justifyContentStyles, positionStyles, fontFamilyStyleNew } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import * as RootNavigation from '../appUtils/rootNavigation.js';

const TopProfilesItem = (props) => {

    const dispatch = useDispatch();
    const width = (Dimensions.get('window').width - 50) / 2;
    const isFromAllProfile = props.isFromAllProfile ? props.isFromAllProfile : false

    const [item, setItem] = useState(props.item)

    const followUnfollow = () => {

        var tempItem = { ...item }
        var payload = {
            followingUser: item._id
        }
        if (tempItem.isFollow && tempItem.isFollow == true) {
            if (tempItem.followers) {
                tempItem.followers = tempItem.followers - 1
            } else {
                tempItem.followers = 0
            }
            tempItem.isFollow = false
            payload['action'] = 'unfollow'
        } else {
            if (tempItem.followers) {
                tempItem.followers = tempItem.followers + 1
            } else {
                tempItem.followers = 1
            }
            tempItem.isFollow = true
            payload['action'] = 'follow'
        }
        setItem(tempItem)
        dispatch(actionCreators.requestFollowUser(payload))
    }

    return (
        <TouchableOpacity onPress={() => RootNavigation.navigate(Constants.nav_other_user_profile, { otherUserId: item._id })} style={{ height: 140, width: isFromAllProfile ? width : 160, marginLeft: item.id == 1 ? 20 : 0, marginRight: 10, marginBottom: isFromAllProfile ? 10 : 0 }} activeOpacity={0.7}>
            <View style={{ height: 107, width: '100%', borderRadius: 5, backgroundColor: colors.black, zIndex: 0, top: 20 }}>

                <Text style={{ marginTop: 40, fontFamily: fontFamilyStyleNew.semiBold, color: colors.white, fontSize: 12, alignSelf: 'center' }} numberOfLines={1} ellipsizeMode='tail'> {item.gamerjiName} </Text>

                <View style={{ flexDirection: 'row', marginTop: 10, alignSelf: 'center' }}>
                    <Image style={{ height: 11, width: 13, alignItems: 'center', alignSelf: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_followers.png')} />

                    <Text style={{ fontFamily: fontFamilyStyleNew.bold, color: colors.white, fontSize: 12, marginLeft: 4 }} numberOfLines={1} ellipsizeMode='tail'> {item.followers ? item.followers : 0}</Text>

                    <Text style={{ fontFamily: fontFamilyStyleNew.regular, color: colors.white, fontSize: 12, marginLeft: 4 }} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_followers}</Text>
                </View>
            </View>

            <Image style={{ height: 50, width: 50, position: 'absolute', top: 0, alignSelf: 'center', alignItems: 'center', resizeMode: 'contain' }} source={require('../assets/images/ic_profile_displaypic.png')} />

            <TouchableOpacity style={{ position: 'absolute', height: 30, width: 80, backgroundColor: colors.white, borderRadius: 5, borderWidth: 1, borderColor: colors.black, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', bottom: 0, zIndex: 1 }} onPress={() => followUnfollow()} activeOpacity={0.8}>

                <Text style={{ fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, fontSize: 14, textAlign: 'center' }}>{item.isFollow && item.isFollow == true ? 'Following' : 'Follow'}</Text>
            </TouchableOpacity>
        </TouchableOpacity>
    )
}

export default TopProfilesItem;