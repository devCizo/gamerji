import React from 'react'
import { Constants } from '../../appUtils/constants';

const topProfileData = [{
    id: '1',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '2',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '5',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '6',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '7',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '8',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '9',
    name: 'Kevin Peterson',
    followers: '20K'
},
{
    id: '10',
    name: 'Kevin Peterson',
    followers: '20K'
}
];

export default topProfileData;