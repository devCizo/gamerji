import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/account/claimCoinAction';

const initialState = {
    model: {
        claimCoinResponse: undefined
    },
    error: false
};


const requestClaimCoin = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimCoinResponse = action.model;
    });
};

const requestClaimCoinFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimCoinResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CLAIM_COIN:
            return requestClaimCoin(state, action);
        case actionTypes.CLAIM_COIN_FAILED:
            return requestClaimCoinFailed(state, action);

        default:
            return state;
    }
};

export default reducer;
