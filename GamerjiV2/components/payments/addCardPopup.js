import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import moment from 'moment';
import { AppConstants } from '../../appUtils/appConstants';
import { addLog, showErrorToastMessage } from '../../appUtils/commonUtlis';

const AddCardPopup = (props) => {

    const [cardHolderName, setCardHolderName] = useState('')
    const [cardNumber, setCardNumber] = useState('')
    const [expiryDate, setExpiryDate] = useState('')
    const [cvv, setCvv] = useState('')

    const handleCardNumberChange = (text) => {
        let value = text.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ')
        addLog(value)
        setCardNumber(value)
    }

    const handleExpiryDateChange = (text) => {

        let textWithoutSpace = text.replace('/', '').replace(' ', '')
        var count = 0
        var tempStr = ''

        textWithoutSpace.split('').forEach(element => {
            count += 1
            if (count == 2) {
                tempStr += (element + '/')
            } else {
                tempStr += element
            }
        });
        setExpiryDate(tempStr)
    }

    const saveCard = () => {
        if (cardHolderName == '') {
            showErrorToastMessage('Please enter card holder name')
            return
        }

        var tempCardNumberARR = cardNumber.split(" ");
        var tempCardNumber = tempCardNumberARR.join("");
        // var tempCardNumber = cardNumber.replaceAll(' ', '')
        if (tempCardNumber == '') {
            showErrorToastMessage('Please enter card number')
            return
        }
        if (tempCardNumber.length < 16) {
            showErrorToastMessage('Please enter valid card number')
            return
        }

        if (expiryDate == '') {
            showErrorToastMessage('Please enter expiry date')
            return
        }

        let currentMonth = moment(new Date()).format('MM')
        let currentYear = moment(new Date()).format('YY')

        var selectedMonth = ''
        var selectedYear = ''

        let expiryData = expiryDate.split('/')
        if (expiryData.length != 2) {
            showErrorToastMessage('Please enter valid expiry date')
            return
        }
        selectedMonth = expiryData[0]
        selectedYear = expiryData[1]

        let isInvalidExpiryDate = selectedMonth > 12 || selectedMonth < 1 || selectedYear < currentYear || (selectedMonth < currentMonth.asInt && selectedYear.asInt <= currentYear.asInt)

        if (isInvalidExpiryDate) {
            showErrorToastMessage('Please enter valid expiry date')
            return
        }

        if (cvv == '') {
            showErrorToastMessage('Please enter cvv')
            return
        }

        if (cvv.length < 3) {
            showErrorToastMessage('Please enter valid cvv')
            return
        }

        var params = {
            cardHolderName: cardHolderName,
            cardNumber: tempCardNumber,
            cardExpiryMonth: selectedMonth,
            cardExpiryYear: 20 + selectedYear,
            cardCVV: cvv,
            paymentType: AppConstants.PAYMENT_TYPE_CARD,
            card_save: 1
        }

        props.setOpenAddCardPopup(false)
        props.saveCardFromPopupAndProceed(params)
    }

    return (
        <>
            <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, backgroundColor: colors.yellow }} >

                {/* Header View */}
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    {/* Title */}
                    <Text style={{ marginLeft: 60, flex: 1, color: colors.black, fontSize: 22, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} > Debit / Credit Card</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ marginTop: 0, marginRight: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }} onPress={() => props.setOpenAddCardPopup(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Card Holder */}
                <View style={{ marginTop: 28, marginLeft: 20, marginRight: 20 }}>

                    {/* Title */}
                    <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Card Holder Name</Text>

                    {/* Input */}
                    <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                        placeholder='Enter card holder name'
                        placeholderTextColor={'#676741'}
                        onChangeText={(text) => setCardHolderName(text)}
                    >{cardHolderName}
                    </TextInput>
                </View>

                {/* Card Number */}
                <View style={{ marginTop: 16, marginLeft: 20, marginRight: 20 }}>

                    {/* Title */}
                    <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Card Number</Text>

                    {/* Input */}
                    <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                        placeholder='XXXX XXXX XXXX XXXX'
                        placeholderTextColor={'#676741'}
                        keyboardType={'numeric'}
                        maxLength={19}
                        onChangeText={(text) => handleCardNumberChange(text)}
                    >{cardNumber}
                    </TextInput>
                </View>

                <View style={{ marginTop: 16, marginLeft: 20, marginRight: 20, flexDirection: 'row', justifyContent: 'space-between' }}>

                    {/* Expiry Date */}
                    <View style={{ width: '48%' }}>

                        {/* Title */}
                        <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >Expiry (MM/YY)</Text>

                        {/* Input */}
                        <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                            placeholder='MM/YY'
                            placeholderTextColor={'#676741'}
                            keyboardType={'numeric'}
                            maxLength={5}
                            onChangeText={(text) => handleExpiryDateChange(text)}
                        >{expiryDate}
                        </TextInput>
                    </View>

                    {/* CVV */}
                    <View style={{ width: '48%' }}>

                        {/* Title */}
                        <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >CVV</Text>

                        {/* Input */}
                        <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                            placeholder='CVV'
                            placeholderTextColor={'#676741'}
                            keyboardType={'numeric'}
                            maxLength={3}
                            onChangeText={(text) => setCvv(text)}
                        >{cvv}
                        </TextInput>
                    </View>
                </View>

                {/* CVV Description */}
                <View style={{ marginTop: 24, flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }}>
                    <Image style={{ height: 20, width: 15, resizeMode: 'contain' }} source={require('../../assets/images/lock-icon.png')} />
                    <Text style={{ marginLeft: 10, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Your CVV will not be saved</Text>
                </View>


                {/* Save Card & Proceed */}
                <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, borderRadius: 23, backgroundColor: colors.black, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} activeOpacity={0.8} onPress={() => saveCard()}>

                    <Text style={{ marginLeft: 22, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >PROCEED</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                </TouchableOpacity>

            </View>
        </>
    )
}

export default AddCardPopup