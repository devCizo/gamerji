import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import * as generalSetting from '../webServices/generalSetting'

const AvatarItem = (props) => {

  const [isAvatarSelected, setIsAvatarSelected] = useState(false);

  // const setAvatarOnselection = () => {
  //   const txt = props.avatars.map(
  //     (i, arrIndex) => {
  //       if (arrIndex == props.index) {
  //         setIsAvatarSelected(!isAvatarSelected);
  //       }
  //       addLog('-----------------------')
  //       addLog('iiiiiiiiiii:::> ', i);
  //       addLog('arrayIndex:::> ', arrIndex);
  //       addLog('props.index:::> ', props.index);
  //       addLog('props.index:::> ', isAvatarSelected);
  //       addLog('-----------------------')
  //     }
  //   );
  //   return txt;
  // }

  return (
    <TouchableOpacity onPress={() => setIsAvatarSelected(!isAvatarSelected)} activeOpacity={0.5}>

      {/* Wrapper - Border */}
      <View style={{ height: 80, width: 80, borderRadius: 5, backgroundColor: (props.isAvatarSelected ? colors.yellow : colors.grey), marginRight: 20, justifyContent: 'center', alignItems: 'center' }}>

        {/* Image - Avatar */}
        <Image style={{ height: 75, width: 75, borderRadius: 5, resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />
      </View>
    </TouchableOpacity>
  )
}

export default AvatarItem;