import axios from '../../../webServices/axios-api';
import * as actionTypes from './requestActionTypes';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request */ }
export const request = (viewModel) => {
    return {
        type: actionTypes.REQUEST,
        model: viewModel
    };
};

{ /* Request Failure */ }
export const requestFailed = (viewModel) => {
    return {
        type: actionTypes.REQUEST_FAILED,
        model: viewModel
    };
};

{ /* Request API */ }
export const requestAPI = (subUrl, payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(subUrl, payload)
            .then((response) => {
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(request(response.data.item));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(requestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};