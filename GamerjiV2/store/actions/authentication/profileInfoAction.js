import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Avatar List */ }
export const avatarListForProfileInfo = (viewModel) => {
    return {
        type: AVATAR_LIST_PROFILE_INFO,
        model: viewModel
    };
};

{ /* Request Failure - Avatar List */ }
export const avatarListForProfileInfoFailed = (viewModel) => {
    return {
        type: AVATAR_LIST_PROFILE_INFO_FAILED,
        model: viewModel
    };
};

{ /* Request - Avatars */ }
export const requestAvatarsForProfileInfo = (payload) => {
    return (dispatch) => {

        addLog("URL:::> ", WebFields.PROFILE.MODE_AVATAR_LIST);
        addLog("Request:::> ", payload);

        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_FREE_AVATAR_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(avatarListForProfileInfo(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(avatarListForProfileInfoFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


// ---------------------------------------


{ /* Request - Banner List */ }
export const bannerListForProfileInfo = (viewModel) => {
    return {
        type: BANNER_LIST_PROFILE_INFO,
        model: viewModel
    };
};

{ /* Request Failure - Banner List */ }
export const bannerListForProfileInfoFailed = (viewModel) => {
    return {
        type: BANNER_LIST_PROFILE_INFO_FAILED,
        model: viewModel
    };
};

{ /* Request - Banners */ }
export const requestBannersForProfileInfo = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE.MODE_BANNER_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(bannerListForProfileInfo(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(bannerListForProfileInfoFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const applySignupCode = (viewModel) => {
    return {
        type: APPLY_SIGN_UP_CODE,
        model: viewModel
    };
};

export const applySignupCodeFailed = (viewModel) => {
    return {
        type: APPLY_SIGN_UP_CODE_FAILED,
        model: viewModel
    };
};

export const requestApplySignupCode = (payload) => {
    return (dispatch) => {
        addLog("API:::> ", WebFields.SIGN_UP_CODE.APPLY_SIGN_UP_CODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.SIGN_UP_CODE.APPLY_SIGN_UP_CODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(applySignupCode(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(applySignupCodeFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const AVATAR_LIST_PROFILE_INFO = 'AVATAR_LIST_PROFILE_INFO';
export const AVATAR_LIST_PROFILE_INFO_FAILED = 'AVATAR_LIST_PROFILE_INFO_FAILED';
export const BANNER_LIST_PROFILE_INFO = 'BANNER_LIST_PROFILE_INFO';
export const BANNER_LIST_PROFILE_INFO_FAILED = 'BANNER_LIST_PROFILE_INFO_FAILED';
export const APPLY_SIGN_UP_CODE = 'APPLY_SIGN_UP_CODE';
export const APPLY_SIGN_UP_CODE_FAILED = 'APPLY_SIGN_UP_CODE_FAILED';



export const resetProfileInfoState = () => {
    return {
        type: RESET_PROFILE_INFO_STATE,
    };
};

export const RESET_PROFILE_INFO_STATE = 'RESET_PROFILE_INFO_STATE';
