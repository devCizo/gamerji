import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';

// =======================

{ /* Request - Search Users List */ }
export const levelList = (viewModel) => {
    return {
        type: LEVEL_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Search Users List */ }
export const levelListFailed = (viewModel) => {
    return {
        type: LEVEL_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestLevelList = (payload) => {

    // addLog("searchUserList payload :::> ", payload);

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.LEVEL_LIST.MODE, payload)
            .then((response) => {
                //       addLog("searchUserList:::> ", response.data);
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(levelList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(levelListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetLevelListState = () => {
    return {
        type: RESET_LEVEL_LIST_STATE,
    };
};

export const LEVEL_LIST = 'LEVEL_LIST';
export const LEVEL_LIST_FAILED = 'LEVEL_LIST_FAILED';
export const RESET_LEVEL_LIST_STATE = 'RESET_LEVEL_LIST_STATE';