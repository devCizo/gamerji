import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native'
import { containerStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, fontStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import colors from '../../assets/colors/colors'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import { saveDataToLocalStorage } from '../../appUtils/sessionManager';
import { AppConstants } from '../../appUtils/appConstants';
import CustomProgressbar from '../../appUtils/customProgressBar';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const ChangeUserName = (props) => {

    const dispatch = useDispatch();
    let game = props.game
    const [newGameName, setGameName] = useState('')
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        return () => {
            dispatch(actionCreators.resetChangeUserNameState())
        }
    }, [])

    let findUserName = () => {
        if (global.profile.gameNames) {
            let index = global.profile.gameNames.findIndex(obj => obj.game == game._id)
            addLog("index==", index);
            if (index > -1) {
                return global.profile.gameNames[index].uniqueIGN || ""
            } else {
                return "NA"
            }
        }
    }

    let findUserNameIndexFromGameNames = () => {
        if (global.profile.gameNames) {
            let index = global.profile.gameNames.findIndex(obj => obj.game == game._id)
            return index
        }
    }

    let submitData = () => {
        if (newGameName == '') {
            showErrorToastMessage('Please enter username')
        } else {



            var gameNames = global.profile.gameNames.map(item => {
                let tempItem = { ...item };
                return tempItem;
            })

            if (!findUserName()) {
                gameNames.push({
                    game: game._id,
                    uniqueIGN: newGameName
                })
            } else {
                let index = findUserNameIndexFromGameNames()
                if (gameNames[index]) {
                    gameNames[index]['uniqueIGN'] = newGameName
                }
            }

            let payload = {
                gameNames: gameNames
            }
            setLoading(true)
            dispatch(actionCreators.requestChangeUserName(payload))
        }
    }
    addLog("findUserName()", findUserName());
    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.changeUserName.model }),
        shallowEqual
    );
    var { changeUserNameResponse } = currentState

    useEffect(() => {
        if (changeUserNameResponse) {
            setLoading(false)
            if (changeUserNameResponse.success == true && changeUserNameResponse.item) {
                var profile = changeUserNameResponse.item
                global.profile = profile

                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(profile))
                props.closeUserNamePopup()
                showSuccessToastMessage('Username is changed successfully')
            } else {
                if (changeUserNameResponse.errors && changeUserNameResponse.errors[0] && changeUserNameResponse.errors[0].msg) {
                    showErrorToastMessage(changeUserNameResponse.errors[0].msg)
                }
            }
        }
        changeUserNameResponse = undefined
    }, [changeUserNameResponse])

    return (
        <View>

            {/* Top curved header */}
            <View style={{ height: 65, borderTopStartRadius: 50, borderTopEndRadius: 50, backgroundColor: '#FFC609', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>

                <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, marginLeft: 50, marginRight: 50, textAlign: 'center' }}>Add / Change {game && game.name} Username</Text>

                {/* Close Button */}
                <TouchableOpacity style={{ height: 65, aspectRatio: 1, justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 0 }} onPress={() =>
                    props.closeUserNamePopup()
                } activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>

            {/* Input Container */}
            <View style={{ backgroundColor: 'white', }}>

                {/* Currnent Username */}
                {findUserName() != "" ?
                    <View style={{ marginTop: 20, height: 70 }}>
                        <Text style={{ marginLeft: 20, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>CURRENT {game && game.name} USERNAME</Text>

                        <TextInput
                            style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 46, borderRadius: 23, color: colors.black, borderWidth: 1, borderColor: colors.border_color, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold, backgroundColor: '#E8E9EB' }}
                            placeholder='Enter current Username'
                            editable={false}
                            maxLength={25}
                        >
                            {findUserName()}
                        </TextInput>
                    </View>
                    : <View></View>}

                {/* New Username */}
                <View style={{ marginTop: 20 }}>
                    <Text style={{ marginLeft: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }}>NEW {game && game.name} USERNAME</Text>
                    <TextInput
                        style={{ marginTop: 8, marginLeft: 20, marginRight: 20, height: 46, borderRadius: 23, color: colors.black, borderWidth: 1, borderColor: colors.border_color, paddingHorizontal: 15, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }}
                        placeholder='Enter new Username'
                        onChangeText={text => setGameName(text)}
                        spellCheck={false}
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        maxLength={25}
                    >
                    </TextInput>
                </View>

                {/* Note Text */}
                <Text style={{ marginTop: 20, marginLeft: 20, marginRight: 20, color: colors.red, fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }}>NOTE : You can change {game && game.name} Username one time only.</Text>

                {/* My Contests View */}
                <TouchableOpacity style={{ marginTop: 22, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, borderRadius: 23, backgroundColor: colors.black, flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', fontFamily: fontFamilyStyleNew.regular }} onPress={() => submitData()} activeOpacity={0.5} >
                    <Text style={{ color: colors.white, fontWeight: 'bold', fontSize: 16, paddingLeft: 22 }}>SUBMIT</Text>
                    <Image style={{ position: 'absolute', right: 22, width: 16, height: 14 }} source={require('../../assets/images/right_arrow.png')}></Image>
                </TouchableOpacity>

                {checkIsSponsorAdsEnabled('addChangeUsernamePopup') &&
                    <SponsorBannerAds screenCode={'addChangeUsernamePopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('addChangeUsernamePopup')} />
                }
            </View>
            {isLoading && <CustomProgressbar />}
        </View>
    )
}


export default ChangeUserName;
