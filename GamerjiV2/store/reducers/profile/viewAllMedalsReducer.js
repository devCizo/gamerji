import produce from 'immer';
import * as actionTypes from '../../actions/profile/viewAllMedalsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        viewAllMedalsListResponse: undefined,
    },
    error: false
};

{ /* Request - View All Medal List */ }
const viewAllMedalList = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.viewAllMedalsListResponse = action.model;
    });
};

{ /* Request Failure - View All Medal List */ }
const viewAllMedalListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.viewAllMedalsListResponse = action.model;
    });
};

{ /* Reset State - View All Medal List */ }
const resetViewAllMedalsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.viewAllMedalsListResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.VIEW_ALL_MEDAL_LIST:
            return viewAllMedalList(state, action);
        case actionTypes.VIEW_ALL_MEDAL_LIST_FAILED:
            return viewAllMedalListFailed(state, action);
        case actionTypes.RESET_VIEW_ALL_MEDALS_LIST_STATE:
            return resetViewAllMedalsState(state, action);
        default:
            return state;
    }
};

export default reducer;