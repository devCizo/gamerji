import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, TextInput, Image, Dimensions, StyleSheet, ScrollView, FlatList, Keyboard } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import ModalDropdown from 'react-native-modal-dropdown';
import moment from 'moment';
import RBSheet from "react-native-raw-bottom-sheet";
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import * as generalSetting from '../../webServices/generalSetting';
import CountryJSON from '../Authentication/countries.json';
import CustomDatePicker from '../../appUtils/customDatePicker';
import Modal from 'react-native-modal';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { isIndianUser, getUserCountry } from '../../appUtils/globleMethodsForUser';
import StateListPopup from '../../appUtils/stateListPopup'
import * as RootNavigation from '../../appUtils/rootNavigation.js';


const EditProfile = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const data = props.route.params;

    const [dob, setDOB] = useState(Constants.validation_select_date_of_birth);
    const [isOpenDatePickerPopup, setIsOpenDatePickerPopup] = useState(false);
    const [lockDateOfBirthField, setLockDateOfBirthField] = useState(false)

    const [stateList, setStateList] = useState([])
    const [selectedState, setSelectedState] = useState(undefined)
    const [isOpenStateListPicker, setOpenStateListPicker] = useState(false)

    const [email, setEmail] = useState('')
    const [gamerjiName, setGamerjiName] = useState('')
    const [avatars, setAvatars] = useState(undefined);
    const [avatarCounts, setAvatarCounts] = useState(undefined);

    const [avatarId, setAvatarId] = useState('')
    const [countryFlag, setCountryFlag] = useState(undefined)
    const [isLoading, setLoading] = useState(false);

    // This function should call the Country Data Json to fetch the Flag image
    useEffect(() => {

        if (global.profile) {

            if (global.profile.dateOfBirth) {
                setDOB(moment(global.profile.dateOfBirth).format('DD/MM/YYYY'))
            }

            if (global.profile1.state._id) {
                setSelectedState(global.profile1.state)
            }

            if (global.profile.email) {
                setEmail(global.profile.email)
            }
            if (global.profile.gamerjiName) {
                setGamerjiName(global.profile.gamerjiName)
            }
        }
    }, []);


    // This Use-Effect function should call the Avatars, Banners API
    useEffect(() => {
        // Featured Videos API Params
        let payload = {
            skip: 0,
            limit: 10
        }
        setLoading(true)
        dispatch(actionCreators.requestAvatars(payload))
        // dispatch(actionCreators.requestProfileBanners(payload))

        return () => {
            dispatch(actionCreators.resetEditProfileState())
            dispatch(actionCreators.resetProfileInfoState())
        }
    }, [])

    // This function should set the API Response to Model
    const { currentStateProfile } = useSelector(
        (state) => ({ currentStateProfile: state.editProfile.model }),
        shallowEqual
    );
    var { avatarsListResponse, profileBannersListResponse, editProfileResponse } = currentStateProfile

    // This function should return the Avatars API Response
    useEffect(() => {
        if (avatarsListResponse && avatarsListResponse.list) {
            setLoading(false)
            setAvatarCounts(avatarsListResponse.count)

            var tempArr = avatarsListResponse.list.map(item => {
                let tempItem = { ...item };
                tempItem.isSelected = false;
                return tempItem;
            })

            if (global.profile.avatar && global.profile.avatar._id) {
                setAvatarId(global.profile.avatar._id);
            } else {
                setAvatarId(avatarsListResponse.list[0]._id);
            }
            setAvatars(tempArr)
        }
        avatarsListResponse = undefined
    }, [avatarsListResponse])

    // This function should return the Profile Banners API Response

    // This function should set the Avatar Id if selected 
    const getAvatarsData = (index) => {
        var tempContestList = [...avatars]
        tempContestList[index].isSelected = !tempContestList[index].isSelected
        setAvatarId(tempContestList[index]._id);
    }

    // This function should set the Banner Id if selected 

    // This function should check the validation first and then call the Update Profile API
    const checkValidation = () => {

        Keyboard.dismiss();
        if (dob == Constants.validation_select_date_of_birth) {
            showErrorToastMessage(Constants.error_select_date_of_birth)
            return;
        } else if (!selectedState) {
            showErrorToastMessage(Constants.error_select_state)
            return;
        } else if (email == '') {
            showErrorToastMessage('Please enter email address')
            return
        }
        callToUpdateProfileAPI(props);
    };

    // This function should call the Update Profile API
    const callToUpdateProfileAPI = () => {
        let payload = {
            gamerjiName: gamerjiName,
            email: email,
            phoneCode: global.profile.phoneCode,
            phone: global.profile.phone,
            dateOfBirth: dob,
            address: {
                state: selectedState._id
            },
            avatar: avatarId
            // userBanner: bannerId
        }
        addLog('Payload:::> ', payload);
        dispatch(actionCreators.requestEditProfile(payload))
    }

    // This function should return the Profile Banners API Response
    useEffect(() => {
        if (editProfileResponse) {
            setLoading(false)
            showSuccessToastMessage(Constants.success_profile_updated);
            //RootNavigation.navigate(Constants.nav_profile_screen)

            props.navigation.setParams(editProfileResponse);
            props.navigation.goBack();
            data.setAccountsData();
        }
        editProfileResponse = undefined
    }, [editProfileResponse])

    useEffect(() => {

        // API Call
        dispatch(actionCreators.requestStateList({
            skip: 0,
            limit: 100, sortBy: 'name', sort: 'asc'
        }))

        return () => {
            addLog('Unmount')
            dispatch(actionCreators.resetDobStateValidationState())
        }

    }, []);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.dobStateValidation.model }),
        shallowEqual
    );
    var { stateListResponse } = currentState

    useEffect(() => {
        if (stateListResponse) {
            setLoading(false)
            if (stateListResponse.list) {
                setStateList(stateListResponse.list)
            }
        }
    }, [stateListResponse]);

    // This function should open up the Date Picker popup
    let openDatePickerPopup = (visible) => {
        setLockDateOfBirthField(true);
        setIsOpenDatePickerPopup(visible)
    }

    const setDOBFromPicker = (dobPicker) => {
        setDOB(moment(dobPicker).format('DD/MM/YYYY'))
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5} >

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Edit Profile Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_edit_profile} </Text>
                </View>

                <ScrollView style={{ flexDirection: 'column', marginBottom: 10 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyboardShouldPersistTaps='always'>

                    {/* Wrapper - Body Content */}
                    <View style={[positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, borderStyles.borderBottomLeftRadius_40, borderStyles.borderBottomRightRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor]}>

                        {/* Wrapper - Mobile No */}
                        <View style={[flexDirectionStyles.row]}>

                            {/* Text - Country Code Header */}
                            <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14]}> {Constants.text_country_code} </Text>

                            {/* Text - Mobile Number Header */}
                            <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_16]}> {Constants.text_mobile_no} </Text>
                        </View>

                        {/* Wrapper - Input Texts */}
                        <View style={[flexDirectionStyles.row, positionStyles.relative, marginStyles.topMargin_7]}>

                            {/* Wrapper - Input Text Country Code */}
                            {getUserCountry() &&
                                <View style={[flexDirectionStyles.row, heightStyles.height_46, widthStyles.width_100, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.lightSilverBackgroundColor]}>

                                    {/* Image - Indian Flag */}

                                    <Image style={[heightStyles.height_15, widthStyles.width_24]} source={getUserCountry().flag && getUserCountry().flag.default && { uri: generalSetting.UPLOADED_FILE_URL + getUserCountry().flag.default }} />
                                    {/* <Image style={[heightStyles.height_15, widthStyles.width_24]} source={{ uri: countryFlag }} /> */}

                                    {/* Input Text - Country Code (+91) */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, alignmentStyles.alignTextCenter, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_16, marginStyles.leftMargin_4]}> {getUserCountry().dialingCode} </Text>
                                </View>
                            }

                            {/* Wrapper - Mobile No */}
                            <View style={[containerStyles.container, flexDirectionStyles.row, heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, colorStyles.lightSilverBackgroundColor, marginStyles.leftMargin_10]}>

                                {/* Text Input - Mobile No */}
                                <TextInput style={[fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_15, paddingStyles.leftPadding_15, marginStyles.rightMargin_35]}
                                    autoCorrect={false}
                                    editable={false}> {global.profile.phone} </TextInput>

                                {/* Image - Verified */}
                                {global.profile.isMobileVerified &&
                                    <Image style={{ position: 'absolute', right: 15, height: 20, width: 20, alignSelf: 'center' }} source={require('../../assets/images/ic_phone_verified.png')} />
                                }
                            </View>
                        </View>

                        {/* Date of Birth */}
                        <View style={{ marginTop: 22 }}>

                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>{Constants.text_date_of_birth}</Text>

                            {dob == '' || dob == null ?
                                <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                    onPress={() => openDatePickerPopup(true)}
                                    activeOpacity={0.5} >

                                    <Text
                                        style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                        {moment(dob).format('DD/MM/YYYY')}
                                    </Text>

                                    <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />
                                </TouchableOpacity>
                                :
                                <View style={{ marginTop: 7, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: '#E8E9EB' }}>
                                    <Text
                                        style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                        {moment(dob).format('DD/MM/YYYY')}
                                    </Text>

                                    <Image style={{ marginRight: 15, height: 20, width: 20 }} source={require('../../assets/images/dob_icon.png')} />
                                </View>
                            }
                        </View>

                        {/* Wrapper - State */}
                        {isIndianUser() ?
                            <View style={{ marginTop: 22 }}>

                                <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>{Constants.text_state}</Text>

                                {!selectedState ?
                                    <TouchableOpacity style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}
                                        onPress={() => setOpenStateListPicker(true)}
                                        activeOpacity={0.5} >

                                        <Text
                                            style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                            Select your state
                                        </Text>

                                        <Image style={{ marginRight: 15, height: 12, width: 12, resizeMode: 'contain' }} source={require('../../assets/images/drop_down_arrow_state.png')} />

                                    </TouchableOpacity>
                                    :
                                    <View style={{ marginTop: 7, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: '#E8E9EB' }}>
                                        <Text
                                            style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                            {selectedState.name}
                                        </Text>
                                    </View>
                                }
                            </View>
                            :
                            // Country
                            <View style={{ marginTop: 22 }}>

                                <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Country</Text>

                                <View style={{ marginTop: 7, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: '#E8E9EB' }}>
                                    <Text
                                        style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                        {getUserCountry().name}
                                    </Text>
                                </View>
                            </View>
                        }

                        {/* Text - Email */}
                        <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_22]}> {Constants.text_email}</Text>

                        {/* Wrapper - Email */}
                        {global.profile.email == '' || global.profile.email == null ?
                            <View style={[flexDirectionStyles.row, heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, marginStyles.topMargin_7]}>

                                {/* Text Input - Email */}
                                <TextInput style={[widthStyles.width_100p, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15, marginStyles.rightMargin_35, paddingStyles.leftPadding_20]}
                                    placeholder={Constants.validation_email}
                                    keyboardType={'email-address'}
                                    autoCorrect={false}
                                    value={email}
                                    onChangeText={text => setEmail(text)}
                                    autoCapitalize='none'
                                />
                            </View>
                            :
                            <View style={[flexDirectionStyles.row, heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, colorStyles.lightSilverBackgroundColor, marginStyles.topMargin_7, alignmentStyles.alignItemsCenter]}>

                                <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                    {global.profile.email}
                                </Text>

                                {/* Image - Verified */}
                                {global.profile.isEmailVerified &&
                                    <Image style={{ position: 'absolute', right: 15, height: 20, width: 20, alignSelf: 'center' }} source={require('../../assets/images/ic_phone_verified.png')} />
                                }
                            </View>
                        }

                        {/* Gamerji User Name */}
                        <View style={{ marginTop: 22 }}>

                            {/* Title */}
                            <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>Gamerji Username</Text>

                            {/* User Name */}
                            <View style={{ marginTop: 7, height: 46, borderRadius: 23, borderWidth: 1, borderColor: '#D5D7E3', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
                                <TextInput style={{ width: "100%", flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}
                                    placeholder={Constants.validation_gamerjiname}
                                    keyboardType={'email-address'}
                                    autoCorrect={false}
                                    value={gamerjiName}
                                    maxLength={30}
                                    onChangeText={text => setGamerjiName(text.trim())}
                                    autoCapitalize='none'
                                />
                                {/* <Text
                                    style={{ flex: 1, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16, paddingHorizontal: 15, color: '#70717A' }}>
                                    {global.profile && global.profile.gamerjiName}
                                </Text> */}
                            </View>
                        </View>

                    </View>

                    {/* Wrapper - Avatar & Banner View */}
                    <View style={[flexDirectionStyles.column, marginStyles.topMargin_10, alignmentStyles.alignItemsCenter, marginStyles.bottomMargin_10]}>

                        {/* Text - Avatar */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.topMargin_10]}> {Constants.text_avatar} </Text>

                        {/* Text - Avatar Separator */}
                        <Text style={[heightStyles.height_2, widthStyles.width_30_new, marginStyles.topMargin_4, marginStyles.bottomMargin_4, colorStyles.yellowBackgroundColor, alignmentStyles.alignSelfCenter]} />

                        {/* Wrapper - Avatar Carousel */}
                        <View style={[flexDirectionStyles.row, justifyContentStyles.spaceBetween, marginStyles.topMargin_10, { marginLeft: 15, marginRight: avatarCounts >= 3 ? 15 : 0, justifyContent: 'center' }]}>


                            {/* Flatlist - Avatar Lists */}
                            <FlatList
                                data={avatars}
                                renderItem={({ item, index }) => <AvatarItem item={item} counts={avatars.length} aId={avatarId} index={index} getAvatarsData={getAvatarsData} />}
                                keyExtractor={(item) => item.id}
                                horizontal={true}
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false} />
                        </View>
                    </View>



                    {/* TouchableOpacity - Save Button Click Event */}
                    <TouchableOpacity style={[heightStyles.height_46, flexDirectionStyles.row, colorStyles.redBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, marginStyles.leftMargin_20, marginStyles.rightMargin_20, marginStyles.bottomMargin_20, marginStyles.topMargin_40]}
                        onPress={() => checkValidation()} activeOpacity={0.5}>

                        {/* Text - Button (Save) */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_save}</Text>

                        {/* Image - Right Arrow */}
                        <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                    </TouchableOpacity>
                </ScrollView>

                {checkIsSponsorAdsEnabled('editProfile') &&
                    <SponsorBannerAds screenCode={'editProfile'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('editProfile')} />
                }
            </View>

            <Modal
                isVisible={isOpenDatePickerPopup}
                coverScreen={true}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <CustomDatePicker setDOBFromPicker={setDOBFromPicker} openDatePickerPopup={openDatePickerPopup} mode='date' />
            </Modal>

            {isOpenStateListPicker &&
                <Modal
                    isVisible={isOpenStateListPicker}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <StateListPopup stateList={stateList} openStateListPicker={setOpenStateListPicker} setSelectedState={setSelectedState} />
                </Modal>
            }

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

function AvatarItem(props) {

    // Variable Declarations
    var item = props.item

    // This function should set the avatar values 
    const handleAvatarSelection = (index) => {
        props.getAvatarsData(index);
    }

    return (
        <TouchableOpacity onPress={() => handleAvatarSelection(props.index)} activeOpacity={0.5}>

            {/* Wrapper - Border */}
            <View style={{ height: 260, width: 140, borderRadius: 5, borderWidth: 2, borderColor: (props.aId == item._id ? colors.yellow : colors.black), marginRight: 20, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5} >
                {props.aId == item._id &&
                    <Image style={{ position: 'absolute', top: 5, right: 7, height: 16, width: 16 }} source={require('../../assets/images/check_yellow_bg.png')} />
                }
                {/* Image - Avatar */}
                <Image style={{ height: 240, width: 120, borderRadius: 5, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />

            </View>
            <View style={{ width: 120, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5} >

                <Text style={{ color: colors.white, flexWrap: 'wrap', flex: 0.3, alignItems: 'center', justifyContent: 'center', flexDirection: 'column', }}>{props.item.name}</Text>
            </View>
        </TouchableOpacity>
    )
}

function FrameItem(props) {

    // Variable Declarations
    var item = props.item

    // This function should set the Frame values 
    const handleFrameSelection = (index) => {
        props.getFramesData(index);
    }

    return (
        <TouchableOpacity onPress={() => handleFrameSelection(props.index)} activeOpacity={0.5} >

            {/* Wrapper - Border */}
            <View style={{ height: 66, width: 138, borderRadius: 5, backgroundColor: (props.bId == item._id ? colors.yellow : colors.grey), marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>

                {/* Image - Frame */}
                <Image style={{ height: 60, width: 132, borderRadius: 5, resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center'
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
    },
    submitButton: {
        height: 46,
        position: 'absolute',
        left: 20,
        right: 20,
        bottom: 20,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dateHeaderContainer: {
        height: 45,
        borderBottomWidth: 1,
        borderColor: "#ccc",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    dateHeaderButton: {
        height: "100%",
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    dateHeaderButtonCancel: {
        fontSize: 18,
        color: "#666",
        fontWeight: "400"
    },
    dateHeaderButtonDone: {
        fontSize: 18,
        color: "#006BFF",
        fontWeight: "500"
    },


    // Country Picker Styles
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    titleText: {
        color: '#000',
        fontSize: 25,
        marginBottom: 25,
        fontWeight: 'bold',
    },
    pickerTitleStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'center',
        fontWeight: 'bold',
        flex: 1,
        marginLeft: 10,
        fontSize: 16,
        color: '#000',
        paddingTop: Platform.OS === 'ios' ? 20 : 0
    },
    pickerStyle: {
        height: 60,
        width: 250,
        marginBottom: 10,
        justifyContent: 'center',
        padding: 10,
        borderWidth: 2,
        borderColor: '#303030',
        backgroundColor: 'white',
    },
    selectedCountryTextStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        color: '#000',
        textAlign: 'right',
    },
    countryNameTextStyle: {
        paddingLeft: 10,
        color: '#000',
        textAlign: 'right',
    },
    searchBarStyle: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        marginLeft: 8,
        marginRight: 10,
    }
})

export default EditProfile;