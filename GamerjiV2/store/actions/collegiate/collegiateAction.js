import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const collegeList = (viewModel) => {
    return {
        type: COLLEGE_LIST,
        model: viewModel
    };
};

export const collegeListFailed = (viewModel) => {
    return {
        type: COLLEGE_LIST_FAILED,
        model: viewModel
    };
};

export const requestCollegeList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.COLLEGIATE.COLLEGE_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(collegeList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(collegeListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const COLLEGE_LIST = 'COLLEGE_LIST';
export const COLLEGE_LIST_FAILED = 'COLLEGE_LIST_FAILED';



export const submitCollege = (viewModel) => {
    return {
        type: SUBMIT_COLLEGE,
        model: viewModel
    };
};

export const submitCollegeFailed = (viewModel) => {
    return {
        type: SUBMIT_COLLEGE_FAILED,
        model: viewModel
    };
};

export const requestSubmitCollege = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(submitCollege(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(submitCollegeFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const SUBMIT_COLLEGE = 'SUBMIT_COLLEGE';
export const SUBMIT_COLLEGE_FAILED = 'SUBMIT_COLLEGE_FAILED';



export const resetCollegiateState = () => {
    return {
        type: RESET_COLLEGIATE_STATE,
    };
};
export const RESET_COLLEGIATE_STATE = 'RESET_COLLEGIATE_STATE';