import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const tournamentList = (viewModel) => {
    return {
        type: TOURNAMENT_LIST,
        model: viewModel
    };
};

export const tournamentListFailed = (viewModel) => {
    return {
        type: TOURNAMENT_LIST_FAILED,
        model: viewModel
    };
};

export const requestTournamentList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.TOURNAMENT.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(tournamentList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(tournamentListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const walletUsageLimitForTournament = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_TOURNAMENT,
        model: viewModel
    };
};

export const walletUsageLimitForTournamentFailed = (viewModel) => {
    return {
        type: WALLET_USAGE_LIMIT_FOR_TOURNAMENT_FAILED,
        model: viewModel
    };
};

export const requestWalletUsageLimitForTournament = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.GET_WALLET_USAGE_LIMIT.MODE);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.GET_WALLET_USAGE_LIMIT.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(walletUsageLimitForTournament(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(walletUsageLimitForTournamentFailed(error.response.data));
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetTounamentListState = () => {
    return {
        type: RESET_TOURNAMENT_LIST_STATE,
    };
};

export const TOURNAMENT_LIST = 'TOURNAMENT_LIST';
export const TOURNAMENT_LIST_FAILED = 'TOURNAMENT_LIST_FAILED';
export const WALLET_USAGE_LIMIT_FOR_TOURNAMENT = 'WALLET_USAGE_FOR_TOURNAMENT_LIMIT';
export const WALLET_USAGE_LIMIT_FOR_TOURNAMENT_FAILED = 'WALLET_USAGE_LIMIT_FOR_TOURNAMENT_FAILED';
export const RESET_TOURNAMENT_LIST_STATE = 'RESET_TOURNAMENT_LIST_STATE';