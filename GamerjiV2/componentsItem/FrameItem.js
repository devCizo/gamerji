import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import frameData from '../assets/data/frameData';
import * as generalSetting from '../webServices/generalSetting'

const FrameItem = (props) => {

  const [isFrameSelected, setIsFrameSelected] = useState(false);

  const setFrameOnselection = () => {
    const txt = frameData.map(
      (i, arrIndex) => {
        if (arrIndex == props.index) {
          setIsFrameSelected(!isFrameSelected);
        }
        addLog('-----------------------')
        addLog('iiiiiiiiiii:::> ', i);
        addLog('arrayIndex:::> ', arrIndex);
        addLog('props.index:::> ', props.index);
        addLog('props.index:::> ', isFrameSelected);
        addLog('-----------------------')
      }
    );
    return txt;
  }

  return (
    <TouchableOpacity onPress={() => setFrameOnselection()} activeOpacity={0.5}>

      {/* Wrapper - Border */}
      <View style={{ height: 66, width: 138, borderRadius: 5, backgroundColor: (isFrameSelected ? colors.yellow : colors.grey), marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>

        {/* Image - Frame */}
        <Image style={{ height: 60, width: 132, borderRadius: 5, resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.img.default }} />
      </View>
    </TouchableOpacity>
  )
}

export default FrameItem;