import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { positionStyles, heightStyles, flexDirectionStyles, alignmentStyles, fontFamilyStyles, colorStyles, marginStyles, fontSizeStyles } from '../appUtils/commonStyles';
import RightArrowWhiteImage from '../assets/images/ic_right_arrow_black.svg';
import { Constants } from '../appUtils/constants';
import { addLog } from '../appUtils/commonUtlis';
import { AppConstants } from '../appUtils/appConstants';

const NetBankingItem = (props) => {

  // This function should redirected to the Payment Gateway Screen
  const redirectToPaymentGatewayScreen = () => {

    addLog("Payment Type:::> " + props.paymentType);
    var params = {
      paymentType: props.paymentType,
      bankId: props.item.id,
    }

    props.moveToPaymentGateway(params)
  }

  return (
    <View>

      {/* TouchableOpacity - All Games Click Event */}
      <TouchableOpacity style={[flexDirectionStyles.column]} onPress={() => redirectToPaymentGatewayScreen()} activeOpacity={0.5}>

        {/* Wrapper - Bank Data */}
        <View style={[flexDirectionStyles.row]}>

          {/* Text - Bank Name */}
          <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_035]}>{props.item.name}</Text>

          {/* Image - Right Arrow */}
          <RightArrowWhiteImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter]} />
        </View>
      </TouchableOpacity>

      {/* Text - Separator */}
      <Text style={[heightStyles.height_001, colorStyles.silverBackgroundColor, marginStyles.topMargin_01, marginStyles.bottomMargin_01]} />
    </View>
  )
}

export default NetBankingItem;