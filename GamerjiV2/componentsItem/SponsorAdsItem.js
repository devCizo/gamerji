import React, { useEffect, useState, } from 'react'
import { useDispatch } from "react-redux";
import { View, Image, TouchableOpacity, Linking } from 'react-native'
import { addLog } from '../appUtils/commonUtlis'
import * as actionCreators from '../store/actions/index';
import * as generalSetting from '../webServices/generalSetting'
import { BannerView } from 'react-native-fbads';
import colors from '../assets/colors/colors';
import { AppConstants } from '../appUtils/appConstants';
//import io from 'socket.io-client';

const SponsorAdsItem = (props) => {

    // Variable Declarations
    //  const socket = io(generalSetting.SOCKET_URL)
    const dispatch = useDispatch();
    const sendAdsAnalytics = () => {
        Linking.openURL(props.item.url)

        var params = {
            type: AppConstants.TYPE_SPONSOR_ADS_CLICK,
            user: global.profile && global.profile._id,
            screen: props.checkIsSponsorAdsEnabled && props.checkIsSponsorAdsEnabled._id,
            sponsorAd: props.item._id
        }
        console.log('Params:::> ', params)
        dispatch(actionCreators.requestSponsorAdsLog(params));
        //  socket.emit("gsaLog", params)
    }

    return (
        <View>
            {!props.item.isFbAds ?
                <TouchableOpacity onPress={() => sendAdsAnalytics()}>
                    <Image style={{ height: 60, width: '100%', resizeMode: 'cover' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + props.item.banner.default }} />
                </TouchableOpacity>
                :
                <View>
                    <BannerView
                        placementId={AppConstants.FB_BANNER_ADS}
                        type="standard"
                        onPress={() => console.log('click')}
                        onLoad={() => console.log('loaded')}
                        onError={(err) => console.log('error', err)}
                    />
                </View>
            }
        </View >
    )
}

export default SponsorAdsItem;