import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, FlatList, TouchableOpacity, Alert, Image, ScrollView } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import moreMenuData from '../../assets/data/moreMenuData';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import { clearDataFromLocalStorage } from '../../appUtils/sessionManager';
import colors from '../../assets/colors/colors';
import MoreMenuItem from '../../componentsItem/MoreMenuItem';
import { SafeAreaView } from 'react-navigation';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { StackActions, useNavigation } from '@react-navigation/native';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import BottomSheetApplyPromoCode from '../../components/More/BottomSheetApplyPromoCode';
import BottomSheetJoinViaInviteCode from '../../components/More/BottomSheetJoinViaInviteCode';
import Modal from 'react-native-modal';

const More = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [videos, setVideos] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [videoCounts, setVideoCounts] = useState('');
    const [status, setStatus] = useState('');
    const [youTubeChannelLink, setYouTubeChannelLink] = useState('');
    const [id, setId] = useState('');
    const useNav = useNavigation();

    const [isOpenApplyPromoCodePopup, setOpenApplyPromoCodePopup] = useState(false);
    const [isOpenJoinViaInviteCodePopup, setOpenJoinViaInviteCodePopup] = useState(false);

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        checkStreamerStatus()

        return () => {
            dispatch(actionCreators.resetVideoRequestsListState())
        }
    }, []);

    // This function calls the API when coming to this screen for the first time
    // Also calls when coming from the Stream on Gamerji Form
    function checkStreamerStatus() {
        let payload = {
            skip: 0,
            limit: 10,
        }
        dispatch(actionCreators.requestVideoRequests(payload))
    }

    // This function sets the Video Requests Code Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.more.model }),
        shallowEqual
    );
    var { videoRequestsResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (videoRequestsResponse) {
            setLoading(false)
            if (videoRequestsResponse.list) {
                setVideos(videoRequestsResponse.list)
                setVideoCounts(videoRequestsResponse.count)
                setId(videoRequestsResponse.list[0] && videoRequestsResponse.list[0]._id);
                setStatus(videoRequestsResponse.list[0] && videoRequestsResponse.list[0].status);
                setYouTubeChannelLink(videoRequestsResponse.list[0] && videoRequestsResponse.list[0].youTubeChannelLink);
                addLog('YouTube Channel Link:::> ', videoRequestsResponse.list[0] && videoRequestsResponse.list[0].youTubeChannelLink);
            }
        }
        videoRequestsResponse = undefined
    }, [videoRequestsResponse])

    // This Use-Effect function should set the Videos Data
    // useEffect(() => {
    //     if (props.isSucess && checkAPICall === 'videoRequests') {
    //         setCheckAPICall(undefined);
    //         setVideos(props.list.videoRequests && props.list.videoRequests.list)
    //         setVideoCounts(props.list.videoRequests && props.list.videoRequests.count);
    //         setId(props.list.videoRequests && props.list.videoRequests.list && props.list.videoRequests.list[0] && props.list.videoRequests.list[0]._id);
    //         setStatus(props.list.videoRequests && props.list.videoRequests.list && props.list.videoRequests.list[0] && props.list.videoRequests.list[0].status && props.list.videoRequests.list[0].status);
    //     }
    // }, [props]);

    // This function should call the logout alert and then call the logoutUserFromApp function
    const callLogoutAlert = () =>
        Alert.alert(
            Constants.action_logout,
            Constants.alert_logout_message, // <- this part is optional, you can pass an empty string
            [
                { text: Constants.action_cancel, onPress: () => null },
                { text: Constants.action_ok, onPress: () => logoutUserFromApp() },
            ],
            { cancelable: false },
        );

    // This function should clear all the keys saved into the local database and then redirected to the Login Screen.
    const logoutUserFromApp = () => {
        setLoading(true);
        (async () => {
            await clearDataFromLocalStorage();
        })();

        // useNav.dispatch(StackActions.popToTop())
        dispatch(actionCreators.logoutUser())
        setTimeout(function () { props.navigation.replace(Constants.nav_login); }, 500);


        // props.navigation.navigate(Constants.nav_login);
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Top View */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* Text - More Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.nav_more} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderRadius_40, paddingStyles.padding_20, colorStyles.whiteBackgroundColor, marginStyles.bottomMargin_10]}>

                    {/* ScrollView */}
                    <ScrollView style={marginStyles.bottomMargin_10}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Menu Lists */}
                        <FlatList
                            data={moreMenuData}
                            renderItem={({ item, index }) => <MoreMenuItem item={item} index={index} navigation={props.navigation} count={videoCounts} status={status} id={id} youTubeChannelLink={youTubeChannelLink} checkStreamerStatus={checkStreamerStatus} setOpenApplyPromoCodePopup={setOpenApplyPromoCodePopup} setOpenJoinViaInviteCodePopup={setOpenJoinViaInviteCodePopup} />}
                            // renderItem={({ item, index }) => <MoreMenuItem data={{
                            //     item: item,
                            //     index: index,
                            //     navigation: props.navigation,
                            //     count: videoCounts,
                            //     videoData: videos
                            // }} />}
                            keyExtractor={(item) => item.id}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false} />

                        {/* TouchableOpacity - Logout Button Click Event */}
                        <TouchableOpacity style={[heightStyles.height_46, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, alignmentStyles.bottom_0, marginStyles.topMargin_20, marginStyles.bottomMargin_10]} onPress={callLogoutAlert} activeOpacity={0.5}>

                            {/* Text - Button (Logout) */}
                            <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_logout}</Text>

                            {/* Image - Right Arrow */}
                            <RightArrowImage style={[positionStyles.absolute, alignmentStyles.alignSelfCenter, alignmentStyles.right_0, marginStyles.rightMargin_04]} />
                        </TouchableOpacity>

                    </ScrollView>

                    {checkIsSponsorAdsEnabled('more') &&
                        <View style={{ marginBottom: 10 }}>
                            <SponsorBannerAds screenCode={'more'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('more')} />
                        </View>
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}

            {/* Popup - Apply Promo Code */}
            <Modal
                isVisible={isOpenApplyPromoCodePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <BottomSheetApplyPromoCode openApplyPromoCodePopup={setOpenApplyPromoCodePopup} />
            </Modal>

            {/* Popup - Join Via Invite Code */}
            <Modal
                isVisible={isOpenJoinViaInviteCodePopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <BottomSheetJoinViaInviteCode openJoinViaInviteCodePopup={setOpenJoinViaInviteCodePopup} />
            </Modal>

        </SafeAreaView>
    )
}

export default More;