import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import moment from 'moment';
import { AppConstants } from '../../appUtils/appConstants';
import { getUpiPattern, showErrorToastMessage } from '../../appUtils/commonUtlis';

const UpiPaymentPopup = (props) => {

    const [upiAddress, setUpiAddress] = useState('')

    const proceed = () => {

        if (upiAddress == '') {
            showErrorToastMessage('Please enter UPI address')
        } else if (!getUpiPattern().test(upiAddress)) {
            showErrorToastMessage('Please enter valid UPI address');
        } else {
            props.setOpenUpiPaymentPopup(false)
            props.proceedWithUpiPayment(upiAddress)
        }
    }

    return (
        <>
            <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, backgroundColor: colors.yellow }} >

                {/* Header View */}
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    {/* Title */}
                    <Text style={{ marginLeft: 60, flex: 1, color: colors.black, fontSize: 22, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} >Add UPI</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={{ marginTop: 0, marginRight: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }} onPress={() => props.setOpenUpiPaymentPopup(false)} activeOpacity={0.5} >
                        <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* UPI */}
                <View style={{ marginTop: 28, marginLeft: 20, marginRight: 20 }}>

                    {/* Title */}
                    <Text style={{ color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.regular }} >UPI Address</Text>

                    {/* Input */}
                    <TextInput style={{ marginTop: 8, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, color: colors.black, paddingHorizontal: 15 }}
                        placeholder='Enter UPI Address'
                        placeholderTextColor={'#676741'}
                        onChangeText={(text) => setUpiAddress(text)}
                        autoCapitalize='none'
                        keyboardType='email-address'
                    >{upiAddress}
                    </TextInput>
                </View>

                {/* Proceed */}
                <TouchableOpacity style={{ marginTop: 40, marginLeft: 20, marginRight: 20, marginBottom: 20, height: 46, borderRadius: 23, backgroundColor: colors.black, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} activeOpacity={0.8} onPress={() => proceed()}>

                    <Text style={{ marginLeft: 22, color: colors.white, fontSize: 16, fontFamily: fontFamilyStyleNew.semiBold }} >PROCEED</Text>
                    <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>

                </TouchableOpacity>
            </View>
        </>
    )
}

export default UpiPaymentPopup