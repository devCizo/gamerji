import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, ScrollView, Dimensions, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew, fontFamilyStyles, shadowStyle } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as generalSetting from '../../webServices/generalSetting'
import CustomProgressbar from '../../appUtils/customProgressBar'
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const CollegeDetail = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const collegeDetail = props.route.params.collegeDetail
    // const collegeList = props.route.params.collegeList
    const [isActiveTab, setIsActiveTab] = useState('members');
    const [isLoading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const [members, setMembers] = useState([]);
    const [membersFetchingStatus, setMembersFetchingStatus] = useState(false);
    const [collegeList, setCollegeList] = useState([]);
    addLog("collegeDetail==>", collegeDetail)

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setMembers([])

        getMembers()
        return () => {
            dispatch(actionCreators.resetCollegeDetailState())
        }
    }, [refreshing]);

    //Fetch Members
    useEffect(() => {

        setLoading(true)
        let payload = {
            skip: members.length,
            limit: 100,
            filter: {
                college: collegeDetail._id
            }
        }

        dispatch(actionCreators.requestCollegeMembers(payload))
        dispatch(actionCreators.requestCollegeList({ skip: 0, limit: 100 }))
        return () => {
            dispatch(actionCreators.resetCollegeDetailState())
        }
    }, [])
    useEffect(() => {
        if (collegeListResponse) {
            setLoading(false)
            setRefreshing(false)

            if (collegeListResponse.list) {
                setCollegeList(collegeListResponse.list)


            }
        }
        collegeListResponse = undefined
    }, [collegeListResponse]);

    let getMembers = () => {
        let payload = {
            skip: members.length,
            limit: 10,
            filter: {
                college: collegeDetail._id
            }
        }
        setMembersFetchingStatus(members.length != 0)
        dispatch(actionCreators.requestCollegeMembers(payload))
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.collegeDetail.model }),
        shallowEqual
    );
    var { collegeMembersResponse } = currentState

    const { currentStateCollege } = useSelector(
        (state) => ({ currentStateCollege: state.collegiate.model }),
        shallowEqual
    );
    var { collegeListResponse, submitCollegeResponse } = currentStateCollege
    //Set members
    useEffect(() => {
        if (collegeMembersResponse) {
            setLoading(false)
            setRefreshing(false)

            if (collegeMembersResponse.list) {
                setMembersFetchingStatus(collegeMembersResponse.list.length == 0)
                var tempArr = collegeMembersResponse.list.map(item => {
                    return { ...item }
                })
                setMembers([...members, ...tempArr])
            }
            collegeMembersResponse = undefined
        }
    }, [collegeMembersResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack}>
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Game Name */}
                <Text style={[fontFamilyStyles.extraBold, styles.gameTypeName]} numberOfLines={1} >{collegeDetail.name}</Text>
            </View>
            <ScrollView style={{ backgroundColor: colors.black }}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                bounces={false}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >
                {/* Container View */}
                <View style={{ flex: 1, marginTop: 0, backgroundColor: 'white', borderTopStartRadius: 40, borderTopEndRadius: 40, justifyContent: 'space-around' }}>

                    <View style={{}} >

                        {/* College Name */}
                        {/* <Text style={{ marginTop: 20, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 24, textAlign: 'center' }} >{collegeDetail.name}</Text> */}

                        <View style={{ marginTop: 18, marginLeft: 10, width: Constants.SCREEN_WIDTH - 20, flexDirection: 'row', justifyContent: 'space-between' }} >

                            {/* Rank */}
                            <View style={{ width: '30%', flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }} >

                                    <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{collegeDetail.rank}</Text>

                                    <Text style={{ marginTop: 1, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Rank</Text>
                                </View>

                                <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                            </View>

                            {/* Members */}
                            <View style={{ width: '30%', flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: '#43F2FF', borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }} >

                                    <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{collegeDetail.members}</Text>

                                    <Text style={{ marginTop: 1, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Members</Text>
                                </View>

                                <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/members-collegiate.png')} />
                            </View>

                            {/* Points */}
                            <View style={{ width: '30%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ marginTop: 18, width: '100%', height: 120, backgroundColor: colors.yellow, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }} >

                                    <Text style={{ marginTop: 48, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 36 }} >{collegeDetail.points}</Text>

                                    <Text style={{ marginTop: 1, alignSelf: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }} >Points</Text>
                                </View>

                                <Image style={{ position: 'absolute', top: 0, height: 60, width: 60 }} source={require('../../assets/images/rank-collegiate.png')} />
                            </View>
                        </View>
                    </View>

                    <View style={{ flex: 1 }}>

                        {/* Tabs View */}
                        <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 40, flexDirection: 'row', overflow: 'hidden', backgroundColor: colors.red, borderRadius: 20 }} >

                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: isActiveTab === 'members' ? colors.black : 'transparent' }} onPress={() => setIsActiveTab('members')}>
                                <Text style={{ color: 'white', fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Members</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '50%', borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: isActiveTab === 'colleges' ? colors.black : 'transparent' }} onPress={() => setIsActiveTab('colleges')}>
                                <Text style={{ color: 'white', fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Colleges</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Top Container */}
                        <View style={{ marginTop: 15, marginLeft: 20, marginRight: 20, height: 50, backgroundColor: colors.yellow, borderTopLeftRadius: 20, borderTopRightRadius: 20, flexDirection: 'row', alignItems: 'center' }} >
                            {/* Rank */}
                            <Text style={{ width: 64, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Rank</Text>
                            {/* User Name */}
                            <Text style={{ marginLeft: 46, flex: 1, textAlign: 'left', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >{isActiveTab === 'members' ? 'User Name' : 'College Name'}</Text>

                            {/* Points */}
                            {/* {isActiveTab === 'members' && */}
                            <Text style={{ width: 64, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Points</Text>
                            {/* } */}

                            {/* Level */}
                            {/* <Text style={{ width: 64, textAlign: 'center', color: colors.black, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 14 }} >Kill</Text> */}


                        </View>

                        <FlatList
                            style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderWidth: 1, borderColor: '#DFE4E9', }}
                            data={isActiveTab === 'members' ? members : collegeList}
                            renderItem={({ item, index }) => <CollegeDetailItem item={item} index={index} isMember={isActiveTab === 'members'} />}
                            keyExtractor={(item, index) => index.toString()}
                        >
                        </FlatList>
                    </View>

                    {checkIsSponsorAdsEnabled('collegeLeaguesDetails') &&
                        <View style={{ width: '100%' }}>
                            <SponsorBannerAds screenCode={'collegeLeaguesDetails'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('collegeLeaguesDetails')} />
                        </View>
                    }
                </View>
            </ScrollView>
            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const CollegeDetailItem = (props) => {

    var isMember = props.isMember
    var item = props.item

    const getLevelData = () => {
        if (isMember) {
            if (item.wallet) {
                return item.wallet.level
            }
        } else {
            return item.members
        }
    }

    const getPointData = () => {
        if (isMember) {
            if (item.wallet) {
                return item.wallet.pointAmount
            }
        } else {
            return item.points
        }
    }

    const getRankData = () => {
        if (isMember) {
            if (item.wallet) {
                return item.wallet.rank
            }
        } else {
            return item.rank
        }
    }

    return (

        <View>
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }} >

                <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{item.rank}</Text>
                {/* <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{getRankData()}</Text> */}

                {/* Badge */}
                {isMember ?

                    item.level && item.level.featuredImage ?
                        <Image style={{ marginLeft: 8, width: 30, height: 30, resizeMode: 'contain' }} source={{ uri: generalSetting.UPLOADED_FILE_URL + item.level.featuredImage.default }} />
                        :
                        <Text></Text>

                    :
                    <Text></Text>
                }

                <View style={{ marginLeft: 8, flex: 1 }}>

                    {/* Username */}
                    {isMember ?
                        <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} numberOfLines={1} >{item.gamerjiName}</Text>
                        :
                        <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} numberOfLines={3} >{item.name}</Text>
                    }
                    {isMember &&
                        // Mobile number
                        <Text style={{ marginTop: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 9, color: colors.black }} >XXXXX{item.phone && item.phone.substr(item.phone.length - 5)}</Text>
                    }
                </View>

                {/* {isMember &&
                    // Points
                    <Text style={{ width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{item.wallet && item.wallet.pointAmount}</Text>
                } */}

                {/* Points */}
                {/* <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{getLevelData()}</Text> */}

                {/* Rank */}
                {/* <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{getRankData()}</Text> */}

                {/* Points */}
                <Text style={{ right: 0, width: 64, textAlign: 'center', fontFamily: fontFamilyStyleNew.bold, fontSize: 13, color: colors.black }} >{item.points}</Text>

                {/* Rank */}


                {/* Separator line */}
                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 1, backgroundColor: '#DFE4E9' }} />

            </View>
        </View>
    );
}

export default CollegeDetail;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
        textTransform: 'uppercase'
    }
})