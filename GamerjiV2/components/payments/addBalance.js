import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Image, ScrollView, SafeAreaView, TextInput, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import colors from '../../assets/colors/colors';
import { addLog, showSuccessToastMessage, showErrorToastMessage, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import Modal from 'react-native-modal';
import BottomSheetUpdateEmail from '../Account/BottomSheetUpdateEmail';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const AddBalance = (props, navigation) => {

    // Variable Declarations
    const data = props.route.params;
    const [amount, setAmount] = useState('100');
    const [isOpenUpdateEmailPopup, setIsOpenUpdateEmailPopup] = useState(false);
    let depositCash = parseFloat(global.profile.wallet && global.profile.wallet.depositedAmount && global.profile.wallet.depositedAmount);
    let winningCash = parseFloat(global.profile.wallet && global.profile.wallet.winningAmount && global.profile.wallet.winningAmount);
    let bonusCash = parseFloat(global.profile.wallet && global.profile.wallet.bonusAmount && global.profile.wallet.bonusAmount);
    let coinAmount = parseFloat(global.profile.wallet && global.profile.wallet.bonusAmount && global.profile.wallet.coinAmount);
    let totalCurrentBalance = depositCash + winningCash + bonusCash;

    // This function should allow users to go back to the Login Screen and returns true.
    const setSelectedValue = (value) => {
        addLog(value);
        setAmount(value);
    }

    // This function will check if email exists or not
    // If not exists - Open up the popup for update email id
    // If exists - It should check the validation and proceed further to the payment options screen
    const checkIfEmailExists = () => {
        if (!global.profile.email) {
            openUpdateEmailPopup(true);
        } else {
            checkValidation();
        }
    }

    // This function should open up the Apply Promo Code Popup
    let openUpdateEmailPopup = (visible) => {
        setIsOpenUpdateEmailPopup(visible)
    }

    // This function should check the validation first and then it sould be redirected to the Payment Options Screen.
    const checkValidation = () => {
        if (amount == '') {
            showErrorToastMessage(Constants.error_enter_amount)
            return;
        }
        var params = {
            amount: amount,
        }

        // if (global.addBalanceScreenType == Constants.contest_coin_payment_type || global.addBalanceScreenType == Constants.tournament_coin_payment_type) {
        if (global.addBalanceCurrencyType == Constants.coin_payment) {
            params['paymentTypeForCreateTransaction'] = 18
        } else {
            params['paymentTypeForCreateTransaction'] = 5
        }

        // props.navigation.push(Constants.nav_payment_options, params);
        props.navigation.push(Constants.nav_payment_options_new, params);

        addLog('params', params)
    };

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.pop();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        if (props.route && props.route.params && props.route.params.amountToAdd) {
            setAmount(`${props.route.params.amountToAdd}`)
            // setAmount('10')
        }
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const getCurrentBalanceToShow = () => {

        if (global.addBalanceCurrencyType == Constants.coin_payment) {
            return coinAmount
        } else {
            return totalCurrentBalance//winningCash
        }
    }

    const goBack = () => {
        props.navigation.pop()
    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => goBack()}
                        activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Add Balance Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.header_add_balance} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    <ScrollView style={[paddingStyles.padding_20]}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'>

                        {/* Wrapper - Current Balance */}
                        <View style={[flexDirectionStyles.row, positionStyles.relative, heightStyles.height_50, colorStyles.redBackgroundColor, shadowStyles.shadowBackground, colorStyles.redShadowColor, borderStyles.borderRadius_50, paddingStyles.leftPadding_24]}>

                            {/* Image - Currency */}
                            <Image style={[heightStyles.height_16, widthStyles.width_24, alignmentStyles.alignSelfCenter]} source={require('../../assets/images/ic_currency.png')} />

                            {/* Text - Current Balance */}
                            <Text style={[alignmentStyles.alignSelfCenter, fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_13]}>{Constants.text_current_balance}</Text>

                            {/* Text - Total Current Balance */}
                            <View style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, { flexDirection: 'row', alignItems: 'center' }]} >
                                {global.addBalanceCurrencyType == Constants.coin_payment ?
                                    <Image style={{ height: 18, width: 18, marginRight: 4 }} source={require('../../assets/images/usable_gamerji_coin_popup.png')} />
                                    :
                                    <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, fontSizeStyles.fontSize_05, { marginRight: 2 }]}>₹</Text>
                                }
                                <Text style={[fontFamilyStyles.bold, colorStyles.whiteColor, paddingStyles.rightPadding_17, fontSizeStyles.fontSize_05]}>{getCurrentBalanceToShow()}</Text>
                            </View>

                        </View>

                        {/* Text - Add balance to your account */}
                        <Text style={[positionStyles.relative, alignmentStyles.alignTextCenter, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_14, marginStyles.topMargin_17]}> {Constants.text_add_balance_to_account} </Text>

                        {/* Wrapper - Add Amount */}
                        <View style={[flexDirectionStyles.column, positionStyles.relative, marginStyles.topMargin_33]}>

                            {/* Text - Add Amount */}
                            <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14]}> {Constants.text_add_amount} </Text>

                            {/* Text Validator - Add Amount */}
                            <View style={{ marginTop: 7, height: 46, borderRadius: 23, borderWidth: 1, borderColor: colors.color_input_text_border, alignItems: 'center', flexDirection: 'row' }} >

                                <Text style={{ marginLeft: 15, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }}>{Constants.text_rupees}</Text>
                                <TextInput style={{ flex: 1, height: 46, fontFamily: fontFamilyStyleNew.semiBold, fontSize: 16 }}
                                    keyboardType="numeric"
                                    onChangeText={text => setAmount(text)}
                                >
                                    {amount}
                                </TextInput>
                                {/* <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, justifyContentStyles.center, marginStyles.topMargin_7, colorStyles.greyColor, fontFamilyStyles.semiBold, fontSizeStyles.fontSize_16, paddingStyles.leftPadding_15]}
                                keyboardType="numeric"
                                    onChangeText={text => setAmount(text)} >{amount}</TextInput> */}
                            </View>
                        </View>

                        {/* Wrapper - Select Amount (100, 500 or 1000) */}
                        <View style={[flexDirectionStyles.row, heightStyles.height_44, justifyContentStyles.spaceBetween, marginStyles.topMargin_20, marginStyles.bottomMargin_1]}>

                            {/* TouchableOpacity - Amount 100 Button Click Event */}
                            <TouchableOpacity style={[{ width: '33%' }, shadowStyles.shadowBackgroundForView, colorStyles.yellowShadowColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.yellowBackgroundColor, borderStyles.borderRadius_50]} activeOpacity={1}
                                onPress={() => setSelectedValue('100')} activeOpacity={0.5}>

                                {/* Text - Amount 100 */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_16]}>{Constants.text_rupees}{Constants.text_hundred}</Text>
                            </TouchableOpacity>

                            {/* TouchableOpacity - Amount 500 Button Click Event */}
                            <TouchableOpacity style={[{ width: '33%' }, shadowStyles.shadowBackgroundForView, colorStyles.skyBlueShadowColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.skyBlueBackgroundColor, borderStyles.borderRadius_50]} activeOpacity={1}
                                onPress={() => setSelectedValue('500')} activeOpacity={0.5}>

                                {/* Text - Amount 500 */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_16]}>{Constants.text_rupees}{Constants.text_five_hundred}</Text>
                            </TouchableOpacity>

                            {/* TouchableOpacity - Amount 1000 Button Click Event */}
                            <TouchableOpacity style={[{ width: '33%' }, shadowStyles.shadowBackgroundForView, colorStyles.lightGreenShadowColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.lightGreenBackgroundColor, borderStyles.borderRadius_50]} activeOpacity={1}
                                onPress={() => setSelectedValue('1000')} activeOpacity={0.5}>

                                {/* Text - Amount 1000 */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_16]}>{Constants.text_rupees}{Constants.text_thousand}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>

                    {/* Wrapper - Bottom Action Buttons */}
                    <View style={{ width: '100%' }}>

                        <View style={{ padding: 20 }}>

                            {/* TouchableOpacity - Add Balance Button Click Event */}
                            <TouchableOpacity style={[heightStyles.height_46, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter]}
                                onPress={() => checkIfEmailExists()} activeOpacity={0.5}>

                                {/* Text - Button (Add Balance) */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>{Constants.action_add_balance}</Text>

                                {/* Image - Right Arrow */}
                                <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                            </TouchableOpacity>
                        </View>

                        {checkIsSponsorAdsEnabled('addBalance') &&
                            <SponsorBannerAds screenCode={'addBalance'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('addBalance')} />
                        }
                    </View>
                </View>
            </View>

            {/* Popup - Apply Promo Code */}
            <Modal
                isVisible={isOpenUpdateEmailPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <BottomSheetUpdateEmail openUpdateEmailPopup={openUpdateEmailPopup} checkIfEmailExists={checkIfEmailExists} />
            </Modal>
        </SafeAreaView>
    )
}

export default AddBalance;