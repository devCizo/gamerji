import axios from '../../../webServices/axios-api';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';


{ /* Request - User Login OTP */ }
export const userLoginOTPRequest = (viewModel) => {
    return {
        type: USER_LOGIN_REQUEST_OTP,
        model: viewModel
    };
};

{ /* Request Failure - User Login OTP */ }
export const userLoginOTPRequestFailed = (viewModel) => {
    return {
        type: USER_LOGIN_REQUEST_OTP_FAILED,
        model: viewModel
    };
};

{ /* Set - User Data */ }
export const setUserData = (viewModel) => {
    return {
        type: SET_USER_DATA,
        model: viewModel
    };
};

{ /* Validate Failure - User Login OTP */ }
export const userLoginOTPValidateFailed = () => {
    return {
        type: USER_LOGIN_VALIDATE_OTP_FAILED
    };
};

{ /* Reset State - Authentication */ }
export const resetAuthenticationState = () => {
    return {
        type: RESET_AUTHENTICATION_STATE,
    };
};


{ /* Request - OTP */ }
export const userRequestOTP = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.USER_TYPE] = WebFields.HEADERS.USER_TYPE_VALUE;
        axios
            .post(WebFields.LOGIN.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                if (payload.type === WebFields.OTP.REQUEST_VALIDATE_OTP) {
                    response.data[WebFields.LOGIN.RESPONSE_TYPE] = payload.type;
                    dispatch(userLoginOTPRequest(response.data));
                    saveDataToLocalStorage(AppConstants.key_is_login, JSON.stringify(true))
                    global.token = response.data.token;
                    global.mobile = payload.username;
                    saveDataToLocalStorage(AppConstants.key_token, global.token)
                    saveDataToLocalStorage(AppConstants.key_username, global.mobile)
                    saveDataToLocalStorage(AppConstants.key_user_data, JSON.stringify(response.data.item))
                } else {
                    response.data[WebFields.LOGIN.RESPONSE_TYPE] = payload.type;
                    dispatch(userLoginOTPRequest(response.data));
                }
            })
            .catch((error) => {
                addLog(payload.type);
                error.response.data[WebFields.LOGIN.RESPONSE_TYPE] = payload.type;
                addLog(error.response.data);
                dispatch(userLoginOTPRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const countryList = (viewModel) => {
    return {
        type: COUNTRY_LIST,
        model: viewModel
    };
};

export const countryListFailed = (viewModel) => {
    return {
        type: COUNTRY_LIST_FAILED,
        model: viewModel
    };
};

export const requestCountryList = (payload) => {

    return (dispatch) => {
        addLog("API:::> ", WebFields.LOGIN.COUNTRY_LIST);
        // axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.LOGIN.COUNTRY_LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(countryList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(countryListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


{ /* Action Types */ }
export const USER_LOGIN_REQUEST_OTP = 'USER_LOGIN_REQUEST_OTP';
export const USER_LOGIN_REQUEST_OTP_FAILED = 'USER_LOGIN_REQUEST_OTP_FAILED';
export const SET_USER_DATA = 'SET_USER_DATA';
export const USER_LOGIN_VALIDATE_OTP_FAILED = 'USER_LOGIN_VALIDATE_OTP_FAILED';
export const COUNTRY_LIST = 'COUNTRY_LIST';
export const COUNTRY_LIST_FAILED = 'COUNTRY_LIST_FAILED';
export const RESET_AUTHENTICATION_STATE = 'RESET_AUTHENTICATION_STATE';