import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, ScrollView, Dimensions, StyleSheet } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';
import HtmlText from 'react-native-html-to-text';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import HTMLView from 'react-native-htmlview';
import { encode } from 'html-entities';

const Legality = (props, navigation) => {

    // Variable Declarations
    const data = props.route.params;
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    const [legality, setLegality] = useState(undefined);
    const height = (Dimensions.get('window').height);

    // This Use-Effect function should call the Legality API
    useEffect(() => {
        setLoading(true)
        dispatch(actionCreators.requestLegality(data.legalityType))

        return () => {
            dispatch(actionCreators.resetLegalityState())
        }
    }, []);

    // This function sets the Legality API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.legality.model }),
        shallowEqual
    );
    var { legalityResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (legalityResponse && legalityResponse.item) {
            setLoading(false)
            setLegality(legalityResponse.item)
        }
        legalityResponse = undefined
    }, [legalityResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, [])

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Legality Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}>{data.legalityType}</Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Scroll View */}


                    {/* Wrapper - Terms Of Use / Privacy Policy Content */}
                    <View style={[{ margin: 15, height: height - 80 }, borderStyles.borderWidth_1, borderStyles.borderRadius_20, colorStyles.greyColor, colorStyles.greyBorderColor, marginStyles.topMargin_18, paddingStyles.padding_20]}>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            keyboardShouldPersistTaps='always'>
                            {/* Text - Terms Of Use / Privacy Policy Content */}
                            {/* <Text style={[alignmentStyles.alignSelfCenter, fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_14]}>{legality && legality.description}</Text> */}

                            {legality && legality.description &&
                                // <HtmlText style={[fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_14]} html={legality.description} />
                                // <Text>{encode(legality.description)}</Text>
                                <HTMLView
                                    value={legality.description}
                                    stylesheet={styles}

                                />

                                //     <WebView
                                //     originWhitelist={['*']}
                                //     source={{ html: legality.description }}
                                //   />
                            }
                        </ScrollView>
                    </View>



                    {checkIsSponsorAdsEnabled('legality') &&
                        <SponsorBannerAds screenCode={'legality'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('legality')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    p: {
        fontFamily: 'ProximaNova-Regular', fontSize: 14, color: "grey", margin: 0, padding: 0,
    },
});
export default Legality;