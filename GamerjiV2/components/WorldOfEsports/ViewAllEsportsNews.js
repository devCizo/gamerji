import React, { useCallback, useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, SafeAreaView, FlatList, Linking, Dimensions, RefreshControl } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import colors from '../../assets/colors/colors';
import CustomProgressbar from '../../appUtils/customProgressBar';
import * as actionCreators from '../../store/actions/index';
import EsportsNewsItem from '../../componentsItem/EsportsNewsItem';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { checkIsSponsorAdsEnabled } from '../../appUtils/commonUtlis';

const ViewAllEsportsNews = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);
    const [blogs, setBlogs] = useState([]);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 10

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setBlogs([])
        callToBlogsListAPI();
        return () => {
            dispatch(actionCreators.resetViewAllBlogsState())
        }
    }, [refreshing]);

    // This Use-Effect function should call the method for the first time
    useEffect(() => {
        setLoading(true)
        callToBlogsListAPI();

        return () => {
            dispatch(actionCreators.resetViewAllBlogsState())
        }
    }, []);

    // This Use-Effect function should call the Esports News API
    const callToBlogsListAPI = () => {
        let payload = {
            skip: blogs.length,
            limit: pageLimit,
            sortBy: 'createdAt',
            sort: 'desc'
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestViewAllBlogs(payload))
    }

    // This function sets the View All Blogs API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.viewAllEsportsNews.model }),
        shallowEqual
    );
    var { viewAllBlogsListResponse } = currentState

    // This function checks the response and then sets the data to the UI
    useEffect(() => {
        if (viewAllBlogsListResponse) {
            setLoading(false)
            setRefreshing(false);

            if (viewAllBlogsListResponse.count == 0) {
                setTotalRecords(true)
            }

            if (viewAllBlogsListResponse.list) {
                setIsFooterLoading(viewAllBlogsListResponse.list.length == 0)
                var tempArr = viewAllBlogsListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setBlogs([...blogs, ...tempArr])
            }
        }
        viewAllBlogsListResponse = undefined
    }, [viewAllBlogsListResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, heightStyles.height_50]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - Esports Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.bold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_20]}> {Constants.text_esports_news} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.borderTopLeftRadius_40, borderStyles.borderTopRightRadius_40, colorStyles.whiteBackgroundColor]}>

                    {/* Wrapper - View All Esports News Lists */}
                    {!totalRecords ?
                        <FlatList style={marginStyles.margin_20}
                            data={blogs}
                            renderItem={({ item, index }) => <EsportsNewsItem item={item} index={index} navigation={props.navigation} isFromViewAllEsportsNews={true} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    callToBlogsListAPI()
                                }
                            }}

                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            } />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('viewAllEsportsNews') &&
                        <SponsorBannerAds screenCode={'viewAllEsportsNews'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('viewAllEsportsNews')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

export default ViewAllEsportsNews;