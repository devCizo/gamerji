
import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, BackHandler, RefreshControl } from 'react-native'
import { Constants } from '../../appUtils/constants'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as actionCreators from '../../store/actions/index';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import Modal from 'react-native-modal';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import TournamentCommonDetail from './tournamentCommonDetail'
import * as generalSetting from '../../webServices/generalSetting'
import TournamentTimingPopup from './tournamentTimingPopup'
import JoinTournamentWalletValidation from './joinTournamentWalletValidation'
import JoinViaInviteCode from '../JoinContest/joinViaInviteCode'
import CustomProgressbar from '../../appUtils/customProgressBar'
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
import LottieView from 'lottie-react-native';

const TournamentList = (props) => {

    //Variable Declartion
    const dispatch = useDispatch();
    const game = props.route.params.game;
    const [tournamentList, setTournamentList] = useState([]);
    const [selectedTournament, setSelectedTournament] = useState(undefined);
    const [isVissibleTournamentTimingPopup, setTournamentTimingPopup] = useState(false)
    const [selectedTournamentTiming, setSelectedTournamentTiming] = useState(undefined)
    const [walletUsageLimit, setWalletUsageLimit] = useState(undefined);
    const [isVissibleWalletPopup, setJoinTournamentWalletPopup] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [tournamentListFetchingStatus, setTournamentListFetchingStatus] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    //Fetch tournaments
    useEffect(() => {

        setLoading(true)
        getTournamentList()

        return () => {
            dispatch(actionCreators.resetTounamentListState())
            dispatch(actionCreators.resetJoinViaInviteCodeState())
        }
    }, [])

    let getTournamentList = (skip) => {
        let payload = {
            skip: skip,
            limit: 10,
            filter: {
                "game": game._id
            }
        }
        setTournamentListFetchingStatus(tournamentList.length != 0)
        dispatch(actionCreators.requestTournamentList(payload))
    }

    const onRefresh = useCallback(async () => {
        setRefreshing(true)
        setTournamentList([])
        getTournamentList()

    }, [refreshing]);

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.tournamentList.model }),
        shallowEqual
    );
    var { tournamentListResponse, walletUsageLimitForTournamentResponse } = currentState

    //Set tournament list
    useEffect(() => {
        if (tournamentListResponse) {
            setLoading(false)
            setRefreshing(false)
            if (tournamentListResponse.list) {
                setTournamentListFetchingStatus(tournamentListResponse.list.length == 0)
                var tempArr = tournamentListResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setTournamentList([...tournamentList, ...tempArr])
            }
            tournamentListResponse = undefined
        }
    }, [tournamentListResponse]);

    //Tournament joining validation
    let joinNowValidation = (tournament) => {

        // RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })
        // return

        // RootNavigation.navigate(Constants.nav_squad_registration, { contestTournament: tournament, isContest: false })
        // return

        setSelectedTournament(tournament)

        if (tournament.isJoined && tournament.isJoined == true) {
            RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: tournament._id })
        } else if (!checkForDobStateGameName(tournament)) {
            RootNavigation.navigate(Constants.nav_dob_state_validation, { game: tournament.game, dobStateUpdateSuccess: dobStateUpdateSuccess })
        } else {
            setTournamentTimingPopup(true)
        }
    }

    // DOB State Validation
    let checkForDobStateGameName = (tournament) => {

        var isValidToJoin = true

        if (global.profile) {
            if (global.profile.gamerjiName) {
                if (global.profile.gamerjiName == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.dateOfBirth) {
                if (global.profile.dateOfBirth == '') {
                    isValidToJoin = false
                }
            }

            if (global.profile.address) {
                if (global.profile.address.state == null) {
                    isValidToJoin = false
                }
            }

            if (global.profile.gameNames, tournament.game) {
                let index = global.profile.gameNames.findIndex(obj => obj.game == tournament.game._id)
                if (index < 0) {
                    isValidToJoin = false
                }
            }
        }
        return isValidToJoin
    }

    const dobStateUpdateSuccess = () => {
        setTournamentTimingPopup(true)
    }

    //Tournament Timing
    let tournamentTimingSelected = (contest) => {
        setSelectedTournamentTiming(contest)
        setLoading(true)
        getWalletUsageLimit()
    }

    let closeTournamentTimingPopup = () => {
        setTournamentTimingPopup(false)
    }

    //Wallet validation
    let getWalletUsageLimit = () => {
        if (selectedTournament) {
            let payload = {
                event: selectedTournament._id,
                type: 'event'
            }
            dispatch(actionCreators.requestWalletUsageLimitForTournament(payload))
        }
    }

    //Wallet Usage Limit For Tournament Response
    useEffect(() => {
        if (walletUsageLimitForTournamentResponse) {
            setLoading(false)
            if (walletUsageLimitForTournamentResponse.item) {
                setWalletUsageLimit(walletUsageLimitForTournamentResponse.item)
                setTimeout(function () {
                    setJoinTournamentWalletPopup(true)
                }, 500);
            } else {
                if (walletUsageLimitForTournamentResponse.errors && walletUsageLimitForTournamentResponse.errors[0] && walletUsageLimitForTournamentResponse.errors[0].msg) {
                    showErrorToastMessage(walletUsageLimitForTournamentResponse.errors[0].msg)
                }
            }
            walletUsageLimitForTournamentResponse = undefined
        }
    }, [walletUsageLimitForTournamentResponse]);

    let closeWalletValidationPopup = () => {
        setJoinTournamentWalletPopup(false)
    }

    let moveToSquadRegistration = (contest) => {
        RootNavigation.navigate(Constants.nav_squad_registration, { contestTournament: selectedTournament, roundContest: contest, isContest: false, setTournamentAsJoined: setTournamentAsJoined })
    }

    let setTournamentAsJoined = () => {
        if (selectedTournament) {
            RootNavigation.navigate(Constants.nav_tournament_detail, { tournamentId: selectedTournament._id })
            let index = tournamentList.findIndex(obj => obj._id == selectedTournament._id)
            if (index > -1) {
                var tempArr = [...tournamentList]
                tempArr[index].isJoined = true
                setTournamentList(tempArr)
            }
        }
    }

    let moveToAddBalance = (amountToAdd, isCoin) => {
        if (isCoin) {
            global.addBalanceScreenType = Constants.tournament_coin_payment_type
            global.addBalanceCurrencyType = Constants.coin_payment
        } else {
            global.addBalanceScreenType = Constants.tournament_money_payment_type
            global.addBalanceCurrencyType = Constants.money_payment
        }
        global.paymentSuccessCompletion = reOpenWalletPaymentPopup
        RootNavigation.navigate(Constants.nav_coin_reward_store, { amountToAdd: amountToAdd })
    }

    // Re-Open Wallet Payment After Remaining Payment Done
    const reOpenWalletPaymentPopup = () => {
        setWalletUsageLimit(undefined)
        if (selectedTournament) {
            setLoading(true)
            getWalletUsageLimit()
        }
    }

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>
            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={RootNavigation.goBack} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                {/* Game Name */}
                <Text style={[styles.gameTypeName]} numberOfLines={1} >Tournaments</Text>
            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                {/* Tournament List View */}
                {tournamentList.length > 0 ?
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 10, flex: 1, alignSelf: 'stretch' }}>
                        <FlatList
                            ListHeaderComponent={
                                <View style={{ marginBottom: 10 }} >
                                    {/* Join Via Invite Code */}
                                    <JoinViaInviteCode setLoading={setLoading} />
                                </View>
                            }
                            data={tournamentList}
                            renderItem={({ item }) => <TournamentListItem item={item} joinNowValidation={joinNowValidation} />}
                            keyExtractor={(item, index) => index.toString()}
                            onEndReachedThreshold={0.1}
                            onEndReached={() => {
                                if (!tournamentListFetchingStatus) {
                                    getTournamentList(tournamentList.length)
                                }
                            }}
                            showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            }
                        />
                    </View>
                    : <NoRecordsFound></NoRecordsFound>}

                {checkIsSponsorAdsEnabled('tournamentLists') &&
                    <SponsorBannerAds screenCode={'tournamentLists'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('tournamentLists')} />
                }
            </View>

            <Modal isVisible={isVissibleTournamentTimingPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <TournamentTimingPopup tournament={selectedTournament} closeTournamentTimingPopup={closeTournamentTimingPopup} tournamentTimingSelected={tournamentTimingSelected} />
            </Modal>

            <Modal isVisible={isVissibleWalletPopup}
                coverScreen={false}
                testID={'modal'}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <JoinTournamentWalletValidation tournament={selectedTournament} contest={selectedTournamentTiming} walletUsageLimit={walletUsageLimit} closeWalletValidationPopup={closeWalletValidationPopup} setTournamentAsJoined={setTournamentAsJoined} moveToSquadRegistration={moveToSquadRegistration} moveToAddBalance={moveToAddBalance} />
            </Modal>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const TournamentListItem = (props) => {

    var tournament = props.item

    return (

        <View>
            <TournamentCommonDetail tournament={tournament} isFromTournamentDetail={false} currency={tournament.currency} />

            {/* Tournament Image */}
            <View style={{ position: 'absolute', top: 40, left: 8, width: 100, height: 130, borderRadius: 5, overflow: 'hidden' }} >

                <Image style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, width: 100, height: 130 }} source={tournament.featuredImage && tournament.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + tournament.featuredImage.default }} />
                <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, width: 100, height: 70 }} source={require('../../assets/images/tournament_image_layer.png')} />
                <Text style={{ position: 'absolute', left: 0, right: 0, bottom: 6, color: 'white', textAlign: 'center', fontSize: 12, fontFamily: fontFamilyStyleNew.bold }} >{tournament.gameType && tournament.gameType.name}</Text>

            </View>

            <View style={{ position: 'absolute', flexDirection: 'row', left: 8, bottom: 16, right: 8, height: 44, backgroundColor: colors.yellow, borderRadius: 5, overflow: 'hidden', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }} >

                {/* Hosted By View */}
                <View style={{ width: '50%', backgroundColor: colors.yellow, overflow: 'hidden', justifyContent: 'center' }} >

                    <Text style={{ marginLeft: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 10 }}>Hosted By</Text>

                    <View style={{ marginTop: 2, marginLeft: 10, marginRight: 10, flexDirection: 'row', alignItems: 'center', overflow: 'hidden' }} >

                        <CustomMarquee style={{ color: colors.black, fontSize: 10, fontWeight: 'bold' }} value={(tournament.host && tournament.host.name) + ' (' + (tournament.host && tournament.host.rate.toFixed(2)) + '★)'} />
                    </View>


                    <Image style={{ position: 'absolute', backgroundColor: colors.black, top: 0, bottom: 0, right: 0, width: 1 }} />
                </View>

                {/* Join Now View */}
                {tournament.totalPlayers - tournament.totalJoinedPlayers > 0 ?
                    <TouchableOpacity style={{ width: '50%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }} onPress={() => props.joinNowValidation(tournament)} activeOpacity={1} >
                        <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, color: colors.black }} >{tournament.isJoined ? 'JOINED' : 'JOIN NOW'}</Text>
                        {tournament.isJoined &&
                            <View style={{ marginLeft: 4, height: 16, width: 16 }}>
                                <LottieView
                                    source={require('../../assets/jsonFiles/success.json')}
                                    autoPlay
                                    loop
                                />
                            </View>
                        }
                    </TouchableOpacity>
                    : tournament.isJoined ?
                        <TouchableOpacity style={{ width: '50%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }} onPress={() => props.joinNowValidation(tournament)} activeOpacity={1} >
                            <Text style={{ fontFamily: fontFamilyStyleNew.bold, fontSize: 14, color: colors.black }} >JOINED</Text>
                            <View style={{ marginLeft: 4, height: 16, width: 16 }}>
                                <LottieView
                                    source={require('../../assets/jsonFiles/success.json')}
                                    autoPlay
                                    loop
                                />
                            </View>
                        </TouchableOpacity> :
                        <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.yellow, flexDirection: 'row' }}   >
                            <Text style={{ fontWeight: 'bold', fontSize: 12, color: colors.black }} >FULL</Text>

                        </View>}
            </View>

        </View >
    )
}

export default TournamentList;

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    gameTypeName: {
        color: 'white',
        fontFamily: fontFamilyStyleNew.bold,
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center',
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
        justifyContent: 'space-around'
    },
})