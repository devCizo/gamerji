import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinTournament/joinTournamentWalletValidationAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        joinTournamentResponse: {},
        success: true,
        errorMessage: ''
    },
    error: false
};

{ /* Request - Game Types List */ }
const joinTournament = (state, action) => {
    return produce(state, (draft) => {
        draft.model.joinTournamentResponse = action.model;
        draft.model.success = true
        draft.model.errorMessage = ''
    });
};

{ /* Request Failure - Game Types List */ }
const joinTournamentFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.model.success = false
        draft.model.errorMessage = action.model.errors
        draft.model.joinTournamentResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.JOIN_TOURNAMENT:
            return joinTournament(state, action);
        case actionTypes.JOIN_TOURNAMENT_FAILED:
            return joinTournamentFailed(state, action);
        case actionTypes.RESET_JOIN_TOURNAMENT_STATE:
            return resetJoinTournamentState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetJoinTournamentState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.joinTournamentResponse = undefined
    })
}