import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView } from 'react-native'
import { splashStyles } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import SplashImage from '../../assets/images/splash_bg.svg';
import LogoImage from '../../assets/images/logo.svg';
import PUBGImage from '../../assets/images/ic_pubg.svg';
import FreeFireImage from '../../assets/images/ic_free_fire.svg';
import splashData from '../../assets/data/splashData';
import { retrieveDataFromLocalStorage } from '../../appUtils/sessionManager';
import { AppConstants } from '../../appUtils/appConstants';
import { addLog } from '../../appUtils/commonUtlis';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export default function Splash({ navigation }) {

    // This Use-Effect function should redirected to the,
    // Login Screen - (if isLogin == false)
    // Bottom Navigation Screen - (if isLogin == true)

    const insets = useSafeAreaInsets()

    useEffect(() => {
        (async () => {
            const isLogin = await retrieveDataFromLocalStorage(AppConstants.key_is_login);
            global.token = await retrieveDataFromLocalStorage(AppConstants.key_token);
            global.mobile = await retrieveDataFromLocalStorage(AppConstants.key_username);
            global.profile = await retrieveDataFromLocalStorage(AppConstants.key_user_profile);

            addLog('isLogin:::> ', isLogin)
            addLog('Token:::> ', token)

            if (isLogin == 'true') {
                navigation.navigate(Constants.nav_bottom_navigation);
                // navigation.navigate(Constants.nav_update_profile_after_signUp)
            } else {
                navigation.navigate(Constants.nav_login)
            }
        })();
    }, []);

    // This function should render the footer items from the Splash Data 
    const renderFooterItems = ({ item }) => {
        return (
            /* Wrapper - Footer Items */
            <View style={splashStyles.wrapperFooterItems}>
                <StatusBar hidden={true} />
                <View style={{
                    marginLeft: item.id == 1 ? 5 : 0,
                }}></View>

                {/* Image - Footer Icons */}
                {item.image}

                {/* Text - Footer Items */}
                <Text style={splashStyles.textFooterItems}>{item.title}</Text>
            </View>
        );
    };

    return (
        <>
            {/* <View style={{ flex: 1 }} > */}
            <Image style={{ position: 'absolute', resizeMode: 'stretch', width: '100%', height: '100%', backgroundColor: 'white' }} source={require('../../assets/images/splash_bg.png')} />

            <Image style={{ position: 'absolute', top: insets.top + 80, left: 25, right: 25, width: Constants.SCREEN_WIDTH - 50, height: 296.5, resizeMode: 'contain', }} source={require('../../assets/images/splash_icon.png')} />

            <Image style={{ position: 'absolute', left: 20, right: 20, bottom: insets.bottom + 15, width: Constants.SCREEN_WIDTH - 40, height: 25, resizeMode: 'contain' }} source={require('../../assets/images/splash_bottom_image.png')} />

            <Image style={{ position: 'absolute', left: 20, right: 20, bottom: insets.bottom + 15 + 25 + 40, width: Constants.SCREEN_WIDTH - 40, height: 57.5, resizeMode: 'contain' }} source={require('../../assets/images/splash_approved_by.png')} />

            {/* <SafeAreaView style={{ flex: 1, backgroundColor: 'red' }} >
                <Image style={{ position: 'absolute', top: 110, left: 25, right: 25, width: Constants.SCREEN_WIDTH - 50, height: 296.5, resizeMode: 'contain', }} source={require('../../assets/images/splash_icon.png')} />
            </SafeAreaView> */}
            {/* </View> */}
        </>

        // <SafeAreaView style={splashStyles.container}>

        //     {/* Wrapper - Main View */}
        //     <View style={splashStyles.container}>

        //         {/* Container - Full Background */}
        //         <SplashImage style={splashStyles.containerFullBackground} />

        //         {/* Wrapper - Body Content */}
        //         <View style={splashStyles.wrapperBodyContent}>

        //             {/* Image - GamerJi Logo */}
        //             <LogoImage style={splashStyles.imageGamerJiLogo} />

        //             {/* Wrapper - Top Content */}
        //             <View style={splashStyles.wrapperTopContent}>

        //                 {/* Text - India's Only eSports */}
        //                 <Text style={splashStyles.textIndiasOnlyESports}> {Constants.text_esports_header} </Text>

        //                 {/* Text - Tournament Platform */}
        //                 <Text style={splashStyles.textTournamentPlatform}> {Constants.text_esports_sub_header} </Text>
        //             </View>
        //         </View>

        //         {/* Wrapper - Footer Content */}
        //         <View style={splashStyles.wrapperFooterContent}>

        //             {/* Wrapper - Approved By */}
        //             <View style={splashStyles.wrapperApprovedBy}>

        //                 <View>
        //                     {/* Text - Approved By */}
        //                     <Text style={splashStyles.textApprovedBy}> {Constants.text_approved_by} </Text>

        //                     {/* Wrapper - Approved By Companies */}
        //                     <View style={splashStyles.wrapperApprovedByCompanies}>

        //                         {/* Image - Approved By PUBG */}
        //                         <PUBGImage style={splashStyles.imageApprovedByPUBG} />

        //                         {/* Image - Approved By Free Fire */}
        //                         <FreeFireImage style={splashStyles.imageApprovedByFreeFire} />
        //                     </View>
        //                 </View>
        //             </View>

        //             {/* Wrapper - Footer Lists */}
        //             <View>
        //                 <FlatList
        //                     data={splashData}
        //                     renderItem={renderFooterItems}
        //                     keyExtractor={(item) => item.id}
        //                     horizontal={true} />
        //             </View>
        //         </View>
        //     </View>
        // </SafeAreaView>
    )
}