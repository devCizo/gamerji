import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { showMessage } from 'react-native-flash-message';
import { fontFamilyStyleNew } from '../appUtils/commonStyles';
import { checkIsSponsorAdsEnabled, showSuccessToastMessage } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import colors from '../assets/colors/colors';
import SponsorBannerAds from './SponsorBannerAds';

const CommonInfoPopup = (props) => {

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() => {
        if (global.infoPupupList) {
            let index = global.infoPupupList.findIndex(item => {
                if (item.code == props.popupType) {
                    return true
                }
            })

            if (index != -1) {
                setTitle(global.infoPupupList[index].name)
                setDescription(global.infoPupupList[index].description)
            }
        }

    }, []);



    return (
        <View style={styles.mainContainer}>
            {/* Main View */}
            <View style={styles.mainContainer}>

                {/* Header View */}
                <View style={styles.headerContainer}>

                    <Text style={{ marginLeft: 60, color: colors.black, fontSize: 20, fontFamily: fontFamilyStyleNew.bold, flex: 1, textAlign: 'center' }}>{title}</Text>

                    {/* Close Button */}
                    <TouchableOpacity style={styles.closeButton} onPress={() => props.setOpenCommonInfoPopup(false)} activeOpacity={0.5}>
                        <Image style={{ height: 25, width: 25 }} source={require('../assets/images/close_icon.png')} ></Image>
                    </TouchableOpacity>
                </View>

                {/* Center Content */}
                <View style={styles.centerContentContainer} >


                    {/* Message */}
                    <View style={{ flexDirection: 'column' }} >
                        <Text style={{ textAlign: 'center', fontFamily: fontFamilyStyleNew.regular, color: colors.black, fontSize: 15, marginLeft: 10, marginRight: 10, marginBottom: 30 }}>{description}</Text>
                    </View>
                </View>
            </View>

            {
                checkIsSponsorAdsEnabled('infoPopup') &&
                <SponsorBannerAds screenCode={'infoPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('infoPopup')} />
            }
        </View >
    );
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: colors.yellow,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
    centerContentContainer: {
        marginTop: 10,
        alignSelf: 'center'
    },
})

export default CommonInfoPopup;