import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actionCreators from '../../store/actions/index';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import MyRewardDetailPopup from './myRewardDetailPopup';
import Modal from 'react-native-modal';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';

const RewardStoreRedeemPopup = (props) => {

    const dispatch = useDispatch()
    let voucher = props.voucher
    const [isLoading, setLoading] = useState(false)
    const [isOpenMyRewardDetailPopup, setOpenMyRewardDetailPopup] = useState(false)
    const [isRedeemButton, setRedeemButton] = useState(false)

    useEffect(() => {

        return () => {
            dispatch(actionCreators.resetRewardStoreRedeemState())
        }
    }, [])

    const redeemVoucher = () => {
        setLoading(true)
        setRedeemButton(true);


        addLog("Clicked! redeemVoucher")
        dispatch(actionCreators.requestApplyReward({ product: voucher._id }))
        // props.setOpenRedeemPopup(false)

    }


    //API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.rewardStoreRedeem.model }),
        shallowEqual
    );
    var { applyRewardResponse } = currentState

    // Profile Response
    useEffect(() => {
        if (applyRewardResponse) {
            setLoading(false)

            if (applyRewardResponse.success && applyRewardResponse.success == true) {
                showSuccessToastMessage("Voucher redeemed successfully")
                // setOpenMyRewardDetailPopup(true)
                // props.setOpenRedeemPopup(false)


                let rewards = 'rewards';
                let myRewards = 'myRewards';
                RootNavigation.navigate(Constants.nav_coin_reward_store, { rewards, myRewards });
            } else {
                if (applyRewardResponse.errors[0] && applyRewardResponse.errors[0].msg) {
                    showErrorToastMessage(applyRewardResponse.errors[0].msg)
                    props.setOpenRedeemPopup(false)

                }
            }
        }
        applyRewardResponse = undefined
    }, [applyRewardResponse])

    return (
        <>
            <View style={{ backgroundColor: colors.white, borderTopStartRadius: 50, borderTopEndRadius: 50, alignItems: 'center' }} >

                {/* Close Button */}
                <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', marginRight: 0, zIndex: 1 }} onPress={() => props.setOpenRedeemPopup(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>

                <View>

                    {/* Name */}
                    <View style={{ marginTop: 24, marginLeft: 20, marginRight: 20, alignItems: 'center' }}>
                        <CustomMarquee style={{ color: colors.black, fontSize: 18, fontFamily: fontFamilyStyleNew.bold, textAlign: 'center' }} value={voucher.name} />
                    </View>

                    <View style={{ marginTop: 12, height: 20, alignSelf: 'center', flexDirection: 'row' }}>
                        {/* Discount Image */}
                        <Image style={{ marginLeft: 6, height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                        <Text style={{ marginLeft: 6, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, }} >{voucher.coinAmount}</Text>
                    </View>

                    {/* Description */}
                    <Text style={{ marginTop: 30, marginLeft: 20, marginRight: 20, color: colors.black, fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold, textAlign: 'center' }}>{voucher.description} </Text>

                    {/* Redeem */}
                    <TouchableOpacity disabled={isRedeemButton} style={{ marginTop: 30, marginBottom: 30, height: 46, width: 160, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', backgroundColor: colors.yellow, borderRadius: 23, flexDirection: 'row' }} activeOpacity={0.6} onPress={() => redeemVoucher()}>
                        <Image style={{ top: -2, height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/images/coin.png')} />
                        <Text style={{ marginLeft: 8, color: colors.black, fontSize: 16, fontFamily: fontFamilyStyleNew.bold, }} >{voucher.coinAmount} Redeem</Text>
                    </TouchableOpacity>

                    {isOpenMyRewardDetailPopup &&
                        <Modal
                            isVisible={isOpenMyRewardDetailPopup}
                            coverScreen={true}
                            testID={'modal'}
                            style={{ justifyContent: 'flex-end', margin: 0 }}
                        >
                            <MyRewardDetailPopup setOpenMyRewardDetailPopup={setOpenMyRewardDetailPopup} myReward={voucher} />
                        </Modal>
                    }
                    {checkIsSponsorAdsEnabled('redeemRewardsPopup') &&
                        <SponsorBannerAds screenCode={'redeemRewardsPopup'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('redeemRewardsPopup')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </>
    )
}

export default RewardStoreRedeemPopup