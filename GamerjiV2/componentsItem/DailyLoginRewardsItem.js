import moment from 'moment';
import React, { useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { AppConstants } from '../appUtils/appConstants';
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, justifyContentStyles, positionStyles, fontFamilyStyleNew, otherStyles } from '../appUtils/commonStyles';
import { addLog } from '../appUtils/commonUtlis';
import { Constants } from '../appUtils/constants';
import { retrieveDataFromLocalStorage } from '../appUtils/sessionManager';
import colors from '../assets/colors/colors';
import * as generalSetting from '../webServices/generalSetting'

const DailyLoginRewardsItem = (props) => {

  // Variable Declarations
  const currentDay = props.currentDay



  var reward = { ...props.item }

  // This Use-Effect function should call the Daily Login Rewards List API
  useEffect(() => {

  }, [])

  const isSameDate = moment(moment(reward.createdAt).format('DD/MM/YYYY')).isSame(moment(Date()).format('DD/MM/YYYY'))

  return (
    <>
      <View style={[flexDirectionStyles.row, reward.day == currentDay ? heightStyles.height_06 : heightStyles.height_05, marginStyles.bottomMargin_10, borderStyles.borderWidth, colorStyles.greyBorderColor, reward.day == currentDay ? colorStyles.redBackgroundColor : colorStyles.whiteBackgroundColor, alignmentStyles.oveflow, reward.day != currentDay && marginStyles.leftMargin_10, reward.day != currentDay && marginStyles.rightMargin_10]}>

        {/* Wrapper - Day */}
        <View style={[widthStyles.width_20p, reward.day == currentDay ? colorStyles.primaryBackgroundColor : colorStyles.lightGreenBackgroundColor, borderStyles.cardRoundedCorner, otherStyles.zIndex_1, { justifyContent: 'center' }]}>

          {/* Text - Day */}
          < Text style={[alignmentStyles.alignTextCenter, fontFamilyStyles.semiBold, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor, fontSizeStyles.fontSize_035, { textAlignVertical: 'center' }]} numberOfLines={1} ellipsizeMode='tail' > {Constants.text_day} - {reward.day} </Text>
        </View>

        {/* Wrapper - Rewards Amount */}
        <View style={[alignmentStyles.alignSelfCenter, widthStyles.width_20p]}>

          {/* Text - Rewards Amount */}
          {reward.amountType == '2' ?
            <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }} source={reward.avatar && reward.avatar.img && reward.avatar.img.default && { uri: generalSetting.UPLOADED_FILE_URL + reward.avatar.img.default }} />
            :
            <Text style={[alignmentStyles.alignTextCenter, fontFamilyStyles.extraBold, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor, fontSizeStyles.fontSize_045]} numberOfLines={1} ellipsizeMode='tail'>{reward.amountType != 1 ? <Image style={{ height: 15, width: 14, resizeMode: 'contain' }} source={require('../assets/images/discount-reward.png')} /> : ""} {reward.amount}</Text>
          }

          {/* Text - Rewards Separator */}
          <Text style={[positionStyles.absolute, reward.day == currentDay ? colorStyles.whiteBackgroundColor : colorStyles.greyBackgroundColor, { width: 0.5, right: 0, top: 1, bottom: 1 }]} />
        </View>

        {/* Wrapper - Description */}
        <View style={[alignmentStyles.alignSelfCenter, widthStyles.width_35p, marginStyles.leftMargin_04]}>

          {/* Text - Description */}
          {reward.amountType == '2' ?
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
              <Text style={[fontFamilyStyles.regular, fontSizeStyles.fontSize_03, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor]} numberOfLines={1} ellipsizeMode='tail'>{reward.avatar && reward.avatar.name}</Text>
              <TouchableOpacity style={[reward.day == currentDay ? heightStyles.height_06 : heightStyles.height_05, { width: 50, justifyContent: 'center', alignItems: 'center', }]} onPress={() => props.openDailyLoginAvatarPopup(reward)} >
                <Image style={{ height: 16, width: 16, resizeMode: 'contain', tintColor: reward.day == currentDay ? colors.white : colors.red }} source={require('../assets/images/info_icon_popup.png')} />
              </TouchableOpacity>
            </View>
            :
            <Text style={[fontFamilyStyles.regular, fontSizeStyles.fontSize_03, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor]} numberOfLines={1} ellipsizeMode='tail'>{reward.name}</Text>
          }

          {/* Text - Description Separator */}
          {reward.day <= currentDay &&
            <Text style={[positionStyles.absolute, reward.day == currentDay ? colorStyles.whiteBackgroundColor : colorStyles.greyBackgroundColor, marginStyles.rightMargin_03, { width: 0.5, right: 0, top: 1, bottom: 1 }]} />
          }
          {/* {reward.day == currentDay && reward.isClaimed && reward.isClaimed == true &&
                        <Text style={[positionStyles.absolute, reward.day == currentDay ? colorStyles.whiteBackgroundColor : colorStyles.greyBackgroundColor, marginStyles.rightMargin_03, { width: 0.5, right: 0, top: 1, bottom: 1 }]} />
                    } */}
        </View>

        {
          reward.day < currentDay &&

          <View style={[flexDirectionStyles.row, alignmentStyles.alignSelfCenter, justifyContentStyles.center]}>

            {/* Claimed Icon */}
            <Image source={require('../assets/images/ic_claim.png')} />

            {/* Claimed Text */}
            <Text style={[fontFamilyStyles.semiBold, fontSizeStyles.fontSize_03, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor, marginStyles.leftMargin_4]} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_claimed}</Text>
          </View>
        }
        {
          reward.day == currentDay &&
          <>
            {
              reward.isClaimed && reward.isClaimed == true ?

                <View style={[flexDirectionStyles.row, alignmentStyles.alignSelfCenter, justifyContentStyles.center]}>

                  {/* Claimed Icon */}
                  <Image source={require('../assets/images/ic_claim.png')} />

                  {/* Claimed Text */}
                  <Text style={[fontFamilyStyles.semiBold, fontSizeStyles.fontSize_03, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor, marginStyles.leftMargin_4]} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_claimed}</Text>
                </View>
                :
                <>
                  {
                    // reward.amountType == '2' &&
                    // <TouchableOpacity style={[reward.day == currentDay ? heightStyles.height_06 : heightStyles.height_05, { width: 50, justifyContent: 'center', alignItems: 'center' }]} onPress={() => props.setOpenDailyLoginAvatarPopup(true)} >
                    //   <Image style={{ height: 16, width: 16, resizeMode: 'contain', tintColor: reward.day == currentDay ? colors.white : colors.red }} source={require('../assets/images/info_icon_popup.png')} />
                    // </TouchableOpacity>
                  }
                </>
            }
          </>
        }


        {/* {
                    reward.day == currentDay ?
                    <TouchableOpacity style={[reward.day == currentDay ? heightStyles.height_06 : heightStyles.height_05, { width: 50, justifyContent: 'center', alignItems: 'center' }]} onPress={() => setOpenDailyLoginAvatarPopup(true)} >
                        <Image style={{ height: 16, width: 16, resizeMode: 'contain', tintColor: reward.day == currentDay ? colors.white : colors.red }} source={require('../assets/images/info_icon_popup.png')} />
                    </TouchableOpacity>
                    :
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignSelfCenter, justifyContentStyles.center]}> */}

        {/* Image - Claimed Status */}
        {/* <Image source={require('../assets/images/ic_claim.png')} /> */}

        {/* Text - Claimed Status */}
        {/* <Text style={[fontFamilyStyles.semiBold, fontSizeStyles.fontSize_03, reward.day == currentDay ? colorStyles.whiteColor : colorStyles.blackColor, marginStyles.leftMargin_4]} numberOfLines={1} ellipsizeMode='tail'>{Constants.text_claimed}</Text>
                    </View>
                } */}
      </View >
    </>
  )
}

export default DailyLoginRewardsItem;