import produce from 'immer';
import * as actionTypes from '../../actions/authentication/profileInfoAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        avatarsForProfileInfoResponse: undefined,
        bannersForProfileInfoResponse: undefined,
        applySignupCodeResponse: undefined,
    },
    error: false
};

{ /* Request - Avatar List */ }
const avatarListForProfileInfo = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.avatarsForProfileInfoResponse = action.model;
    });
};

{ /* Request Failure - Avatar List */ }
const avatarListForProfileInfoFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.avatarsForProfileInfoResponse = action.model;
    });
};

{ /* Request - Banner List */ }
const bannerListForProfileInfo = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.bannersForProfileInfoResponse = action.model;
    });
};

{ /* Request Failure - Banner List */ }
const bannerListForProfileInfoFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.bannersForProfileInfoResponse = action.model;
    });
};

const applySignupCode = (state, action) => {
    return produce(state, (draft) => {
        draft.sucess = true;
        draft.error = false;
        draft.model.applySignupCodeResponse = action.model;
    });
};

const applySignupCodeFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.applySignupCodeResponse = action.model;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AVATAR_LIST_PROFILE_INFO:
            return avatarListForProfileInfo(state, action);
        case actionTypes.AVATAR_LIST_PROFILE_INFO_FAILED:
            return avatarListForProfileInfoFailed(state, action);
        case actionTypes.BANNER_LIST_PROFILE_INFO:
            return bannerListForProfileInfo(state, action);
        case actionTypes.BANNER_LIST_PROFILE_INFO_FAILED:
            return bannerListForProfileInfoFailed(state, action);
        case actionTypes.APPLY_SIGN_UP_CODE:
            return applySignupCode(state, action);
        case actionTypes.APPLY_SIGN_UP_CODE_FAILED:
            return applySignupCodeFailed(state, action);
        case actionTypes.RESET_PROFILE_INFO_STATE:
            return resetProfileInfoState(state);
        default:
            return state;
    }
};

export default reducer;

const resetProfileInfoState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.avatarsForProfileInfoResponse = undefined;
        draft.model.bannersForProfileInfoResponse = undefined;
    })
}