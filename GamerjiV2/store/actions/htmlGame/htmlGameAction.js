import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Request - Game Types List */ }
export const html5GamesList = (viewModel) => {
    return {
        type: HTML5_GAME_LIST,
        model: viewModel
    };
};

{ /* Request Failure - Game Types List */ }
export const html5GamesListFailed = (viewModel) => {
    return {
        type: HTML5_GAME_LIST_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestHtml5Games = (payload) => {

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HTML5_GAME.MODE, payload)
            .then((response) => {
                addLog("requestHtml5Games:::> ", response.data);
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(html5GamesList(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(html5GamesListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


{ /* Request - Game Types List */ }
export const html5GamesUpdateView = (viewModel) => {
    return {
        type: HTML5_GAME_UPDATE_VIEW,
        model: viewModel
    };
};

{ /* Request Failure - Game Types List */ }
export const html5GamesUpdateViewFailed = (viewModel) => {
    return {
        type: HTML5_GAME_UPDATE_VIEW_FAILED,
        model: viewModel
    };
};

{ /* Request - Game Types */ }
export const requestHtml5GamesUpdateView = (payload) => {

    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HTML5_GAME.UPDATE_VIEW, payload)
            .then((response) => {
                addLog("html5GamesUpdateView:::> ", response.data);
                // addLog("URL:::> ", response.config.baseURL + response.config.url);
                // addLog("Request:::> ", response.config.data);
                // addLog("Response:::> ", response.data);
                dispatch(html5GamesUpdateView(response.data));
            })
            .catch((error) => {
                // addLog("Error:::> ", error.response.data);
                dispatch(html5GamesUpdateViewFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};

export const resetHtml5GamesState = () => {
    return {
        type: RESET_HTML5_GAME_STATE,
    };
};

export const HTML5_GAME_LIST = 'HTML5_GAME_LIST';
export const HTML5_GAME_LIST_FAILED = 'HTML5_GAME_LIST_FAILED';
export const RESET_HTML5_GAME_STATE = 'RESET_HTML5_GAME_STATE';

export const HTML5_GAME_UPDATE_VIEW = 'HTML5_GAME_UPDATE_VIEW';
export const HTML5_GAME_UPDATE_VIEW_FAILED = 'HTML5_GAME_UPDATE_VIEW_FAILED';