import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/account/earnCoinsAction';

const initialState = {
    model: {
        earnCoinListResponse: undefined,
        claimCoinResponse: undefined
    },
    error: false
};

const getEarnCoinsList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.earnCoinListResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const getEarnCoinsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.earnCoinListResponse = action.model;
    });
};

const claimCoin = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimCoinResponse = action.model;
    });
};

const claimCoinFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.claimCoinResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.EARN_LIST:
            return getEarnCoinsList(state, action);
        case actionTypes.EARN_LIST_FAILED:
            return getEarnCoinsListFailed(state, action);
        case actionTypes.CLAIM_COIN:
            return claimCoin(state, action);
        case actionTypes.CLAIM_COIN_FAILED:
            return claimCoinFailed(state, action);

        default:
            return state;
    }
};

export default reducer;
