import produce from 'immer';
import * as actionTypes from '../../actions/tabAllGames/dailyLoginRewardsAction';

{ /* Initialise - State */ }
const initialState = {
    model: {
        dailyLoginRewardsResponse: undefined,
    },
    error: false
};

{ /* Request - Daily Login Rewards List */ }
const dailyLoginRewardsList = (state, action) => {
    return produce(state, (draft) => {
        draft.model.dailyLoginRewardsResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Daily Login Rewards List */ }
const dailyLoginRewardsListFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.dailyLoginRewardsResponse = action.model;
    });
};

{ /* Request - Daily Login Rewards Status Check */ }
const dailyLoginRewardsStatusCheck = (state, action) => {
    return produce(state, (draft) => {
        draft.model.dailyLoginRewardsStatusCheckResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

{ /* Request Failure - Daily Login Rewards Status Check */ }
const dailyLoginRewardsStatusCheckFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.dailyLoginRewardsStatusCheckResponse = action.model;
    });
};
{ /* Reset State - Daily Login Rewards List */ }
const resetDailyLoginRewardsState = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.dailyLoginRewardsResponse = undefined;
    });
};

{ /* Reducer - For All Methods */ }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DAILY_LOGIN_REWARDS_LIST:
            return dailyLoginRewardsList(state, action);
        case actionTypes.DAILY_LOGIN_REWARDS_LIST_FAILED:
            return dailyLoginRewardsListFailed(state, action);
        case actionTypes.RESET_DAILY_LOGIN_REWARDS_STATE:
            return resetDailyLoginRewardsState(state);
        case actionTypes.DAILY_LOGIN_REWARDS_STATUS_CHECK:
            return dailyLoginRewardsStatusCheck(state, action);
        case actionTypes.DAILY_LOGIN_REWARDS_STATUS_CHECK_FAILED:
            return dailyLoginRewardsStatusCheckFailed(state, action);
        default:
            return state;
    }
};

export default reducer;