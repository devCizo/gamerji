import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, ScrollView, TouchableOpacity, BackHandler, Keyboard, SafeAreaView, Image, TextInput, } from 'react-native'
import { containerStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, fontFamilyStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, widthStyles, heightStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import RightArrowImage from '../../assets/images/ic_right_arrow.svg';
import * as actionCreators from '../../store/actions/index';
import WebFields from '../../webServices/webFields.json';
import colors from '../../assets/colors/colors';
import { showErrorToastMessage, addLog, hasLocationPermission } from '../../appUtils/commonUtlis';
import CountryPicker from './CountryPicker'
import CustomProgressbar from '../../appUtils/customProgressBar';
import { retrieveDataFromLocalStorage } from '../../appUtils/sessionManager';
import { AppConstants } from '../../appUtils/appConstants';
import CountryJSON from '../Authentication/countries.json';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import * as generalSetting from '../../webServices/generalSetting'
import CountryListPopup from '../../appUtils/countryListPopup'
import Modal from 'react-native-modal';
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import * as RNLocalize from "react-native-localize";
//import { getUniqueId, getManufacturer, getSystemName, getSystemVersion, getBatteryLevel, getIpAddress, getUserAgent } from 'react-native-device-info';
import DeviceInfo from 'react-native-device-info';
//import { NetworkInfo } from "react-native-network-info";

const Login = (props, navigation) => {

    // console.log("getUniqueId==>", getUniqueId());
    // console.log("getManufacturer==>", getManufacturer());
    // console.log("getSystemName==>", getSystemName());
    // console.log("getSystemVersion==>", getSystemVersion());
    // console.log("getBatteryLevel==>", getBatteryLevel());
    // console.log("getIpAddress==>", getIpAddress());
    // console.log("getUserAgent==>", getUserAgent());


    // Variable Declarations

    const dispatch = useDispatch();
    const [mobileNo, setMobileNo] = useState('');
    const [showSignUp, setshowSignUp] = React.useState(false);

    const [isLoading, setLoading] = useState(false)
    const [isLoadingForInitialStage, setLoadingForInitialStage] = useState(true)

    const [countryList, setCountryList] = useState([])
    const [selectedCountry, setSelectedCountry] = useState(undefined)
    const [isOpenCountryListPicker, openCountryListPicker] = useState(false)

    const [isAlreadyLoggedIn, setAlreadyLoggedIn] = useState(false)

    // const [counter, setCounter] = useState(0);
    // const [systemName, setSystemName] = useState('');
    // const [systemVersion, setSystemVersion] = useState('');
    // const [brand, setBrand] = useState('');
    // const [ipAddress, setIpAddress] = useState('');
    // const [batteryLevel, setBatteryLevel] = useState('');
    // const [carrier, setCarrier] = useState('');
    // const [device, setDevice] = useState('');
    // const [deviceName, setDeviceName] = useState('');
    // const [macAddress, setMacAddress] = useState('');


    useEffect(() => {



        // setSystemName(DeviceInfo.getSystemName());
        // setCounter(counter + 1);
        // addLog('counter====>', counter);
        // setSystemVersion(DeviceInfo.getSystemVersion());
        // setCounter(counter + 1);
        // addLog('counter====>', counter);
        // setBrand(DeviceInfo.getBrand());
        // setCounter(counter + 1);



        // DeviceInfo.getIpAddress().then(ip => {
        //     setIpAddress(ip);
        //     setCounter(counter + 1);

        // });
        // DeviceInfo.getBatteryLevel().then(batteryLevel => {
        //     setBatteryLevel(batteryLevel);
        //     setCounter(counter + 1);

        // });
        // DeviceInfo.getCarrier().then(carrier => {
        //     setCarrier(carrier);
        //     setCounter(counter + 1);

        // });
        // DeviceInfo.getDevice().then(device => {
        //     setDevice(device);
        //     setCounter(counter + 1);

        // });
        // DeviceInfo.getDeviceName().then(deviceName => {
        //     setDeviceName(deviceName);
        //     setCounter(counter + 1);

        // });
        // DeviceInfo.getMacAddress().then(mac => {
        //     setMacAddress(mac);
        //     setCounter(counter + 1);

        // });
        // addLog('counter====>', counter);

        // if (counter === 9) {
        //     addLog('systemName====>', systemName);
        //     addLog('systemVersion====>', systemVersion);
        //     addLog('brand====>', brand);
        //     addLog('ipAddress====>', ipAddress);
        //     addLog('batteryLevel====>', batteryLevel);
        //     addLog('carrier====>', carrier);
        //     addLog('device====>', device);
        //     addLog('deviceName====>', deviceName);
        //     addLog('macAddress====>', macAddress);
        // }



        (async () => {
            const isLogin = await retrieveDataFromLocalStorage(AppConstants.key_is_login);
            global.token = await retrieveDataFromLocalStorage(AppConstants.key_token);
            global.mobile = await retrieveDataFromLocalStorage(AppConstants.key_username);
            global.profile = await retrieveDataFromLocalStorage(AppConstants.key_user_profile);

            addLog('isLogin:::> ', isLogin)
            addLog('Token:::> ', global.token)

            setAlreadyLoggedIn(isLogin)

            if (isLogin == 'true') {
                props.navigation.replace(Constants.nav_bottom_navigation);
                // props.navigation.replace(Constants.nav_update_profile_after_signUp)

                setTimeout(() => { setLoadingForInitialStage(false) }, 2000)

                dispatch(actionCreators.requestCountryList({}))
            } else {
                setLoadingForInitialStage(false)

                setLoading(true)
                dispatch(actionCreators.requestCountryList({}))
            }
        })();
    }, []);

    // This function should check the validation first and then call the Login API
    const checkValidation = () => {
        if (!mobileNo.trim()) {
            showErrorToastMessage(Constants.error_enter_mobile_no)
            return;
        } else if (mobileNo.length != 10) {
            showErrorToastMessage(Constants.error_enter_valid_mobile_no)
            return;
        }
        callToOTPRequestAPI();
    };

    // This function should call the OTP Request API
    const callToOTPRequestAPI = () => {

        if (!selectedCountry) {
            showErrorToastMessage('Please select country from drop down')
            return
        }

        Keyboard.dismiss();
        var payload = {
            type: WebFields.LOGIN.REQUEST_OTP_REQUEST,
            username: mobileNo,
            phoneCode: selectedCountry.dialingCode,
            country: selectedCountry._id,
            deviceInfo: {
                os: DeviceInfo.getSystemName(),
                osVersion: DeviceInfo.getSystemVersion(),
                brand: DeviceInfo.getBrand(),
            }
        }

        setLoading(true)
        dispatch(actionCreators.userRequestOTP(payload))
    };

    // This function sets the OTP Request API Response to the current state
    const { currentState } = useSelector(
        (state) => ({ currentState: state.authentication.model }),
        shallowEqual
    );
    var { authenticationResponse, countryListResponse } = currentState

    // This function checks the response and then it should be redirected to the OTP screen
    useEffect(() => {
        if (authenticationResponse) {
            setLoading(false)
            if (authenticationResponse.success) {

                if (!selectedCountry) {
                    return
                }

                var isNewUser = false
                if (authenticationResponse.isNewUser && authenticationResponse.isNewUser == true) {
                    isNewUser = true
                }

                props.navigation.navigate(Constants.nav_otp, { mobileNo, selectedCountry, isNewUser })
            } else if (authenticationResponse.errors) {
                if (authenticationResponse.errors[0] && authenticationResponse.errors[0].msg) {
                    showErrorToastMessage(authenticationResponse.errors[0].msg)
                }
            }
        }
        authenticationResponse = undefined
    }, [authenticationResponse]);

    // CountryListResponse
    useEffect(() => {
        if (countryListResponse) {
            setLoading(false)

            addLog('Country -> ', RNLocalize.getCountry())

            // NetworkInfo.getIPAddress().then(ipAddress => {
            //     console.log("ipAddress==>", ipAddress);
            //     addLog('ipAddress==>', ipAddressr)
            // });
            if (countryListResponse.list && countryListResponse.list.length != 0) {

                var index = countryListResponse.list.findIndex(item => {
                    if (item.code === RNLocalize.getCountry()) {
                        return item
                    }
                })

                if (index < 0) {

                    var index = countryListResponse.list.findIndex(item => {
                        if (item.code === "IN") {
                            return item
                        }
                    })

                    if (index < 0) {
                        setSelectedCountry(countryListResponse.list[index])
                    } else {
                        if (!isAlreadyLoggedIn) {
                            //showErrorToastMessage('Currently we are not available in your region')
                        }
                        setSelectedCountry(countryListResponse.list[0])
                    }
                } else {
                    setSelectedCountry(countryListResponse.list[index])
                }

                setCountryList(countryListResponse.list)
                // getCurrentCountry(countryListResponse.list)
            }
        }
        countryListResponse = undefined
    }, [countryListResponse]);

    const getCurrentCountry = (countryList) => {
        addLog('CONDITON 11111')
        setLoading(true)
        if (hasLocationPermission) {
            addLog('CONDITON 22222')
            Geolocation.getCurrentPosition(
                (response) => {
                    addLog('CONDITON 33333')
                    setLoading(false)

                    if (response) {
                        addLog('CONDITON 44444')
                        Geocoder.init(AppConstants.GOOGLE_PLACE_API_KEY); // use a valid API key
                        Geocoder.from(response && response.coords && response.coords.latitude && response.coords.latitude,
                            response && response.coords && response.coords.longitude && response.coords.longitude)
                            .then(json => {
                                addLog('CONDITON 55555')
                                var addressComponent = json.results[0].address_components;

                                if (addressComponent) {
                                    addLog('CONDITON 66666')
                                    for (const items of addressComponent) {
                                        addLog('CONDITON 77777')
                                        if (items.types.includes('country')) {
                                            addLog('CONDITON 88888')
                                            addLog('Country Code:::> ', items.short_name)

                                            var index = countryList.findIndex(item => {
                                                if (item.code === items.short_name) {
                                                    addLog('country', item)
                                                    return item
                                                }
                                            })

                                            addLog('country filtered', index)
                                            if (index < 0) {
                                                addLog('CONDITON 99999')
                                                //  showErrorToastMessage('Currently we are not available in you region')
                                                addLog('Currently we are available in you region')
                                                addLog('countryList', countryList)

                                                if (countryList.length > 0) {
                                                    addLog('CONDITON 99999 111111')
                                                    setSelectedCountry(countryList[0])
                                                }
                                            } else {
                                                addLog('CONDITON 1010101010')
                                                setSelectedCountry(countryList[index])
                                            }
                                        }
                                    }
                                }
                            })
                            .catch(error => console.warn(error));
                    }
                },
                (error) => {
                    // See error code charts below.
                    setLoading(false)
                    console.log(error.code, error.message)
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
            );
        }
    }

    // This function should allow users to exit the app and returns true.
    function handleBackButton() {
        BackHandler.exitApp();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    const openUrlInBrowser = async (url) => {


    }

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} hidden={false} />

                {/* Container - Full Curve Background */}
                <View style={marginStyles.topMargin_120} />

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, paddingStyles.padding_20, borderStyles.bodyTopRoundedCorner_60, colorStyles.whiteBackgroundColor]}>

                    {/* Image - GamerJi Logo */}
                    <Image style={[positionStyles.absolute, heightStyles.height_103, widthStyles.width_132, alignmentStyles.alignSelfCenter, resizeModeStyles.resizeMode, marginStyles.top_75_minus]} source={require('../../assets/images/logo.png')} />

                    {/* Scroll View */}
                    {!isLoadingForInitialStage &&
                        <ScrollView style={[positionStyles.relative]}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            keyboardShouldPersistTaps='always'>

                            {/* Image - User Authentication */}
                            {/* <Image style={[positionStyles.relative, heightStyles.height_210, widthStyles.width_206, alignmentStyles.alignSelfCenter, marginStyles.topMargin_40, marginStyles.bottomMargin_10]} source={require('../../assets/images/login_logo.png')} /> */}
                            {/* <Image style={{ position: "relative", height: 215, width: 265, alignSelf: "center", marginTop: 40, marginBottom: 10 }} source={require('../../assets/images/login_logo.png')} /> */}

                            {/* Wrapper - Input Headers */}
                            <View style={[flexDirectionStyles.row, positionStyles.relative, marginStyles.topMargin_65]}>

                                {/* Text - Country Code Header */}
                                <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14]}> {Constants.text_country_code} </Text>

                                {/* Text - Mobile Number Header */}
                                <Text style={[positionStyles.relative, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_16]}> {Constants.text_mobile_no} </Text>
                            </View>

                            {/* Wrapper - Input Texts */}
                            <View style={[flexDirectionStyles.row, positionStyles.relative, marginStyles.topMargin_7]}>

                                {/* Wrapper - Input Text Country Code */}
                                <TouchableOpacity style={[flexDirectionStyles.row, heightStyles.height_46, widthStyles.width_100, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, alignmentStyles.alignItemsCenter, justifyContentStyles.center]} onPress={() => openCountryListPicker(true)} activeOpacity={0.5}>

                                    {/* Image - Indian Flag */}
                                    <Image style={[heightStyles.height_15, widthStyles.width_24]} source={selectedCountry && selectedCountry.flag && selectedCountry.flag.default && { uri: generalSetting.UPLOADED_FILE_URL + selectedCountry.flag.default }} />

                                    {/* Input Text - Country Code (+91) */}
                                    <Text style={[fontFamilyStyles.semiBold, colorStyles.greyColor, alignmentStyles.alignTextCenter, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_16, marginStyles.leftMargin_5, marginStyles.rightMargin_5]}>{selectedCountry && selectedCountry.dialingCode}</Text>

                                    {/* Image - Down Arrow */}
                                    <Image style={[heightStyles.height_5, widthStyles.width_8]} source={require('../../assets/images/ic_down_arrow.png')} />
                                </TouchableOpacity>

                                {/* Wrapper - Input Text Mobile No */}
                                <View style={[containerStyles.container, marginStyles.leftMargin_10]}>

                                    {/* Text Input - Mobile No */}
                                    <TextInput style={[heightStyles.height_46, borderStyles.borderWidth_1, borderStyles.borderRadius_50, colorStyles.greyBorderColor, fontFamilyStyles.semiBold, colorStyles.greyColor, fontSizeStyles.fontSize_15, paddingStyles.leftPadding_15]}
                                        keyboardType="number-pad"
                                        // autoFocus={true}
                                        placeholder={Constants.validation_mobile_no}
                                        onChangeText={(value) => setMobileNo(value)} maxLength={14} />
                                </View>
                            </View>

                            {/* TouchableOpacity - Login Button Click Event */}
                            <TouchableOpacity style={[heightStyles.height_46, widthStyles.width_100p, flexDirectionStyles.row, colorStyles.primaryBackgroundColor, shadowStyles.shadowBackground, borderStyles.borderRadius_60, alignmentStyles.alignSelfCenter, marginStyles.topMargin_33, marginStyles.bottomMargin_10]}
                                onPress={() => checkValidation()} activeOpacity={0.5}>

                                {/* Text - Button (Login) */}
                                <Text style={[fontFamilyStyles.semiBold, colorStyles.whiteColor, fontSizeStyles.fontSize_16, marginStyles.leftMargin_20, alignmentStyles.alignSelfCenter, otherStyles.capitalize]}>LOGIN / REGISTER</Text>

                                {/* Image - Right Arrow */}
                                <RightArrowImage style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_15]} />
                            </TouchableOpacity>

                            {/* Terms & condition */}
                            <View style={{ alignItems: 'center' }} >
                                <Text style={{ marginTop: 10, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}>By Registering, I agree to GamerJi's</Text>

                                <View style={{ flexDirection: 'row', alignItems: 'center' }} >

                                    {/* Terms Condition */}
                                    <TouchableOpacity style={{ height: 30, justifyContent: 'center' }} onPress={() => RootNavigation.navigate(Constants.nav_more_legality, { legalityType: Constants.nav_more_terms_of_use })} >
                                        <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }}>Terms and Conditions</Text>
                                    </TouchableOpacity>

                                    <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }}> and </Text>

                                    {/* Privacy Policy */}
                                    <TouchableOpacity style={{ height: 30, justifyContent: 'center' }} onPress={() => RootNavigation.navigate(Constants.nav_more_legality, { legalityType: Constants.nav_more_privacy_policy })} >
                                        <Text style={{ color: colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 14 }}>Privacy Policy</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {showSignUp ? (
                                <View>

                                    {/* Text - Or Sign Up Using Social Media */}
                                    <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_14, marginStyles.topMargin_17]}> {Constants.text_sign_up_using_social} </Text>

                                    {/* Wrapper - Social Media Login */}
                                    <View style={[flexDirectionStyles.row, justifyContentStyles.spaceBetween, marginStyles.topMargin_19]}>

                                        {/* TouchableOpacity - Facebook Button Click Event */}
                                        <TouchableOpacity style={[flexDirectionStyles.row, heightStyles.height_40, widthStyles.width_160, colorStyles.blueBackgroundColor, containerStyles.container, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, justifyContentStyles.center, alignmentStyles.alignItemsCenter, marginStyles.rightMargin_15, marginStyles.bottomMargin_10]} activeOpacity={0.5}>

                                            {/* Image - Facebook */}
                                            <Image style={[heightStyles.height_21, widthStyles.width_12_new]} source={require('../../assets/images/ic_facebook.png')} />

                                            {/* Text - Facebook */}
                                            <Text style={[flexDirectionStyles.row, fontFamilyStyles.semiBold, fontFamilyStyles.regular, colorStyles.whiteColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_10]}>{Constants.text_facebook}</Text>
                                        </TouchableOpacity>

                                        {/* TouchableOpacity - Google Button Click Event */}
                                        <TouchableOpacity style={[flexDirectionStyles.row, heightStyles.height_40, widthStyles.width_160, colorStyles.orangeBackgroundColor, containerStyles.container, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, justifyContentStyles.center, alignmentStyles.alignItemsCenter]} activeOpacity={0.5}>

                                            {/* Image - Google */}
                                            <Image style={[heightStyles.height_21, widthStyles.width_21]} source={require('../../assets/images/ic_google.png')} />

                                            {/* Text - Google */}
                                            <Text style={[flexDirectionStyles.row, fontFamilyStyles.regular, fontFamilyStyles.regular, colorStyles.whiteColor, fontSizeStyles.fontSize_14, marginStyles.leftMargin_10]}>{Constants.text_google}</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* Wrapper - Footer Content */}
                                    <View style={[heightStyles.height_46, widthStyles.width_100p, alignmentStyles.alignItemsCenter]}>

                                        {/* Wrapper - Footer Content */}
                                        <View style={[containerStyles.container, positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.alignFooterContentCenter, alignmentStyles.bottom_0, justifyContentStyles.center, alignmentStyles.alignItemsCenter]}>

                                            {/* Text - Don't have an account? */}
                                            <Text style={[fontFamilyStyles.regular, colorStyles.blackColor, alignmentStyles.alignSelfCenter, fontSizeStyles.fontSize_14]}> {Constants.text_dont_have_an_account} </Text>

                                            {/* TouchableOpacity - Sign Up Button Click Event */}
                                            <TouchableOpacity onPress={() => navigation.navigate(Constants.nav_reset_password)} activeOpacity={0.5}>

                                                {/* Text - Sign Up */}
                                                <Text style={[fontFamilyStyles.bold, colorStyles.redColor, fontSizeStyles.fontSize_14, otherStyles.capitalize]}> {Constants.action_sign_up} </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            ) : null}
                        </ScrollView>
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
            {isLoadingForInitialStage && <CustomProgressbar />}

            {isOpenCountryListPicker &&
                <Modal
                    isVisible={isOpenCountryListPicker}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <CountryListPopup countryList={countryList} openCountryListPicker={openCountryListPicker} setSelectedCountry={setSelectedCountry} />
                </Modal>
            }

        </SafeAreaView>
    )
}

export default Login;