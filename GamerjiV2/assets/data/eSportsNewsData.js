import React from 'react'
import { Constants } from '../../appUtils/constants';

const eSportsNewsData = [{
    id: '1',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '2',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '5',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '6',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '7',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '8',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '9',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
},
{
    id: '10',
    title: 'You can get Watch Dogs 2',
    description: 'Ubisoft will let you redeem a free copy of open-world hacking...'
}
];

export default eSportsNewsData;