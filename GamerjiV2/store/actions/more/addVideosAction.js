import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_ADD_VIDEOS = 'REQUEST_ADD_VIDEOS';
export const REQUEST_ADD_VIDEOS_FAILED = 'REQUEST_ADD_VIDEOS_FAILED';
export const RESET_ADD_VIDEOS_STATE = 'RESET_ADD_VIDEOS_STATE';

{ /* Request - Add Videos */ }
export const addVideosRequest = (viewModel) => {
    return {
        type: REQUEST_ADD_VIDEOS,
        model: viewModel
    };
};

{ /* Request Failure - Add Videos */ }
export const addVideosRequestFailed = (viewModel) => {
    return {
        type: REQUEST_ADD_VIDEOS_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Add Videos */ }
export const resetAddVideosState = () => {
    return {
        type: RESET_ADD_VIDEOS_STATE,
    };
};

{ /* Request - Add Videos */ }
export const requestAddVideos = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.ADD_VIDOES.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(addVideosRequest(response.data));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(addVideosRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};