import React, { useEffect, useState } from 'react'
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { View, Text, StatusBar, TouchableOpacity, BackHandler, Image, FlatList, SafeAreaView, ActivityIndicator, Dimensions } from 'react-native'
import { containerStyles, heightStyles, widthStyles, flexDirectionStyles, positionStyles, justifyContentStyles, alignmentStyles, resizeModeStyles, bodyStyles, scrollViewStyles, imageStyles, fontFamilyStyles, inputTextStyles, colorStyles, marginStyles, paddingStyles, fontSizeStyles, borderStyles, shadowStyles, otherStyles, fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { Constants } from '../../appUtils/constants';
import BackArrowImage from '../../assets/images/ic_back.svg';
import RightArrowWhiteImage from '../../assets/images/ic_right_arrow_black.svg';
import colors from '../../assets/colors/colors';
import { addLog, checkIsSponsorAdsEnabled, showSuccessToastMessage } from '../../appUtils/commonUtlis';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import CustomProgressbar from '../../appUtils/customProgressBar';
import moment from 'moment';
import NoRecordsFound from '../../commonComponents/NoRecordsFound';
const { height, width } = Dimensions.get('window');
import PaymentTypesJSON from '../Account/paymentTypes.json';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';

const MyRecentTransactions = (props, navigation) => {

    // Variable Declarations
    const dispatch = useDispatch();
    const [myRecentTransactions, setMyRecentTransactions] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [totalRecords, setTotalRecords] = useState(false);
    const [isFooterLoading, setIsFooterLoading] = useState(false);
    var pageLimit = 10

    // This Use-Effect function should call the My Recent Transactions API
    useEffect(() => {
        setLoading(true)
        callToRecentTransctionAPI()
        return () => {
            dispatch(actionCreators.resetMyRecentTransactionsState())
        }
    }, []);

    // This Use-Effect function should call the My Recent Transactions API 
    let callToRecentTransctionAPI = () => {
        let payload = {
            skip: myRecentTransactions.length,
            limit: pageLimit,
        }
        setIsFooterLoading(true)
        dispatch(actionCreators.requestMyRecentTransactions(payload))
    }

    // This function should set the API Response to Model
    const { currentState } = useSelector(
        (state) => ({ currentState: state.myRecentTransactions.model }),
        shallowEqual
    );
    var { myRecentTransactionsResponse, emailInvoiceResponse } = currentState

    // This function should return the My Recent Transactions API Response
    useEffect(() => {
        if (myRecentTransactionsResponse) {
            setLoading(false)

            if (myRecentTransactionsResponse.count == 0) {
                setTotalRecords(true)
            }

            if (myRecentTransactionsResponse.list) {
                setIsFooterLoading(myRecentTransactionsResponse.list.length == 0)
                var tempArr = myRecentTransactionsResponse.list.map(item => {
                    let tempItem = { ...item }
                    return tempItem
                })
                setMyRecentTransactions([...myRecentTransactions, ...tempArr])
            }
        }
        myRecentTransactionsResponse = undefined
    }, [myRecentTransactionsResponse])

    // This function should call the Email Invoice API
    function callToEmailInvoiceAPI(transactionUniqueID) {
        setLoading(true)
        dispatch(actionCreators.requestEmailInvoice({}, transactionUniqueID))
    }

    // This function should return the My Recent Transactions API Response
    useEffect(() => {
        if (emailInvoiceResponse) {
            setLoading(false)
            showSuccessToastMessage(emailInvoiceResponse.message)
        }
        emailInvoiceResponse = undefined
    }, [emailInvoiceResponse])

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

            {/* Wrapper - Main View */}
            <View style={[containerStyles.container, colorStyles.primaryBackgroundColor]}>

                {/* StatusBar - Show the status bar as per the theme color */}
                <StatusBar backgroundColor={colors.black} />

                {/* Wrapper - Back Arrow */}
                <View style={[flexDirectionStyles.row, containerStyles.headerHeight, marginStyles.topMargin_01]}>

                    {/* TouchableOpacity - Back Arrow Button Click Event */}
                    <TouchableOpacity style={[positionStyles.absolute, alignmentStyles.alignItemsCenter, alignmentStyles.alignSelfCenter, justifyContentStyles.center, otherStyles.zIndex_1]}
                        onPress={() => props.navigation.goBack()} activeOpacity={0.5}>

                        {/* Image - Back Arrow */}
                        <BackArrowImage style={[marginStyles.leftMargin_04, alignmentStyles.alignSelfCenter]} />
                    </TouchableOpacity>

                    {/* Text - My Recent Transactions Header */}
                    <Text style={[containerStyles.container, positionStyles.relative, fontFamilyStyles.extraBold, colorStyles.whiteColor, justifyContentStyles.center, alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, otherStyles.capitalize, fontSizeStyles.fontSize_05]}> {Constants.header_my_recent_transactions} </Text>
                </View>

                {/* Wrapper - Body Content */}
                <View style={[containerStyles.container, positionStyles.relative, borderStyles.bodyRoundedCornerForTop, colorStyles.whiteBackgroundColor]}>

                    {/* Section List */}
                    {!totalRecords ?
                        <FlatList style={marginStyles.margin_20}
                            data={myRecentTransactions}
                            keyExtractor={(item, index) => index.toString()}
                            onEndReachedThreshold={0.1}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            onEndReached={() => {
                                if (!isFooterLoading) {
                                    callToRecentTransctionAPI()
                                }
                            }}
                            renderItem={({ item, index }) => <MyRecentTransactionItem item={item} index={index} previousItem={(index != 0 && myRecentTransactions[index - 1])} callToEmailInvoiceAPI={callToEmailInvoiceAPI} />}
                        />
                        :
                        // Wrapper - No Records 
                        <NoRecordsFound />
                    }

                    {checkIsSponsorAdsEnabled('myRecentTransactions') &&
                        <SponsorBannerAds screenCode={'myRecentTransactions'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('myRecentTransactions')} />
                    }
                </View>
            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    )
}

const MyRecentTransactionItem = (props) => {

    // Variable Declarations
    let item = props.item
    let previousItem = props.previousItem
    let currentDate = moment(item.createdAt).format('Do MMMM YYYY')
    let previousDate = previousItem ? moment(props.previousItem.createdAt).format('Do MMMM YYYY') : ''
    const [isOpen, setOpen] = useState(false);
    const [transactionTitle, setTransactionTitle] = useState(undefined);

    // This function should hit the callToEmailInvoiceAPI function when clicking
    const handleEmailInvoice = (transactionUniqueID) => {
        props.callToEmailInvoiceAPI(transactionUniqueID);
    }

    // This function should call the Payment Types Json Data to fetch the Payment Title
    useEffect(() => {
        getTransactionTitleFromJson();
    }, []);

    // This function should set the Payment Title as compared to the json data
    const getTransactionTitleFromJson = () => {

        var transactionTypesArr = PaymentTypesJSON.map(i => {
            if (i.type == item.paymentType) {
                return setTransactionTitle(i.title);
            }
        })
    }

    return (
        <View style={marginStyles.margin_01}>

            {/* Text - Hdeader Date */}
            {props.index == 0 &&
                <Text style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_045, marginStyles.topMargin_02, marginStyles.bottomMargin_02]}>{currentDate}</Text>
            }

            {props.index != 0 &&
                previousItem && currentDate != previousDate &&
                <Text style={[alignmentStyles.alignSelfCenter, alignmentStyles.alignTextCenter, fontFamilyStyles.regular, colorStyles.greyColor, fontSizeStyles.fontSize_045, marginStyles.topMargin_02, marginStyles.bottomMargin_02]}>{currentDate}</Text>
            }

            {/* TouchableOpacity - Full View Click Event */}
            <TouchableOpacity style={[containerStyles.container, flexDirectionStyles.row, isOpen ? colorStyles.redBackgroundColor : colorStyles.whiteBackgroundColor, shadowStyles.shadowBackground, borderStyles.buttonRoundedCorner, paddingStyles.padding_03, otherStyles.zIndex_1]} onPress={() => setOpen(!isOpen)} activeOpacity={1}>

                {/* Wrapper - Transaction Header */}
                <View style={[containerStyles.container, flexDirectionStyles.row, marginStyles.rightMargin_35, alignmentStyles.oveflow]}>

                    {/* Wrapper - Transaction Icon */}
                    <View style={[alignmentStyles.alignSelfCenter]}>

                        {/* Image - Transaction Icon */}
                        <Image style={[heightStyles.height_24, widthStyles.width_24, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_03]} source={require('../../assets/images/ic_transactions.png')} />
                    </View>

                    {/* Text - Transaction Amount */}
                    <Text style={[widthStyles.width_65, fontFamilyStyles.extraBold, (isOpen ? colorStyles.whiteColor : (item.paymentStatus != 'SUCCESS' ? colorStyles.lightOrangeColor : (item.transactionType == '2' ? colorStyles.darkRedColor : colorStyles.darkGreenColor))), fontSizeStyles.fontSize_18, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_02, marginStyles.rightMargin_02]} numberOfLines={1} ellipsizeMode='tail'>{item.transactionType == '2' ? '-' : '+'} {Constants.text_rupees}{item.amount}</Text>

                    {/* Text - Transaction Title */}
                    <View style={[alignmentStyles.alignSelfCenter]}>
                        <CustomMarquee value={transactionTitle} style={[fontFamilyStyles.semiBold, (isOpen ? colorStyles.whiteColor : (item.paymentStatus != 'SUCCESS' ? colorStyles.lightOrangeColor : (item.transactionType == '2' ? colorStyles.darkRedColor : colorStyles.darkGreenColor))), fontSizeStyles.fontSize_14]} />
                    </View>
                </View>

                {/* Image - Right Arrow */}
                {!isOpen ?
                    <RightArrowWhiteImage style={[positionStyles.absolute, widthStyles.width_50, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} />
                    :
                    <Image style={[positionStyles.absolute, alignmentStyles.right_0, alignmentStyles.alignSelfCenter, marginStyles.rightMargin_05]} source={require('../../assets/images/ic_down_arrow_white.png')} />
                }
            </TouchableOpacity>

            {/* Wrapper - Transaction Details */}
            {
                isOpen &&
                <View style={[{ height: height * 0.23 }, colorStyles.yellowBackgroundColor, borderStyles.borderRadius_15, paddingStyles.padding_02, paddingStyles.topPadding_05, marginStyles.top_22_minus]}>

                    {/* Wrapper - Transaction Id */}
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_02]}>

                        {/* Text - Transaction Id Header */}
                        <Text style={[widthStyles.width_40p, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_transaction_id}</Text>

                        {/* Text - Transaction Id */}
                        <Text style={[widthStyles.width_60p, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01]} numberOfLines={1} ellipsizeMode='tail'>{item.transactionUniqueID}</Text>
                    </View>

                    {/* Wrapper - Transaction Date & Time */}
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_02, marginStyles.topMargin_01]}>

                        {/* Text - Transaction Date & Time Header */}
                        <Text style={[widthStyles.width_40p, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_transaction_date}</Text>

                        {/* Text - Transaction Date & Time */}
                        <Text style={[widthStyles.width_60p, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01]} numberOfLines={1} ellipsizeMode='tail'>{moment(item.createdAt).format('DD-MM-YYYY HH:mm:ss')}</Text>
                    </View>

                    {/* Wrapper - Transaction Status */}
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_02, marginStyles.topMargin_01]}>

                        {/* Text - Transaction Status Header */}
                        <Text style={[widthStyles.width_40p, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_transaction_status}</Text>

                        {/* Text - Transaction Status */}
                        <Text style={[widthStyles.width_60p, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01]} numberOfLines={1} ellipsizeMode='tail'>{item.paymentStatus}</Text>
                    </View>

                    {/* Wrapper - Game Name */}
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_02, marginStyles.topMargin_01]}>

                        {/* Text - Game Name Header */}
                        <Text style={[widthStyles.width_40p, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_game_name}</Text>

                        {/* Text - Game Name */}
                        <Text style={[widthStyles.width_60p, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01]} numberOfLines={1} ellipsizeMode='tail'>{item.game && item.game.name ? item.game.name : '-'}</Text>
                    </View>

                    {/* Wrapper - Game Type */}
                    <View style={[flexDirectionStyles.row, alignmentStyles.alignItemsCenter, marginStyles.leftMargin_02, marginStyles.topMargin_01]}>

                        {/* Text - Game Type Header */}
                        <Text style={[widthStyles.width_40p, fontFamilyStyles.extraBold, colorStyles.blackColor, fontSizeStyles.fontSize_035]}>{Constants.text_game_type}</Text>

                        {/* Text - Game Type */}
                        <Text style={[widthStyles.width_60p, fontFamilyStyles.regular, colorStyles.blackColor, fontSizeStyles.fontSize_035, marginStyles.leftMargin_01]} numberOfLines={1} ellipsizeMode='tail'>{item.gameType && item.gameType.name ? item.gameType.name : '-'}</Text>
                    </View>
                </View>
            }

            {/* Wrapper - Action Buttons */}
            <View style={[positionStyles.absolute, flexDirectionStyles.row, alignmentStyles.alignSelfCenter, justifyContentStyles.center, alignmentStyles.bottom_0, marginStyles.bottomMargin_01]}>

                {/* TouchableOpacity - Email Invoice Button Click Event */}
                <TouchableOpacity style={[widthStyles.width_132, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.lightGreenBackgroundColor, borderStyles.borderRadius_20, borderStyles.borderWidth_2, colorStyles.yellowBorderColor, marginStyles.rightMargin_01]}
                    onPress={() => handleEmailInvoice(item._id)} activeOpacity={0.8}>

                    {/* Wrapper - Email Invoice */}
                    <View style={[flexDirectionStyles.row, paddingStyles.padding_009, alignmentStyles.alignSelfCenter]}>

                        {/* Image - Email Invoice */}
                        <Image source={require('../../assets/images/ic_email_invoice.png')} />

                        {/* Text - Email Invoice */}
                        <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_025, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_02]}>{Constants.text_email_invoice}</Text>
                    </View>
                </TouchableOpacity>

                {/* TouchableOpacity - Download Invoice Button Click Event */}
                {/* <TouchableOpacity style={[widthStyles.width_45p, alignmentStyles.alignItemsCenter, justifyContentStyles.center, colorStyles.skyBlueBackgroundColor, borderStyles.borderRadius_20, borderStyles.borderWidth_2, colorStyles.yellowBorderColor, marginStyles.leftMargin_01]} activeOpacity={0.5}> */}

                {/* Wrapper - Download Invoice */}
                {/* <View style={[flexDirectionStyles.row, paddingStyles.padding_009, alignmentStyles.alignSelfCenter]}> */}

                {/* Image - Download Invoice */}
                {/* <Image source={require('../../assets/images/ic_download_invoice.png')} /> */}

                {/* Text - Download Invoice */}
                {/* <Text style={[fontFamilyStyles.semiBold, colorStyles.blackColor, fontSizeStyles.fontSize_025, alignmentStyles.alignSelfCenter, marginStyles.leftMargin_02]}>{Constants.text_download_invoice}</Text>
                    </View>
                </TouchableOpacity> */}
            </View>
        </View>
    )
}

export default MyRecentTransactions;