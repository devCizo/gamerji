import axios from '../../../webServices/axios-api';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

export const getHowToJoinList = (viewModel) => {
    return {
        type: HOW_TO_JOIN_LIST,
        model: viewModel
    };
};

export const getHowToJoinListFailed = (viewModel) => {
    return {
        type: HOW_TO_JOIN_LIST_FAILED,
        model: viewModel
    };
};

export const requestHowToJoinList = (payload) => {

    return (dispatch) => {
        addLog("Global Token Get:::> ", global.token);
        addLog("API:::> ", WebFields.HOW_TO_JOIN.LIST);
        addLog("Payload:::> ", payload);
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.HOW_TO_JOIN.LIST, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);
                dispatch(getHowToJoinList(response.data));
            })
            .catch((error) => {
                addLog(error.response.data);
                dispatch(getHowToJoinListFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};


export const resetHowToJoinState = () => {
    return {
        type: RESET_HOW_TO_JOIN_STATE,
    };
};

export const HOW_TO_JOIN_LIST = 'HOW_TO_JOIN_LIST';
export const HOW_TO_JOIN_LIST_FAILED = 'HOW_TO_JOIN_LIST_FAILED';
export const RESET_HOW_TO_JOIN_STATE = 'RESET_HOW_TO_JOIN_STATE';