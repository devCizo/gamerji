import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { ProgressBar } from 'react-native-paper';
import moment from 'moment';
import colors from '../../assets/colors/colors'
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import WiningPrizePool from './WiningPrizePool'
import Modal from 'react-native-modal';
import { addLog } from '../../appUtils/commonUtlis';
import CustomMarquee from '../../appUtils/customMarquee';
import * as generalSetting from '../../webServices/generalSetting'

const ContestCommonDetail = (props) => {

    const contest = props.item
    const [isVissibleWinningPrizePoolPopup, setWinningPrizePoolPopup] = useState(false)
    var isFromMyContest = props.isFromMyContest ? props.isFromMyContest : false

    let openWiningPrizePool = () => {
        setWinningPrizePoolPopup(true)
        // RootNavigation.navigate(Constants.nav_wining_prize_pool, { prizePool: contest.prizePool, prizepool: contest.prizepool })
    }

    let closeWiningPrizePool = () => {
        setWinningPrizePoolPopup(false)
    }

    let getProgressValue = () => {
        if (contest.totalJoinedPlayers && contest.totalPlayers) {
            return contest.totalJoinedPlayers / contest.totalPlayers
        } else {
            return 0
        }
    }

    let getDateTimeSubViewWidth = () => {

        let counts = findEnabledTitles().length
        if (counts >= 2) {
            return '25%'
        }
        if (counts == 1) {
            return '33.3%'
        }
        return '50%'
    }

    let findEnabledTitles = () => {
        let filtered = contest.titles && contest.titles.filter(item => {
            if (item.isSelection && item.isSelection == true) {
                return item
            }
        })
        return filtered ? filtered : []
    }

    let findTitleKeyAndValue = (index, type) => {

        let titles = findEnabledTitles()

        if (titles.length >= index + 1) {
            if (type == 'title') { return titles[index].name }
            if (type == 'value') { return titles[index].value }
        }
    }

    let getWinningWinnersSubViewWidth = () => {

        if (contest.winningModel == 'perKill') {
            return '33.33%'
        } else if (contest.winningModel == 'perRank') {
            return '33.33%'
        } else if (contest.winningModel == 'perKillRank') {
            return '25%'
        }
    }

    let showWinnersView = () => {
        if (contest.winningModel == 'perRank' || contest.winningModel == 'perKillRank') {
            return true
        }
        return false
    }

    let showPerKillView = () => {
        if (contest.winningModel == 'perKill' || contest.winningModel == 'perKillRank') {
            return true
        }
        return false
    }

    let totalRemainingPlayers = () => {
        let players = contest.totalPlayers - contest.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' remaining'
    }

    let totalJoinedPlayersPlayers = () => {
        let players = contest.totalJoinedPlayers
        return players + ' player' + (players > 1 ? 's' : '') + ' joined'
    }

    const getCurrencyImage = () => {
        return (
            contest.currency && contest.currency.code == 'coin' ?
                <Image style={{ height: 14, width: 14, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                :
                // <Image style={{ height: 12, width: 8, tintColor: colors.white, resizeMode: 'contain' }} source={contest.currency && contest.currency.img && contest.currency.img.default && { uri: generalSetting.UPLOADED_FILE_URL + contest.currency.img.default }} />
                <Text style={{ fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.currency.symbol}</Text>
        )
    }

    return (
        <>
            {/* Data */}
            <View style={{ flex: 1, alignSelf: 'stretch', marginBottom: !isFromMyContest ? 25 : 40, }}>

                {/* Contest Title View */}
                <View style={{ height: 40, backgroundColor: colors.black, borderTopLeftRadius: 20, borderTopRightRadius: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>

                    {/* Title */}
                    <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', overflow: 'hidden' }}>
                        <CustomMarquee style={{ width: '100%', color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 14, marginLeft: 8 }} value={contest.title || contest.contestType.name} />
                    </View>

                    {/* Short Code */}
                    <View style={{ marginRight: 0, height: 24, backgroundColor: colors.yellow, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, alignItems: 'center', justifyContent: 'center', }} >
                        <Text style={{ marginLeft: 8, marginRight: 8, color: colors.black, textAlign: 'center', fontSize: 12, fontFamily: fontFamilyStyleNew.semiBold }} >ID: {contest.shortCode}</Text>
                    </View>
                </View>

                {/* Date Time View */}
                <View style={styles.dateTimeContainer} >

                    {/* Date */}
                    <View style={{ height: 40, width: getDateTimeSubViewWidth(), justifyContent: 'center' }}>

                        <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >Date</Text>
                        <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{moment(contest.date).format('DD/MM/yyyy')}</Text>
                    </View>

                    {/* Time */}
                    <View style={{ height: 40, width: getDateTimeSubViewWidth(), justifyContent: 'center' }}>
                        <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >Time</Text>
                        <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{moment(contest.time).format('hh:mm A')}</Text>
                        <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 8, bottom: 8, left: 0, width: 1 }} />
                    </View>

                    {/* Map */}
                    {findEnabledTitles().length >= 1 &&
                        <View style={{ height: 40, width: getDateTimeSubViewWidth(), justifyContent: 'center' }}>

                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(0, 'title')}</Text>
                            <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(0, 'value')}</Text>
                            <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 8, bottom: 8, left: 0, width: 1 }} />
                        </View>
                    }

                    {/* Perspective */}
                    {findEnabledTitles().length >= 2 &&
                        <View style={{ height: 40, width: getDateTimeSubViewWidth(), justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.black, fontFamily: fontFamilyStyleNew.regular }} >{findTitleKeyAndValue(1, 'title')}</Text>
                            <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.black, fontFamily: fontFamilyStyleNew.bold }} >{findTitleKeyAndValue(1, 'value')}</Text>
                            <Text style={{ position: 'absolute', backgroundColor: colors.black, top: 8, bottom: 8, left: 0, width: 1 }} />
                        </View>
                    }
                </View>

                {/* Winning Winner Main View */}
                <View style={styles.winningWinnerMainContainer}>

                    {/* Winning Winner Sub View */}
                    <View style={styles.winningWinneSubContainer} >

                        {/* Winning */}
                        <View style={{ height: 40, width: getWinningWinnersSubViewWidth(), justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Prize Pool</Text>
                            <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                {getCurrencyImage()}
                                <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.prizePool}</Text>
                            </View>
                            <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                        </View>

                        {/* Winners */}
                        {showWinnersView() &&
                            <TouchableOpacity style={{ height: 40, width: getWinningWinnersSubViewWidth(), justifyContent: 'center' }} onPress={() => openWiningPrizePool()} activeOpacity={0.5} >
                                <Text style={{ marginLeft: 6, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Winners</Text>

                                <View style={{ marginLeft: 6, flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.totalWinners}</Text>
                                    <Image style={{ marginLeft: 3, width: 7, height: 10, resizeMode: 'contain', tintColor: colors.white }} source={require('../../assets/images/drop_down_winners.png')} />
                                </View>

                                <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                            </TouchableOpacity>
                        }

                        {/* Per Kill */}
                        {showPerKillView() &&
                            <View style={{ height: 40, width: getWinningWinnersSubViewWidth(), justifyContent: 'center' }}>

                                <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Per Kill</Text>
                                <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                    {getCurrencyImage()}
                                    <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.killPoints}</Text>
                                </View>
                                <Text style={{ position: 'absolute', backgroundColor: colors.white, top: 8, bottom: 8, right: 0, width: 1 }} />
                            </View>
                        }

                        {/* Entry Fee */}
                        <View style={{ height: 40, width: getWinningWinnersSubViewWidth(), justifyContent: 'center' }}>
                            <Text style={{ marginLeft: 8, fontSize: 11, color: colors.white, fontFamily: fontFamilyStyleNew.regular }} >Join Using</Text>
                            {contest.entryFee != '0' ?
                                <View style={{ marginTop: 2, marginLeft: 8, flexDirection: 'row', alignItems: 'center' }} >
                                    {getCurrencyImage()}
                                    <Text style={{ marginLeft: 2, fontSize: 12, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >{contest.entryFee}</Text>
                                </View>
                                :
                                <Text style={{ marginLeft: 8, fontSize: 12, marginTop: 2, color: colors.white, fontFamily: fontFamilyStyleNew.bold }} >Free</Text>
                            }

                            {/* <Text style={{ color: colors.white, position: 'absolute', fontSize: 10, top: 7, left: 10, fontFamily: fontFamilyStyleNew.regular }}>Entry Fee</Text>
                            <Text style={{ color: colors.white, position: 'absolute', fontSize: 10, fontWeight: 'bold', top: 19, left: 10, fontFamily: fontFamilyStyleNew.regular }}>{contest.entryFee != '0' ? '₹' + contest.entryFee : '-'}</Text> */}
                        </View>
                    </View>

                    {/* Spots Progress Indicator */}
                    <View style={{ marginLeft: 11, marginRight: 11, height: 5, flex: 1 }} >
                        <ProgressBar style={{ height: 5, width: '100%', backgroundColor: 'white', borderRadius: 2.5 }} progress={getProgressValue()} color={colors.yellow} />
                    </View>

                    {/* Spots Available View */}
                    <View style={styles.spotsAvailableContainer} >
                        {/* Spots Available */}
                        <Text style={{ color: colors.white, fontSize: 10, marginBottom: 0, fontFamily: fontFamilyStyleNew.regular }} >{totalRemainingPlayers()}</Text>

                        {/* Spots Available Left */}
                        <Text style={{ color: colors.white, fontSize: 10, fontFamily: fontFamilyStyleNew.regular }} >{totalJoinedPlayersPlayers()}</Text>
                    </View>
                </View>

                <Modal isVisible={isVissibleWinningPrizePoolPopup}
                    coverScreen={true}
                    testID={'modal'}
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <WiningPrizePool contest={contest} closeWiningPrizePool={closeWiningPrizePool} />
                </Modal>

            </View>
        </>

    );
}

export default ContestCommonDetail;

const styles = StyleSheet.create({
    dateTimeContainer: {
        height: 40,
        backgroundColor: colors.yellow,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    winningWinnerMainContainer: {
        backgroundColor: colors.black,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    winningWinneSubContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    spotsAvailableContainer: {
        marginTop: 5,
        marginLeft: 11,
        marginRight: 11,
        marginBottom: 25,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
});