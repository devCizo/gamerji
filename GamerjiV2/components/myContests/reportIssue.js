import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, ScrollView } from 'react-native'
import colors from '../../assets/colors/colors'
import * as actionCreators from '../../store/actions/index';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { addLog, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { Constants } from '../../appUtils/constants';
import CustomProgressbar from '../../appUtils/customProgressBar';

const ReportIssue = (props) => {


    const dispatch = useDispatch();
    var contestTournament = props.contestTournament
    var isContest = props.isContest
    const [issueList, setIssueList] = useState([])
    const [selectedIssue, setSelectedIssue] = useState(undefined)
    const [isLoading, setLoading] = useState(false);
    const [mainViewHeight, setMainViewHeight] = useState(undefined);

    useEffect(() => {

        // if (isContest) {
        if (contestTournament && contestTournament.game.settings.reportIssues && contestTournament.game.settings.reportIssues.length > 0) {
            var tempArr = contestTournament.game.settings.reportIssues.map(item => {
                let tempItem = { ...item }
                tempItem.isSelected = false
                return tempItem
            })
            setIssueList(tempArr)
            setSelectedIssue(undefined)
        }
        // }


        return () => {
            dispatch(actionCreators.resetReportIssueState())
        }
    }, [])

    let selectIssue = (index) => {

        var tempIssueList = [...issueList]
        tempIssueList.forEach((item, ind) => {
            item.isSelected = false
        })
        tempIssueList[index]['isSelected'] = true
        setIssueList(tempIssueList)
        setSelectedIssue(tempIssueList[index])
    }

    let submitIssue = () => {
        if (!selectedIssue) {
            showErrorToastMessage('Please select issue')
        } else {
            var payload = {
                reportIssue: selectedIssue
            }
            if (isContest) {
                payload['contest'] = contestTournament && contestTournament.item && contestTournament.item._id
            } else {
                payload['event'] = contestTournament && contestTournament.item && contestTournament.item._id
            }
            setLoading(true)
            dispatch(actionCreators.requestReportIssue(payload))
        }
    }

    // API Response
    var { currentState } = useSelector(
        (state) => ({ currentState: state.reportIssue.model }),
        shallowEqual
    );
    var { reportIssueResponse } = currentState

    //Report Issue Response
    useEffect(() => {
        if (reportIssueResponse) {
            setLoading(true)
            if (reportIssueResponse.success == true && reportIssueResponse.item) {
                props.issueSubmitted()
                props.setReportIssuePopupVisibility(false)
            }
        }
        reportIssueResponse = undefined
    }, [reportIssueResponse]);

    let find_dimesions = (layout) => {
        if (mainViewHeight) {
            return
        }
        var { x, y, width, height } = layout;

        if (height >= Constants.SCREEN_HEIGHT) {
            setMainViewHeight(Constants.SCREEN_HEIGHT - 100)
        } else {
            setMainViewHeight(height)
        }

    }

    return (
        /* Main View */
        <View style={{ height: mainViewHeight && mainViewHeight, backgroundColor: colors.yellow, borderTopStartRadius: 50, borderTopEndRadius: 50, }} onLayout={(event) => { find_dimesions(event.nativeEvent.layout) }} >
            {/* Header View */}
            <View style={styles.headerContainer}>

                <Text style={{ flex: 1, marginLeft: 60, color: colors.red, fontFamily: fontFamilyStyleNew.bold, fontSize: 18, textAlign: 'center', }} >REPORT ISSUE</Text>

                {/* Close Button */}
                <TouchableOpacity style={styles.closeButton} onPress={() => props.setReportIssuePopupVisibility(false)} activeOpacity={0.5} >
                    <Image style={{ height: 25, width: 25 }} source={require('../../assets/images/close_icon.png')} ></Image>
                </TouchableOpacity>
            </View>

            <FlatList
                style={{ marginTop: 4, marginLeft: 20, marginRight: 20, flexGrow: 0 }}
                data={issueList}
                renderItem={({ item, index }) => <ReportIssueItem item={item} index={index} selectIssue={selectIssue} />}
                keyExtractor={(item, index) => index.toString()}
            />

            <TouchableOpacity style={{ height: 46, marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 20, flexDirection: 'row', backgroundColor: colors.black, borderRadius: 23, justifyContent: 'space-between', justifyContent: 'center', alignItems: 'center' }} onPress={() => submitIssue()} activeOpacity={0.5} >
                <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >SUBMIT</Text>
            </TouchableOpacity>

            {isLoading && <CustomProgressbar />}

        </View>
    )
}

const ReportIssueItem = (props) => {

    var issue = props.item
    let index = props.index

    return (
        <View style={{}} >

            <TouchableOpacity style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }} onPress={() => props.selectIssue(index)} activeOpacity={0.8} >
                {issue.isSelected && issue.isSelected == true ?
                    <Image style={{ width: 16, height: 16 }} source={require('../../assets/images/red_radio_selected.png')} />
                    :
                    <Image style={{ width: 16, height: 16 }} source={require('../../assets/images/radio_deselected.png')} />
                }
                <Text style={{ marginTop: 12, marginLeft: 10, marginBottom: 12, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} numberOfLines={0} >{issue.issue}</Text>

            </TouchableOpacity>

            <Image style={{ marginBottom: 0, height: 1, backgroundColor: '#000000', opacity: 0.2 }} />
        </View>
    )
}

export default ReportIssue;

const styles = StyleSheet.create({
    headerContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center'
    },
    closeButton: {
        height: 60,
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0
    },
})