import axios from '../../../webServices/axios-api';
import { saveDataToLocalStorage } from '../../../appUtils/sessionManager';
import { AppConstants } from '../../../appUtils/appConstants';
import { addLog } from '../../../appUtils/commonUtlis';
import WebFields from '../../../webServices/webFields.json';

{ /* Action Types */ }
export const REQUEST_UPDATE_FAVORITE_GAMES = 'REQUEST_UPDATE_FAVORITE_GAMES';
export const REQUEST_UPDATE_FAVORITE_GAMES_FAILED = 'REQUEST_UPDATE_FAVORITE_GAMES_FAILED';
export const RESET_UPDATE_FAVORITE_GAMES_STATE = 'RESET_UPDATE_FAVORITE_GAMES_STATE';

{ /* Request - Update Favorite Games Popup */ }
export const updateFavoriteGamesPopupRequest = (viewModel) => {
    return {
        type: REQUEST_UPDATE_FAVORITE_GAMES,
        model: viewModel
    };
};

{ /* Request Failure - Update Favorite Games Popup */ }
export const updateFavoriteGamesPopupRequestFailed = (viewModel) => {
    return {
        type: REQUEST_UPDATE_FAVORITE_GAMES_FAILED,
        model: viewModel
    };
};

{ /* Reset State - Update Favorite Games Popup */ }
export const resetUpdateFavoriteGamesPopupState = () => {
    return {
        type: RESET_UPDATE_FAVORITE_GAMES_STATE,
    };
};

{ /* Request - Update Favorite Games Popup */ }
export const requestUpdateFavoriteGamesPopup = (payload) => {
    return (dispatch) => {
        axios.defaults.headers.post[WebFields.HEADERS.ACCESS_TOKEN] = global.token;
        axios
            .post(WebFields.PROFILE_UPDATE.MODE, payload)
            .then((response) => {
                addLog("URL:::> ", response.config.baseURL + response.config.url);
                addLog("Request:::> ", response.config.data);
                addLog("Response:::> ", response.data);

                global.profile = response.data.item
                global.profile1 = response.data
                saveDataToLocalStorage(AppConstants.key_user_profile_full, JSON.stringify(response.data))
                saveDataToLocalStorage(AppConstants.key_user_profile, JSON.stringify(response.data.item))

                dispatch(updateFavoriteGamesPopupRequest(response.data.item));
            })
            .catch((error) => {
                addLog("Error:::> ", error.response.data);
                dispatch(updateFavoriteGamesPopupRequestFailed(error.response.data));
            })
            .finally(() => {
                // $('#overlay').hide();
            });
    };
};