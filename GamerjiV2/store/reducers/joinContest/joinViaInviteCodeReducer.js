import produce from 'immer';
import { addLog } from '../../../appUtils/commonUtlis';
import * as actionTypes from '../../actions/joinContest/joinViaInviteCodeAction';

const initialState = {
    model: {
        joinViaInviteCodeResponse: {},
    },
    error: false
};

const joinViaInviteCode = (state, action) => {
    return produce(state, (draft) => {
        draft.model.joinViaInviteCodeResponse = action.model;
        draft.sucess = true;
        draft.error = false;
    });
};

const joinViaInviteCodeFailed = (state, action) => {
    return produce(state, (draft) => {
        draft.error = true;
        draft.sucess = false;
        draft.errorMessage = action.model.errors;
        draft.model.joinViaInviteCodeResponse = action.model;
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.JOIN_VIA_INVITE_CODE:
            return joinViaInviteCode(state, action);
        case actionTypes.JOIN_VIA_INVITE_CODE_FAILED:
            return joinViaInviteCodeFailed(state, action);
        case actionTypes.RESET_JOIN_VIA_INVITE_CODE_STATE:
            return resetJoinViaInviteCodeState(state, action);
        default:
            return state;
    }
};

export default reducer;

const resetJoinViaInviteCodeState = (state) => {
    return produce(state, (draft) => {
        draft.error = false;
        draft.sucess = undefined;
        draft.errorMessage = undefined;
        draft.model.joinViaInviteCodeResponse = undefined;
    })
}