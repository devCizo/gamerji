import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, FlatList, TextInput, BackHandler } from 'react-native'
import colors from '../../assets/colors/colors'
import * as RootNavigation from '../../appUtils/rootNavigation.js';
import { Constants } from '../../appUtils/constants';
import { addLog, checkIsSponsorAdsEnabled, showErrorToastMessage } from '../../appUtils/commonUtlis';
import { fontFamilyStyleNew } from '../../appUtils/commonStyles';
import * as actionCreators from '../../store/actions/index';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import CustomProgressbar from '../../appUtils/customProgressBar';
import CustomMarquee from '../../appUtils/customMarquee';
import SponsorBannerAds from '../../commonComponents/SponsorBannerAds';
import { getUserCountry } from '../../appUtils/globleMethodsForUser';
import * as generalSetting from '../../webServices/generalSetting';

const SquadRegistration = (props) => {

    const dispatch = useDispatch();
    const contestTournament = props.route.params.contestTournament
    const roundContest = props.route.params.roundContest
    const isContest = props.route.params.isContest
    const [players, setPlayers] = useState(new Array(Number(contestTournament.gameType.players) + (contestTournament.gameType && Number(contestTournament.gameType.extraPlayers))));
    const [userCheckCurrentIndex, setUserCheckCurrentIndex] = useState(0);
    const [isLoading, setLoading] = useState(false);

    const defaultCountryCode = getUserCountry().dialingCode

    useEffect(() => {
        return () => {
            dispatch(actionCreators.resetSquadRegistrationState())
        }
    }, [])

    // Add or remove player from players array
    let addRemovePlayer = (isAddPlayer, index, mobileNumber) => {

        if (isAddPlayer) {

            if (mobileNumber == '') {
                showErrorToastMessage('Please enter mobile number')
            } else if (mobileNumber.length < 8) {
                showErrorToastMessage('Please enter valid mobile number')
            } else if (mobileNumber == global.profile.phone) {
                showErrorToastMessage('Enter detail other than yours')
            } else {
                setUserCheckCurrentIndex(index)
                let payload = {
                    phone: mobileNumber,
                    dialingCode: defaultCountryCode
                }
                // API Call
                setLoading(true)
                dispatch(actionCreators.requestCheckSquad(payload))
            }
        } else {
            var playersTemp = [...players]
            playersTemp[index] = undefined
            setPlayers(playersTemp)
        }
    }

    // API Response
    const { currentState } = useSelector(
        (state) => ({ currentState: state.squadRegistration.model }),
        shallowEqual
    );
    var { checkSquadResponse, joinContestSquadResponse, errorMessage, success } = currentState

    // Validate and add squad data in players
    useEffect(() => {
        if (checkSquadResponse) {
            setLoading(false)
            if (checkSquadResponse.item) {

                var message = ''

                if (checkSquadResponse.item.gameNames && contestTournament && contestTournament.game && contestTournament.game._id) {
                    let index = checkSquadResponse.item.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
                    if (index < 0) {
                        message = 'Please ask user to add game name'
                    }
                } else {
                    message = 'Please ask user to add game name'
                }

                if (message != '') {
                    showErrorToastMessage(message)
                    return
                }

                if (!checkSquadResponse.item.gamerjiName) {
                    message = 'Please ask user to add gamerji name'
                } else if (checkSquadResponse.item.gamerjiName == '') {
                    message = 'Please ask user to add gamerji name'
                }

                if (message != '') {
                    showErrorToastMessage(message)
                    return
                }

                var playersTemp = [...players]
                playersTemp[userCheckCurrentIndex] = checkSquadResponse
                setPlayers(playersTemp)


            } else if (errorMessage[0] && errorMessage[0].msg) {
                showErrorToastMessage(errorMessage[0].msg)
            }

            checkSquadResponse = {}
            errorMessage = []
            success = false
        }

    }, [checkSquadResponse]);

    //Save and Join validation and API
    let saveTeam = () => {
        for (let index = 0; index < players.length; index++) {
            if (index != 0 && contestTournament.gameType.players > index) {
                if (!players[index]) {
                    showErrorToastMessage(`Please add player ${index + 1} detail`)
                    return
                }
            }
        }

        var fetchedPlayers = players.filter(function (item) {
            return item ? true : false
        })

        var playersArr = fetchedPlayers.map(item => {
            return item.item._id
        })

        var payload = {
            team: playersArr,
        }
        if (isContest) {
            payload['contest'] = contestTournament._id
            payload['type'] = 'contest'
        } else {
            payload['event'] = contestTournament._id
            payload['contest'] = roundContest._id
            payload['type'] = 'event'
        }

        if (global.profile.gameNames) {
            let index = global.profile.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
            if (index > -1) {
                payload['uniqueIGN'] = global.profile.gameNames[index].uniqueIGN
            }
        }
        if (contestTournament.isActive) {
            payload['isActive'] = contestTournament.isActive
        }

        setLoading(true)
        dispatch(actionCreators.requestJoinContestWithSquad(payload))
    }

    useEffect(() => {

        if (joinContestSquadResponse) {
            setLoading(false)
            if (joinContestSquadResponse.item) {
                addLog('Contest joined')

                if (isContest) {
                    RootNavigation.goBack()
                    props.route.params.setContestAsJoined()
                } else {
                    RootNavigation.goBack()
                    props.route.params.setTournamentAsJoined()
                }
            } else {
                if (joinContestSquadResponse.errors && joinContestSquadResponse.errors[0] && joinContestSquadResponse.errors[0].msg) {
                    showErrorToastMessage(joinContestSquadResponse.errors[0].msg)
                }
            }
        }

        joinContestResponse = undefined
        errorMessage = undefined
        success = false

    }, [joinContestSquadResponse]);

    // This function should allow users to go back to the Login Screen and returns true.
    function handleBackButton() {
        props.navigation.goBack();
        return true;
    }

    // This Use-Effect function should call the handleBackButton and returns RemoveEventListener.
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.black }}>

            {/* Navigation Bar */}
            <View style={styles.navigationView} >

                {/* Back Button */}
                {<TouchableOpacity style={styles.backButton} onPress={() => RootNavigation.goBack()} activeOpacity={0.5} >
                    <Image style={styles.backImage} source={require('../../assets/images/back_icon.png')} />
                </TouchableOpacity>}

                <Text style={styles.navigationTitle} >SQUAD REGISTRATIONS</Text>

            </View>

            {/* Container View */}
            <View style={[styles.roundContainer]}>

                <FlatList
                    style={{ marginTop: 20, marginLeft: 14, marginRight: 14 }}
                    data={players}
                    renderItem={({ item, index }) => <SquadRegistrationItem item={item} index={index} requireUser={Number(contestTournament.gameType.players)} addRemovePlayer={addRemovePlayer} contestTournament={contestTournament} defaultCountryCode={defaultCountryCode} />}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'always'}

                    ListFooterComponent={
                        // Save Team
                        < TouchableOpacity style={styles.saveTeam} onPress={() => saveTeam()} activeOpacity={0.5} >
                            <Text style={{ marginLeft: 22, color: colors.white, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >SAVE TEAM</Text>
                            <Image style={{ right: 22, width: 16, height: 14, resizeMode: 'contain' }} source={require('../../assets/images/right_arrow.png')}></Image>
                        </TouchableOpacity>
                    }

                ></FlatList>

            </View>

            {isLoading && <CustomProgressbar />}
        </SafeAreaView>
    );
}

const SquadRegistrationItem = (props) => {

    let showRequireTitle = (props.index < props.requireUser)
    let contestTournament = props.contestTournament
    var [mobileNumber, setMobileNumber] = useState('');
    var player = props.item

    let getCaptainUserName = () => {
        if (global.profile.gameNames) {
            let index = global.profile.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
            if (index > -1) {
                return global.profile.gameNames[index].uniqueIGN
            }
        }
    }

    let addRemovePlayerInternal = () => {
        let isAddPlayer = player ? false : true
        props.addRemovePlayer(isAddPlayer, props.index, mobileNumber)

        !isAddPlayer && setMobileNumber('')
    }

    let findGameName = () => {
        if (player && player.item && player.item.gameNames && contestTournament && contestTournament.game && contestTournament.game._id) {
            let index = player.item.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
            if (index > -1) {
                return player.item.gameNames[index].uniqueIGN
            }
        }
        return '-'
    }

    let getGamerJiName = () => {
        if (player && player.item && player.item.gamerjiName) {
            return player.item.gamerjiName
        }
        return '-'
    }

    return (
        <View>
            {props.index == 0 ?
                <>
                    {/* Captain Data */}
                    <View style={{ marginTop: 30, marginBottom: 10, backgroundColor: colors.yellow, borderRadius: 20 }} >

                        <View style={{ marginRight: 0, height: 24, width: 80, backgroundColor: colors.black, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-end' }} >
                            <Text style={{ color: colors.white, textAlign: 'center', fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Captain</Text>
                        </View>

                        {/* Mobile Number */}
                        <View style={{ marginTop: 28, marginLeft: 26, marginRight: 26, height: 40, borderRadius: 20, backgroundColor: colors.white, justifyContent: 'center' }} >
                            <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: '#70717A' }} >{props.defaultCountryCode} {global.profile && global.profile.phone}</Text>
                        </View>

                        <View style={{ marginTop: 20, height: 84, flexDirection: 'row' }} >

                            {/* InGame Name */}
                            <View style={{ width: (Constants.SCREEN_WIDTH - 38) / 2, backgroundColor: colors.black, borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomLeftRadius: 20, justifyContent: 'center' }} >

                                <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.white }} >InGame Name</Text>
                                <CustomMarquee style={{ marginTop: 6, marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white }} value={getCaptainUserName()} />
                            </View>

                            {/* Gamerji Name */}
                            <View style={{ marginLeft: 10, width: (Constants.SCREEN_WIDTH - 38) / 2, backgroundColor: colors.black, borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomRightRadius: 20, justifyContent: 'center' }} >

                                <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.white }} >Gamerji Name</Text>
                                <CustomMarquee style={{ marginTop: 6, marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.white }} value={global.profile && global.profile.gamerjiName} />

                            </View>

                        </View >
                    </View>

                    {/* Top Image */}
                    <Image style={{ position: 'absolute', top: 0, height: 60, width: 60, alignSelf: 'center', resizeMode: 'contain' }} source={global.profile.level.featuredImage && global.profile.level.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + global.profile.level.featuredImage.default }} />
                </>

                :

                // Other Player Data
                <View style={{ marginTop: 10, backgroundColor: colors.black, borderRadius: 20 }} >

                    <View style={{ marginRight: 0, height: 24, width: showRequireTitle ? 80 : 140, backgroundColor: colors.red, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-end' }} >
                        <Text style={{ color: colors.white, textAlign: 'center', fontSize: 14, fontFamily: fontFamilyStyleNew.semiBold }} >Player-{props.index + 1}{!showRequireTitle && ' ( Optional )'}</Text>
                    </View>

                    {/* Enter Mobile Number */}
                    <View style={{ marginTop: 10, marginRight: 12, marginLeft: 12, flexDirection: 'row', alignItems: 'center' }} >

                        {player && player.level ?
                            <Image style={{ height: 60, width: 60, alignSelf: 'center', resizeMode: 'contain' }} source={player.level.featuredImage && player.level.featuredImage.default && { uri: generalSetting.UPLOADED_FILE_URL + player.level.featuredImage.default }} />
                            :
                            <Image style={{ height: 60, width: 60, alignSelf: 'center' }} source={require('../../assets/images/squad-dummy.png')} />
                        }

                        <View style={{ marginLeft: 12, flex: 1, height: 40, backgroundColor: colors.white, borderRadius: 20, flexDirection: 'row', alignItems: 'center' }} >

                            <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: '#70717A' }} >{props.defaultCountryCode} </Text>

                            {/* Mobile number input */}
                            <TextInput
                                style={{ marginLeft: 4, flex: 1, height: 40, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: '#70717A' }}
                                placeholder='Enter Mobile No'
                                keyboardType={'number-pad'}
                                onChangeText={text => setMobileNumber(text)}
                                editable={player ? false : true}
                            >{mobileNumber}</TextInput>

                            {/* Add remove button */}
                            <TouchableOpacity
                                style={{ marginRight: 0, height: 40, width: 80, alignSelf: 'flex-end', borderRadius: 20, backgroundColor: player ? colors.red : colors.yellow, alignItems: 'center', justifyContent: 'center' }}
                                onPress={() => addRemovePlayerInternal()} activeOpacity={0.5} >

                                <Text style={{ color: player ? colors.white : colors.black, fontFamily: fontFamilyStyleNew.bold, fontSize: 16 }} >{player ? 'Remove' : 'Apply'}</Text>

                            </TouchableOpacity>

                        </View>
                    </View>

                    <View style={{ marginTop: 20, height: 60, flexDirection: 'row' }} >

                        {/* InGame Name */}
                        <View style={{ width: (Constants.SCREEN_WIDTH - 38) / 2, backgroundColor: colors.yellow, borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomLeftRadius: 20, justifyContent: 'center' }} >

                            <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }} >InGame Name</Text>
                            <CustomMarquee style={{ marginTop: 2, marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.black }} value={findGameName()} />
                        </View>

                        {/* Gamerji Name */}
                        <View style={{ marginLeft: 10, width: (Constants.SCREEN_WIDTH - 38) / 2, backgroundColor: colors.yellow, borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomRightRadius: 20, justifyContent: 'center' }} >

                            <Text style={{ marginLeft: 11, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }} >Gamerji Name</Text>
                            <CustomMarquee style={{ marginTop: 2, marginLeft: 11, fontFamily: fontFamilyStyleNew.bold, fontSize: 16, color: colors.black }} value={getGamerJiName()} />

                        </View>
                    </View>
                </View>
            }

            {checkIsSponsorAdsEnabled('squadRegistration') &&
                <SponsorBannerAds screenCode={'squadRegistration'} checkIsSponsorAdsEnabled={checkIsSponsorAdsEnabled('squadRegistration')} />
            }
        </View>
    )
}
// const SquadRegistrationItem = (props) => {

//     let showRequireTitle = (props.index < props.requireUser)
//     let contestTournament = props.contestTournament
//     var [mobileNumber, setMobileNumber] = useState('');
//     var player = props.item

//     let getCaptainUserName = () => {
//         if (global.profile.gameNames) {
//             let index = global.profile.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
//             if (index > -1) {
//                 return global.profile.gameNames[index].uniqueIGN
//             }
//         }
//     }

//     let addRemovePlayerInternal = () => {
//         let isAddPlayer = player ? false : true
//         props.addRemovePlayer(isAddPlayer, props.index, mobileNumber)

//         !isAddPlayer && setMobileNumber('')
//     }

//     let findGameName = () => {
//         if (player && player.item && player.item.gameNames && contestTournament && contestTournament.game && contestTournament.game._id) {
//             let index = player.item.gameNames.findIndex(obj => obj.game == contestTournament.game._id)
//             if (index > -1) {
//                 return player.item.gameNames[index].uniqueIGN
//             }
//         }
//         return ''
//     }

//     let getGamerJiName = () => {
//         if (player && player.item && player.item.gamerjiName) {
//             return player.item.gamerjiName
//         }
//         return ''
//     }

//     return (

//         <View>
//             {props.index == 0 ?

//                 // Captain Data
//                 <View>

//                     {/* Captain Text */}
//                     <View style={{ flexDirection: 'row', height: 27 }} >

//                         <View style={{ marginTop: 4, height: 15, width: 15, backgroundColor: colors.yellow, justifyContent: 'center', alignItems: 'center' }} >
//                             <Text style={{ color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 10 }} >C</Text>
//                         </View>

//                         <Text style={{ marginTop: 3, marginLeft: 8, color: colors.black, textAlign: 'center', fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Captain</Text>

//                     </View>

//                     <View style={{ borderRadius: 8, marginBottom: 8, borderWidth: 1, borderColor: colors.grey, backgroundColor: 'white' }} elevation={5} >

//                         {/* Mobile Number */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13 }} >Mobile No</Text>

//                             <View style={{ position: 'absolute', left: 90, top: 0, right: 0, bottom: 0, flexDirection: 'row', alignItems: 'center' }} >

//                                 <Image style={{ width: 20, resizeMode: 'center' }} source={require('../../assets/images/india_flag.png')} ></Image>
//                                 <Text style={{ marginLeft: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >+91</Text>
//                                 <Text style={{ marginLeft: 8, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >{global.profile && global.profile.phone}</Text>

//                                 <View style={{ marginLeft: 4, height: 15, width: 15, backgroundColor: colors.yellow, justifyContent: 'center', alignItems: 'center' }} >
//                                     <Text style={{ color: 'white', fontFamily: fontFamilyStyleNew.semiBold, fontSize: 10 }} >C</Text>
//                                 </View>

//                             </View>

//                             <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 0.5, backgroundColor: colors.grey }} />

//                         </View>

//                         {/* Game User Name */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13, width: 70 }} numberOfLines={2} >{contestTournament && contestTournament.game && contestTournament.game.name} Name</Text>

//                             <Text style={{ position: 'absolute', left: 92, top: 14, bottom: 0, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >{getCaptainUserName()}</Text>

//                             <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 0.5, backgroundColor: colors.grey }} />

//                         </View>

//                         {/* Gamerji Name */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13, width: 70 }} >Gamerji Name</Text>

//                             <Text style={{ position: 'absolute', left: 92, top: 14, bottom: 0, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >{global.profile && global.profile.gamerjiName}</Text>

//                         </View>
//                     </View>

//                 </View>
//                 :

//                 //  Other User Data
//                 <View>
//                     <View style={{ flexDirection: 'row', height: 27 }} >

//                         <Text style={{ marginTop: 3, color: colors.black, textAlign: 'center', fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >Player-{props.index + 1}</Text>

//                         {showRequireTitle ?
//                             <Text />
//                             :
//                             <Text style={{ marginTop: 3, marginLeft: 8, color: colors.grey, textAlign: 'center', fontFamily: fontFamilyStyleNew.regular, fontSize: 13 }} >(Optional)</Text>
//                         }

//                     </View>

//                     <View style={{ borderRadius: 8, marginBottom: 8, borderWidth: 1, borderColor: colors.grey, backgroundColor: 'white' }} elevation={5} >

//                         {/* Mobile Number */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13 }} >Mobile No</Text>

//                             <View style={{ position: 'absolute', left: 90, top: 0, right: 0, bottom: 0, flexDirection: 'row', alignItems: 'center' }} >

//                                 {/* County Code */}
//                                 <TouchableOpacity style={{ height: 46, width: 80, flexDirection: 'row', alignItems: 'center' }} activeOpacity={0.5} >
//                                     <Image style={{ width: 20, resizeMode: 'center' }} source={require('../../assets/images/india_flag.png')} ></Image>
//                                     <Text style={{ marginLeft: 4, color: colors.black, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >+91</Text>

//                                     <Image style={{ marginLeft: 10, width: 11, height: 6, resizeMode: 'center' }} source={require('../../assets/images/drop_down.png')} ></Image>
//                                 </TouchableOpacity>

//                                 <TextInput
//                                     style={{ height: 46, flex: 1, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }}
//                                     placeholder='Mobile No'
//                                     keyboardType={'number-pad'}
//                                     onChangeText={text => setMobileNumber(text)}
//                                     editable={player ? false : true}
//                                 >{mobileNumber}</TextInput>

//                                 <TouchableOpacity
//                                     style={{ position: 'absolute', height: 34, right: 10, width: 70, borderRadius: 17, backgroundColor: player ? colors.red : colors.black, alignItems: 'center', justifyContent: 'center' }}
//                                     onPress={() => addRemovePlayerInternal()} activeOpacity={0.5} >

//                                     <Text style={{ color: colors.white, fontFamily: fontFamilyStyleNew.regular, fontSize: 14 }} >{player ? 'Remove' : 'Add'}</Text>

//                                 </TouchableOpacity>

//                             </View>

//                             <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 0.5, backgroundColor: colors.grey }} />

//                         </View>

//                         {/* Game User Name */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13, width: 70 }} numberOfLines={2} >{contestTournament && contestTournament.game && contestTournament.game.name} Name</Text>

//                             <TextInput style={{ position: 'absolute', left: 92, top: 0, bottom: 0, right: 0, height: 46, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }} placeholder={`Player ${props.index + 1} Username`} editable={false} >{findGameName()}</TextInput>

//                             <Image style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 0.5, backgroundColor: colors.grey }} />

//                         </View>

//                         {/* Gamerji Name */}
//                         <View style={{ height: 46, flexDirection: 'row', alignItems: 'center' }} >

//                             <Text style={{ marginLeft: 8, color: colors.grey, fontFamily: fontFamilyStyleNew.regular, fontSize: 13, width: 70 }} >Gamerji Name</Text>

//                             <TextInput style={{ position: 'absolute', left: 92, top: 0, bottom: 0, right: 0, height: 46, fontFamily: fontFamilyStyleNew.regular, fontSize: 14, color: colors.black }} placeholder={`Player ${props.index + 1} gamerji name`} editable={false} >{getGamerJiName()}</TextInput>

//                         </View>
//                     </View>

//                 </View>
//             }
//         </View >
//     )
// }

const styles = StyleSheet.create({
    navigationView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    backButton: {
        left: 0,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backImage: {
        width: 25,
        height: 23,
    },
    navigationTitle: {
        color: 'white',
        fontFamily: fontFamilyStyleNew.extraBold,
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'stretch',
        marginRight: 0,
        flex: 1,
        marginRight: 50,
        alignSelf: 'center'
    },
    roundContainer: {
        flex: 1,
        marginTop: 0,
        backgroundColor: 'white',
        borderTopStartRadius: 40,
        borderTopEndRadius: 40,
    },
    saveTeam: {
        marginTop: 20,
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 20,
        height: 46,
        flexDirection: 'row',
        backgroundColor: colors.black,
        borderRadius: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default SquadRegistration;